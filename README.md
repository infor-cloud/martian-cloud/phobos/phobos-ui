# Phobos UI

Phobos UI is a modern, fast, and user-friendly frontend written in TypeScript using ReactJS. Powered by GraphQL and WebSockets, it a tool for deployment orchestration and release management. More information coming soon...

## Get started

## Documentation

## Security

If you've discovered a security vulnerability in the Phobos UI, please let us know by creating a **confidential** issue in this project.

## License

Phobos UI is distributed under [Mozilla Public License v2.0](https://www.mozilla.org/en-US/MPL/2.0/).
