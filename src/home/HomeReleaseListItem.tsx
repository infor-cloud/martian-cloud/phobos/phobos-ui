import { Link, ListItemButton, ListItemIcon, ListItemText } from "@mui/material";
import graphql from 'babel-plugin-relay/macro';
import { useFragment } from "react-relay";
import { useNavigate } from 'react-router-dom';
import PipelineStatusType from "../organizations/projects/pipelines/PipelineStatusType";
import { HomeReleaseListItemFragment_release$key } from "./__generated__/HomeReleaseListItemFragment_release.graphql";

interface Props {
    fragmentRef: HomeReleaseListItemFragment_release$key
    last?: boolean
}

function HomeReleaseListItem({ fragmentRef, last }: Props) {
    const navigate = useNavigate();

    const data = useFragment(graphql`
        fragment HomeReleaseListItemFragment_release on Release {
            id
            semanticVersion
            project {
                name
                organizationName
            }
            pipeline {
                status
            }
        }
    `, fragmentRef);

    return (
        <ListItemButton
            dense
            onClick={() => navigate(`/organizations/${data.project.organizationName}/projects/${data.project.name}/-/releases/${data.id}`)}
            divider={!last}
        >
            <ListItemIcon sx={{ minWidth: 48 }}>
                {PipelineStatusType[data.pipeline.status]?.icon}
            </ListItemIcon>
            <ListItemText
                sx={{ wordBreak: 'break-word' }}
                primary={<Link
                    underline="hover"
                    fontWeight={500}
                    variant="body2"
                    color="textPrimary"
                >
                    {data.semanticVersion}
                </Link>}
                secondary={`${data.project.organizationName}/${data.project.name}`}
            />
        </ListItemButton>
    );
}

export default HomeReleaseListItem
