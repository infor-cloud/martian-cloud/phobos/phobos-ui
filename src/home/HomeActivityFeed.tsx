import { Box, Typography, useTheme } from '@mui/material';
import graphql from 'babel-plugin-relay/macro';
import InfiniteScroll from 'react-infinite-scroll-component';
import { useLazyLoadQuery, usePaginationFragment } from 'react-relay';
import ActivityEventList from '../activity/ActivityEventList';
import ListSkeleton from '../skeletons/ListSkeleton';
import { HomeActivityFeedFragment_activity$key } from './__generated__/HomeActivityFeedFragment_activity.graphql';
import { HomeActivityFeedPaginationQuery } from './__generated__/HomeActivityFeedPaginationQuery.graphql';
import { HomeActivityFeedQuery } from './__generated__/HomeActivityFeedQuery.graphql';

const INITIAL_ITEM_COUNT = 20;

interface Props {
    userId: string | undefined;
}

function HomeActivityFeed({ userId }: Props) {
    const theme = useTheme();
    const queryData = useLazyLoadQuery<HomeActivityFeedQuery>(graphql`
        query HomeActivityFeedQuery($first: Int, $after: String, $userId: String) {
           ...HomeActivityFeedFragment_activity
        }
    `, { first: INITIAL_ITEM_COUNT, userId }, { fetchPolicy: 'store-and-network' });

    const { data, loadNext, hasNext } = usePaginationFragment<HomeActivityFeedPaginationQuery, HomeActivityFeedFragment_activity$key>(
        graphql`
      fragment HomeActivityFeedFragment_activity on Query
      @refetchable(queryName: "HomeActivityFeedPaginationQuery") {
        activityEvents(
            after: $after
            first: $first
            sort: CREATED_AT_DESC
            userId: $userId
        ) @connection(key: "HomeActivityFeed_activityEvents") {
            edges {
                node {
                    id
                }
            }
            ...ActivityEventListFragment_connection
        }
      }
    `, queryData);

    const eventCount = data.activityEvents?.edges?.length || 0;

    return (
        <Box>
            {eventCount > 0 && <InfiniteScroll
                dataLength={data.activityEvents?.edges?.length ?? 0}
                next={() => loadNext(INITIAL_ITEM_COUNT)}
                hasMore={hasNext}
                loader={<ListSkeleton rowCount={3} />}
            >
                <ActivityEventList showOrgLink showProjectLink fragmentRef={data.activityEvents} />
            </InfiniteScroll>}
            {eventCount === 0 && <Box
                border={`1px dashed ${theme.palette.divider}`}
                borderRadius={2}
                mt={4}
                display="flex"
                justifyContent="center"
            >
                <Box
                    padding={4}
                    display="flex"
                    flexDirection="column"
                    justifyContent="center"
                    alignItems="center"
                    sx={{ maxWidth: 600 }}
                >
                    <Typography variant="subtitle1" color="textSecondary">You do not have any activity events</Typography>
                </Box>
            </Box>}
        </Box>
    );
}

export default HomeActivityFeed;
