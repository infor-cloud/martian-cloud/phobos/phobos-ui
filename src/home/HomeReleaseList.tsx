import { Box, Link, List, ListItem, Typography, useTheme } from '@mui/material';
import graphql from 'babel-plugin-relay/macro';
import { useLazyLoadQuery, usePaginationFragment } from 'react-relay/hooks';
import HomeReleaseListItem from './HomeReleaseListItem';
import { HomeReleaseListFragment_releases$key } from './__generated__/HomeReleaseListFragment_releases.graphql';
import { HomeReleaseListPaginationQuery } from './__generated__/HomeReleaseListPaginationQuery.graphql';
import { HomeReleaseListQuery } from './__generated__/HomeReleaseListQuery.graphql';

const INITIAL_ITEM_COUNT = 5;

const query = graphql`
    query HomeReleaseListQuery($first: Int!, $after: String) {
        me {
            ...on User {
                ...HomeReleaseListFragment_releases
            }
        }
    }
`;

function HomeReleaseList() {
    const theme = useTheme();
    const queryData = useLazyLoadQuery<HomeReleaseListQuery>(query, { first: INITIAL_ITEM_COUNT }, { fetchPolicy: 'store-and-network' });

    const { data, loadNext, hasNext } = usePaginationFragment<HomeReleaseListPaginationQuery, HomeReleaseListFragment_releases$key>(
        graphql`
        fragment HomeReleaseListFragment_releases on User
        @refetchable(queryName: "HomeReleaseListPaginationQuery") {
            releases(
                first: $first
                after: $after
                sort: UPDATED_AT_DESC
                ) @connection(key: "HomeReleaseList_releases") {
                    totalCount
                    edges {
                        node {
                            id
                            ...HomeReleaseListItemFragment_release
                        }
                    }
                }
            }
        `, queryData.me);

    const edgeCount = data?.releases?.edges?.length ?? 0;

    return (
        <Box>
            <Typography variant="subtitle1" fontWeight={600}>Recent Releases</Typography>
            {edgeCount === 0 && <Box sx={{ mt: 2, mb: 2, p: 2, border: `1px dashed ${theme.palette.divider}`, borderRadius: 2 }}>
                <Typography color="textSecondary" variant="body2">
                    You do not have any recent releases. You must be added as a participant to a release to see it here.
                </Typography>
            </Box>}
            {edgeCount > 0 && <List>
                {data?.releases?.edges?.map((edge: any, index: number) => <HomeReleaseListItem
                    key={edge.node.id}
                    last={index === (edgeCount - 1)}
                    fragmentRef={edge.node} />
                )}
                {hasNext && <ListItem>
                    <Link
                        variant="body2"
                        color="textSecondary"
                        sx={{ cursor: 'pointer' }}
                        underline="hover"
                        onClick={() => loadNext(INITIAL_ITEM_COUNT)}
                    >
                        Show more
                    </Link>
                </ListItem>}
            </List>}
        </Box>
    );
}

export default HomeReleaseList
