import { Box, Link, List, ListItem, Typography, useTheme } from '@mui/material';
import graphql from 'babel-plugin-relay/macro';
import throttle from 'lodash.throttle';
import { useEffect, useMemo, useState } from 'react';
import { fetchQuery, useLazyLoadQuery, usePaginationFragment, useRelayEnvironment } from 'react-relay/hooks';
import SearchInput from '../common/SearchInput';
import HomeProjectListItem from './HomeProjectListItem';
import { HomeProjectListFragment_projects$key } from './__generated__/HomeProjectListFragment_projects.graphql';
import { HomeProjectListPaginationQuery } from './__generated__/HomeProjectListPaginationQuery.graphql';
import { HomeProjectListQuery } from './__generated__/HomeProjectListQuery.graphql';

const INITIAL_ITEM_COUNT = 7;

const query = graphql`
    query HomeProjectListQuery($first: Int!, $after: String, $search: String) {
        ...HomeProjectListFragment_projects
    }
`;

function HomeProjectList() {
    const theme = useTheme();
    const [search, setSearch] = useState<string | undefined>('');
    const [isRefreshing, setIsRefreshing] = useState(false);

    const queryData = useLazyLoadQuery<HomeProjectListQuery>(query, { first: INITIAL_ITEM_COUNT }, { fetchPolicy: 'store-and-network' });

    const { data, loadNext, hasNext, refetch } = usePaginationFragment<HomeProjectListPaginationQuery, HomeProjectListFragment_projects$key>(
        graphql`
        fragment HomeProjectListFragment_projects on Query
        @refetchable(queryName: "HomeProjectListPaginationQuery") {
            projects(
                first: $first
                after: $after
                search: $search
                sort: UPDATED_AT_DESC
                ) @connection(key: "HomeProjectList_projects") {
                    totalCount
                    edges {
                        node {
                            id
                            ...HomeProjectListItemFragment_project
                        }
                    }
                }
            }
        `, queryData);

    const environment = useRelayEnvironment();

    const fetch = useMemo(
        () =>
            throttle(
                (input?: string) => {
                    setIsRefreshing(true);

                    const normalizedInput = input?.trim();

                    fetchQuery(environment, query, { first: INITIAL_ITEM_COUNT, search: normalizedInput })
                        .subscribe({
                            complete: () => {
                                setIsRefreshing(false);
                                setSearch(input);

                                // *After* the query has been fetched, we call
                                // refetch again to re-render with the updated data.
                                // At this point the data for the query should
                                // be cached, so we use the 'store-only'
                                // fetchPolicy to avoid suspending.
                                refetch({ first: INITIAL_ITEM_COUNT, search: normalizedInput }, { fetchPolicy: 'store-only' });
                            },
                            error: () => {
                                setIsRefreshing(false);
                            }
                        }
                        );
                },
                2000,
                { leading: false, trailing: true }
            ),
        [environment, refetch],
    );

    useEffect(() => {
        return () => {
            fetch.cancel()
        }
    }, [fetch]);

    const onSearchChange = (event: React.ChangeEvent<HTMLInputElement>) => {
        fetch(event.target.value.toLowerCase().trim());
    };

    const onKeyDown = (event: React.KeyboardEvent<HTMLInputElement>) => {
        // Only handle enter key type
        if (event.key === 'Enter') {
            fetch.flush();
        }
    };

    const edges = data?.projects?.edges ?? [];
    const edgeCount = edges.length;

    return (
        <Box>
            {search === '' && edgeCount === 0 && <Box sx={{ p: 2, border: `1px dashed ${theme.palette.divider}`, borderRadius: 2 }}>
                <Typography color="textSecondary" variant="body2">You are not currently a member of any projects.</Typography>
            </Box>}
            {(search !== '' || edgeCount !== 0) && <Box>
                {(edgeCount > 1 || search !== '') && <SearchInput
                    placeholder="search by project name"
                    fullWidth
                    onChange={onSearchChange}
                    onKeyDown={onKeyDown}
                />}
                {(edgeCount === 0 && search !== '') && <Typography
                    sx={{ p: 2 }}
                    variant="body2"
                    align="center"
                    color="textSecondary"
                >
                    No projects matching search <strong>{search}</strong>
                </Typography>}
                <List sx={isRefreshing ? { opacity: 0.5 } : null} >
                    {edges.map((edge: any, index: number) => <HomeProjectListItem
                        key={edge.node.id}
                        fragmentRef={edge.node}
                        last={index === (edgeCount - 1)}
                    />)}
                    {hasNext && <ListItem>
                        <Link
                            variant="body2"
                            color="textSecondary"
                            sx={{ cursor: 'pointer' }}
                            underline="hover"
                            onClick={() => loadNext(INITIAL_ITEM_COUNT)}
                        >
                            Show more
                        </Link>
                    </ListItem>}
                </List>
            </Box>}
        </Box>
    );
}

export default HomeProjectList
