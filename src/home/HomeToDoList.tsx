import { Box, Link, ListItem, Typography, useTheme } from '@mui/material';
import graphql from 'babel-plugin-relay/macro';
import { useMemo } from 'react';
import { ConnectionHandler, useLazyLoadQuery, usePaginationFragment } from 'react-relay/hooks';
import ToDoList from '../todo/ToDoList';
import { HomeToDoListFragment_todoItems$key } from './__generated__/HomeToDoListFragment_todoItems.graphql';
import { HomeToDoListPaginationQuery } from './__generated__/HomeToDoListPaginationQuery.graphql';
import { HomeToDoListQuery } from './__generated__/HomeToDoListQuery.graphql';

const INITIAL_ITEM_COUNT = 5;

interface Props {
    noResultsText: string;
    projectId?: string;
}

function HomeToDoList({ noResultsText, projectId }: Props) {
    const theme = useTheme();
    const query = useLazyLoadQuery<HomeToDoListQuery>(graphql`
        query HomeToDoListQuery($first: Int, $after: String, $projectId: String) {
            me {
                ...on User {
                    ...HomeToDoListFragment_todoItems
                }
            }
        }`, { first: INITIAL_ITEM_COUNT, projectId }, { fetchPolicy: 'store-and-network' });

    const { data, loadNext, hasNext } = usePaginationFragment<HomeToDoListPaginationQuery, HomeToDoListFragment_todoItems$key>(
        graphql`
            fragment HomeToDoListFragment_todoItems on User
            @refetchable(queryName: "HomeToDoListPaginationQuery") {
                id
                todoItems(
                    first: $first
                    after: $after
                    resolved: false
                    sort: CREATED_AT_DESC,
                    projectId: $projectId
                ) @connection(key: "HomeToDoList_todoItems") {
                    totalCount
                    edges {
                        node {
                            id
                        }
                    }
                    ...ToDoListFragment_connection
                }
            }
        `, query.me
    );

    const getConnections = useMemo(() => {
        return (userId: string, resolved: boolean): string[] => {
            if (resolved) {
                return [];
            }

            const connectionId = ConnectionHandler.getConnectionID(
                userId,
                'HomeToDoList_todoItems',
                { resolved: false, sort: "CREATED_AT_DESC", projectId }
            );
            return [connectionId];
        };
    }, [projectId]);

    const userId = data?.id as string;
    const count = data?.todoItems?.totalCount ?? 0;

    return data ? (
        <Box>
            {count === 0 && <Typography variant="subtitle1" fontWeight={600}>To-Do Items</Typography>}
            {count > 0 && <Typography variant="subtitle1" fontWeight={600}>{count} To-Do Item{count === 1 ? '' : 's'}</Typography>}
            {count === 0 && <Box sx={{ mt: 2, mb: 2, p: 2, border: `1px dashed ${theme.palette.divider}`, borderRadius: 2 }}>
                <Typography color="textSecondary" variant="body2">
                    {noResultsText}
                </Typography>
            </Box>}
            {count > 0 && <ToDoList fragmentRef={data.todoItems} userId={userId} getConnections={getConnections} dense />}
            {hasNext && <ListItem>
                <Link
                    variant="body2"
                    color="textSecondary"
                    sx={{ cursor: 'pointer' }}
                    underline="hover"
                    onClick={() => loadNext(INITIAL_ITEM_COUNT)}
                >
                    Show more
                </Link>
            </ListItem>}
        </Box>
    ) : null;
}

export default HomeToDoList;
