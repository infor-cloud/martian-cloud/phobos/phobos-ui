import RocketLaunchIcon from '@mui/icons-material/RocketLaunchOutlined';
import HelpIcon from '@mui/icons-material/HelpOutline';
import { Box, CircularProgress, Link, ListItemButton, Paper, ToggleButton, Typography, useTheme } from '@mui/material';
import graphql from 'babel-plugin-relay/macro';
import React, { Suspense, useContext } from 'react';
import { PreloadedQuery, useFragment, usePreloadedQuery } from 'react-relay';
import { useSearchParams } from 'react-router-dom';
import { UserContext } from '../UserContext';
import config from '../common/config';
import HomeActivityFeed from './HomeActivityFeed';
import HomeDrawer from './HomeDrawer';
import HomeReleaseList from './HomeReleaseList';
import HomeToDoList from './HomeToDoList';
import { HomeFragment_activity$key } from './__generated__/HomeFragment_activity.graphql';
import { HomeQuery } from './__generated__/HomeQuery.graphql';

const query = graphql`
    query HomeQuery {
      ...HomeFragment_activity
    }
`;

interface Props {
    queryRef: PreloadedQuery<HomeQuery>;
}

function Home({ queryRef }: Props) {
    const theme = useTheme();
    const [searchParams, setSearchParams] = useSearchParams();
    const user = useContext(UserContext);
    const queryData = usePreloadedQuery<HomeQuery>(query, queryRef);

    // This filter will only show the current users activity when true
    const showMyActivity = searchParams.get('filter') === 'myActivity';

    const data = useFragment<HomeFragment_activity$key>(
        graphql`
      fragment HomeFragment_activity on Query {
        activityEvents(
            first: 0
        ) {
            totalCount
        }
      }
    `, queryData);

    return (
        <Box display="flex">
            <HomeDrawer />
            <Box component="main" sx={{ flexGrow: 1 }}>
                <Box sx={{
                    maxWidth: 1400,
                    margin: 'auto',
                    display: 'flex',
                    [theme.breakpoints.down('lg')]: {
                        flexDirection: 'column-reverse',
                        alignItems: 'flex-start',
                        '& > *': { mb: 2 }
                    }
                }}>
                    {data.activityEvents.totalCount === 0 && <Box flex={1} mt={4} display="flex" justifyContent="center">
                        <Box padding={4} display="flex" flexDirection="column" justifyContent="center" alignItems="center" sx={{ maxWidth: 600 }}>
                            <Typography variant="h6">Welcome to Phobos!</Typography>
                            <Typography color="textSecondary" align="center" sx={{ mb: 2 }}>
                                Phobos is a tool for deployment orchestration and release management
                            </Typography>
                        </Box>
                    </Box>}
                    {data.activityEvents.totalCount > 0 && <React.Fragment>
                        <Box padding={`${theme.spacing(2)} ${theme.spacing(3)}`} flex={1}>
                            <Box display="flex" justifyContent="space-between" alignItems="center">
                                <Typography variant="h6" fontWeight={600} mb={1}>Activity</Typography>
                                <ToggleButton
                                    onChange={() => setSearchParams(!showMyActivity ? { filter: 'myActivity' } : {}, { replace: true })}
                                    color="secondary"
                                    selected={showMyActivity}
                                    size="small"
                                    value="myActivity">
                                    My Activity
                                </ToggleButton>
                            </Box>
                            <Suspense fallback={<Box
                                sx={{
                                    width: '100%',
                                    height: `calc(100vh - 64px)`,
                                    display: 'flex',
                                    alignItems: 'center',
                                    justifyContent: 'center'
                                }}>
                                <CircularProgress />
                            </Box>}>
                                <HomeActivityFeed userId={showMyActivity ? user.id : undefined} />
                            </Suspense>
                        </Box>
                        <Box sx={{
                            padding: 2,
                            width: '100%',
                            [theme.breakpoints.up('lg')]: {
                                width: 400,
                                maxWidth: 400,
                            }
                        }}>
                            <Paper sx={{ mb: 3 }}>
                                <ListItemButton component={Link} target='_blank' rel='noopener noreferrer' href={config.docsUrl}>
                                    <RocketLaunchIcon sx={{ mr: 2 }} />
                                    <Box>
                                        <Typography variant="subtitle1" fontWeight={600}>Getting Started</Typography>
                                        <Typography variant="body2">Learn how to use Phobos</Typography>
                                    </Box>
                                </ListItemButton>
                            </Paper>
                            {config.supportUrl !== '' && <Paper sx={{ mb: 3 }}>
                                <ListItemButton component={Link} target='_blank' rel='noopener noreferrer' href={config.supportUrl}>
                                    <HelpIcon sx={{ mr: 2 }} />
                                    <Box>
                                        <Typography variant="subtitle1" fontWeight={600}>Need assistance?</Typography>
                                        <Typography variant="body2">Contact our support team or report an issue</Typography>
                                    </Box>
                                </ListItemButton>
                            </Paper>}
                            <Paper sx={{ padding: 2, mb: 3 }}>
                                <HomeToDoList
                                    noResultsText="You do not have any To-Do items at this time. Any item that requires your attention will automatically show up here."
                                />
                            </Paper>
                            <Paper sx={{ padding: 2 }}>
                                <HomeReleaseList />
                            </Paper>
                        </Box>
                    </React.Fragment>}
                </Box>
            </Box>
        </Box>
    );
}

export default Home;
