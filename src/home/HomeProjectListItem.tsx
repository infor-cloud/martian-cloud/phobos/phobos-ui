import { Avatar, Link, ListItemButton, ListItemText } from "@mui/material";
import purple from '@mui/material/colors/purple';
import graphql from 'babel-plugin-relay/macro';
import { useFragment } from "react-relay";
import { useNavigate } from 'react-router-dom';
import { HomeProjectListItemFragment_project$key } from "./__generated__/HomeProjectListItemFragment_project.graphql";

interface Props {
    fragmentRef: HomeProjectListItemFragment_project$key
    last?: boolean
}

function HomeProjectListItem({ fragmentRef, last }: Props) {
    const navigate = useNavigate();

    const data = useFragment(graphql`
        fragment HomeProjectListItemFragment_project on Project {
            name
            description
            organizationName
        }
    `, fragmentRef);

    return (
        <ListItemButton
            dense
            onClick={() => navigate(`/organizations/${data.organizationName}/projects/${data.name}`)}
            divider={!last}
        >
            <Avatar sx={{ width: 24, height: 24, mr: 2, bgcolor: purple[300] }} variant="rounded">{data.name[0].toUpperCase()}</Avatar>
            <ListItemText
                primary={<Link
                    underline="hover"
                    fontWeight={500}
                    variant="body2"
                    color="textPrimary"
                    sx={{
                        wordWrap: 'break-word'
                    }}
                >{data.organizationName}/{data.name}</Link>} />
        </ListItemButton>
    );
}

export default HomeProjectListItem
