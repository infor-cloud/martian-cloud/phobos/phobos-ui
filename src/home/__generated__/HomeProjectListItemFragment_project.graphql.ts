/**
 * @generated SignedSource<<d292fe76827a55ec544918af42fe9305>>
 * @lightSyntaxTransform
 * @nogrep
 */

/* tslint:disable */
/* eslint-disable */
// @ts-nocheck

import { Fragment, ReaderFragment } from 'relay-runtime';
import { FragmentRefs } from "relay-runtime";
export type HomeProjectListItemFragment_project$data = {
  readonly description: string;
  readonly name: string;
  readonly organizationName: string;
  readonly " $fragmentType": "HomeProjectListItemFragment_project";
};
export type HomeProjectListItemFragment_project$key = {
  readonly " $data"?: HomeProjectListItemFragment_project$data;
  readonly " $fragmentSpreads": FragmentRefs<"HomeProjectListItemFragment_project">;
};

const node: ReaderFragment = {
  "argumentDefinitions": [],
  "kind": "Fragment",
  "metadata": null,
  "name": "HomeProjectListItemFragment_project",
  "selections": [
    {
      "alias": null,
      "args": null,
      "kind": "ScalarField",
      "name": "name",
      "storageKey": null
    },
    {
      "alias": null,
      "args": null,
      "kind": "ScalarField",
      "name": "description",
      "storageKey": null
    },
    {
      "alias": null,
      "args": null,
      "kind": "ScalarField",
      "name": "organizationName",
      "storageKey": null
    }
  ],
  "type": "Project",
  "abstractKey": null
};

(node as any).hash = "7f4e4692d8ae81ae949aec7722e30d83";

export default node;
