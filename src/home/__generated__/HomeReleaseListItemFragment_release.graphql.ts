/**
 * @generated SignedSource<<420f357002cab92e30626f0783f70310>>
 * @lightSyntaxTransform
 * @nogrep
 */

/* tslint:disable */
/* eslint-disable */
// @ts-nocheck

import { Fragment, ReaderFragment } from 'relay-runtime';
export type PipelineNodeStatus = "APPROVAL_PENDING" | "CANCELED" | "CANCELING" | "CREATED" | "DEFERRED" | "FAILED" | "PENDING" | "READY" | "RUNNING" | "SKIPPED" | "SUCCEEDED" | "WAITING" | "%future added value";
import { FragmentRefs } from "relay-runtime";
export type HomeReleaseListItemFragment_release$data = {
  readonly id: string;
  readonly pipeline: {
    readonly status: PipelineNodeStatus;
  };
  readonly project: {
    readonly name: string;
    readonly organizationName: string;
  };
  readonly semanticVersion: string;
  readonly " $fragmentType": "HomeReleaseListItemFragment_release";
};
export type HomeReleaseListItemFragment_release$key = {
  readonly " $data"?: HomeReleaseListItemFragment_release$data;
  readonly " $fragmentSpreads": FragmentRefs<"HomeReleaseListItemFragment_release">;
};

const node: ReaderFragment = {
  "argumentDefinitions": [],
  "kind": "Fragment",
  "metadata": null,
  "name": "HomeReleaseListItemFragment_release",
  "selections": [
    {
      "alias": null,
      "args": null,
      "kind": "ScalarField",
      "name": "id",
      "storageKey": null
    },
    {
      "alias": null,
      "args": null,
      "kind": "ScalarField",
      "name": "semanticVersion",
      "storageKey": null
    },
    {
      "alias": null,
      "args": null,
      "concreteType": "Project",
      "kind": "LinkedField",
      "name": "project",
      "plural": false,
      "selections": [
        {
          "alias": null,
          "args": null,
          "kind": "ScalarField",
          "name": "name",
          "storageKey": null
        },
        {
          "alias": null,
          "args": null,
          "kind": "ScalarField",
          "name": "organizationName",
          "storageKey": null
        }
      ],
      "storageKey": null
    },
    {
      "alias": null,
      "args": null,
      "concreteType": "Pipeline",
      "kind": "LinkedField",
      "name": "pipeline",
      "plural": false,
      "selections": [
        {
          "alias": null,
          "args": null,
          "kind": "ScalarField",
          "name": "status",
          "storageKey": null
        }
      ],
      "storageKey": null
    }
  ],
  "type": "Release",
  "abstractKey": null
};

(node as any).hash = "b82b435a5a2530d5dce433767000d6a0";

export default node;
