import React from 'react';
import { Link as RouterLink, LinkProps as RouterLinkProps } from 'react-router-dom';
import { Link as MaterialLink, LinkProps as MaterialLinkProps } from '@mui/material';

export type LinkProps = RouterLinkProps & MaterialLinkProps;

const Link = React.forwardRef<HTMLAnchorElement, LinkProps>((props, ref) => {
    const { underline, ...extraProps } = props;
    return (
        <MaterialLink
            ref={ref}
            {...extraProps}
            underline={underline ?? 'hover'}
            component={RouterLink}
        >
            {props.children}
        </MaterialLink>
    );
});

export default Link;
