import { Box, Typography } from '@mui/material';
import { useRouteError } from 'react-router-dom';

function RouteError() {
    const error = useRouteError() as any;

    return (
        <Box padding={4} display="flex" flexDirection="column" justifyContent="center" alignItems="center" height="400px">
            <Typography variant="h3" color="textSecondary">404</Typography>
            <Typography variant="h5" color="textSecondary">The page you're looking for does not exist or you're not authorized to view it</Typography>
            <Typography variant="h6" color="textSecondary">Error: {error.statusText || error.message}</Typography>
        </Box>
    );
}

export default RouteError;
