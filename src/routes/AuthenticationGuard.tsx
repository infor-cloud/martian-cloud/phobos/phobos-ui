import React from 'react';
import { hasAuthParams, useAuth } from "react-oidc-context";
import { Navigate, useLocation } from 'react-router-dom';

const LOGIN_RETURN_TO = 'phobos.oidc.login.return_to';
const SIGNIN_REDIRECT_NAVIGATOR = 'signinRedirect';

interface Props {
    element: React.ReactElement
}

function AuthenticationGuard(props: Props) {
    const auth = useAuth();
    const location = useLocation();

    React.useEffect(() => {
        if (auth.error && auth.error.message === 'No matching state found in storage') {
            // This error can occur if an invalid code and state query param is passed in the URL
            // Clear the query params and redirect to the login page
            location.search = '';
            auth.signinRedirect();
        }
    }, [auth.error]);

    React.useEffect(() => {
        // Automatically sign user in
        if (!hasAuthParams() && !auth.isAuthenticated && !auth.activeNavigator && !auth.isLoading && !auth.error) {
            auth.signinRedirect();
        }

        if (auth.error) {
            window.history.replaceState(
                {},
                document.title,
                window.location.pathname
            );
        }
    }, [auth.isAuthenticated, auth.activeNavigator, auth.isLoading, auth.error, auth.signinRedirect]);

    // Set return to path if redirect has been requested
    if (auth.activeNavigator === SIGNIN_REDIRECT_NAVIGATOR) {
        window.sessionStorage.setItem(LOGIN_RETURN_TO, location.pathname + location.search);
        return null;
    }

    const returnToPath = window.sessionStorage.getItem(LOGIN_RETURN_TO);

    // Remove state params and set return path if we have been redirected back to the app form the IDP
    if (hasAuthParams() && auth.isAuthenticated) {
        if (returnToPath) {
            return <Navigate to={returnToPath} replace state={{}} />;
        } else {
            window.history.replaceState(
                {},
                document.title,
                window.location.pathname
            );
        }
    }

    // Return null if user is not currently authenticated
    if (auth.isLoading || !auth.isAuthenticated) {
        return null;
    }

    if (returnToPath) {
        if (returnToPath === (location.pathname + location.search)) {
            window.sessionStorage.removeItem(LOGIN_RETURN_TO);
        } else {
            return null;
        }
    }

    return props.element;
}

export default AuthenticationGuard;
