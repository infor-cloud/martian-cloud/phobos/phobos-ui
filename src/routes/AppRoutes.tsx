import { Navigate, createBrowserRouter } from "react-router-dom";
import Root from "../Root";
import AdminArea from '../administrator/AdminArea';
import AdminAreaAgentDetails from "../administrator/agents/AdminAreaAgentDetails";
import AdminAreaAgents from "../administrator/agents/AdminAreaAgents";
import EditAdminAreaAgent from "../administrator/agents/EditAdminAreaAgent";
import NewAdminAreaAgent from "../administrator/agents/NewAdminAreaAgent";
import AdminAreaNewOrganization from "../administrator/organizations/AdminAreaNewOrganization";
import AdminAreaOrganizationList from "../administrator/organizations/AdminAreaOrganizationList";
import AdminAreaServiceAccountDetails from "../administrator/serviceaccounts/AdminAreaServiceAccountDetails";
import GraphiQLEditor from "../graphiql/GraphiQLEditor";
import HomeEntryPoint from "../home/HomeEntryPoint";
import OrganizationActivity from "../organizations/OrganizationActivity";
import OrganizationDetails from "../organizations/OrganizationDetails";
import OrganizationDetailsIndex from "../organizations/OrganizationDetailsIndex";
import EditOrganizationAgent from "../organizations/agents/EditOrganizationAgent";
import NewOrganizationAgent from "../organizations/agents/NewOrganizationAgent";
import OrganizationAgentDetails from "../organizations/agents/OrganizationAgentDetails";
import OrganizationAgents from "../organizations/agents/OrganizationAgents";
import NewOrganizationMembership from "../organizations/members/NewOrganizationMembership";
import OrganizationMemberships from "../organizations/members/OrganizationMemberships";
import NewProject from "../organizations/projects/NewProject";
import ProjectDetails from "../organizations/projects/ProjectDetails";
import ProjectDetailsIndex from "../organizations/projects/ProjectDetailsIndex";
import EditEnvironment from "../organizations/projects/environments/EditEnvironment";
import EnvironmentDetails from "../organizations/projects/environments/EnvironmentDetails";
import Environments from "../organizations/projects/environments/Environments";
import NewEnvironment from "../organizations/projects/environments/NewEnvironment";
import ProjectInsights from "../organizations/projects/insights/ProjectInsights";
import NewProjectMembership from "../organizations/projects/members/NewProjectMembership";
import ProjectMemberships from "../organizations/projects/members/ProjectMemberships";
import PipelineDetails from "../organizations/projects/pipelines/PipelineDetails";
import PipelineDetailsIndex from "../organizations/projects/pipelines/PipelineDetailsIndex";
import PipelineNestedPipelineDetails from "../organizations/projects/pipelines/PipelineNestedPipelineDetails";
import PipelineTaskDetails from "../organizations/projects/pipelines/PipelineTaskDetails";
import Pipelines from "../organizations/projects/pipelines/Pipelines";
import NewPipelineTemplate from "../organizations/projects/pipelinetemplates/NewPipelineTemplate";
import NewPipelineTemplateVersion from "../organizations/projects/pipelinetemplates/NewPipelineTemplateVersion";
import PipelineTemplateDetails from "../organizations/projects/pipelinetemplates/PipelineTemplateDetails";
import PipelineTemplates from "../organizations/projects/pipelinetemplates/PipelineTemplates";
import ReleaseDetailsContainer from "../organizations/projects/releases/ReleaseDetailsContainer";
import Releases from "../organizations/projects/releases/Releases";
import EditOrgApprovalRule from "../organizations/approvalrules/EditOrgApprovalRule";
import NewOrgApprovalRule from "../organizations/approvalrules/NewOrgApprovalRule";
import OrgApprovalRules from "../organizations/approvalrules/OrgApprovalRules";
import EditProjectApprovalRule from "../organizations/projects/approvalrules/EditProjectApprovalRule";
import NewProjectApprovalRule from "../organizations/projects/approvalrules/NewProjectApprovalRule";
import ProjectApprovalRules from "../organizations/projects/approvalrules/ProjectApprovalRules";
import EditProjectReleaseLifecycle from "../organizations/projects/releaselifecycles/EditProjectReleaseLifecycle";
import NewProjectReleaseLifecycle from "../organizations/projects/releaselifecycles/NewProjectReleaseLifecycle";
import ProjectReleaseLifecycleDetails from "../organizations/projects/releaselifecycles/ProjectReleaseLifecycleDetails";
import ProjectReleaseLifecycles from "../organizations/projects/releaselifecycles/ProjectReleaseLifecycles";
import NewOrgReleaseLifecycle from "../organizations/releaselifecycles/NewOrgReleaseLifecycle";
import OrgReleaseLifecycleDetails from "../organizations/releaselifecycles/OrgReleaseLifecycleDetails";
import OrgReleaseLifecycles from "../organizations/releaselifecycles/OrgReleaseLifecycles";
import NewOrgServiceAccount from "../organizations/serviceaccounts/NewOrgServiceAccount";
import OrgServiceAccountDetails from "../organizations/serviceaccounts/OrgServiceAccountDetails";
import OrgServiceAccounts from "../organizations/serviceaccounts/OrgServiceAccounts";
import EditOrgServiceAccount from "../organizations/serviceaccounts/EditOrgServiceAccount";
import EditOrgReleaseLifecycle from "../organizations/releaselifecycles/EditOrgReleaseLifecycle";
import EditProjectServiceAccount from "../organizations/projects/serviceaccounts/EditProjectServiceAccount";
import NewProjectServiceAccount from "../organizations/projects/serviceaccounts/NewProjectServiceAccount";
import ProjectServiceAccountDetails from "../organizations/projects/serviceaccounts/ProjectServiceAccountDetails";
import ProjectServiceAccounts from "../organizations/projects/serviceaccounts/ProjectServiceAccounts";
import EditRelease from "../organizations/projects/releases/create/EditRelease";
import EditReleaseDeployment from "../organizations/projects/releases/create/EditReleaseDeployment";
import NewRelease from "../organizations/projects/releases/create/NewRelease";
import ReleaseTemplates from "../organizations/projects/releasetemplates/ReleaseTemplates";
import ProjectSettings from "../organizations/projects/settings/ProjectSettings";
import EditProjectEnvironmentRule from "../organizations/projects/settings/environmentrules/EditProjectEnvironmentRule";
import NewProjectEnvironmentRule from "../organizations/projects/settings/environmentrules/NewProjectEnvironmentRule";
import EditProjectVCSProvider from "../organizations/projects/vcsproviders/EditProjectVCSProvider";
import EditProjectVCSProviderAuthSettings from "../organizations/projects/vcsproviders/EditProjectVCSProviderAuthSettings";
import NewProjectVCSProvider from "../organizations/projects/vcsproviders/NewProjectVCSProvider";
import ProjectVCSProviderDetails from "../organizations/projects/vcsproviders/ProjectVCSProviderDetails";
import ProjectVCSProviderList from "../organizations/projects/vcsproviders/ProjectVCSProviders";
import OrganizationSettings from "../organizations/settings/OrganizationSettings";
import EditOrgEnvironmentRule from "../organizations/settings/environmentrules/EditOrgEnvironmentRule";
import NewOrgEnvironmentRule from "../organizations/settings/environmentrules/NewOrgEnvironmentRule";
import EditOrgVCSProvider from "../organizations/vcsproviders/EditOrgVCSProvider";
import EditOrgVCSProviderAuthSettings from "../organizations/vcsproviders/EditOrgVCSProviderAuthSettings";
import NewOrgVCSProvider from "../organizations/vcsproviders/NewOrgVCSProvider";
import OrgVCSProviderDetails from "../organizations/vcsproviders/OrgVCSProviderDetails";
import OrganizationVCSProviderList from "../organizations/vcsproviders/OrganizationVCSProviders";
import PluginSearchEntryPoint from "../plugins/PluginSearch";
import PluginVersionDetailsEntryPoint from "../plugins/PluginVersionDetails";
import TeamDetails from "../teams/TeamDetails";
import ToDoPage from "../todo/ToDoPage";
import AuthenticationGuard from "./AuthenticationGuard";
import RouteError from "./RouteError";
import OrganizationInsights from "../organizations/insights/OrganizationInsights";
import AdminInsights from "../administrator/insights/AdminInsights";
import CreatePipeline from "../organizations/projects/pipelines/create/CreatePipeline";
import ProjectVariables from "../organizations/projects/variables/ProjectVariables";
import NewProjectVariables from "../organizations/projects/variables/NewProjectVariables";

export function buildRoutes(getAccessToken: () => Promise<string>) {
    const router = createBrowserRouter([
        {
            path: "/",
            element: <AuthenticationGuard element={<Root />} />,
            errorElement: <RouteError />,
            children: [
                {
                    index: true,
                    element: <HomeEntryPoint />
                },
                {
                    path: "graphiql",
                    element: <GraphiQLEditor getAccessToken={getAccessToken} />
                },
                {
                    path: "admin",
                    element: <AdminArea />,
                    children: [
                        {
                            index: true,
                            element: <Navigate to="organizations" />
                        },
                        {
                            path: "organizations",
                            element: <AdminAreaOrganizationList />
                        },
                        {
                            path: "organizations/new",
                            element: <AdminAreaNewOrganization />
                        },
                        {
                            path: "agents",
                            children: [
                                {
                                    index: true,
                                    element: <AdminAreaAgents />
                                },
                                {
                                    path: "new",
                                    element: <NewAdminAreaAgent />
                                },
                                {
                                    path: ":agentId",
                                    children: [
                                        {
                                            index: true,
                                            element: <AdminAreaAgentDetails />
                                        },
                                        {
                                            path: "edit",
                                            element: <EditAdminAreaAgent />
                                        }
                                    ]
                                }
                            ]
                        },
                        {
                            path: "service_accounts/:serviceAccountId",
                            element: <AdminAreaServiceAccountDetails />,
                        },
                        {
                            path: "insights",
                            children: [
                                {
                                    index: true,
                                    element: <AdminInsights />
                                }
                            ]
                        },
                    ]
                },
                {
                    path: "plugin-registry",
                    element: <PluginSearchEntryPoint />,
                },
                {
                    path: "plugin-registry/:organizationName/:pluginName/:version",
                    element: <PluginVersionDetailsEntryPoint />,
                },
                {
                    path: "todos",
                    element: <ToDoPage />
                },
                {
                    path: "organizations/:orgName",
                    element: <OrganizationDetails />,
                    children: [
                        {
                            index: true,
                            element: <Navigate to="-/projects" replace />,
                        },
                        {
                            path: "-/projects",
                            children: [
                                {
                                    index: true,
                                    element: <OrganizationDetailsIndex />
                                },
                                {
                                    path: "new",
                                    element: <NewProject />
                                }
                            ]
                        },
                        {
                            path: "-/activity",
                            element: <OrganizationActivity />
                        },
                        {
                            path: "-/service_accounts",
                            children: [
                                {
                                    index: true,
                                    element: <OrgServiceAccounts />
                                },
                                {
                                    path: "new",
                                    element: <NewOrgServiceAccount />

                                },
                                {
                                    path: ":serviceAccountId",
                                    children: [
                                        {
                                            index: true,
                                            element: <OrgServiceAccountDetails />
                                        },
                                        {
                                            path: "edit",
                                            element: <EditOrgServiceAccount />
                                        }
                                    ]
                                },
                            ]
                        },
                        {
                            path: "-/release_lifecycles",
                            children: [
                                {
                                    index: true,
                                    element: <OrgReleaseLifecycles />
                                },
                                {
                                    path: "new",
                                    element: <NewOrgReleaseLifecycle />
                                },
                                {
                                    path: ":releaseLifecycleId",
                                    children: [
                                        {
                                            index: true,
                                            element: <OrgReleaseLifecycleDetails />
                                        },
                                        {
                                            path: "edit",
                                            element: <EditOrgReleaseLifecycle />
                                        }
                                    ]
                                }
                            ]
                        },
                        {
                            path: "-/members",
                            children: [
                                {
                                    index: true,
                                    element: <OrganizationMemberships />
                                },
                                {
                                    path: "new",
                                    element: <NewOrganizationMembership />
                                }
                            ]
                        },
                        {
                            path: "-/approval_rules",
                            children: [
                                {
                                    index: true,
                                    element: <OrgApprovalRules />
                                },
                                {
                                    path: "new",
                                    element: <NewOrgApprovalRule />
                                },
                                {
                                    path: ":approvalRuleId/edit",
                                    element: <EditOrgApprovalRule />
                                }
                            ]
                        },
                        {
                            path: "-/agents",
                            children: [
                                {
                                    index: true,
                                    element: <OrganizationAgents />
                                },
                                {
                                    path: "new",
                                    element: <NewOrganizationAgent />
                                },
                                {
                                    path: ":agentId",
                                    children: [
                                        {
                                            index: true,
                                            element: <OrganizationAgentDetails />
                                        },
                                        {
                                            path: "edit",
                                            element: <EditOrganizationAgent />
                                        }
                                    ]
                                }
                            ]
                        },
                        {
                            path: "-/insights",
                            children: [
                                {
                                    index: true,
                                    element: <OrganizationInsights />
                                }
                            ]
                        },
                        {
                            path: "-/vcs_providers",
                            children: [
                                {
                                    index: true,
                                    element: <OrganizationVCSProviderList />
                                },
                                {
                                    path: "new",
                                    element: <NewOrgVCSProvider />
                                },
                                {
                                    path: ":vcsProviderId",
                                    children: [
                                        {
                                            index: true,
                                            element: <OrgVCSProviderDetails />
                                        },
                                        {
                                            path: "edit",
                                            element: <EditOrgVCSProvider />
                                        },
                                        {
                                            path: "edit_auth_settings",
                                            element: <EditOrgVCSProviderAuthSettings />
                                        }
                                    ]
                                }
                            ]
                        },
                        {
                            path: "-/settings",
                            children: [
                                {
                                    index: true,
                                    element: <OrganizationSettings />
                                },
                                {
                                    path: "new_environment_rule",
                                    element: <NewOrgEnvironmentRule />
                                },
                                {
                                    path: "edit_environment_rule/:environmentRuleId",
                                    element: <EditOrgEnvironmentRule />
                                }
                            ]
                        },
                    ]
                },
                {
                    path: "organizations/:orgName/projects/:projectName",
                    element: <ProjectDetails />,
                    children: [
                        {
                            index: true,
                            element: <ProjectDetailsIndex />
                        },
                        {
                            path: "-/releases",
                            children: [
                                {
                                    index: true,
                                    element: <Releases />
                                },
                                {
                                    path: "new",
                                    element: <NewRelease />
                                },
                                {
                                    path: ":releaseId",
                                    children: [
                                        {
                                            index: true,
                                            element: <ReleaseDetailsContainer />
                                        },
                                        {
                                            path: "edit",
                                            element: <EditRelease />
                                        },
                                        {
                                            path: "edit_deployment/:environmentName",
                                            element: <EditReleaseDeployment />
                                        }
                                    ]
                                }
                            ]
                        },
                        {
                            path: "-/release_templates",
                            element: <ReleaseTemplates />
                        },
                        {
                            path: "-/pipelines",
                            children: [
                                {
                                    index: true,
                                    element: <Pipelines />
                                },
                                {
                                    path: "create",
                                    element: <CreatePipeline />
                                },
                                {
                                    path: ":pipelineId",
                                    element: <PipelineDetails />,
                                    children: [
                                        {
                                            index: true,
                                            element: <PipelineDetailsIndex />
                                        },
                                        {
                                            path: "task/:nodePath",
                                            element: <PipelineTaskDetails />
                                        },
                                        {
                                            path: "nested_pipeline/:nodePath",
                                            element: <PipelineNestedPipelineDetails />
                                        }
                                    ]
                                }
                            ]
                        },
                        {
                            path: "-/pipeline_templates",
                            children: [
                                {
                                    index: true,
                                    element: <PipelineTemplates />
                                },
                                {
                                    path: "new",
                                    element: <NewPipelineTemplate />
                                },
                                {
                                    path: ":pipelineTemplateId",
                                    element: <PipelineTemplateDetails />
                                },
                                {
                                    path: ":pipelineTemplateId/new_version",
                                    element: <NewPipelineTemplateVersion />
                                }
                            ]
                        },
                        {
                            path: "-/release_lifecycles",
                            children: [
                                {
                                    index: true,
                                    element: <ProjectReleaseLifecycles />
                                },
                                {
                                    path: "new",
                                    element: <NewProjectReleaseLifecycle />
                                },
                                {
                                    path: ":releaseLifecycleId",
                                    children: [
                                        {
                                            index: true,
                                            element: <ProjectReleaseLifecycleDetails />
                                        },
                                        {
                                            path: "edit",
                                            element: <EditProjectReleaseLifecycle />
                                        }
                                    ]
                                }
                            ]
                        },
                        {
                            path: "-/service_accounts",
                            children: [
                                {
                                    index: true,
                                    element: <ProjectServiceAccounts />
                                },
                                {
                                    path: "new",
                                    element: <NewProjectServiceAccount />

                                },
                                {
                                    path: ":serviceAccountId",
                                    children: [
                                        {
                                            index: true,
                                            element: <ProjectServiceAccountDetails />
                                        },
                                        {
                                            path: "edit",
                                            element: <EditProjectServiceAccount />
                                        }
                                    ]
                                },
                            ]
                        },
                        {
                            path: "-/approval_rules",
                            children: [
                                {
                                    index: true,
                                    element: <ProjectApprovalRules />
                                },
                                {
                                    path: "new",
                                    element: <NewProjectApprovalRule />
                                },
                                {
                                    path: ":approvalRuleId/edit",
                                    element: <EditProjectApprovalRule />
                                }
                            ]
                        },
                        {
                            path: "-/environments",
                            children: [
                                {
                                    index: true,
                                    element: <Environments />
                                },
                                {
                                    path: "new",
                                    element: <NewEnvironment />
                                },
                                {
                                    path: ":environmentId",
                                    children: [
                                        {
                                            index: true,
                                            element: <EnvironmentDetails />
                                        },
                                        {
                                            path: "edit",
                                            element: <EditEnvironment />
                                        }
                                    ]
                                }
                            ]
                        },
                        {
                            path: "-/vcs_providers",
                            children: [
                                {
                                    index: true,
                                    element: <ProjectVCSProviderList />
                                },
                                {
                                    path: "new",
                                    element: <NewProjectVCSProvider />
                                },
                                {
                                    path: ":vcsProviderId",
                                    children: [
                                        {
                                            index: true,
                                            element: <ProjectVCSProviderDetails />
                                        },
                                        {
                                            path: "edit",
                                            element: <EditProjectVCSProvider />
                                        },
                                        {
                                            path: "edit_auth_settings",
                                            element: <EditProjectVCSProviderAuthSettings />
                                        }
                                    ]
                                }
                            ],
                        },
                        {
                            path: "-/insights",
                            children: [
                                {
                                    index: true,
                                    element: <ProjectInsights />
                                }
                            ]
                        },
                        {
                            path: "-/members",
                            children: [
                                {
                                    index: true,
                                    element: <ProjectMemberships />
                                },
                                {
                                    path: "new",
                                    element: <NewProjectMembership />
                                }
                            ]
                        },
                        {
                            path: "-/variables",
                            children: [
                                {
                                    index: true,
                                    element: <ProjectVariables />
                                },
                                {
                                    path: "new",
                                    element: <NewProjectVariables />
                                }
                            ]
                        },
                        {
                            path: "-/settings",
                            children: [
                                {
                                    index: true,
                                    element: <ProjectSettings />
                                },
                                {
                                    path: "new_environment_rule",
                                    element: <NewProjectEnvironmentRule />
                                },
                                {
                                    path: "edit_environment_rule/:environmentRuleId",
                                    element: <EditProjectEnvironmentRule />
                                }
                            ]
                        }
                    ]
                },
                {
                    path: "teams/:teamName",
                    element: <TeamDetails />
                }
            ],
        },
    ]);

    return router;
}
