import { Avatar, Box, Chip, ListItemButton, ListItemText, Typography, useTheme } from '@mui/material';
import purple from '@mui/material/colors/purple';
import graphql from 'babel-plugin-relay/macro';
import moment from 'moment';
import { useFragment } from 'react-relay/hooks';
import { Link as RouterLink } from 'react-router-dom';
import { ServiceAccountListItemFragment_serviceAccount$key } from './__generated__/ServiceAccountListItemFragment_serviceAccount.graphql';

interface Props {
    showOrgChip?: boolean
    fragmentRef: ServiceAccountListItemFragment_serviceAccount$key
}

function ServiceAccountListItem({ showOrgChip, fragmentRef }: Props) {
    const theme = useTheme();

    const data = useFragment<ServiceAccountListItemFragment_serviceAccount$key>(graphql`
        fragment ServiceAccountListItemFragment_serviceAccount on ServiceAccount {
            id
            name
            scope
            description
            organization {
                name
            }
            project {
                name
            }
            metadata {
                updatedAt
            }
        }
    `, fragmentRef);

    const createRoute = (scope: string) => {
        switch (scope) {
            case 'ORGANIZATION':
                return `/organizations/${data.organization?.name}/-/service_accounts/${data.id}`;
            case 'PROJECT':
                return `/organizations/${data.organization?.name}/projects/${data.project?.name}/-/service_accounts/${data.id}`;
            case 'GLOBAL':
                return `/admin/service_accounts/${data.id}`;
            default:
                return '';
         }
    }

    return (
        <ListItemButton
            component={RouterLink}
            to={createRoute(data.scope)}
            sx={{
                paddingY: 1,
                borderBottom: `1px solid ${theme.palette.divider}`,
                borderLeft: `1px solid ${theme.palette.divider}`,
                borderRight: `1px solid ${theme.palette.divider}`,
                '&:last-child': {
                    borderBottomLeftRadius: 4,
                    borderBottomRightRadius: 4
                }
            }}>
            <Avatar variant="rounded" sx={{ width: 32, height: 32, bgcolor: purple[300], marginRight: 2 }}>
                {data.name[0].toUpperCase()}
            </Avatar>
            <ListItemText
                primary={
                    <Box sx={{ display: "flex", alignItems: "center" }}>
                        <Box>
                            <Typography fontWeight={500}>{data.name}</Typography>
                            {data.description && <Typography variant="body2" color="textSecondary">{data.description}</Typography>}
                        </Box>
                        <Box>{showOrgChip && <Chip
                            sx={{ ml: 1, color: theme.palette.text.secondary }}
                            size="xs"
                            label='inherited from org'
                        />}
                        </Box>
                    </Box>
                }
            />
            <Typography variant="body2" color="textSecondary">
                {moment(data.metadata.updatedAt as moment.MomentInput).fromNow()}
            </Typography>
        </ListItemButton>
    );
}

export default ServiceAccountListItem;
