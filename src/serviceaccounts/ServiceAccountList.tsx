import { Box, List, Paper, Typography, useTheme } from '@mui/material';
import graphql from 'babel-plugin-relay/macro';
import InfiniteScroll from 'react-infinite-scroll-component';
import { LoadMoreFn, useFragment } from 'react-relay/hooks';
import ListSkeleton from '../skeletons/ListSkeleton';
import ServiceAccountListItem from './ServiceAccountListItem';
import { OrgServiceAccountsPaginationQuery } from '../organizations/serviceaccounts/__generated__/OrgServiceAccountsPaginationQuery.graphql';
import { ProjectServiceAccountsPaginationQuery } from '../organizations/projects/serviceaccounts/__generated__/ProjectServiceAccountsPaginationQuery.graphql';
import { ServiceAccountListFragment_serviceAccounts$key } from './__generated__/ServiceAccountListFragment_serviceAccounts.graphql';

interface Props {
    search: string | undefined
    isRefreshing: boolean
    loadNext: LoadMoreFn<OrgServiceAccountsPaginationQuery | ProjectServiceAccountsPaginationQuery>
    hasNext: boolean
    showOrgChip?: boolean
    fragmentRef: ServiceAccountListFragment_serviceAccounts$key
}

function ServiceAccountList({ fragmentRef, isRefreshing, loadNext, hasNext, showOrgChip, search }: Props) {
    const theme = useTheme();

    const data = useFragment<ServiceAccountListFragment_serviceAccounts$key>(
        graphql`
            fragment ServiceAccountListFragment_serviceAccounts on ServiceAccountConnection
            {
                totalCount
                edges {
                    node {
                        id
                        scope
                        ...ServiceAccountListItemFragment_serviceAccount
                    }
                }
            }
    `, fragmentRef);

    return (
        <Box>
            <Paper sx={{ borderBottomLeftRadius: 0, borderBottomRightRadius: 0, border: `1px solid ${theme.palette.divider}` }}>
                <Box padding={2} display="flex" alignItems="center" justifyContent="space-between">
                    <Typography variant="subtitle1">
                        {data.totalCount} service account{data.totalCount === 1 ? '' : 's'}
                    </Typography>
                </Box>
            </Paper>
            {(data?.edges?.length === 0) && search !== '' && <Typography
                sx={{
                    padding: 4,
                    borderBottom: `1px solid ${theme.palette.divider}`,
                    borderLeft: `1px solid ${theme.palette.divider}`,
                    borderRight: `1px solid ${theme.palette.divider}`,
                    borderBottomLeftRadius: 4,
                    borderBottomRightRadius: 4
                }}
                align="center"
                color="textSecondary"
            >
                No service accounts matching search <strong>{search}</strong>
            </Typography>}
            <InfiniteScroll
                dataLength={data?.edges?.length ?? 0}
                next={() => loadNext(20)}
                hasMore={hasNext}
                loader={<ListSkeleton rowCount={3} />}
            >
                <List sx={isRefreshing ? { opacity: 0.5 } : null} disablePadding>
                    {data?.edges?.map((edge: any) => <ServiceAccountListItem
                        key={edge.node.id}
                        fragmentRef={edge.node}
                        showOrgChip={showOrgChip && edge.node.scope === 'ORGANIZATION'}
                    />)}
                </List>
            </InfiniteScroll>
        </Box>
    );
}

export default ServiceAccountList
