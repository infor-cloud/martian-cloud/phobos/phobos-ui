import { useState } from 'react';
import { Box, Button, Divider, Typography } from '@mui/material';
import LoadingButton from '@mui/lab/LoadingButton';
import graphql from 'babel-plugin-relay/macro';
import { nanoid } from 'nanoid';
import { useLazyLoadQuery, useMutation } from "react-relay/hooks";
import { Link as RouterLink, useNavigate, useParams } from 'react-router-dom';
import { MutationError } from '../common/error';
import ServiceAccountForm, { FormData } from './ServiceAccountForm';
import { BoundClaimsType, EditServiceAccountMutation } from './__generated__/EditServiceAccountMutation.graphql';
import { EditServiceAccountQuery } from './__generated__/EditServiceAccountQuery.graphql';

function EditServiceAccount() {
    const serviceAccountId = useParams().serviceAccountId as string;
    const navigate = useNavigate();

    const queryData = useLazyLoadQuery<EditServiceAccountQuery>(graphql`
        query EditServiceAccountQuery($id: String!) {
            node(id: $id) {
                ... on ServiceAccount {
                    metadata {
                        createdAt
                    }
                    id
                    name
                    description
                    createdBy
                    oidcTrustPolicies {
                        issuer
                        boundClaimsType
                        boundClaims {
                            name
                            value
                        }
                    }
                }
            }
        }
    `, { id: serviceAccountId });

    const [commit, isInFlight] = useMutation<EditServiceAccountMutation>(graphql`
        mutation EditServiceAccountMutation($input: UpdateServiceAccountInput!) {
            updateServiceAccount(input: $input) {
                serviceAccount {
                    id
                    name
                    description
                    createdBy
                    oidcTrustPolicies {
                        issuer
                        boundClaimsType
                        boundClaims {
                            name
                            value
                        }
                    }
                }
                problems {
                    message
                    field
                    type
                }
            }
        }
    `);

    const serviceAccount = queryData.node as any;

    const [error, setError] = useState<MutationError>();
    const [formData, setFormData] = useState<FormData>({
        name: serviceAccount.name,
        description: serviceAccount.description,
        oidcTrustPolicies: serviceAccount.oidcTrustPolicies.map((trustPolicy: { issuer: string; boundClaimsType: BoundClaimsType; boundClaims: [{ name: string, value: string }]; }) => ({ ...trustPolicy, _id: nanoid() }))
    });

    const onUpdate = () => {
        if (formData) {
            commit({
                variables: {
                    input: {
                        id: serviceAccount.id,
                        description: formData.description,
                        oidcTrustPolicies: formData.oidcTrustPolicies
                    }
                },
                onCompleted: data => {
                    if (data.updateServiceAccount.problems.length) {
                        setError({
                            severity: 'warning',
                            message: data.updateServiceAccount.problems.map((problem: any) => problem.message).join('; ')
                        });
                    } else if (!data.updateServiceAccount.serviceAccount) {
                        setError({
                            severity: 'error',
                            message: "Unexpected error occurred"
                        });
                    } else {
                        navigate(`../`);
                    }
                },
                onError: error => {
                    setError({
                        severity: 'error',
                        message: `Unexpected error occurred: ${error.message}`
                    });
                }
            });
        }
    };

    return (
        <Box>
            <Typography variant="h5">Edit Service Account</Typography>
            <ServiceAccountForm
                editMode
                data={formData}
                onChange={(data: FormData) => setFormData(data)}
                error={error}
            />
            <Divider sx={{ opacity: 0.6 }} />
            <Box marginTop={2}>
                <LoadingButton
                    loading={isInFlight}
                    variant="outlined"
                    color="primary"
                    sx={{ marginRight: 2 }}
                    onClick={onUpdate}>
                    Update Service Account
                </LoadingButton>
                <Button component={RouterLink} color="inherit" to={-1 as any}>Cancel</Button>
            </Box>
        </Box>
    );
}

export default EditServiceAccount
