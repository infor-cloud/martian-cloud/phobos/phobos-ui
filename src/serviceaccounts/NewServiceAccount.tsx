import LoadingButton from '@mui/lab/LoadingButton';
import { Box, Button, Divider, Typography } from '@mui/material';
import graphql from 'babel-plugin-relay/macro';
import { nanoid } from 'nanoid';
import { useState } from 'react';
import { useMutation } from "react-relay/hooks";
import { Link as RouterLink, useNavigate } from 'react-router-dom';
import { MutationError } from '../common/error';
import ServiceAccountForm, { FormData } from './ServiceAccountForm';
import { NewServiceAccountMutation } from './__generated__/NewServiceAccountMutation.graphql';

interface Props {
    orgId?: string
    projectId?: string
    getConnections: () => [string]
}

function NewServiceAccount({ orgId, projectId, getConnections }: Props) {
    const navigate = useNavigate();

    const [commit, isInFlight] = useMutation<NewServiceAccountMutation>(graphql`
        mutation NewServiceAccountMutation($input: CreateServiceAccountInput!, $connections: [ID!]!) {
            createServiceAccount(input: $input) {
                # Use @prependNode to add the node to the connection
                serviceAccount  @prependNode(connections: $connections, edgeTypeName: "ServiceAccountEdge")  {
                    id
                    name
                    description
                    createdBy
                    oidcTrustPolicies {
                        issuer
                        boundClaimsType
                        boundClaims {
                            name
                            value
                        }
                    }
                }
                problems {
                    message
                    field
                    type
                }
            }
        }
    `);

    const [error, setError] = useState<MutationError>()
    const [formData, setFormData] = useState<FormData>({
        name: '',
        description: '',
        oidcTrustPolicies: [{ _id: nanoid(), issuer: '', boundClaimsType: 'STRING', boundClaims: [] }],
    });

    const onSave = () => {
        commit({
            variables: {
                input: {
                    name: formData.name,
                    description: formData.description,
                    oidcTrustPolicies: formData.oidcTrustPolicies,
                    scope: orgId ? 'ORGANIZATION' : 'PROJECT',
                    organizationId: orgId || null,
                    projectId: projectId || null
                },
                connections: getConnections()
            },
            onCompleted: data => {
                if (data.createServiceAccount.problems.length) {
                    setError({
                        severity: 'warning',
                        message: data.createServiceAccount.problems.map((problem: any) => problem.message).join('; ')
                    });
                } else if (!data.createServiceAccount.serviceAccount) {
                    setError({
                        severity: 'error',
                        message: "Unexpected error occurred"
                    });
                } else {
                    navigate(`../${data.createServiceAccount.serviceAccount.id}`);
                }
            },
            onError: error => {
                setError({
                    severity: 'error',
                    message: `Unexpected error occurred: ${error.message}`
                });
            }
        });
    };

    return (
        <Box>
            <Typography variant="h5">New Service Account</Typography>
            <ServiceAccountForm
                data={formData}
                onChange={(data: FormData) => setFormData(data)}
                error={error}
            />
            <Divider sx={{ opacity: 0.6 }} />
            <Box marginTop={2}>
                <LoadingButton
                    loading={isInFlight}
                    variant="outlined"
                    color="primary"
                    sx={{ marginRight: 2 }}
                    onClick={onSave}
                >
                    Create Service Account
                </LoadingButton>
                <Button component={RouterLink} color="inherit" to={-1 as any}>Cancel</Button>
            </Box>
        </Box>
    );
}

export default NewServiceAccount;
