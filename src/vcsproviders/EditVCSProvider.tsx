import { useState } from 'react';
import { Box, Button, Typography } from '@mui/material';
import LoadingButton from '@mui/lab/LoadingButton';
import { Link as RouterLink, useNavigate } from 'react-router-dom';
import { useFragment, useMutation } from 'react-relay/hooks';
import VCSProviderForm, { FormData } from './VCSProviderForm';
import { useSnackbar } from 'notistack';
import graphql from 'babel-plugin-relay/macro';
import { MutationError } from '../common/error';
import { EditVCSProviderFragment_vcsProvider$key } from './__generated__/EditVCSProviderFragment_vcsProvider.graphql';
import { EditVCSProviderMutation } from './__generated__/EditVCSProviderMutation.graphql';

interface Props {
    fragmentRef: EditVCSProviderFragment_vcsProvider$key
}

function EditVCSProvider({ fragmentRef }: Props) {
    const navigate = useNavigate();
    const { enqueueSnackbar } = useSnackbar();
    const [error, setError] = useState<MutationError>();

    const vcsProvider = useFragment<EditVCSProviderFragment_vcsProvider$key>(graphql`
        fragment EditVCSProviderFragment_vcsProvider on VCSProvider
        {
            id
            name
            description
            url
            type
        }`,
        fragmentRef
    );

    const [commit, isInFlight] = useMutation<EditVCSProviderMutation>(graphql`
        mutation EditVCSProviderMutation($input: UpdateVCSProviderInput!) {
            updateVCSProvider(input: $input) {
                vcsProvider{
                    id
                    description
                    ...VCSProviderDetailsFragment_vcsProvider
                }
                problems {
                    message
                    field
                    type
                }
            }
        }
    `);

    const [formData, setFormData] = useState<FormData>({
        type: vcsProvider.type,
        name: vcsProvider.name,
        description: vcsProvider.description,
        url: vcsProvider.url,
    });

    const onUpdate = () => {
        commit({
            variables: {
                input: {
                    id: vcsProvider.id,
                    description: formData.description,
                }
            },
            onCompleted: data => {
                if (data.updateVCSProvider.problems.length) {
                    setError({
                        severity: 'warning',
                        message: data.updateVCSProvider.problems.map((problem: any) => problem.message).join('; ')
                    });
                } else if (!data.updateVCSProvider.vcsProvider) {
                    setError({
                        severity: 'error',
                        message: "Unexpected error occurred"
                    });
                } else {
                    navigate(`..`);
                    enqueueSnackbar('VCS Provider updated', { variant: 'success' });
                }
            },
            onError: error => {
                setError({
                    severity: 'error',
                    message: `Unexpected error occurred: ${error.message}`
                });
            }
        });
    };

    return (
        <Box>
            <Typography variant="h5">Edit VCS Provider</Typography>
            <VCSProviderForm
                editMode
                data={formData}
                onChange={(data: FormData) => setFormData(data)}
                error={error}
            />
            <Box marginTop={2}>
                <LoadingButton
                    loading={isInFlight}
                    variant="outlined"
                    color="primary"
                    sx={{ marginRight: 2 }}
                    onClick={onUpdate}>
                    Update VCS Provider
                </LoadingButton>
                <Button component={RouterLink} color="inherit" to={-1 as any}>Cancel</Button>
            </Box>
        </Box>
    );
}

export default EditVCSProvider;
