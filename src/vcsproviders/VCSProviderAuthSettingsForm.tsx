import { useMemo, useState } from 'react';
import { Alert, Box, Button, Chip, Checkbox, Divider, Paper, Stack, TextField, Typography } from '@mui/material';
import { MutationError } from '../common/error';
import graphql from 'babel-plugin-relay/macro';
import { useFragment } from 'react-relay/hooks';
import { IconButton, InputAdornment } from '@mui/material';
import Visibility from '@mui/icons-material/Visibility';
import VisibilityOff from '@mui/icons-material/VisibilityOff';
import { VCSProviderAuthSettingsForm_vcsProvider$key } from './__generated__/VCSProviderAuthSettingsForm_vcsProvider.graphql';
import { getRequiredOAuthScopesForProvider } from './VCSProviderForm';

export interface AuthFormData {
    oAuthClientId?: string
    oAuthClientSecret?: string
    extraOAuthScopes?: string[]
    personalAccessToken?: string
}

interface Props {
    fragmentRef: VCSProviderAuthSettingsForm_vcsProvider$key
    data: AuthFormData
    onChange: (data: AuthFormData) => void
    error?: MutationError
}

function VCSProviderAuthSettingsForm({ data, onChange, error, fragmentRef }: Props) {
    const [showPassword, setShowPassword] = useState(false);
    const [showOAuthCredentialFields, setShowOAuthCredentialFields] = useState(false);
    const [oAuthScopeToAdd, setOAuthScopeToAdd] = useState('');

    const onAddExtraOAuthScope = () => {
        onChange({ ...data, extraOAuthScopes: [...(data.extraOAuthScopes || []), oAuthScopeToAdd] });
        setOAuthScopeToAdd('');
    };

    const onRemoveOAuthScope = (oAuthScope: string) => {
        const index = (data.extraOAuthScopes || []).indexOf(oAuthScope);
        if (index !== -1) {
            const extraOAuthScopesCopy = [...(data.extraOAuthScopes || [])];
            extraOAuthScopesCopy.splice(index, 1);
            onChange({ ...data, extraOAuthScopes: extraOAuthScopesCopy });
        }
    };

    const handleClickShowPassword = () => {
        setShowPassword(!showPassword);
    };

    const handleShowOAuthCredentialsFieldsCheckboxChange = (event: React.ChangeEvent<HTMLInputElement>) => {
        onChange({
            ...data,
            oAuthClientId: event.target.checked ? '' : undefined,
            oAuthClientSecret: event.target.checked ? '' : undefined
        });
        setShowOAuthCredentialFields(event.target.checked);
    }

    const vcsProvider = useFragment<VCSProviderAuthSettingsForm_vcsProvider$key>(graphql`
        fragment VCSProviderAuthSettingsForm_vcsProvider on VCSProvider
        {
            type
            authType
        }
    `, fragmentRef);

    const requiredOAuthScopes = useMemo(() => getRequiredOAuthScopesForProvider(vcsProvider.type), [vcsProvider.type]);
    const addScopeButtonDisabled = useMemo(() => {
        return oAuthScopeToAdd === '' ||
            data.extraOAuthScopes?.includes(oAuthScopeToAdd) ||
            requiredOAuthScopes.includes(oAuthScopeToAdd);
    }, [oAuthScopeToAdd, data.extraOAuthScopes, requiredOAuthScopes]);

    return (
        <Box sx={{ mt: 2, mb: 2 }}>
            {error && <Alert sx={{ mt: 2, mb: 2 }} severity={error.severity}>
                {error.message}
            </Alert>}
            {vcsProvider.authType === 'OAUTH' && <Box>
                <Box sx={{ display: 'flex', alignItems: 'center', mb: 2 }}>
                    <Checkbox
                        checked={showOAuthCredentialFields}
                        onChange={handleShowOAuthCredentialsFieldsCheckboxChange}
                        color="secondary"
                    />
                    <Typography variant="subtitle1">
                        Change Application ID or Secret
                    </Typography>
                </Box>
                {showOAuthCredentialFields && <Box component="form">
                    <TextField
                        size="small"
                        fullWidth
                        autoComplete="off"
                        label={vcsProvider.type === "GITHUB" ? "ClientID - write only" : "Application ID - write only"}
                        value={data.oAuthClientId}
                        onChange={event => onChange({ ...data, oAuthClientId: event.target.value })}
                    />
                    <TextField
                        size="small"
                        margin="normal"
                        fullWidth
                        autoComplete="off"
                        type={showPassword ? 'text' : 'password'}
                        InputProps={{
                            endAdornment: (
                                <InputAdornment position="end">
                                    <IconButton onClick={handleClickShowPassword}>
                                        {showPassword ? <VisibilityOff /> : <Visibility />}
                                    </IconButton>
                                </InputAdornment>
                            )
                        }}
                        label={vcsProvider.type === "GITHUB" ? "Client Secret - write only" : "Secret - write only"}
                        value={data.oAuthClientSecret}
                        onChange={event => onChange({ ...data, oAuthClientSecret: event.target.value })}
                    />
                </Box>}
                <Box marginBottom={3}>
                    <Typography sx={{ mt: 2 }} variant="subtitle1" gutterBottom>Extra OAuth Scopes</Typography>
                    <Divider sx={{ mb: 2, opacity: 0.6 }} />
                    <Paper sx={{ padding: 2, mb: 2 }} variant="outlined">
                        <Typography gutterBottom color="textSecondary">
                            Add or remove OAuth scopes to control the permissions granted to Phobos. These scopes must match your OAuth application settings.
                        </Typography>
                        <Paper sx={{ padding: 2, display: 'flex', alignItems: 'center', mb: 2 }}>
                            <TextField
                                size="small"
                                margin="none"
                                sx={{ flex: 1, mr: 1 }}
                                fullWidth
                                value={oAuthScopeToAdd}
                                placeholder="Enter OAuth scope to add"
                                variant="standard"
                                color="secondary"
                                onChange={event => setOAuthScopeToAdd(event.target.value)}
                            />
                            <Button
                                onClick={onAddExtraOAuthScope}
                                disabled={addScopeButtonDisabled}
                                variant="outlined"
                                color="secondary">
                                Add Scope
                            </Button>
                        </Paper>
                        {data.extraOAuthScopes && data.extraOAuthScopes.length > 0 && (
                            <Stack direction="row" spacing={2} mb={2}>
                                {data.extraOAuthScopes?.map(scope => (
                                    <Chip
                                        key={scope}
                                        color="secondary"
                                        label={scope}
                                        onDelete={() => onRemoveOAuthScope(scope)}
                                    />
                                ))}
                            </Stack>
                        )}
                        {requiredOAuthScopes.length > 0 && (
                            <Typography color="textSecondary">
                                Phobos will always request the following scopes: <strong>{requiredOAuthScopes.join(', ')}</strong>.
                            </Typography>
                        )}
                    </Paper>
                </Box>
                <Box marginBottom={2}>
                    <Typography sx={{ mb: 2 }} variant="subtitle2" color="textSecondary">
                        After updating your OAuth settings, you may need to reset your OAuth token.
                    </Typography>
                </Box>
            </Box>}
            {vcsProvider.authType === 'ACCESS_TOKEN' &&
                <Box component="form">
                    <TextField
                        size="small"
                        fullWidth
                        type={showPassword ? 'text' : 'password'}
                        autoComplete="off"
                        label="Personal Access Token"
                        value={data.personalAccessToken}
                        InputProps={{
                            endAdornment: (
                                <InputAdornment position="end">
                                    <IconButton onClick={handleClickShowPassword}>
                                        {showPassword ? <VisibilityOff /> : <Visibility />}
                                    </IconButton>
                                </InputAdornment>
                            )
                        }}
                        onChange={event => onChange({ ...data, personalAccessToken: event.target.value })}
                    />
                </Box>}
            <Divider sx={{ opacity: 0.6 }} />
        </Box>
    );
}

export default VCSProviderAuthSettingsForm;
