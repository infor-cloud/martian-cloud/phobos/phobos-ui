import { Box, Chip, Divider, Stack, IconButton, Paper, TextField, Typography, InputAdornment, Button, InputLabel, Select, FormControl, MenuItem, List, ListItem } from '@mui/material';
import { useMemo, useState } from 'react';
import Visibility from '@mui/icons-material/Visibility';
import VisibilityOff from '@mui/icons-material/VisibilityOff';
import CopyButton from '../common/CopyButton';
import { FormData, getRequiredOAuthScopesForProvider } from './VCSProviderForm';
import PanelButton from '../common/PanelButton';
import { StyledCode } from '../common/StyledCode';
import cfg from '../common/config';

interface Props {
    data: FormData
    onChange: (data: FormData) => void
    editMode?: boolean
}

interface AuthTypesObj {
    name: 'OAUTH' | 'ACCESS_TOKEN'
    title: string
    description: string
}

const AuthTypes: AuthTypesObj[] = [
    { name: 'OAUTH', title: 'OAuth', description: 'Create an OAuth application and authorize via an ID and secret value' },
    { name: 'ACCESS_TOKEN', title: 'Personal Access Token', description: 'Authorize with a personal access token' }
];

function VCSProviderSetup({ data, onChange, editMode }: Props) {
    const [showPassword, setShowPassword] = useState(false);
    const [gitHubAccessTokenType, setGitHubAccessTokenType] = useState('fine-grained');
    const [oAuthScopeToAdd, setOAuthScopeToAdd] = useState('');

    const onAddOAuthScope = () => {
        onChange({ ...data, extraOAuthScopes: [...(data.extraOAuthScopes || []), oAuthScopeToAdd] });
        setOAuthScopeToAdd('');
    };

    const onRemoveOAuthScope = (oAuthScope: string) => {
        const index = (data.extraOAuthScopes ?? []).indexOf(oAuthScope);
        if (index !== -1) {
            const extraOAuthScopesCopy = [...(data.extraOAuthScopes || [])];
            extraOAuthScopesCopy.splice(index, 1);
            onChange({ ...data, extraOAuthScopes: extraOAuthScopesCopy });
        }
    };

    const onTypeChange = (authType: 'OAUTH' | 'ACCESS_TOKEN') => {
        if (!editMode && (data.authType !== authType)) {
            onChange({
                ...data,
                authType, oAuthClientId: '', oAuthClientSecret: '', extraOAuthScopes: [], personalAccessToken: ''
            });
        }
        setGitHubAccessTokenType('fine-grained');
        setShowPassword(false);
    };

    const personalAccessTokenScopes = useMemo(() => {
        switch (data.type) {
            case 'GITHUB':
                if (gitHubAccessTokenType === 'fine-grained') {
                    return ['Contents (read-only)', 'Repository metadata (read-only)'];
                }
                if (gitHubAccessTokenType === 'classic') {
                    return ['repo', 'read:user'];
                }
                return [];
            case 'GITLAB':
                return ['read_api', 'read_user'];
            default:
                return [];
        }
    }, [data.type, gitHubAccessTokenType]);

    const requiredOAuthScopes = useMemo(() => data.type ? getRequiredOAuthScopesForProvider(data.type) : [], [data.type]);
    const addScopeButtonDisabled = useMemo(() => {
        return oAuthScopeToAdd === '' || data.extraOAuthScopes?.includes(oAuthScopeToAdd) || requiredOAuthScopes.includes(oAuthScopeToAdd);
    }, [oAuthScopeToAdd, data.extraOAuthScopes, requiredOAuthScopes]);

    const handleClickShowPassword = () => {
        setShowPassword(!showPassword);
    };

    return (
        <Box>
            <Typography sx={{ mt: 2, mb: 2 }} variant="h5" gutterBottom>Provider Setup</Typography>
            <Box>
                <Typography sx={{ mt: 2 }} variant="subtitle1" gutterBottom>Authorization Type</Typography>
                <Typography sx={{ mb: 2 }} variant="subtitle2" color="textSecondary">Select a method to authenticate and authorize your VCS provider.</Typography>
                <Divider sx={{ opacity: 0.6 }} />
                <Stack sx={{ mt: 2, mb: 2 }} direction="row" spacing={2}>
                    {AuthTypes.map(type => <PanelButton
                        key={type.name}
                        selected={type.name === data.authType}
                        onClick={() => onTypeChange(type.name)}
                    >
                        <Typography variant="subtitle1" align="center">{type.title}</Typography>
                        <Typography variant="caption" align="center">{type.description}</Typography>
                    </PanelButton>)
                    }
                </Stack>
            </Box>
            {data.authType === 'OAUTH' && <Box sx={{ mt: 2 }}>
                <Typography>Start the process to register a new OAuth application in {data.type === 'GITHUB' ? 'GitHub' : 'GitLab'}. Copy the following, required for creating a new OAuth application:</Typography>
                <Stack sx={{ mt: 2, mb: 2, ml: 2 }}>
                    {data.type === 'GITHUB' &&
                        <Stack direction="row" spacing={1} alignItems="center">
                            <Typography variant="body1" component="span"><strong>Homepage URL:</strong> {window.location.protocol}//{window.location.host}</Typography>
                            <CopyButton
                                toolTip='Copy Homepage URL'
                                data={window.location.protocol + '//' + window.location.host}
                            />
                        </Stack>}
                    <Stack direction="row" spacing={1} alignItems="center">
                        <Typography variant="body1" component="span" sx={{ fontWeight: 'bold' }}>{data.type === 'GITHUB' ? 'Callback URL' : 'Redirect URI'}:
                            <Typography variant="body1" component="span">{` ${cfg.apiUrl}/v1/vcs/auth/callback`}</Typography>
                        </Typography>
                        <CopyButton
                            toolTip={data.type === 'GITHUB' ? 'Copy Callback URL' : 'Copy Redirect URI'}
                            data={`${cfg.apiUrl}/v1/vcs/auth/callback`}
                        />
                    </Stack>
                </Stack>
                {data.type === 'GITLAB' && <Box sx={{ mt: 2, mb: 2 }}>
                    <Typography sx={{ mb: 2 }}>Enable the <StyledCode>Confidential</StyledCode> setting.</Typography>
                </Box>}
                <Box marginBottom={3}>
                    <Typography sx={{ mt: 2 }} variant="subtitle1" gutterBottom>Extra OAuth Scopes</Typography>
                    <Divider sx={{ mb: 2, opacity: 0.6 }} />
                    <Paper sx={{ padding: 2, mb: 2 }} variant="outlined">
                        <Typography gutterBottom color="textSecondary">
                            Add additional OAuth scopes to control the permissions granted to Phobos. These scopes must match your OAuth application settings.
                        </Typography>
                        <Paper sx={{ padding: 2, display: 'flex', alignItems: 'center', mb: 2 }}>
                            <TextField
                                size="small"
                                margin="none"
                                sx={{ flex: 1, mr: 1 }}
                                fullWidth
                                value={oAuthScopeToAdd}
                                placeholder="Enter OAuth scope to add"
                                variant="standard"
                                color="secondary"
                                onChange={event => setOAuthScopeToAdd(event.target.value)}
                            />
                            <Button
                                onClick={onAddOAuthScope}
                                disabled={addScopeButtonDisabled}
                                variant="outlined"
                                color="secondary">
                                Add Scope
                            </Button>
                        </Paper>
                        {data.extraOAuthScopes && data.extraOAuthScopes.length > 0 && (
                            <Stack direction="row" spacing={2} mb={2}>
                                {data.extraOAuthScopes.map(oAuthScope => (
                                    <Chip
                                        key={oAuthScope}
                                        color="secondary"
                                        label={oAuthScope}
                                        onDelete={() => onRemoveOAuthScope(oAuthScope)}
                                    />
                                ))}
                            </Stack>
                        )}
                        {requiredOAuthScopes.length > 0 && (
                            <Typography color="textSecondary">
                                Phobos will always request the following scopes: <strong>{requiredOAuthScopes.join(', ')}</strong>.
                            </Typography>
                        )}
                    </Paper>
                </Box>
                <Box>
                    <Typography>After registering a new OAuth application, {data.type === 'GITHUB' ? 'a Client ID and Client Secret' : 'an Application ID and Secret value'} will be generated. Copy and paste the ID and Secret into the fields below.</Typography>
                </Box>
                <Box sx={{ mt: 2, mb: 2 }}>
                    <TextField
                        size="small"
                        fullWidth
                        label={data.type === 'GITHUB' ? "Client ID" : "Application ID"}
                        value={data.oAuthClientId}
                        onChange={event => onChange({ ...data, oAuthClientId: event.target.value })}
                    />
                    <TextField
                        component="form"
                        autoComplete="off"
                        size="small"
                        margin="normal"
                        fullWidth
                        type={showPassword ? 'text' : 'password'}
                        InputProps={{
                            endAdornment: (
                                <InputAdornment position="end">
                                    <IconButton onClick={handleClickShowPassword}>
                                        {showPassword ? <VisibilityOff /> : <Visibility />}
                                    </IconButton>
                                </InputAdornment>
                            )
                        }}
                        label={(data.type === 'GITHUB' ? "Client Secret" : "Secret")}
                        value={data.oAuthClientSecret}
                        onChange={event => onChange({ ...data, oAuthClientSecret: event.target.value })}
                    />
                </Box>
                <Box marginBottom={2}>
                    <Typography>
                        If creation is successful, Phobos will immediately generate a new authorization URL and redirect the browser to the VCS provider to finalize the OAuth flow.
                    </Typography>
                </Box>

            </Box>}
            {data.authType === 'ACCESS_TOKEN' && <Box sx={{ mt: 2 }}>
                <Typography>Generate a personal access token from your selected VCS provider type.</Typography>
                <Box mt={2}>
                    <Typography>{data.type === 'GITHUB' ? 'Select your preferred access token type and ensure' : 'Ensure'} the following scopes are enabled:</Typography>
                    {data.type === 'GITHUB' && <Box mt={2} width={185}>
                        <FormControl fullWidth size='small'>
                            <InputLabel>Access Token Type</InputLabel>
                            <Select
                                label="Access Token Type"
                                value={gitHubAccessTokenType}
                                onChange={event => setGitHubAccessTokenType(event.target.value)}
                                variant="outlined"
                            >
                                <MenuItem value="fine-grained">Fine-grained token</MenuItem>
                                <MenuItem value="classic">Classic token</MenuItem>
                            </Select>
                        </FormControl>
                    </Box>}
                    <Paper sx={{ pt: 0.25, pb: 0.25, mt: 1, mb: 1 }}>
                        <List sx={{ listStyleType: 'disc', pl: 4 }}>
                            {personalAccessTokenScopes.map((scope) => <ListItem key={scope} sx={{ display: 'list-item', padding: 0 }}>{scope}</ListItem>)}
                        </List>
                    </Paper>
                </Box>
                <Typography sx={{ mt: 2 }}>Then, copy and paste the personal access token into the field below.</Typography>
                <Box sx={{ mt: 2, mb: 2 }}>
                    <TextField
                        component="form"
                        size="small"
                        fullWidth
                        type={showPassword ? 'text' : 'password'}
                        autoComplete="off"
                        label="Personal Access Token"
                        value={data.personalAccessToken}
                        InputProps={{
                            endAdornment: (
                                <InputAdornment position="end">
                                    <IconButton onClick={handleClickShowPassword}>
                                        {showPassword ? <VisibilityOff /> : <Visibility />}
                                    </IconButton>
                                </InputAdornment>
                            )
                        }}
                        onChange={event => onChange({ ...data, personalAccessToken: event.target.value })}
                    />
                </Box>
            </Box>}
            <Divider sx={{ opacity: 0.6 }} />
        </Box>
    );
}

export default VCSProviderSetup;
