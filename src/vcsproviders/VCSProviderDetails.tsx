import { useState } from 'react';
import { Alert, Avatar, Box, Button, ButtonGroup, Dialog, DialogActions, DialogTitle, DialogContent, Collapse, Divider, Link, Menu, MenuItem, Paper, Stack, styled, Typography, Chip } from '@mui/material';
import { LoadingButton } from '@mui/lab';
import purple from '@mui/material/colors/purple';
import { useFragment, useMutation } from 'react-relay/hooks';
import { MutationError } from '../common/error';
import { ArrowDropUp } from '@mui/icons-material';
import graphql from 'babel-plugin-relay/macro';
import { useNavigate } from 'react-router-dom';
import moment from 'moment';
import { GetConnections as GetOrgConnections } from '../organizations/vcsproviders/OrganizationVCSProviders';
import { GetConnections as GetProjectConnections } from '../organizations/projects/vcsproviders/ProjectVCSProviders';
import { useSnackbar } from 'notistack';
import ConfirmationDialog from '../common/ConfirmationDialog';
import PRNButton from '../common/PRNButton';
import { default as ArrowDropDown, default as ArrowDropDownIcon } from '@mui/icons-material/ArrowDropDown';
import { VCSProviderDetailsDeleteMutation } from './__generated__/VCSProviderDetailsDeleteMutation.graphql';
import { VCSProviderDetailsFragment_vcsProvider$key } from './__generated__/VCSProviderDetailsFragment_vcsProvider.graphql';
import { VCSProviderDetailsResetOAuthMutation } from './__generated__/VCSProviderDetailsResetOAuthMutation.graphql';

const FieldLabel = styled(
    Typography
)(() => ({}));

const FieldValue = styled(
    Typography
)(({ theme }) => ({
    color: theme.palette.text.secondary,
    marginBottom: '16px',
    '&:last-child': {
        marginBottom: 0
    }
}));

interface DialogProps {
    onClose: (confirm?: boolean) => void
    resetInProgress: boolean
    error?: MutationError
}

function ResetOAuthDialog(props: DialogProps) {
    const { onClose, resetInProgress, error, ...other } = props

    return (
        <Dialog
            maxWidth="xs"
            open
            {...other}
        >
            <DialogTitle>Reset OAuth Token</DialogTitle>
            <DialogContent dividers>
                <Box sx={{ mt: 2, mb: 2 }}>
                    {error && <Alert sx={{ mt: 2, mb: 2 }} severity={error.severity}>
                        {error.message}
                    </Alert>}
                    <Typography>
                        Resetting the OAuth token will immediately generate a new authorization URL and redirect the browser to the VCS provider to finalize the OAuth flow.
                    </Typography>
                </Box>
            </DialogContent>
            <DialogActions>
                <Button color="inherit" onClick={() => onClose()}>
                    Cancel
                </Button>
                <LoadingButton
                    color="primary"
                    loading={resetInProgress} onClick={() => onClose(true)}
                >
                    Reset OAuth Token
                </LoadingButton>
            </DialogActions>
        </Dialog>
    );
}

interface Props {
    fragmentRef: VCSProviderDetailsFragment_vcsProvider$key
}

function VCSProviderDetails({ fragmentRef }: Props) {
    const [menuAnchorEl, setMenuAnchorEl] = useState<Element | null>(null);
    const [showMore, setShowMore] = useState(false);
    const [showResetOAuthDialog, setShowResetOAuthDialog] = useState<boolean>(false);
    const [error, setError] = useState<MutationError>();
    const [showDeleteConfirmationDialog, setShowDeleteConfirmationDialog] = useState<boolean>(false);
    const { enqueueSnackbar } = useSnackbar();
    const navigate = useNavigate();

    const CARD_PADDING = 3;

    const data = useFragment<VCSProviderDetailsFragment_vcsProvider$key>(graphql`
        fragment VCSProviderDetailsFragment_vcsProvider on VCSProvider
        {
            id
            name
            description
            type
            url
            authType
            extraOAuthScopes
            organization {
                id
            }
            project {
                id
            }
            scope
            metadata {
                prn
                createdAt
            }
            createdBy
        }
    `, fragmentRef);

    const onOpenMenu = (event: React.MouseEvent<HTMLButtonElement>) => {
        setMenuAnchorEl(event.currentTarget);
    };

    const onMenuClose = () => {
        setMenuAnchorEl(null);
    };

    const onMenuAction = (actionCallback: () => void) => {
        setMenuAnchorEl(null);
        actionCallback();
    };

    const [commitResetOAuth, commitResetOAuthInFlight] = useMutation<VCSProviderDetailsResetOAuthMutation>(graphql`
        mutation VCSProviderDetailsResetOAuthMutation($input: ResetVCSProviderOAuthTokenInput!) {
            resetVCSProviderOAuthToken(input: $input) {
                oAuthAuthorizationUrl
                problems {
                    message
                    field
                    type
                }
            }
        }
    `);

    const onResetOAuthDialogClosed = (confirm?: boolean) => {
        if (confirm) {
            commitResetOAuth({
                variables: {
                    input: {
                        providerId: data.id
                    }
                },
                onCompleted: data => {
                    if (data.resetVCSProviderOAuthToken.problems.length) {
                        setError({
                            severity: 'warning',
                            message: data.resetVCSProviderOAuthToken.problems.map((problem: any) => problem.message).join('; ')
                        });
                    } else if (!data.resetVCSProviderOAuthToken.oAuthAuthorizationUrl) {
                        setError({
                            severity: 'error',
                            message: "Unexpected error occurred"
                        });
                    }
                    else {
                        enqueueSnackbar('OAuth token reset', { variant: 'success' })
                        window.open(data.resetVCSProviderOAuthToken.oAuthAuthorizationUrl, '_blank')
                        setShowResetOAuthDialog(false);
                    }
                },
                onError: error => {
                    setError({
                        severity: 'error',
                        message: `Unexpected error occurred: ${error.message}`
                    });
                }
            });
        } else {
            setShowResetOAuthDialog(false);
        }
    };

    const [commit, commitInFlight] = useMutation<VCSProviderDetailsDeleteMutation>(graphql`
        mutation VCSProviderDetailsDeleteMutation($input: DeleteVCSProviderInput!, $connections: [ID!]!) {
            deleteVCSProvider(input: $input) {
                vcsProvider {
                    id @deleteEdge(connections: $connections)
                    ...VCSProviderDetailsFragment_vcsProvider
                }
                problems {
                    message
                    field
                    type
                }
            }
        }
    `);

    const onDeleteConfirmationDialogClosed = () => {
        commit({
            variables: {
                input: {
                    id: data.id
                },
                connections: data.scope === 'PROJECT' && data.project ? GetProjectConnections(data.project?.id) : GetOrgConnections(data.organization.id)
            },
            onCompleted: data => {
                if (data.deleteVCSProvider.problems.length) {
                    enqueueSnackbar(data.deleteVCSProvider.problems.map((problem: any) => problem.message).join('; '), { variant: 'warning' });
                } else {
                    navigate(`..`);
                }
            },
            onError: error => {
                enqueueSnackbar(`Unexpected error occurred: ${error.message}`, { variant: 'error' });
            }
        });
        setShowDeleteConfirmationDialog(false);
    };

    return (
        <Box>
            <Paper variant="outlined" sx={{ marginTop: 3, padding: CARD_PADDING }}>
                <Box display="flex" justifyContent="space-between">
                    <Box display="flex" alignItems="center">
                        <Avatar variant="rounded" sx={{ width: 32, height: 32, marginRight: 1, bgcolor: purple[300] }}>
                            {data.name[0].toUpperCase()}
                        </Avatar>
                        <Box marginLeft={1}>
                            <Box display="flex" alignItems="center">
                                <Typography variant="h5" sx={{ marginRight: 1 }}>{data.name}</Typography>
                            </Box>
                            <Typography color="textSecondary">{data.description}</Typography>
                        </Box>
                    </Box>
                    <Box>
                        <Stack direction="row" spacing={1}>
                            <PRNButton prn={data.metadata.prn} />
                            <ButtonGroup variant="outlined" color="primary">
                                <Button onClick={() => navigate('edit')}>Edit</Button>
                                <Button
                                    color="primary"
                                    size="small"
                                    aria-label="more options menu"
                                    aria-haspopup="menu"
                                    onClick={onOpenMenu}
                                >
                                    <ArrowDropDownIcon fontSize="small" />
                                </Button>
                            </ButtonGroup>
                        </Stack>
                        <Menu
                            id="vcs-provider-more-options-menu"
                            anchorEl={menuAnchorEl}
                            open={Boolean(menuAnchorEl)}
                            onClose={onMenuClose}
                            anchorOrigin={{
                                vertical: 'bottom',
                                horizontal: 'right',
                            }}
                            transformOrigin={{
                                vertical: 'top',
                                horizontal: 'right',
                            }}
                        >
                            <MenuItem onClick={() => navigate('edit_auth_settings')}>
                                Edit Auth Settings
                            </MenuItem>
                            {data.authType === 'OAUTH' && <MenuItem onClick={() => onMenuAction(() =>
                                setShowResetOAuthDialog(true))}>
                                Reset OAuth Token
                            </MenuItem>}
                            <MenuItem onClick={() => onMenuAction(() => setShowDeleteConfirmationDialog(true))}>
                                Delete VCS Provider
                            </MenuItem>
                        </Menu>
                    </Box>
                </Box>
                <Divider
                    sx={{
                        mt: 2,
                        mb: 2,
                        ml: -CARD_PADDING,
                        mr: -CARD_PADDING,
                        opacity: 0.6
                    }}
                />
                <Box>
                    <Box>
                        <FieldLabel>Type</FieldLabel>
                        <FieldValue>{data.type === 'GITHUB' ? 'GitHub' : 'GitLab'}</FieldValue>
                        <FieldLabel>URL</FieldLabel>
                        <FieldValue>{data.url}</FieldValue>
                        <FieldLabel>Authorization Type</FieldLabel>
                        <FieldValue>{data.authType === 'OAUTH' ? 'OAuth' : 'Personal Access Token'}</FieldValue>
                        {data.authType == 'OAUTH' && <Box>
                            <FieldLabel>Extra OAuth Scopes</FieldLabel>
                            {data.extraOAuthScopes.length === 0 && <FieldValue>None</FieldValue>}
                            {data.extraOAuthScopes.length > 0 && <Stack direction="row" spacing={1} mt={1}>
                                {data.extraOAuthScopes.map(scope => <Chip key={scope} size="small" color="secondary" label={scope} />)}
                            </Stack>}
                        </Box>}
                    </Box>
                    <Box marginTop={4}>
                        <Link
                            sx={{ display: 'flex', alignItems: 'center' }}
                            component="button" variant="body1"
                            color="textSecondary"
                            underline="hover"
                            onClick={() => setShowMore(!showMore)}
                        >
                            More Details {showMore ? <ArrowDropUp /> : <ArrowDropDown />}
                        </Link>
                        <Collapse in={showMore} timeout="auto" unmountOnExit>
                            <Box marginTop={2}>
                                <Typography variant="body2">
                                    Created {moment(data.metadata.createdAt as moment.MomentInput).fromNow()} by {data.createdBy}
                                </Typography>
                            </Box>
                        </Collapse>
                    </Box>
                </Box>
            </Paper>
            {showDeleteConfirmationDialog && <ConfirmationDialog
                title="Delete VCS Provider"
                message={<span>Are you sure you want to delete VCS provider <strong>{data.name}</strong>?</span>}
                confirmButtonLabel="Delete"
                opInProgress={commitInFlight}
                onConfirm={onDeleteConfirmationDialogClosed}
                onClose={() => setShowDeleteConfirmationDialog(false)}
            />}
            {showResetOAuthDialog && <ResetOAuthDialog
                onClose={onResetOAuthDialogClosed}
                resetInProgress={commitResetOAuthInFlight}
                error={error}
            />}
        </Box>
    );
}

export default VCSProviderDetails;
