/**
 * @generated SignedSource<<7b263bc632840b4c35ab87b0d5499870>>
 * @lightSyntaxTransform
 * @nogrep
 */

/* tslint:disable */
/* eslint-disable */
// @ts-nocheck

import { Fragment, ReaderFragment } from 'relay-runtime';
export type VCSProviderAuthType = "ACCESS_TOKEN" | "OAUTH" | "%future added value";
export type VCSProviderType = "GITHUB" | "GITLAB" | "%future added value";
import { FragmentRefs } from "relay-runtime";
export type EditVCSProviderAuthSettingsFragment_vcsProvider$data = {
  readonly authType: VCSProviderAuthType;
  readonly extraOAuthScopes: ReadonlyArray<string>;
  readonly id: string;
  readonly name: string;
  readonly type: VCSProviderType;
  readonly " $fragmentSpreads": FragmentRefs<"VCSProviderAuthSettingsForm_vcsProvider">;
  readonly " $fragmentType": "EditVCSProviderAuthSettingsFragment_vcsProvider";
};
export type EditVCSProviderAuthSettingsFragment_vcsProvider$key = {
  readonly " $data"?: EditVCSProviderAuthSettingsFragment_vcsProvider$data;
  readonly " $fragmentSpreads": FragmentRefs<"EditVCSProviderAuthSettingsFragment_vcsProvider">;
};

const node: ReaderFragment = {
  "argumentDefinitions": [],
  "kind": "Fragment",
  "metadata": null,
  "name": "EditVCSProviderAuthSettingsFragment_vcsProvider",
  "selections": [
    {
      "alias": null,
      "args": null,
      "kind": "ScalarField",
      "name": "id",
      "storageKey": null
    },
    {
      "alias": null,
      "args": null,
      "kind": "ScalarField",
      "name": "name",
      "storageKey": null
    },
    {
      "alias": null,
      "args": null,
      "kind": "ScalarField",
      "name": "type",
      "storageKey": null
    },
    {
      "alias": null,
      "args": null,
      "kind": "ScalarField",
      "name": "authType",
      "storageKey": null
    },
    {
      "alias": null,
      "args": null,
      "kind": "ScalarField",
      "name": "extraOAuthScopes",
      "storageKey": null
    },
    {
      "args": null,
      "kind": "FragmentSpread",
      "name": "VCSProviderAuthSettingsForm_vcsProvider"
    }
  ],
  "type": "VCSProvider",
  "abstractKey": null
};

(node as any).hash = "a719da1cd6b6e301d92efe450039e2c8";

export default node;
