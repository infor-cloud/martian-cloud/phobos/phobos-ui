/**
 * @generated SignedSource<<e18d298d2e88190e83ddfdb63a08bc11>>
 * @lightSyntaxTransform
 * @nogrep
 */

/* tslint:disable */
/* eslint-disable */
// @ts-nocheck

import { Fragment, ReaderFragment } from 'relay-runtime';
export type ScopeType = "GLOBAL" | "ORGANIZATION" | "PROJECT" | "%future added value";
export type VCSProviderAuthType = "ACCESS_TOKEN" | "OAUTH" | "%future added value";
export type VCSProviderType = "GITHUB" | "GITLAB" | "%future added value";
import { FragmentRefs } from "relay-runtime";
export type VCSProviderDetailsFragment_vcsProvider$data = {
  readonly authType: VCSProviderAuthType;
  readonly createdBy: string;
  readonly description: string;
  readonly extraOAuthScopes: ReadonlyArray<string>;
  readonly id: string;
  readonly metadata: {
    readonly createdAt: any;
    readonly prn: string;
  };
  readonly name: string;
  readonly organization: {
    readonly id: string;
  };
  readonly project: {
    readonly id: string;
  } | null | undefined;
  readonly scope: ScopeType;
  readonly type: VCSProviderType;
  readonly url: string;
  readonly " $fragmentType": "VCSProviderDetailsFragment_vcsProvider";
};
export type VCSProviderDetailsFragment_vcsProvider$key = {
  readonly " $data"?: VCSProviderDetailsFragment_vcsProvider$data;
  readonly " $fragmentSpreads": FragmentRefs<"VCSProviderDetailsFragment_vcsProvider">;
};

const node: ReaderFragment = (function(){
var v0 = {
  "alias": null,
  "args": null,
  "kind": "ScalarField",
  "name": "id",
  "storageKey": null
},
v1 = [
  (v0/*: any*/)
];
return {
  "argumentDefinitions": [],
  "kind": "Fragment",
  "metadata": null,
  "name": "VCSProviderDetailsFragment_vcsProvider",
  "selections": [
    (v0/*: any*/),
    {
      "alias": null,
      "args": null,
      "kind": "ScalarField",
      "name": "name",
      "storageKey": null
    },
    {
      "alias": null,
      "args": null,
      "kind": "ScalarField",
      "name": "description",
      "storageKey": null
    },
    {
      "alias": null,
      "args": null,
      "kind": "ScalarField",
      "name": "type",
      "storageKey": null
    },
    {
      "alias": null,
      "args": null,
      "kind": "ScalarField",
      "name": "url",
      "storageKey": null
    },
    {
      "alias": null,
      "args": null,
      "kind": "ScalarField",
      "name": "authType",
      "storageKey": null
    },
    {
      "alias": null,
      "args": null,
      "kind": "ScalarField",
      "name": "extraOAuthScopes",
      "storageKey": null
    },
    {
      "alias": null,
      "args": null,
      "concreteType": "Organization",
      "kind": "LinkedField",
      "name": "organization",
      "plural": false,
      "selections": (v1/*: any*/),
      "storageKey": null
    },
    {
      "alias": null,
      "args": null,
      "concreteType": "Project",
      "kind": "LinkedField",
      "name": "project",
      "plural": false,
      "selections": (v1/*: any*/),
      "storageKey": null
    },
    {
      "alias": null,
      "args": null,
      "kind": "ScalarField",
      "name": "scope",
      "storageKey": null
    },
    {
      "alias": null,
      "args": null,
      "concreteType": "ResourceMetadata",
      "kind": "LinkedField",
      "name": "metadata",
      "plural": false,
      "selections": [
        {
          "alias": null,
          "args": null,
          "kind": "ScalarField",
          "name": "prn",
          "storageKey": null
        },
        {
          "alias": null,
          "args": null,
          "kind": "ScalarField",
          "name": "createdAt",
          "storageKey": null
        }
      ],
      "storageKey": null
    },
    {
      "alias": null,
      "args": null,
      "kind": "ScalarField",
      "name": "createdBy",
      "storageKey": null
    }
  ],
  "type": "VCSProvider",
  "abstractKey": null
};
})();

(node as any).hash = "7fc903296000c01a77e4e95e5f3888d1";

export default node;
