/**
 * @generated SignedSource<<682bc718b6aadde93727aaaf01aa18ec>>
 * @lightSyntaxTransform
 * @nogrep
 */

/* tslint:disable */
/* eslint-disable */
// @ts-nocheck

import { ConcreteRequest, Mutation } from 'relay-runtime';
import { FragmentRefs } from "relay-runtime";
export type ProblemType = "BAD_REQUEST" | "CONFLICT" | "FORBIDDEN" | "NOT_FOUND" | "%future added value";
export type ScopeType = "GLOBAL" | "ORGANIZATION" | "PROJECT" | "%future added value";
export type VCSProviderAuthType = "ACCESS_TOKEN" | "OAUTH" | "%future added value";
export type VCSProviderType = "GITHUB" | "GITLAB" | "%future added value";
export type CreateVCSProviderInput = {
  authType: VCSProviderAuthType;
  clientMutationId?: string | null | undefined;
  description: string;
  extraOAuthScopes?: ReadonlyArray<string> | null | undefined;
  name: string;
  oAuthClientId?: string | null | undefined;
  oAuthClientSecret?: string | null | undefined;
  organizationId?: string | null | undefined;
  personalAccessToken?: string | null | undefined;
  projectId?: string | null | undefined;
  scope: ScopeType;
  type: VCSProviderType;
  url?: string | null | undefined;
};
export type NewVCSProviderMutation$variables = {
  connections: ReadonlyArray<string>;
  input: CreateVCSProviderInput;
};
export type NewVCSProviderMutation$data = {
  readonly createVCSProvider: {
    readonly oAuthAuthorizationUrl: string | null | undefined;
    readonly problems: ReadonlyArray<{
      readonly field: ReadonlyArray<string> | null | undefined;
      readonly message: string;
      readonly type: ProblemType;
    }>;
    readonly vcsProvider: {
      readonly id: string;
      readonly " $fragmentSpreads": FragmentRefs<"VCSProviderListItemFragment_vcsProvider">;
    } | null | undefined;
  };
};
export type NewVCSProviderMutation = {
  response: NewVCSProviderMutation$data;
  variables: NewVCSProviderMutation$variables;
};

const node: ConcreteRequest = (function(){
var v0 = {
  "defaultValue": null,
  "kind": "LocalArgument",
  "name": "connections"
},
v1 = {
  "defaultValue": null,
  "kind": "LocalArgument",
  "name": "input"
},
v2 = [
  {
    "kind": "Variable",
    "name": "input",
    "variableName": "input"
  }
],
v3 = {
  "alias": null,
  "args": null,
  "kind": "ScalarField",
  "name": "id",
  "storageKey": null
},
v4 = {
  "alias": null,
  "args": null,
  "kind": "ScalarField",
  "name": "oAuthAuthorizationUrl",
  "storageKey": null
},
v5 = {
  "alias": null,
  "args": null,
  "concreteType": "Problem",
  "kind": "LinkedField",
  "name": "problems",
  "plural": true,
  "selections": [
    {
      "alias": null,
      "args": null,
      "kind": "ScalarField",
      "name": "message",
      "storageKey": null
    },
    {
      "alias": null,
      "args": null,
      "kind": "ScalarField",
      "name": "field",
      "storageKey": null
    },
    {
      "alias": null,
      "args": null,
      "kind": "ScalarField",
      "name": "type",
      "storageKey": null
    }
  ],
  "storageKey": null
},
v6 = {
  "alias": null,
  "args": null,
  "kind": "ScalarField",
  "name": "name",
  "storageKey": null
};
return {
  "fragment": {
    "argumentDefinitions": [
      (v0/*: any*/),
      (v1/*: any*/)
    ],
    "kind": "Fragment",
    "metadata": null,
    "name": "NewVCSProviderMutation",
    "selections": [
      {
        "alias": null,
        "args": (v2/*: any*/),
        "concreteType": "CreateVCSProviderPayload",
        "kind": "LinkedField",
        "name": "createVCSProvider",
        "plural": false,
        "selections": [
          {
            "alias": null,
            "args": null,
            "concreteType": "VCSProvider",
            "kind": "LinkedField",
            "name": "vcsProvider",
            "plural": false,
            "selections": [
              (v3/*: any*/),
              {
                "args": null,
                "kind": "FragmentSpread",
                "name": "VCSProviderListItemFragment_vcsProvider"
              }
            ],
            "storageKey": null
          },
          (v4/*: any*/),
          (v5/*: any*/)
        ],
        "storageKey": null
      }
    ],
    "type": "Mutation",
    "abstractKey": null
  },
  "kind": "Request",
  "operation": {
    "argumentDefinitions": [
      (v1/*: any*/),
      (v0/*: any*/)
    ],
    "kind": "Operation",
    "name": "NewVCSProviderMutation",
    "selections": [
      {
        "alias": null,
        "args": (v2/*: any*/),
        "concreteType": "CreateVCSProviderPayload",
        "kind": "LinkedField",
        "name": "createVCSProvider",
        "plural": false,
        "selections": [
          {
            "alias": null,
            "args": null,
            "concreteType": "VCSProvider",
            "kind": "LinkedField",
            "name": "vcsProvider",
            "plural": false,
            "selections": [
              (v3/*: any*/),
              (v6/*: any*/),
              {
                "alias": null,
                "args": null,
                "kind": "ScalarField",
                "name": "scope",
                "storageKey": null
              },
              {
                "alias": null,
                "args": null,
                "kind": "ScalarField",
                "name": "description",
                "storageKey": null
              },
              {
                "alias": null,
                "args": null,
                "kind": "ScalarField",
                "name": "organizationName",
                "storageKey": null
              },
              {
                "alias": null,
                "args": null,
                "concreteType": "Project",
                "kind": "LinkedField",
                "name": "project",
                "plural": false,
                "selections": [
                  (v6/*: any*/),
                  (v3/*: any*/)
                ],
                "storageKey": null
              },
              {
                "alias": null,
                "args": null,
                "concreteType": "ResourceMetadata",
                "kind": "LinkedField",
                "name": "metadata",
                "plural": false,
                "selections": [
                  {
                    "alias": null,
                    "args": null,
                    "kind": "ScalarField",
                    "name": "updatedAt",
                    "storageKey": null
                  }
                ],
                "storageKey": null
              }
            ],
            "storageKey": null
          },
          {
            "alias": null,
            "args": null,
            "filters": null,
            "handle": "prependNode",
            "key": "",
            "kind": "LinkedHandle",
            "name": "vcsProvider",
            "handleArgs": [
              {
                "kind": "Variable",
                "name": "connections",
                "variableName": "connections"
              },
              {
                "kind": "Literal",
                "name": "edgeTypeName",
                "value": "VCSProviderEdge"
              }
            ]
          },
          (v4/*: any*/),
          (v5/*: any*/)
        ],
        "storageKey": null
      }
    ]
  },
  "params": {
    "cacheID": "2b356736c147125a9cf2eedbe453a460",
    "id": null,
    "metadata": {},
    "name": "NewVCSProviderMutation",
    "operationKind": "mutation",
    "text": "mutation NewVCSProviderMutation(\n  $input: CreateVCSProviderInput!\n) {\n  createVCSProvider(input: $input) {\n    vcsProvider {\n      id\n      ...VCSProviderListItemFragment_vcsProvider\n    }\n    oAuthAuthorizationUrl\n    problems {\n      message\n      field\n      type\n    }\n  }\n}\n\nfragment VCSProviderListItemFragment_vcsProvider on VCSProvider {\n  id\n  name\n  scope\n  description\n  organizationName\n  project {\n    name\n    id\n  }\n  metadata {\n    updatedAt\n  }\n}\n"
  }
};
})();

(node as any).hash = "af687c90f8dc4504507e78790b9e01cf";

export default node;
