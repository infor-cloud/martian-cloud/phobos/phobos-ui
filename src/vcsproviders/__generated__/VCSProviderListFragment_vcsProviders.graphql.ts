/**
 * @generated SignedSource<<a5f7fc61725cc0e4fbdfaf00b1a5f47a>>
 * @lightSyntaxTransform
 * @nogrep
 */

/* tslint:disable */
/* eslint-disable */
// @ts-nocheck

import { Fragment, ReaderFragment } from 'relay-runtime';
export type ScopeType = "GLOBAL" | "ORGANIZATION" | "PROJECT" | "%future added value";
import { FragmentRefs } from "relay-runtime";
export type VCSProviderListFragment_vcsProviders$data = {
  readonly edges: ReadonlyArray<{
    readonly node: {
      readonly id: string;
      readonly scope: ScopeType;
      readonly " $fragmentSpreads": FragmentRefs<"VCSProviderListItemFragment_vcsProvider">;
    } | null | undefined;
  } | null | undefined> | null | undefined;
  readonly totalCount: number;
  readonly " $fragmentType": "VCSProviderListFragment_vcsProviders";
};
export type VCSProviderListFragment_vcsProviders$key = {
  readonly " $data"?: VCSProviderListFragment_vcsProviders$data;
  readonly " $fragmentSpreads": FragmentRefs<"VCSProviderListFragment_vcsProviders">;
};

const node: ReaderFragment = {
  "argumentDefinitions": [],
  "kind": "Fragment",
  "metadata": null,
  "name": "VCSProviderListFragment_vcsProviders",
  "selections": [
    {
      "alias": null,
      "args": null,
      "kind": "ScalarField",
      "name": "totalCount",
      "storageKey": null
    },
    {
      "alias": null,
      "args": null,
      "concreteType": "VCSProviderEdge",
      "kind": "LinkedField",
      "name": "edges",
      "plural": true,
      "selections": [
        {
          "alias": null,
          "args": null,
          "concreteType": "VCSProvider",
          "kind": "LinkedField",
          "name": "node",
          "plural": false,
          "selections": [
            {
              "alias": null,
              "args": null,
              "kind": "ScalarField",
              "name": "id",
              "storageKey": null
            },
            {
              "alias": null,
              "args": null,
              "kind": "ScalarField",
              "name": "scope",
              "storageKey": null
            },
            {
              "args": null,
              "kind": "FragmentSpread",
              "name": "VCSProviderListItemFragment_vcsProvider"
            }
          ],
          "storageKey": null
        }
      ],
      "storageKey": null
    }
  ],
  "type": "VCSProviderConnection",
  "abstractKey": null
};

(node as any).hash = "2e242979de55bb8521697931d09b08ff";

export default node;
