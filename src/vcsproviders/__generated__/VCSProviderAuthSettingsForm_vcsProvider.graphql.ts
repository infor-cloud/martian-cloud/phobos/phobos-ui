/**
 * @generated SignedSource<<043027abf8b84d69118280144c8b7ddd>>
 * @lightSyntaxTransform
 * @nogrep
 */

/* tslint:disable */
/* eslint-disable */
// @ts-nocheck

import { Fragment, ReaderFragment } from 'relay-runtime';
export type VCSProviderAuthType = "ACCESS_TOKEN" | "OAUTH" | "%future added value";
export type VCSProviderType = "GITHUB" | "GITLAB" | "%future added value";
import { FragmentRefs } from "relay-runtime";
export type VCSProviderAuthSettingsForm_vcsProvider$data = {
  readonly authType: VCSProviderAuthType;
  readonly type: VCSProviderType;
  readonly " $fragmentType": "VCSProviderAuthSettingsForm_vcsProvider";
};
export type VCSProviderAuthSettingsForm_vcsProvider$key = {
  readonly " $data"?: VCSProviderAuthSettingsForm_vcsProvider$data;
  readonly " $fragmentSpreads": FragmentRefs<"VCSProviderAuthSettingsForm_vcsProvider">;
};

const node: ReaderFragment = {
  "argumentDefinitions": [],
  "kind": "Fragment",
  "metadata": null,
  "name": "VCSProviderAuthSettingsForm_vcsProvider",
  "selections": [
    {
      "alias": null,
      "args": null,
      "kind": "ScalarField",
      "name": "type",
      "storageKey": null
    },
    {
      "alias": null,
      "args": null,
      "kind": "ScalarField",
      "name": "authType",
      "storageKey": null
    }
  ],
  "type": "VCSProvider",
  "abstractKey": null
};

(node as any).hash = "58862c2e5771e0462852cd5dc8e8565c";

export default node;
