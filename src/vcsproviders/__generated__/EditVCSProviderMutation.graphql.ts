/**
 * @generated SignedSource<<c6953d31815b1679ebdd14e34353f169>>
 * @lightSyntaxTransform
 * @nogrep
 */

/* tslint:disable */
/* eslint-disable */
// @ts-nocheck

import { ConcreteRequest, Mutation } from 'relay-runtime';
import { FragmentRefs } from "relay-runtime";
export type ProblemType = "BAD_REQUEST" | "CONFLICT" | "FORBIDDEN" | "NOT_FOUND" | "%future added value";
export type UpdateVCSProviderInput = {
  clientMutationId?: string | null | undefined;
  description?: string | null | undefined;
  extraOAuthScopes?: ReadonlyArray<string> | null | undefined;
  id: string;
  metadata?: ResourceMetadataInput | null | undefined;
  oAuthClientId?: string | null | undefined;
  oAuthClientSecret?: string | null | undefined;
  personalAccessToken?: string | null | undefined;
};
export type ResourceMetadataInput = {
  version: string;
};
export type EditVCSProviderMutation$variables = {
  input: UpdateVCSProviderInput;
};
export type EditVCSProviderMutation$data = {
  readonly updateVCSProvider: {
    readonly problems: ReadonlyArray<{
      readonly field: ReadonlyArray<string> | null | undefined;
      readonly message: string;
      readonly type: ProblemType;
    }>;
    readonly vcsProvider: {
      readonly description: string;
      readonly id: string;
      readonly " $fragmentSpreads": FragmentRefs<"VCSProviderDetailsFragment_vcsProvider">;
    } | null | undefined;
  };
};
export type EditVCSProviderMutation = {
  response: EditVCSProviderMutation$data;
  variables: EditVCSProviderMutation$variables;
};

const node: ConcreteRequest = (function(){
var v0 = [
  {
    "defaultValue": null,
    "kind": "LocalArgument",
    "name": "input"
  }
],
v1 = [
  {
    "kind": "Variable",
    "name": "input",
    "variableName": "input"
  }
],
v2 = {
  "alias": null,
  "args": null,
  "kind": "ScalarField",
  "name": "id",
  "storageKey": null
},
v3 = {
  "alias": null,
  "args": null,
  "kind": "ScalarField",
  "name": "description",
  "storageKey": null
},
v4 = {
  "alias": null,
  "args": null,
  "kind": "ScalarField",
  "name": "type",
  "storageKey": null
},
v5 = {
  "alias": null,
  "args": null,
  "concreteType": "Problem",
  "kind": "LinkedField",
  "name": "problems",
  "plural": true,
  "selections": [
    {
      "alias": null,
      "args": null,
      "kind": "ScalarField",
      "name": "message",
      "storageKey": null
    },
    {
      "alias": null,
      "args": null,
      "kind": "ScalarField",
      "name": "field",
      "storageKey": null
    },
    (v4/*: any*/)
  ],
  "storageKey": null
},
v6 = [
  (v2/*: any*/)
];
return {
  "fragment": {
    "argumentDefinitions": (v0/*: any*/),
    "kind": "Fragment",
    "metadata": null,
    "name": "EditVCSProviderMutation",
    "selections": [
      {
        "alias": null,
        "args": (v1/*: any*/),
        "concreteType": "UpdateVCSProviderPayload",
        "kind": "LinkedField",
        "name": "updateVCSProvider",
        "plural": false,
        "selections": [
          {
            "alias": null,
            "args": null,
            "concreteType": "VCSProvider",
            "kind": "LinkedField",
            "name": "vcsProvider",
            "plural": false,
            "selections": [
              (v2/*: any*/),
              (v3/*: any*/),
              {
                "args": null,
                "kind": "FragmentSpread",
                "name": "VCSProviderDetailsFragment_vcsProvider"
              }
            ],
            "storageKey": null
          },
          (v5/*: any*/)
        ],
        "storageKey": null
      }
    ],
    "type": "Mutation",
    "abstractKey": null
  },
  "kind": "Request",
  "operation": {
    "argumentDefinitions": (v0/*: any*/),
    "kind": "Operation",
    "name": "EditVCSProviderMutation",
    "selections": [
      {
        "alias": null,
        "args": (v1/*: any*/),
        "concreteType": "UpdateVCSProviderPayload",
        "kind": "LinkedField",
        "name": "updateVCSProvider",
        "plural": false,
        "selections": [
          {
            "alias": null,
            "args": null,
            "concreteType": "VCSProvider",
            "kind": "LinkedField",
            "name": "vcsProvider",
            "plural": false,
            "selections": [
              (v2/*: any*/),
              (v3/*: any*/),
              {
                "alias": null,
                "args": null,
                "kind": "ScalarField",
                "name": "name",
                "storageKey": null
              },
              (v4/*: any*/),
              {
                "alias": null,
                "args": null,
                "kind": "ScalarField",
                "name": "url",
                "storageKey": null
              },
              {
                "alias": null,
                "args": null,
                "kind": "ScalarField",
                "name": "authType",
                "storageKey": null
              },
              {
                "alias": null,
                "args": null,
                "kind": "ScalarField",
                "name": "extraOAuthScopes",
                "storageKey": null
              },
              {
                "alias": null,
                "args": null,
                "concreteType": "Organization",
                "kind": "LinkedField",
                "name": "organization",
                "plural": false,
                "selections": (v6/*: any*/),
                "storageKey": null
              },
              {
                "alias": null,
                "args": null,
                "concreteType": "Project",
                "kind": "LinkedField",
                "name": "project",
                "plural": false,
                "selections": (v6/*: any*/),
                "storageKey": null
              },
              {
                "alias": null,
                "args": null,
                "kind": "ScalarField",
                "name": "scope",
                "storageKey": null
              },
              {
                "alias": null,
                "args": null,
                "concreteType": "ResourceMetadata",
                "kind": "LinkedField",
                "name": "metadata",
                "plural": false,
                "selections": [
                  {
                    "alias": null,
                    "args": null,
                    "kind": "ScalarField",
                    "name": "prn",
                    "storageKey": null
                  },
                  {
                    "alias": null,
                    "args": null,
                    "kind": "ScalarField",
                    "name": "createdAt",
                    "storageKey": null
                  }
                ],
                "storageKey": null
              },
              {
                "alias": null,
                "args": null,
                "kind": "ScalarField",
                "name": "createdBy",
                "storageKey": null
              }
            ],
            "storageKey": null
          },
          (v5/*: any*/)
        ],
        "storageKey": null
      }
    ]
  },
  "params": {
    "cacheID": "4a3204cf44906afd086208a669484a7d",
    "id": null,
    "metadata": {},
    "name": "EditVCSProviderMutation",
    "operationKind": "mutation",
    "text": "mutation EditVCSProviderMutation(\n  $input: UpdateVCSProviderInput!\n) {\n  updateVCSProvider(input: $input) {\n    vcsProvider {\n      id\n      description\n      ...VCSProviderDetailsFragment_vcsProvider\n    }\n    problems {\n      message\n      field\n      type\n    }\n  }\n}\n\nfragment VCSProviderDetailsFragment_vcsProvider on VCSProvider {\n  id\n  name\n  description\n  type\n  url\n  authType\n  extraOAuthScopes\n  organization {\n    id\n  }\n  project {\n    id\n  }\n  scope\n  metadata {\n    prn\n    createdAt\n  }\n  createdBy\n}\n"
  }
};
})();

(node as any).hash = "da9b5932e2be709f2f872e87df3455b3";

export default node;
