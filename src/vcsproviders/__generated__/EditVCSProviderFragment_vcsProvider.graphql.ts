/**
 * @generated SignedSource<<76e7b0dce04b253a6c4e45ddd5359b4d>>
 * @lightSyntaxTransform
 * @nogrep
 */

/* tslint:disable */
/* eslint-disable */
// @ts-nocheck

import { Fragment, ReaderFragment } from 'relay-runtime';
export type VCSProviderType = "GITHUB" | "GITLAB" | "%future added value";
import { FragmentRefs } from "relay-runtime";
export type EditVCSProviderFragment_vcsProvider$data = {
  readonly description: string;
  readonly id: string;
  readonly name: string;
  readonly type: VCSProviderType;
  readonly url: string;
  readonly " $fragmentType": "EditVCSProviderFragment_vcsProvider";
};
export type EditVCSProviderFragment_vcsProvider$key = {
  readonly " $data"?: EditVCSProviderFragment_vcsProvider$data;
  readonly " $fragmentSpreads": FragmentRefs<"EditVCSProviderFragment_vcsProvider">;
};

const node: ReaderFragment = {
  "argumentDefinitions": [],
  "kind": "Fragment",
  "metadata": null,
  "name": "EditVCSProviderFragment_vcsProvider",
  "selections": [
    {
      "alias": null,
      "args": null,
      "kind": "ScalarField",
      "name": "id",
      "storageKey": null
    },
    {
      "alias": null,
      "args": null,
      "kind": "ScalarField",
      "name": "name",
      "storageKey": null
    },
    {
      "alias": null,
      "args": null,
      "kind": "ScalarField",
      "name": "description",
      "storageKey": null
    },
    {
      "alias": null,
      "args": null,
      "kind": "ScalarField",
      "name": "url",
      "storageKey": null
    },
    {
      "alias": null,
      "args": null,
      "kind": "ScalarField",
      "name": "type",
      "storageKey": null
    }
  ],
  "type": "VCSProvider",
  "abstractKey": null
};

(node as any).hash = "f52002744a783c747be133dc94880765";

export default node;
