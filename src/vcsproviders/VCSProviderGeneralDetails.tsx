import React from 'react';
import { Box, Divider, Stack, TextField, Typography } from '@mui/material'
import { GitHub } from '@mui/icons-material';
import { Gitlab } from 'mdi-material-ui';
import PanelButton from '../common/PanelButton';
import { FormData } from './VCSProviderForm';

const PANEL_ICONS = {
    GITHUB: <GitHub />,
    GITLAB: <Gitlab />
};

interface Props {
    editMode?: boolean
    data: FormData
    onChange: (data: FormData) => void
}

interface VCSProviderTypesObj {
    name: 'GITHUB' | 'GITLAB'
    title: string
}

const VCSProviderTypes: VCSProviderTypesObj[] = [
    { name: 'GITHUB', title: 'GitHub' },
    { name: 'GITLAB', title: 'GitLab' }
];

function VCSProviderGeneralDetails({ editMode, data, onChange }: Props) {

    const onTypeChange = (type: 'GITHUB' | 'GITLAB') => {
        if (!editMode && (data.type !== type)) {
            onChange({
                ...data,
                type, url: '', oAuthClientId: '', oAuthClientSecret: '', extraOAuthScopes: [],
            });
        }
    };

    return (
        <Box>
            <Typography variant="subtitle1" gutterBottom>VCS Provider</Typography>
            {!editMode && <Typography
                sx={{ mb: 2 }}
                variant="subtitle2"
                color="textSecondary">
                Select a VCS Provider type. Phobos supports the following VCS providers:</Typography>}
            <Divider sx={{ opacity: 0.6 }} />
            <Stack marginTop={2} marginBottom={2} direction="row" spacing={2}>
                {VCSProviderTypes.map(type => <PanelButton
                    key={type.name}
                    disabled={editMode}
                    selected={data.type === type.name}
                    onClick={() => onTypeChange(type.name)}
                >
                    {PANEL_ICONS[type.name]}
                    <Typography variant="subtitle1">{type.title}</Typography>
                </PanelButton>)}
            </Stack>
            <Typography sx={{ mt: 2 }} variant="subtitle1" gutterBottom>Provider Details</Typography>
            {!editMode && <Typography
                sx={{ mb: 2 }}
                variant="subtitle2"
                color="textSecondary">
                Choose a unique name as a resource can have multiple instances of a VCS provider.
            </Typography>}
            <Divider sx={{ opacity: 0.6 }} />
            <Box marginTop={2} marginBottom={2}>
                <TextField
                    disabled={editMode}
                    size="small" fullWidth
                    label="Name"
                    value={data.name}
                    onChange={event => onChange({ ...data, name: event.target.value })}
                />
                <TextField
                    size="small"
                    margin='normal'
                    fullWidth
                    label="Description"
                    value={data.description}
                    onChange={event => onChange({ ...data, description: event.target.value })}
                />
            </Box>
            {data.type && <React.Fragment>
                <Typography sx={{ mt: 2 }} variant="subtitle1" gutterBottom>URL &#x28;optional&#x29;</Typography>
                {!editMode && <Typography sx={{ mb: 2 }}
                    variant="subtitle2" color="textSecondary">
                    You may enter an API URL. If no URL is entered, Phobos will use the selected provider type's publicly available API URL.
                </Typography>}
                <Divider sx={{ opacity: 0.6 }} />
                <Box marginTop={2} marginBottom={2}>
                    <TextField
                        disabled={editMode}
                        size="small" fullWidth
                        placeholder={data.type === 'GITHUB' ? 'https://api.github.com' : 'https://gitlab.com'}
                        value={data.url}
                        onChange={event => onChange({ ...data, url: event.target.value })}
                    />
                </Box>
                <Divider sx={{ opacity: 0.6 }} />
            </React.Fragment>}
        </Box>
    );
}

export default VCSProviderGeneralDetails;
