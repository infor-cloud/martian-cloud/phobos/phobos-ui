import { Avatar, Box, Chip, ListItemButton, ListItemText, Typography, useTheme } from '@mui/material';
import purple from '@mui/material/colors/purple';
import graphql from 'babel-plugin-relay/macro';
import Timestamp from '../common/Timestamp';
import { useFragment } from 'react-relay/hooks';
import { Link as RouterLink } from 'react-router-dom';
import { VCSProviderListItemFragment_vcsProvider$key } from './__generated__/VCSProviderListItemFragment_vcsProvider.graphql';

interface Props {
    showOrgChip?: boolean
    fragmentRef: VCSProviderListItemFragment_vcsProvider$key
}

function VCSProviderListItem({ fragmentRef, showOrgChip }: Props) {
    const theme = useTheme();

    const data = useFragment<VCSProviderListItemFragment_vcsProvider$key>(graphql`
        fragment VCSProviderListItemFragment_vcsProvider on VCSProvider {
            id
            name
            scope
            description
            organizationName
            project {
                name
            }
            metadata {
                updatedAt
            }
        }
    `, fragmentRef);

    const baseLink = `/organizations/${data.organizationName}`;
    const route = data.scope === 'PROJECT' && data.project ? `${baseLink}/projects/${data.project.name}/-/vcs_providers/${data.id}` : `${baseLink}/-/vcs_providers/${data.id}`;

    return (
        <ListItemButton
            component={RouterLink}
            to={route}
            sx={{
                paddingY: 1,
                borderBottom: `1px solid ${theme.palette.divider}`,
                borderLeft: `1px solid ${theme.palette.divider}`,
                borderRight: `1px solid ${theme.palette.divider}`,
                '&:last-child': {
                    borderBottomLeftRadius: 4,
                    borderBottomRightRadius: 4
                }
            }}>
            <Avatar variant="rounded" sx={{ width: 32, height: 32, bgcolor: purple[300], marginRight: 2 }}>
                {data.name[0].toUpperCase()}
            </Avatar>
            <ListItemText
                primary={
                    <Box>
                        <Typography fontWeight={500}>{data.name}</Typography>
                        {data.description && <Typography variant="body2" color="textSecondary">{data.description}</Typography>}
                        {showOrgChip && <Chip
                            sx={{ color: theme.palette.text.secondary }}
                            size="xs"
                            label='inherited from org'
                        />}
                    </Box>
                }
            />
            <Timestamp variant="body2" color="textSecondary" timestamp={data.metadata.updatedAt}></Timestamp>
        </ListItemButton>
    );
}

export default VCSProviderListItem;
