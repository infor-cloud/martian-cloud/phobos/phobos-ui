import { Box } from "@mui/material";
import { useMemo, useState } from "react";
import { useFragment, useMutation } from "react-relay/hooks";
import { useNavigate } from "react-router-dom";
import graphql from "babel-plugin-relay/macro";
import { EditVCSProviderAuthSettingsMutation } from "./__generated__/EditVCSProviderAuthSettingsMutation.graphql";
import { AuthFormData } from "./VCSProviderAuthSettingsForm";
import VCSProviderAuthSettingsForm from "./VCSProviderAuthSettingsForm";
import { MutationError } from "../common/error";
import LoadingButton from "@mui/lab/LoadingButton";
import { Button, Typography } from "@mui/material";
import { useSnackbar } from "notistack";
import { Link as RouterLink } from "react-router-dom";
import { EditVCSProviderAuthSettingsFragment_vcsProvider$key } from "./__generated__/EditVCSProviderAuthSettingsFragment_vcsProvider.graphql";

interface Props {
    fragmentRef: EditVCSProviderAuthSettingsFragment_vcsProvider$key;
}

function EditVCSProviderAuthSettings({ fragmentRef }: Props) {
    const { enqueueSnackbar } = useSnackbar();
    const navigate = useNavigate();

    const vcsProvider = useFragment<EditVCSProviderAuthSettingsFragment_vcsProvider$key>(graphql`
        fragment EditVCSProviderAuthSettingsFragment_vcsProvider on VCSProvider {
            id
            name
            type
            authType
            extraOAuthScopes
            ...VCSProviderAuthSettingsForm_vcsProvider
        }`,
        fragmentRef
    );

    const [commit, isInFlight] = useMutation<EditVCSProviderAuthSettingsMutation>(graphql`
        mutation EditVCSProviderAuthSettingsMutation($input: UpdateVCSProviderInput!) {
            updateVCSProvider(input: $input) {
                vcsProvider{
                    id
                    name
                    ...VCSProviderDetailsFragment_vcsProvider
                }
                problems {
                    message
                    field
                    type
                }
            }
        }
    `);

    const [error, setError] = useState<MutationError>();
    const [formData, setFormData] = useState<AuthFormData>({
        oAuthClientId: undefined,
        oAuthClientSecret: undefined,
        personalAccessToken: undefined,
        extraOAuthScopes: [...vcsProvider.extraOAuthScopes]
    });

    const onUpdate = () => {
        const input: any = {
            id: vcsProvider.id
        };

        if (vcsProvider.authType === 'OAUTH') {
            input.oAuthClientId = formData.oAuthClientId;
            input.oAuthClientSecret = formData.oAuthClientSecret;
            input.extraOAuthScopes = formData.extraOAuthScopes;
        }
        if (vcsProvider.authType === 'ACCESS_TOKEN') {
            input.personalAccessToken = formData.personalAccessToken;
        }

        commit({
            variables: {
                input
            },
            onCompleted: data => {
                if (data.updateVCSProvider.problems.length) {
                    setError({
                        severity: 'warning',
                        message: data.updateVCSProvider.problems.map((problem: any) => problem.message).join('; ')
                    });
                } else if (!data.updateVCSProvider.vcsProvider) {
                    setError({
                        severity: 'error',
                        message: "Unexpected error occurred"
                    });
                }
                else {
                    enqueueSnackbar('Auth settings updated', { variant: 'success' })
                    navigate(-1 as any);
                }
            },
            onError: error => {
                setError({
                    severity: 'error',
                    message: `Unexpected error occurred: ${error.message}`
                });
            }
        });
    };

    const disableButton = useMemo(() => {
        return (vcsProvider.authType === 'OAUTH' && (formData.oAuthClientId === undefined && formData.oAuthClientSecret === undefined && formData.extraOAuthScopes?.length === vcsProvider.extraOAuthScopes.length))
            || (vcsProvider.authType === 'ACCESS_TOKEN' && !formData.personalAccessToken);
    }, [vcsProvider.authType, formData, vcsProvider.extraOAuthScopes.length]);

    return (
        <Box>
            <Typography variant="h5">Edit Authorization Settings</Typography>
            <VCSProviderAuthSettingsForm
                data={formData}
                onChange={(data: any) => setFormData(data)}
                error={error}
                fragmentRef={vcsProvider}
            />
            <Box marginTop={2}>
                <LoadingButton
                    loading={isInFlight}
                    disabled={disableButton}
                    variant="outlined"
                    color="primary"
                    sx={{ marginRight: 2 }}
                    onClick={onUpdate}>
                    Update Auth Settings
                </LoadingButton>
                <Button
                    component={RouterLink}
                    color="inherit"
                    to={-1 as any}>
                    Cancel
                </Button>
            </Box>
        </Box>
    );
}

export default EditVCSProviderAuthSettings;
