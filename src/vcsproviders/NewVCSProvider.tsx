import { useMemo, useState } from 'react';
import { Box, Button, Typography } from '@mui/material';
import LoadingButton from '@mui/lab/LoadingButton';
import { Link as RouterLink, useNavigate } from 'react-router-dom';
import { useMutation } from 'react-relay/hooks';
import { useSnackbar } from 'notistack';
import VCSProviderForm, { FormData } from './VCSProviderForm';
import graphql from 'babel-plugin-relay/macro';
import { MutationError } from '../common/error';
import { NewVCSProviderMutation } from './__generated__/NewVCSProviderMutation.graphql';

interface Props {
    id: string
    scope: 'ORGANIZATION' | 'PROJECT'
    getConnections: (id: string) => string[]
}

function NewVCSProvider({ id, scope, getConnections }: Props) {
    const navigate = useNavigate();
    const { enqueueSnackbar } = useSnackbar();
    const [error, setError] = useState<MutationError>();
    const [formData, setFormData] = useState<FormData>({
        name: '',
        description: '',
        url: '',
        oAuthClientId: '',
        oAuthClientSecret: '',
        extraOAuthScopes: [],
        personalAccessToken: '',
        type: undefined,
        authType: undefined
    });

    const [commit, isInFlight] = useMutation<NewVCSProviderMutation>(graphql`
        mutation NewVCSProviderMutation($input: CreateVCSProviderInput!, $connections: [ID!]!) {
            createVCSProvider(input: $input) {
                # Use @prependNode to add the node to the connection
                vcsProvider @prependNode(connections: $connections, edgeTypeName: "VCSProviderEdge")  {
                    id
                    ...VCSProviderListItemFragment_vcsProvider
                }
                oAuthAuthorizationUrl
                problems {
                    message
                    field
                    type
                }
            }
        }
    `);

    const onSave = () => {
        const input: any = {
            name: formData.name,
            description: formData.description,
            type: formData.type,
            scope,
            authType: formData.authType
        };

        if (scope === 'ORGANIZATION') {
            input.organizationId = id;
        } else {
            input.projectId = id;
        }

        if (formData.authType === 'OAUTH') {
            input.oAuthClientId = formData.oAuthClientId;
            input.oAuthClientSecret = formData.oAuthClientSecret;
            input.extraOAuthScopes = formData.extraOAuthScopes;
        } else {
            input.personalAccessToken = formData.personalAccessToken;
        }

        if (formData.url !== '') {
            input.url = formData.url;
        }

        commit({
            variables: {
                input,
                connections: getConnections(id)
            },
            onCompleted: data => {
                if (data.createVCSProvider.problems.length) {
                    setError({
                        severity: 'warning',
                        message: data.createVCSProvider.problems.map((problem: any) => problem.message).join('; ')
                    });
                } else if (!data.createVCSProvider.vcsProvider) {
                    setError({
                        severity: 'error',
                        message: "Unexpected error occurred"
                    });
                } else {
                    if (data.createVCSProvider.oAuthAuthorizationUrl) {
                        window.open(data.createVCSProvider.oAuthAuthorizationUrl, '_blank')
                    }
                    navigate(`../${data.createVCSProvider.vcsProvider.id}`);
                    enqueueSnackbar('VCS Provider created', { variant: 'success' });
                }
            },
            onError: error => {
                setError({
                    severity: 'error',
                    message: `Unexpected error occurred: ${error.message}`
                });
            }
        });
    };

    const disableButton = useMemo(() => {
        return !formData.name || !formData.type || !formData.authType ||
            (formData.authType === 'OAUTH' && (!formData.oAuthClientId || !formData.oAuthClientSecret)) ||
            (formData.authType === 'ACCESS_TOKEN' && !formData.personalAccessToken);
    }, [formData]);

    return (
        <Box>
            <Typography variant="h5">New VCS Provider</Typography>
            <VCSProviderForm
                data={formData}
                onChange={(data: FormData) => setFormData(data)} error={error} />
            <LoadingButton
                loading={isInFlight}
                disabled={disableButton}
                variant="outlined"
                color="primary"
                sx={{ marginRight: 2 }}
                onClick={onSave}>
                Create VCS Provider
            </LoadingButton>
            <Button component={RouterLink} color="inherit" to={-1 as any}>Cancel</Button>
        </Box>
    );
}

export default NewVCSProvider;
