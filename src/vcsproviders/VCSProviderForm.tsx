import { Alert, Box } from '@mui/material';
import { MutationError } from '../common/error';
import VCSProviderGeneralDetails from './VCSProviderGeneralDetails';
import VCSProviderSetup from './VCSProviderSetup';
import { VCSProviderType } from './__generated__/VCSProviderDetailsFragment_vcsProvider.graphql';

export const getRequiredOAuthScopesForProvider = (type: VCSProviderType) => {
    switch (type) {
        case 'GITHUB':
            return ['repo', 'read:user'];
        case 'GITLAB':
            return ['read_user', 'read_api'];
        default:
            return [];
    }
};

export interface FormData {
    name: string
    description: string
    url: string
    type: VCSProviderType | undefined
    oAuthClientId?: string
    oAuthClientSecret?: string
    extraOAuthScopes?: string[]
    personalAccessToken?: string
    authType?: 'OAUTH' | 'ACCESS_TOKEN' | undefined
}

interface Props {
    data: FormData
    onChange: (data: FormData) => void
    editMode?: boolean
    error?: MutationError
}

function VCSProviderForm({ data, onChange, editMode, error }: Props) {
    return (
        <Box sx={{ mt: 2, mb: 2 }}>
            {error && <Alert sx={{ mt: 2, mb: 2 }} severity={error.severity}>
                {error.message}
            </Alert>}
            <VCSProviderGeneralDetails
                data={data}
                editMode={editMode}
                onChange={(data: FormData) => onChange(data)}
            />
            {(!data.type || editMode) ? null : <VCSProviderSetup
                data={data}
                onChange={(data: FormData) => onChange(data)}
            />}
        </Box>
    );
}

export default VCSProviderForm;
