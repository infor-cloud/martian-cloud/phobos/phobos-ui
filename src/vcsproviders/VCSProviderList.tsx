import { Box, List, Paper, Typography, useTheme } from '@mui/material';
import InfiniteScroll from 'react-infinite-scroll-component';
import ListSkeleton from '../skeletons/ListSkeleton';
import { useFragment } from 'react-relay/hooks';
import graphql from 'babel-plugin-relay/macro';
import VCSProviderListItem from './VCSProviderListItem';
import { VCSProviderListFragment_vcsProviders$key } from './__generated__/VCSProviderListFragment_vcsProviders.graphql';

interface Props {
    search: string | undefined
    isRefreshing: boolean
    loadNext: any
    hasNext: boolean
    showOrgChip?: boolean
    fragmentRef: VCSProviderListFragment_vcsProviders$key
}

function VCSProviderList({ fragmentRef, isRefreshing, loadNext, hasNext, showOrgChip, search }: Props) {
    const theme = useTheme();

    const data = useFragment<VCSProviderListFragment_vcsProviders$key>(graphql`
        fragment VCSProviderListFragment_vcsProviders on VCSProviderConnection {
            totalCount
            edges {
                node {
                    id
                    scope
                    ...VCSProviderListItemFragment_vcsProvider
                }
            }
        }
    `, fragmentRef);

    return (
        <Box>
            <Paper sx={{ borderBottomLeftRadius: 0, borderBottomRightRadius: 0, border: `1px solid ${theme.palette.divider}` }}>
                <Box padding={2} display="flex" alignItems="center" justifyContent="space-between">
                    <Typography variant="subtitle1">
                        {data.totalCount} VCS provider{data.totalCount === 1 ? '' : 's'}
                    </Typography>
                </Box>
            </Paper>
            {(data?.edges?.length === 0) && search !== '' && <Typography
                sx={{
                    padding: 4,
                    borderBottom: `1px solid ${theme.palette.divider}`,
                    borderLeft: `1px solid ${theme.palette.divider}`,
                    borderRight: `1px solid ${theme.palette.divider}`,
                    borderBottomLeftRadius: 4,
                    borderBottomRightRadius: 4
                }}
                align="center"
                color="textSecondary"
            >
                No VCS providers matching search <strong>{search}</strong>
            </Typography>}
            <InfiniteScroll
                dataLength={data?.edges?.length ?? 0}
                next={() => loadNext(20)}
                hasMore={hasNext}
                loader={<ListSkeleton rowCount={3} />}
            >
                <List sx={isRefreshing ? { opacity: 0.5 } : null} disablePadding>
                    {data?.edges?.map((edge: any) => <VCSProviderListItem
                        key={edge.node.id}
                        fragmentRef={edge.node}
                        showOrgChip={showOrgChip && edge.node.scope === 'ORGANIZATION'}
                    />)}
                </List>
            </InfiniteScroll>
        </Box>
    );
}

export default VCSProviderList;
