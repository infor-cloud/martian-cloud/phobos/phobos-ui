import { Box, ListItemButton, ListItemText, Typography, useTheme, Tooltip, Chip } from '@mui/material';
import graphql from 'babel-plugin-relay/macro';
import moment from 'moment';
import { useFragment } from "react-relay/hooks";
import { Link as RouterLink } from 'react-router-dom';
import Gravatar from '../common/Gravatar';
import { PluginVersionListItemFragment_version$key } from './__generated__/PluginVersionListItemFragment_version.graphql';

interface Props {
    fragmentRef: PluginVersionListItemFragment_version$key
}

function PluginVersionListItem(props: Props) {
    const theme = useTheme();

    const data = useFragment<PluginVersionListItemFragment_version$key>(graphql`
        fragment PluginVersionListItemFragment_version on PluginVersion {
            metadata {
                createdAt
            }
            id
            version
            createdBy
            latest
            plugin {
                name
                organizationName
            }
        }
    `, props.fragmentRef);

    return (
        <ListItemButton
            component={RouterLink}
            to={`/plugin-registry/${data.plugin.organizationName}/${data.plugin.name}/${data.version}`}
            sx={{
                borderBottom: `1px solid ${theme.palette.divider}`,
                borderLeft: `1px solid ${theme.palette.divider}`,
                borderRight: `1px solid ${theme.palette.divider}`,
                '&:last-child': {
                    borderBottomLeftRadius: 4,
                    borderBottomRightRadius: 4
                }
            }}>
            <ListItemText
                primary={<Box display="flex" alignItems="center">
                    <Typography>{data.version}</Typography>
                    {data.latest && <Chip size="small" color="secondary" sx={{ marginLeft: 1 }} label="latest" />}
                </Box>} />
            <Box display="flex" alignItems="center">
                <Typography variant="body2" color="textSecondary" sx={{ marginRight: 1 }}>
                    {moment(data.metadata.createdAt as moment.MomentInput).fromNow()} by
                </Typography>
                <Tooltip title={data.createdBy}>
                    <Box>
                        <Gravatar width={20} height={20} email={data.createdBy} />
                    </Box>
                </Tooltip>
            </Box>
        </ListItemButton>
    )
}

export default PluginVersionListItem;
