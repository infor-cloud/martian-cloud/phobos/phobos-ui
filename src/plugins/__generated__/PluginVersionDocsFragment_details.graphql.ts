/**
 * @generated SignedSource<<325963a8f86ab3dafc6ec5bbc4770937>>
 * @lightSyntaxTransform
 * @nogrep
 */

/* tslint:disable */
/* eslint-disable */
// @ts-nocheck

import { Fragment, ReaderFragment } from 'relay-runtime';
export type PluginVersionDocFileCategory = "ACTION" | "GUIDE" | "OVERVIEW" | "%future added value";
import { FragmentRefs } from "relay-runtime";
export type PluginVersionDocsFragment_details$data = {
  readonly docFiles: ReadonlyArray<{
    readonly category: PluginVersionDocFileCategory;
    readonly filePath: string;
  }>;
  readonly id: string;
  readonly " $fragmentSpreads": FragmentRefs<"PluginVersionDocsFileTreeFragment_pluginVersion">;
  readonly " $fragmentType": "PluginVersionDocsFragment_details";
};
export type PluginVersionDocsFragment_details$key = {
  readonly " $data"?: PluginVersionDocsFragment_details$data;
  readonly " $fragmentSpreads": FragmentRefs<"PluginVersionDocsFragment_details">;
};

const node: ReaderFragment = {
  "argumentDefinitions": [],
  "kind": "Fragment",
  "metadata": null,
  "name": "PluginVersionDocsFragment_details",
  "selections": [
    {
      "alias": null,
      "args": null,
      "kind": "ScalarField",
      "name": "id",
      "storageKey": null
    },
    {
      "alias": null,
      "args": null,
      "concreteType": "PluginVersionDocFileMetadata",
      "kind": "LinkedField",
      "name": "docFiles",
      "plural": true,
      "selections": [
        {
          "alias": null,
          "args": null,
          "kind": "ScalarField",
          "name": "category",
          "storageKey": null
        },
        {
          "alias": null,
          "args": null,
          "kind": "ScalarField",
          "name": "filePath",
          "storageKey": null
        }
      ],
      "storageKey": null
    },
    {
      "args": null,
      "kind": "FragmentSpread",
      "name": "PluginVersionDocsFileTreeFragment_pluginVersion"
    }
  ],
  "type": "PluginVersion",
  "abstractKey": null
};

(node as any).hash = "5c25bbcfbdb8913966e9116aeab599c1";

export default node;
