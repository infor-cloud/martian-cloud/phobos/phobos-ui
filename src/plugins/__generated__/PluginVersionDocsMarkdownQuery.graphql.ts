/**
 * @generated SignedSource<<3b86684cf5d1f96f2ecb72c5757a27c8>>
 * @lightSyntaxTransform
 * @nogrep
 */

/* tslint:disable */
/* eslint-disable */
// @ts-nocheck

import { ConcreteRequest, Query } from 'relay-runtime';
export type PluginVersionDocsMarkdownQuery$variables = {
  filePath: string;
  id: string;
};
export type PluginVersionDocsMarkdownQuery$data = {
  readonly pluginDocFile: string | null | undefined;
};
export type PluginVersionDocsMarkdownQuery = {
  response: PluginVersionDocsMarkdownQuery$data;
  variables: PluginVersionDocsMarkdownQuery$variables;
};

const node: ConcreteRequest = (function(){
var v0 = {
  "defaultValue": null,
  "kind": "LocalArgument",
  "name": "filePath"
},
v1 = {
  "defaultValue": null,
  "kind": "LocalArgument",
  "name": "id"
},
v2 = [
  {
    "alias": null,
    "args": [
      {
        "kind": "Variable",
        "name": "filePath",
        "variableName": "filePath"
      },
      {
        "kind": "Variable",
        "name": "pluginVersionId",
        "variableName": "id"
      }
    ],
    "kind": "ScalarField",
    "name": "pluginDocFile",
    "storageKey": null
  }
];
return {
  "fragment": {
    "argumentDefinitions": [
      (v0/*: any*/),
      (v1/*: any*/)
    ],
    "kind": "Fragment",
    "metadata": null,
    "name": "PluginVersionDocsMarkdownQuery",
    "selections": (v2/*: any*/),
    "type": "Query",
    "abstractKey": null
  },
  "kind": "Request",
  "operation": {
    "argumentDefinitions": [
      (v1/*: any*/),
      (v0/*: any*/)
    ],
    "kind": "Operation",
    "name": "PluginVersionDocsMarkdownQuery",
    "selections": (v2/*: any*/)
  },
  "params": {
    "cacheID": "1754714abdddb72b306b0774a3e817b7",
    "id": null,
    "metadata": {},
    "name": "PluginVersionDocsMarkdownQuery",
    "operationKind": "query",
    "text": "query PluginVersionDocsMarkdownQuery(\n  $id: String!\n  $filePath: String!\n) {\n  pluginDocFile(pluginVersionId: $id, filePath: $filePath)\n}\n"
  }
};
})();

(node as any).hash = "62581992e83f95124172b750a6328fc5";

export default node;
