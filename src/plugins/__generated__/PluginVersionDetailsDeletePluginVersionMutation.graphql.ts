/**
 * @generated SignedSource<<5bf758ac29ee239a94e546763130bde5>>
 * @lightSyntaxTransform
 * @nogrep
 */

/* tslint:disable */
/* eslint-disable */
// @ts-nocheck

import { ConcreteRequest, Mutation } from 'relay-runtime';
export type ProblemType = "BAD_REQUEST" | "CONFLICT" | "FORBIDDEN" | "NOT_FOUND" | "%future added value";
export type DeletePluginVersionInput = {
  clientMutationId?: string | null | undefined;
  id: string;
  metadata?: ResourceMetadataInput | null | undefined;
};
export type ResourceMetadataInput = {
  version: string;
};
export type PluginVersionDetailsDeletePluginVersionMutation$variables = {
  input: DeletePluginVersionInput;
};
export type PluginVersionDetailsDeletePluginVersionMutation$data = {
  readonly deletePluginVersion: {
    readonly problems: ReadonlyArray<{
      readonly field: ReadonlyArray<string> | null | undefined;
      readonly message: string;
      readonly type: ProblemType;
    }>;
  };
};
export type PluginVersionDetailsDeletePluginVersionMutation = {
  response: PluginVersionDetailsDeletePluginVersionMutation$data;
  variables: PluginVersionDetailsDeletePluginVersionMutation$variables;
};

const node: ConcreteRequest = (function(){
var v0 = [
  {
    "defaultValue": null,
    "kind": "LocalArgument",
    "name": "input"
  }
],
v1 = [
  {
    "alias": null,
    "args": [
      {
        "kind": "Variable",
        "name": "input",
        "variableName": "input"
      }
    ],
    "concreteType": "DeletePluginVersionPayload",
    "kind": "LinkedField",
    "name": "deletePluginVersion",
    "plural": false,
    "selections": [
      {
        "alias": null,
        "args": null,
        "concreteType": "Problem",
        "kind": "LinkedField",
        "name": "problems",
        "plural": true,
        "selections": [
          {
            "alias": null,
            "args": null,
            "kind": "ScalarField",
            "name": "message",
            "storageKey": null
          },
          {
            "alias": null,
            "args": null,
            "kind": "ScalarField",
            "name": "field",
            "storageKey": null
          },
          {
            "alias": null,
            "args": null,
            "kind": "ScalarField",
            "name": "type",
            "storageKey": null
          }
        ],
        "storageKey": null
      }
    ],
    "storageKey": null
  }
];
return {
  "fragment": {
    "argumentDefinitions": (v0/*: any*/),
    "kind": "Fragment",
    "metadata": null,
    "name": "PluginVersionDetailsDeletePluginVersionMutation",
    "selections": (v1/*: any*/),
    "type": "Mutation",
    "abstractKey": null
  },
  "kind": "Request",
  "operation": {
    "argumentDefinitions": (v0/*: any*/),
    "kind": "Operation",
    "name": "PluginVersionDetailsDeletePluginVersionMutation",
    "selections": (v1/*: any*/)
  },
  "params": {
    "cacheID": "e08967ed026adcc9d45c3ffc0e864212",
    "id": null,
    "metadata": {},
    "name": "PluginVersionDetailsDeletePluginVersionMutation",
    "operationKind": "mutation",
    "text": "mutation PluginVersionDetailsDeletePluginVersionMutation(\n  $input: DeletePluginVersionInput!\n) {\n  deletePluginVersion(input: $input) {\n    problems {\n      message\n      field\n      type\n    }\n  }\n}\n"
  }
};
})();

(node as any).hash = "a40a01377bafb597bb6d7ffed07d181f";

export default node;
