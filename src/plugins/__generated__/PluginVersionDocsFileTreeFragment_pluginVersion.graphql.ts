/**
 * @generated SignedSource<<1dacd9bf29ca8b73013ae7420d76d957>>
 * @lightSyntaxTransform
 * @nogrep
 */

/* tslint:disable */
/* eslint-disable */
// @ts-nocheck

import { Fragment, ReaderFragment } from 'relay-runtime';
export type PluginVersionDocFileCategory = "ACTION" | "GUIDE" | "OVERVIEW" | "%future added value";
import { FragmentRefs } from "relay-runtime";
export type PluginVersionDocsFileTreeFragment_pluginVersion$data = {
  readonly docFiles: ReadonlyArray<{
    readonly category: PluginVersionDocFileCategory;
    readonly filePath: string;
    readonly name: string;
    readonly subcategory: string;
    readonly title: string;
  }>;
  readonly " $fragmentType": "PluginVersionDocsFileTreeFragment_pluginVersion";
};
export type PluginVersionDocsFileTreeFragment_pluginVersion$key = {
  readonly " $data"?: PluginVersionDocsFileTreeFragment_pluginVersion$data;
  readonly " $fragmentSpreads": FragmentRefs<"PluginVersionDocsFileTreeFragment_pluginVersion">;
};

const node: ReaderFragment = {
  "argumentDefinitions": [],
  "kind": "Fragment",
  "metadata": null,
  "name": "PluginVersionDocsFileTreeFragment_pluginVersion",
  "selections": [
    {
      "alias": null,
      "args": null,
      "concreteType": "PluginVersionDocFileMetadata",
      "kind": "LinkedField",
      "name": "docFiles",
      "plural": true,
      "selections": [
        {
          "alias": null,
          "args": null,
          "kind": "ScalarField",
          "name": "name",
          "storageKey": null
        },
        {
          "alias": null,
          "args": null,
          "kind": "ScalarField",
          "name": "title",
          "storageKey": null
        },
        {
          "alias": null,
          "args": null,
          "kind": "ScalarField",
          "name": "category",
          "storageKey": null
        },
        {
          "alias": null,
          "args": null,
          "kind": "ScalarField",
          "name": "subcategory",
          "storageKey": null
        },
        {
          "alias": null,
          "args": null,
          "kind": "ScalarField",
          "name": "filePath",
          "storageKey": null
        }
      ],
      "storageKey": null
    }
  ],
  "type": "PluginVersion",
  "abstractKey": null
};

(node as any).hash = "5e95f06263de5f842f9c7c9f36b5a5e3";

export default node;
