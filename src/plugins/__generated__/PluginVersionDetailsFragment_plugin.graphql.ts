/**
 * @generated SignedSource<<eae8344f292b6434aa0c22ca75bb59d8>>
 * @lightSyntaxTransform
 * @nogrep
 */

/* tslint:disable */
/* eslint-disable */
// @ts-nocheck

import { Fragment, ReaderFragment } from 'relay-runtime';
import { FragmentRefs } from "relay-runtime";
export type PluginVersionDetailsFragment_plugin$data = {
  readonly " $fragmentSpreads": FragmentRefs<"PluginVersionDetailsIndexFragment_details">;
  readonly " $fragmentType": "PluginVersionDetailsFragment_plugin";
};
export type PluginVersionDetailsFragment_plugin$key = {
  readonly " $data"?: PluginVersionDetailsFragment_plugin$data;
  readonly " $fragmentSpreads": FragmentRefs<"PluginVersionDetailsFragment_plugin">;
};

const node: ReaderFragment = {
  "argumentDefinitions": [],
  "kind": "Fragment",
  "metadata": null,
  "name": "PluginVersionDetailsFragment_plugin",
  "selections": [
    {
      "args": null,
      "kind": "FragmentSpread",
      "name": "PluginVersionDetailsIndexFragment_details"
    }
  ],
  "type": "PluginVersion",
  "abstractKey": null
};

(node as any).hash = "6e0063b827adfebc864d6872acdc3a7a";

export default node;
