/**
 * @generated SignedSource<<8d5c14ef2310b61a72775b83038509fb>>
 * @lightSyntaxTransform
 * @nogrep
 */

/* tslint:disable */
/* eslint-disable */
// @ts-nocheck

import { Fragment, ReaderFragment } from 'relay-runtime';
import { FragmentRefs } from "relay-runtime";
export type PluginVersionListFragment_plugin$data = {
  readonly id: string;
  readonly " $fragmentType": "PluginVersionListFragment_plugin";
};
export type PluginVersionListFragment_plugin$key = {
  readonly " $data"?: PluginVersionListFragment_plugin$data;
  readonly " $fragmentSpreads": FragmentRefs<"PluginVersionListFragment_plugin">;
};

const node: ReaderFragment = {
  "argumentDefinitions": [],
  "kind": "Fragment",
  "metadata": null,
  "name": "PluginVersionListFragment_plugin",
  "selections": [
    {
      "alias": null,
      "args": null,
      "kind": "ScalarField",
      "name": "id",
      "storageKey": null
    }
  ],
  "type": "Plugin",
  "abstractKey": null
};

(node as any).hash = "719fcdbdb31fc6d46690d196dc0c6b00";

export default node;
