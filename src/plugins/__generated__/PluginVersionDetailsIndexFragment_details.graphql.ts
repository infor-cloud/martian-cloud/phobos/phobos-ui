/**
 * @generated SignedSource<<89b8d29c54e9cf77550cc550b7da35e9>>
 * @lightSyntaxTransform
 * @nogrep
 */

/* tslint:disable */
/* eslint-disable */
// @ts-nocheck

import { Fragment, ReaderFragment } from 'relay-runtime';
import { FragmentRefs } from "relay-runtime";
export type PluginVersionDetailsIndexFragment_details$data = {
  readonly id: string;
  readonly plugin: {
    readonly id: string;
    readonly name: string;
    readonly organizationName: string;
    readonly private: boolean;
    readonly " $fragmentSpreads": FragmentRefs<"PluginVersionListFragment_plugin">;
  };
  readonly shaSumsUploaded: boolean;
  readonly version: string;
  readonly " $fragmentSpreads": FragmentRefs<"PluginVersionDetailsSidebarFragment_details" | "PluginVersionDocsFragment_details">;
  readonly " $fragmentType": "PluginVersionDetailsIndexFragment_details";
};
export type PluginVersionDetailsIndexFragment_details$key = {
  readonly " $data"?: PluginVersionDetailsIndexFragment_details$data;
  readonly " $fragmentSpreads": FragmentRefs<"PluginVersionDetailsIndexFragment_details">;
};

const node: ReaderFragment = (function(){
var v0 = {
  "alias": null,
  "args": null,
  "kind": "ScalarField",
  "name": "id",
  "storageKey": null
};
return {
  "argumentDefinitions": [],
  "kind": "Fragment",
  "metadata": null,
  "name": "PluginVersionDetailsIndexFragment_details",
  "selections": [
    (v0/*: any*/),
    {
      "alias": null,
      "args": null,
      "kind": "ScalarField",
      "name": "version",
      "storageKey": null
    },
    {
      "alias": null,
      "args": null,
      "kind": "ScalarField",
      "name": "shaSumsUploaded",
      "storageKey": null
    },
    {
      "alias": null,
      "args": null,
      "concreteType": "Plugin",
      "kind": "LinkedField",
      "name": "plugin",
      "plural": false,
      "selections": [
        (v0/*: any*/),
        {
          "alias": null,
          "args": null,
          "kind": "ScalarField",
          "name": "name",
          "storageKey": null
        },
        {
          "alias": null,
          "args": null,
          "kind": "ScalarField",
          "name": "organizationName",
          "storageKey": null
        },
        {
          "alias": null,
          "args": null,
          "kind": "ScalarField",
          "name": "private",
          "storageKey": null
        },
        {
          "args": null,
          "kind": "FragmentSpread",
          "name": "PluginVersionListFragment_plugin"
        }
      ],
      "storageKey": null
    },
    {
      "args": null,
      "kind": "FragmentSpread",
      "name": "PluginVersionDetailsSidebarFragment_details"
    },
    {
      "args": null,
      "kind": "FragmentSpread",
      "name": "PluginVersionDocsFragment_details"
    }
  ],
  "type": "PluginVersion",
  "abstractKey": null
};
})();

(node as any).hash = "0b5c9cfad9ef7c4c31ff4c65f681dc10";

export default node;
