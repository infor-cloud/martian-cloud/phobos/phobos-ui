/**
 * @generated SignedSource<<ea04603461ef530030ee1ebdb63da6f0>>
 * @lightSyntaxTransform
 * @nogrep
 */

/* tslint:disable */
/* eslint-disable */
// @ts-nocheck

import { Fragment, ReaderFragment } from 'relay-runtime';
import { FragmentRefs } from "relay-runtime";
export type PluginVersionDetailsSidebarFragment_details$data = {
  readonly createdBy: string;
  readonly latest: boolean;
  readonly metadata: {
    readonly createdAt: any;
    readonly prn: string;
  };
  readonly platforms: ReadonlyArray<{
    readonly arch: string;
    readonly binaryUploaded: boolean;
    readonly id: string;
    readonly os: string;
  }>;
  readonly plugin: {
    readonly id: string;
    readonly name: string;
    readonly organizationName: string;
    readonly private: boolean;
    readonly repositoryUrl: string;
  };
  readonly protocols: ReadonlyArray<string>;
  readonly version: string;
  readonly " $fragmentType": "PluginVersionDetailsSidebarFragment_details";
};
export type PluginVersionDetailsSidebarFragment_details$key = {
  readonly " $data"?: PluginVersionDetailsSidebarFragment_details$data;
  readonly " $fragmentSpreads": FragmentRefs<"PluginVersionDetailsSidebarFragment_details">;
};

const node: ReaderFragment = (function(){
var v0 = {
  "alias": null,
  "args": null,
  "kind": "ScalarField",
  "name": "id",
  "storageKey": null
};
return {
  "argumentDefinitions": [],
  "kind": "Fragment",
  "metadata": null,
  "name": "PluginVersionDetailsSidebarFragment_details",
  "selections": [
    {
      "alias": null,
      "args": null,
      "kind": "ScalarField",
      "name": "version",
      "storageKey": null
    },
    {
      "alias": null,
      "args": null,
      "kind": "ScalarField",
      "name": "createdBy",
      "storageKey": null
    },
    {
      "alias": null,
      "args": null,
      "kind": "ScalarField",
      "name": "protocols",
      "storageKey": null
    },
    {
      "alias": null,
      "args": null,
      "kind": "ScalarField",
      "name": "latest",
      "storageKey": null
    },
    {
      "alias": null,
      "args": null,
      "concreteType": "PluginPlatform",
      "kind": "LinkedField",
      "name": "platforms",
      "plural": true,
      "selections": [
        (v0/*: any*/),
        {
          "alias": null,
          "args": null,
          "kind": "ScalarField",
          "name": "os",
          "storageKey": null
        },
        {
          "alias": null,
          "args": null,
          "kind": "ScalarField",
          "name": "arch",
          "storageKey": null
        },
        {
          "alias": null,
          "args": null,
          "kind": "ScalarField",
          "name": "binaryUploaded",
          "storageKey": null
        }
      ],
      "storageKey": null
    },
    {
      "alias": null,
      "args": null,
      "concreteType": "ResourceMetadata",
      "kind": "LinkedField",
      "name": "metadata",
      "plural": false,
      "selections": [
        {
          "alias": null,
          "args": null,
          "kind": "ScalarField",
          "name": "createdAt",
          "storageKey": null
        },
        {
          "alias": null,
          "args": null,
          "kind": "ScalarField",
          "name": "prn",
          "storageKey": null
        }
      ],
      "storageKey": null
    },
    {
      "alias": null,
      "args": null,
      "concreteType": "Plugin",
      "kind": "LinkedField",
      "name": "plugin",
      "plural": false,
      "selections": [
        (v0/*: any*/),
        {
          "alias": null,
          "args": null,
          "kind": "ScalarField",
          "name": "name",
          "storageKey": null
        },
        {
          "alias": null,
          "args": null,
          "kind": "ScalarField",
          "name": "organizationName",
          "storageKey": null
        },
        {
          "alias": null,
          "args": null,
          "kind": "ScalarField",
          "name": "private",
          "storageKey": null
        },
        {
          "alias": null,
          "args": null,
          "kind": "ScalarField",
          "name": "repositoryUrl",
          "storageKey": null
        }
      ],
      "storageKey": null
    }
  ],
  "type": "PluginVersion",
  "abstractKey": null
};
})();

(node as any).hash = "7cba16522c98969b5dcc9a4e1dcd56ea";

export default node;
