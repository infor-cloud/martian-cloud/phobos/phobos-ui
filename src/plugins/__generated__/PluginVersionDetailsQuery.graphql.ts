/**
 * @generated SignedSource<<7c1a6b0270dd0d2b5883f1c8df72b7c1>>
 * @lightSyntaxTransform
 * @nogrep
 */

/* tslint:disable */
/* eslint-disable */
// @ts-nocheck

import { ConcreteRequest, Query } from 'relay-runtime';
import { FragmentRefs } from "relay-runtime";
export type PluginVersionDetailsQuery$variables = {
  prn: string;
};
export type PluginVersionDetailsQuery$data = {
  readonly node: {
    readonly " $fragmentSpreads": FragmentRefs<"PluginVersionDetailsFragment_plugin">;
  } | null | undefined;
};
export type PluginVersionDetailsQuery = {
  response: PluginVersionDetailsQuery$data;
  variables: PluginVersionDetailsQuery$variables;
};

const node: ConcreteRequest = (function(){
var v0 = [
  {
    "defaultValue": null,
    "kind": "LocalArgument",
    "name": "prn"
  }
],
v1 = [
  {
    "kind": "Variable",
    "name": "id",
    "variableName": "prn"
  }
],
v2 = {
  "alias": null,
  "args": null,
  "kind": "ScalarField",
  "name": "id",
  "storageKey": null
},
v3 = {
  "alias": null,
  "args": null,
  "kind": "ScalarField",
  "name": "name",
  "storageKey": null
};
return {
  "fragment": {
    "argumentDefinitions": (v0/*: any*/),
    "kind": "Fragment",
    "metadata": null,
    "name": "PluginVersionDetailsQuery",
    "selections": [
      {
        "alias": null,
        "args": (v1/*: any*/),
        "concreteType": null,
        "kind": "LinkedField",
        "name": "node",
        "plural": false,
        "selections": [
          {
            "kind": "InlineFragment",
            "selections": [
              {
                "args": null,
                "kind": "FragmentSpread",
                "name": "PluginVersionDetailsFragment_plugin"
              }
            ],
            "type": "PluginVersion",
            "abstractKey": null
          }
        ],
        "storageKey": null
      }
    ],
    "type": "Query",
    "abstractKey": null
  },
  "kind": "Request",
  "operation": {
    "argumentDefinitions": (v0/*: any*/),
    "kind": "Operation",
    "name": "PluginVersionDetailsQuery",
    "selections": [
      {
        "alias": null,
        "args": (v1/*: any*/),
        "concreteType": null,
        "kind": "LinkedField",
        "name": "node",
        "plural": false,
        "selections": [
          {
            "alias": null,
            "args": null,
            "kind": "ScalarField",
            "name": "__typename",
            "storageKey": null
          },
          (v2/*: any*/),
          {
            "kind": "InlineFragment",
            "selections": [
              {
                "alias": null,
                "args": null,
                "kind": "ScalarField",
                "name": "version",
                "storageKey": null
              },
              {
                "alias": null,
                "args": null,
                "kind": "ScalarField",
                "name": "shaSumsUploaded",
                "storageKey": null
              },
              {
                "alias": null,
                "args": null,
                "concreteType": "Plugin",
                "kind": "LinkedField",
                "name": "plugin",
                "plural": false,
                "selections": [
                  (v2/*: any*/),
                  (v3/*: any*/),
                  {
                    "alias": null,
                    "args": null,
                    "kind": "ScalarField",
                    "name": "organizationName",
                    "storageKey": null
                  },
                  {
                    "alias": null,
                    "args": null,
                    "kind": "ScalarField",
                    "name": "private",
                    "storageKey": null
                  },
                  {
                    "alias": null,
                    "args": null,
                    "kind": "ScalarField",
                    "name": "repositoryUrl",
                    "storageKey": null
                  }
                ],
                "storageKey": null
              },
              {
                "alias": null,
                "args": null,
                "kind": "ScalarField",
                "name": "createdBy",
                "storageKey": null
              },
              {
                "alias": null,
                "args": null,
                "kind": "ScalarField",
                "name": "protocols",
                "storageKey": null
              },
              {
                "alias": null,
                "args": null,
                "kind": "ScalarField",
                "name": "latest",
                "storageKey": null
              },
              {
                "alias": null,
                "args": null,
                "concreteType": "PluginPlatform",
                "kind": "LinkedField",
                "name": "platforms",
                "plural": true,
                "selections": [
                  (v2/*: any*/),
                  {
                    "alias": null,
                    "args": null,
                    "kind": "ScalarField",
                    "name": "os",
                    "storageKey": null
                  },
                  {
                    "alias": null,
                    "args": null,
                    "kind": "ScalarField",
                    "name": "arch",
                    "storageKey": null
                  },
                  {
                    "alias": null,
                    "args": null,
                    "kind": "ScalarField",
                    "name": "binaryUploaded",
                    "storageKey": null
                  }
                ],
                "storageKey": null
              },
              {
                "alias": null,
                "args": null,
                "concreteType": "ResourceMetadata",
                "kind": "LinkedField",
                "name": "metadata",
                "plural": false,
                "selections": [
                  {
                    "alias": null,
                    "args": null,
                    "kind": "ScalarField",
                    "name": "createdAt",
                    "storageKey": null
                  },
                  {
                    "alias": null,
                    "args": null,
                    "kind": "ScalarField",
                    "name": "prn",
                    "storageKey": null
                  }
                ],
                "storageKey": null
              },
              {
                "alias": null,
                "args": null,
                "concreteType": "PluginVersionDocFileMetadata",
                "kind": "LinkedField",
                "name": "docFiles",
                "plural": true,
                "selections": [
                  {
                    "alias": null,
                    "args": null,
                    "kind": "ScalarField",
                    "name": "category",
                    "storageKey": null
                  },
                  {
                    "alias": null,
                    "args": null,
                    "kind": "ScalarField",
                    "name": "filePath",
                    "storageKey": null
                  },
                  (v3/*: any*/),
                  {
                    "alias": null,
                    "args": null,
                    "kind": "ScalarField",
                    "name": "title",
                    "storageKey": null
                  },
                  {
                    "alias": null,
                    "args": null,
                    "kind": "ScalarField",
                    "name": "subcategory",
                    "storageKey": null
                  }
                ],
                "storageKey": null
              }
            ],
            "type": "PluginVersion",
            "abstractKey": null
          }
        ],
        "storageKey": null
      }
    ]
  },
  "params": {
    "cacheID": "df2b3d88d8e3bfc64470286919b8cf8e",
    "id": null,
    "metadata": {},
    "name": "PluginVersionDetailsQuery",
    "operationKind": "query",
    "text": "query PluginVersionDetailsQuery(\n  $prn: String!\n) {\n  node(id: $prn) {\n    __typename\n    ... on PluginVersion {\n      ...PluginVersionDetailsFragment_plugin\n    }\n    id\n  }\n}\n\nfragment PluginVersionDetailsFragment_plugin on PluginVersion {\n  ...PluginVersionDetailsIndexFragment_details\n}\n\nfragment PluginVersionDetailsIndexFragment_details on PluginVersion {\n  id\n  version\n  shaSumsUploaded\n  plugin {\n    id\n    name\n    organizationName\n    private\n    ...PluginVersionListFragment_plugin\n  }\n  ...PluginVersionDetailsSidebarFragment_details\n  ...PluginVersionDocsFragment_details\n}\n\nfragment PluginVersionDetailsSidebarFragment_details on PluginVersion {\n  version\n  createdBy\n  protocols\n  latest\n  platforms {\n    id\n    os\n    arch\n    binaryUploaded\n  }\n  metadata {\n    createdAt\n    prn\n  }\n  plugin {\n    id\n    name\n    organizationName\n    private\n    repositoryUrl\n  }\n}\n\nfragment PluginVersionDocsFileTreeFragment_pluginVersion on PluginVersion {\n  docFiles {\n    name\n    title\n    category\n    subcategory\n    filePath\n  }\n}\n\nfragment PluginVersionDocsFragment_details on PluginVersion {\n  id\n  docFiles {\n    category\n    filePath\n  }\n  ...PluginVersionDocsFileTreeFragment_pluginVersion\n}\n\nfragment PluginVersionListFragment_plugin on Plugin {\n  id\n}\n"
  }
};
})();

(node as any).hash = "0bb2f868286cee14699ae0d91f001a17";

export default node;
