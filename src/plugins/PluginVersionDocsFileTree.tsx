import ChevronRightIcon from '@mui/icons-material/ChevronRight';
import ExpandMoreIcon from '@mui/icons-material/ExpandMore';
import { Typography } from "@mui/material";
import { TreeItem } from '@mui/x-tree-view/TreeItem';
import { TreeView } from '@mui/x-tree-view/TreeView';
import graphql from "babel-plugin-relay/macro";
import { useMemo } from "react";
import { useFragment } from "react-relay/hooks";
import { PluginVersionDocsFileTreeFragment_pluginVersion$key } from "./__generated__/PluginVersionDocsFileTreeFragment_pluginVersion.graphql";

interface TreeNode {
  id: string;
  label: string;
  children: TreeNode[];
}

interface Props {
  fragmentRef: PluginVersionDocsFileTreeFragment_pluginVersion$key;
  selectedFilePath: string;
  onFileSelected: (filePath: string) => void;
}

interface DocTreeItemProps {
  node: TreeNode;
}

function DocTreeItem({ node }: DocTreeItemProps) {
  return (
    <TreeItem
      sx={{
        '& .MuiTreeItem-content': {
          flexDirection: 'row-reverse'
        }
      }}
      nodeId={node.id}
      label={< Typography variant="subtitle1" > {node.label}</Typography >}
    >
      {node.children.map((child) => <DocTreeItem key={child.id} node={child} />)}
    </TreeItem >
  );
}

function PluginVersionDocsFileTree({ fragmentRef, selectedFilePath, onFileSelected }: Props) {
  const data = useFragment<PluginVersionDocsFileTreeFragment_pluginVersion$key>(
    graphql`
      fragment PluginVersionDocsFileTreeFragment_pluginVersion on PluginVersion {
        docFiles {
          name
          title
          category
          subcategory
          filePath
        }
      }
    `,
    fragmentRef
  );

  const nodes = useMemo<TreeNode[]>(() => {
    const treeNodes: TreeNode[] = [];
    const nodeMap = {} as any;

    data.docFiles.forEach((docFile) => {
      // Overview is a special case
      if (docFile.category === 'OVERVIEW') {
        treeNodes.push({
          id: docFile.filePath,
          label: 'Overview',
          children: []
        });
      } else {
        if (!nodeMap[docFile.category]) {
          nodeMap[docFile.category] = {
            subcategories: {},
            nodes: []
          };
        }

        const node = {
          id: docFile.filePath,
          label: docFile.category === 'GUIDE' ? docFile.title : docFile.name,
          children: []
        };

        const topLevelNode = nodeMap[docFile.category];
        if (docFile.subcategory) {
          // Check if subcategory node has been created
          if (!topLevelNode.subcategories[docFile.subcategory]) {
            const subcategoryId = `${docFile.category}/${docFile.subcategory}`;
            topLevelNode.subcategories[docFile.subcategory] = {
              id: subcategoryId,
              label: docFile.subcategory,
              children: []
            };
          }

          // Add node to subcategory
          topLevelNode.subcategories[docFile.subcategory].children.push(node);
        } else {
          // Add node to top level
          topLevelNode.nodes.push(node);
        }
      }
    });

    if (nodeMap['ACTION']) {
      treeNodes.push({
        id: "actions",
        label: "Actions",
        children: [...Object.values(nodeMap['ACTION'].subcategories), ...nodeMap['ACTION'].nodes]
      });
    }

    if (nodeMap['GUIDE']) {
      treeNodes.push({
        id: "guides",
        label: "Guides",
        children: [...Object.values(nodeMap['GUIDE'].subcategories), ...nodeMap['GUIDE'].nodes]
      });
    }

    return treeNodes;
  }, [data]);

  const onNodeSelected = (_: React.SyntheticEvent, nodeId: string) => {
    const docFile = data.docFiles.find(docFile => docFile.filePath === nodeId);
    if (docFile) {
      onFileSelected(docFile.filePath);
    }
  };

  return (
    <TreeView
      aria-label="plugin doc file tree"
      defaultCollapseIcon={<ExpandMoreIcon />}
      defaultExpandIcon={<ChevronRightIcon />}
      onNodeSelect={onNodeSelected}
      defaultSelected={selectedFilePath}
      defaultExpanded={nodes.map(node => node.id)}
    >
      {nodes.map((node: TreeNode) => <DocTreeItem key={node.id} node={node} />)}
    </TreeView>
  );
}

export default PluginVersionDocsFileTree;
