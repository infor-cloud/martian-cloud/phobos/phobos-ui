import Box from "@mui/material/Box";
import graphql from "babel-plugin-relay/macro";
import { useLazyLoadQuery } from "react-relay/hooks";
import remarkGfm from 'remark-gfm';
import MuiMarkdown from '../common/Markdown';
import { PluginVersionDocsMarkdownQuery } from "./__generated__/PluginVersionDocsMarkdownQuery.graphql";

interface Props {
  pluginVersionId: string;
  filePath: string;
}

function PluginVersionDocsMarkdown({ pluginVersionId, filePath }: Props) {
  const queryData = useLazyLoadQuery<PluginVersionDocsMarkdownQuery>(graphql`
        query PluginVersionDocsMarkdownQuery($id: String! $filePath: String!) {
          pluginDocFile(pluginVersionId: $id, filePath: $filePath)
        }
    `, { id: pluginVersionId, filePath });

  return (
    <Box>
      <MuiMarkdown
        remarkPlugins={[remarkGfm]}
        children={queryData.pluginDocFile}
      />
    </Box>
  );
}

export default PluginVersionDocsMarkdown;
