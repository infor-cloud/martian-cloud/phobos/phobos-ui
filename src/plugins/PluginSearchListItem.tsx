import { Box, Chip, ListItemIcon, Tooltip, Typography } from "@mui/material";
import Link from "@mui/material/Link";
import ListItemButton from "@mui/material/ListItemButton";
import { useTheme } from "@mui/material/styles";
import graphql from "babel-plugin-relay/macro";
import ExtensionIcon from "@mui/icons-material/Extension";
import moment from "moment";
import { useFragment } from "react-relay/hooks";
import { Link as LinkRouter } from "react-router-dom";
import Gravatar from "../common/Gravatar";
import { PluginSearchListItemFragment_plugin$key } from "./__generated__/PluginSearchListItemFragment_plugin.graphql";

interface Props {
  fragmentRef: PluginSearchListItemFragment_plugin$key;
}

function PluginSearchListItem(props: Props) {
  const theme = useTheme();

  const data = useFragment<PluginSearchListItemFragment_plugin$key>(
    graphql`
      fragment PluginSearchListItemFragment_plugin on Plugin {
        id
        name
        organizationName
        private
        latestVersion {
          version
          createdBy
          metadata {
            createdAt
          }
        }
      }
    `,
    props.fragmentRef
  );

  const buttonLink = data.latestVersion ? `/plugin-registry/${data.organizationName}/${data.name}/${data.latestVersion.version}` : "/plugin-registry"

  return (
    <ListItemButton
      component={LinkRouter}
      to={buttonLink}
      sx={{
        borderBottom: `1px solid ${theme.palette.divider}`,
        borderLeft: `1px solid ${theme.palette.divider}`,
        borderRight: `1px solid ${theme.palette.divider}`,
        "&:last-child": {
          borderBottomLeftRadius: 4,
          borderBottomRightRadius: 4,
        },
      }}
    >
      <ListItemIcon sx={{ minWidth: 40 }}>
        <ExtensionIcon color="disabled" />
      </ListItemIcon>
      <Box
        flex={1}
        display="flex"
        justifyContent="space-between"
        alignItems="center"
      >
        <Box>
          <Link
            component="div"
            underline="hover"
            variant="body1"
            color="textPrimary"
            sx={{ fontWeight: "500" }}
          >
            {data.organizationName}/{data.name}
          </Link>
          <Box>
            {data.latestVersion && (
              <Box display="flex" alignItems="center">
                <Typography variant="body2" color="textSecondary">
                  {data.latestVersion.version} published{" "}
                  {moment(
                    data.latestVersion.metadata.createdAt as moment.MomentInput
                  ).fromNow()}{" "}
                  by
                </Typography>
                <Tooltip title={data.latestVersion.createdBy}>
                  <Box>
                    <Gravatar
                      width={16}
                      height={16}
                      sx={{ marginLeft: 1, marginRight: 1 }}
                      email={data.latestVersion.createdBy}
                    />
                  </Box>
                </Tooltip>
              </Box>
            )}
            {!data.latestVersion && (
              <Typography variant="body2" color="textSecondary">
                No versions published
              </Typography>
            )}
          </Box>
        </Box>
        {data.private && (
          <Chip
            sx={{ marginLeft: 2 }}
            variant="outlined"
            color="warning"
            size="small"
            label="private"
          />
        )}
      </Box>
    </ListItemButton>
  );
}

export default PluginSearchListItem;
