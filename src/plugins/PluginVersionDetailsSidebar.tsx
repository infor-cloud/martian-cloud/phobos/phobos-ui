import {
  Chip,
  Link,
  styled,
  Toolbar,
  Tooltip,
  Typography,
  useTheme,
  TypographyProps,
  IconButton,
} from "@mui/material";
import { useMemo } from "react";
import Box from "@mui/material/Box";
import InfoIcon from '@mui/icons-material/Info';
import CopyIcon from '@mui/icons-material/ContentCopy';
import MuiDrawer, { DrawerProps } from "@mui/material/Drawer";
import graphql from "babel-plugin-relay/macro";
import moment from "moment";
import { useFragment } from "react-relay/hooks";
import Gravatar from "../common/Gravatar";
import { PluginVersionDetailsSidebarFragment_details$key } from "./__generated__/PluginVersionDetailsSidebarFragment_details.graphql";

interface Props {
  fragmentRef: PluginVersionDetailsSidebarFragment_details$key;
  open: boolean;
  temporary: boolean;
  onClose: () => void;
}

export const SidebarWidth = 400;

const Section = styled(Box)(() => ({
  marginBottom: 24,
}));

const FieldLabel = styled(({ ...props }: TypographyProps) => (
  <Typography color="textSecondary" variant="subtitle2" {...props} />
))(() => ({
  fontSize: 16,
  marginBottom: 1,
}));

const Drawer = styled(MuiDrawer)<DrawerProps>(() => ({
  flexShrink: 0,
  overflowX: "hidden",
  [`& .MuiDrawer-paper`]: {
    overflowX: "hidden",
    width: SidebarWidth,
    boxSizing: "border-box",
  },
  width: SidebarWidth,
}));

function PluginVersionDetailsSidebar(props: Props) {
  const { open, temporary, onClose } = props;
  const theme = useTheme();

  const data = useFragment<PluginVersionDetailsSidebarFragment_details$key>(
    graphql`
      fragment PluginVersionDetailsSidebarFragment_details on PluginVersion {
        version
        createdBy
        protocols
        latest
        platforms {
          id
          os
          arch
          binaryUploaded
        }
        metadata {
          createdAt
          prn
        }
        plugin {
          id
          name
          organizationName
          private
          repositoryUrl
        }
      }
    `,
    props.fragmentRef
  );

  const filteredPlatforms = useMemo(() => {
    return data.platforms
      .filter((platform) => platform.binaryUploaded)
      .map((platform) => `${platform.os}_${platform.arch}`)
      .sort();
  }, [data.platforms]);

  return (
    <Drawer
      variant={temporary ? "temporary" : "permanent"}
      open={open}
      hideBackdrop={false}
      anchor="right"
      onClose={onClose}
    >
      <Toolbar />
      <Box padding={2}>
        <Section>
          <FieldLabel>Version</FieldLabel>
          <Box display="flex" alignItems="center">
            <Typography>{data.version}</Typography>
            {data.latest && (
              <Chip
                size="small"
                color="secondary"
                sx={{ marginLeft: 1 }}
                label="latest"
              />
            )}
          </Box>
        </Section>
        {data && (
          <Section>
            <FieldLabel>Published</FieldLabel>
            <Box display="flex" alignItems="center">
              <Tooltip title={data.metadata.createdAt as string}>
                <Typography sx={{ marginRight: 1 }}>
                  {moment(
                    data.metadata.createdAt as moment.MomentInput
                  ).fromNow()}{" "}
                  by
                </Typography>
              </Tooltip>
              <Tooltip title={data.createdBy}>
                <Box>
                  <Gravatar width={20} height={20} email={data.createdBy} />
                </Box>
              </Tooltip>
            </Box>
          </Section>
        )}
        <Section>
          <FieldLabel>Source</FieldLabel>
          <Typography>
            {data.plugin.organizationName}/{data.plugin.name}
          </Typography>
        </Section>
        {data && (
          <Section>
            <FieldLabel>Phobos Resource Name</FieldLabel>
            <Box sx={{ wordWrap: "break-word" }}>
              <Typography>{data.metadata.prn}
                <IconButton
                  sx={{
                    padding: '0px',
                    marginLeft: '10px',
                    opacity: '20%',
                    transition: 'ease',
                    transitionDuration: '300ms',
                    ":hover": {
                      opacity: '100%'
                    }
                  }}
                  onClick={() => navigator.clipboard.writeText(data?.metadata.prn)}
                  title="Copy PRN"
                >
                  <CopyIcon sx={{ width: 16, height: 16 }} />
                </IconButton>
              </Typography>
            </Box>
          </Section>
        )}
        <Section>
          <FieldLabel>Repository</FieldLabel>
          {data.plugin.repositoryUrl && (
            <Typography
              component="p"
              noWrap
              sx={{ color: theme.palette.primary.main }}
            >
              <Link noWrap underline="hover" color="secondary" href={data.plugin.repositoryUrl}>
                <Typography component="span" noWrap>
                  {data.plugin.repositoryUrl}
                </Typography>
              </Link>
            </Typography>
          )}
          {!data.plugin.repositoryUrl && <Typography>None</Typography>}
        </Section>
        <Section>
          <FieldLabel>
            Protocols
            <Tooltip title="Protocols are the Phobos API versions that are supported by this plugin used to determine compatibility.">
              <InfoIcon sx={{
                width: 16,
                height: 16,
                marginLeft: '10px',
                verticalAlign: 'middle',
                opacity: '20%',
                transition: 'ease',
                transitionDuration: '300ms',
                ":hover": {
                  opacity: '100%'
                }
              }} />
            </Tooltip>
          </FieldLabel>
          {data.protocols.length > 0 && (
            <Box
              display="flex"
              flexWrap="wrap"
              sx={{
                margin: "0 -4px",
                "& > *": {
                  margin: "4px",
                },
              }}
            >
              {data.protocols.map((protocol: string) => (
                <Chip
                  size="small"
                  key={protocol}
                  variant="outlined"
                  label={protocol}
                />
              ))}
            </Box>
          )}
        </Section>
        <Section>
          <FieldLabel>Platforms</FieldLabel>
          {filteredPlatforms.length > 0 ? (
            <Box
              display="flex"
              flexWrap="wrap"
              sx={{
                margin: "0 -4px",
                "& > *": {
                  margin: "4px",
                },
              }}
            >
              {filteredPlatforms.map((platform: any) => (
                <Chip
                  size="small"
                  key={platform}
                  variant="outlined"
                  label={platform}
                />
              ))}
            </Box>
          ) : (
            <Typography>None</Typography>
          )}
        </Section>
      </Box >
    </Drawer >
  );
}

export default PluginVersionDetailsSidebar;
