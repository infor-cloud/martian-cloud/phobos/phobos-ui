import CopyIcon from '@mui/icons-material/ContentCopy';
import DoubleArrowIcon from '@mui/icons-material/DoubleArrow';
import { LoadingButton } from "@mui/lab";
import { Alert, Button, Chip, CircularProgress, Dialog, DialogActions, DialogContent, DialogTitle, IconButton, Typography } from '@mui/material';
import Box from '@mui/material/Box';
import Tab from '@mui/material/Tab';
import Tabs from '@mui/material/Tabs';
import { useTheme } from '@mui/material/styles';
import useMediaQuery from '@mui/material/useMediaQuery';
import graphql from 'babel-plugin-relay/macro';
import { enqueueSnackbar } from 'notistack';
import React, { Suspense, useEffect, useState } from 'react';
import { PreloadedQuery, useFragment, useMutation, usePreloadedQuery, useQueryLoader } from 'react-relay/hooks';
import { useNavigate, useParams, useSearchParams } from 'react-router-dom';
import { Prism as SyntaxHighlighter } from 'react-syntax-highlighter';
import { atomDark as prismTheme } from 'react-syntax-highlighter/dist/esm/styles/prism';
import ListSkeleton from '../skeletons/ListSkeleton';
import PluginVersionDetailsSidebar, { SidebarWidth } from './PluginVersionDetailsSidebar';
import PluginVersionDocs from './PluginVersionDocs';
import PluginVersionList from './PluginVersionList';
import { PluginVersionDetailsDeletePluginVersionMutation } from './__generated__/PluginVersionDetailsDeletePluginVersionMutation.graphql';
import { PluginVersionDetailsFragment_plugin$key } from './__generated__/PluginVersionDetailsFragment_plugin.graphql';
import { PluginVersionDetailsIndexFragment_details$key } from './__generated__/PluginVersionDetailsIndexFragment_details.graphql';
import { PluginVersionDetailsQuery } from './__generated__/PluginVersionDetailsQuery.graphql';

const query = graphql`
    query PluginVersionDetailsQuery($prn: String!) {
      node(id: $prn) {
        ...on PluginVersion {
          ...PluginVersionDetailsFragment_plugin
        }
      }
    }
`;

function PluginVersionDetailsEntryPoint() {
  const { organizationName, pluginName, version } = useParams() as { organizationName: string, pluginName: string, version: string };

  const [queryRef, loadQuery] = useQueryLoader<PluginVersionDetailsQuery>(query)

  useEffect(() => {
    loadQuery(
      { prn: `prn:plugin_version:${organizationName}/${pluginName}/${version}` },
      { fetchPolicy: 'store-and-network' }
    );
  }, [loadQuery, organizationName, pluginName, version])

  return queryRef != null ? <PluginVersionDetailsContainer queryRef={queryRef} organizationName={organizationName} pluginName={pluginName} version={version} /> : null
}

interface PluginVersionDetailsContainerProps {
  queryRef: PreloadedQuery<PluginVersionDetailsQuery>
  organizationName: string
  pluginName: string
  version: string
}

function PluginVersionDetailsContainer({ queryRef, organizationName, pluginName, version }: PluginVersionDetailsContainerProps) {
  const queryData = usePreloadedQuery<PluginVersionDetailsQuery>(query, queryRef)

  return queryData.node ? (
    <PluginVersionDetails fragmentRef={queryData.node} />
  ) : (
    <Box display="flex" justifyContent="center" pt={4}>
      <Typography color="textSecondary">
        version <strong>{version}</strong> not found for plugin <strong>{organizationName}/{pluginName}</strong>
      </Typography>
    </Box>
  );
}

function buildUsageInfo(organizationName: string, pluginName: string, version: string) {
  return `plugin_requirements {
    ${pluginName} = {
       source  = "${organizationName}/${pluginName}"
       version = "${version}"
    }
 }

plugin "${pluginName}" {
 # Configuration options
}`;
}

interface ConfirmationDialogProps {
  version: string;
  deleteInProgress: boolean;
  keepMounted: boolean;
  open: boolean;
  onClose: (confirm?: boolean) => void;
}

function DeleteConfirmationDialog(props: ConfirmationDialogProps) {
  const { version, deleteInProgress, onClose, open, ...other } = props;
  return (
    <Dialog maxWidth="xs" open={open} {...other}>
      <DialogTitle>Delete Version</DialogTitle>
      <DialogContent dividers>
        Are you sure you want to delete plugin version <strong>{version}</strong>?
      </DialogContent>
      <DialogActions>
        <Button color="inherit" onClick={() => onClose()}>
          Cancel
        </Button>
        <LoadingButton
          color="error"
          loading={deleteInProgress}
          onClick={() => onClose(true)}
        >
          Delete
        </LoadingButton>
      </DialogActions>
    </Dialog>
  );
}

interface PluginVersionDetailsProps {
  fragmentRef: PluginVersionDetailsFragment_plugin$key
}

function PluginVersionDetails({ fragmentRef }: PluginVersionDetailsProps) {
  const data = useFragment<PluginVersionDetailsFragment_plugin$key>(
    graphql`
        fragment PluginVersionDetailsFragment_plugin on PluginVersion
        {
          ...PluginVersionDetailsIndexFragment_details
        }
  `, fragmentRef)

  return (
    <Box display="flex">
      <Box component="main" flexGrow={1}>
        <Suspense fallback={<Box
          sx={{
            width: '100%',
            height: `calc(100vh - 64px)`,
            display: 'flex',
            alignItems: 'center',
            justifyContent: 'center'
          }}
        >
          <CircularProgress />
        </Box>}>
          <Box maxWidth={1600} margin="auto" padding={2}>
            <PluginVersionDetailsIndex fragmentRef={data} />
          </Box>
        </Suspense>
      </Box>
    </Box>
  );
}

interface IndexProps {
  fragmentRef: PluginVersionDetailsIndexFragment_details$key
}

function PluginVersionDetailsIndex(props: IndexProps) {
  const [searchParams, setSearchParams] = useSearchParams();
  const [showDeleteConfirmationDialog, setShowDeleteConfirmationDialog] = useState(false);

  const navigate = useNavigate();
  const theme = useTheme();
  const mobile = useMediaQuery(theme.breakpoints.down('md'));

  const [sidebarOpen, setSidebarOpen] = useState(false);

  const data = useFragment<PluginVersionDetailsIndexFragment_details$key>(
    graphql`
          fragment PluginVersionDetailsIndexFragment_details on PluginVersion
          {
              id
              version
              shaSumsUploaded
              plugin {
                  id
                  name
                  organizationName
                  private
                  ...PluginVersionListFragment_plugin
              }
              ...PluginVersionDetailsSidebarFragment_details
              ...PluginVersionDocsFragment_details
          }
        `, props.fragmentRef);

  const tab = searchParams.get('tab') || 'docs';

  const onToggleSidebar = () => {
    setSidebarOpen(prev => !prev);
  };

  const onTabChange = (event: React.SyntheticEvent, newValue: string) => {
    searchParams.set('tab', newValue);
    setSearchParams(searchParams, { replace: true });
  };

  const [commit, commitInFlight] = useMutation<PluginVersionDetailsDeletePluginVersionMutation>(graphql`
    mutation PluginVersionDetailsDeletePluginVersionMutation($input: DeletePluginVersionInput!) {
        deletePluginVersion(input: $input) {
            problems {
                message
                field
                type
            }
        }
    }
  `);

  const onDeleteConfirmationDialogClosed = (confirm?: boolean) => {
    if (confirm) {
      commit({
        variables: {
          input: {
            id: data.id
          },
        },
        onCompleted: (data) => {
          setShowDeleteConfirmationDialog(false);

          if (data.deletePluginVersion.problems.length) {
            enqueueSnackbar(data.deletePluginVersion.problems.
              map((problem: any) => problem.message).
              join("; "),
              { variant: "warning" }
            );
          } else {
            navigate(`/plugin-registry`);
          }
        },
        onError: (error) => {
          setShowDeleteConfirmationDialog(false);
          enqueueSnackbar(`Unexpected error occurred: ${error.message}`, {
            variant: "error",
          });
        },
      });
    } else {
      setShowDeleteConfirmationDialog(false);
    }
  };

  const usageInfo = buildUsageInfo(data.plugin.organizationName, data.plugin.name, data.version);

  return (
    <Box>
      <PluginVersionDetailsSidebar
        fragmentRef={data}
        open={sidebarOpen}
        temporary={mobile}
        onClose={onToggleSidebar}
      />
      <Box>
        <Box paddingRight={!mobile ? `${SidebarWidth}px` : 0}>
          {(!data.shaSumsUploaded) && <Alert sx={{ marginBottom: 2 }} variant='outlined' severity="warning">
            This plugin version is missing the required checksum file
          </Alert>}
          <Box display="flex" alignItems="center" marginBottom={2} justifyContent="space-between">
            <Box display="flex" alignItems="center">
              <Typography variant="h6">{data.plugin.organizationName} / {data.plugin.name}</Typography>
              {data.plugin.private && <Chip sx={{ marginLeft: 2 }} variant="outlined" color="warning" size="small" label="private" />}
            </Box>
            <Button sx={{ minWidth: 160 }}
              color="error"
              variant="outlined"
              onClick={() => setShowDeleteConfirmationDialog(true)}
            >
              Delete Version
            </Button>
            {mobile && <Box display="flex" justifyContent="space-between">
              <IconButton onClick={onToggleSidebar}><DoubleArrowIcon sx={{ transform: 'rotate(180deg)' }} /></IconButton>
            </Box>}
          </Box>
          <Box sx={{ border: 1, borderColor: 'divider', marginBottom: 2 }}>
            <Tabs value={tab} onChange={onTabChange}>
              <Tab label="Docs" value="docs" />
              <Tab label="How To Use" value="usage" />
              <Tab label="Versions" value="versions" />
            </Tabs>
          </Box>
          <React.Fragment>
            {tab === 'docs' && <PluginVersionDocs fragmentRef={data} />}
            {tab === 'usage' && <Box marginTop={2} position="relative">
              <IconButton sx={{ padding: 2, position: 'absolute', top: 0, right: 0 }} onClick={() => navigator.clipboard.writeText(usageInfo)}>
                <CopyIcon sx={{ width: 16, height: 16 }} />
              </IconButton>
              <SyntaxHighlighter wrapLines customStyle={{ fontSize: 14 }} language="hcl" style={prismTheme} children={usageInfo} />
            </Box>}
            {tab === 'versions' && <Box marginTop={2}>
              <Suspense fallback={<ListSkeleton rowCount={3} />}>
                <PluginVersionList fragmentRef={data.plugin} />
              </Suspense>
            </Box>}
          </React.Fragment>
        </Box>
      </Box>
      {showDeleteConfirmationDialog && <DeleteConfirmationDialog
        version={data.version}
        keepMounted
        deleteInProgress={commitInFlight}
        open={showDeleteConfirmationDialog}
        onClose={onDeleteConfirmationDialogClosed}
      />}
    </Box>
  );
}

export default PluginVersionDetailsEntryPoint;
