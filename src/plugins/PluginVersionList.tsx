import { Box, List, Paper, Typography, useTheme } from '@mui/material';
import graphql from 'babel-plugin-relay/macro';
import InfiniteScroll from 'react-infinite-scroll-component';
import { useFragment, useLazyLoadQuery, usePaginationFragment } from "react-relay/hooks";
import ListSkeleton from '../skeletons/ListSkeleton';
import PluginVersionListItem from './PluginVersionListItem';
import { PluginVersionListFragment_plugin$key } from './__generated__/PluginVersionListFragment_plugin.graphql';
import { PluginVersionListFragment_versions$key } from './__generated__/PluginVersionListFragment_versions.graphql';
import { PluginVersionListPaginationQuery } from './__generated__/PluginVersionListPaginationQuery.graphql';
import { PluginVersionListQuery } from './__generated__/PluginVersionListQuery.graphql';

const INITIAL_ITEM_COUNT = 100;

const query = graphql`
    query PluginVersionListQuery($first: Int, $last: Int, $after: String, $before: String, $pluginId: String!) {
        ...PluginVersionListFragment_versions
    }
`;

interface Props {
    fragmentRef: PluginVersionListFragment_plugin$key
}

function PluginVersionList(props: Props) {
    const theme = useTheme();
    const plugin = useFragment<PluginVersionListFragment_plugin$key>(
        graphql`
        fragment PluginVersionListFragment_plugin on Plugin
        {
          id
        }
    `, props.fragmentRef)

    const queryData = useLazyLoadQuery<PluginVersionListQuery>(query, { first: INITIAL_ITEM_COUNT, pluginId: plugin.id }, { fetchPolicy: 'store-and-network' })

    const { data, loadNext, hasNext } = usePaginationFragment<PluginVersionListPaginationQuery, PluginVersionListFragment_versions$key>(
        graphql`
      fragment PluginVersionListFragment_versions on Query
      @refetchable(queryName: "PluginVersionListPaginationQuery") {
        node(id: $pluginId) {
            ...on Plugin {
                versions(
                    after: $after
                    before: $before
                    first: $first
                    last: $last
                    sort: CREATED_AT_DESC
                ) @connection(key: "PluginVersionList_versions") {
                    totalCount
                    edges {
                        node {
                            id
                            ...PluginVersionListItemFragment_version
                        }
                    }
                }
            }
        }
      }
    `, queryData);

    return (
        <Box>
            <Paper sx={{ borderBottomLeftRadius: 0, borderBottomRightRadius: 0, border: `1px solid ${theme.palette.divider}` }}>
                <Box padding={2} display="flex" alignItems="center" justifyContent="space-between">
                    <Typography variant="subtitle1">
                        {data.node?.versions?.edges?.length} version{data.node?.versions?.edges?.length === 1 ? '' : 's'}
                    </Typography>
                </Box>
            </Paper>
            <InfiniteScroll
                dataLength={data.node?.versions?.edges?.length ?? 0}
                next={() => loadNext(20)}
                hasMore={hasNext}
                loader={<ListSkeleton rowCount={3} />}
            >
                <List disablePadding>
                    {data.node?.versions?.edges?.map((edge: any) => <PluginVersionListItem
                        key={edge.node.id}
                        fragmentRef={edge.node}
                    />)}
                </List>
            </InfiniteScroll>
        </Box>
    )
}

export default PluginVersionList
