import {
  CircularProgress,
  Typography,
  useTheme
} from "@mui/material";
import Box from "@mui/material/Box";
import graphql from "babel-plugin-relay/macro";
import { Suspense, useMemo } from "react";
import { useFragment } from "react-relay/hooks";
import { useSearchParams } from 'react-router-dom';
import PluginVersionDocsFileTree from "./PluginVersionDocsFileTree";
import PluginVersionDocsMarkdown from "./PluginVersionDocsMarkdown";
import { PluginVersionDocsFragment_details$key } from "./__generated__/PluginVersionDocsFragment_details.graphql";

interface Props {
  fragmentRef: PluginVersionDocsFragment_details$key;
}

function PluginVersionDocs(props: Props) {
  const theme = useTheme();
  const [searchParams, setSearchParams] = useSearchParams();
  const file = searchParams.get('file');

  const data = useFragment<PluginVersionDocsFragment_details$key>(
    graphql`
      fragment PluginVersionDocsFragment_details on PluginVersion {
        id
        docFiles {
          category
          filePath
        }
        ...PluginVersionDocsFileTreeFragment_pluginVersion
      }
    `,
    props.fragmentRef
  );

  const onNodeSelected = (filePath: string) => {
    searchParams.set('file', filePath);
    setSearchParams(searchParams, { replace: true });
  };

  const selectedFile = useMemo(() => {
    if (file) {
      return file;
    }
    // Default to the overview file
    const overviewFile = data.docFiles.find((f) => f.category === 'OVERVIEW');
    if (overviewFile) {
      return overviewFile.filePath;
    }

    // Default to the first file
    return data.docFiles[0]?.filePath;
  }, [data, file]);

  return data.docFiles.length > 0 ? (
    <Box
      display="flex"
      sx={{
        [theme.breakpoints.down('md')]: {
          flexDirection: 'column',
          alignItems: 'flex-start',
          '& > *': { mb: 4 },
        }
      }}
    >
      <Box
        width={230}
        paddingRight={1}
        sx={{
          [theme.breakpoints.down('md')]: {
            paddingRight: 0,
            width: '100%'
          }
        }}
      >
        <PluginVersionDocsFileTree selectedFilePath={selectedFile} onFileSelected={onNodeSelected} fragmentRef={data} />
      </Box>
      <Box
        flex={1}
        borderLeft={`1px solid ${theme.palette.divider}`}
        paddingLeft={4}
        sx={{
          [theme.breakpoints.down('md')]: {
            borderLeft: 'none',
            paddingLeft: 0
          }
        }}
      >
        <Suspense fallback={<Box
          sx={{
            width: '100%',
            minHeight: 400,
            display: 'flex',
            alignItems: 'center',
            justifyContent: 'center'
          }}>
          <CircularProgress />
        </Box>}>
          {selectedFile && <PluginVersionDocsMarkdown pluginVersionId={data.id} filePath={selectedFile} />}
        </Suspense>
      </Box>
    </Box>
  ) : (
    <Box
      sx={{
        width: '100%',
        minHeight: 200,
        display: 'flex',
        alignItems: 'center',
        justifyContent: 'center'
      }}
    >
      <Typography variant="subtitle1" color="textSecondary">No plugin documentation available</Typography>
    </Box>
  );
}

export default PluginVersionDocs;
