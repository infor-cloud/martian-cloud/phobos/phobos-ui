import { Box, CircularProgress } from '@mui/material';
import graphql from 'babel-plugin-relay/macro';
import { Suspense, useEffect } from 'react';
import { PreloadedQuery, usePreloadedQuery, useQueryLoader } from 'react-relay/hooks';
import { Outlet, ScrollRestoration } from 'react-router-dom';
import ErrorBoundary from './ErrorBoundary';
import { RootQuery } from './__generated__/RootQuery.graphql';
import AppHeader from './nav/AppHeader';
import { User, UserContext } from './UserContext';

const query = graphql`
    query RootQuery {
        me {
            ...on User {
                id
                username
                email
                admin
            }
        }
        ...AppHeaderFragment
    }
`;

function RootEntryPoint() {
    const [queryRef, loadQuery] = useQueryLoader<RootQuery>(query);

    useEffect(() => {
        loadQuery({}, { fetchPolicy: 'store-and-network' })
    }, [loadQuery])

    return queryRef != null ? <Root queryRef={queryRef} /> : null;
}

interface Props {
    queryRef: PreloadedQuery<RootQuery>;
}

function Root({ queryRef }: Props) {
    const queryData = usePreloadedQuery<RootQuery>(query, queryRef);

    return (
        <Box>
            <ScrollRestoration />
            <Box marginTop="65px">
                <ErrorBoundary>
                    <Suspense>
                        <UserContext.Provider value={queryData.me as User}>
                            <AppHeader fragmentRef={queryData} />
                            <ErrorBoundary>
                                <Suspense fallback={<Box
                                    sx={{
                                        position: 'absolute',
                                        top: 0,
                                        left: 0,
                                        width: '100%',
                                        height: '100vh',
                                        display: 'flex',
                                        alignItems: 'center',
                                        justifyContent: 'center'
                                    }}>
                                    <CircularProgress />
                                </Box>}>
                                    <Outlet />
                                </Suspense>
                            </ErrorBoundary>
                        </UserContext.Provider>
                    </Suspense>
                </ErrorBoundary>
            </Box>
        </Box>
    );
}

export default RootEntryPoint;
