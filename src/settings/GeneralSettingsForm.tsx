import { Alert, Box, Divider, TextField, Typography } from '@mui/material';
import { MutationError } from '../common/error';

export interface FormData {
    name: string
    description: string
}

interface Props {
    editMode?: boolean
    error?: MutationError
    data: FormData
    onChange: (data: FormData) => void
}

function GeneralSettingsForm({ data, onChange, editMode, error }: Props) {

    return (
        <Box>
            {error && <Alert sx={{ mb: 2 }} severity={error.severity}>
                {error.message}
            </Alert>}
            <Typography mt={2} variant="subtitle1" gutterBottom>Details</Typography>
            <Divider />
            <Box sx={{ mt: 2, mb: 2 }}>
                <TextField
                    disabled={editMode}
                    inputProps={{ maxLength: 64 }}
                    fullWidth
                    autoComplete="off"
                    size="small"
                    label="Name"
                    value={data.name}
                    onChange={event => onChange({ ...data, name: event.target.value })}
                />
                <TextField
                    inputProps={{ maxLength: 100 }}
                    fullWidth
                    margin="normal"
                    autoComplete="off"
                    size="small"
                    label="Description"
                    value={data.description}
                    onChange={event => onChange({ ...data, description: event.target.value })}
                />
            </Box>
        </Box>
    );
}

export default GeneralSettingsForm
