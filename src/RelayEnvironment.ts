import { Environment, Network, Observable, RecordSource, RequestParameters, Store, Variables } from 'relay-runtime';
import { SubscriptionClient } from 'subscriptions-transport-ws';
import cfg from './common/config';
import graphQLFetcher from './api/fetchGraphQL';

const KEEP_ALIVE_INTERVAL = 60 * 1000; // 1 minute

const environment = (getAccessToken: () => Promise<string>) => {
    const subscriptionClient = new SubscriptionClient(`${cfg.wsUrl}/graphql`, {
        reconnect: true,
        lazy: true, // connect only when the first subscription is created
        timeout: KEEP_ALIVE_INTERVAL * 2, // The max time to wait for a keep alive response from the server
        connectionParams: async () => {
            const token = await getAccessToken();
            return {
                Authorization: `bearer ${token}`,
            };
        },
    });

    const subscribe = (request: RequestParameters, variables: Variables) => {
        const subscribeObservable = subscriptionClient.request({
            query: request.text as string,
            operationName: request.name,
            variables,
        });
        // Important: Convert subscriptions-transport-ws observable type to Relay's
        return Observable.from(subscribeObservable as any);
    };

    setInterval(() => {
        if (subscriptionClient.status === 1) {
            subscriptionClient.client.send('{"type":"ka"}');
        }
    }, KEEP_ALIVE_INTERVAL);

    const fetchGraphQL = graphQLFetcher(getAccessToken);

    return new Environment({
        network: Network.create(
            async (params: any, variables) => {
                return fetchGraphQL(params.text, variables);
            },
            subscribe),
        store: new Store(new RecordSource()),
    });
}

export default environment
