import { useState } from 'react';
import { Box, Button, Divider, Typography } from '@mui/material';
import { LoadingButton } from '@mui/lab';
import { useNavigate, useParams } from 'react-router-dom';
import { MutationError } from '../common/error';
import graphql from 'babel-plugin-relay/macro';
import { useLazyLoadQuery, useMutation } from 'react-relay/hooks';
import { Link as RouterLink } from 'react-router-dom';
import ApprovalRuleForm, { ApprovalRule, User, ServiceAccount, Team } from './ApprovalRuleForm';
import { EditApprovalRuleMutation } from '../organizations/approvalrules/__generated__/EditApprovalRuleMutation.graphql';
import { EditApprovalRuleQuery } from '../organizations/approvalrules/__generated__/EditApprovalRuleQuery.graphql';

interface Props {
    orgId?: string
    projectId?: string
}

function EditApprovalRule({ orgId, projectId }: Props) {
    const approvalRuleId = useParams().approvalRuleId as string;
    const navigate = useNavigate();

    const queryData = useLazyLoadQuery<EditApprovalRuleQuery>(graphql`
        query EditApprovalRuleQuery($id: String!) {
            node(id: $id) {
                ... on ApprovalRule {
                    id
                    name
                    description
                    approvalsRequired
                    users {
                        id
                        email
                        username
                    }
                    serviceAccounts {
                        id
                        name
                    }
                    teams {
                        id
                        name
                    }
                }
            }
        }`, { id: approvalRuleId })

    const approvalRule = queryData.node as ApprovalRule;

    const [error, setError] = useState<MutationError>();
    const [formData, setFormData] = useState<ApprovalRule>({
        id: approvalRule.id,
        name: approvalRule.name,
        description: approvalRule.description,
        approvalsRequired: approvalRule.approvalsRequired,
        users: approvalRule.users,
        serviceAccounts: approvalRule.serviceAccounts,
        teams: approvalRule.teams,
    });

    const [commit, isInFlight] = useMutation<EditApprovalRuleMutation>(
        graphql`
        mutation EditApprovalRuleMutation($input: UpdateApprovalRuleInput!) {
            updateApprovalRule(input: $input) {
                approvalRule {
                    ...ApprovalRuleListItemFragment_approvalRule
                }
                problems {
                    message
                    field
                    type
                }
            }
        }`
    );

    const onUpdate = () => {
        if (approvalRule.id) {
            commit({
                variables: {
                    input: {
                        id: approvalRule.id,
                        description: formData.description,
                        users: formData.users.map((user: User) => user.id),
                        serviceAccounts: formData.serviceAccounts.map((sa: ServiceAccount) => sa.id),
                        teams: formData.teams.map((team: Team) => team.id)
                    }
                },
                onCompleted: (data) => {
                    if (data.updateApprovalRule.problems.length) {
                        setError({
                            severity: 'warning',
                            message: data.updateApprovalRule.problems.map((problem: any) => problem.message).join('; ')
                        })
                    }
                    else {
                        navigate('..');
                    }
                },
                onError: (error) => {
                    setError({
                        severity: 'error',
                        message: `Unexpected error occurred: ${error.message}`
                    })
                }
            })
        }
        else {
            navigate('..');
         }
    };

    return (
        <Box>
            <Typography variant="h5">Edit Approval Rule</Typography>
            <ApprovalRuleForm
                editMode
                orgId={orgId}
                projectId={projectId}
                data={formData}
                error={error}
                onChange={(approvalRule: ApprovalRule) => setFormData(approvalRule)}
            />
            <Divider sx={{ opacity: 0.6 }}  />
            <Box mt={2}>
                <LoadingButton
                    sx={{ mr: 2 }}
                    loading={isInFlight}
                    onClick={onUpdate}
                    variant="outlined">
                    Update Approval Rule
                </LoadingButton>
                <Button component={RouterLink} color="inherit" to={-1 as any}>Cancel</Button>
            </Box>
        </Box>
    );
}

export default EditApprovalRule;
