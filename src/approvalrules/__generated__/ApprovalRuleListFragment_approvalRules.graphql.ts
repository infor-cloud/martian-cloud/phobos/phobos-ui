/**
 * @generated SignedSource<<6a13cd537e6fa6fee26df1703bb373b6>>
 * @lightSyntaxTransform
 * @nogrep
 */

/* tslint:disable */
/* eslint-disable */
// @ts-nocheck

import { Fragment, ReaderFragment } from 'relay-runtime';
export type ScopeType = "GLOBAL" | "ORGANIZATION" | "PROJECT" | "%future added value";
import { FragmentRefs } from "relay-runtime";
export type ApprovalRuleListFragment_approvalRules$data = {
  readonly edges: ReadonlyArray<{
    readonly node: {
      readonly id: string;
      readonly scope: ScopeType;
      readonly " $fragmentSpreads": FragmentRefs<"ApprovalRuleListItemFragment_approvalRule">;
    } | null | undefined;
  } | null | undefined> | null | undefined;
  readonly totalCount: number;
  readonly " $fragmentType": "ApprovalRuleListFragment_approvalRules";
};
export type ApprovalRuleListFragment_approvalRules$key = {
  readonly " $data"?: ApprovalRuleListFragment_approvalRules$data;
  readonly " $fragmentSpreads": FragmentRefs<"ApprovalRuleListFragment_approvalRules">;
};

const node: ReaderFragment = {
  "argumentDefinitions": [],
  "kind": "Fragment",
  "metadata": null,
  "name": "ApprovalRuleListFragment_approvalRules",
  "selections": [
    {
      "alias": null,
      "args": null,
      "kind": "ScalarField",
      "name": "totalCount",
      "storageKey": null
    },
    {
      "alias": null,
      "args": null,
      "concreteType": "ApprovalRuleEdge",
      "kind": "LinkedField",
      "name": "edges",
      "plural": true,
      "selections": [
        {
          "alias": null,
          "args": null,
          "concreteType": "ApprovalRule",
          "kind": "LinkedField",
          "name": "node",
          "plural": false,
          "selections": [
            {
              "alias": null,
              "args": null,
              "kind": "ScalarField",
              "name": "id",
              "storageKey": null
            },
            {
              "alias": null,
              "args": null,
              "kind": "ScalarField",
              "name": "scope",
              "storageKey": null
            },
            {
              "args": null,
              "kind": "FragmentSpread",
              "name": "ApprovalRuleListItemFragment_approvalRule"
            }
          ],
          "storageKey": null
        }
      ],
      "storageKey": null
    }
  ],
  "type": "ApprovalRuleConnection",
  "abstractKey": null
};

(node as any).hash = "bc89e81b243921f5588ba137290985b2";

export default node;
