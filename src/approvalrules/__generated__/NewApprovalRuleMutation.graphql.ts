/**
 * @generated SignedSource<<fc2a5187af8a3a055d3d440613bea748>>
 * @lightSyntaxTransform
 * @nogrep
 */

/* tslint:disable */
/* eslint-disable */
// @ts-nocheck

import { ConcreteRequest, Mutation } from 'relay-runtime';
import { FragmentRefs } from "relay-runtime";
export type ProblemType = "BAD_REQUEST" | "CONFLICT" | "FORBIDDEN" | "NOT_FOUND" | "%future added value";
export type ScopeType = "GLOBAL" | "ORGANIZATION" | "PROJECT" | "%future added value";
export type CreateApprovalRuleInput = {
  approvalsRequired: number;
  clientMutationId?: string | null | undefined;
  description: string;
  name: string;
  organizationId?: string | null | undefined;
  projectId?: string | null | undefined;
  scope: ScopeType;
  serviceAccounts: ReadonlyArray<string>;
  teams: ReadonlyArray<string>;
  users: ReadonlyArray<string>;
};
export type NewApprovalRuleMutation$variables = {
  connections: ReadonlyArray<string>;
  input: CreateApprovalRuleInput;
};
export type NewApprovalRuleMutation$data = {
  readonly createApprovalRule: {
    readonly approvalRule: {
      readonly " $fragmentSpreads": FragmentRefs<"ApprovalRuleListItemFragment_approvalRule">;
    } | null | undefined;
    readonly problems: ReadonlyArray<{
      readonly field: ReadonlyArray<string> | null | undefined;
      readonly message: string;
      readonly type: ProblemType;
    }>;
  };
};
export type NewApprovalRuleMutation = {
  response: NewApprovalRuleMutation$data;
  variables: NewApprovalRuleMutation$variables;
};

const node: ConcreteRequest = (function(){
var v0 = {
  "defaultValue": null,
  "kind": "LocalArgument",
  "name": "connections"
},
v1 = {
  "defaultValue": null,
  "kind": "LocalArgument",
  "name": "input"
},
v2 = [
  {
    "kind": "Variable",
    "name": "input",
    "variableName": "input"
  }
],
v3 = {
  "alias": null,
  "args": null,
  "concreteType": "Problem",
  "kind": "LinkedField",
  "name": "problems",
  "plural": true,
  "selections": [
    {
      "alias": null,
      "args": null,
      "kind": "ScalarField",
      "name": "message",
      "storageKey": null
    },
    {
      "alias": null,
      "args": null,
      "kind": "ScalarField",
      "name": "field",
      "storageKey": null
    },
    {
      "alias": null,
      "args": null,
      "kind": "ScalarField",
      "name": "type",
      "storageKey": null
    }
  ],
  "storageKey": null
},
v4 = {
  "alias": null,
  "args": null,
  "kind": "ScalarField",
  "name": "id",
  "storageKey": null
},
v5 = {
  "alias": null,
  "args": null,
  "kind": "ScalarField",
  "name": "name",
  "storageKey": null
},
v6 = [
  (v4/*: any*/),
  (v5/*: any*/)
];
return {
  "fragment": {
    "argumentDefinitions": [
      (v0/*: any*/),
      (v1/*: any*/)
    ],
    "kind": "Fragment",
    "metadata": null,
    "name": "NewApprovalRuleMutation",
    "selections": [
      {
        "alias": null,
        "args": (v2/*: any*/),
        "concreteType": "CreateApprovalRulePayload",
        "kind": "LinkedField",
        "name": "createApprovalRule",
        "plural": false,
        "selections": [
          {
            "alias": null,
            "args": null,
            "concreteType": "ApprovalRule",
            "kind": "LinkedField",
            "name": "approvalRule",
            "plural": false,
            "selections": [
              {
                "args": null,
                "kind": "FragmentSpread",
                "name": "ApprovalRuleListItemFragment_approvalRule"
              }
            ],
            "storageKey": null
          },
          (v3/*: any*/)
        ],
        "storageKey": null
      }
    ],
    "type": "Mutation",
    "abstractKey": null
  },
  "kind": "Request",
  "operation": {
    "argumentDefinitions": [
      (v1/*: any*/),
      (v0/*: any*/)
    ],
    "kind": "Operation",
    "name": "NewApprovalRuleMutation",
    "selections": [
      {
        "alias": null,
        "args": (v2/*: any*/),
        "concreteType": "CreateApprovalRulePayload",
        "kind": "LinkedField",
        "name": "createApprovalRule",
        "plural": false,
        "selections": [
          {
            "alias": null,
            "args": null,
            "concreteType": "ApprovalRule",
            "kind": "LinkedField",
            "name": "approvalRule",
            "plural": false,
            "selections": [
              (v4/*: any*/),
              (v5/*: any*/),
              {
                "alias": null,
                "args": null,
                "kind": "ScalarField",
                "name": "description",
                "storageKey": null
              },
              {
                "alias": null,
                "args": null,
                "kind": "ScalarField",
                "name": "approvalsRequired",
                "storageKey": null
              },
              {
                "alias": null,
                "args": null,
                "concreteType": "User",
                "kind": "LinkedField",
                "name": "users",
                "plural": true,
                "selections": [
                  (v4/*: any*/),
                  {
                    "alias": null,
                    "args": null,
                    "kind": "ScalarField",
                    "name": "username",
                    "storageKey": null
                  },
                  {
                    "alias": null,
                    "args": null,
                    "kind": "ScalarField",
                    "name": "email",
                    "storageKey": null
                  }
                ],
                "storageKey": null
              },
              {
                "alias": null,
                "args": null,
                "concreteType": "ServiceAccount",
                "kind": "LinkedField",
                "name": "serviceAccounts",
                "plural": true,
                "selections": (v6/*: any*/),
                "storageKey": null
              },
              {
                "alias": null,
                "args": null,
                "concreteType": "Team",
                "kind": "LinkedField",
                "name": "teams",
                "plural": true,
                "selections": (v6/*: any*/),
                "storageKey": null
              },
              {
                "alias": null,
                "args": null,
                "concreteType": "ResourceMetadata",
                "kind": "LinkedField",
                "name": "metadata",
                "plural": false,
                "selections": [
                  {
                    "alias": null,
                    "args": null,
                    "kind": "ScalarField",
                    "name": "prn",
                    "storageKey": null
                  }
                ],
                "storageKey": null
              }
            ],
            "storageKey": null
          },
          {
            "alias": null,
            "args": null,
            "filters": null,
            "handle": "prependNode",
            "key": "",
            "kind": "LinkedHandle",
            "name": "approvalRule",
            "handleArgs": [
              {
                "kind": "Variable",
                "name": "connections",
                "variableName": "connections"
              },
              {
                "kind": "Literal",
                "name": "edgeTypeName",
                "value": "ApprovalRuleEdge"
              }
            ]
          },
          (v3/*: any*/)
        ],
        "storageKey": null
      }
    ]
  },
  "params": {
    "cacheID": "029b5e539645cf0d5261615f355f62eb",
    "id": null,
    "metadata": {},
    "name": "NewApprovalRuleMutation",
    "operationKind": "mutation",
    "text": "mutation NewApprovalRuleMutation(\n  $input: CreateApprovalRuleInput!\n) {\n  createApprovalRule(input: $input) {\n    approvalRule {\n      ...ApprovalRuleListItemFragment_approvalRule\n      id\n    }\n    problems {\n      message\n      field\n      type\n    }\n  }\n}\n\nfragment ApprovalRuleListItemFragment_approvalRule on ApprovalRule {\n  id\n  name\n  description\n  approvalsRequired\n  users {\n    id\n    username\n    email\n  }\n  serviceAccounts {\n    id\n    name\n  }\n  teams {\n    id\n    name\n  }\n  metadata {\n    prn\n  }\n}\n"
  }
};
})();

(node as any).hash = "3e015b8063350e319777322a8144098f";

export default node;
