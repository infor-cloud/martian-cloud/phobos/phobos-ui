import { useState } from 'react';
import { Box, Button, Divider, Typography } from '@mui/material';
import LoadingButton from '@mui/lab/LoadingButton';
import graphql from 'babel-plugin-relay/macro';
import { useMutation } from "react-relay/hooks";
import { Link as RouterLink, useNavigate } from 'react-router-dom';
import { MutationError } from '../common/error';
import ApprovalRuleForm, { ApprovalRule, User, ServiceAccount, Team } from './ApprovalRuleForm';
import { NewApprovalRuleMutation } from '../organizations/approvalrules/__generated__/NewApprovalRuleMutation.graphql';

interface Props {
    orgId?: string
    projectId?: string
    getConnections: () => [string]
}

function NewApprovalRule({ orgId, projectId, getConnections }: Props) {
    const navigate = useNavigate();

    const [commit, isInFlight] = useMutation<NewApprovalRuleMutation>(graphql`
        mutation NewApprovalRuleMutation($input: CreateApprovalRuleInput!, $connections: [ID!]!) {
            createApprovalRule(input: $input) {
                # Use @prependNode to add the node to the connection
                approvalRule  @prependNode(connections: $connections, edgeTypeName: "ApprovalRuleEdge")  {
                    ...ApprovalRuleListItemFragment_approvalRule
                }
                problems {
                    message
                    field
                    type
                }
            }
        }
    `);

    const [error, setError] = useState<MutationError>()
    const [approvalRule, setApprovalRule] = useState<ApprovalRule>({
        id: '',
        name: '',
        description: '',
        approvalsRequired: 1,
        users: [],
        serviceAccounts: [],
        teams: [],
    });

    const onSave = () => {

        commit({
            variables: {
                input: {
                    name: approvalRule.name,
                    scope: orgId ? 'ORGANIZATION' : 'PROJECT',
                    organizationId: orgId,
                    projectId: projectId,
                    description: approvalRule.description,
                    approvalsRequired: typeof approvalRule.approvalsRequired === 'number'
                        ? approvalRule.approvalsRequired
                        : parseInt(approvalRule.approvalsRequired),
                    users: approvalRule.users.map((user: User) => user.id),
                    serviceAccounts: approvalRule.serviceAccounts.map((sa: ServiceAccount) => sa.id),
                    teams: approvalRule.teams.map((team: Team) => team.id)
                },
                connections: getConnections()
            },
            onCompleted: data => {
                if (data.createApprovalRule.problems.length) {
                    setError({
                        severity: 'warning',
                        message: data.createApprovalRule.problems.map((problem: any) => problem.message).join('; ')
                    });
                } else if (!data.createApprovalRule.approvalRule) {
                    setError({
                        severity: 'error',
                        message: "Unexpected error occurred"
                    });
                } else {
                    navigate(`..`);
                }
            },
            onError: error => {
                setError({
                    severity: 'error',
                    message: `Unexpected error occurred: ${error.message}`
                });
            }
        });
    };

    const disabled = !approvalRule.name ||
        approvalRule.approvalsRequired === '' ||
        approvalRule.users.length + approvalRule.serviceAccounts.length + approvalRule.teams.length === 0;

    return (
        <Box>
            <Typography variant="h5">New Approval Rule</Typography>
            <ApprovalRuleForm
                data={approvalRule}
                orgId={orgId}
                projectId={projectId}
                onChange={(approvalRule: ApprovalRule) => setApprovalRule(approvalRule)}
                error={error}
            />
            <Divider sx={{ opacity: 0.6 }} />
            <Box mt={2}>
                <LoadingButton
                    disabled={disabled}
                    loading={isInFlight}
                    variant="outlined"
                    color="primary"
                    sx={{ mr: 2 }}
                    onClick={onSave}
                >
                    Create Approval Rule
                </LoadingButton>
                <Button component={RouterLink} color="inherit" to={-1 as any}>Cancel</Button>
            </Box>
        </Box>
    );
}

export default NewApprovalRule;
