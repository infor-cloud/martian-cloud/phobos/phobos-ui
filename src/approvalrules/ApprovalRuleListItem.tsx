import { useEffect, useState } from 'react';
import graphql from 'babel-plugin-relay/macro';
import { Box, Button, Chip, Stack, styled, TableCell, TableRow, Tooltip, Typography, useTheme } from '@mui/material';
import Gravatar from '../common/Gravatar';
import { Link as RouterLink } from 'react-router-dom';
import DeleteIcon from '@mui/icons-material/CloseOutlined';
import EditIcon from '@mui/icons-material/EditOutlined';
import { useFragment } from 'react-relay/hooks';
import { User, ServiceAccount, Team } from './ApprovalRuleForm';
import PRNButton from '../common/PRNButton';
import { ApprovalRuleListItemFragment_approvalRule$data, ApprovalRuleListItemFragment_approvalRule$key } from './__generated__/ApprovalRuleListItemFragment_approvalRule.graphql';
import StyledAvatar from '../common/StyledAvatar';
import StackedContainer from '../common/StackedContainer';

const StyledTableRow = styled(
    TableRow
)(() => ({
    '&:last-child td, &:last-child th': {
        border: 0
    }
}));

interface Props {
    showOrgChip?: boolean
    onDelete: (approvalRuleId: string) => void
    fragmentRef: ApprovalRuleListItemFragment_approvalRule$key
}

const buildApproverList = (users: User[], serviceAccounts: ServiceAccount[], teams: Team[]) => {
    return [
        ...users.map((user: User) =>
            ({ id: user.id, label: user.email, tooltip: user.email, type: 'user' })),
        ...serviceAccounts.map((serviceAccount: ServiceAccount) =>
            ({ id: serviceAccount.id, label: serviceAccount.name[0].toUpperCase(), tooltip: serviceAccount.name, type: 'serviceAccount' })),
        ...teams.map((team: Team) =>
            ({ id: team.id, label: team.name[0].toUpperCase(), tooltip: team.name, type: 'team' }))
    ]
}

function ApprovalRuleListItem({ onDelete, fragmentRef, showOrgChip }: Props) {
    const [approverList, setApproverList] = useState<(User | ServiceAccount | Team)[]>([]);
    const theme = useTheme();

    const data = useFragment<ApprovalRuleListItemFragment_approvalRule$key>(
        graphql`
            fragment ApprovalRuleListItemFragment_approvalRule on ApprovalRule
            {
                id
                name
                description
                approvalsRequired
                users {
                    id
                    username
                    email
                }
                serviceAccounts {
                    id
                    name
                }
                teams {
                    id
                    name
                }
                metadata {
                    prn
                }
            }
        `, fragmentRef
    );

    useEffect(() => {
        if (data.users && data.serviceAccounts && data.teams) {
            setApproverList(buildApproverList(
                data.users as User[],
                data.serviceAccounts as ServiceAccount[],
                data.teams as Team[]
            ).map(approver => ({
                ...approver,
                name: approver.label
            })));
        }
    }, [data]);

    const approvalRule = data as ApprovalRuleListItemFragment_approvalRule$data;

    return (
        <StyledTableRow>
            <TableCell>
                <Box sx={{ display: "flex", alignItems: "center" }}>
                    <Typography>{approvalRule.name}</Typography>
                    {showOrgChip && <Chip
                        sx={{ ml: 1, color: theme.palette.text.secondary }}
                        size="xs"
                        label='inherited from org'
                    />}
                </Box>
            </TableCell>
            <TableCell>
                <Typography variant="body2" style={{ maxWidth: 250, wordWrap: 'break-word', overflowWrap: 'break-word' }}>{approvalRule.description || '--'}</Typography>
            </TableCell>
            <TableCell>
                <StackedContainer sx={{ maxWidth: 200 }}>
                    {approverList.map((approver: any) => (
                        <Tooltip key={approver.id} title={approver.tooltip}>
                            <Box>
                                {approver.type == 'user'
                                    ? <Gravatar width={24} height={24} email={approver.label} />
                                    : <StyledAvatar>{approver.label}</StyledAvatar>}
                            </Box>
                        </Tooltip>
                    ))}
                </StackedContainer>
            </TableCell>
            <TableCell align="center">
                <Typography variant="body2">{approvalRule.approvalsRequired}</Typography>
            </TableCell>
            <TableCell>
                <Stack direction="row" spacing={1} justifyContent="flex-end">
                    <PRNButton prn={data.metadata.prn} />
                    {!showOrgChip && <Button
                        sx={{ minWidth: 40, padding: '2px' }}
                        size="small"
                        color="info"
                        variant="outlined"
                        component={RouterLink}
                        to={`${data.id}/edit`}
                    >
                        <EditIcon />
                    </Button>}
                    {!showOrgChip && <Button
                        sx={{ minWidth: 40, padding: '2px' }}
                        size="small"
                        color="info"
                        variant="outlined"
                        onClick={() => onDelete(approvalRule.id)}
                    >
                        <DeleteIcon />
                    </Button>}
                </Stack>
            </TableCell>
        </StyledTableRow>
    );
}

export default ApprovalRuleListItem;
