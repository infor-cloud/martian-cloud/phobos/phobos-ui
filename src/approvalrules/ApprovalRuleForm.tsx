import { Alert, Box, Divider, IconButton, List, ListItem, ListItemText, TextField, Typography, useTheme } from '@mui/material';
import DeleteIcon from '@mui/icons-material/Delete';
import Gravatar from '../common/Gravatar';
import StyledAvatar from '../common/StyledAvatar';
import { MutationError } from '../common/error';
import PrincipalAutocomplete, { Option, UserOption, BaseOption } from '../common/PrincipalAutocomplete';

export interface User {
    id: string
    email: string
    username: string
}

export interface ServiceAccount {
    id: string
    name: string
}

export interface Team {
    id: string
    name: string
}

export interface ApprovalRule {
    readonly id: string
    name: string
    description: string
    approvalsRequired: number | string
    users: User[]
    serviceAccounts: ServiceAccount[]
    teams: Team[]
}

interface Props {
    data: ApprovalRule
    orgId?: string
    projectId?: string
    onChange: (approvalRule: ApprovalRule) => void
    editMode?: boolean
    error?: MutationError
}

function ApprovalRuleForm({ data, orgId, projectId, onChange, editMode, error }: Props) {
    const theme = useTheme();

    const onSelected = (value: Option | null) => {
        if (value) {
            switch (value.type) {
                case 'user': {
                    const user = value as UserOption;
                    onChange({ ...data, users: [...data.users, { id: user.id, email: user.email, username: user.username }] });
                    break;
                }
                case 'team': {
                    const team = value as BaseOption;
                    onChange({ ...data, teams: [...data.teams, { id: team.id, name: team.label }] });
                    break;
                }
                case 'serviceaccount': {
                    const sa = value as BaseOption;
                    onChange({ ...data, serviceAccounts: [...data.serviceAccounts, { id: sa.id, name: sa.label }] });
                    break;
                }
            }
        }
    };

    const approvers = [
        ...data.users.map((user: User) => ({ id: user.id, type: 'user', label: user.email, tooltip: user.username, name: user.username })),
        ...data.serviceAccounts.map((sa: ServiceAccount) => ({ id: sa.id, type: 'serviceaccount', label: sa.name[0].toUpperCase(), tooltip: sa.name, name: sa.name })),
        ...data.teams.map((team: Team) => ({ id: team.id, type: 'team', label: team.name[0].toUpperCase(), tooltip: team.name, name: team.name }))
    ];

    const onDelete = (approver: { id: string; type: 'user' | 'team' | 'serviceaccount' }) => {
        switch (approver.type) {
            case 'user': {
                onChange({ ...data, users: data.users.filter((user: User) => user.id !== approver.id) });
                break;
            }
            case 'team': {
                onChange({ ...data, teams: data.teams.filter((team: Team) => team.id !== approver.id) });
                break;
            }
            case 'serviceaccount': {
                onChange({ ...data, serviceAccounts: data.serviceAccounts.filter((sa: ServiceAccount) => sa.id !== approver.id) });
                break;
            }
        }
    };

    const selectedIds = approvers.reduce((accumulator: Set<string>, item: any) => {
        accumulator.add(item.id);
        return accumulator;
    }, new Set());

    const handleApprovalsRequiredChange = (event: React.ChangeEvent<HTMLInputElement>) => {
        let value: number | string = parseInt(event.target.value);
        value = isNaN(value) ? "" : value > 0 ? value : "";
        onChange({ ...data, approvalsRequired: value });
    };

    return (
        <Box>
            {error && <Alert sx={{ mt: 2 }} severity={error.severity}>
                {error.message}
            </Alert>}
            <Typography sx={{ mt: 2 }} variant="subtitle1" gutterBottom>Details</Typography>
            <Divider sx={{ opacity: 0.6 }} />
            <Box sx={{ mt: 2, mb: 2 }}>
                <TextField
                    disabled={editMode}
                    size="small"
                    fullWidth
                    label="Name"
                    value={data.name}
                    inputProps={{ maxLength: 64 }}
                    onChange={event => onChange({ ...data, name: event.target.value })}
                />
                <TextField
                    size="small"
                    margin='normal'
                    fullWidth
                    label="Description"
                    autoComplete="off"
                    value={data.description}
                    inputProps={{ maxLength: 64 }}
                    onChange={event => onChange({ ...data, description: event.target.value })}
                />
                <Box sx={{ mb: 4 }}>
                    <TextField
                        disabled={editMode}
                        sx={{ maxWidth: 250, mb: 1 }}
                        size="small"
                        margin="normal"
                        label="Approvals Required"
                        type="number"
                        autoComplete="off"
                        value={data.approvalsRequired}
                        onChange={handleApprovalsRequiredChange}
                    />
                    <Typography variant="subtitle2" gutterBottom>A rule requires a minimum of at least 1 approver</Typography>
                </Box>
                <>
                    <Typography sx={{ mb: 1 }} variant="body1">Add Approvers</Typography>
                    <Box sx={{ mb: 4, p: 2, border: `1px solid ${theme.palette.divider}`, borderRadius: '4px' }}>
                        <Box sx={{ mb: 2 }}>
                            <PrincipalAutocomplete
                                orgId={orgId}
                                projectId={projectId}
                                placeholder='Select an approver to add'
                                onSelected={onSelected}
                                filterOptions={(options: Option[]) => options.filter(option => !selectedIds.has(option.id))}
                            />
                        </Box>
                        <Typography color="textSecondary">
                            {approvers.length} approver{approvers.length === 1 ? '' : 's'} selected
                        </Typography>
                        <List dense>
                            {approvers.map((approver: any) => (
                                <ListItem
                                    disableGutters
                                    secondaryAction={
                                        <IconButton onClick={() => onDelete(approver)}>
                                            <DeleteIcon />
                                        </IconButton>}
                                    key={approver.id}>
                                    {approver.type === 'user' && <Gravatar sx={{ marginRight: 1 }} width={24} height={24} email={approver.label} />}
                                    {approver.type !== 'user' &&
                                        <StyledAvatar sx={{ mr: 1 }}>{approver.label}</StyledAvatar>}
                                    <ListItemText primary={approver.name} primaryTypographyProps={{ noWrap: true }} />
                                </ListItem>
                            ))}
                        </List>
                    </Box>
                </>
            </Box>
        </Box>
    );
}

export default ApprovalRuleForm;
