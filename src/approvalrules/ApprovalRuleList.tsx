import { LoadingButton } from '@mui/lab';
import { Alert, Box, Button, Dialog, DialogActions, DialogContent, DialogTitle, Paper, Table, TableBody, TableCell, TableContainer, TableHead, TableRow, Typography, useTheme } from '@mui/material';
import graphql from 'babel-plugin-relay/macro';
import { useState } from 'react';
import InfiniteScroll from 'react-infinite-scroll-component';
import { LoadMoreFn, useFragment, useMutation } from 'react-relay/hooks';
import { Link as RouterLink } from 'react-router-dom';
import { MutationError } from '../common/error';
import ListSkeleton from '../skeletons/ListSkeleton';
import { ApprovalRuleListDeleteMutation } from './__generated__/ApprovalRuleListDeleteMutation.graphql';
import ApprovalRuleListItem from './ApprovalRuleListItem';
import { ApprovalRuleListFragment_approvalRules$key } from './__generated__/ApprovalRuleListFragment_approvalRules.graphql';
import { OrgApprovalRulesPaginationQuery } from '../organizations/approvalrules/__generated__/OrgApprovalRulesPaginationQuery.graphql';
import { ProjectApprovalRulesPaginationQuery } from '../organizations/projects/approvalrules/__generated__/ProjectApprovalRulesPaginationQuery.graphql';

const DESCRIPTION = 'Approval rules specify the users, service accounts, and teams authorized to approve pipeline tasks.';

interface ConfirmationDialogProps {
    error?: MutationError
    deleteInProgress: boolean
    onClose: (confirm?: boolean) => void
}

function DeleteConfirmationDialog({ error, deleteInProgress, onClose, ...other }: ConfirmationDialogProps) {
    return (
        <Dialog
            maxWidth="xs"
            open
            {...other}
        >
            <DialogTitle>Delete Approval Rule</DialogTitle>
            <DialogContent dividers>
                {error && <Alert sx={{ mb: 2 }} severity={error.severity}>
                    {error.message}
                </Alert>}
                Are you sure you want to delete this approval rule?
            </DialogContent>
            <DialogActions>
                <Button color="inherit" onClick={() => onClose()}>
                    Cancel
                </Button>
                <LoadingButton
                    color="error"
                    loading={deleteInProgress}
                    onClick={() => onClose(true)}>
                    Delete
                </LoadingButton>
            </DialogActions>
        </Dialog>
    );
}

interface Props {
    loadNext: LoadMoreFn<OrgApprovalRulesPaginationQuery | ProjectApprovalRulesPaginationQuery>
    hasNext: boolean
    showOrgChip?: boolean
    fragmentRef: ApprovalRuleListFragment_approvalRules$key
    getConnections: () => [string]
}

function ApprovalRuleList({ fragmentRef, loadNext, hasNext, showOrgChip, getConnections }: Props) {
    const theme = useTheme();
    const [approvalRuleIdToDelete, setApprovalRuleIdToDelete] = useState<string | null>(null);
    const [error, setError] = useState<MutationError>();

    const data = useFragment<ApprovalRuleListFragment_approvalRules$key>(
        graphql`
            fragment ApprovalRuleListFragment_approvalRules on ApprovalRuleConnection
            {
                totalCount
                edges {
                    node {
                        id
                        scope
                        ...ApprovalRuleListItemFragment_approvalRule
                        }
                }
            }
    `, fragmentRef);

    const [commitDeleteApprovalRule, commitDeleteApprovalRuleInFlight] = useMutation<ApprovalRuleListDeleteMutation>(
        graphql`
        mutation ApprovalRuleListDeleteMutation($input: DeleteApprovalRuleInput!, $connections: [ID!]!) {
            deleteApprovalRule(input: $input) {
                approvalRule {
                    id @deleteEdge(connections: $connections)
                }
                problems {
                    message
                    field
                    type
                }
            }
        }`
    );

    const onDeleteConfirmationDialogClosed = (confirm?: boolean) => {
        if (confirm && approvalRuleIdToDelete) {
            commitDeleteApprovalRule({
                variables: {
                    input: {
                        id: approvalRuleIdToDelete
                    },
                    connections: getConnections()
                },
                onCompleted: (data) => {
                    if (data.deleteApprovalRule.problems.length) {
                        setError({
                            severity: 'warning',
                            message: data.deleteApprovalRule.problems.map((problem: any) => problem.message).join('; ')
                        })
                    }
                    else {
                        setApprovalRuleIdToDelete(null);
                    }
                },
                onError: (error) => {
                    setError({
                        severity: 'error',
                        message: `Unexpected error occurred: ${error.message}`
                    })
                }
            })
        } else {
            setApprovalRuleIdToDelete(null);
            setError(undefined);
        }
    };

    const edges = data?.edges ?? [];

    return (
        <Box>
            <Box>
                <Box sx={{
                    display: 'flex',
                    flexDirection: 'row',
                    justifyContent: 'space-between',
                    marginBottom: 2,
                    [theme.breakpoints.down('md')]: {
                        flexDirection: 'column',
                        alignItems: 'flex-start',
                        '& > *': { mb: 2 },
                    }
                }}>
                    <Box>
                        <Typography variant="h5" gutterBottom>Approval Rules</Typography>
                        <Typography variant="body2">
                            {DESCRIPTION}
                        </Typography>
                    </Box>
                    <Box>
                        <Button sx={{ minWidth: 200 }}
                            variant="outlined"
                            component={RouterLink}
                            to="new"
                        >
                            Add Approval Rule
                        </Button>
                    </Box>
                </Box>
                <TableContainer component={Paper}>
                    <InfiniteScroll
                        dataLength={edges.length ?? 0}
                        next={() => loadNext(20)}
                        hasMore={hasNext}
                        loader={<ListSkeleton rowCount={3} />}
                    >
                        <Table>
                            <TableHead>
                                <TableRow>
                                    <TableCell>Name</TableCell>
                                    <TableCell>Description</TableCell>
                                    <TableCell>Approvers</TableCell>
                                    <TableCell align="center">Approvals Required</TableCell>
                                    <TableCell></TableCell>
                                </TableRow>
                            </TableHead>
                            <TableBody>
                                {edges.map((edge: any) => (
                                    <ApprovalRuleListItem
                                        showOrgChip={showOrgChip && edge.node.scope === 'ORGANIZATION'}
                                        key={edge.node.id}
                                        onDelete={setApprovalRuleIdToDelete}
                                        fragmentRef={edge.node}
                                    />
                                ))}
                            </TableBody>
                        </Table>
                    </InfiniteScroll>
                </TableContainer>
            </Box>
            {approvalRuleIdToDelete && <DeleteConfirmationDialog
                error={error}
                deleteInProgress={commitDeleteApprovalRuleInFlight}
                onClose={onDeleteConfirmationDialogClosed}
            />}
        </Box>
    );
}

export default ApprovalRuleList;
