import { List } from '@mui/material';
import graphql from 'babel-plugin-relay/macro';
import { useFragment } from "react-relay/hooks";
import { ActivityEventListFragment_connection$key } from './__generated__/ActivityEventListFragment_connection.graphql';
import ActivityEventAgentTarget from './targets/ActivityEventAgentTarget';
import ActivityEventApprovalRuleTarget from './targets/ActivityEventApprovalRuleTarget';
import ActivityEventCommentTarget from './targets/ActivityEventCommentTarget';
import ActivityEventEnvironmentRuleTarget from './targets/ActivityEventEnvironmentRuleTarget';
import ActivityEventEnvironmentTarget from './targets/ActivityEventEnvironmentTarget';
import ActivityEventMembershipTarget from './targets/ActivityEventMembershipTarget';
import ActivityEventOrganizationTarget from './targets/ActivityEventOrganizationTarget';
import ActivityEventPipelineTarget from './targets/ActivityEventPipelineTarget';
import ActivityEventPipelineTemplateTarget from './targets/ActivityEventPipelineTemplateTarget';
import ActivityEventPluginTarget from './targets/ActivityEventPluginTarget';
import ActivityEventPluginVersionTarget from './targets/ActivityEventPluginVersionTarget';
import ActivityEventProjectTarget from './targets/ActivityEventProjectTarget';
import ActivityEventProjectVariableSetTarget from './targets/ActivityEventProjectVariableSetTarget';
import ActivityEventReleaseLifecycleTarget from './targets/ActivityEventReleaseLifecycleTarget';
import ActivityEventReleaseTarget from './targets/ActivityEventReleaseTarget';
import ActivityEventRoleTarget from './targets/ActivityEventRoleTarget';
import ActivityEventServiceAccountTarget from './targets/ActivityEventServiceAccountTarget';
import ActivityEventTeamTarget from './targets/ActivityEventTeamTarget';
import ActivityEventThreadTarget from './targets/ActivityEventThreadTarget';
import ActivityEventVCSProviderTarget from './targets/ActivityEventVCSProviderTarget';

const TARGET_COMPONENT_MAP = {
    Organization: ActivityEventOrganizationTarget,
    Project: ActivityEventProjectTarget,
    ProjectVariableSet: ActivityEventProjectVariableSetTarget,
    ReleaseLifecycle: ActivityEventReleaseLifecycleTarget,
    ServiceAccount: ActivityEventServiceAccountTarget,
    Membership: ActivityEventMembershipTarget,
    Agent: ActivityEventAgentTarget,
    Environment: ActivityEventEnvironmentTarget,
    Release: ActivityEventReleaseTarget,
    Pipeline: ActivityEventPipelineTarget,
    PipelineTemplate: ActivityEventPipelineTemplateTarget,
    ApprovalRule: ActivityEventApprovalRuleTarget,
    Team: ActivityEventTeamTarget,
    Role: ActivityEventRoleTarget,
    Plugin: ActivityEventPluginTarget,
    PluginVersion: ActivityEventPluginVersionTarget,
    Comment: ActivityEventCommentTarget,
    Thread: ActivityEventThreadTarget,
    VCSProvider: ActivityEventVCSProviderTarget,
    EnvironmentRule: ActivityEventEnvironmentRuleTarget
} as any;

interface Props {
    fragmentRef: ActivityEventListFragment_connection$key
    showProjectLink?: boolean
    showOrgLink?: boolean
}

function ActivityEventList({ fragmentRef, showOrgLink, showProjectLink }: Props) {
    const data = useFragment<ActivityEventListFragment_connection$key>(
        graphql`
            fragment ActivityEventListFragment_connection on ActivityEventConnection {
                edges {
                    node {
                        id
                        action
                        target {
                            __typename
                        }
                        ...ActivityEventOrganizationTargetFragment_event
                        ...ActivityEventProjectTargetFragment_event
                        ...ActivityEventProjectVariableSetTargetFragment_event
                        ...ActivityEventReleaseLifecycleTargetFragment_event
                        ...ActivityEventServiceAccountTargetFragment_event
                        ...ActivityEventMembershipTargetFragment_event
                        ...ActivityEventAgentTargetFragment_event
                        ...ActivityEventEnvironmentTargetFragment_event
                        ...ActivityEventEnvironmentRuleTargetFragment_event
                        ...ActivityEventReleaseTargetFragment_event
                        ...ActivityEventPipelineTargetFragment_event
                        ...ActivityEventPipelineTemplateTargetFragment_event
                        ...ActivityEventApprovalRuleTargetFragment_event
                        ...ActivityEventTeamTargetFragment_event
                        ...ActivityEventRoleTargetFragment_event
                        ...ActivityEventPluginTargetFragment_event
                        ...ActivityEventPluginVersionTargetFragment_event
                        ...ActivityEventCommentTargetFragment_event
                        ...ActivityEventThreadTargetFragment_event
                        ...ActivityEventVCSProviderTargetFragment_event
                    }
                }
            }
        `,
        fragmentRef
    );

    return (
        <List sx={{ paddingTop: 0 }}>
            {data?.edges?.map((edge: any) => {
                const Target = TARGET_COMPONENT_MAP[edge.node.target?.__typename];
                return Target ?
                    <Target
                        key={edge.node.id}
                        fragmentRef={edge.node}
                        showOrgLink={showOrgLink}
                        showProjectLink={showProjectLink}
                        showReleaseLink={true}
                        showPipelineLink={true}
                    /> : null;
            })}
        </List>
    );
}

export default ActivityEventList;
