/**
 * @generated SignedSource<<eb18b030867d17751219a211f488d48b>>
 * @lightSyntaxTransform
 * @nogrep
 */

/* tslint:disable */
/* eslint-disable */
// @ts-nocheck

import { Fragment, ReaderFragment } from 'relay-runtime';
export type ActivityEventAction = "ADD_PARTICIPANT" | "APPROVE" | "CANCEL" | "CREATE" | "CREATE_MEMBERSHIP" | "DEFER" | "DELETE" | "REMOVE_MEMBERSHIP" | "REMOVE_PARTICIPANT" | "RETRY" | "REVOKE_APPROVAL" | "SCHEDULE" | "SKIP" | "START" | "UNDEFER" | "UNSCHEDULE" | "UPDATE" | "%future added value";
import { FragmentRefs } from "relay-runtime";
export type ActivityEventListFragment_connection$data = {
  readonly edges: ReadonlyArray<{
    readonly node: {
      readonly action: ActivityEventAction;
      readonly id: string;
      readonly target: {
        readonly __typename: string;
      } | null | undefined;
      readonly " $fragmentSpreads": FragmentRefs<"ActivityEventAgentTargetFragment_event" | "ActivityEventApprovalRuleTargetFragment_event" | "ActivityEventCommentTargetFragment_event" | "ActivityEventEnvironmentRuleTargetFragment_event" | "ActivityEventEnvironmentTargetFragment_event" | "ActivityEventMembershipTargetFragment_event" | "ActivityEventOrganizationTargetFragment_event" | "ActivityEventPipelineTargetFragment_event" | "ActivityEventPipelineTemplateTargetFragment_event" | "ActivityEventPluginTargetFragment_event" | "ActivityEventPluginVersionTargetFragment_event" | "ActivityEventProjectTargetFragment_event" | "ActivityEventProjectVariableSetTargetFragment_event" | "ActivityEventReleaseLifecycleTargetFragment_event" | "ActivityEventReleaseTargetFragment_event" | "ActivityEventRoleTargetFragment_event" | "ActivityEventServiceAccountTargetFragment_event" | "ActivityEventTeamTargetFragment_event" | "ActivityEventThreadTargetFragment_event" | "ActivityEventVCSProviderTargetFragment_event">;
    } | null | undefined;
  } | null | undefined> | null | undefined;
  readonly " $fragmentType": "ActivityEventListFragment_connection";
};
export type ActivityEventListFragment_connection$key = {
  readonly " $data"?: ActivityEventListFragment_connection$data;
  readonly " $fragmentSpreads": FragmentRefs<"ActivityEventListFragment_connection">;
};

const node: ReaderFragment = {
  "argumentDefinitions": [],
  "kind": "Fragment",
  "metadata": null,
  "name": "ActivityEventListFragment_connection",
  "selections": [
    {
      "alias": null,
      "args": null,
      "concreteType": "ActivityEventEdge",
      "kind": "LinkedField",
      "name": "edges",
      "plural": true,
      "selections": [
        {
          "alias": null,
          "args": null,
          "concreteType": "ActivityEvent",
          "kind": "LinkedField",
          "name": "node",
          "plural": false,
          "selections": [
            {
              "alias": null,
              "args": null,
              "kind": "ScalarField",
              "name": "id",
              "storageKey": null
            },
            {
              "alias": null,
              "args": null,
              "kind": "ScalarField",
              "name": "action",
              "storageKey": null
            },
            {
              "alias": null,
              "args": null,
              "concreteType": null,
              "kind": "LinkedField",
              "name": "target",
              "plural": false,
              "selections": [
                {
                  "alias": null,
                  "args": null,
                  "kind": "ScalarField",
                  "name": "__typename",
                  "storageKey": null
                }
              ],
              "storageKey": null
            },
            {
              "args": null,
              "kind": "FragmentSpread",
              "name": "ActivityEventOrganizationTargetFragment_event"
            },
            {
              "args": null,
              "kind": "FragmentSpread",
              "name": "ActivityEventProjectTargetFragment_event"
            },
            {
              "args": null,
              "kind": "FragmentSpread",
              "name": "ActivityEventProjectVariableSetTargetFragment_event"
            },
            {
              "args": null,
              "kind": "FragmentSpread",
              "name": "ActivityEventReleaseLifecycleTargetFragment_event"
            },
            {
              "args": null,
              "kind": "FragmentSpread",
              "name": "ActivityEventServiceAccountTargetFragment_event"
            },
            {
              "args": null,
              "kind": "FragmentSpread",
              "name": "ActivityEventMembershipTargetFragment_event"
            },
            {
              "args": null,
              "kind": "FragmentSpread",
              "name": "ActivityEventAgentTargetFragment_event"
            },
            {
              "args": null,
              "kind": "FragmentSpread",
              "name": "ActivityEventEnvironmentTargetFragment_event"
            },
            {
              "args": null,
              "kind": "FragmentSpread",
              "name": "ActivityEventEnvironmentRuleTargetFragment_event"
            },
            {
              "args": null,
              "kind": "FragmentSpread",
              "name": "ActivityEventReleaseTargetFragment_event"
            },
            {
              "args": null,
              "kind": "FragmentSpread",
              "name": "ActivityEventPipelineTargetFragment_event"
            },
            {
              "args": null,
              "kind": "FragmentSpread",
              "name": "ActivityEventPipelineTemplateTargetFragment_event"
            },
            {
              "args": null,
              "kind": "FragmentSpread",
              "name": "ActivityEventApprovalRuleTargetFragment_event"
            },
            {
              "args": null,
              "kind": "FragmentSpread",
              "name": "ActivityEventTeamTargetFragment_event"
            },
            {
              "args": null,
              "kind": "FragmentSpread",
              "name": "ActivityEventRoleTargetFragment_event"
            },
            {
              "args": null,
              "kind": "FragmentSpread",
              "name": "ActivityEventPluginTargetFragment_event"
            },
            {
              "args": null,
              "kind": "FragmentSpread",
              "name": "ActivityEventPluginVersionTargetFragment_event"
            },
            {
              "args": null,
              "kind": "FragmentSpread",
              "name": "ActivityEventCommentTargetFragment_event"
            },
            {
              "args": null,
              "kind": "FragmentSpread",
              "name": "ActivityEventThreadTargetFragment_event"
            },
            {
              "args": null,
              "kind": "FragmentSpread",
              "name": "ActivityEventVCSProviderTargetFragment_event"
            }
          ],
          "storageKey": null
        }
      ],
      "storageKey": null
    }
  ],
  "type": "ActivityEventConnection",
  "abstractKey": null
};

(node as any).hash = "0609772d03b6497eaec14ec6c00b8ebb";

export default node;
