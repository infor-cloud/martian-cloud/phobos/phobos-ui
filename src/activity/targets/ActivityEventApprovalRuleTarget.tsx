import React from 'react';
import { Typography } from '@mui/material';
import graphql from 'babel-plugin-relay/macro';
import { useFragment } from 'react-relay/hooks';
import { ApprovalRuleIcon } from '../../common/Icons';
import ActivityEventLink from '../ActivityEventLink';
import ActivityEventListItem from '../ActivityEventListItem';
import { ActivityEventApprovalRuleTargetFragment_event$key } from './__generated__/ActivityEventApprovalRuleTargetFragment_event.graphql';

const ACTION_TEXT = {
    CREATE: 'created',
    UPDATE: 'updated',
} as any;

interface Props {
    showOrgLink: boolean
    fragmentRef: ActivityEventApprovalRuleTargetFragment_event$key
}

function ActivityEventApprovalRuleTarget({ showOrgLink, fragmentRef }: Props) {
    const data = useFragment<ActivityEventApprovalRuleTargetFragment_event$key>(
        graphql`
        fragment ActivityEventApprovalRuleTargetFragment_event on ActivityEvent
        {
            action
            target {
                ...on ApprovalRule {
                    name
                    scope
                    metadata {
                        prn
                    }
                }
            }
            ...ActivityEventListItemFragment_event
        }
      `, fragmentRef);

    const actionText = ACTION_TEXT[data.action];
    const approvalRule = data.target;
    const scope = approvalRule?.scope;
    const orgName = approvalRule?.metadata?.prn.split(':')[2].split('/')[0];
    const projectName = scope === 'PROJECT' ? approvalRule?.metadata?.prn.split(':')[2].split('/')[1] : null;
    const baseLink = `/organizations/${orgName}`;

    const contextLink = scope === 'ORGANIZATION' ?
        showOrgLink && <span> in organization <ActivityEventLink to={baseLink}>{orgName}</ActivityEventLink></span>
        :
        <span> in project <ActivityEventLink to={`${baseLink}/projects/${projectName}`}>{projectName}</ActivityEventLink></span>;

    return (
        <ActivityEventListItem
            fragmentRef={data}
            icon={<ApprovalRuleIcon />}
            primary={<React.Fragment>
                Approval rule <Typography component="span" sx={{ fontWeight: 500 }}>{approvalRule?.name}</Typography>  {actionText}
                {contextLink}
            </React.Fragment>}
        />
    );
}

export default ActivityEventApprovalRuleTarget;
