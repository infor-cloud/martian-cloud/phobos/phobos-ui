import React, { useMemo } from 'react';
import graphql from 'babel-plugin-relay/macro';
import { useFragment } from 'react-relay/hooks';
import { PipelineTemplateIcon } from '../../common/Icons';
import ActivityEventLink from '../ActivityEventLink';
import ActivityEventListItem from '../ActivityEventListItem';
import { ActivityEventPipelineTemplateTargetFragment_event$key } from './__generated__/ActivityEventPipelineTemplateTargetFragment_event.graphql';

const ACTION_TEXT = {
    CREATE: 'created',
    UPDATE: 'updated',
} as any;

interface Props {
    showProjectLink: boolean
    fragmentRef: ActivityEventPipelineTemplateTargetFragment_event$key
}

function ActivityEventPipelineTemplateTarget({ showProjectLink, fragmentRef }: Props) {
    const data = useFragment<ActivityEventPipelineTemplateTargetFragment_event$key>(
        graphql`
        fragment ActivityEventPipelineTemplateTargetFragment_event on ActivityEvent
        {
            action
            target {
                ...on PipelineTemplate {
                    id
                    versioned
                    templateName: name
                    templateSemanticVersion: semanticVersion
                    metadata {
                        prn
                    }
                }
            }
            ...ActivityEventListItemFragment_event
        }
      `, fragmentRef);

    const actionText = ACTION_TEXT[data.action];
    const pipelineTemplate = data.target as any;
    const orgName = pipelineTemplate.metadata.prn.split(':')[2].split('/')[0];
    const projectName = pipelineTemplate.metadata.prn.split(':')[2].split('/')[1];
    const pipelineTemplateRoute = `/organizations/${orgName}/projects/${projectName}/-/pipeline_templates/${pipelineTemplate.id}`;

    const pipelineTemplateLabel = useMemo(() => {
        if (pipelineTemplate.versioned) {
            return `${pipelineTemplate.templateName}:${pipelineTemplate.templateSemanticVersion}`
        } else {
            return `${pipelineTemplate.id.substring(0, 8)}...`
        }
    }, [pipelineTemplate.id]);

    return (
        <ActivityEventListItem
            fragmentRef={data}
            icon={<PipelineTemplateIcon />}
            primary={<React.Fragment>
                {pipelineTemplate.versioned ? 'Versioned' : 'Non-Versioned'} Pipeline template <ActivityEventLink to={pipelineTemplateRoute}>{pipelineTemplateLabel}</ActivityEventLink> {actionText}
                {showProjectLink && <span> in project <ActivityEventLink to={`/organizations/${orgName}/projects/${projectName}`}>{projectName}</ActivityEventLink></span>}
            </React.Fragment>}
        />
    );
}

export default ActivityEventPipelineTemplateTarget;
