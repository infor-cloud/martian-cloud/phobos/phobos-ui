import React from 'react';
import graphql from 'babel-plugin-relay/macro';
import { useFragment } from 'react-relay/hooks';
import { ReleaseIcon } from '../../common/Icons';
import ActivityEventLink from '../ActivityEventLink';
import ActivityEventListItem from '../ActivityEventListItem';
import { ActivityEventReleaseTargetFragment_event$key } from './__generated__/ActivityEventReleaseTargetFragment_event.graphql';

const CHANGE_TYPE = {
    ADD_PARTICIPANT: 'added',
    REMOVE_PARTICIPANT: 'removed'
} as any;

const CHANGE_TYPE_PREPS = {
    ADD_PARTICIPANT: 'to',
    REMOVE_PARTICIPANT: 'from'
} as any;

const ACTION_TYPE = {
    CREATE: 'created',
    UPDATE: 'updated',
    DELETE: 'deleted'
} as any;

const RESOURCE_TYPE = {
    TEAM: 'Team',
    USER: 'User',
    COMMENT: 'Comment',
    THREAD: 'Conversation'
} as any;

function getParticipantIdentifier(type: 'TEAM' | 'USER', resource: any) {
    if (!type || !resource) {
        return 'n/a';
    }

    switch (type) {
        case 'USER':
            return resource.username;
        case 'TEAM':
            return <ActivityEventLink to={`/teams/${resource.name}`}>{resource.name}</ActivityEventLink>;
    }
}

interface Props {
    showProjectLink: boolean
    showReleaseLink: boolean
    fragmentRef: ActivityEventReleaseTargetFragment_event$key
}

function ActivityEventReleaseTarget({ showProjectLink, showReleaseLink, fragmentRef }: Props) {

    const data = useFragment<ActivityEventReleaseTargetFragment_event$key>(
        graphql`
            fragment ActivityEventReleaseTargetFragment_event on ActivityEvent
            {
                project {
                    name
                    organizationName
                }
                target {
                    ...on Release {
                        id
                        semanticVersion
                        metadata {
                            prn
                        }
                    }
                }
                action
                payload {
                    __typename
                    ... on ActivityEventUpdateReleasePayload {
                        changeType
                        type
                        resource {
                            ... on Team {
                                name
                            }
                            ... on User {
                                username
                            }
                        }
                    }
                    ...on ActivityEventDeleteResourcePayload {
                        type
                    }
                }
                ...ActivityEventListItemFragment_event
            }
        `, fragmentRef);

    const payload = data.payload as any;
    const release = data.target as any;
    const orgName = data.project?.organizationName as string;
    const projectName = data.project?.name as string;
    const releaseRoute = `/organizations/${orgName}/projects/${projectName}/-/releases/${release.id}`;

    let primary;

    if (payload) {
        switch (payload.__typename) {
            case 'ActivityEventUpdateReleasePayload':
                primary = (
                    <React.Fragment>
                        {RESOURCE_TYPE[payload.type]}{' '}
                        <span style={{ fontWeight: 500 }}>{getParticipantIdentifier(payload.type, payload.resource)}</span>{' '}
                        {CHANGE_TYPE[payload.changeType]} {showReleaseLink && <span>{CHANGE_TYPE_PREPS[payload.changeType]} release{' '}
                            <ActivityEventLink to={releaseRoute}>{release.semanticVersion}</ActivityEventLink></span>}
                    </React.Fragment>
                );
                break;
            case 'ActivityEventDeleteResourcePayload': {
                primary = (
                    <React.Fragment>
                        {RESOURCE_TYPE[payload.type]}{' '}
                        {ACTION_TYPE[data.action]} {showReleaseLink && <span> in release{' '}
                            <ActivityEventLink to={releaseRoute}>{release.semanticVersion}</ActivityEventLink></span>}
                    </React.Fragment>
                );
                break;
            }
            default:
                null;
        }
    } else {
        primary = (
            <React.Fragment>
                Release{' '}
                <ActivityEventLink
                    to={releaseRoute}>{release.semanticVersion}
                </ActivityEventLink> {ACTION_TYPE[data.action]}{showProjectLink && <span> in project{' '}
                    <ActivityEventLink
                        to={`/organizations/${orgName}/projects/${projectName}`}>{projectName}
                    </ActivityEventLink></span>}
            </React.Fragment>
        );
    }

    return (
        <ActivityEventListItem
            fragmentRef={data}
            icon={<ReleaseIcon />}
            primary={primary}
        />
    );
}

export default ActivityEventReleaseTarget;
