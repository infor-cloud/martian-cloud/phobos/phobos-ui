import React from 'react';
import { Typography } from '@mui/material';
import graphql from 'babel-plugin-relay/macro';
import { useFragment } from 'react-relay/hooks';
import { ProjectIcon } from '../../common/Icons';
import ActivityEventLink from '../ActivityEventLink';
import ActivityEventListItem from '../ActivityEventListItem';
import { ActivityEventProjectTargetFragment_event$key } from './__generated__/ActivityEventProjectTargetFragment_event.graphql';
import { ShowChart } from '@mui/icons-material';

const ACTION_TEXT = {
    CREATE: 'created',
    DELETE: 'deleted',
    UPDATE: 'updated',
    CREATE_MEMBERSHIP: 'added to',
    REMOVE_MEMBERSHIP: 'removed from',
} as any;

const RESOURCE_TYPES = {
    VARIABLE: 'Variable',
    RELEASE: 'Release',
    ENVIRONMENT: 'Environment',
    PIPELINE_TEMPLATE: 'Pipeline template',
    VCS_PROVIDER: 'VCS provider',
    ENVIRONMENT_RULE: 'Environment protection rule',
    SERVICE_ACCOUNT: 'Service account',
    RELEASE_LIFECYCLE: 'Release lifecycle',
    APPROVAL_RULE: 'Approval rule'
} as any;

const MEMBER_TYPES = {
    User: 'User',
    ServiceAccount: 'Service account',
    Team: 'Team'
} as any;

function getMemberIdentifier(member: any) {
    if (!member) {
        return 'n/a';
    }
    switch (member.__typename) {
        case 'User':
            return member.username;
        case 'ServiceAccount':
            return <ActivityEventLink to={`/organizations/${member.metadata.prn.split(':')[2].split('/')[0]}/-/service_accounts/${member.id}`}>{member.name}</ActivityEventLink>;
        case 'Team':
            return <ActivityEventLink to={`/teams/${member.name}`}>{member.name}</ActivityEventLink>;
    }
}

interface Props {
    showOrgLink: boolean
    showProjectLink: boolean
    fragmentRef: ActivityEventProjectTargetFragment_event$key
}

function ActivityEventProjectTarget({ showOrgLink, showProjectLink, fragmentRef }: Props) {
    const data = useFragment<ActivityEventProjectTargetFragment_event$key>(
        graphql`
        fragment ActivityEventProjectTargetFragment_event on ActivityEvent
        {
            action
            target {
                ...on Project {
                    name
                    description
                    metadata {
                        prn
                    }
                }
            }
            payload {
                __typename
                ...on ActivityEventDeleteResourcePayload {
                    name
                    type
                }
                ...on ActivityEventCreateMembershipPayload {
                    member {
                      __typename
                      ... on User {
                        username
                      }
                      ... on ServiceAccount {
                        id
                        name
                        metadata {
                            prn
                        }
                      }
                      ... on Team {
                        name
                      }
                    }
                    role
                  }
                ...on ActivityEventRemoveMembershipPayload {
                    member {
                      __typename
                      ... on User {
                        username
                      }
                      ... on ServiceAccount {
                        id
                        name
                        metadata {
                            prn
                        }
                      }
                      ... on Team {
                        name
                      }
                    }
                }
            }
            ...ActivityEventListItemFragment_event
        }
      `, fragmentRef);

    const actionText = ACTION_TEXT[data.action];
    const proj = data.target as any;
    const payload = data.payload as any;
    const orgName = proj.metadata.prn.split(':')[2].split('/')[0];

    const route = <ActivityEventLink to={`/organizations/${orgName}/projects/${proj.name}`}>{proj.name}</ActivityEventLink>;

    let primary;

    if (['CREATE', 'UPDATE'].includes(data.action)) {
        primary = <React.Fragment>Project {route} {actionText}</React.Fragment>;
    } else if ('CREATE_MEMBERSHIP' === data.action) {
        primary = <React.Fragment>{MEMBER_TYPES[payload?.member?.__typename] || 'Unknown member type'}
            <Typography component="span" sx={{ fontWeight: 500 }}>{' '}{getMemberIdentifier(payload?.member)}</Typography> added
            {(showOrgLink || showProjectLink || !ShowChart) && <span> to project {route}</span>}
            {' '}with role {payload?.role}</React.Fragment>;
    } else if ('REMOVE_MEMBERSHIP' === data.action) {
        primary = <React.Fragment>{MEMBER_TYPES[payload?.member?.__typename] || 'Unknown member type'}
            <Typography component="span" sx={{ fontWeight: 500 }}>{' '}{getMemberIdentifier(payload?.member)}</Typography> removed
            {(showOrgLink || showProjectLink) && <span> from project {route}</span>}</React.Fragment>;
    } else if (data.action === 'DELETE') {
        primary = <React.Fragment>{RESOURCE_TYPES[payload.type] || 'Unknown resource type'} <Typography component="span" sx={{ fontWeight: 500 }}>{payload.name}</Typography> deleted
            {(showOrgLink || showProjectLink) && <span> from project {route}</span>}</React.Fragment>;
    }

    return (
        <ActivityEventListItem
            fragmentRef={data}
            icon={<ProjectIcon />}
            primary={primary}
        />
    );
}

export default ActivityEventProjectTarget;
