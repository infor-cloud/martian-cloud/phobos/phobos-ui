import graphql from 'babel-plugin-relay/macro';
import React from 'react';
import { useFragment } from 'react-relay/hooks';
import ExtensionIcon from '@mui/icons-material/Extension';
import ActivityEventLink from '../ActivityEventLink';
import ActivityEventListItem from '../ActivityEventListItem';
import { ActivityEventPluginVersionTargetFragment_event$key } from './__generated__/ActivityEventPluginVersionTargetFragment_event.graphql';

const ACTION_TEXT = {
    CREATE: 'created',
} as any;

interface Props {
    fragmentRef: ActivityEventPluginVersionTargetFragment_event$key
}

function ActivityEventPluginVersionTarget({ fragmentRef }: Props) {
    const data = useFragment<ActivityEventPluginVersionTargetFragment_event$key>(
        graphql`
        fragment ActivityEventPluginVersionTargetFragment_event on ActivityEvent
        {
            action
            target {
                ...on PluginVersion {
                    version
                    plugin {
                        name
                        organizationName
                    }
                }
            }
            ...ActivityEventListItemFragment_event
        }
      `, fragmentRef);

    const actionText = ACTION_TEXT[data.action];
    const pluginVersion = data.target as any;

    return (
        <ActivityEventListItem
            fragmentRef={data}
            icon={<ExtensionIcon />}
            primary={<React.Fragment>
                Version <ActivityEventLink to={`/plugin-registry/${pluginVersion.plugin.organizationName}/${pluginVersion.plugin.name}/${pluginVersion.version}`}>{pluginVersion.version}</ActivityEventLink> {actionText} for plugin {pluginVersion.plugin.name}
            </React.Fragment>}
        />
    );
}

export default ActivityEventPluginVersionTarget;
