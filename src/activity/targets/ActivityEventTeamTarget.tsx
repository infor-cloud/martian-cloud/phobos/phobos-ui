import React from 'react';
import { Typography } from '@mui/material';
import graphql from 'babel-plugin-relay/macro';
import { useFragment } from 'react-relay/hooks';
import { TeamIcon } from '../../common/Icons';
import ActivityEventListItem from '../ActivityEventListItem';
import ActivityEventLink from '../ActivityEventLink';
import { ActivityEventTeamTargetFragment_event$key } from './__generated__/ActivityEventTeamTargetFragment_event.graphql';

interface Props {
    fragmentRef: ActivityEventTeamTargetFragment_event$key
}

function ActivityEventTeamTarget({ fragmentRef }: Props) {
    const data = useFragment<ActivityEventTeamTargetFragment_event$key>(
        graphql`
        fragment ActivityEventTeamTargetFragment_event on ActivityEvent
        {
            action
            target {
                ...on Team {
                    name
                }
            }
            payload {
                __typename
                ... on ActivityEventUpdateTeamPayload {
                    user {
                        username
                    }
                    changeType
                    maintainer
                 }
            }
            ...ActivityEventListItemFragment_event
        }
      `, fragmentRef);

    const team = data.target as any;
    const payload = data.payload as any;
    const teamLink = <ActivityEventLink to={`/teams/${team.name}`}>{team.name}</ActivityEventLink>;

    let primary;

    switch (data.action) {
        case 'CREATE':
            primary = (
                <React.Fragment>
                    Team {teamLink} created
                </React.Fragment>
            );
            break;
        case 'CREATE_MEMBERSHIP':
            primary = (
                <React.Fragment>
                    User <Typography component="span" sx={{ fontWeight: 500 }}>{payload.user?.username || 'unknown'}</Typography> added to team {teamLink}
                </React.Fragment>
            );
            break;
        case 'REMOVE_MEMBERSHIP':
            primary = (
                <React.Fragment>
                    User <Typography component="span" sx={{ fontWeight: 500 }}>{payload.user?.username || 'unknown'}</Typography> removed from team {teamLink}
                </React.Fragment>
            );
            break;
        case 'UPDATE': {
            switch (payload?.changeType) {
                case 'ADD':
                    primary = (
                        <React.Fragment>
                            Team member <Typography component="span" sx={{ fontWeight: 500 }}>{payload.user?.username || 'unknown'}</Typography> added to team {teamLink}
                        </React.Fragment>);
                    break;
                case 'REMOVE':
                    primary = (
                        <React.Fragment>
                            Team member <Typography component="span" sx={{ fontWeight: 500 }}>{payload.user?.username || 'unknown'}</Typography> removed from team {teamLink}
                        </React.Fragment>);
                    break;
                case 'MODIFY':
                    primary = (
                        <React.Fragment>
                            Team member <Typography
                                component="span"
                                sx={{ fontWeight: 500 }}>{payload.user?.username || 'unknown'}
                            </Typography> maintainer status changed to {payload.maintainer ? 'true' : 'false'} for team {teamLink}
                        </React.Fragment>
                    );
                    break;
                }
            }
        }

    return (
        <ActivityEventListItem
            fragmentRef={data}
            icon={<TeamIcon />}
            primary={primary}
        />
    );
}

export default ActivityEventTeamTarget;
