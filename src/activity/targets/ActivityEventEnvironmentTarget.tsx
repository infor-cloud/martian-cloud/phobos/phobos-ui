import React from 'react';
import graphql from 'babel-plugin-relay/macro';
import { useFragment } from 'react-relay/hooks';
import { EnvironmentIcon } from '../../common/Icons';
import ActivityEventLink from '../ActivityEventLink';
import ActivityEventListItem from '../ActivityEventListItem';
import { ActivityEventEnvironmentTargetFragment_event$key } from './__generated__/ActivityEventEnvironmentTargetFragment_event.graphql';

const ACTION_TEXT = {
    CREATE: 'created',
    UPDATE: 'updated',
} as any;

interface Props {
    showProjectLink: boolean
    fragmentRef: ActivityEventEnvironmentTargetFragment_event$key
}

function ActivityEventEnvironmentTarget({ showProjectLink, fragmentRef }: Props) {
    const data = useFragment<ActivityEventEnvironmentTargetFragment_event$key>(
        graphql`
        fragment ActivityEventEnvironmentTargetFragment_event on ActivityEvent
        {
            action
            target {
                ...on Environment {
                    id
                    name
                    metadata {
                        prn
                    }
                }
            }
            ...ActivityEventListItemFragment_event
        }
      `, fragmentRef);

    const actionText = ACTION_TEXT[data.action];
    const environment = data.target as any;
    const prnParts = environment.metadata.prn.split(':')[2].split('/');
    const projectName = prnParts[1];
    const projectRoute = `/organizations/${prnParts[0]}/projects/${projectName}`;
    const envRoute = `${projectRoute}/-/environments/${environment.id}`;

    return (
        <ActivityEventListItem
            fragmentRef={data}
            icon={<EnvironmentIcon />}
            primary={<React.Fragment>
                Environment <ActivityEventLink to={envRoute}>{environment.name}</ActivityEventLink> {actionText}
                {showProjectLink && <span> in project <ActivityEventLink to={`${projectRoute}`}>{projectName}</ActivityEventLink></span>}
            </React.Fragment>}
        />
    );
}

export default ActivityEventEnvironmentTarget;
