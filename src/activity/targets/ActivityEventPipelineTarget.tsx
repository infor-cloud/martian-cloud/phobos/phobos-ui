import React from 'react';
import graphql from 'babel-plugin-relay/macro';
import { useFragment } from 'react-relay/hooks';
import { PipelineIcon } from '../../common/Icons';
import ActivityEventLink from '../ActivityEventLink';
import ActivityEventListItem from '../ActivityEventListItem';
import { ActivityEventPipelineTargetFragment_event$key } from './__generated__/ActivityEventPipelineTargetFragment_event.graphql';
import { Paper, Typography } from '@mui/material';
import TimeStamp from '../../common/Timestamp';

const ACTION_TEXT = {
    CREATE: 'created',
    UPDATE: 'updated',
    CANCEL: 'canceled',
    DELETE: 'deleted',
    START: 'started',
    RETRY: 'retried',
    DEFER: 'deferred',
    UNDEFER: 'restored',
    APPROVE: 'approved',
    REVOKE_APPROVAL: 'revoked approval',
    SCHEDULE: 'scheduled',
    UNSCHEDULE: 'unscheduled',
} as any;

const RESOURCE_TYPE = {
    COMMENT: 'Comment',
    THREAD: 'Conversation'
} as any;

const PIPELINE_TYPE = {
    RUNBOOK: 'Runbook',
    DEPLOYMENT: 'Deployment',
    RELEASE_LIFECYCLE: 'Release lifecycle',
    NESTED: 'Nested'
} as any;

interface Props {
    showProjectLink: boolean
    showPipelineLink: boolean
    fragmentRef: ActivityEventPipelineTargetFragment_event$key
}

function ActivityEventPipelineTarget({ showProjectLink, showPipelineLink, fragmentRef }: Props) {
    const data = useFragment<ActivityEventPipelineTargetFragment_event$key>(
        graphql`
        fragment ActivityEventPipelineTargetFragment_event on ActivityEvent
        {
            action
            target {
                ...on Pipeline {
                    id
                    metadata {
                        prn
                    }
                    type
                    environmentName
                }
            }
            payload {
                __typename
                ...on ActivityEventDeleteResourcePayload {
                    type
                }
                ...on ActivityEventUpdatePipelineNodePayload {
                    nodePath
                    nodeType
                }
                ...on ActivityEventApprovePipelineNodePayload {
                    nodePath
                    nodeType
                }
                ...on ActivityEventRevokeApprovalPipelineNodePayload {
                    nodePath
                    nodeType
                }
                ...on ActivityEventRetryPipelineNodePayload {
                    nodePath
                    nodeType
                }
                ...on ActivityEventSchedulePipelineNodePayload {
                    nodePath
                    nodeType
                    startTime
                }
                ...on ActivityEventUnschedulePipelineNodePayload {
                    nodePath
                    nodeType
                }
                ...on ActivityEventStartPipelineNodePayload {
                    nodePath
                    nodeType
                }
                ...on ActivityEventDeferPipelineNodePayload {
                    nodePath
                    nodeType
                    reason
                }
                ...on ActivityEventUndeferPipelineNodePayload {
                    nodePath
                    nodeType
                }
            }
            ...ActivityEventListItemFragment_event
        }
    `, fragmentRef);

    const payload = data.payload as any;
    const actionText = ACTION_TEXT[data.action];
    const pipeline = data.target as any;
    const orgName = pipeline.metadata.prn.split(':')[2].split('/')[0];
    const projectName = pipeline.metadata.prn.split(':')[2].split('/')[1];
    const pipelineRoute = `/organizations/${orgName}/projects/${projectName}/-/pipelines/${pipeline.id}`;

    const pipelineNodeLink = (nodeType: string, nodePath: string) => {
        if (nodeType === 'TASK') {
            return `/organizations/${orgName}/projects/${projectName}/-/pipelines/${pipeline.id}/task/${nodePath}`;
        }
        return `/organizations/${orgName}/projects/${projectName}/-/pipelines/${pipeline.id}/nested_pipeline/${nodePath}`;
    }

    let primary;
    let secondary;

    switch (payload?.__typename) {
        case 'ActivityEventDeleteResourcePayload':
            primary = (
                <React.Fragment>
                    {RESOURCE_TYPE[payload.type]} {actionText} {showPipelineLink && <span> in pipeline{' '}
                        <ActivityEventLink to={pipelineRoute}>{`${pipeline.id.substring(0, 8)}...`}</ActivityEventLink></span>}
                </React.Fragment>
            );
            break;
        case 'ActivityEventSchedulePipelineNodePayload': {
            const nodeName = payload.nodePath.split(".").pop();
            const nodeType = payload.nodeType === 'TASK' ? 'Task' : (payload.nodeType === 'PIPELINE' && pipeline.type !== 'RELEASE_LIFECYCLE') ? 'Nested pipeline' : 'Deployment';
            primary = (
                <React.Fragment>
                    {nodeType}{' '}
                    <ActivityEventLink to={pipelineNodeLink(payload.nodeType, payload.nodePath)}>{nodeName}</ActivityEventLink>{' '}
                    {actionText}
                    {payload.startTime && <> to run <TimeStamp component="span" format="relative" timestamp={payload.startTime} /></>}
                    {showPipelineLink && <span> in pipeline{' '}
                        <ActivityEventLink to={pipelineRoute}>{`${pipeline.id.substring(0, 8)}...`}</ActivityEventLink></span>}
                </React.Fragment>
            );
            break;
        }
        case 'ActivityEventUpdatePipelineNodePayload':
        case 'ActivityEventApprovePipelineNodePayload':
        case 'ActivityEventRevokeApprovalPipelineNodePayload':
        case 'ActivityEventRetryPipelineNodePayload':
        case 'ActivityEventUnschedulePipelineNodePayload':
        case 'ActivityEventStartPipelineNodePayload':
        case 'ActivityEventDeferPipelineNodePayload':
        case 'ActivityEventUndeferPipelineNodePayload': {
            const nodeName = payload.nodePath.split(".").pop();
            const nodeType = payload.nodeType === 'TASK' ? 'Task' : (payload.nodeType === 'PIPELINE' && pipeline.type !== 'RELEASE_LIFECYCLE') ? 'Nested pipeline' : 'Deployment';
            primary = (
                <React.Fragment>
                    {nodeType}{' '}
                    <ActivityEventLink to={pipelineNodeLink(payload.nodeType, payload.nodePath)}>{nodeName}</ActivityEventLink>{' '}
                    {actionText} {showPipelineLink && <span> in pipeline{' '}
                        <ActivityEventLink to={pipelineRoute}>{`${pipeline.id.substring(0, 8)}...`}</ActivityEventLink></span>}
                </React.Fragment>
            );
            secondary = payload.reason ? (
                <Paper sx={{ padding: `4px 8px` }}>
                    <Typography variant="body2">
                        {payload.reason}
                    </Typography>
                </Paper>
            ) : null;
            break;
        }
        default:
            primary = (
                <React.Fragment>
                    {PIPELINE_TYPE[pipeline.type]} <ActivityEventLink to={pipelineRoute}>pipeline</ActivityEventLink> {actionText} {pipeline.environmentName ? `in environment ${pipeline.environmentName}` : ''}
                    {showProjectLink &&
                        <span> in project <ActivityEventLink to={`/organizations/${orgName}/projects/${projectName}`}>{projectName}</ActivityEventLink>
                        </span>}
                </React.Fragment>
            );
            break;
    }

    return (
        <ActivityEventListItem
            fragmentRef={data}
            icon={<PipelineIcon />}
            primary={primary}
            secondary={secondary}
        />
    );
}

export default ActivityEventPipelineTarget;
