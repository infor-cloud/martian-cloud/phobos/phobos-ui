/**
 * @generated SignedSource<<b7d111ae833aefbedb39d56efb7698a2>>
 * @lightSyntaxTransform
 * @nogrep
 */

/* tslint:disable */
/* eslint-disable */
// @ts-nocheck

import { Fragment, ReaderFragment } from 'relay-runtime';
export type ActivityEventAction = "ADD_PARTICIPANT" | "APPROVE" | "CANCEL" | "CREATE" | "CREATE_MEMBERSHIP" | "DEFER" | "DELETE" | "REMOVE_MEMBERSHIP" | "REMOVE_PARTICIPANT" | "RETRY" | "REVOKE_APPROVAL" | "SCHEDULE" | "SKIP" | "START" | "UNDEFER" | "UNSCHEDULE" | "UPDATE" | "%future added value";
export type PipelineType = "DEPLOYMENT" | "NESTED" | "RELEASE_LIFECYCLE" | "RUNBOOK" | "%future added value";
import { FragmentRefs } from "relay-runtime";
export type ActivityEventPipelineTargetFragment_event$data = {
  readonly action: ActivityEventAction;
  readonly payload: {
    readonly __typename: "ActivityEventApprovePipelineNodePayload";
    readonly nodePath: string;
    readonly nodeType: string;
  } | {
    readonly __typename: "ActivityEventDeferPipelineNodePayload";
    readonly nodePath: string;
    readonly nodeType: string;
    readonly reason: string;
  } | {
    readonly __typename: "ActivityEventDeleteResourcePayload";
    readonly type: string;
  } | {
    readonly __typename: "ActivityEventRetryPipelineNodePayload";
    readonly nodePath: string;
    readonly nodeType: string;
  } | {
    readonly __typename: "ActivityEventRevokeApprovalPipelineNodePayload";
    readonly nodePath: string;
    readonly nodeType: string;
  } | {
    readonly __typename: "ActivityEventSchedulePipelineNodePayload";
    readonly nodePath: string;
    readonly nodeType: string;
    readonly startTime: any | null | undefined;
  } | {
    readonly __typename: "ActivityEventStartPipelineNodePayload";
    readonly nodePath: string;
    readonly nodeType: string;
  } | {
    readonly __typename: "ActivityEventUndeferPipelineNodePayload";
    readonly nodePath: string;
    readonly nodeType: string;
  } | {
    readonly __typename: "ActivityEventUnschedulePipelineNodePayload";
    readonly nodePath: string;
    readonly nodeType: string;
  } | {
    readonly __typename: "ActivityEventUpdatePipelineNodePayload";
    readonly nodePath: string;
    readonly nodeType: string;
  } | {
    // This will never be '%other', but we need some
    // value in case none of the concrete values match.
    readonly __typename: "%other";
  } | null | undefined;
  readonly target: {
    readonly environmentName?: string | null | undefined;
    readonly id?: string;
    readonly metadata?: {
      readonly prn: string;
    };
    readonly type?: PipelineType;
  } | null | undefined;
  readonly " $fragmentSpreads": FragmentRefs<"ActivityEventListItemFragment_event">;
  readonly " $fragmentType": "ActivityEventPipelineTargetFragment_event";
};
export type ActivityEventPipelineTargetFragment_event$key = {
  readonly " $data"?: ActivityEventPipelineTargetFragment_event$data;
  readonly " $fragmentSpreads": FragmentRefs<"ActivityEventPipelineTargetFragment_event">;
};

const node: ReaderFragment = (function(){
var v0 = {
  "alias": null,
  "args": null,
  "kind": "ScalarField",
  "name": "type",
  "storageKey": null
},
v1 = {
  "alias": null,
  "args": null,
  "kind": "ScalarField",
  "name": "nodePath",
  "storageKey": null
},
v2 = {
  "alias": null,
  "args": null,
  "kind": "ScalarField",
  "name": "nodeType",
  "storageKey": null
},
v3 = [
  (v1/*: any*/),
  (v2/*: any*/)
];
return {
  "argumentDefinitions": [],
  "kind": "Fragment",
  "metadata": null,
  "name": "ActivityEventPipelineTargetFragment_event",
  "selections": [
    {
      "alias": null,
      "args": null,
      "kind": "ScalarField",
      "name": "action",
      "storageKey": null
    },
    {
      "alias": null,
      "args": null,
      "concreteType": null,
      "kind": "LinkedField",
      "name": "target",
      "plural": false,
      "selections": [
        {
          "kind": "InlineFragment",
          "selections": [
            {
              "alias": null,
              "args": null,
              "kind": "ScalarField",
              "name": "id",
              "storageKey": null
            },
            {
              "alias": null,
              "args": null,
              "concreteType": "ResourceMetadata",
              "kind": "LinkedField",
              "name": "metadata",
              "plural": false,
              "selections": [
                {
                  "alias": null,
                  "args": null,
                  "kind": "ScalarField",
                  "name": "prn",
                  "storageKey": null
                }
              ],
              "storageKey": null
            },
            (v0/*: any*/),
            {
              "alias": null,
              "args": null,
              "kind": "ScalarField",
              "name": "environmentName",
              "storageKey": null
            }
          ],
          "type": "Pipeline",
          "abstractKey": null
        }
      ],
      "storageKey": null
    },
    {
      "alias": null,
      "args": null,
      "concreteType": null,
      "kind": "LinkedField",
      "name": "payload",
      "plural": false,
      "selections": [
        {
          "alias": null,
          "args": null,
          "kind": "ScalarField",
          "name": "__typename",
          "storageKey": null
        },
        {
          "kind": "InlineFragment",
          "selections": [
            (v0/*: any*/)
          ],
          "type": "ActivityEventDeleteResourcePayload",
          "abstractKey": null
        },
        {
          "kind": "InlineFragment",
          "selections": (v3/*: any*/),
          "type": "ActivityEventUpdatePipelineNodePayload",
          "abstractKey": null
        },
        {
          "kind": "InlineFragment",
          "selections": (v3/*: any*/),
          "type": "ActivityEventApprovePipelineNodePayload",
          "abstractKey": null
        },
        {
          "kind": "InlineFragment",
          "selections": (v3/*: any*/),
          "type": "ActivityEventRevokeApprovalPipelineNodePayload",
          "abstractKey": null
        },
        {
          "kind": "InlineFragment",
          "selections": (v3/*: any*/),
          "type": "ActivityEventRetryPipelineNodePayload",
          "abstractKey": null
        },
        {
          "kind": "InlineFragment",
          "selections": [
            (v1/*: any*/),
            (v2/*: any*/),
            {
              "alias": null,
              "args": null,
              "kind": "ScalarField",
              "name": "startTime",
              "storageKey": null
            }
          ],
          "type": "ActivityEventSchedulePipelineNodePayload",
          "abstractKey": null
        },
        {
          "kind": "InlineFragment",
          "selections": (v3/*: any*/),
          "type": "ActivityEventUnschedulePipelineNodePayload",
          "abstractKey": null
        },
        {
          "kind": "InlineFragment",
          "selections": (v3/*: any*/),
          "type": "ActivityEventStartPipelineNodePayload",
          "abstractKey": null
        },
        {
          "kind": "InlineFragment",
          "selections": [
            (v1/*: any*/),
            (v2/*: any*/),
            {
              "alias": null,
              "args": null,
              "kind": "ScalarField",
              "name": "reason",
              "storageKey": null
            }
          ],
          "type": "ActivityEventDeferPipelineNodePayload",
          "abstractKey": null
        },
        {
          "kind": "InlineFragment",
          "selections": (v3/*: any*/),
          "type": "ActivityEventUndeferPipelineNodePayload",
          "abstractKey": null
        }
      ],
      "storageKey": null
    },
    {
      "args": null,
      "kind": "FragmentSpread",
      "name": "ActivityEventListItemFragment_event"
    }
  ],
  "type": "ActivityEvent",
  "abstractKey": null
};
})();

(node as any).hash = "86b98049532acc94042d92a408f8e4ab";

export default node;
