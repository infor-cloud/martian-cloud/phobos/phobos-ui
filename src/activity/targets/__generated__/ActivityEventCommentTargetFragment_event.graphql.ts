/**
 * @generated SignedSource<<6552404e847a1b01e24b7283c0d79e80>>
 * @lightSyntaxTransform
 * @nogrep
 */

/* tslint:disable */
/* eslint-disable */
// @ts-nocheck

import { Fragment, ReaderFragment } from 'relay-runtime';
export type ActivityEventAction = "ADD_PARTICIPANT" | "APPROVE" | "CANCEL" | "CREATE" | "CREATE_MEMBERSHIP" | "DEFER" | "DELETE" | "REMOVE_MEMBERSHIP" | "REMOVE_PARTICIPANT" | "RETRY" | "REVOKE_APPROVAL" | "SCHEDULE" | "SKIP" | "START" | "UNDEFER" | "UNSCHEDULE" | "UPDATE" | "%future added value";
import { FragmentRefs } from "relay-runtime";
export type ActivityEventCommentTargetFragment_event$data = {
  readonly action: ActivityEventAction;
  readonly project: {
    readonly name: string;
    readonly organizationName: string;
  } | null | undefined;
  readonly target: {
    readonly text?: string;
    readonly thread?: {
      readonly pipelineType: {
        readonly id: string;
      } | null | undefined;
      readonly releaseType: {
        readonly id: string;
        readonly semanticVersion: string;
      } | null | undefined;
    };
  } | null | undefined;
  readonly " $fragmentSpreads": FragmentRefs<"ActivityEventListItemFragment_event">;
  readonly " $fragmentType": "ActivityEventCommentTargetFragment_event";
};
export type ActivityEventCommentTargetFragment_event$key = {
  readonly " $data"?: ActivityEventCommentTargetFragment_event$data;
  readonly " $fragmentSpreads": FragmentRefs<"ActivityEventCommentTargetFragment_event">;
};

const node: ReaderFragment = (function(){
var v0 = {
  "alias": null,
  "args": null,
  "kind": "ScalarField",
  "name": "id",
  "storageKey": null
};
return {
  "argumentDefinitions": [],
  "kind": "Fragment",
  "metadata": null,
  "name": "ActivityEventCommentTargetFragment_event",
  "selections": [
    {
      "alias": null,
      "args": null,
      "kind": "ScalarField",
      "name": "action",
      "storageKey": null
    },
    {
      "alias": null,
      "args": null,
      "concreteType": "Project",
      "kind": "LinkedField",
      "name": "project",
      "plural": false,
      "selections": [
        {
          "alias": null,
          "args": null,
          "kind": "ScalarField",
          "name": "name",
          "storageKey": null
        },
        {
          "alias": null,
          "args": null,
          "kind": "ScalarField",
          "name": "organizationName",
          "storageKey": null
        }
      ],
      "storageKey": null
    },
    {
      "alias": null,
      "args": null,
      "concreteType": null,
      "kind": "LinkedField",
      "name": "target",
      "plural": false,
      "selections": [
        {
          "kind": "InlineFragment",
          "selections": [
            {
              "alias": null,
              "args": null,
              "kind": "ScalarField",
              "name": "text",
              "storageKey": null
            },
            {
              "alias": null,
              "args": null,
              "concreteType": "Thread",
              "kind": "LinkedField",
              "name": "thread",
              "plural": false,
              "selections": [
                {
                  "alias": "releaseType",
                  "args": null,
                  "concreteType": "Release",
                  "kind": "LinkedField",
                  "name": "release",
                  "plural": false,
                  "selections": [
                    (v0/*: any*/),
                    {
                      "alias": null,
                      "args": null,
                      "kind": "ScalarField",
                      "name": "semanticVersion",
                      "storageKey": null
                    }
                  ],
                  "storageKey": null
                },
                {
                  "alias": "pipelineType",
                  "args": null,
                  "concreteType": "Pipeline",
                  "kind": "LinkedField",
                  "name": "pipeline",
                  "plural": false,
                  "selections": [
                    (v0/*: any*/)
                  ],
                  "storageKey": null
                }
              ],
              "storageKey": null
            }
          ],
          "type": "Comment",
          "abstractKey": null
        }
      ],
      "storageKey": null
    },
    {
      "args": null,
      "kind": "FragmentSpread",
      "name": "ActivityEventListItemFragment_event"
    }
  ],
  "type": "ActivityEvent",
  "abstractKey": null
};
})();

(node as any).hash = "b2ee7e40b453f9dbed51c8a62a76dd88";

export default node;
