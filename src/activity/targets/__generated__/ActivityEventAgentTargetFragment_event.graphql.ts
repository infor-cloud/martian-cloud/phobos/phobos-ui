/**
 * @generated SignedSource<<c2d9212686f0b617074cb174ecb3280a>>
 * @lightSyntaxTransform
 * @nogrep
 */

/* tslint:disable */
/* eslint-disable */
// @ts-nocheck

import { Fragment, ReaderFragment } from 'relay-runtime';
export type ActivityEventAction = "ADD_PARTICIPANT" | "APPROVE" | "CANCEL" | "CREATE" | "CREATE_MEMBERSHIP" | "DEFER" | "DELETE" | "REMOVE_MEMBERSHIP" | "REMOVE_PARTICIPANT" | "RETRY" | "REVOKE_APPROVAL" | "SCHEDULE" | "SKIP" | "START" | "UNDEFER" | "UNSCHEDULE" | "UPDATE" | "%future added value";
export type AgentType = "ORGANIZATION" | "SHARED" | "%future added value";
import { FragmentRefs } from "relay-runtime";
export type ActivityEventAgentTargetFragment_event$data = {
  readonly action: ActivityEventAction;
  readonly target: {
    readonly agentType?: AgentType;
    readonly id?: string;
    readonly metadata?: {
      readonly prn: string;
    };
    readonly name?: string;
  } | null | undefined;
  readonly " $fragmentSpreads": FragmentRefs<"ActivityEventListItemFragment_event">;
  readonly " $fragmentType": "ActivityEventAgentTargetFragment_event";
};
export type ActivityEventAgentTargetFragment_event$key = {
  readonly " $data"?: ActivityEventAgentTargetFragment_event$data;
  readonly " $fragmentSpreads": FragmentRefs<"ActivityEventAgentTargetFragment_event">;
};

const node: ReaderFragment = {
  "argumentDefinitions": [],
  "kind": "Fragment",
  "metadata": null,
  "name": "ActivityEventAgentTargetFragment_event",
  "selections": [
    {
      "alias": null,
      "args": null,
      "kind": "ScalarField",
      "name": "action",
      "storageKey": null
    },
    {
      "alias": null,
      "args": null,
      "concreteType": null,
      "kind": "LinkedField",
      "name": "target",
      "plural": false,
      "selections": [
        {
          "kind": "InlineFragment",
          "selections": [
            {
              "alias": null,
              "args": null,
              "kind": "ScalarField",
              "name": "id",
              "storageKey": null
            },
            {
              "alias": null,
              "args": null,
              "kind": "ScalarField",
              "name": "name",
              "storageKey": null
            },
            {
              "alias": "agentType",
              "args": null,
              "kind": "ScalarField",
              "name": "type",
              "storageKey": null
            },
            {
              "alias": null,
              "args": null,
              "concreteType": "ResourceMetadata",
              "kind": "LinkedField",
              "name": "metadata",
              "plural": false,
              "selections": [
                {
                  "alias": null,
                  "args": null,
                  "kind": "ScalarField",
                  "name": "prn",
                  "storageKey": null
                }
              ],
              "storageKey": null
            }
          ],
          "type": "Agent",
          "abstractKey": null
        }
      ],
      "storageKey": null
    },
    {
      "args": null,
      "kind": "FragmentSpread",
      "name": "ActivityEventListItemFragment_event"
    }
  ],
  "type": "ActivityEvent",
  "abstractKey": null
};

(node as any).hash = "82d5f41a1bef214b1e50312b2ebbae0a";

export default node;
