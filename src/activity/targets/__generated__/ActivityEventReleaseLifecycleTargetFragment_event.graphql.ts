/**
 * @generated SignedSource<<a9c5ca8fb7a7a851da86fa1718c0040b>>
 * @lightSyntaxTransform
 * @nogrep
 */

/* tslint:disable */
/* eslint-disable */
// @ts-nocheck

import { Fragment, ReaderFragment } from 'relay-runtime';
export type ActivityEventAction = "ADD_PARTICIPANT" | "APPROVE" | "CANCEL" | "CREATE" | "CREATE_MEMBERSHIP" | "DEFER" | "DELETE" | "REMOVE_MEMBERSHIP" | "REMOVE_PARTICIPANT" | "RETRY" | "REVOKE_APPROVAL" | "SCHEDULE" | "SKIP" | "START" | "UNDEFER" | "UNSCHEDULE" | "UPDATE" | "%future added value";
export type ScopeType = "GLOBAL" | "ORGANIZATION" | "PROJECT" | "%future added value";
import { FragmentRefs } from "relay-runtime";
export type ActivityEventReleaseLifecycleTargetFragment_event$data = {
  readonly action: ActivityEventAction;
  readonly target: {
    readonly id?: string;
    readonly metadata?: {
      readonly prn: string;
    };
    readonly name?: string;
    readonly scope?: ScopeType;
  } | null | undefined;
  readonly " $fragmentSpreads": FragmentRefs<"ActivityEventListItemFragment_event">;
  readonly " $fragmentType": "ActivityEventReleaseLifecycleTargetFragment_event";
};
export type ActivityEventReleaseLifecycleTargetFragment_event$key = {
  readonly " $data"?: ActivityEventReleaseLifecycleTargetFragment_event$data;
  readonly " $fragmentSpreads": FragmentRefs<"ActivityEventReleaseLifecycleTargetFragment_event">;
};

const node: ReaderFragment = {
  "argumentDefinitions": [],
  "kind": "Fragment",
  "metadata": null,
  "name": "ActivityEventReleaseLifecycleTargetFragment_event",
  "selections": [
    {
      "alias": null,
      "args": null,
      "kind": "ScalarField",
      "name": "action",
      "storageKey": null
    },
    {
      "alias": null,
      "args": null,
      "concreteType": null,
      "kind": "LinkedField",
      "name": "target",
      "plural": false,
      "selections": [
        {
          "kind": "InlineFragment",
          "selections": [
            {
              "alias": null,
              "args": null,
              "kind": "ScalarField",
              "name": "id",
              "storageKey": null
            },
            {
              "alias": null,
              "args": null,
              "kind": "ScalarField",
              "name": "name",
              "storageKey": null
            },
            {
              "alias": null,
              "args": null,
              "kind": "ScalarField",
              "name": "scope",
              "storageKey": null
            },
            {
              "alias": null,
              "args": null,
              "concreteType": "ResourceMetadata",
              "kind": "LinkedField",
              "name": "metadata",
              "plural": false,
              "selections": [
                {
                  "alias": null,
                  "args": null,
                  "kind": "ScalarField",
                  "name": "prn",
                  "storageKey": null
                }
              ],
              "storageKey": null
            }
          ],
          "type": "ReleaseLifecycle",
          "abstractKey": null
        }
      ],
      "storageKey": null
    },
    {
      "args": null,
      "kind": "FragmentSpread",
      "name": "ActivityEventListItemFragment_event"
    }
  ],
  "type": "ActivityEvent",
  "abstractKey": null
};

(node as any).hash = "0072978138399589dc6b94fe38ca3ce8";

export default node;
