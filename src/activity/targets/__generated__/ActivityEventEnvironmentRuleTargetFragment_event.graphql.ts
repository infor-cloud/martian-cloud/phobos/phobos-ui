/**
 * @generated SignedSource<<b4d7d2f6dca18c9310f4fd91b24cd234>>
 * @lightSyntaxTransform
 * @nogrep
 */

/* tslint:disable */
/* eslint-disable */
// @ts-nocheck

import { Fragment, ReaderFragment } from 'relay-runtime';
export type ActivityEventAction = "ADD_PARTICIPANT" | "APPROVE" | "CANCEL" | "CREATE" | "CREATE_MEMBERSHIP" | "DEFER" | "DELETE" | "REMOVE_MEMBERSHIP" | "REMOVE_PARTICIPANT" | "RETRY" | "REVOKE_APPROVAL" | "SCHEDULE" | "SKIP" | "START" | "UNDEFER" | "UNSCHEDULE" | "UPDATE" | "%future added value";
export type ScopeType = "GLOBAL" | "ORGANIZATION" | "PROJECT" | "%future added value";
import { FragmentRefs } from "relay-runtime";
export type ActivityEventEnvironmentRuleTargetFragment_event$data = {
  readonly action: ActivityEventAction;
  readonly target: {
    readonly environmentNameType?: string;
    readonly id?: string;
    readonly organizationName?: string;
    readonly projectName?: string | null | undefined;
    readonly scope?: ScopeType;
  } | null | undefined;
  readonly " $fragmentSpreads": FragmentRefs<"ActivityEventListItemFragment_event">;
  readonly " $fragmentType": "ActivityEventEnvironmentRuleTargetFragment_event";
};
export type ActivityEventEnvironmentRuleTargetFragment_event$key = {
  readonly " $data"?: ActivityEventEnvironmentRuleTargetFragment_event$data;
  readonly " $fragmentSpreads": FragmentRefs<"ActivityEventEnvironmentRuleTargetFragment_event">;
};

const node: ReaderFragment = {
  "argumentDefinitions": [],
  "kind": "Fragment",
  "metadata": null,
  "name": "ActivityEventEnvironmentRuleTargetFragment_event",
  "selections": [
    {
      "alias": null,
      "args": null,
      "kind": "ScalarField",
      "name": "action",
      "storageKey": null
    },
    {
      "alias": null,
      "args": null,
      "concreteType": null,
      "kind": "LinkedField",
      "name": "target",
      "plural": false,
      "selections": [
        {
          "kind": "InlineFragment",
          "selections": [
            {
              "alias": null,
              "args": null,
              "kind": "ScalarField",
              "name": "id",
              "storageKey": null
            },
            {
              "alias": null,
              "args": null,
              "kind": "ScalarField",
              "name": "scope",
              "storageKey": null
            },
            {
              "alias": null,
              "args": null,
              "kind": "ScalarField",
              "name": "organizationName",
              "storageKey": null
            },
            {
              "alias": null,
              "args": null,
              "kind": "ScalarField",
              "name": "projectName",
              "storageKey": null
            },
            {
              "alias": "environmentNameType",
              "args": null,
              "kind": "ScalarField",
              "name": "environmentName",
              "storageKey": null
            }
          ],
          "type": "EnvironmentRule",
          "abstractKey": null
        }
      ],
      "storageKey": null
    },
    {
      "args": null,
      "kind": "FragmentSpread",
      "name": "ActivityEventListItemFragment_event"
    }
  ],
  "type": "ActivityEvent",
  "abstractKey": null
};

(node as any).hash = "0e900365b02471250e5d884feda55095";

export default node;
