/**
 * @generated SignedSource<<b9d217a758818ad60341111f41a4d0a9>>
 * @lightSyntaxTransform
 * @nogrep
 */

/* tslint:disable */
/* eslint-disable */
// @ts-nocheck

import { Fragment, ReaderFragment } from 'relay-runtime';
export type ActivityEventAction = "ADD_PARTICIPANT" | "APPROVE" | "CANCEL" | "CREATE" | "CREATE_MEMBERSHIP" | "DEFER" | "DELETE" | "REMOVE_MEMBERSHIP" | "REMOVE_PARTICIPANT" | "RETRY" | "REVOKE_APPROVAL" | "SCHEDULE" | "SKIP" | "START" | "UNDEFER" | "UNSCHEDULE" | "UPDATE" | "%future added value";
import { FragmentRefs } from "relay-runtime";
export type ActivityEventPluginVersionTargetFragment_event$data = {
  readonly action: ActivityEventAction;
  readonly target: {
    readonly plugin?: {
      readonly name: string;
      readonly organizationName: string;
    };
    readonly version?: string;
  } | null | undefined;
  readonly " $fragmentSpreads": FragmentRefs<"ActivityEventListItemFragment_event">;
  readonly " $fragmentType": "ActivityEventPluginVersionTargetFragment_event";
};
export type ActivityEventPluginVersionTargetFragment_event$key = {
  readonly " $data"?: ActivityEventPluginVersionTargetFragment_event$data;
  readonly " $fragmentSpreads": FragmentRefs<"ActivityEventPluginVersionTargetFragment_event">;
};

const node: ReaderFragment = {
  "argumentDefinitions": [],
  "kind": "Fragment",
  "metadata": null,
  "name": "ActivityEventPluginVersionTargetFragment_event",
  "selections": [
    {
      "alias": null,
      "args": null,
      "kind": "ScalarField",
      "name": "action",
      "storageKey": null
    },
    {
      "alias": null,
      "args": null,
      "concreteType": null,
      "kind": "LinkedField",
      "name": "target",
      "plural": false,
      "selections": [
        {
          "kind": "InlineFragment",
          "selections": [
            {
              "alias": null,
              "args": null,
              "kind": "ScalarField",
              "name": "version",
              "storageKey": null
            },
            {
              "alias": null,
              "args": null,
              "concreteType": "Plugin",
              "kind": "LinkedField",
              "name": "plugin",
              "plural": false,
              "selections": [
                {
                  "alias": null,
                  "args": null,
                  "kind": "ScalarField",
                  "name": "name",
                  "storageKey": null
                },
                {
                  "alias": null,
                  "args": null,
                  "kind": "ScalarField",
                  "name": "organizationName",
                  "storageKey": null
                }
              ],
              "storageKey": null
            }
          ],
          "type": "PluginVersion",
          "abstractKey": null
        }
      ],
      "storageKey": null
    },
    {
      "args": null,
      "kind": "FragmentSpread",
      "name": "ActivityEventListItemFragment_event"
    }
  ],
  "type": "ActivityEvent",
  "abstractKey": null
};

(node as any).hash = "578fc7e245ef37cae91d53704a0a9fb5";

export default node;
