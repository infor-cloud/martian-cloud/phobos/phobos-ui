/**
 * @generated SignedSource<<c6622eebb4afe37d71d71e6db3c78e5a>>
 * @lightSyntaxTransform
 * @nogrep
 */

/* tslint:disable */
/* eslint-disable */
// @ts-nocheck

import { Fragment, ReaderFragment } from 'relay-runtime';
export type ActivityEventAction = "ADD_PARTICIPANT" | "APPROVE" | "CANCEL" | "CREATE" | "CREATE_MEMBERSHIP" | "DEFER" | "DELETE" | "REMOVE_MEMBERSHIP" | "REMOVE_PARTICIPANT" | "RETRY" | "REVOKE_APPROVAL" | "SCHEDULE" | "SKIP" | "START" | "UNDEFER" | "UNSCHEDULE" | "UPDATE" | "%future added value";
import { FragmentRefs } from "relay-runtime";
export type ActivityEventPipelineTemplateTargetFragment_event$data = {
  readonly action: ActivityEventAction;
  readonly target: {
    readonly id?: string;
    readonly metadata?: {
      readonly prn: string;
    };
    readonly templateName?: string | null | undefined;
    readonly templateSemanticVersion?: string | null | undefined;
    readonly versioned?: boolean;
  } | null | undefined;
  readonly " $fragmentSpreads": FragmentRefs<"ActivityEventListItemFragment_event">;
  readonly " $fragmentType": "ActivityEventPipelineTemplateTargetFragment_event";
};
export type ActivityEventPipelineTemplateTargetFragment_event$key = {
  readonly " $data"?: ActivityEventPipelineTemplateTargetFragment_event$data;
  readonly " $fragmentSpreads": FragmentRefs<"ActivityEventPipelineTemplateTargetFragment_event">;
};

const node: ReaderFragment = {
  "argumentDefinitions": [],
  "kind": "Fragment",
  "metadata": null,
  "name": "ActivityEventPipelineTemplateTargetFragment_event",
  "selections": [
    {
      "alias": null,
      "args": null,
      "kind": "ScalarField",
      "name": "action",
      "storageKey": null
    },
    {
      "alias": null,
      "args": null,
      "concreteType": null,
      "kind": "LinkedField",
      "name": "target",
      "plural": false,
      "selections": [
        {
          "kind": "InlineFragment",
          "selections": [
            {
              "alias": null,
              "args": null,
              "kind": "ScalarField",
              "name": "id",
              "storageKey": null
            },
            {
              "alias": null,
              "args": null,
              "kind": "ScalarField",
              "name": "versioned",
              "storageKey": null
            },
            {
              "alias": "templateName",
              "args": null,
              "kind": "ScalarField",
              "name": "name",
              "storageKey": null
            },
            {
              "alias": "templateSemanticVersion",
              "args": null,
              "kind": "ScalarField",
              "name": "semanticVersion",
              "storageKey": null
            },
            {
              "alias": null,
              "args": null,
              "concreteType": "ResourceMetadata",
              "kind": "LinkedField",
              "name": "metadata",
              "plural": false,
              "selections": [
                {
                  "alias": null,
                  "args": null,
                  "kind": "ScalarField",
                  "name": "prn",
                  "storageKey": null
                }
              ],
              "storageKey": null
            }
          ],
          "type": "PipelineTemplate",
          "abstractKey": null
        }
      ],
      "storageKey": null
    },
    {
      "args": null,
      "kind": "FragmentSpread",
      "name": "ActivityEventListItemFragment_event"
    }
  ],
  "type": "ActivityEvent",
  "abstractKey": null
};

(node as any).hash = "b44414d28ae80a44d667555f547cad9b";

export default node;
