/**
 * @generated SignedSource<<2e3b85afae2e3be71ab44a58b63db98b>>
 * @lightSyntaxTransform
 * @nogrep
 */

/* tslint:disable */
/* eslint-disable */
// @ts-nocheck

import { Fragment, ReaderFragment } from 'relay-runtime';
export type ActivityEventAction = "ADD_PARTICIPANT" | "APPROVE" | "CANCEL" | "CREATE" | "CREATE_MEMBERSHIP" | "DEFER" | "DELETE" | "REMOVE_MEMBERSHIP" | "REMOVE_PARTICIPANT" | "RETRY" | "REVOKE_APPROVAL" | "SCHEDULE" | "SKIP" | "START" | "UNDEFER" | "UNSCHEDULE" | "UPDATE" | "%future added value";
import { FragmentRefs } from "relay-runtime";
export type ActivityEventReleaseTargetFragment_event$data = {
  readonly action: ActivityEventAction;
  readonly payload: {
    readonly __typename: "ActivityEventDeleteResourcePayload";
    readonly type: string;
  } | {
    readonly __typename: "ActivityEventUpdateReleasePayload";
    readonly changeType: string;
    readonly resource: {
      readonly name?: string;
      readonly username?: string;
    } | null | undefined;
    readonly type: string;
  } | {
    // This will never be '%other', but we need some
    // value in case none of the concrete values match.
    readonly __typename: "%other";
  } | null | undefined;
  readonly project: {
    readonly name: string;
    readonly organizationName: string;
  } | null | undefined;
  readonly target: {
    readonly id?: string;
    readonly metadata?: {
      readonly prn: string;
    };
    readonly semanticVersion?: string;
  } | null | undefined;
  readonly " $fragmentSpreads": FragmentRefs<"ActivityEventListItemFragment_event">;
  readonly " $fragmentType": "ActivityEventReleaseTargetFragment_event";
};
export type ActivityEventReleaseTargetFragment_event$key = {
  readonly " $data"?: ActivityEventReleaseTargetFragment_event$data;
  readonly " $fragmentSpreads": FragmentRefs<"ActivityEventReleaseTargetFragment_event">;
};

const node: ReaderFragment = (function(){
var v0 = {
  "alias": null,
  "args": null,
  "kind": "ScalarField",
  "name": "name",
  "storageKey": null
},
v1 = {
  "alias": null,
  "args": null,
  "kind": "ScalarField",
  "name": "type",
  "storageKey": null
};
return {
  "argumentDefinitions": [],
  "kind": "Fragment",
  "metadata": null,
  "name": "ActivityEventReleaseTargetFragment_event",
  "selections": [
    {
      "alias": null,
      "args": null,
      "concreteType": "Project",
      "kind": "LinkedField",
      "name": "project",
      "plural": false,
      "selections": [
        (v0/*: any*/),
        {
          "alias": null,
          "args": null,
          "kind": "ScalarField",
          "name": "organizationName",
          "storageKey": null
        }
      ],
      "storageKey": null
    },
    {
      "alias": null,
      "args": null,
      "concreteType": null,
      "kind": "LinkedField",
      "name": "target",
      "plural": false,
      "selections": [
        {
          "kind": "InlineFragment",
          "selections": [
            {
              "alias": null,
              "args": null,
              "kind": "ScalarField",
              "name": "id",
              "storageKey": null
            },
            {
              "alias": null,
              "args": null,
              "kind": "ScalarField",
              "name": "semanticVersion",
              "storageKey": null
            },
            {
              "alias": null,
              "args": null,
              "concreteType": "ResourceMetadata",
              "kind": "LinkedField",
              "name": "metadata",
              "plural": false,
              "selections": [
                {
                  "alias": null,
                  "args": null,
                  "kind": "ScalarField",
                  "name": "prn",
                  "storageKey": null
                }
              ],
              "storageKey": null
            }
          ],
          "type": "Release",
          "abstractKey": null
        }
      ],
      "storageKey": null
    },
    {
      "alias": null,
      "args": null,
      "kind": "ScalarField",
      "name": "action",
      "storageKey": null
    },
    {
      "alias": null,
      "args": null,
      "concreteType": null,
      "kind": "LinkedField",
      "name": "payload",
      "plural": false,
      "selections": [
        {
          "alias": null,
          "args": null,
          "kind": "ScalarField",
          "name": "__typename",
          "storageKey": null
        },
        {
          "kind": "InlineFragment",
          "selections": [
            {
              "alias": null,
              "args": null,
              "kind": "ScalarField",
              "name": "changeType",
              "storageKey": null
            },
            (v1/*: any*/),
            {
              "alias": null,
              "args": null,
              "concreteType": null,
              "kind": "LinkedField",
              "name": "resource",
              "plural": false,
              "selections": [
                {
                  "kind": "InlineFragment",
                  "selections": [
                    (v0/*: any*/)
                  ],
                  "type": "Team",
                  "abstractKey": null
                },
                {
                  "kind": "InlineFragment",
                  "selections": [
                    {
                      "alias": null,
                      "args": null,
                      "kind": "ScalarField",
                      "name": "username",
                      "storageKey": null
                    }
                  ],
                  "type": "User",
                  "abstractKey": null
                }
              ],
              "storageKey": null
            }
          ],
          "type": "ActivityEventUpdateReleasePayload",
          "abstractKey": null
        },
        {
          "kind": "InlineFragment",
          "selections": [
            (v1/*: any*/)
          ],
          "type": "ActivityEventDeleteResourcePayload",
          "abstractKey": null
        }
      ],
      "storageKey": null
    },
    {
      "args": null,
      "kind": "FragmentSpread",
      "name": "ActivityEventListItemFragment_event"
    }
  ],
  "type": "ActivityEvent",
  "abstractKey": null
};
})();

(node as any).hash = "65209ca0dee5711a8255020a33442772";

export default node;
