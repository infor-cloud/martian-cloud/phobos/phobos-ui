import React from 'react';
import graphql from 'babel-plugin-relay/macro';
import { useFragment } from 'react-relay/hooks';
import { ReleaseLifecycleIcon } from '../../common/Icons';
import ActivityEventLink from '../ActivityEventLink';
import ActivityEventListItem from '../ActivityEventListItem';
import { ActivityEventReleaseLifecycleTargetFragment_event$key } from './__generated__/ActivityEventReleaseLifecycleTargetFragment_event.graphql';

const ACTION_TEXT = {
    CREATE: 'created',
    UPDATE: 'updated',
} as any;

interface Props {
    showOrgLink: boolean
    showProjectLink: boolean
    fragmentRef: ActivityEventReleaseLifecycleTargetFragment_event$key
}

function ActivityEventReleaseLifecycleTarget({ showOrgLink, showProjectLink, fragmentRef }: Props) {
    const data = useFragment<ActivityEventReleaseLifecycleTargetFragment_event$key>(
        graphql`
        fragment ActivityEventReleaseLifecycleTargetFragment_event on ActivityEvent
        {
            action
            target {
                ...on ReleaseLifecycle {
                    id
                    name
                    scope
                    metadata {
                        prn
                    }
                }
            }
            ...ActivityEventListItemFragment_event
        }
      `, fragmentRef);

    const actionText = ACTION_TEXT[data.action];
    const releaseLifecycle = data.target;
    const scope = releaseLifecycle?.scope;
    const orgName = releaseLifecycle?.metadata?.prn.split(':')[2].split('/')[0];
    const projectName = scope === 'PROJECT' ? releaseLifecycle?.metadata?.prn.split(':')[2].split('/')[1] : null;
    const baseLink = `/organizations/${orgName}`;
    const releaseLifecycleLink = `${baseLink}${projectName ? `/projects/${projectName}` : ''}/-/release_lifecycles/${releaseLifecycle?.id}`;

    const contextLink = scope === 'ORGANIZATION' ?
    showOrgLink && <span> in organization <ActivityEventLink to={baseLink}>{orgName}</ActivityEventLink></span>
    :
    showProjectLink && <span> in project <ActivityEventLink to={`${baseLink}/projects/${projectName}`}>{projectName}</ActivityEventLink></span>;

    return (
        <ActivityEventListItem
            fragmentRef={data}
            icon={<ReleaseLifecycleIcon />}
            primary={<React.Fragment>
                Release lifecycle <ActivityEventLink to={releaseLifecycleLink}>{releaseLifecycle?.name}</ActivityEventLink> {actionText}
                {contextLink}
            </React.Fragment>}
        />
    );
}

export default ActivityEventReleaseLifecycleTarget;
