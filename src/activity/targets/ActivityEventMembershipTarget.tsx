import React from 'react';
import { Paper, Typography } from '@mui/material';
import graphql from 'babel-plugin-relay/macro';
import { useFragment } from 'react-relay/hooks';
import { MemberIcon } from '../../common/Icons';
import ActivityEventLink from '../ActivityEventLink';
import ActivityEventListItem from '../ActivityEventListItem';
import { ActivityEventMembershipTargetFragment_event$key } from './__generated__/ActivityEventMembershipTargetFragment_event.graphql';

const ACTION_TEXT = {
    UPDATE: 'updated',
} as any;

function getMemberIdentifier(member: any) {
    if (!member) {
        return 'n/a';
    }

    switch(member.__typename) {
        case 'User':
            return member.username;
        case 'ServiceAccount':
            return <ActivityEventLink to={`/organizations/${member.metadata.prn.split(':')[2].split('/')[0]}/-/service_accounts/${member.id}`}>{member.name}</ActivityEventLink>;
        case 'Team':
            return <ActivityEventLink to={`/teams/${member.name}`}>{member.name}</ActivityEventLink>;
    }
}

const MEMBER_TYPES = {
    User: 'user',
    ServiceAccount: 'service account',
    Team: 'team'
} as any;

interface Props {
    showOrgLink: boolean
    fragmentRef: ActivityEventMembershipTargetFragment_event$key
}

function ActivityEventMembershipTarget({ showOrgLink, fragmentRef}: Props) {
    const data = useFragment<ActivityEventMembershipTargetFragment_event$key>(
        graphql`
        fragment ActivityEventMembershipTargetFragment_event on ActivityEvent
        {
            action
            target {
                ...on Membership {
                    scope
                    organization {
                        name
                        metadata {
                            prn
                        }
                    }
                    project {
                        name
                        metadata {
                            prn
                        }
                    }
                    member {
                        __typename
                        ...on User {
                            username
                        }
                        ...on Team {
                            name
                        }
                        ...on ServiceAccount {
                            id
                            name
                            metadata {
                                prn
                            }
                        }
                    }
                }
            }
            targetType
            payload {
                __typename
                ...on ActivityEventUpdateMembershipPayload {
                    prevRole
                    newRole
                }
            }
            ...ActivityEventListItemFragment_event
        }
      `, fragmentRef);

    const actionText = ACTION_TEXT[data.action];
    const payload = data.payload as any;
    const target = data.target as any;
    let resourceName;

    const memberIdentifier = getMemberIdentifier(target.member);

    const createRoute = (scope: string) => {
        if (scope === 'ORGANIZATION') {
            resourceName = target.organization.name;
            const orgName = target.organization.metadata.prn.split(':')[2].split('/')[0];
            return `/organizations/${orgName}`;
        } else if (scope === 'PROJECT') {
            resourceName = target.project.name;
            const orgName = target.project.metadata.prn.split(':')[2].split('/')[0];
            const projectName = target.project.metadata.prn.split(':')[2].split('/')[1];
            return `/organizations/${orgName}/projects/${projectName}`;
        } else {
            return '';
        }
    };

    return (
        <ActivityEventListItem
            fragmentRef={data}
            icon={<MemberIcon />}
            primary={<React.Fragment>
                Membership {actionText} for {MEMBER_TYPES[target.member.__typename]} <Typography component="span" sx={{ fontWeight: 500 }}>{memberIdentifier} </Typography>
                {!showOrgLink && <React.Fragment>
                    in{' '}{target.scope.toLowerCase()}{' '}
                    <ActivityEventLink to={createRoute(target.scope)}>{resourceName}</ActivityEventLink>
                </React.Fragment>}
            </React.Fragment>}
            secondary={data.payload?.__typename === 'ActivityEventUpdateMembershipPayload' ? <Paper sx={{ padding: `4px 8px` }}>
                <Typography variant="body2">
                    {payload.prevRole} role changed to {payload.newRole}
                </Typography>
            </Paper> : null}
        />
    );
}

export default ActivityEventMembershipTarget;
