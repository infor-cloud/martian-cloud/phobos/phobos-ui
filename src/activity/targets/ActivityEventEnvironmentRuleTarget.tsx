import React from 'react';
import graphql from 'babel-plugin-relay/macro';
import { useFragment } from 'react-relay/hooks';
import { EnvironmentIcon } from '../../common/Icons';
import ActivityEventLink from '../ActivityEventLink';
import ActivityEventListItem from '../ActivityEventListItem';
import { ActivityEventEnvironmentRuleTargetFragment_event$key } from './__generated__/ActivityEventEnvironmentRuleTargetFragment_event.graphql';

const ACTION_TEXT = {
    CREATE: 'created',
    UPDATE: 'updated'
} as any;

interface Props {
    showOrgLink: boolean
    showProjectLink: boolean
    fragmentRef: ActivityEventEnvironmentRuleTargetFragment_event$key
}

function ActivityEventEnvironmentRuleTarget({ showOrgLink, showProjectLink, fragmentRef }: Props) {
    const data = useFragment<ActivityEventEnvironmentRuleTargetFragment_event$key>(
        graphql`
        fragment ActivityEventEnvironmentRuleTargetFragment_event on ActivityEvent
        {
            action
            target {
                ...on EnvironmentRule {
                    id
                    scope
                    organizationName
                    projectName
                    environmentNameType: environmentName
                }
            }
            ...ActivityEventListItemFragment_event
        }
      `, fragmentRef);

    const actionText = ACTION_TEXT[data.action];
    const environmentRule = data.target;
    const { scope, organizationName, projectName } = environmentRule || {};
    const baseLink = `/organizations/${organizationName}`;
    const envRuleLink = `${baseLink}${projectName ? `/projects/${projectName}` : ''}/-/settings`;

    const contextLink = scope === 'ORGANIZATION' ?
        showOrgLink && <span> in organization <ActivityEventLink to={baseLink}>{organizationName}</ActivityEventLink></span>
        :
        showProjectLink && <span> in project <ActivityEventLink to={`${baseLink}/projects/${projectName}`}>{projectName}</ActivityEventLink></span>;

    return (
        <ActivityEventListItem
            fragmentRef={data}
            icon={<EnvironmentIcon />}
            primary={<React.Fragment>
                Environment protection rule for environment <ActivityEventLink to={envRuleLink}>{environmentRule?.environmentNameType}</ActivityEventLink>{' '}
                {actionText}{contextLink}
            </React.Fragment>}
        />
    );
}

export default ActivityEventEnvironmentRuleTarget;
