import graphql from 'babel-plugin-relay/macro';
import React from 'react';
import { useFragment } from 'react-relay/hooks';
import remarkGfm from 'remark-gfm';
import { CommentIcon } from '../../common/Icons';
import MuiMarkdown from '../../common/Markdown';
import ActivityEventLink from '../ActivityEventLink';
import ActivityEventListItem from '../ActivityEventListItem';
import { ActivityEventCommentTargetFragment_event$key } from './__generated__/ActivityEventCommentTargetFragment_event.graphql';

const ACTION_TEXT = {
    CREATE: 'created',
    UPDATE: 'updated',
} as any;

interface Props {
    fragmentRef: ActivityEventCommentTargetFragment_event$key
}

function ActivityEventCommentTarget({ fragmentRef }: Props) {
    const data = useFragment<ActivityEventCommentTargetFragment_event$key>(
        graphql`
        fragment ActivityEventCommentTargetFragment_event on ActivityEvent
        {
            action
            project {
                name
                organizationName
            }
            target {
                ...on Comment {
                    text
                    thread {
                        releaseType: release {
                            id
                            semanticVersion
                        }
                        pipelineType: pipeline {
                            id
                        }
                    }
                }
            }
            ...ActivityEventListItemFragment_event
        }
      `, fragmentRef);

    const actionText = ACTION_TEXT[data.action];
    const target = data.target;
    const resource = target?.thread?.releaseType || target?.thread?.pipelineType;
    const resourceType = target?.thread?.releaseType ? 'release' : 'pipeline';

    const route = `/organizations/${data.project?.organizationName}/projects/${data.project?.name}/-/${resourceType}s/${resource?.id}`;

    const text = target?.text ?? '';

    const primary =
        <React.Fragment>
            Comment {actionText} in {resourceType} <ActivityEventLink
                to={`${route}/?tab=comments`}>{resource === target?.thread?.releaseType ? target?.thread?.releaseType?.semanticVersion : `${target?.thread?.pipelineType?.id.substring(0, 8)}...`}
            </ActivityEventLink>
        </React.Fragment>;

    const secondary =
        <React.Fragment>
            <MuiMarkdown remarkPlugins={[remarkGfm]}>
                {`${text.substring(0, 50)}${text.length > 50 ? '...' : ''}`}
            </MuiMarkdown>
        </React.Fragment>;

    return (
        <ActivityEventListItem
            fragmentRef={data}
            icon={<CommentIcon />}
            primary={primary}
            secondary={secondary}
        />
    );
}

export default ActivityEventCommentTarget;
