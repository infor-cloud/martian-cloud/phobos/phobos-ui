import graphql from 'babel-plugin-relay/macro';
import React from 'react';
import { useFragment } from 'react-relay/hooks';
import remarkGfm from 'remark-gfm';
import { CommentIcon } from '../../common/Icons';
import MuiMarkdown from '../../common/Markdown';
import ActivityEventLink from '../ActivityEventLink';
import ActivityEventListItem from '../ActivityEventListItem';
import { ActivityEventThreadTargetFragment_event$key } from './__generated__/ActivityEventThreadTargetFragment_event.graphql';

const ACTION_TEXT = {
    CREATE: 'created',
    UPDATE: 'updated',
} as any;

interface Props {
    fragmentRef: ActivityEventThreadTargetFragment_event$key
}

function ActivityEventThreadTarget({ fragmentRef }: Props) {
    const data = useFragment<ActivityEventThreadTargetFragment_event$key>(
        graphql`
        fragment ActivityEventThreadTargetFragment_event on ActivityEvent
        {
            action
            project {
                name
                organizationName
            }
            target {
                ...on Thread {
                    rootComment {
                        id
                        text
                    }
                    releaseType: release {
                        id
                        semanticVersion
                    }
                    pipelineType: pipeline {
                        id
                    }
                }
            }
            ...ActivityEventListItemFragment_event
        }
      `, fragmentRef);

    const actionText = ACTION_TEXT[data.action];
    const target = data.target;
    const resource = target?.releaseType || target?.pipelineType;
    const resourceType = target?.releaseType ? 'release' : 'pipeline';

    const route = `/organizations/${data.project?.organizationName}/projects/${data.project?.name}/-/${resourceType}s/${resource?.id}`;

    const text = target?.rootComment?.text ?? '';

    const primary =
        <React.Fragment>
            Conversation {actionText} in {resourceType} <ActivityEventLink
                to={`${route}/?tab=comments`}>{resourceType === 'release' ? target?.releaseType?.semanticVersion : `${target?.pipelineType?.id.substring(0, 8)}...`}
            </ActivityEventLink>
        </React.Fragment>;

    const secondary =
        <React.Fragment>
            <MuiMarkdown remarkPlugins={[remarkGfm]}>
                {`${text.substring(0, 50)}${text.length > 50 ? '...' : ''}`}
            </MuiMarkdown>
        </React.Fragment>;

    return (
        <ActivityEventListItem
            fragmentRef={data}
            icon={<CommentIcon />}
            primary={primary}
            secondary={secondary}
        />
    );
}

export default ActivityEventThreadTarget;
