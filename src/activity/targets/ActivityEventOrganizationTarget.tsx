import React from 'react';
import { Typography } from '@mui/material';
import graphql from 'babel-plugin-relay/macro';
import { useFragment } from 'react-relay/hooks';
import { OrganizationIcon } from '../../common/Icons';
import ActivityEventLink from '../ActivityEventLink';
import ActivityEventListItem from '../ActivityEventListItem';
import { ActivityEventOrganizationTargetFragment_event$key } from './__generated__/ActivityEventOrganizationTargetFragment_event.graphql';

const ACTION_TEXT = {
    CREATE: 'created',
    DELETE: 'deleted',
    UPDATE: 'updated',
    CREATE_MEMBERSHIP: 'added to',
    REMOVE_MEMBERSHIP: 'removed from',
} as any;

const RESOURCE_TYPES = {
    ORGANIZATION: 'Organization',
    PROJECT: 'Project',
    RELEASE_LIFECYCLE: 'Release lifecycle',
    SERVICE_ACCOUNT: 'Service account',
    PIPELINE_TEMPLATE: 'Pipeline template',
    PIPELINE: 'Pipeline',
    APPROVAL_RULE: 'Approval rule',
    AGENT: 'Agent',
    PLUGIN: 'Plugin',
    VCS_PROVIDER: 'VCS provider',
    ENVIRONMENT_RULE: 'Environment protection rule'
} as any;

const MEMBER_TYPES = {
    User: 'User',
    ServiceAccount: 'Service account',
    Team: 'Team'
} as any;

function getMemberIdentifier(member: any) {
    if (!member) {
        return 'n/a';
    }

    switch (member.__typename) {
        case 'User':
            return member.username;
        case 'ServiceAccount':
            return <ActivityEventLink to={`/organizations/${member.metadata.prn.split(':')[2].split('/')[0]}/-/service_accounts/${member.id}`}>{member.name}</ActivityEventLink>;
        case 'Team':
            return <ActivityEventLink to={`/teams/${member.name}`}>{member.name}</ActivityEventLink>;
    }
}

interface Props {
    showOrgLink: boolean
    fragmentRef: ActivityEventOrganizationTargetFragment_event$key
}

function ActivityEventOrganizationTarget({ showOrgLink, fragmentRef }: Props) {
    const data = useFragment<ActivityEventOrganizationTargetFragment_event$key>(
        graphql`
        fragment ActivityEventOrganizationTargetFragment_event on ActivityEvent
        {
            action
            target {
                ...on Organization {
                    name
                    description
                    metadata {
                        prn
                    }
                }
            }
            payload {
                __typename
                ...on ActivityEventDeleteResourcePayload {
                    name
                    type
                }
                ...on ActivityEventCreateMembershipPayload {
                    member {
                      __typename
                      ... on User {
                        username
                      }
                      ... on ServiceAccount {
                        id
                        name
                        metadata {
                            prn
                        }
                      }
                      ... on Team {
                        name
                      }
                    }
                    role
                  }
                ...on ActivityEventRemoveMembershipPayload {
                    member {
                      __typename
                      ... on User {
                        username
                      }
                      ... on ServiceAccount {
                        id
                        name
                        metadata {
                            prn
                        }
                      }
                      ... on Team {
                        name
                      }
                    }
                }
            }
            ...ActivityEventListItemFragment_event
        }
    `, fragmentRef);

    const actionText = ACTION_TEXT[data.action];
    const org = data.target as any;
    const payload = data.payload as any;

    const route = <ActivityEventLink to={`/organizations/${org.metadata.prn.split(':')[2]}`}>{org.name}</ActivityEventLink>;

    let primary;

    if (['CREATE', 'UPDATE'].includes(data.action)) {
        primary = <React.Fragment>Organization {route} {actionText}</React.Fragment>;
    } else if ('CREATE_MEMBERSHIP' === data.action) {
        primary = <React.Fragment>{MEMBER_TYPES[payload?.member?.__typename] || 'Unknown member type'}
            <Typography component="span" sx={{ fontWeight: 500 }}>{' '}{getMemberIdentifier(payload?.member)}</Typography> added
            {showOrgLink && <span> to organization {route}</span>}
            {' '}with role {payload?.role}</React.Fragment>;
    } else if ('REMOVE_MEMBERSHIP' === data.action) {
        primary = <React.Fragment>{MEMBER_TYPES[payload?.member?.__typename] || 'Unknown member type'}
            <Typography component="span" sx={{ fontWeight: 500 }}>{' '}{getMemberIdentifier(payload?.member)}</Typography> removed
            {showOrgLink && <span> from organization {route}</span>}
        </React.Fragment>;
    } else if ('DELETE' === data.action) {
        primary = <React.Fragment>{RESOURCE_TYPES[payload?.type] || 'Unknown resource type'}
            <Typography component="span" sx={{ fontWeight: 500 }}>{' '}{payload?.name}</Typography> deleted
            {showOrgLink && <span> from organization {route}</span>}
        </React.Fragment>;
    }
    return (
        <ActivityEventListItem
            fragmentRef={data}
            icon={<OrganizationIcon />}
            primary={primary}
        />
    );
}

export default ActivityEventOrganizationTarget;
