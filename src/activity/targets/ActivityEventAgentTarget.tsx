import React from 'react';
import graphql from 'babel-plugin-relay/macro';
import { useFragment } from 'react-relay/hooks';
import { AgentIcon } from '../../common/Icons';
import ActivityEventLink from '../ActivityEventLink';
import ActivityEventListItem from '../ActivityEventListItem';
import { ActivityEventAgentTargetFragment_event$key } from './__generated__/ActivityEventAgentTargetFragment_event.graphql';

const ACTION_TEXT = {
    CREATE: 'created',
    UPDATE: 'updated',
} as any;

interface Props {
    showOrgLink: boolean
    fragmentRef: ActivityEventAgentTargetFragment_event$key
}

function ActivityEventAgentTarget({ showOrgLink, fragmentRef }: Props) {
    const data = useFragment<ActivityEventAgentTargetFragment_event$key>(
        graphql`
        fragment ActivityEventAgentTargetFragment_event on ActivityEvent
        {
            action
            target {
                ...on Agent {
                    id
                    name
                    agentType: type
                    metadata {
                        prn
                    }
                }
            }
            ...ActivityEventListItemFragment_event
        }
      `, fragmentRef);

    const actionText = ACTION_TEXT[data.action];
    const agent = data.target as any;

    const getActivityLink = (agent: any) => {
        if (agent.agentType === 'SHARED') {
            return (
                <React.Fragment>
                    Agent <ActivityEventLink to={`/admin/agents/${agent.id}`}>{agent.name}</ActivityEventLink> {actionText} at the administrator level
                </React.Fragment>
            );
        } else {
            const orgName = agent.metadata.prn.split(':')[2].split('/')[0];
            return (
                <React.Fragment>
                    Agent <ActivityEventLink to={`/organizations/${orgName}/-/agents/${agent.id}`}>{agent.name}</ActivityEventLink> {actionText}
                    {showOrgLink && <span> in organization <ActivityEventLink to={`/organizations/${orgName}`}>{orgName}</ActivityEventLink></span>}
                </React.Fragment>
            );
        }
    }

    return (
        <ActivityEventListItem
            fragmentRef={data}
            icon={<AgentIcon />}
            primary={getActivityLink(agent)}
        />
    );
}

export default ActivityEventAgentTarget;
