import graphql from 'babel-plugin-relay/macro';
import React from 'react';
import { useFragment } from 'react-relay/hooks';
import { VariableIcon } from '../../common/Icons';
import ActivityEventLink from '../ActivityEventLink';
import ActivityEventListItem from '../ActivityEventListItem';
import { ActivityEventProjectVariableSetTargetFragment_event$key } from './__generated__/ActivityEventProjectVariableSetTargetFragment_event.graphql';

interface Props {
    showOrgLink: boolean
    showProjectLink: boolean
    fragmentRef: ActivityEventProjectVariableSetTargetFragment_event$key
}

function ActivityEventProjectVariableSetTarget({ showOrgLink, showProjectLink, fragmentRef }: Props) {
    const data = useFragment<ActivityEventProjectVariableSetTargetFragment_event$key>(
        graphql`
        fragment ActivityEventProjectVariableSetTargetFragment_event on ActivityEvent
        {
            action
            target {
                ...on ProjectVariableSet {
                    metadata {
                        prn
                    }
                    revision
                }
            }
            ...ActivityEventListItemFragment_event
        }
      `, fragmentRef);

    const orgName = data?.target?.metadata?.prn?.split(':')[2].split('/')[0];
    const projectName = data?.target?.metadata?.prn?.split(':')[2].split('/')[1];
    const revision = data.target?.revision;

    const contextLink = (showOrgLink || showProjectLink) ? <span> in project <ActivityEventLink to={`/organizations/${orgName}/projects/${projectName}`}>{projectName}</ActivityEventLink></span> : null;

    return (
        <ActivityEventListItem
            fragmentRef={data}
            icon={<VariableIcon />}
            primary={<React.Fragment>
                Project variable set revision <ActivityEventLink to={`/organizations/${orgName}/projects/${projectName}/-/variables?revision=${revision}`}>{revision}</ActivityEventLink> created {contextLink}
            </React.Fragment>}
        />
    );
}

export default ActivityEventProjectVariableSetTarget;
