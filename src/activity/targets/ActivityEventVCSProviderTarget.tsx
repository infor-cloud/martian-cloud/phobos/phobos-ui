import React from 'react';
import graphql from 'babel-plugin-relay/macro';
import { useFragment } from 'react-relay/hooks';
import { VCSProviderIcon } from '../../common/Icons';
import ActivityEventLink from '../ActivityEventLink';
import ActivityEventListItem from '../ActivityEventListItem';
import { ActivityEventVCSProviderTargetFragment_event$key } from './__generated__/ActivityEventVCSProviderTargetFragment_event.graphql';

const ACTION_TEXT = {
    CREATE: 'created',
    UPDATE: 'updated',
} as any;

interface Props {
    showOrgLink: boolean
    showProjectLink: boolean
    fragmentRef: ActivityEventVCSProviderTargetFragment_event$key
}

function ActivityEventVCSProviderTarget({ fragmentRef, showOrgLink, showProjectLink }: Props) {
    const data = useFragment<ActivityEventVCSProviderTargetFragment_event$key>(
        graphql`
            fragment ActivityEventVCSProviderTargetFragment_event on ActivityEvent
            {
                action
                target {
                    ...on VCSProvider {
                        id
                        name
                        scope
                        metadata {
                            prn
                        }
                    }
                }
                ...ActivityEventListItemFragment_event
            }
        `, fragmentRef
    );

    const actionText = ACTION_TEXT[data.action];
    const provider = data.target;
    const scope = provider?.scope;
    const orgName = provider?.metadata?.prn.split(':')[2].split('/')[0];
    const projectName = scope === 'PROJECT' ? provider?.metadata?.prn.split(':')[2].split('/')[1] : null;
    const baseLink = `/organizations/${orgName}`;
    const vcsLink = `${baseLink}${projectName ? `/projects/${projectName}` : ''}/-/vcs_providers/${provider?.id}`;

    const contextLink = scope === 'ORGANIZATION' ?
        showOrgLink && <span> in organization <ActivityEventLink to={baseLink}>{orgName}</ActivityEventLink></span>
        :
        showProjectLink && <span> in project <ActivityEventLink to={`${baseLink}/projects/${projectName}`}>{projectName}</ActivityEventLink></span>;

    return (
        <ActivityEventListItem
            fragmentRef={data}
            icon={<VCSProviderIcon />}
            primary={<React.Fragment>VCS provider <ActivityEventLink to={vcsLink}>{provider?.name}</ActivityEventLink>{' '}
                {actionText}{contextLink}</React.Fragment>}
        />
    );
}

export default ActivityEventVCSProviderTarget;
