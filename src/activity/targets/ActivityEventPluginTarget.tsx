import graphql from 'babel-plugin-relay/macro';
import React from 'react';
import { useFragment } from 'react-relay/hooks';
import ExtensionIcon from '@mui/icons-material/Extension';
import ActivityEventLink from '../ActivityEventLink';
import ActivityEventListItem from '../ActivityEventListItem';
import { ActivityEventPluginTargetFragment_event$key } from './__generated__/ActivityEventPluginTargetFragment_event.graphql';

const ACTION_TEXT = {
    CREATE: 'created',
    UPDATE: 'updated',
} as any;

interface Props {
    fragmentRef: ActivityEventPluginTargetFragment_event$key
}

function ActivityEventPluginTarget({ fragmentRef }: Props) {
    const data = useFragment<ActivityEventPluginTargetFragment_event$key>(
        graphql`
        fragment ActivityEventPluginTargetFragment_event on ActivityEvent
        {
            action
            target {
                ...on Plugin {
                    name
                    organizationName
                    latestVersion {
                        version
                    }
                }
            }
            ...ActivityEventListItemFragment_event
        }
      `, fragmentRef);

    const actionText = ACTION_TEXT[data.action];
    const plugin = data.target as any;

    return (
        <ActivityEventListItem
            fragmentRef={data}
            icon={<ExtensionIcon />}
            primary={<React.Fragment>
                Plugin {plugin.latestVersion ? <ActivityEventLink to={`/plugin-registry/${plugin.organizationName}/${plugin.name}/${plugin.latestVersion.version}`}>{plugin.name}</ActivityEventLink> : plugin.name} {actionText} in organization <ActivityEventLink to={`/organizations/${plugin.organizationName}`}>{plugin.organizationName}</ActivityEventLink>
            </React.Fragment>}
        />
    );
}

export default ActivityEventPluginTarget;
