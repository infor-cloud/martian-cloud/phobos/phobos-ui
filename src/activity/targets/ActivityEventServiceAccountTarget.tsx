import React from 'react';
import graphql from 'babel-plugin-relay/macro';
import { useFragment } from 'react-relay/hooks';
import { LanConnect as ServiceAccountIcon } from 'mdi-material-ui';
import ActivityEventLink from '../ActivityEventLink';
import ActivityEventListItem from '../ActivityEventListItem';
import { ScopeType, ActivityEventServiceAccountTargetFragment_event$key } from './__generated__/ActivityEventServiceAccountTargetFragment_event.graphql';

const ACTION_TEXT = {
    CREATE: 'created',
    UPDATE: 'updated',
} as any;

interface Props {
    showOrgLink: boolean
    showProjectLink: boolean
    fragmentRef: ActivityEventServiceAccountTargetFragment_event$key
}

function ActivityEventServiceAccountTarget({ showOrgLink, showProjectLink, fragmentRef }: Props) {
    const data = useFragment<ActivityEventServiceAccountTargetFragment_event$key>(
        graphql`
        fragment ActivityEventServiceAccountTargetFragment_event on ActivityEvent
        {
            action
            target {
                ...on ServiceAccount {
                    id
                    name
                    scope
                    metadata {
                        prn
                    }
                }
            }
            ...ActivityEventListItemFragment_event
        }
      `, fragmentRef);

    const actionText = ACTION_TEXT[data.action];
    const serviceAccount = data.target;
    const scope = serviceAccount?.scope;
    const orgName = serviceAccount?.metadata?.prn.split(':')[2].split('/')[0];
    const projectName = scope === 'PROJECT' ? serviceAccount?.metadata?.prn.split(':')[2].split('/')[1] : null;
    const baseLink = `/organizations/${orgName}`;
    const serviceAccountLink = `${baseLink}${projectName ? `/projects/${projectName}` : ''}/-/service_accounts/${serviceAccount?.id}`;

    const getContextLink = (scope: ScopeType | undefined) => {
        switch (scope) {
            case 'ORGANIZATION':
                return showOrgLink && <span> in organization <ActivityEventLink to={baseLink}>{orgName}</ActivityEventLink></span>;
            case 'PROJECT':
                return showProjectLink && <span> in project <ActivityEventLink to={`${baseLink}/projects/${projectName}`}>{projectName}</ActivityEventLink></span>;
        }
    };

    return (
        <ActivityEventListItem
            fragmentRef={data}
            icon={<ServiceAccountIcon />}
            primary={<React.Fragment>
                Service account {scope === 'GLOBAL' ? serviceAccount?.name : <ActivityEventLink to={serviceAccountLink}>{serviceAccount?.name}</ActivityEventLink>}{' '}
                    {actionText}
                {getContextLink(scope)}
            </React.Fragment>}
        />
    );
}

export default ActivityEventServiceAccountTarget;
