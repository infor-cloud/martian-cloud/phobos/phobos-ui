import { Avatar, Tooltip } from '@mui/material';
import { purple } from '@mui/material/colors';
import { SxProps } from '@mui/material';

interface Props {
    name: string
    sx?: SxProps
}

function ServiceAccountAvatar({ name, sx }: Props) {

    return (
        <Tooltip title={name}>
            <Avatar
                variant="rounded"
                sx={{ width: 24, height: 24, bgcolor: purple[300], ...sx }}
            >
                {name[0].toUpperCase()}
            </Avatar>
        </Tooltip>
    );
}

export default ServiceAccountAvatar
