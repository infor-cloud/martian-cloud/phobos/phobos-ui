import SearchIcon from "@mui/icons-material/Search";
import {
    InputAdornment,
    LinearProgress,
    ListSubheader,
    OutlinedInput,
    Select,
    SelectProps,
    TextField
} from "@mui/material";
import React from "react";

interface Props {
    children: React.ReactNode;
    onSearchChange: (value: string) => void;
    loading?: boolean
}

function SearchableSelect<Value>({ children, onSearchChange, loading, ...selectProps }: Props & SelectProps<Value>) {
    return (
        <Select
            {...selectProps}
            MenuProps={{ autoFocus: false }}
            onClose={() => onSearchChange("")}
            input={<OutlinedInput size="small" />}
        >
            <ListSubheader sx={{ backgroundColor: 'inherit' }}>
                <TextField
                    size="small"
                    autoFocus
                    autoComplete="off"
                    placeholder="Search by name"
                    fullWidth
                    InputProps={{
                        startAdornment: (
                            <InputAdornment position="start">
                                <SearchIcon />
                            </InputAdornment>
                        )
                    }}
                    onChange={(e) => onSearchChange(e.target.value)}
                    onKeyDown={(e) => {
                        if (e.key !== "Escape") {
                            // Prevent autoselecting item while typing
                            e.stopPropagation();
                        }
                    }}
                />
                {loading && <LinearProgress />}
            </ListSubheader>
            {children}
        </Select>
    );
}

export default SearchableSelect;
