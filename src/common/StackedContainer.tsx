import { Box, styled } from '@mui/system';

const StackedContainer = styled(
    Box
)(() => ({
    display: 'flex',
    flexWrap: 'wrap',
    margin: '0 -4px',
    '& > *': {
        margin: '4px'
    }
}));

export default StackedContainer;
