import OrganizationIcon from '@mui/icons-material/Business'
import ProjectIcon from '@mui/icons-material/FolderOpen';
import ReleaseLifecycleIcon from '@mui/icons-material/Sync';
import PipelineIcon from '@mui/icons-material/RocketLaunch';
import ActivitiesIcon from '@mui/icons-material/Timeline';
import ServiceAccountIcon from 'mdi-material-ui/LanConnect';
import UserIcon from '@mui/icons-material/PersonOutline';
import MemberIcon from '@mui/icons-material/PeopleOutline';
import PipelineTemplateIcon from '@mui/icons-material/AssignmentOutlined';
import EnvironmentIcon from '@mui/icons-material/Language';
import ReleaseIcon from '@mui/icons-material/Publish';
import ReleaseTemplateIcon from '@mui/icons-material/InsertDriveFile';
import CommentIcon from '@mui/icons-material/Comment';
import SettingsIcon from '@mui/icons-material/SettingsOutlined';
import ApprovalRuleIcon from '@mui/icons-material/AssignmentTurnedInOutlined';
import PlaylistAddCheckRoundedIcon from '@mui/icons-material/PlaylistAddCheckRounded';
import TaskIcon from '@mui/icons-material/Task';
import MetricIcon from '@mui/icons-material/BarChart';
import VariableIcon from '@mui/icons-material/WindowOutlined';
import { RobotOutline as AgentIcon, FileCogOutline as PipelineTaskIcon, SourceMerge as VCSProviderIcon } from 'mdi-material-ui';

export {
    OrganizationIcon,
    ProjectIcon,
    ActivitiesIcon,
    ReleaseLifecycleIcon,
    ServiceAccountIcon,
    MemberIcon,
    UserIcon,
    MemberIcon as RoleIcon,
    MemberIcon as TeamIcon,
    ApprovalRuleIcon,
    PipelineTemplateIcon,
    EnvironmentIcon,
    ReleaseIcon,
    ReleaseTemplateIcon,
    CommentIcon,
    PipelineIcon,
    SettingsIcon,
    AgentIcon,
    PipelineTaskIcon,
    VCSProviderIcon,
    MetricIcon,
    PlaylistAddCheckRoundedIcon as ToDoListIcon,
    TaskIcon as ApprovalIcon,
    VariableIcon
}
