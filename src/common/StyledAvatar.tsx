import { Avatar, styled } from '@mui/material';
import { purple } from '@mui/material/colors';

const StyledAvatar = styled(
    Avatar
)(() => ({
    width: 24,
    height: 24,
    marginRight: 1,
    backgroundColor: purple[300],
}));

export default StyledAvatar;
