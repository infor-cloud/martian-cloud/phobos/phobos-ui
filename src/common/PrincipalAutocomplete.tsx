import React, { useEffect, useState } from 'react';
import { Autocomplete, Box, CircularProgress, ListItem, ListItemText, TextField, useTheme } from '@mui/material';
import Gravatar from './Gravatar';
import StyledAvatar from './StyledAvatar';
import { UserIcon, TeamIcon, ServiceAccountIcon } from './Icons';
import match from 'autosuggest-highlight/match';
import parse from 'autosuggest-highlight/parse';
import graphql from 'babel-plugin-relay/macro';
import throttle from 'lodash.throttle';
import { useRelayEnvironment } from "react-relay/hooks";
import { fetchQuery } from 'relay-runtime';
import { PrincipalAutocompleteQuery } from './__generated__/PrincipalAutocompleteQuery.graphql';

export interface BaseOption {
    readonly id: string
    readonly type: string
    readonly label: string
    readonly avatarLabel: string
}

export interface UserOption extends BaseOption {
    readonly username: string
    readonly email: string
}

export type Option = UserOption | BaseOption;

interface Props {
    orgId?: string
    projectId?: string
    placeholder?: string
    onSelected: (value: Option | null) => void
    filterOptions: (options: Option[]) => Option[]
}

function PrincipalAutocomplete({ orgId, projectId, onSelected, filterOptions, placeholder = 'Select a principal' }: Props) {
    const theme = useTheme();
    const [options, setOptions] = useState<ReadonlyArray<Option> | null>(null);
    const [loading, setLoading] = useState<boolean>(false);
    const [inputValue, setInputValue] = useState('');

    const environment = useRelayEnvironment();

    const fetch = React.useMemo(
        () =>
            throttle(
                (
                    request: { input: string },
                    callback: (results?: readonly Option[]) => void,
                ) => {
                    fetchQuery<PrincipalAutocompleteQuery>(
                        environment,
                        graphql`
                          query PrincipalAutocompleteQuery($first: Int, $organizationId: String, $projectId: String, $scopes: [ScopeType!], $search: String!) {
                                serviceAccounts(first: $first, search: $search, organizationId: $organizationId, projectId: $projectId, scopes: $scopes) {
                                    edges {
                                        node {
                                            id
                                            name
                                        }
                                    }
                                }
                            teams (first: $first, search: $search){
                                edges {
                                    node {
                                        id
                                        name
                                    }
                                }
                            }
                            users (first: $first, search: $search) {
                                edges {
                                    node {
                                        id
                                        username
                                        email
                                    }
                                }
                            }
                          }
                        `,
                        {
                            search: request.input,
                            first: 20,
                            organizationId: orgId || null,
                            projectId: projectId || null,
                            scopes: orgId ? ['ORGANIZATION'] : ['ORGANIZATION', 'PROJECT']
                        },
                        { fetchPolicy: 'network-only' }
                    ).toPromise().then(async response => {
                        const options = [
                            ...response?.teams?.edges?.map(edge => ({
                                type: 'team',
                                id: edge?.node?.id,
                                label: edge?.node?.name,
                                avatarLabel: edge?.node?.name[0].toUpperCase()
                            } as Option)) || [],
                            ...response?.users?.edges?.map(edge => ({
                                type: 'user',
                                id: edge?.node?.id,
                                label: edge?.node?.username,
                                username: edge?.node?.username,
                                email: edge?.node?.email,
                                avatarLabel: edge?.node?.email
                            } as Option)) || [],
                            ...response?.serviceAccounts?.edges?.map(edge => ({
                                type: 'serviceaccount',
                                id: edge?.node?.id,
                                label: edge?.node?.name,
                                avatarLabel: edge?.node?.name[0].toUpperCase()
                            } as Option)) || []
                        ];
                        callback(options);
                    });
                },
                500,
            ),
        [environment],
    );

    useEffect(() => {
        let active = true;

        setLoading(true);

        fetch({ input: inputValue }, (results?: readonly Option[]) => {
            if (active) {
                setOptions(results ?? []);
                setLoading(false);
            }
        });

        return () => {
            active = false;
        };
    }, [fetch, inputValue]);

    return (
        <Autocomplete
            fullWidth
            size="small"
            value={null}
            inputValue={inputValue}
            onChange={(event: React.SyntheticEvent, value: Option | null) => {
                setInputValue('');
                onSelected(value);
            }}
            onInputChange={(_, newInputValue: string) => setInputValue(newInputValue)}
            filterOptions={filterOptions}
            isOptionEqualToValue={(option: Option, value: Option) => option.id === value.id}
            getOptionLabel={(option: Option) => option.label}
            renderOption={(props: React.HTMLAttributes<HTMLLIElement>, option: Option, { inputValue }) => {
                const matches = match(option.label, inputValue);
                const parts = parse(option.label, matches);
                return (
                    <ListItem dense {...props}>
                        <Box mr={1}>
                            {option.type === 'user' && <Gravatar width={24} height={24} email={option.avatarLabel} />}
                            {(option.type === 'team' || option.type === 'serviceaccount') && <StyledAvatar>{option.avatarLabel}</StyledAvatar>}
                        </Box>
                        <ListItemText primary={
                            parts.map((part: any, index: number) => (
                                <span
                                    key={index}
                                    style={{
                                        fontWeight: part.highlight ? 700 : 400,
                                    }}
                                >
                                    {part.text}
                                </span>
                            ))}
                            primaryTypographyProps={{ noWrap: true }}
                        />
                        <Box flex={1} />
                        <Box ml={1}>
                            {option.type === 'user' && <UserIcon sx={{ color: theme.palette.text.secondary }} />}
                            {option.type === 'team' && <TeamIcon sx={{ color: theme.palette.text.secondary }} />}
                            {option.type === 'serviceaccount' && <ServiceAccountIcon sx={{ color: theme.palette.text.secondary }} />}
                        </Box>
                    </ListItem>
                );
            }}
            options={options ?? []}
            loading={loading}
            renderInput={(params) => (
                <TextField
                    {...params}
                    placeholder={placeholder}
                    InputProps={{
                        ...params.InputProps,
                        endAdornment: (
                            <React.Fragment>
                                {loading ? <CircularProgress color="inherit" size={20} /> : null}
                                {params.InputProps.endAdornment}
                            </React.Fragment>
                        ),
                    }}
                />
            )}
        />
    );
}

export default PrincipalAutocomplete;
