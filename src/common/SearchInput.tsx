import { darken, styled } from '@mui/material/styles';
import TextField, { TextFieldProps } from '@mui/material/TextField';

const SearchInput = styled((props: TextFieldProps) => (
  <TextField fullWidth size="small" margin="none" autoComplete='off' {...props} />
))(({ theme }) => ({
  '& .MuiOutlinedInput-root': { background: theme.palette.mode === 'dark' ? darken(theme.palette.background.default, 0.5) : theme.palette.background.default }
}));

export default SearchInput;
