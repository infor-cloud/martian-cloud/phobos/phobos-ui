import DropdownIcon from "@mui/icons-material/ArrowDropDown";
import { Box, Button, CircularProgress, Popover } from "@mui/material";
import React, { Suspense, useState } from "react";
import OrganizationMenu from "../organizations/OrganizationMenu";

function OrganizationMenuButton() {
    const [open, setOpen] = useState(false);
    const [menuAnchorEl, setMenuAnchorEl] = useState<null | HTMLElement>(null);

    function onMenuOpen(event: React.MouseEvent<HTMLButtonElement>) {
        setMenuAnchorEl(event.currentTarget);
        setOpen(true);
    }

    function onMenuClose() {
        setOpen(false);
    }

    return (
        <Box>
            <Button
                color="inherit"
                sx={{ textTransform: "none", fontWeight: 600 }}
                onClick={onMenuOpen}>
                Organizations
                <DropdownIcon />
            </Button>
            <Popover
                id="organization-menu"
                open={open}
                anchorEl={menuAnchorEl}
                onClose={onMenuClose}
                anchorOrigin={{
                    vertical: 'bottom',
                    horizontal: 'center',
                }}
                transformOrigin={{
                    vertical: 'top',
                    horizontal: 'center',
                }}
            >
                <Box width={250} minHeight={200} display="flex" flexDirection="column">
                    <Suspense fallback={<Box
                        sx={{
                            flex: 1,
                            width: '100%',
                            display: 'flex',
                            alignItems: 'center',
                            justifyContent: 'center'
                        }}>
                        <CircularProgress />
                    </Box>}>
                        <OrganizationMenu onClose={onMenuClose} />
                    </Suspense>
                </Box>
            </Popover>
        </Box>
    );
}

export default OrganizationMenuButton
