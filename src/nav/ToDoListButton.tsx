import { Box, Badge, IconButton, Tooltip } from '@mui/material';
import { useFragment, useSubscription } from 'react-relay/hooks';
import graphql from 'babel-plugin-relay/macro';
import { Link as RouterLink } from 'react-router-dom';
import { ToDoListIcon } from '../common/Icons';
import { useMemo } from 'react';
import { ConnectionHandler, GraphQLSubscriptionConfig, RecordSourceProxy } from 'relay-runtime';
import { ToDoListButtonFragment$key } from './__generated__/ToDoListButtonFragment.graphql';
import { ToDoListButtonSubscription, ToDoListButtonSubscription$data } from './__generated__/ToDoListButtonSubscription.graphql';

const todoListButtonSubscription = graphql`
    subscription ToDoListButtonSubscription {
        userToDoItemEvents {
            totalUnresolved
        }
    }`

interface Props {
    fragmentRef: ToDoListButtonFragment$key;
}

function ToDoListButton({ fragmentRef }: Props) {
    const data = useFragment<ToDoListButtonFragment$key>(
        graphql`
            fragment ToDoListButtonFragment on Query
            {
                me {
                    ... on User {
                        id
                        todoItems(
                            first: 0,
                            resolved: false,
                        ) @connection(key: "ToDoListButton_todoItems") {
                            totalCount
                            edges {
                                node {
                                    id
                                }
                            }
                        }
                    }
                }
            }
        `, fragmentRef);

    const userId = data.me?.id as string;

    const todoListButtonSubscriptionConfig = useMemo<GraphQLSubscriptionConfig<ToDoListButtonSubscription>>(() => ({
        variables: {},
        subscription: todoListButtonSubscription,
        onCompleted: () => console.log('Subscription completed'),
        onError: () => console.warn('Subscription error'),
        updater: (store: RecordSourceProxy, payload: ToDoListButtonSubscription$data) => {
            const connectionId = ConnectionHandler.getConnectionID(
                userId,
                "ToDoListButton_todoItems",
                { resolved: false },
            )
            const connectionRecord = store.get(connectionId);

            if (connectionRecord) {
                // Simply update the number of unresolved items to be the totalCount.
                connectionRecord.setValue(payload.userToDoItemEvents.totalUnresolved, 'totalCount');
            }
        }
    }), [userId]);

    useSubscription<ToDoListButtonSubscription>(todoListButtonSubscriptionConfig);

    const totalCount = data?.me?.todoItems?.totalCount as number;

    return (
        <Box>
            <Tooltip title="To-Do list">
                <IconButton
                    color="inherit"
                    component={RouterLink} to="/todos"
                >
                    <Badge
                        max={99}
                        color="primary"
                        badgeContent={totalCount}
                    >
                        <ToDoListIcon />
                    </Badge>
                </IconButton>
            </Tooltip>
        </Box>
    );
}

export default ToDoListButton;
