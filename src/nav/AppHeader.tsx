import { Box, Button, Toolbar } from '@mui/material';
import AppBar, { AppBarProps } from '@mui/material/AppBar';
import { styled } from "@mui/material/styles";
import graphql from 'babel-plugin-relay/macro';
import { useFragment } from 'react-relay/hooks';
import { Link as RouterLink } from 'react-router-dom';
import AccountMenu from '../nav/AccountMenu';
import Link from '../routes/Link';
import OrganizationMenuButton from './OrganizationMenuButton';
import ToDoListButton from './ToDoListButton';
import { AppHeaderFragment$key } from './__generated__/AppHeaderFragment.graphql';

const StyledAppBar = styled(AppBar)<AppBarProps>(({ theme }) => ({
    boxShadow: 'none',
    borderBottomStyle: 'solid',
    borderBottomWidth: 1,
    borderBottomColor: theme.palette.divider,
    zIndex: theme.zIndex.drawer + 1,
    backgroundImage: 'none'
}));

interface Props {
    fragmentRef: AppHeaderFragment$key
}

function AppHeader(props: Props) {
    const data = useFragment<AppHeaderFragment$key>(
        graphql`
        fragment AppHeaderFragment on Query
        {
            ...ToDoListButtonFragment
            ...AccountMenuFragment
        }
        `, props.fragmentRef);

    return (
        <StyledAppBar position="fixed" color="inherit">
            <Toolbar>
                <Link underline="none" color="primary" variant="h5" fontWeight="bold" to='/'>Phobos</Link>
                <Box display="flex" flex="1" justifyContent="flex-start" alignItems="center" marginLeft={2}>
                    <OrganizationMenuButton />
                    <Button
                        color="inherit"
                        sx={{ textTransform: "none", fontWeight: 600 }}
                        component={RouterLink} to="/plugin-registry"
                    >
                        Plugins
                    </Button>
                </Box>
                <Box display="flex" flex="1" justifyContent="flex-end" alignItems="center">
                    <ToDoListButton fragmentRef={data} />
                    <AccountMenu fragmentRef={data} />
                </Box>
            </Toolbar>
        </StyledAppBar>
    );
}

export default AppHeader;
