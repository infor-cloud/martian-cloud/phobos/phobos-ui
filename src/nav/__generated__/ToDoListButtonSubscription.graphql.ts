/**
 * @generated SignedSource<<38dc2a16627089a3ad1e1f86b6579ea1>>
 * @lightSyntaxTransform
 * @nogrep
 */

/* tslint:disable */
/* eslint-disable */
// @ts-nocheck

import { ConcreteRequest, GraphQLSubscription } from 'relay-runtime';
export type ToDoListButtonSubscription$variables = Record<PropertyKey, never>;
export type ToDoListButtonSubscription$data = {
  readonly userToDoItemEvents: {
    readonly totalUnresolved: number;
  };
};
export type ToDoListButtonSubscription = {
  response: ToDoListButtonSubscription$data;
  variables: ToDoListButtonSubscription$variables;
};

const node: ConcreteRequest = (function(){
var v0 = [
  {
    "alias": null,
    "args": null,
    "concreteType": "ToDoItemEvent",
    "kind": "LinkedField",
    "name": "userToDoItemEvents",
    "plural": false,
    "selections": [
      {
        "alias": null,
        "args": null,
        "kind": "ScalarField",
        "name": "totalUnresolved",
        "storageKey": null
      }
    ],
    "storageKey": null
  }
];
return {
  "fragment": {
    "argumentDefinitions": [],
    "kind": "Fragment",
    "metadata": null,
    "name": "ToDoListButtonSubscription",
    "selections": (v0/*: any*/),
    "type": "Subscription",
    "abstractKey": null
  },
  "kind": "Request",
  "operation": {
    "argumentDefinitions": [],
    "kind": "Operation",
    "name": "ToDoListButtonSubscription",
    "selections": (v0/*: any*/)
  },
  "params": {
    "cacheID": "04c4799e41c0855f7e9704e83a41cca1",
    "id": null,
    "metadata": {},
    "name": "ToDoListButtonSubscription",
    "operationKind": "subscription",
    "text": "subscription ToDoListButtonSubscription {\n  userToDoItemEvents {\n    totalUnresolved\n  }\n}\n"
  }
};
})();

(node as any).hash = "c9b8e7b208ec0090fff8f64ee118f154";

export default node;
