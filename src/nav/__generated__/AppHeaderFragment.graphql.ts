/**
 * @generated SignedSource<<6cd5841b90dbefe5bd47c3f949b9f006>>
 * @lightSyntaxTransform
 * @nogrep
 */

/* tslint:disable */
/* eslint-disable */
// @ts-nocheck

import { Fragment, ReaderFragment } from 'relay-runtime';
import { FragmentRefs } from "relay-runtime";
export type AppHeaderFragment$data = {
  readonly " $fragmentSpreads": FragmentRefs<"AccountMenuFragment" | "ToDoListButtonFragment">;
  readonly " $fragmentType": "AppHeaderFragment";
};
export type AppHeaderFragment$key = {
  readonly " $data"?: AppHeaderFragment$data;
  readonly " $fragmentSpreads": FragmentRefs<"AppHeaderFragment">;
};

const node: ReaderFragment = {
  "argumentDefinitions": [],
  "kind": "Fragment",
  "metadata": null,
  "name": "AppHeaderFragment",
  "selections": [
    {
      "args": null,
      "kind": "FragmentSpread",
      "name": "ToDoListButtonFragment"
    },
    {
      "args": null,
      "kind": "FragmentSpread",
      "name": "AccountMenuFragment"
    }
  ],
  "type": "Query",
  "abstractKey": null
};

(node as any).hash = "5a8afc3910b42d801ca261575c5022a8";

export default node;
