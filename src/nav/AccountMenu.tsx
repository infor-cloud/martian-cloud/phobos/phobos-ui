import { Launch } from '@mui/icons-material';
import { Box, Link, List, ListItem, ListItemButton, ListItemText, Typography } from '@mui/material';
import Divider from '@mui/material/Divider';
import IconButton from '@mui/material/IconButton';
import Popover from '@mui/material/Popover';
import React, { useContext, useState } from 'react';
import { useAuth } from 'react-oidc-context';
import { useNavigate } from 'react-router-dom';
import { UserContext } from '../UserContext';
import Gravatar from '../common/Gravatar';
import config from '../common/config';
import { useFragment } from 'react-relay/hooks';
import graphql from 'babel-plugin-relay/macro';
import { AccountMenuFragment$key } from './__generated__/AccountMenuFragment.graphql';
import AboutDialog from './AboutDialog';

interface Props {
    fragmentRef: AccountMenuFragment$key;
}

function AccountMenu({ fragmentRef }: Props) {
    const navigate = useNavigate();
    const auth = useAuth();
    const user = useContext(UserContext);
    const [showAboutDialog, setShowAboutDialog] = useState(false);
    const [menuAnchorEl, setMenuAnchorEl] = useState<null | HTMLElement>(null);

    const data = useFragment<AccountMenuFragment$key>(
        graphql`
            fragment AccountMenuFragment on Query {
                version {
                    apiVersion
                    dbMigrationVersion
                    dbMigrationDirty
                }
        }
        `, fragmentRef);

    function onMenuOpen(event: React.MouseEvent<HTMLButtonElement>) {
        setMenuAnchorEl(event.currentTarget);
    }

    function onMenuClose() {
        setMenuAnchorEl(null);
    }

    function onShowGraphiql() {
        onMenuClose();
        navigate('graphiql');
    }

    function onShowAdminArea() {
        onMenuClose();
        navigate('admin');
    }

    function onShowAboutDialog() {
        onMenuClose();
        setShowAboutDialog(true);
    }

    return (
        <div>
            <IconButton onClick={onMenuOpen}>
                <Gravatar width={32} height={32} email={user.email} />
            </IconButton>
            <Popover
                id="account-menu"
                open={Boolean(menuAnchorEl)}
                anchorEl={menuAnchorEl}
                onClose={onMenuClose}
                anchorOrigin={{
                    vertical: 'bottom',
                    horizontal: 'center',
                }}
                transformOrigin={{
                    vertical: 'top',
                    horizontal: 'center',
                }}
            >
                <div>
                    <Box padding={2}>
                        <Typography>{user.username}</Typography>
                    </Box>
                    <Divider />
                    <List dense>
                        {user.admin && <ListItemButton>
                            <ListItemText onClick={onShowAdminArea}>
                                Admin Area
                            </ListItemText>
                        </ListItemButton>}
                        <ListItemButton onClick={onShowGraphiql}>
                            <ListItemText>GraphQL Editor</ListItemText>
                        </ListItemButton>
                        <ListItem
                            disablePadding
                            secondaryAction={
                                <IconButton
                                    LinkComponent={Link}
                                    edge='end'
                                    href={config.docsUrl}
                                    target='_blank'
                                    rel='noopener noreferrer'
                                    disableRipple
                                >
                                    <Launch fontSize='small' />
                                </IconButton>
                            }>
                            <ListItemButton LinkComponent={Link} href={config.docsUrl} target='_blank' rel='noopener noreferrer' dense>
                                <ListItemText primary="Documentation" />
                            </ListItemButton>
                        </ListItem>
                        {config.supportUrl !== '' && <ListItem
                            disablePadding
                            secondaryAction={
                                <IconButton
                                    LinkComponent={Link}
                                    edge='end'
                                    href={config.supportUrl}
                                    target='_blank'
                                    rel='noopener noreferrer'
                                    disableRipple
                                >
                                    <Launch fontSize='small' />
                                </IconButton>
                            }>
                            <ListItemButton LinkComponent={Link} href={config.supportUrl} target='_blank' rel='noopener noreferrer' dense>
                                <ListItemText primary="Support" />
                            </ListItemButton>
                        </ListItem>}
                        <ListItemButton>
                            <ListItemText onClick={onShowAboutDialog}>About Phobos</ListItemText>
                        </ListItemButton>
                        <ListItemButton onClick={() => auth.signoutRedirect()}>
                            <ListItemText primary="Sign Out" />
                        </ListItemButton>
                    </List>
                </div>
            </Popover>
            {showAboutDialog && <AboutDialog
                frontendVersion={config.version}
                backendVersion={data.version.apiVersion}
                dbMigrationVersion={data.version.dbMigrationVersion}
                dbMigrationDirty={data.version.dbMigrationDirty}
                onClose={() => setShowAboutDialog(false)}
            />}
        </div>
    );
}

export default AccountMenu;
