/**
 * @generated SignedSource<<c948933b2248c8eeb039014f88458c0d>>
 * @lightSyntaxTransform
 * @nogrep
 */

/* tslint:disable */
/* eslint-disable */
// @ts-nocheck

import { ConcreteRequest, Query } from 'relay-runtime';
export type MetricBucketPeriod = "day" | "hour" | "month" | "%future added value";
export type MetricName = "APPROVAL_REQUEST_COMPLETED" | "APPROVAL_REQUEST_CREATED" | "APPROVAL_REQUEST_WAIT_TIME" | "DEPLOYMENT_RECOVERY_TIME" | "DEPLOYMENT_UPDATED" | "PIPELINE_COMPLETED" | "PIPELINE_DURATION" | "PIPELINE_LEAD_TIME" | "%future added value";
export type MetricTagName = "APPROVAL_TARGET_TYPE" | "PIPELINE_STATUS" | "PIPELINE_TYPE" | "RELEASE_LIFECYCLE" | "%future added value";
export type AggregatedMetricStatisticsQuery = {
  environmentName?: string | null | undefined;
  metricName: MetricName;
  organizationId?: string | null | undefined;
  pipelineId?: string | null | undefined;
  projectId?: string | null | undefined;
  releaseId?: string | null | undefined;
  tags?: ReadonlyArray<MetricTagInput> | null | undefined;
};
export type MetricTagInput = {
  name: MetricTagName;
  value: string;
};
export type InsightsChartQuery$variables = {
  bucketCount: number;
  bucketPeriod: MetricBucketPeriod;
  queries: ReadonlyArray<AggregatedMetricStatisticsQuery>;
};
export type InsightsChartQuery$data = {
  readonly aggregatedMetricStatistics: ReadonlyArray<{
    readonly data: ReadonlyArray<{
      readonly average: number;
      readonly bucket: any;
      readonly sum: number;
    }>;
  }>;
};
export type InsightsChartQuery = {
  response: InsightsChartQuery$data;
  variables: InsightsChartQuery$variables;
};

const node: ConcreteRequest = (function(){
var v0 = {
  "defaultValue": null,
  "kind": "LocalArgument",
  "name": "bucketCount"
},
v1 = {
  "defaultValue": null,
  "kind": "LocalArgument",
  "name": "bucketPeriod"
},
v2 = {
  "defaultValue": null,
  "kind": "LocalArgument",
  "name": "queries"
},
v3 = [
  {
    "alias": null,
    "args": [
      {
        "kind": "Variable",
        "name": "bucketCount",
        "variableName": "bucketCount"
      },
      {
        "kind": "Variable",
        "name": "bucketPeriod",
        "variableName": "bucketPeriod"
      },
      {
        "kind": "Variable",
        "name": "queries",
        "variableName": "queries"
      }
    ],
    "concreteType": "AggregatedMetricStatisticsResult",
    "kind": "LinkedField",
    "name": "aggregatedMetricStatistics",
    "plural": true,
    "selections": [
      {
        "alias": null,
        "args": null,
        "concreteType": "AggregatedMetricStatistics",
        "kind": "LinkedField",
        "name": "data",
        "plural": true,
        "selections": [
          {
            "alias": null,
            "args": null,
            "kind": "ScalarField",
            "name": "bucket",
            "storageKey": null
          },
          {
            "alias": null,
            "args": null,
            "kind": "ScalarField",
            "name": "sum",
            "storageKey": null
          },
          {
            "alias": null,
            "args": null,
            "kind": "ScalarField",
            "name": "average",
            "storageKey": null
          }
        ],
        "storageKey": null
      }
    ],
    "storageKey": null
  }
];
return {
  "fragment": {
    "argumentDefinitions": [
      (v0/*: any*/),
      (v1/*: any*/),
      (v2/*: any*/)
    ],
    "kind": "Fragment",
    "metadata": null,
    "name": "InsightsChartQuery",
    "selections": (v3/*: any*/),
    "type": "Query",
    "abstractKey": null
  },
  "kind": "Request",
  "operation": {
    "argumentDefinitions": [
      (v1/*: any*/),
      (v0/*: any*/),
      (v2/*: any*/)
    ],
    "kind": "Operation",
    "name": "InsightsChartQuery",
    "selections": (v3/*: any*/)
  },
  "params": {
    "cacheID": "2f779990db3200c845d889c9b36cfdf2",
    "id": null,
    "metadata": {},
    "name": "InsightsChartQuery",
    "operationKind": "query",
    "text": "query InsightsChartQuery(\n  $bucketPeriod: MetricBucketPeriod!\n  $bucketCount: Int!\n  $queries: [AggregatedMetricStatisticsQuery!]!\n) {\n  aggregatedMetricStatistics(bucketPeriod: $bucketPeriod, bucketCount: $bucketCount, queries: $queries) {\n    data {\n      bucket\n      sum\n      average\n    }\n  }\n}\n"
  }
};
})();

(node as any).hash = "ae5d9bea00dbe895355b04481dc20d37";

export default node;
