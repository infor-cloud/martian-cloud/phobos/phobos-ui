import InsightsChart from './InsightsChart';
import InsightsChartContainer, { ChartHeight } from './InsightsChartContainer';
import { TimeRangeOptions } from './InsightsTimeRangeSelector';

interface Props {
    timeRange: string
    organizationId?: string
    projectId?: string
    filters: any[]
    showLegend: boolean
}

function InsightsChartApprovalRequestWaitTime({ timeRange, filters, organizationId, projectId, showLegend }: Props) {
    return (
        <InsightsChartContainer
            title="Approval Request Wait Time"
            tooltip="Approval request wait time tracks the time it takes for an approval request to be approved"
        >
            <InsightsChart
                query={{
                    bucketPeriod: TimeRangeOptions[timeRange].bucketPeriod,
                    bucketCount: TimeRangeOptions[timeRange].bucketCount,
                    series: filters.map(filter => ({
                        metricName: 'APPROVAL_REQUEST_WAIT_TIME',
                        organizationId,
                        projectId,
                        ...filter
                    }))
                }}
                chartOptions={{
                    type: 'line',
                    height: ChartHeight,
                    legend: {
                        display: showLegend
                    },
                    y: { title: 'Hours (avg)' }
                }}
                getDatasetLabel={(_, index) => filters[index].label}
                getDataValue={(data) => data.average / 60 / 60}
            />
        </InsightsChartContainer>
    );
}

export default InsightsChartApprovalRequestWaitTime;
