import InfoIcon from '@mui/icons-material/Info';
import { Box, CircularProgress, Paper, Tooltip, Typography } from '@mui/material';
import { Suspense } from 'react';

export const ChartHeight = 320;

interface Props {
    title: string,
    tooltip?: string,
    children: React.ReactNode
}

function InsightsChartContainer({ title, children, tooltip }: Props) {
    return (
        <Paper sx={{ padding: 2, flex: 1 }}>
            <Box display="flex" alignItems="center" mb={1}>
                <Typography variant="h6" color="textSecondary">{title}</Typography>
                {tooltip && <Tooltip title={tooltip}>
                    <InfoIcon sx={{
                        width: 16,
                        height: 16,
                        marginLeft: '10px',
                        verticalAlign: 'middle',
                        opacity: '20%',
                        transition: 'ease',
                        transitionDuration: '300ms',
                        ":hover": {
                            opacity: '100%'
                        }
                    }} />
                </Tooltip>}
            </Box>
            <Suspense fallback={<Box
                sx={{
                    width: '100%',
                    minHeight: ChartHeight,
                    display: 'flex',
                    alignItems: 'center',
                    justifyContent: 'center'
                }}>
                <CircularProgress />
            </Box>}>{children}</Suspense>
        </Paper>
    );
}

export default InsightsChartContainer;
