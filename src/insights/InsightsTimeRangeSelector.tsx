import ToggleButton from '@mui/material/ToggleButton';
import ToggleButtonGroup from '@mui/material/ToggleButtonGroup';

export const TimeRangeOptions = {
    '1d': {
        bucketPeriod: 'hour',
        bucketCount: 24,
        timeScaleUnit: 'hour',
    },
    '7d': {
        bucketPeriod: 'day',
        bucketCount: 7,
    },
    '30d': {
        bucketPeriod: 'day',
        bucketCount: 30,
    },
    '3m': {
        bucketPeriod: 'month',
        bucketCount: 3,
    },
    '12m': {
        bucketPeriod: 'month',
        bucketCount: 12,
    },
} as any;

interface Props {
    value: string;
    onChange: (value: string) => void;
}

function InsightsTimeRangeSelector({ value, onChange }: Props) {
    const onTimeRangeChange = (_: React.MouseEvent<HTMLElement>, value: string | null) => {
        if (value) {
            onChange(value);
        }
    };

    return (
        <ToggleButtonGroup value={value} exclusive color="secondary" onChange={onTimeRangeChange} size="small">
            {Object.keys(TimeRangeOptions).map((key: string) => <ToggleButton key={key} value={key}>{key}</ToggleButton>)}
        </ToggleButtonGroup>
    );
}

export default InsightsTimeRangeSelector;
