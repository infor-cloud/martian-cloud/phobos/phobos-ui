import InsightsChart from './InsightsChart';
import InsightsChartContainer, { ChartHeight } from './InsightsChartContainer';
import { TimeRangeOptions } from './InsightsTimeRangeSelector';

interface Props {
    timeRange: string
    organizationId?: string
    projectId?: string
    filters: any[]
    showLegend: boolean
}

function InsightsChartApprovalRequestCompleted({ timeRange, filters, organizationId, projectId, showLegend }: Props) {
    return (
        <InsightsChartContainer
            title="Approval Requests Completed"
            tooltip="Approval requests completed tracks the number of approval requests that have been completed"
        >
            <InsightsChart
                query={{
                    bucketPeriod: TimeRangeOptions[timeRange].bucketPeriod,
                    bucketCount: TimeRangeOptions[timeRange].bucketCount,
                    series: filters.map(filter => ({
                        metricName: 'APPROVAL_REQUEST_COMPLETED',
                        organizationId,
                        projectId,
                        ...filter
                    }))
                }}
                chartOptions={{
                    type: 'line',
                    height: ChartHeight,
                    legend: {
                        display: showLegend
                    },
                    y: { title: 'Count' }
                }}
                getDatasetLabel={(_, index) => filters[index].label}
                getDataValue={(data) => data.sum}
            />
        </InsightsChartContainer>
    );
}

export default InsightsChartApprovalRequestCompleted;
