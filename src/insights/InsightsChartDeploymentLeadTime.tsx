import InsightsChart from './InsightsChart';
import InsightsChartContainer, { ChartHeight } from './InsightsChartContainer';
import { TimeRangeOptions } from './InsightsTimeRangeSelector';

interface Props {
    timeRange: string
    organizationId?: string
    projectId?: string
    filters: any[]
    showLegend: boolean
}

function InsightsChartDeploymentLeadTime({ timeRange, filters, organizationId, projectId, showLegend }: Props) {
    return (
        <InsightsChartContainer
            title="Deployment Lead Time"
            tooltip="Deployment lead time is a DORA metric that tracks the time it takes for code changes to be deployed"
        >
            <InsightsChart
                query={{
                    bucketPeriod: TimeRangeOptions[timeRange].bucketPeriod,
                    bucketCount: TimeRangeOptions[timeRange].bucketCount,
                    series: filters.map(filter => ({
                        metricName: 'PIPELINE_LEAD_TIME',
                        organizationId,
                        projectId,
                        tags: [{ name: 'PIPELINE_TYPE', value: "DEPLOYMENT" }],
                        ...filter
                    }))
                }}
                chartOptions={{
                    type: 'line',
                    height: ChartHeight,
                    legend: {
                        display: showLegend
                    },
                    y: { title: 'Hours (avg)' }
                }}
                getDatasetLabel={(_, index) => filters[index].label}
                getDataValue={(data) => data.average / 60 / 60}
            />
        </InsightsChartContainer>
    );
}

export default InsightsChartDeploymentLeadTime;
