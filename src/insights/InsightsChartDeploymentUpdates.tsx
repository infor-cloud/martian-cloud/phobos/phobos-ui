import InsightsChart from './InsightsChart';
import InsightsChartContainer, { ChartHeight } from './InsightsChartContainer';
import { TimeRangeOptions } from './InsightsTimeRangeSelector';

interface Props {
    timeRange: string
    organizationId?: string
    projectId?: string
    filters: any[]
    showLegend: boolean
}

function InsightsChartDeploymentUpdates({ timeRange, filters, organizationId, projectId, showLegend }: Props) {
    return (
        <InsightsChartContainer
            title="Deployment Changes"
            tooltip="Deployment changes tracks the number of changes made to a deployment over time"
        >
            <InsightsChart
                query={{
                    bucketPeriod: TimeRangeOptions[timeRange].bucketPeriod,
                    bucketCount: TimeRangeOptions[timeRange].bucketCount,
                    series: filters.map(filter => ({
                        metricName: 'DEPLOYMENT_UPDATED',
                        organizationId,
                        projectId,
                        ...filter
                    }))
                }}
                chartOptions={{
                    type: 'line',
                    height: ChartHeight,
                    legend: {
                        display: showLegend
                    },
                    y: { title: 'Number of Updates' }
                }}
                getDatasetLabel={(_, index) => filters[index].label}
                getDataValue={(data) => data.sum}
            />
        </InsightsChartContainer>
    );
}

export default InsightsChartDeploymentUpdates;
