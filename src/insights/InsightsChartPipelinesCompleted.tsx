import InsightsChart from './InsightsChart';
import InsightsChartContainer, { ChartHeight } from './InsightsChartContainer';
import { TimeRangeOptions } from './InsightsTimeRangeSelector';

interface Props {
    timeRange: string
    organizationId?: string
    projectId?: string
    filters: any[]
    showLegend: boolean
}

function InsightsChartPipelinesCompleted({ timeRange, filters, organizationId, projectId, showLegend }: Props) {
    return (
        <InsightsChartContainer
            title="Completed Pipelines"
            tooltip="Completed pipelines tracks the number of pipelines that have been completed"
        >
            <InsightsChart
                query={{
                    bucketPeriod: TimeRangeOptions[timeRange].bucketPeriod,
                    bucketCount: TimeRangeOptions[timeRange].bucketCount,
                    series: filters.map(filter => ({
                        metricName: 'PIPELINE_COMPLETED',
                        organizationId,
                        projectId,
                        ...filter
                    }))
                }}
                chartOptions={{
                    type: 'line',
                    height: ChartHeight,
                    legend: {
                        display: showLegend
                    },
                    y: { title: 'Completed' }
                }}
                getDatasetLabel={(_, index) => filters[index].label}
                getDataValue={(data) => data.sum}
            />
        </InsightsChartContainer>
    );
}

export default InsightsChartPipelinesCompleted;
