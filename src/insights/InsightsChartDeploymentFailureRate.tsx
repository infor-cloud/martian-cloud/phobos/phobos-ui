import InsightsChart from './InsightsChart';
import InsightsChartContainer, { ChartHeight } from './InsightsChartContainer';
import { TimeRangeOptions } from './InsightsTimeRangeSelector';

interface Props {
    timeRange: string
    organizationId?: string
    projectId?: string
    filters: any[]
    showLegend: boolean
}

function InsightsChartDeploymentFailureRate({ timeRange, filters, organizationId, projectId, showLegend }: Props) {
    return (
        <InsightsChartContainer
            title="Deployment Failure Rate"
            tooltip="Deployment failure rate is a DORA metric that tracks the percentage of failed deployments"
        >
            <InsightsChart
                query={{
                    bucketPeriod: TimeRangeOptions[timeRange].bucketPeriod,
                    bucketCount: TimeRangeOptions[timeRange].bucketCount,
                    series: filters.flatMap(filter => ([
                        {
                            metricName: 'PIPELINE_COMPLETED',
                            organizationId,
                            projectId,
                            tags: [
                                { name: 'PIPELINE_TYPE', value: "DEPLOYMENT" },
                                { name: 'PIPELINE_STATUS', value: "FAILED" }
                            ],
                            ...filter
                        },
                        {
                            metricName: 'PIPELINE_COMPLETED',
                            organizationId,
                            projectId,
                            tags: [{ name: 'PIPELINE_TYPE', value: "DEPLOYMENT" }],
                            ...filter
                        }
                    ]))
                }}
                chartOptions={{
                    type: 'line',
                    height: ChartHeight,
                    legend: {
                        display: showLegend
                    },
                    y: { title: 'Percentage', labelCallback: (value: string | number) => value + '%' }
                }}
                datasetReducer={(datasets: readonly any[]) => {
                    return datasets.reduce((acc: any[], dataset: any, index: number) => {
                        if (index % 2 === 0) {
                            acc.push({
                                ...dataset,
                                data: dataset.data.map((point: any, j: number) => {
                                    const total = datasets[index + 1].data[j].sum;
                                    const failureRate = total === 0 ? 0 : point.sum / total * 100;
                                    return { ...point, value: failureRate }
                                })
                            });
                        }
                        return acc;
                    }, []);
                }}
                getDatasetLabel={(_, index) => filters[index].label}
                getDataValue={(data) => data.value}
            />
        </InsightsChartContainer>
    );
}

export default InsightsChartDeploymentFailureRate;
