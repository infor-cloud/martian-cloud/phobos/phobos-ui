import InsightsChart from './InsightsChart';
import InsightsChartContainer, { ChartHeight } from './InsightsChartContainer';
import { TimeRangeOptions } from './InsightsTimeRangeSelector';

interface Props {
    timeRange: string
    organizationId?: string
    projectId?: string
    filters: any[]
    showLegend: boolean
}

function InsightsChartApprovalRequestCreated({ timeRange, filters, organizationId, projectId, showLegend }: Props) {
    return (
        <InsightsChartContainer
            title="Approval Requests Created"
            tooltip="Approval requests created tracks the number of approval requests that have been created"
        >
            <InsightsChart
                query={{
                    bucketPeriod: TimeRangeOptions[timeRange].bucketPeriod,
                    bucketCount: TimeRangeOptions[timeRange].bucketCount,
                    series: filters.map(filter => ({
                        metricName: 'APPROVAL_REQUEST_CREATED',
                        organizationId,
                        projectId,
                        ...filter
                    }))
                }}
                chartOptions={{
                    type: 'line',
                    height: ChartHeight,
                    legend: {
                        display: showLegend
                    },
                    y: { title: 'Count' }
                }}
                getDatasetLabel={(_, index) => filters[index].label}
                getDataValue={(data) => data.sum}
            />
        </InsightsChartContainer>
    );
}

export default InsightsChartApprovalRequestCreated;
