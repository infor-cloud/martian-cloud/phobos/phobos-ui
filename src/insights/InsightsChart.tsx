import { Box } from '@mui/material';
import { amber, blue, cyan, green, indigo, orange, red, teal, yellow, purple } from "@mui/material/colors";
import graphql from 'babel-plugin-relay/macro';
import { TimeUnit, ChartType } from 'chart.js';
import moment from 'moment';
import { useMemo } from 'react';
import { Chart } from 'react-chartjs-2';
import { useLazyLoadQuery } from 'react-relay/hooks';
import { FORMATS } from './chartjsMomentAdapter.js';
import { InsightsChartQuery, MetricBucketPeriod, MetricName, MetricTagName } from './__generated__/InsightsChartQuery.graphql.js';

export const MAX_SERIES = 30;

interface ChartOptions {
    type: ChartType
    height: number
    legend: {
        display: boolean
    }
    y: {
        title: string
        labelCallback?: (value: string | number) => string
    }
}

interface QueryOptions {
    bucketPeriod: MetricBucketPeriod
    bucketCount: number
    series: { metricName: MetricName, organizationId?: string, projectId?: string, environmentName?: string, tags?: { name: MetricTagName, value: string }[] }[]
}

const chartColors = [
    blue,
    cyan,
    indigo,
    teal,
    red,
    purple,
    green,
    amber,
    yellow,
    orange
] as any;
const chartColorShades = [300, 500, 700];

const chartColorPalette = chartColorShades.flatMap(shade => chartColors.map((color: any) => color[shade]));

const graphqlQuery = graphql`
    query InsightsChartQuery($bucketPeriod: MetricBucketPeriod!, $bucketCount: Int!, $queries: [AggregatedMetricStatisticsQuery!]!) {
        aggregatedMetricStatistics(
            bucketPeriod: $bucketPeriod,
            bucketCount: $bucketCount,
            queries: $queries
        ) {
            data {
                bucket
                sum
                average
            }
        }
    }
`;

interface Props {
    query: QueryOptions
    projectId?: string
    chartOptions: ChartOptions
    getDataValue: (data: any) => number
    getDatasetLabel: (dataset: any, index: number) => string
    datasetReducer?: (datasets: readonly any[]) => any[];
}

function InsightsChart({ query, chartOptions, getDataValue, getDatasetLabel, datasetReducer }: Props) {
    const queryData = useLazyLoadQuery<InsightsChartQuery>(graphqlQuery, {
        bucketPeriod: query.bucketPeriod,
        bucketCount: query.bucketCount,
        queries: query.series.slice(0, MAX_SERIES).map(item => ({
            ...item
        }))
    }, { fetchPolicy: 'network-only' });

    const lineChartOptions = useMemo(() => ({
        responsive: true,
        maintainAspectRatio: false,
        plugins: {
            title: {
                display: false
            },
            legend: {
                display: chartOptions.legend.display,
                position: 'bottom' as const,
                labels: {
                    boxHeight: 6,
                    usePointStyle: true
                }
            },
        },
        scales: {
            x: {
                display: true,
                type: 'timeseries' as const,
                time: {
                    unit: query.bucketPeriod as TimeUnit,
                    displayFormats: {
                        datetime: (FORMATS as any)[query.bucketPeriod],
                    }
                },
                title: {
                    display: false
                }
            },
            y: {
                display: true,
                beginAtZero: true,
                title: {
                    display: true,
                    text: chartOptions.y.title
                },
                ticks: {
                    callback: chartOptions.y.labelCallback
                }
            }
        }
    }), [query.bucketPeriod, chartOptions.legend.display]);

    const datasets = datasetReducer ? datasetReducer(queryData.aggregatedMetricStatistics) : queryData.aggregatedMetricStatistics;

    return (
        <Box width="100%" position="relative" height={chartOptions.height}>
            <Box position="absolute" width="100%" height="100%">
                <Box position="relative" height={chartOptions.height}>
                    <Chart
                        type={chartOptions.type}
                        data={{
                            labels: queryData.aggregatedMetricStatistics.length > 0 ? queryData.aggregatedMetricStatistics[0].data.map((stat) => moment(stat.bucket).utc()) : [],
                            datasets: datasets.map((result, index) => {
                                const color = chartColorPalette[index % chartColorPalette.length];
                                return {
                                    pointHoverBackgroundColor: color,
                                    fill: false,
                                    label: getDatasetLabel(result, index),
                                    data: result.data.map(getDataValue),
                                    borderColor: color,
                                    backgroundColor: color,
                                    tension: 0.1
                                };
                            })
                        }}
                        options={lineChartOptions}
                    />
                </Box>
            </Box>
        </Box>
    );
}

export default InsightsChart;
