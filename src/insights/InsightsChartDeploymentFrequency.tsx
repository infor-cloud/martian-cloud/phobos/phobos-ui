import InsightsChart from './InsightsChart';
import InsightsChartContainer, { ChartHeight } from './InsightsChartContainer';
import { TimeRangeOptions } from './InsightsTimeRangeSelector';

interface Props {
    timeRange: string
    organizationId?: string
    projectId?: string
    filters: any[]
    showLegend: boolean
}

function InsightsChartDeploymentFrequency({ timeRange, filters, organizationId, projectId, showLegend }: Props) {
    return (
        <InsightsChartContainer
            title="Deployment Frequency"
            tooltip="Deployment frequency is a DORA metric that tracks the frequency in which code changes are deployed"
        >
            <InsightsChart
                query={{
                    bucketPeriod: TimeRangeOptions[timeRange].bucketPeriod,
                    bucketCount: TimeRangeOptions[timeRange].bucketCount,
                    series: filters.map(filter => ({
                        metricName: 'PIPELINE_COMPLETED',
                        organizationId,
                        projectId,
                        tags: [{ name: 'PIPELINE_TYPE', value: "DEPLOYMENT" }],
                        ...filter
                    }))
                }}
                chartOptions={{
                    type: 'line',
                    height: ChartHeight,
                    legend: {
                        display: showLegend
                    },
                    y: { title: 'Deployment Count' }
                }}
                getDatasetLabel={(_, index) => filters[index].label}
                getDataValue={(data) => data.sum}
            />
        </InsightsChartContainer>
    );
}

export default InsightsChartDeploymentFrequency;
