import {
    Chart as ChartJS,
    Legend,
    LineController,
    LineElement,
    LinearScale,
    PointElement,
    TimeSeriesScale,
    Title,
    Tooltip
} from 'chart.js';

ChartJS.register(
    LinearScale,
    PointElement,
    Title,
    Tooltip,
    Legend,
    LineElement,
    TimeSeriesScale,
    LineController
);
