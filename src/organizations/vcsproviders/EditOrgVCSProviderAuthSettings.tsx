import { Box } from '@mui/material';
import graphql from 'babel-plugin-relay/macro';
import { useParams, useOutletContext } from 'react-router-dom';
import { useFragment, useLazyLoadQuery } from "react-relay/hooks";
import OrganizationBreadcrumbs from '../OrganizationBreadcrumbs';
import EditVCSProviderAuthSettings from '../../vcsproviders/EditVCSProviderAuthSettings';
import { EditOrgVCSProviderAuthSettingsFragment_organization$key } from './__generated__/EditOrgVCSProviderAuthSettingsFragment_organization.graphql';
import { EditOrgVCSProviderAuthSettingsQuery } from './__generated__/EditOrgVCSProviderAuthSettingsQuery.graphql';

function EditOrgVCSProviderAuthSettings() {
    const vcsProviderId = useParams().vcsProviderId as string;
    const context = useOutletContext<EditOrgVCSProviderAuthSettingsFragment_organization$key>();

    const organization = useFragment<EditOrgVCSProviderAuthSettingsFragment_organization$key>(
        graphql`
            fragment EditOrgVCSProviderAuthSettingsFragment_organization on Organization
            {
                name
            }
        `, context
    );

    const queryData = useLazyLoadQuery<EditOrgVCSProviderAuthSettingsQuery>(graphql`
        query EditOrgVCSProviderAuthSettingsQuery($id: String!) {
            node(id: $id) {
                ... on VCSProvider {
                    id
                    name
                    ...EditVCSProviderAuthSettingsFragment_vcsProvider
                }
            }
        }
    `, { id: vcsProviderId });

    const vcsProvider = queryData.node as any;

    return (
        <Box>
            <OrganizationBreadcrumbs
                orgName={organization.name}
                childRoutes={[
                    { title: "vcs providers", path: 'vcs_providers' },
                    { title: vcsProvider.name, path: vcsProvider.id },
                    { title: "edit auth settings", path: 'edit_auth_settings' },
                ]}
            />
            <EditVCSProviderAuthSettings fragmentRef={vcsProvider} />
        </Box>
    );
}

export default EditOrgVCSProviderAuthSettings;
