import { Box } from '@mui/material';
import { useOutletContext, useParams } from 'react-router-dom';
import OrganizationBreadcrumbs from '../OrganizationBreadcrumbs';
import graphql from 'babel-plugin-relay/macro';
import { useFragment, useLazyLoadQuery } from "react-relay/hooks";
import EditVCSProvider from '../../vcsproviders/EditVCSProvider';
import { EditOrgVCSProviderFragment_organization$key } from './__generated__/EditOrgVCSProviderFragment_organization.graphql';
import { EditOrgVCSProviderQuery } from './__generated__/EditOrgVCSProviderQuery.graphql';

function EditOrgVCSProvider() {
    const vcsProviderId = useParams().vcsProviderId as string;
    const context = useOutletContext<EditOrgVCSProviderFragment_organization$key>();

    const organization = useFragment<EditOrgVCSProviderFragment_organization$key>(
        graphql`
            fragment EditOrgVCSProviderFragment_organization on Organization
            {
                id
                name
            }
        `, context
    );

    const queryData = useLazyLoadQuery<EditOrgVCSProviderQuery>(graphql`
        query EditOrgVCSProviderQuery($id: String!) {
            node(id: $id) {
                ... on VCSProvider {
                    name
                    ...EditVCSProviderFragment_vcsProvider
                }
            }
        }
    `, { id: vcsProviderId });

    const vcsProvider = queryData.node as any;

    return queryData.node ? (
        <Box>
            <OrganizationBreadcrumbs
                orgName={organization.name}
                childRoutes={[
                    { title: "vcs providers", path: 'vcs_providers' },
                    { title: vcsProvider.name, path: vcsProviderId },
                    { title: "edit", path: 'edit' },
                ]}
            />
            <EditVCSProvider fragmentRef={queryData.node} />
        </Box>
    ) : <Box>VCS Provider Not found</Box>;
}

export default EditOrgVCSProvider;
