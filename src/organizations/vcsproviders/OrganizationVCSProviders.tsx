import React, { useMemo, useState } from 'react';
import { Box, Button, Typography, useTheme } from '@mui/material';
import graphql from 'babel-plugin-relay/macro';
import throttle from 'lodash.throttle';
import { ConnectionHandler, fetchQuery, useFragment, useLazyLoadQuery, usePaginationFragment, useRelayEnvironment } from 'react-relay/hooks';
import { Link as RouterLink, useOutletContext } from 'react-router-dom';
import OrganizationBreadcrumbs from '../OrganizationBreadcrumbs';
import VCSProviderList from '../../vcsproviders/VCSProviderList';
import SearchInput from '../../common/SearchInput';
import { OrganizationVCSProvidersFragment_organization$key } from './__generated__/OrganizationVCSProvidersFragment_organization.graphql';
import { OrganizationVCSProvidersFragment_vcsProviders$key } from './__generated__/OrganizationVCSProvidersFragment_vcsProviders.graphql';
import { OrganizationVCSProvidersPaginationQuery } from './__generated__/OrganizationVCSProvidersPaginationQuery.graphql';
import { OrganizationVCSProvidersQuery } from './__generated__/OrganizationVCSProvidersQuery.graphql';

const INITIAL_ITEM_COUNT = 100;

const DESCRIPTION = 'Version control system (VCS) providers allow connections between organizations or projects and Git repositories.';

const NewButton =
    <Button
        sx={{ minWidth: 200 }}
        component={RouterLink}
        variant="outlined"
        to="new"
    >
        New VCS Provider
    </Button>;

const query = graphql`
    query OrganizationVCSProvidersQuery($orgId: String!, $first: Int, $last: Int, $after: String, $before: String, $search: String) {
        ...OrganizationVCSProvidersFragment_vcsProviders
    }
`;

export function GetConnections(orgId: string): [string] {
    const connectionId = ConnectionHandler.getConnectionID(
        orgId,
        'OrganizationVCSProviders_vcsProviders',
        { sort: 'UPDATED_AT_DESC' }
    );
    return [connectionId];
}

function OrganizationVCSProviders() {
    const context = useOutletContext<OrganizationVCSProvidersFragment_organization$key>();
    const theme = useTheme();
    const [search, setSearch] = useState<string | undefined>('');
    const [isRefreshing, setIsRefreshing] = useState(false);

    const organization = useFragment<OrganizationVCSProvidersFragment_organization$key>(
        graphql`
            fragment OrganizationVCSProvidersFragment_organization on Organization
            {
                id
                name
            }
        `, context
    );

    const queryData = useLazyLoadQuery<OrganizationVCSProvidersQuery>(query, { orgId: organization.id, first: INITIAL_ITEM_COUNT }, { fetchPolicy: 'store-and-network' });

    const { data, loadNext, hasNext, refetch } = usePaginationFragment<OrganizationVCSProvidersPaginationQuery, OrganizationVCSProvidersFragment_vcsProviders$key>(
        graphql`
      fragment OrganizationVCSProvidersFragment_vcsProviders on Query
        @refetchable(queryName: "OrganizationVCSProvidersPaginationQuery") {
            node(id: $orgId) {
                ...on Organization {
                    vcsProviders(
                        after: $after
                        before: $before
                        first: $first
                        last: $last
                        search: $search
                        sort: UPDATED_AT_DESC
                    ) @connection(key: "OrganizationVCSProviders_vcsProviders") {
                        totalCount
                        edges {
                            node {
                                id
                            }
                        }
                        ...VCSProviderListFragment_vcsProviders
                    }
                }
            }
        }
    `, queryData);

    const environment = useRelayEnvironment();

    const fetch = useMemo(
        () =>
            throttle(
                (input?: string) => {
                    setIsRefreshing(true);

                    const normalizedInput = input?.trim();

                    fetchQuery(environment, query, { first: INITIAL_ITEM_COUNT, orgId: organization.id, search: normalizedInput })
                        .subscribe({
                            complete: () => {
                                setIsRefreshing(false);
                                setSearch(input);

                                // *After* the query has been fetched, we call
                                // refetch again to re-render with the updated data.
                                // At this point the data for the query should
                                // be cached, so we use the 'store-only'
                                // fetchPolicy to avoid suspending.
                                refetch({
                                    first: INITIAL_ITEM_COUNT,
                                    search: normalizedInput
                                }, {
                                    fetchPolicy: 'store-only'
                                });
                            },
                            error: () => {
                                setIsRefreshing(false);
                            }
                        });
                },
                2000,
                { leading: false, trailing: true }
            ),
        [environment, refetch, organization.name]
    );

    const onSearchChange = (event: React.ChangeEvent<HTMLInputElement>) => {
        fetch(event.target.value.toLowerCase().trim());
    };

    const onKeyDown = (event: React.KeyboardEvent<HTMLInputElement>) => {
        // Only handle enter key type
        if (event.key === 'Enter') {
            fetch.flush();
        }
    };

    const edges = data?.node?.vcsProviders?.edges ?? [];

    return (
        <Box>
            {(search !== '' || edges.length !== 0) && (data.node?.vcsProviders) && <Box>
                <OrganizationBreadcrumbs
                    orgName={organization.name}
                    childRoutes={[
                        { title: "vcs providers", path: 'vcs_providers' }
                    ]}
                />
                <Box sx={{
                    display: 'flex',
                    flexDirection: 'row',
                    justifyContent: 'space-between',
                    [theme.breakpoints.down('md')]: {
                        flexDirection: 'column',
                        alignItems: 'flex-start',
                        '& > *': { marginBottom: 2 },
                    }
                }}>
                    <Box>
                        <Typography variant="h5" gutterBottom>VCS Providers</Typography>
                        <Typography variant="body2">
                            {DESCRIPTION}
                        </Typography>
                    </Box>
                    <Box>
                        {NewButton}
                    </Box>
                </Box>
                <SearchInput
                    sx={{ marginTop: 2, marginBottom: 2 }}
                    placeholder="search for VCS providers"
                    fullWidth
                    onChange={onSearchChange}
                    onKeyDown={onKeyDown}
                />
                <VCSProviderList
                    fragmentRef={data.node.vcsProviders}
                    search={search}
                    isRefreshing={isRefreshing}
                    loadNext={loadNext}
                    hasNext={hasNext}
                />
            </Box>}
            {search === '' && edges.length === 0 && <Box sx={{ marginTop: 4 }} display="flex" justifyContent="center">
                <Box padding={4} display="flex" flexDirection="column" justifyContent="center" alignItems="center" sx={{ maxWidth: 600 }}>
                    <Typography variant="h6">Get started with VCS providers</Typography>
                    <Typography color="textSecondary" align="center" sx={{ marginBottom: 2 }}>
                        {DESCRIPTION}
                    </Typography>
                    {NewButton}
                </Box>
            </Box>}
        </Box>
    );
}

export default OrganizationVCSProviders;
