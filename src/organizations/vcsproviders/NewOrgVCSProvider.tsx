import { Box } from '@mui/material';
import { useOutletContext } from 'react-router-dom';
import { useFragment } from 'react-relay/hooks';
import graphql from 'babel-plugin-relay/macro';
import OrganizationBreadcrumbs from '../OrganizationBreadcrumbs';
import NewVCSProvider from '../../vcsproviders/NewVCSProvider';
import { GetConnections } from './OrganizationVCSProviders';
import { NewOrgVCSProviderFragment_organization$key } from './__generated__/NewOrgVCSProviderFragment_organization.graphql';

function NewOrgVCSProvider() {
    const context = useOutletContext<NewOrgVCSProviderFragment_organization$key>();

    const organization = useFragment<NewOrgVCSProviderFragment_organization$key>(
        graphql`
            fragment NewOrgVCSProviderFragment_organization on Organization
            {
                id
                name
            }
        `, context
    );

    return (
        <Box>
            <OrganizationBreadcrumbs
                orgName={organization.name}
                childRoutes={[
                    { title: "vcs providers", path: 'vcs_providers' },
                    { title: "new", path: 'new' },
                ]}
            />
            <NewVCSProvider scope='ORGANIZATION' id={organization.id} getConnections={GetConnections} />
        </Box>
    );
}

export default NewOrgVCSProvider;
