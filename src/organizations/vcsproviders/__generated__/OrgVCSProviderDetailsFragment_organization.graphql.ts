/**
 * @generated SignedSource<<4a91e07ce4a9fac1d8da60608f38f8c1>>
 * @lightSyntaxTransform
 * @nogrep
 */

/* tslint:disable */
/* eslint-disable */
// @ts-nocheck

import { Fragment, ReaderFragment } from 'relay-runtime';
import { FragmentRefs } from "relay-runtime";
export type OrgVCSProviderDetailsFragment_organization$data = {
  readonly name: string;
  readonly " $fragmentType": "OrgVCSProviderDetailsFragment_organization";
};
export type OrgVCSProviderDetailsFragment_organization$key = {
  readonly " $data"?: OrgVCSProviderDetailsFragment_organization$data;
  readonly " $fragmentSpreads": FragmentRefs<"OrgVCSProviderDetailsFragment_organization">;
};

const node: ReaderFragment = {
  "argumentDefinitions": [],
  "kind": "Fragment",
  "metadata": null,
  "name": "OrgVCSProviderDetailsFragment_organization",
  "selections": [
    {
      "alias": null,
      "args": null,
      "kind": "ScalarField",
      "name": "name",
      "storageKey": null
    }
  ],
  "type": "Organization",
  "abstractKey": null
};

(node as any).hash = "4ac68c1149c437cabd2a7bf06793290c";

export default node;
