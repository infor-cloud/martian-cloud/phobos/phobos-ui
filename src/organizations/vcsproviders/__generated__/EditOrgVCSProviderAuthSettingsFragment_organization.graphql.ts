/**
 * @generated SignedSource<<fa645ac2eb78ce27d8b811f0e2a5ea3a>>
 * @lightSyntaxTransform
 * @nogrep
 */

/* tslint:disable */
/* eslint-disable */
// @ts-nocheck

import { Fragment, ReaderFragment } from 'relay-runtime';
import { FragmentRefs } from "relay-runtime";
export type EditOrgVCSProviderAuthSettingsFragment_organization$data = {
  readonly name: string;
  readonly " $fragmentType": "EditOrgVCSProviderAuthSettingsFragment_organization";
};
export type EditOrgVCSProviderAuthSettingsFragment_organization$key = {
  readonly " $data"?: EditOrgVCSProviderAuthSettingsFragment_organization$data;
  readonly " $fragmentSpreads": FragmentRefs<"EditOrgVCSProviderAuthSettingsFragment_organization">;
};

const node: ReaderFragment = {
  "argumentDefinitions": [],
  "kind": "Fragment",
  "metadata": null,
  "name": "EditOrgVCSProviderAuthSettingsFragment_organization",
  "selections": [
    {
      "alias": null,
      "args": null,
      "kind": "ScalarField",
      "name": "name",
      "storageKey": null
    }
  ],
  "type": "Organization",
  "abstractKey": null
};

(node as any).hash = "b946f5077f2354632e6add9de8d202de";

export default node;
