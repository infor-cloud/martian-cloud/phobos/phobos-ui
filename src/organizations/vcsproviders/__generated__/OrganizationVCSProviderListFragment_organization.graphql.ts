/**
 * @generated SignedSource<<9cf6c6f66ac9280f0176c4b1def1ff22>>
 * @lightSyntaxTransform
 * @nogrep
 */

/* tslint:disable */
/* eslint-disable */
// @ts-nocheck

import { Fragment, ReaderFragment } from 'relay-runtime';
import { FragmentRefs } from "relay-runtime";
export type OrganizationVCSProviderListFragment_organization$data = {
  readonly description: string;
  readonly name: string;
  readonly " $fragmentType": "OrganizationVCSProviderListFragment_organization";
};
export type OrganizationVCSProviderListFragment_organization$key = {
  readonly " $data"?: OrganizationVCSProviderListFragment_organization$data;
  readonly " $fragmentSpreads": FragmentRefs<"OrganizationVCSProviderListFragment_organization">;
};

const node: ReaderFragment = {
  "argumentDefinitions": [],
  "kind": "Fragment",
  "metadata": null,
  "name": "OrganizationVCSProviderListFragment_organization",
  "selections": [
    {
      "alias": null,
      "args": null,
      "kind": "ScalarField",
      "name": "name",
      "storageKey": null
    },
    {
      "alias": null,
      "args": null,
      "kind": "ScalarField",
      "name": "description",
      "storageKey": null
    }
  ],
  "type": "Organization",
  "abstractKey": null
};

(node as any).hash = "8ebeb038a461006f01a2ba287f6925a7";

export default node;
