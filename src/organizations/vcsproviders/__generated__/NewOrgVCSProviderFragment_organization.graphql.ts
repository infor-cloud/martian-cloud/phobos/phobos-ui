/**
 * @generated SignedSource<<d9553a7be6f0b2e5db62724f0826e322>>
 * @lightSyntaxTransform
 * @nogrep
 */

/* tslint:disable */
/* eslint-disable */
// @ts-nocheck

import { Fragment, ReaderFragment } from 'relay-runtime';
import { FragmentRefs } from "relay-runtime";
export type NewOrgVCSProviderFragment_organization$data = {
  readonly id: string;
  readonly name: string;
  readonly " $fragmentType": "NewOrgVCSProviderFragment_organization";
};
export type NewOrgVCSProviderFragment_organization$key = {
  readonly " $data"?: NewOrgVCSProviderFragment_organization$data;
  readonly " $fragmentSpreads": FragmentRefs<"NewOrgVCSProviderFragment_organization">;
};

const node: ReaderFragment = {
  "argumentDefinitions": [],
  "kind": "Fragment",
  "metadata": null,
  "name": "NewOrgVCSProviderFragment_organization",
  "selections": [
    {
      "alias": null,
      "args": null,
      "kind": "ScalarField",
      "name": "id",
      "storageKey": null
    },
    {
      "alias": null,
      "args": null,
      "kind": "ScalarField",
      "name": "name",
      "storageKey": null
    }
  ],
  "type": "Organization",
  "abstractKey": null
};

(node as any).hash = "4c7d542953fb774402cdbc7b7c180f2d";

export default node;
