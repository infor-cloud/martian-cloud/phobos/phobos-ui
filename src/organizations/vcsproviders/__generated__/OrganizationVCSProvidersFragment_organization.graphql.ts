/**
 * @generated SignedSource<<ff7a4fcb001e7a883d59e762e8aacc81>>
 * @lightSyntaxTransform
 * @nogrep
 */

/* tslint:disable */
/* eslint-disable */
// @ts-nocheck

import { Fragment, ReaderFragment } from 'relay-runtime';
import { FragmentRefs } from "relay-runtime";
export type OrganizationVCSProvidersFragment_organization$data = {
  readonly id: string;
  readonly name: string;
  readonly " $fragmentType": "OrganizationVCSProvidersFragment_organization";
};
export type OrganizationVCSProvidersFragment_organization$key = {
  readonly " $data"?: OrganizationVCSProvidersFragment_organization$data;
  readonly " $fragmentSpreads": FragmentRefs<"OrganizationVCSProvidersFragment_organization">;
};

const node: ReaderFragment = {
  "argumentDefinitions": [],
  "kind": "Fragment",
  "metadata": null,
  "name": "OrganizationVCSProvidersFragment_organization",
  "selections": [
    {
      "alias": null,
      "args": null,
      "kind": "ScalarField",
      "name": "id",
      "storageKey": null
    },
    {
      "alias": null,
      "args": null,
      "kind": "ScalarField",
      "name": "name",
      "storageKey": null
    }
  ],
  "type": "Organization",
  "abstractKey": null
};

(node as any).hash = "d49670e17235b527854544563ac5aecb";

export default node;
