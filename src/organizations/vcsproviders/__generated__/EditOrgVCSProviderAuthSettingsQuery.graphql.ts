/**
 * @generated SignedSource<<9b7ca4cca27cc571712c171259e4e752>>
 * @lightSyntaxTransform
 * @nogrep
 */

/* tslint:disable */
/* eslint-disable */
// @ts-nocheck

import { ConcreteRequest, Query } from 'relay-runtime';
import { FragmentRefs } from "relay-runtime";
export type EditOrgVCSProviderAuthSettingsQuery$variables = {
  id: string;
};
export type EditOrgVCSProviderAuthSettingsQuery$data = {
  readonly node: {
    readonly id?: string;
    readonly name?: string;
    readonly " $fragmentSpreads": FragmentRefs<"EditVCSProviderAuthSettingsFragment_vcsProvider">;
  } | null | undefined;
};
export type EditOrgVCSProviderAuthSettingsQuery = {
  response: EditOrgVCSProviderAuthSettingsQuery$data;
  variables: EditOrgVCSProviderAuthSettingsQuery$variables;
};

const node: ConcreteRequest = (function(){
var v0 = [
  {
    "defaultValue": null,
    "kind": "LocalArgument",
    "name": "id"
  }
],
v1 = [
  {
    "kind": "Variable",
    "name": "id",
    "variableName": "id"
  }
],
v2 = {
  "alias": null,
  "args": null,
  "kind": "ScalarField",
  "name": "id",
  "storageKey": null
},
v3 = {
  "alias": null,
  "args": null,
  "kind": "ScalarField",
  "name": "name",
  "storageKey": null
};
return {
  "fragment": {
    "argumentDefinitions": (v0/*: any*/),
    "kind": "Fragment",
    "metadata": null,
    "name": "EditOrgVCSProviderAuthSettingsQuery",
    "selections": [
      {
        "alias": null,
        "args": (v1/*: any*/),
        "concreteType": null,
        "kind": "LinkedField",
        "name": "node",
        "plural": false,
        "selections": [
          {
            "kind": "InlineFragment",
            "selections": [
              (v2/*: any*/),
              (v3/*: any*/),
              {
                "args": null,
                "kind": "FragmentSpread",
                "name": "EditVCSProviderAuthSettingsFragment_vcsProvider"
              }
            ],
            "type": "VCSProvider",
            "abstractKey": null
          }
        ],
        "storageKey": null
      }
    ],
    "type": "Query",
    "abstractKey": null
  },
  "kind": "Request",
  "operation": {
    "argumentDefinitions": (v0/*: any*/),
    "kind": "Operation",
    "name": "EditOrgVCSProviderAuthSettingsQuery",
    "selections": [
      {
        "alias": null,
        "args": (v1/*: any*/),
        "concreteType": null,
        "kind": "LinkedField",
        "name": "node",
        "plural": false,
        "selections": [
          {
            "alias": null,
            "args": null,
            "kind": "ScalarField",
            "name": "__typename",
            "storageKey": null
          },
          (v2/*: any*/),
          {
            "kind": "InlineFragment",
            "selections": [
              (v3/*: any*/),
              {
                "alias": null,
                "args": null,
                "kind": "ScalarField",
                "name": "type",
                "storageKey": null
              },
              {
                "alias": null,
                "args": null,
                "kind": "ScalarField",
                "name": "authType",
                "storageKey": null
              },
              {
                "alias": null,
                "args": null,
                "kind": "ScalarField",
                "name": "extraOAuthScopes",
                "storageKey": null
              }
            ],
            "type": "VCSProvider",
            "abstractKey": null
          }
        ],
        "storageKey": null
      }
    ]
  },
  "params": {
    "cacheID": "d8db4c23dd425ff3d02ce91db93b9c90",
    "id": null,
    "metadata": {},
    "name": "EditOrgVCSProviderAuthSettingsQuery",
    "operationKind": "query",
    "text": "query EditOrgVCSProviderAuthSettingsQuery(\n  $id: String!\n) {\n  node(id: $id) {\n    __typename\n    ... on VCSProvider {\n      id\n      name\n      ...EditVCSProviderAuthSettingsFragment_vcsProvider\n    }\n    id\n  }\n}\n\nfragment EditVCSProviderAuthSettingsFragment_vcsProvider on VCSProvider {\n  id\n  name\n  type\n  authType\n  extraOAuthScopes\n  ...VCSProviderAuthSettingsForm_vcsProvider\n}\n\nfragment VCSProviderAuthSettingsForm_vcsProvider on VCSProvider {\n  type\n  authType\n}\n"
  }
};
})();

(node as any).hash = "6c7695532db6bb758d82ab1eb584674b";

export default node;
