/**
 * @generated SignedSource<<1ab7ac72cf98b853586d24d77ada6b92>>
 * @lightSyntaxTransform
 * @nogrep
 */

/* tslint:disable */
/* eslint-disable */
// @ts-nocheck

import { Fragment, ReaderFragment } from 'relay-runtime';
import { FragmentRefs } from "relay-runtime";
export type EditOrgVCSProviderFragment_organization$data = {
  readonly id: string;
  readonly name: string;
  readonly " $fragmentType": "EditOrgVCSProviderFragment_organization";
};
export type EditOrgVCSProviderFragment_organization$key = {
  readonly " $data"?: EditOrgVCSProviderFragment_organization$data;
  readonly " $fragmentSpreads": FragmentRefs<"EditOrgVCSProviderFragment_organization">;
};

const node: ReaderFragment = {
  "argumentDefinitions": [],
  "kind": "Fragment",
  "metadata": null,
  "name": "EditOrgVCSProviderFragment_organization",
  "selections": [
    {
      "alias": null,
      "args": null,
      "kind": "ScalarField",
      "name": "id",
      "storageKey": null
    },
    {
      "alias": null,
      "args": null,
      "kind": "ScalarField",
      "name": "name",
      "storageKey": null
    }
  ],
  "type": "Organization",
  "abstractKey": null
};

(node as any).hash = "1f4a457dc76607d9520d45bc6cac2344";

export default node;
