import { Box, Typography } from '@mui/material';
import OrganizationBreadcrumbs from '../OrganizationBreadcrumbs';
import { useFragment, useLazyLoadQuery } from 'react-relay/hooks';
import { useOutletContext, useParams } from 'react-router-dom';
import graphql from 'babel-plugin-relay/macro';
import VCSProviderDetails from '../../vcsproviders/VCSProviderDetails';
import { OrgVCSProviderDetailsQuery } from './__generated__/OrgVCSProviderDetailsQuery.graphql';
import { OrgVCSProviderDetailsFragment_organization$key } from './__generated__/OrgVCSProviderDetailsFragment_organization.graphql';

function OrgVCSProviderDetails() {
    const context = useOutletContext<OrgVCSProviderDetailsFragment_organization$key>();
    const vcsProviderId = useParams().vcsProviderId as string;

    const organization = useFragment<OrgVCSProviderDetailsFragment_organization$key>(
        graphql`
            fragment OrgVCSProviderDetailsFragment_organization on Organization
            {
                name
            }
        `, context
    );

    const data = useLazyLoadQuery<OrgVCSProviderDetailsQuery>(graphql`
        query OrgVCSProviderDetailsQuery($id: String!) {
            node(id: $id) {
                ... on VCSProvider {
                    id
                    name
                    ...VCSProviderDetailsFragment_vcsProvider
                }
            }
        }
    `, { id: vcsProviderId }, { fetchPolicy: 'store-and-network' });

    if (data.node) {

        const vcsProvider = data.node as any;

        return (
            <Box>
                <OrganizationBreadcrumbs
                    orgName={organization.name}
                    childRoutes={[
                        { title: "vcs providers", path: 'vcs_providers' },
                        { title: vcsProvider.name, path: vcsProvider.id }
                    ]}
                />
                <VCSProviderDetails fragmentRef={data.node} />
            </Box>
        );
    }
    else {
        return <Box display="flex" justifyContent="center" marginTop={4}>
            <Typography color="textSecondary">VCS Provider with ID {vcsProviderId} not found</Typography>
        </Box>;
    }
}

export default OrgVCSProviderDetails;
