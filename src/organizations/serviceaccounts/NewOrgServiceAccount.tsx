import { Box } from '@mui/material';
import { useOutletContext } from 'react-router-dom';
import { useFragment } from 'react-relay/hooks';
import graphql from 'babel-plugin-relay/macro';
import OrganizationBreadcrumbs from '../OrganizationBreadcrumbs';
import NewServiceAccount from '../../serviceaccounts/NewServiceAccount';
import { GetConnections } from './OrgServiceAccounts';
import { NewOrgServiceAccountFragment_organization$key } from './__generated__/NewOrgServiceAccountFragment_organization.graphql';

function NewOrgServiceAccount() {
    const context = useOutletContext<NewOrgServiceAccountFragment_organization$key>();

    const organization = useFragment<NewOrgServiceAccountFragment_organization$key>(
        graphql`
            fragment NewOrgServiceAccountFragment_organization on Organization
            {
                id
                name
            }
        `, context
    );

    return (
        <Box>
            <OrganizationBreadcrumbs
                orgName={organization.name}
                childRoutes={[
                    { title: "service accounts", path: 'service_accounts' },
                    { title: "new", path: 'new' },
                ]}
            />
            <NewServiceAccount
                orgId={organization.id}
                getConnections={() => GetConnections(organization.id)}
            />
        </Box>
    );
}

export default NewOrgServiceAccount;
