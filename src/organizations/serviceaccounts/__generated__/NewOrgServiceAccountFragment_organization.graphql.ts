/**
 * @generated SignedSource<<e5ba70bf09f4dba40e862a40110149be>>
 * @lightSyntaxTransform
 * @nogrep
 */

/* tslint:disable */
/* eslint-disable */
// @ts-nocheck

import { Fragment, ReaderFragment } from 'relay-runtime';
import { FragmentRefs } from "relay-runtime";
export type NewOrgServiceAccountFragment_organization$data = {
  readonly id: string;
  readonly name: string;
  readonly " $fragmentType": "NewOrgServiceAccountFragment_organization";
};
export type NewOrgServiceAccountFragment_organization$key = {
  readonly " $data"?: NewOrgServiceAccountFragment_organization$data;
  readonly " $fragmentSpreads": FragmentRefs<"NewOrgServiceAccountFragment_organization">;
};

const node: ReaderFragment = {
  "argumentDefinitions": [],
  "kind": "Fragment",
  "metadata": null,
  "name": "NewOrgServiceAccountFragment_organization",
  "selections": [
    {
      "alias": null,
      "args": null,
      "kind": "ScalarField",
      "name": "id",
      "storageKey": null
    },
    {
      "alias": null,
      "args": null,
      "kind": "ScalarField",
      "name": "name",
      "storageKey": null
    }
  ],
  "type": "Organization",
  "abstractKey": null
};

(node as any).hash = "24bad37cd45aa110b12ea5c1f9f04fbf";

export default node;
