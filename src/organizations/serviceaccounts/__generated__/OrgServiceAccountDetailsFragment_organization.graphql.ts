/**
 * @generated SignedSource<<dd9552b1bdd17dcce6a98a2bb2186767>>
 * @lightSyntaxTransform
 * @nogrep
 */

/* tslint:disable */
/* eslint-disable */
// @ts-nocheck

import { Fragment, ReaderFragment } from 'relay-runtime';
import { FragmentRefs } from "relay-runtime";
export type OrgServiceAccountDetailsFragment_organization$data = {
  readonly id: string;
  readonly name: string;
  readonly " $fragmentType": "OrgServiceAccountDetailsFragment_organization";
};
export type OrgServiceAccountDetailsFragment_organization$key = {
  readonly " $data"?: OrgServiceAccountDetailsFragment_organization$data;
  readonly " $fragmentSpreads": FragmentRefs<"OrgServiceAccountDetailsFragment_organization">;
};

const node: ReaderFragment = {
  "argumentDefinitions": [],
  "kind": "Fragment",
  "metadata": null,
  "name": "OrgServiceAccountDetailsFragment_organization",
  "selections": [
    {
      "alias": null,
      "args": null,
      "kind": "ScalarField",
      "name": "id",
      "storageKey": null
    },
    {
      "alias": null,
      "args": null,
      "kind": "ScalarField",
      "name": "name",
      "storageKey": null
    }
  ],
  "type": "Organization",
  "abstractKey": null
};

(node as any).hash = "26ba650e1a9d77d1a43ab4772e1a63cc";

export default node;
