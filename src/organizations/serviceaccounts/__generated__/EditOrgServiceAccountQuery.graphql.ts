/**
 * @generated SignedSource<<292b3ecc325fe39bd6e547cfe6229ce9>>
 * @lightSyntaxTransform
 * @nogrep
 */

/* tslint:disable */
/* eslint-disable */
// @ts-nocheck

import { ConcreteRequest, Query } from 'relay-runtime';
export type ScopeType = "GLOBAL" | "ORGANIZATION" | "PROJECT" | "%future added value";
export type EditOrgServiceAccountQuery$variables = {
  id: string;
};
export type EditOrgServiceAccountQuery$data = {
  readonly node: {
    readonly name?: string;
    readonly scope?: ScopeType;
  } | null | undefined;
};
export type EditOrgServiceAccountQuery = {
  response: EditOrgServiceAccountQuery$data;
  variables: EditOrgServiceAccountQuery$variables;
};

const node: ConcreteRequest = (function(){
var v0 = [
  {
    "defaultValue": null,
    "kind": "LocalArgument",
    "name": "id"
  }
],
v1 = [
  {
    "kind": "Variable",
    "name": "id",
    "variableName": "id"
  }
],
v2 = {
  "kind": "InlineFragment",
  "selections": [
    {
      "alias": null,
      "args": null,
      "kind": "ScalarField",
      "name": "name",
      "storageKey": null
    },
    {
      "alias": null,
      "args": null,
      "kind": "ScalarField",
      "name": "scope",
      "storageKey": null
    }
  ],
  "type": "ServiceAccount",
  "abstractKey": null
};
return {
  "fragment": {
    "argumentDefinitions": (v0/*: any*/),
    "kind": "Fragment",
    "metadata": null,
    "name": "EditOrgServiceAccountQuery",
    "selections": [
      {
        "alias": null,
        "args": (v1/*: any*/),
        "concreteType": null,
        "kind": "LinkedField",
        "name": "node",
        "plural": false,
        "selections": [
          (v2/*: any*/)
        ],
        "storageKey": null
      }
    ],
    "type": "Query",
    "abstractKey": null
  },
  "kind": "Request",
  "operation": {
    "argumentDefinitions": (v0/*: any*/),
    "kind": "Operation",
    "name": "EditOrgServiceAccountQuery",
    "selections": [
      {
        "alias": null,
        "args": (v1/*: any*/),
        "concreteType": null,
        "kind": "LinkedField",
        "name": "node",
        "plural": false,
        "selections": [
          {
            "alias": null,
            "args": null,
            "kind": "ScalarField",
            "name": "__typename",
            "storageKey": null
          },
          (v2/*: any*/),
          {
            "alias": null,
            "args": null,
            "kind": "ScalarField",
            "name": "id",
            "storageKey": null
          }
        ],
        "storageKey": null
      }
    ]
  },
  "params": {
    "cacheID": "069ec00c21c170eb89dc58dac1f3018e",
    "id": null,
    "metadata": {},
    "name": "EditOrgServiceAccountQuery",
    "operationKind": "query",
    "text": "query EditOrgServiceAccountQuery(\n  $id: String!\n) {\n  node(id: $id) {\n    __typename\n    ... on ServiceAccount {\n      name\n      scope\n    }\n    id\n  }\n}\n"
  }
};
})();

(node as any).hash = "36c0f230c21aced8bec7393fcf805d08";

export default node;
