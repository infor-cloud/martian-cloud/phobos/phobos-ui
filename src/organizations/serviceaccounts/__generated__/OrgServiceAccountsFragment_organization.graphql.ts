/**
 * @generated SignedSource<<d3465a67722fccc65824304e772655d2>>
 * @lightSyntaxTransform
 * @nogrep
 */

/* tslint:disable */
/* eslint-disable */
// @ts-nocheck

import { Fragment, ReaderFragment } from 'relay-runtime';
import { FragmentRefs } from "relay-runtime";
export type OrgServiceAccountsFragment_organization$data = {
  readonly id: string;
  readonly name: string;
  readonly " $fragmentType": "OrgServiceAccountsFragment_organization";
};
export type OrgServiceAccountsFragment_organization$key = {
  readonly " $data"?: OrgServiceAccountsFragment_organization$data;
  readonly " $fragmentSpreads": FragmentRefs<"OrgServiceAccountsFragment_organization">;
};

const node: ReaderFragment = {
  "argumentDefinitions": [],
  "kind": "Fragment",
  "metadata": null,
  "name": "OrgServiceAccountsFragment_organization",
  "selections": [
    {
      "alias": null,
      "args": null,
      "kind": "ScalarField",
      "name": "id",
      "storageKey": null
    },
    {
      "alias": null,
      "args": null,
      "kind": "ScalarField",
      "name": "name",
      "storageKey": null
    }
  ],
  "type": "Organization",
  "abstractKey": null
};

(node as any).hash = "716f0beb5ae6b27dc180cbede47d9383";

export default node;
