/**
 * @generated SignedSource<<1435197c834534833067c8bf16b0e054>>
 * @lightSyntaxTransform
 * @nogrep
 */

/* tslint:disable */
/* eslint-disable */
// @ts-nocheck

import { Fragment, ReaderFragment } from 'relay-runtime';
import { FragmentRefs } from "relay-runtime";
export type EditOrgServiceAccountFragment_organization$data = {
  readonly id: string;
  readonly name: string;
  readonly " $fragmentType": "EditOrgServiceAccountFragment_organization";
};
export type EditOrgServiceAccountFragment_organization$key = {
  readonly " $data"?: EditOrgServiceAccountFragment_organization$data;
  readonly " $fragmentSpreads": FragmentRefs<"EditOrgServiceAccountFragment_organization">;
};

const node: ReaderFragment = {
  "argumentDefinitions": [],
  "kind": "Fragment",
  "metadata": null,
  "name": "EditOrgServiceAccountFragment_organization",
  "selections": [
    {
      "alias": null,
      "args": null,
      "kind": "ScalarField",
      "name": "id",
      "storageKey": null
    },
    {
      "alias": null,
      "args": null,
      "kind": "ScalarField",
      "name": "name",
      "storageKey": null
    }
  ],
  "type": "Organization",
  "abstractKey": null
};

(node as any).hash = "69b7a0aeab203fd2a4d1c8a8d3ef8b7c";

export default node;
