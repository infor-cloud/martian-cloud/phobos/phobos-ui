import { Box, Typography } from '@mui/material';
import { useOutletContext, useParams } from 'react-router-dom';
import OrganizationBreadcrumbs from '../OrganizationBreadcrumbs';
import graphql from 'babel-plugin-relay/macro';
import { useFragment, useLazyLoadQuery } from "react-relay/hooks";
import EditServiceAccount from '../../serviceaccounts/EditServiceAccount';
import { EditOrgServiceAccountFragment_organization$key } from './__generated__/EditOrgServiceAccountFragment_organization.graphql';
import { EditOrgServiceAccountQuery } from './__generated__/EditOrgServiceAccountQuery.graphql';

function EditOrgServiceAccount() {
    const serviceAccountId = useParams().serviceAccountId as string;
    const context = useOutletContext<EditOrgServiceAccountFragment_organization$key>();

    const organization = useFragment<EditOrgServiceAccountFragment_organization$key>(
        graphql`
            fragment EditOrgServiceAccountFragment_organization on Organization
            {
                id
                name
            }
        `, context
    );

    const queryData = useLazyLoadQuery<EditOrgServiceAccountQuery>(graphql`
        query EditOrgServiceAccountQuery($id: String!) {
            node(id: $id) {
                ... on ServiceAccount {
                    name
                    scope
                }
            }
        }
    `, { id: serviceAccountId });

    const serviceAccount = queryData.node as any;

    return queryData.node ? (
        <Box>
            <OrganizationBreadcrumbs
                orgName={organization.name}
                childRoutes={[
                    { title: "service accounts", path: 'service_accounts' },
                    { title: serviceAccount.name, path: serviceAccount.id },
                    { title: "edit", path: 'edit' },
                ]}
            />
            <EditServiceAccount />
        </Box>
    ) : <Box display="flex" justifyContent="center" marginTop={4}>
        <Typography color="textSecondary">Service account not found</Typography>
    </Box>;
}

export default EditOrgServiceAccount;
