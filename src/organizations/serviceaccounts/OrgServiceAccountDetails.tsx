import { Box, Typography } from '@mui/material';
import OrganizationBreadcrumbs from '../OrganizationBreadcrumbs';
import { useFragment, useLazyLoadQuery } from 'react-relay/hooks';
import { useOutletContext, useParams } from 'react-router-dom';
import graphql from 'babel-plugin-relay/macro';
import ServiceAccountDetails from '../../serviceaccounts/ServiceAccountDetails';
import { GetConnections } from './OrgServiceAccounts';
import { OrgServiceAccountDetailsQuery } from './__generated__/OrgServiceAccountDetailsQuery.graphql';
import { OrgServiceAccountDetailsFragment_organization$key } from './__generated__/OrgServiceAccountDetailsFragment_organization.graphql';

function OrgServiceAccountDetails() {
    const context = useOutletContext<OrgServiceAccountDetailsFragment_organization$key>();
    const serviceAccountId = useParams().serviceAccountId as string;

    const organization = useFragment<OrgServiceAccountDetailsFragment_organization$key>(
        graphql`
            fragment OrgServiceAccountDetailsFragment_organization on Organization
            {
                id
                name
            }
        `, context
    );

    const data = useLazyLoadQuery<OrgServiceAccountDetailsQuery>(graphql`
        query OrgServiceAccountDetailsQuery($id: String!) {
            node(id: $id) {
                ... on ServiceAccount {
                    id
                    name
                }
            }
        }
    `, { id: serviceAccountId }, { fetchPolicy: 'store-and-network' });

    if (data.node) {

        const serviceAccount = data.node as any;

        return (
            <Box>
                <OrganizationBreadcrumbs
                    orgName={organization.name}
                    childRoutes={[
                        { title: "service accounts", path: 'service_accounts' },
                        { title: serviceAccount.name, path: serviceAccount.id }
                    ]}
                />
                <ServiceAccountDetails getConnections={() => GetConnections(organization.id)} />
            </Box>
        );
    }
    else {
        return <Box display="flex" justifyContent="center" marginTop={4}>
            <Typography color="textSecondary">Service account with ID {serviceAccountId} not found</Typography>
        </Box>;
    }
}

export default OrgServiceAccountDetails;
