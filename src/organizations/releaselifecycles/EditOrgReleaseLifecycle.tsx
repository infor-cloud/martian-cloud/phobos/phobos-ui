import { Box, Typography } from '@mui/material';
import { useOutletContext, useParams } from 'react-router-dom';
import OrganizationBreadcrumbs from '../OrganizationBreadcrumbs';
import graphql from 'babel-plugin-relay/macro';
import { useFragment, useLazyLoadQuery } from "react-relay/hooks";
import EditReleaseLifecycle from '../../releaselifecycles/EditReleaseLifecycle';
import { EditOrgReleaseLifecycleFragment_organization$key } from './__generated__/EditOrgReleaseLifecycleFragment_organization.graphql';
import { EditOrgReleaseLifecycleQuery } from './__generated__/EditOrgReleaseLifecycleQuery.graphql';

function EditOrgReleaseLifecycle() {
    const releaseLifecycleId = useParams().releaseLifecycleId as string;
    const context = useOutletContext<EditOrgReleaseLifecycleFragment_organization$key>();

    const organization = useFragment<EditOrgReleaseLifecycleFragment_organization$key>(
        graphql`
            fragment EditOrgReleaseLifecycleFragment_organization on Organization
            {
                id
                name
            }
        `, context
    );

    const queryData = useLazyLoadQuery<EditOrgReleaseLifecycleQuery>(graphql`
        query EditOrgReleaseLifecycleQuery($id: String!) {
            node(id: $id) {
                ... on ReleaseLifecycle {
                    name
                }
            }
        }
    `, { id: releaseLifecycleId });

    const releaseLifecycle = queryData.node as any;

    return queryData.node ? (
        <Box>
            <OrganizationBreadcrumbs
                orgName={organization.name}
                childRoutes={[
                    { title: "release lifecycles", path: 'release_lifecycles' },
                    { title: releaseLifecycle.name, path: releaseLifecycleId },
                    { title: "edit", path: 'edit' },
                ]}
            />
            <EditReleaseLifecycle orgId={organization.id} />
        </Box>
    ) : <Box display="flex" justifyContent="center" mt={4}>
        <Typography color="textSecondary">Release lifecycle not found</Typography>
    </Box>;
}

export default EditOrgReleaseLifecycle;
