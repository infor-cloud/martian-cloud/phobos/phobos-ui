import { Box, Typography } from '@mui/material';
import OrganizationBreadcrumbs from '../OrganizationBreadcrumbs';
import { useFragment, useLazyLoadQuery } from 'react-relay/hooks';
import { useOutletContext, useParams } from 'react-router-dom';
import graphql from 'babel-plugin-relay/macro';
import ReleaseLifecycleDetails from '../../releaselifecycles/ReleaseLifecycleDetails';
import { GetConnections } from './OrgReleaseLifecycles';
import { OrgReleaseLifecycleDetailsQuery } from './__generated__/OrgReleaseLifecycleDetailsQuery.graphql';
import { OrgReleaseLifecycleDetailsFragment_organization$key } from './__generated__/OrgReleaseLifecycleDetailsFragment_organization.graphql';

function OrgReleaseLifecycleDetails() {
    const context = useOutletContext<OrgReleaseLifecycleDetailsFragment_organization$key>();
    const releaseLifecycleId = useParams().releaseLifecycleId as string;

    const organization = useFragment<OrgReleaseLifecycleDetailsFragment_organization$key>(
        graphql`
            fragment OrgReleaseLifecycleDetailsFragment_organization on Organization
            {
                id
                name
            }
        `, context
    );

    const data = useLazyLoadQuery<OrgReleaseLifecycleDetailsQuery>(graphql`
        query OrgReleaseLifecycleDetailsQuery($id: String!) {
            node(id: $id) {
                ... on ReleaseLifecycle {
                    id
                    name
                }
            }
        }
    `, { id: releaseLifecycleId }, { fetchPolicy: 'store-and-network' });

    if (data.node) {

        const releaseLifecycle = data.node as any;

        return (
            <Box>
                <OrganizationBreadcrumbs
                    orgName={organization.name}
                    childRoutes={[
                        { title: "release lifecycles", path: 'release_lifecycles' },
                        { title: releaseLifecycle.name, path: releaseLifecycle.id }
                    ]}
                />
                <ReleaseLifecycleDetails getConnections={() => GetConnections(organization.id)} />
            </Box>
        );
    }
    else {
        return <Box display="flex" justifyContent="center" marginTop={4}>
            <Typography color="textSecondary">Release lifecycle with ID {releaseLifecycleId} not found</Typography>
        </Box>;
    }
}

export default OrgReleaseLifecycleDetails;
