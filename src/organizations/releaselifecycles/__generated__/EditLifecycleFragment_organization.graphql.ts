/**
 * @generated SignedSource<<d9325bf80efc0934cff477fa068679d1>>
 * @lightSyntaxTransform
 * @nogrep
 */

/* tslint:disable */
/* eslint-disable */
// @ts-nocheck

import { Fragment, ReaderFragment } from 'relay-runtime';
import { FragmentRefs } from "relay-runtime";
export type EditLifecycleFragment_organization$data = {
  readonly id: string;
  readonly name: string;
  readonly " $fragmentType": "EditLifecycleFragment_organization";
};
export type EditLifecycleFragment_organization$key = {
  readonly " $data"?: EditLifecycleFragment_organization$data;
  readonly " $fragmentSpreads": FragmentRefs<"EditLifecycleFragment_organization">;
};

const node: ReaderFragment = {
  "argumentDefinitions": [],
  "kind": "Fragment",
  "metadata": null,
  "name": "EditLifecycleFragment_organization",
  "selections": [
    {
      "alias": null,
      "args": null,
      "kind": "ScalarField",
      "name": "id",
      "storageKey": null
    },
    {
      "alias": null,
      "args": null,
      "kind": "ScalarField",
      "name": "name",
      "storageKey": null
    }
  ],
  "type": "Organization",
  "abstractKey": null
};

(node as any).hash = "6451f21c1bcb2c535b6de58616ac136a";

export default node;
