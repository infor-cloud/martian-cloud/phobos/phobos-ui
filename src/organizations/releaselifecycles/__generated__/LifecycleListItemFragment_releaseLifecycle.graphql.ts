/**
 * @generated SignedSource<<dbba4b1448f5e997928a0e45f0308e4a>>
 * @lightSyntaxTransform
 * @nogrep
 */

/* tslint:disable */
/* eslint-disable */
// @ts-nocheck

import { Fragment, ReaderFragment } from 'relay-runtime';
import { FragmentRefs } from "relay-runtime";
export type LifecycleListItemFragment_releaseLifecycle$data = {
  readonly id: string;
  readonly metadata: {
    readonly updatedAt: any;
  };
  readonly name: string;
  readonly " $fragmentType": "LifecycleListItemFragment_releaseLifecycle";
};
export type LifecycleListItemFragment_releaseLifecycle$key = {
  readonly " $data"?: LifecycleListItemFragment_releaseLifecycle$data;
  readonly " $fragmentSpreads": FragmentRefs<"LifecycleListItemFragment_releaseLifecycle">;
};

const node: ReaderFragment = {
  "argumentDefinitions": [],
  "kind": "Fragment",
  "metadata": null,
  "name": "LifecycleListItemFragment_releaseLifecycle",
  "selections": [
    {
      "alias": null,
      "args": null,
      "concreteType": "ResourceMetadata",
      "kind": "LinkedField",
      "name": "metadata",
      "plural": false,
      "selections": [
        {
          "alias": null,
          "args": null,
          "kind": "ScalarField",
          "name": "updatedAt",
          "storageKey": null
        }
      ],
      "storageKey": null
    },
    {
      "alias": null,
      "args": null,
      "kind": "ScalarField",
      "name": "id",
      "storageKey": null
    },
    {
      "alias": null,
      "args": null,
      "kind": "ScalarField",
      "name": "name",
      "storageKey": null
    }
  ],
  "type": "ReleaseLifecycle",
  "abstractKey": null
};

(node as any).hash = "be77d13e603e4aca16109caede47207f";

export default node;
