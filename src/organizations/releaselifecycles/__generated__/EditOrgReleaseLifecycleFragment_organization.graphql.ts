/**
 * @generated SignedSource<<fa88f9f8e976916fcca1d2d97d95fef2>>
 * @lightSyntaxTransform
 * @nogrep
 */

/* tslint:disable */
/* eslint-disable */
// @ts-nocheck

import { Fragment, ReaderFragment } from 'relay-runtime';
import { FragmentRefs } from "relay-runtime";
export type EditOrgReleaseLifecycleFragment_organization$data = {
  readonly id: string;
  readonly name: string;
  readonly " $fragmentType": "EditOrgReleaseLifecycleFragment_organization";
};
export type EditOrgReleaseLifecycleFragment_organization$key = {
  readonly " $data"?: EditOrgReleaseLifecycleFragment_organization$data;
  readonly " $fragmentSpreads": FragmentRefs<"EditOrgReleaseLifecycleFragment_organization">;
};

const node: ReaderFragment = {
  "argumentDefinitions": [],
  "kind": "Fragment",
  "metadata": null,
  "name": "EditOrgReleaseLifecycleFragment_organization",
  "selections": [
    {
      "alias": null,
      "args": null,
      "kind": "ScalarField",
      "name": "id",
      "storageKey": null
    },
    {
      "alias": null,
      "args": null,
      "kind": "ScalarField",
      "name": "name",
      "storageKey": null
    }
  ],
  "type": "Organization",
  "abstractKey": null
};

(node as any).hash = "a303307028c250d16849645c412f1015";

export default node;
