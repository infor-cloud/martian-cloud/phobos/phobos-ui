/**
 * @generated SignedSource<<29ed17190032d8613d27c6bfcfbb3689>>
 * @lightSyntaxTransform
 * @nogrep
 */

/* tslint:disable */
/* eslint-disable */
// @ts-nocheck

import { Fragment, ReaderFragment } from 'relay-runtime';
import { FragmentRefs } from "relay-runtime";
export type NewLifecycleFragment_organization$data = {
  readonly id: string;
  readonly name: string;
  readonly " $fragmentType": "NewLifecycleFragment_organization";
};
export type NewLifecycleFragment_organization$key = {
  readonly " $data"?: NewLifecycleFragment_organization$data;
  readonly " $fragmentSpreads": FragmentRefs<"NewLifecycleFragment_organization">;
};

const node: ReaderFragment = {
  "argumentDefinitions": [],
  "kind": "Fragment",
  "metadata": null,
  "name": "NewLifecycleFragment_organization",
  "selections": [
    {
      "alias": null,
      "args": null,
      "kind": "ScalarField",
      "name": "id",
      "storageKey": null
    },
    {
      "alias": null,
      "args": null,
      "kind": "ScalarField",
      "name": "name",
      "storageKey": null
    }
  ],
  "type": "Organization",
  "abstractKey": null
};

(node as any).hash = "89b37e97f065ce0c9ab53c1ed6e01557";

export default node;
