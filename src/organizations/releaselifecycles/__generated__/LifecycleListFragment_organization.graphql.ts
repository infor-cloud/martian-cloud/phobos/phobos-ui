/**
 * @generated SignedSource<<22bd0b743b903b71d940fa2c25dc3d3e>>
 * @lightSyntaxTransform
 * @nogrep
 */

/* tslint:disable */
/* eslint-disable */
// @ts-nocheck

import { Fragment, ReaderFragment } from 'relay-runtime';
import { FragmentRefs } from "relay-runtime";
export type LifecycleListFragment_organization$data = {
  readonly description: string;
  readonly name: string;
  readonly " $fragmentType": "LifecycleListFragment_organization";
};
export type LifecycleListFragment_organization$key = {
  readonly " $data"?: LifecycleListFragment_organization$data;
  readonly " $fragmentSpreads": FragmentRefs<"LifecycleListFragment_organization">;
};

const node: ReaderFragment = {
  "argumentDefinitions": [],
  "kind": "Fragment",
  "metadata": null,
  "name": "LifecycleListFragment_organization",
  "selections": [
    {
      "alias": null,
      "args": null,
      "kind": "ScalarField",
      "name": "name",
      "storageKey": null
    },
    {
      "alias": null,
      "args": null,
      "kind": "ScalarField",
      "name": "description",
      "storageKey": null
    }
  ],
  "type": "Organization",
  "abstractKey": null
};

(node as any).hash = "66cfd874d6ecfbe6f20bf6e09803362b";

export default node;
