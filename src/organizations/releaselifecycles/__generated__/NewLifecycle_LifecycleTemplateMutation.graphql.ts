/**
 * @generated SignedSource<<29f2f83f0415a2b1b3a5818b761bf9b6>>
 * @lightSyntaxTransform
 * @nogrep
 */

/* tslint:disable */
/* eslint-disable */
// @ts-nocheck

import { ConcreteRequest, Mutation } from 'relay-runtime';
export type ProblemType = "BAD_REQUEST" | "CONFLICT" | "FORBIDDEN" | "NOT_FOUND" | "%future added value";
export type CreateLifecycleTemplateInput = {
  clientMutationId?: string | null;
  hclData: string;
  organizationId: string;
};
export type NewLifecycle_LifecycleTemplateMutation$variables = {
  input: CreateLifecycleTemplateInput;
};
export type NewLifecycle_LifecycleTemplateMutation$data = {
  readonly createLifecycleTemplate: {
    readonly lifecycleTemplate: {
      readonly hclData: string;
      readonly id: string;
    } | null;
    readonly problems: ReadonlyArray<{
      readonly field: ReadonlyArray<string> | null;
      readonly message: string;
      readonly type: ProblemType;
    }>;
  };
};
export type NewLifecycle_LifecycleTemplateMutation = {
  response: NewLifecycle_LifecycleTemplateMutation$data;
  variables: NewLifecycle_LifecycleTemplateMutation$variables;
};

const node: ConcreteRequest = (function(){
var v0 = [
  {
    "defaultValue": null,
    "kind": "LocalArgument",
    "name": "input"
  }
],
v1 = [
  {
    "alias": null,
    "args": [
      {
        "kind": "Variable",
        "name": "input",
        "variableName": "input"
      }
    ],
    "concreteType": "CreateLifecycleTemplatePayload",
    "kind": "LinkedField",
    "name": "createLifecycleTemplate",
    "plural": false,
    "selections": [
      {
        "alias": null,
        "args": null,
        "concreteType": "LifecycleTemplate",
        "kind": "LinkedField",
        "name": "lifecycleTemplate",
        "plural": false,
        "selections": [
          {
            "alias": null,
            "args": null,
            "kind": "ScalarField",
            "name": "id",
            "storageKey": null
          },
          {
            "alias": null,
            "args": null,
            "kind": "ScalarField",
            "name": "hclData",
            "storageKey": null
          }
        ],
        "storageKey": null
      },
      {
        "alias": null,
        "args": null,
        "concreteType": "Problem",
        "kind": "LinkedField",
        "name": "problems",
        "plural": true,
        "selections": [
          {
            "alias": null,
            "args": null,
            "kind": "ScalarField",
            "name": "message",
            "storageKey": null
          },
          {
            "alias": null,
            "args": null,
            "kind": "ScalarField",
            "name": "field",
            "storageKey": null
          },
          {
            "alias": null,
            "args": null,
            "kind": "ScalarField",
            "name": "type",
            "storageKey": null
          }
        ],
        "storageKey": null
      }
    ],
    "storageKey": null
  }
];
return {
  "fragment": {
    "argumentDefinitions": (v0/*: any*/),
    "kind": "Fragment",
    "metadata": null,
    "name": "NewLifecycle_LifecycleTemplateMutation",
    "selections": (v1/*: any*/),
    "type": "Mutation",
    "abstractKey": null
  },
  "kind": "Request",
  "operation": {
    "argumentDefinitions": (v0/*: any*/),
    "kind": "Operation",
    "name": "NewLifecycle_LifecycleTemplateMutation",
    "selections": (v1/*: any*/)
  },
  "params": {
    "cacheID": "6ae1970c7985958444e17fcff8d3d7fa",
    "id": null,
    "metadata": {},
    "name": "NewLifecycle_LifecycleTemplateMutation",
    "operationKind": "mutation",
    "text": "mutation NewLifecycle_LifecycleTemplateMutation(\n  $input: CreateLifecycleTemplateInput!\n) {\n  createLifecycleTemplate(input: $input) {\n    lifecycleTemplate {\n      id\n      hclData\n    }\n    problems {\n      message\n      field\n      type\n    }\n  }\n}\n"
  }
};
})();

(node as any).hash = "f06f558d8fc5d460e095dba5a74a2624";

export default node;
