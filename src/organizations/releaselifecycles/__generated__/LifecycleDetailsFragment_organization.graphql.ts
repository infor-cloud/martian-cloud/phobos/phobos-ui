/**
 * @generated SignedSource<<257422e82c88c6feda0efb8ad0747422>>
 * @lightSyntaxTransform
 * @nogrep
 */

/* tslint:disable */
/* eslint-disable */
// @ts-nocheck

import { Fragment, ReaderFragment } from 'relay-runtime';
import { FragmentRefs } from "relay-runtime";
export type LifecycleDetailsFragment_organization$data = {
  readonly id: string;
  readonly name: string;
  readonly " $fragmentType": "LifecycleDetailsFragment_organization";
};
export type LifecycleDetailsFragment_organization$key = {
  readonly " $data"?: LifecycleDetailsFragment_organization$data;
  readonly " $fragmentSpreads": FragmentRefs<"LifecycleDetailsFragment_organization">;
};

const node: ReaderFragment = {
  "argumentDefinitions": [],
  "kind": "Fragment",
  "metadata": null,
  "name": "LifecycleDetailsFragment_organization",
  "selections": [
    {
      "alias": null,
      "args": null,
      "kind": "ScalarField",
      "name": "id",
      "storageKey": null
    },
    {
      "alias": null,
      "args": null,
      "kind": "ScalarField",
      "name": "name",
      "storageKey": null
    }
  ],
  "type": "Organization",
  "abstractKey": null
};

(node as any).hash = "d2ca9bd7c0e23df807f8df360deb9367";

export default node;
