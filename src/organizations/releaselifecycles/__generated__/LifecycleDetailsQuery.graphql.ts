/**
 * @generated SignedSource<<6e6a65b944547bddfbdaf0cb7db89787>>
 * @lightSyntaxTransform
 * @nogrep
 */

/* tslint:disable */
/* eslint-disable */
// @ts-nocheck

import { ConcreteRequest, Query } from 'relay-runtime';
export type LifecycleDetailsQuery$variables = {
  id: string;
};
export type LifecycleDetailsQuery$data = {
  readonly node: {
    readonly id?: string;
    readonly lifecycleTemplate?: {
      readonly hclData: string;
      readonly id: string;
    };
    readonly name?: string;
  } | null;
};
export type LifecycleDetailsQuery = {
  response: LifecycleDetailsQuery$data;
  variables: LifecycleDetailsQuery$variables;
};

const node: ConcreteRequest = (function(){
var v0 = [
  {
    "defaultValue": null,
    "kind": "LocalArgument",
    "name": "id"
  }
],
v1 = [
  {
    "kind": "Variable",
    "name": "id",
    "variableName": "id"
  }
],
v2 = {
  "alias": null,
  "args": null,
  "kind": "ScalarField",
  "name": "id",
  "storageKey": null
},
v3 = {
  "alias": null,
  "args": null,
  "kind": "ScalarField",
  "name": "name",
  "storageKey": null
},
v4 = {
  "alias": null,
  "args": null,
  "concreteType": "LifecycleTemplate",
  "kind": "LinkedField",
  "name": "lifecycleTemplate",
  "plural": false,
  "selections": [
    (v2/*: any*/),
    {
      "alias": null,
      "args": null,
      "kind": "ScalarField",
      "name": "hclData",
      "storageKey": null
    }
  ],
  "storageKey": null
};
return {
  "fragment": {
    "argumentDefinitions": (v0/*: any*/),
    "kind": "Fragment",
    "metadata": null,
    "name": "LifecycleDetailsQuery",
    "selections": [
      {
        "alias": null,
        "args": (v1/*: any*/),
        "concreteType": null,
        "kind": "LinkedField",
        "name": "node",
        "plural": false,
        "selections": [
          {
            "kind": "InlineFragment",
            "selections": [
              (v2/*: any*/),
              (v3/*: any*/),
              (v4/*: any*/)
            ],
            "type": "ReleaseLifecycle",
            "abstractKey": null
          }
        ],
        "storageKey": null
      }
    ],
    "type": "Query",
    "abstractKey": null
  },
  "kind": "Request",
  "operation": {
    "argumentDefinitions": (v0/*: any*/),
    "kind": "Operation",
    "name": "LifecycleDetailsQuery",
    "selections": [
      {
        "alias": null,
        "args": (v1/*: any*/),
        "concreteType": null,
        "kind": "LinkedField",
        "name": "node",
        "plural": false,
        "selections": [
          {
            "alias": null,
            "args": null,
            "kind": "ScalarField",
            "name": "__typename",
            "storageKey": null
          },
          (v2/*: any*/),
          {
            "kind": "InlineFragment",
            "selections": [
              (v3/*: any*/),
              (v4/*: any*/)
            ],
            "type": "ReleaseLifecycle",
            "abstractKey": null
          }
        ],
        "storageKey": null
      }
    ]
  },
  "params": {
    "cacheID": "8f05a16d8c36fa2419449ec340e483f2",
    "id": null,
    "metadata": {},
    "name": "LifecycleDetailsQuery",
    "operationKind": "query",
    "text": "query LifecycleDetailsQuery(\n  $id: String!\n) {\n  node(id: $id) {\n    __typename\n    ... on ReleaseLifecycle {\n      id\n      name\n      lifecycleTemplate {\n        id\n        hclData\n      }\n    }\n    id\n  }\n}\n"
  }
};
})();

(node as any).hash = "c933119f0e65b2645c104bbb8932a0a7";

export default node;
