/**
 * @generated SignedSource<<aa05a8968489a52465888a2ba069f3cf>>
 * @lightSyntaxTransform
 * @nogrep
 */

/* tslint:disable */
/* eslint-disable */
// @ts-nocheck

import { Fragment, ReaderFragment } from 'relay-runtime';
import { FragmentRefs } from "relay-runtime";
export type OrgReleaseLifecyclesFragment_organization$data = {
  readonly name: string;
  readonly " $fragmentType": "OrgReleaseLifecyclesFragment_organization";
};
export type OrgReleaseLifecyclesFragment_organization$key = {
  readonly " $data"?: OrgReleaseLifecyclesFragment_organization$data;
  readonly " $fragmentSpreads": FragmentRefs<"OrgReleaseLifecyclesFragment_organization">;
};

const node: ReaderFragment = {
  "argumentDefinitions": [],
  "kind": "Fragment",
  "metadata": null,
  "name": "OrgReleaseLifecyclesFragment_organization",
  "selections": [
    {
      "alias": null,
      "args": null,
      "kind": "ScalarField",
      "name": "name",
      "storageKey": null
    }
  ],
  "type": "Organization",
  "abstractKey": null
};

(node as any).hash = "db3a90648c88d9c13d7b3d6f9b764b54";

export default node;
