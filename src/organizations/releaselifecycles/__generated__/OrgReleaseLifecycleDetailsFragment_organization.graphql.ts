/**
 * @generated SignedSource<<b4a8d0bf1fc5e4ff4ed169cb2f297668>>
 * @lightSyntaxTransform
 * @nogrep
 */

/* tslint:disable */
/* eslint-disable */
// @ts-nocheck

import { Fragment, ReaderFragment } from 'relay-runtime';
import { FragmentRefs } from "relay-runtime";
export type OrgReleaseLifecycleDetailsFragment_organization$data = {
  readonly id: string;
  readonly name: string;
  readonly " $fragmentType": "OrgReleaseLifecycleDetailsFragment_organization";
};
export type OrgReleaseLifecycleDetailsFragment_organization$key = {
  readonly " $data"?: OrgReleaseLifecycleDetailsFragment_organization$data;
  readonly " $fragmentSpreads": FragmentRefs<"OrgReleaseLifecycleDetailsFragment_organization">;
};

const node: ReaderFragment = {
  "argumentDefinitions": [],
  "kind": "Fragment",
  "metadata": null,
  "name": "OrgReleaseLifecycleDetailsFragment_organization",
  "selections": [
    {
      "alias": null,
      "args": null,
      "kind": "ScalarField",
      "name": "id",
      "storageKey": null
    },
    {
      "alias": null,
      "args": null,
      "kind": "ScalarField",
      "name": "name",
      "storageKey": null
    }
  ],
  "type": "Organization",
  "abstractKey": null
};

(node as any).hash = "165dbae78cf491a4569ab894c418d11a";

export default node;
