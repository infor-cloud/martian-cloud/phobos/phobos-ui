/**
 * @generated SignedSource<<1a8ded7e52f5a21e21aa5cbd358415f3>>
 * @lightSyntaxTransform
 * @nogrep
 */

/* tslint:disable */
/* eslint-disable */
// @ts-nocheck

import { ConcreteRequest, Mutation } from 'relay-runtime';
import { FragmentRefs } from "relay-runtime";
export type ProblemType = "BAD_REQUEST" | "CONFLICT" | "FORBIDDEN" | "NOT_FOUND" | "%future added value";
export type CreateReleaseLifecycleInput = {
  clientMutationId?: string | null;
  lifecycleTemplateId: string;
  name: string;
  organizationId: string;
};
export type NewLifecycle_ReleaseLifecycle_Mutation$variables = {
  connections: ReadonlyArray<string>;
  input: CreateReleaseLifecycleInput;
};
export type NewLifecycle_ReleaseLifecycle_Mutation$data = {
  readonly createReleaseLifecycle: {
    readonly problems: ReadonlyArray<{
      readonly field: ReadonlyArray<string> | null;
      readonly message: string;
      readonly type: ProblemType;
    }>;
    readonly releaseLifecycle: {
      readonly createdBy: string;
      readonly id: string;
      readonly name: string;
      readonly " $fragmentSpreads": FragmentRefs<"LifecycleListItemFragment_releaseLifecycle">;
    } | null;
  };
};
export type NewLifecycle_ReleaseLifecycle_Mutation = {
  response: NewLifecycle_ReleaseLifecycle_Mutation$data;
  variables: NewLifecycle_ReleaseLifecycle_Mutation$variables;
};

const node: ConcreteRequest = (function(){
var v0 = {
  "defaultValue": null,
  "kind": "LocalArgument",
  "name": "connections"
},
v1 = {
  "defaultValue": null,
  "kind": "LocalArgument",
  "name": "input"
},
v2 = [
  {
    "kind": "Variable",
    "name": "input",
    "variableName": "input"
  }
],
v3 = {
  "alias": null,
  "args": null,
  "kind": "ScalarField",
  "name": "id",
  "storageKey": null
},
v4 = {
  "alias": null,
  "args": null,
  "kind": "ScalarField",
  "name": "name",
  "storageKey": null
},
v5 = {
  "alias": null,
  "args": null,
  "kind": "ScalarField",
  "name": "createdBy",
  "storageKey": null
},
v6 = {
  "alias": null,
  "args": null,
  "concreteType": "Problem",
  "kind": "LinkedField",
  "name": "problems",
  "plural": true,
  "selections": [
    {
      "alias": null,
      "args": null,
      "kind": "ScalarField",
      "name": "message",
      "storageKey": null
    },
    {
      "alias": null,
      "args": null,
      "kind": "ScalarField",
      "name": "field",
      "storageKey": null
    },
    {
      "alias": null,
      "args": null,
      "kind": "ScalarField",
      "name": "type",
      "storageKey": null
    }
  ],
  "storageKey": null
};
return {
  "fragment": {
    "argumentDefinitions": [
      (v0/*: any*/),
      (v1/*: any*/)
    ],
    "kind": "Fragment",
    "metadata": null,
    "name": "NewLifecycle_ReleaseLifecycle_Mutation",
    "selections": [
      {
        "alias": null,
        "args": (v2/*: any*/),
        "concreteType": "CreateReleaseLifecyclePayload",
        "kind": "LinkedField",
        "name": "createReleaseLifecycle",
        "plural": false,
        "selections": [
          {
            "alias": null,
            "args": null,
            "concreteType": "ReleaseLifecycle",
            "kind": "LinkedField",
            "name": "releaseLifecycle",
            "plural": false,
            "selections": [
              (v3/*: any*/),
              (v4/*: any*/),
              (v5/*: any*/),
              {
                "args": null,
                "kind": "FragmentSpread",
                "name": "LifecycleListItemFragment_releaseLifecycle"
              }
            ],
            "storageKey": null
          },
          (v6/*: any*/)
        ],
        "storageKey": null
      }
    ],
    "type": "Mutation",
    "abstractKey": null
  },
  "kind": "Request",
  "operation": {
    "argumentDefinitions": [
      (v1/*: any*/),
      (v0/*: any*/)
    ],
    "kind": "Operation",
    "name": "NewLifecycle_ReleaseLifecycle_Mutation",
    "selections": [
      {
        "alias": null,
        "args": (v2/*: any*/),
        "concreteType": "CreateReleaseLifecyclePayload",
        "kind": "LinkedField",
        "name": "createReleaseLifecycle",
        "plural": false,
        "selections": [
          {
            "alias": null,
            "args": null,
            "concreteType": "ReleaseLifecycle",
            "kind": "LinkedField",
            "name": "releaseLifecycle",
            "plural": false,
            "selections": [
              (v3/*: any*/),
              (v4/*: any*/),
              (v5/*: any*/),
              {
                "alias": null,
                "args": null,
                "concreteType": "ResourceMetadata",
                "kind": "LinkedField",
                "name": "metadata",
                "plural": false,
                "selections": [
                  {
                    "alias": null,
                    "args": null,
                    "kind": "ScalarField",
                    "name": "updatedAt",
                    "storageKey": null
                  }
                ],
                "storageKey": null
              }
            ],
            "storageKey": null
          },
          {
            "alias": null,
            "args": null,
            "filters": null,
            "handle": "prependNode",
            "key": "",
            "kind": "LinkedHandle",
            "name": "releaseLifecycle",
            "handleArgs": [
              {
                "kind": "Variable",
                "name": "connections",
                "variableName": "connections"
              },
              {
                "kind": "Literal",
                "name": "edgeTypeName",
                "value": "Lifecycle_ReleaseLifecycleEdge"
              }
            ]
          },
          (v6/*: any*/)
        ],
        "storageKey": null
      }
    ]
  },
  "params": {
    "cacheID": "809aa0bf1272d177450f942398b18f5b",
    "id": null,
    "metadata": {},
    "name": "NewLifecycle_ReleaseLifecycle_Mutation",
    "operationKind": "mutation",
    "text": "mutation NewLifecycle_ReleaseLifecycle_Mutation(\n  $input: CreateReleaseLifecycleInput!\n) {\n  createReleaseLifecycle(input: $input) {\n    releaseLifecycle {\n      id\n      name\n      createdBy\n      ...LifecycleListItemFragment_releaseLifecycle\n    }\n    problems {\n      message\n      field\n      type\n    }\n  }\n}\n\nfragment LifecycleListItemFragment_releaseLifecycle on ReleaseLifecycle {\n  metadata {\n    updatedAt\n  }\n  id\n  name\n}\n"
  }
};
})();

(node as any).hash = "e6ff28f5b35ceb7219ff4fce55c514f3";

export default node;
