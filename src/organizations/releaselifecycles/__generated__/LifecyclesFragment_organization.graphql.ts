/**
 * @generated SignedSource<<8f86399b96bcb548f530175eaa122951>>
 * @lightSyntaxTransform
 * @nogrep
 */

/* tslint:disable */
/* eslint-disable */
// @ts-nocheck

import { Fragment, ReaderFragment } from 'relay-runtime';
import { FragmentRefs } from "relay-runtime";
export type LifecyclesFragment_organization$data = {
  readonly name: string;
  readonly " $fragmentSpreads": FragmentRefs<"LifecycleListFragment_organization">;
  readonly " $fragmentType": "LifecyclesFragment_organization";
};
export type LifecyclesFragment_organization$key = {
  readonly " $data"?: LifecyclesFragment_organization$data;
  readonly " $fragmentSpreads": FragmentRefs<"LifecyclesFragment_organization">;
};

const node: ReaderFragment = {
  "argumentDefinitions": [],
  "kind": "Fragment",
  "metadata": null,
  "name": "LifecyclesFragment_organization",
  "selections": [
    {
      "alias": null,
      "args": null,
      "kind": "ScalarField",
      "name": "name",
      "storageKey": null
    },
    {
      "args": null,
      "kind": "FragmentSpread",
      "name": "LifecycleListFragment_organization"
    }
  ],
  "type": "Organization",
  "abstractKey": null
};

(node as any).hash = "106833e4a8fbcf3b50a22092dddba715";

export default node;
