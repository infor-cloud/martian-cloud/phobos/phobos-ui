/**
 * @generated SignedSource<<978654c25f6da25bb340d55876d7c25e>>
 * @lightSyntaxTransform
 * @nogrep
 */

/* tslint:disable */
/* eslint-disable */
// @ts-nocheck

import { ConcreteRequest, Query } from 'relay-runtime';
export type EditOrgReleaseLifecycleQuery$variables = {
  id: string;
};
export type EditOrgReleaseLifecycleQuery$data = {
  readonly node: {
    readonly name?: string;
  } | null | undefined;
};
export type EditOrgReleaseLifecycleQuery = {
  response: EditOrgReleaseLifecycleQuery$data;
  variables: EditOrgReleaseLifecycleQuery$variables;
};

const node: ConcreteRequest = (function(){
var v0 = [
  {
    "defaultValue": null,
    "kind": "LocalArgument",
    "name": "id"
  }
],
v1 = [
  {
    "kind": "Variable",
    "name": "id",
    "variableName": "id"
  }
],
v2 = {
  "kind": "InlineFragment",
  "selections": [
    {
      "alias": null,
      "args": null,
      "kind": "ScalarField",
      "name": "name",
      "storageKey": null
    }
  ],
  "type": "ReleaseLifecycle",
  "abstractKey": null
};
return {
  "fragment": {
    "argumentDefinitions": (v0/*: any*/),
    "kind": "Fragment",
    "metadata": null,
    "name": "EditOrgReleaseLifecycleQuery",
    "selections": [
      {
        "alias": null,
        "args": (v1/*: any*/),
        "concreteType": null,
        "kind": "LinkedField",
        "name": "node",
        "plural": false,
        "selections": [
          (v2/*: any*/)
        ],
        "storageKey": null
      }
    ],
    "type": "Query",
    "abstractKey": null
  },
  "kind": "Request",
  "operation": {
    "argumentDefinitions": (v0/*: any*/),
    "kind": "Operation",
    "name": "EditOrgReleaseLifecycleQuery",
    "selections": [
      {
        "alias": null,
        "args": (v1/*: any*/),
        "concreteType": null,
        "kind": "LinkedField",
        "name": "node",
        "plural": false,
        "selections": [
          {
            "alias": null,
            "args": null,
            "kind": "ScalarField",
            "name": "__typename",
            "storageKey": null
          },
          (v2/*: any*/),
          {
            "alias": null,
            "args": null,
            "kind": "ScalarField",
            "name": "id",
            "storageKey": null
          }
        ],
        "storageKey": null
      }
    ]
  },
  "params": {
    "cacheID": "e11383b36855d11fa91b5fc0295d1f43",
    "id": null,
    "metadata": {},
    "name": "EditOrgReleaseLifecycleQuery",
    "operationKind": "query",
    "text": "query EditOrgReleaseLifecycleQuery(\n  $id: String!\n) {\n  node(id: $id) {\n    __typename\n    ... on ReleaseLifecycle {\n      name\n    }\n    id\n  }\n}\n"
  }
};
})();

(node as any).hash = "ceb1b002ea71411345c97665c2896e26";

export default node;
