/**
 * @generated SignedSource<<e4c864c691aa2ac8b0db8d8513579575>>
 * @lightSyntaxTransform
 * @nogrep
 */

/* tslint:disable */
/* eslint-disable */
// @ts-nocheck

import { ConcreteRequest, Mutation } from 'relay-runtime';
export type ProblemType = "BAD_REQUEST" | "CONFLICT" | "FORBIDDEN" | "NOT_FOUND" | "%future added value";
export type UpdateReleaseLifecycleInput = {
  clientMutationId?: string | null;
  id: string;
  lifecycleTemplateId: string;
  metadata?: ResourceMetadataInput | null;
};
export type ResourceMetadataInput = {
  version: string;
};
export type EditLifecycle_ReleaseLifecycleMutation$variables = {
  input: UpdateReleaseLifecycleInput;
};
export type EditLifecycle_ReleaseLifecycleMutation$data = {
  readonly updateReleaseLifecycle: {
    readonly problems: ReadonlyArray<{
      readonly field: ReadonlyArray<string> | null;
      readonly message: string;
      readonly type: ProblemType;
    }>;
    readonly releaseLifecycle: {
      readonly createdBy: string;
      readonly id: string;
      readonly name: string;
    } | null;
  };
};
export type EditLifecycle_ReleaseLifecycleMutation = {
  response: EditLifecycle_ReleaseLifecycleMutation$data;
  variables: EditLifecycle_ReleaseLifecycleMutation$variables;
};

const node: ConcreteRequest = (function(){
var v0 = [
  {
    "defaultValue": null,
    "kind": "LocalArgument",
    "name": "input"
  }
],
v1 = [
  {
    "alias": null,
    "args": [
      {
        "kind": "Variable",
        "name": "input",
        "variableName": "input"
      }
    ],
    "concreteType": "UpdateReleaseLifecyclePayload",
    "kind": "LinkedField",
    "name": "updateReleaseLifecycle",
    "plural": false,
    "selections": [
      {
        "alias": null,
        "args": null,
        "concreteType": "ReleaseLifecycle",
        "kind": "LinkedField",
        "name": "releaseLifecycle",
        "plural": false,
        "selections": [
          {
            "alias": null,
            "args": null,
            "kind": "ScalarField",
            "name": "id",
            "storageKey": null
          },
          {
            "alias": null,
            "args": null,
            "kind": "ScalarField",
            "name": "name",
            "storageKey": null
          },
          {
            "alias": null,
            "args": null,
            "kind": "ScalarField",
            "name": "createdBy",
            "storageKey": null
          }
        ],
        "storageKey": null
      },
      {
        "alias": null,
        "args": null,
        "concreteType": "Problem",
        "kind": "LinkedField",
        "name": "problems",
        "plural": true,
        "selections": [
          {
            "alias": null,
            "args": null,
            "kind": "ScalarField",
            "name": "message",
            "storageKey": null
          },
          {
            "alias": null,
            "args": null,
            "kind": "ScalarField",
            "name": "field",
            "storageKey": null
          },
          {
            "alias": null,
            "args": null,
            "kind": "ScalarField",
            "name": "type",
            "storageKey": null
          }
        ],
        "storageKey": null
      }
    ],
    "storageKey": null
  }
];
return {
  "fragment": {
    "argumentDefinitions": (v0/*: any*/),
    "kind": "Fragment",
    "metadata": null,
    "name": "EditLifecycle_ReleaseLifecycleMutation",
    "selections": (v1/*: any*/),
    "type": "Mutation",
    "abstractKey": null
  },
  "kind": "Request",
  "operation": {
    "argumentDefinitions": (v0/*: any*/),
    "kind": "Operation",
    "name": "EditLifecycle_ReleaseLifecycleMutation",
    "selections": (v1/*: any*/)
  },
  "params": {
    "cacheID": "83a084926ec4729e5b673e83a9f5b1d3",
    "id": null,
    "metadata": {},
    "name": "EditLifecycle_ReleaseLifecycleMutation",
    "operationKind": "mutation",
    "text": "mutation EditLifecycle_ReleaseLifecycleMutation(\n  $input: UpdateReleaseLifecycleInput!\n) {\n  updateReleaseLifecycle(input: $input) {\n    releaseLifecycle {\n      id\n      name\n      createdBy\n    }\n    problems {\n      message\n      field\n      type\n    }\n  }\n}\n"
  }
};
})();

(node as any).hash = "a39106e0c0cd161afe11d188dd4c1232";

export default node;
