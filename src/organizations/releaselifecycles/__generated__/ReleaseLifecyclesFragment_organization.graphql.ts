/**
 * @generated SignedSource<<4e00f5a3396d88d702491c1ffc24f41d>>
 * @lightSyntaxTransform
 * @nogrep
 */

/* tslint:disable */
/* eslint-disable */
// @ts-nocheck

import { Fragment, ReaderFragment } from 'relay-runtime';
import { FragmentRefs } from "relay-runtime";
export type ReleaseLifecyclesFragment_organization$data = {
  readonly name: string;
  readonly " $fragmentSpreads": FragmentRefs<"ReleaseLifecycleListFragment_organization">;
  readonly " $fragmentType": "ReleaseLifecyclesFragment_organization";
};
export type ReleaseLifecyclesFragment_organization$key = {
  readonly " $data"?: ReleaseLifecyclesFragment_organization$data;
  readonly " $fragmentSpreads": FragmentRefs<"ReleaseLifecyclesFragment_organization">;
};

const node: ReaderFragment = {
  "argumentDefinitions": [],
  "kind": "Fragment",
  "metadata": null,
  "name": "ReleaseLifecyclesFragment_organization",
  "selections": [
    {
      "alias": null,
      "args": null,
      "kind": "ScalarField",
      "name": "name",
      "storageKey": null
    },
    {
      "args": null,
      "kind": "FragmentSpread",
      "name": "ReleaseLifecycleListFragment_organization"
    }
  ],
  "type": "Organization",
  "abstractKey": null
};

(node as any).hash = "b6cc271d2f098b7d3a90ea4550417625";

export default node;
