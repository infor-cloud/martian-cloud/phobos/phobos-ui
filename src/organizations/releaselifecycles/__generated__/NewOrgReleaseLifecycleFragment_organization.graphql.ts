/**
 * @generated SignedSource<<7bd0bcdbb7ce26f70a59a2cf49de7d3f>>
 * @lightSyntaxTransform
 * @nogrep
 */

/* tslint:disable */
/* eslint-disable */
// @ts-nocheck

import { Fragment, ReaderFragment } from 'relay-runtime';
import { FragmentRefs } from "relay-runtime";
export type NewOrgReleaseLifecycleFragment_organization$data = {
  readonly id: string;
  readonly name: string;
  readonly " $fragmentType": "NewOrgReleaseLifecycleFragment_organization";
};
export type NewOrgReleaseLifecycleFragment_organization$key = {
  readonly " $data"?: NewOrgReleaseLifecycleFragment_organization$data;
  readonly " $fragmentSpreads": FragmentRefs<"NewOrgReleaseLifecycleFragment_organization">;
};

const node: ReaderFragment = {
  "argumentDefinitions": [],
  "kind": "Fragment",
  "metadata": null,
  "name": "NewOrgReleaseLifecycleFragment_organization",
  "selections": [
    {
      "alias": null,
      "args": null,
      "kind": "ScalarField",
      "name": "id",
      "storageKey": null
    },
    {
      "alias": null,
      "args": null,
      "kind": "ScalarField",
      "name": "name",
      "storageKey": null
    }
  ],
  "type": "Organization",
  "abstractKey": null
};

(node as any).hash = "0dbccba0da71b71638d665d2cacb4f8f";

export default node;
