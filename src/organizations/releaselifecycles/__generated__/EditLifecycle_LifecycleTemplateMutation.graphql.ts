/**
 * @generated SignedSource<<d641107522313fa1e5012c3349a9b731>>
 * @lightSyntaxTransform
 * @nogrep
 */

/* tslint:disable */
/* eslint-disable */
// @ts-nocheck

import { ConcreteRequest, Mutation } from 'relay-runtime';
export type ProblemType = "BAD_REQUEST" | "CONFLICT" | "FORBIDDEN" | "NOT_FOUND" | "%future added value";
export type CreateLifecycleTemplateInput = {
  clientMutationId?: string | null;
  hclData: string;
  organizationId: string;
};
export type EditLifecycle_LifecycleTemplateMutation$variables = {
  input: CreateLifecycleTemplateInput;
};
export type EditLifecycle_LifecycleTemplateMutation$data = {
  readonly createLifecycleTemplate: {
    readonly lifecycleTemplate: {
      readonly hclData: string;
      readonly id: string;
    } | null;
    readonly problems: ReadonlyArray<{
      readonly field: ReadonlyArray<string> | null;
      readonly message: string;
      readonly type: ProblemType;
    }>;
  };
};
export type EditLifecycle_LifecycleTemplateMutation = {
  response: EditLifecycle_LifecycleTemplateMutation$data;
  variables: EditLifecycle_LifecycleTemplateMutation$variables;
};

const node: ConcreteRequest = (function(){
var v0 = [
  {
    "defaultValue": null,
    "kind": "LocalArgument",
    "name": "input"
  }
],
v1 = [
  {
    "alias": null,
    "args": [
      {
        "kind": "Variable",
        "name": "input",
        "variableName": "input"
      }
    ],
    "concreteType": "CreateLifecycleTemplatePayload",
    "kind": "LinkedField",
    "name": "createLifecycleTemplate",
    "plural": false,
    "selections": [
      {
        "alias": null,
        "args": null,
        "concreteType": "LifecycleTemplate",
        "kind": "LinkedField",
        "name": "lifecycleTemplate",
        "plural": false,
        "selections": [
          {
            "alias": null,
            "args": null,
            "kind": "ScalarField",
            "name": "id",
            "storageKey": null
          },
          {
            "alias": null,
            "args": null,
            "kind": "ScalarField",
            "name": "hclData",
            "storageKey": null
          }
        ],
        "storageKey": null
      },
      {
        "alias": null,
        "args": null,
        "concreteType": "Problem",
        "kind": "LinkedField",
        "name": "problems",
        "plural": true,
        "selections": [
          {
            "alias": null,
            "args": null,
            "kind": "ScalarField",
            "name": "message",
            "storageKey": null
          },
          {
            "alias": null,
            "args": null,
            "kind": "ScalarField",
            "name": "field",
            "storageKey": null
          },
          {
            "alias": null,
            "args": null,
            "kind": "ScalarField",
            "name": "type",
            "storageKey": null
          }
        ],
        "storageKey": null
      }
    ],
    "storageKey": null
  }
];
return {
  "fragment": {
    "argumentDefinitions": (v0/*: any*/),
    "kind": "Fragment",
    "metadata": null,
    "name": "EditLifecycle_LifecycleTemplateMutation",
    "selections": (v1/*: any*/),
    "type": "Mutation",
    "abstractKey": null
  },
  "kind": "Request",
  "operation": {
    "argumentDefinitions": (v0/*: any*/),
    "kind": "Operation",
    "name": "EditLifecycle_LifecycleTemplateMutation",
    "selections": (v1/*: any*/)
  },
  "params": {
    "cacheID": "11ffa6d17011f5cc74f1172b3e9acb2a",
    "id": null,
    "metadata": {},
    "name": "EditLifecycle_LifecycleTemplateMutation",
    "operationKind": "mutation",
    "text": "mutation EditLifecycle_LifecycleTemplateMutation(\n  $input: CreateLifecycleTemplateInput!\n) {\n  createLifecycleTemplate(input: $input) {\n    lifecycleTemplate {\n      id\n      hclData\n    }\n    problems {\n      message\n      field\n      type\n    }\n  }\n}\n"
  }
};
})();

(node as any).hash = "dd78f7e4b828343c0f59816e2dec88eb";

export default node;
