import { Box } from '@mui/material';
import { useOutletContext } from 'react-router-dom';
import { useFragment } from 'react-relay/hooks';
import graphql from 'babel-plugin-relay/macro';
import OrganizationBreadcrumbs from '../OrganizationBreadcrumbs';
import NewReleaseLifecycle from '../../releaselifecycles/NewReleaseLifecycle';
import { GetConnections } from './OrgReleaseLifecycles';
import { NewOrgReleaseLifecycleFragment_organization$key } from './__generated__/NewOrgReleaseLifecycleFragment_organization.graphql';

function NewOrgReleaseLifecycle() {
    const context = useOutletContext<NewOrgReleaseLifecycleFragment_organization$key>();

    const organization = useFragment<NewOrgReleaseLifecycleFragment_organization$key>(
        graphql`
            fragment NewOrgReleaseLifecycleFragment_organization on Organization
            {
                id
                name
            }
        `, context
    );

    return (
        <Box>
            <OrganizationBreadcrumbs
                orgName={organization.name}
                childRoutes={[
                    { title: "release lifecycles", path: 'release_lifecycles' },
                    { title: "new", path: 'new' },
                ]}
            />
            <NewReleaseLifecycle
                orgId={organization.id}
                getConnections={GetConnections}
            />
        </Box>
    );
}

export default NewOrgReleaseLifecycle;
