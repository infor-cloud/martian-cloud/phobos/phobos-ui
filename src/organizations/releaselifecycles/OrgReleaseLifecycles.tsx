import React, { useMemo, useState } from 'react';
import { Box, Button, Typography, useTheme } from '@mui/material';
import graphql from 'babel-plugin-relay/macro';
import SearchInput from '../../common/SearchInput';
import throttle from 'lodash.throttle';
import { useFragment } from "react-relay/hooks";
import { Link as RouterLink, useOutletContext } from 'react-router-dom';
import { ConnectionHandler, fetchQuery, useLazyLoadQuery, usePaginationFragment, useRelayEnvironment } from 'react-relay/hooks';
import OrganizationBreadcrumbs from '../OrganizationBreadcrumbs';
import ReleaseLifecycleList from '../../releaselifecycles/ReleaseLifecycleList';
import { OrgReleaseLifecyclesQuery } from './__generated__/OrgReleaseLifecyclesQuery.graphql';
import { OrgReleaseLifecyclesPaginationQuery } from './__generated__/OrgReleaseLifecyclesPaginationQuery.graphql';
import { ReleaseLifecyclesFragment_organization$key } from './__generated__/ReleaseLifecyclesFragment_organization.graphql';
import { OrgReleaseLifecyclesFragment_releaseLifecycles$key } from './__generated__/OrgReleaseLifecyclesFragment_releaseLifecycles.graphql';

const INITIAL_ITEM_COUNT = 100;

const DESCRIPTION = 'Release lifecycles define how a release will be propagated between environments.';

const NewButton =
    <Button
        sx={{ minWidth: 200 }}
        component={RouterLink}
        variant="outlined"
        to="new"
    >
        New Release Lifecycle
    </Button>;

const query = graphql`
    query OrgReleaseLifecyclesQuery($orgId: String!, $first: Int, $last: Int, $after: String, $before: String, $search: String) {
        ...OrgReleaseLifecyclesFragment_releaseLifecycles
    }
`;

export function GetConnections(orgId: string): [string] {
    const connectionId = ConnectionHandler.getConnectionID(
        orgId,
        'OrgReleaseLifecycles_releaseLifecycles',
    );
    return [connectionId];
}

function OrgReleaseLifecycles() {
    const [search, setSearch] = useState<string | undefined>('');
    const theme = useTheme();
    const [isRefreshing, setIsRefreshing] = useState(false);
    const context = useOutletContext<ReleaseLifecyclesFragment_organization$key>();

    const organization = useFragment(graphql`
        fragment OrgReleaseLifecyclesFragment_organization on Organization
            {
                name
            }
    `, context);

    const orgPrn = `prn:organization:${organization.name}`;

    const queryData = useLazyLoadQuery<OrgReleaseLifecyclesQuery>(query, { orgId: orgPrn, first: INITIAL_ITEM_COUNT }, { fetchPolicy: 'store-and-network' });

    const { data, loadNext, hasNext, refetch } = usePaginationFragment<OrgReleaseLifecyclesPaginationQuery, OrgReleaseLifecyclesFragment_releaseLifecycles$key>(
        graphql`
      fragment OrgReleaseLifecyclesFragment_releaseLifecycles on Query
        @refetchable(queryName: "OrgReleaseLifecyclesPaginationQuery") {
            node(id: $orgId) {
                ...on Organization {
                    releaseLifecycles(
                        after: $after
                        before: $before
                        first: $first
                        last: $last
                        search: $search
                    ) @connection(key: "OrgReleaseLifecycles_releaseLifecycles") {
                        totalCount
                        edges {
                            node {
                                id
                            }
                        }
                        ...ReleaseLifecycleListFragment_releaseLifecycles
                    }
                }
            }
        }
    `, queryData);

    const environment = useRelayEnvironment();

    const fetch = useMemo(
        () =>
            throttle(
                (input?: string) => {
                    setIsRefreshing(true);

                    const normalizedInput = input?.trim();

                    fetchQuery(environment, query, { first: INITIAL_ITEM_COUNT, orgId: orgPrn, search: normalizedInput })
                        .subscribe({
                            complete: () => {
                                setIsRefreshing(false);
                                setSearch(input);

                                // *After* the query has been fetched, we call
                                // refetch again to re-render with the updated data.
                                // At this point the data for the query should
                                // be cached, so we use the 'store-only'
                                // fetchPolicy to avoid suspending.
                                refetch({
                                    first: INITIAL_ITEM_COUNT,
                                    search: normalizedInput
                                }, {
                                    fetchPolicy: 'store-only'
                                });
                            },
                            error: () => {
                                setIsRefreshing(false);
                            }
                        });
                },
                2000,
                { leading: false, trailing: true }
            ),
        [environment, refetch, organization.name]
    );

    const onSearchChange = (event: React.ChangeEvent<HTMLInputElement>) => {
        fetch(event.target.value.toLowerCase().trim());
    };

    const onKeyDown = (event: React.KeyboardEvent<HTMLInputElement>) => {
        // Only handle enter key type
        if (event.key === 'Enter') {
            fetch.flush();
        }
    };

    const edges = data?.node?.releaseLifecycles?.edges ?? [];

    return (
        <Box>
            <OrganizationBreadcrumbs
                orgName={organization.name}
                childRoutes={[
                    { title: "release lifecycles", path: 'release_lifecycles' }
                ]}
            />
            <Box>{(search !== '' || edges.length !== 0) && (data.node?.releaseLifecycles) && <Box>
                <Box>
                    <Box sx={{
                        display: 'flex',
                        flexDirection: 'row',
                        justifyContent: 'space-between',
                        [theme.breakpoints.down('md')]: {
                            flexDirection: 'column',
                            alignItems: 'flex-start',
                            '& > *': { marginBottom: 2 },
                        }
                    }}>
                        <Box>
                            <Typography variant="h5" gutterBottom>Release Lifecycles</Typography>
                            <Typography variant="body2">
                                {DESCRIPTION}
                            </Typography>
                        </Box>
                        <Box>
                            {NewButton}
                        </Box>
                    </Box>
                    <SearchInput
                        sx={{ marginTop: 2, marginBottom: 2 }}
                        placeholder="search for release lifecycles"
                        fullWidth
                        onChange={onSearchChange}
                        onKeyDown={onKeyDown}
                    />
                </Box>
                <ReleaseLifecycleList
                    fragmentRef={data.node.releaseLifecycles}
                    search={search}
                    isRefreshing={isRefreshing}
                    loadNext={loadNext}
                    hasNext={hasNext}
                />
            </Box>}
                {search === '' && edges.length === 0 && <Box sx={{ marginTop: 4 }} display="flex" justifyContent="center">
                    <Box padding={4} display="flex" flexDirection="column" justifyContent="center" alignItems="center" sx={{ maxWidth: 600 }}>
                        <Typography variant="h6">Get started with release lifecycles</Typography>
                        <Typography color="textSecondary" align="center" sx={{ marginBottom: 2 }}>
                            {DESCRIPTION}
                        </Typography>
                        {NewButton}
                    </Box>
                </Box>}
            </Box>
        </Box>
    );
}

export default OrgReleaseLifecycles;
