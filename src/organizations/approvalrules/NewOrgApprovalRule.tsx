import Box from '@mui/material/Box';
import graphql from 'babel-plugin-relay/macro';
import { useFragment } from "react-relay/hooks";
import { useOutletContext } from 'react-router-dom';
import OrganizationBreadcrumbs from '../OrganizationBreadcrumbs';
import { GetConnections } from './OrgApprovalRules';
import NewApprovalRule from '../../approvalrules/NewApprovalRule';
import { NewOrgApprovalRuleFragment_organization$key } from './__generated__/NewOrgApprovalRuleFragment_organization.graphql';

function NewOrgApprovalRule() {
    const context = useOutletContext<NewOrgApprovalRuleFragment_organization$key>();

    const organization = useFragment<NewOrgApprovalRuleFragment_organization$key>(
        graphql`
            fragment NewOrgApprovalRuleFragment_organization on Organization
            {
                id
                name
            }
        `, context
    );

    return (
        <Box>
            <OrganizationBreadcrumbs
                orgName={organization.name}
                childRoutes={[
                    { title: "approval rules", path: 'approval_rules' },
                    { title: "new", path: 'new' }
                ]}
            />
            <NewApprovalRule
                orgId={organization.id}
                getConnections={() => GetConnections(organization.id)}
            />
        </Box>
    );
}

export default NewOrgApprovalRule;
