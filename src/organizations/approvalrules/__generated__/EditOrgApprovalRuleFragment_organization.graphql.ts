/**
 * @generated SignedSource<<8033f96e07d90410ad55d5ed8b0acb9d>>
 * @lightSyntaxTransform
 * @nogrep
 */

/* tslint:disable */
/* eslint-disable */
// @ts-nocheck

import { Fragment, ReaderFragment } from 'relay-runtime';
import { FragmentRefs } from "relay-runtime";
export type EditOrgApprovalRuleFragment_organization$data = {
  readonly id: string;
  readonly name: string;
  readonly " $fragmentType": "EditOrgApprovalRuleFragment_organization";
};
export type EditOrgApprovalRuleFragment_organization$key = {
  readonly " $data"?: EditOrgApprovalRuleFragment_organization$data;
  readonly " $fragmentSpreads": FragmentRefs<"EditOrgApprovalRuleFragment_organization">;
};

const node: ReaderFragment = {
  "argumentDefinitions": [],
  "kind": "Fragment",
  "metadata": null,
  "name": "EditOrgApprovalRuleFragment_organization",
  "selections": [
    {
      "alias": null,
      "args": null,
      "kind": "ScalarField",
      "name": "id",
      "storageKey": null
    },
    {
      "alias": null,
      "args": null,
      "kind": "ScalarField",
      "name": "name",
      "storageKey": null
    }
  ],
  "type": "Organization",
  "abstractKey": null
};

(node as any).hash = "384252526391a32a9f51856a50be0eb0";

export default node;
