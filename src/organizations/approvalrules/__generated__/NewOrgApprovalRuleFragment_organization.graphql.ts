/**
 * @generated SignedSource<<2ffa82e6becf3045b30c5c5dbd9bd095>>
 * @lightSyntaxTransform
 * @nogrep
 */

/* tslint:disable */
/* eslint-disable */
// @ts-nocheck

import { Fragment, ReaderFragment } from 'relay-runtime';
import { FragmentRefs } from "relay-runtime";
export type NewOrgApprovalRuleFragment_organization$data = {
  readonly id: string;
  readonly name: string;
  readonly " $fragmentType": "NewOrgApprovalRuleFragment_organization";
};
export type NewOrgApprovalRuleFragment_organization$key = {
  readonly " $data"?: NewOrgApprovalRuleFragment_organization$data;
  readonly " $fragmentSpreads": FragmentRefs<"NewOrgApprovalRuleFragment_organization">;
};

const node: ReaderFragment = {
  "argumentDefinitions": [],
  "kind": "Fragment",
  "metadata": null,
  "name": "NewOrgApprovalRuleFragment_organization",
  "selections": [
    {
      "alias": null,
      "args": null,
      "kind": "ScalarField",
      "name": "id",
      "storageKey": null
    },
    {
      "alias": null,
      "args": null,
      "kind": "ScalarField",
      "name": "name",
      "storageKey": null
    }
  ],
  "type": "Organization",
  "abstractKey": null
};

(node as any).hash = "fdcc6a3741649a3efe7fb0c00d9c59ea";

export default node;
