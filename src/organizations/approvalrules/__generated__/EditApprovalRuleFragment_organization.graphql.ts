/**
 * @generated SignedSource<<c426677cbf63c29a5412907fadfef911>>
 * @lightSyntaxTransform
 * @nogrep
 */

/* tslint:disable */
/* eslint-disable */
// @ts-nocheck

import { Fragment, ReaderFragment } from 'relay-runtime';
import { FragmentRefs } from "relay-runtime";
export type EditApprovalRuleFragment_organization$data = {
  readonly name: string;
  readonly " $fragmentType": "EditApprovalRuleFragment_organization";
};
export type EditApprovalRuleFragment_organization$key = {
  readonly " $data"?: EditApprovalRuleFragment_organization$data;
  readonly " $fragmentSpreads": FragmentRefs<"EditApprovalRuleFragment_organization">;
};

const node: ReaderFragment = {
  "argumentDefinitions": [],
  "kind": "Fragment",
  "metadata": null,
  "name": "EditApprovalRuleFragment_organization",
  "selections": [
    {
      "alias": null,
      "args": null,
      "kind": "ScalarField",
      "name": "name",
      "storageKey": null
    }
  ],
  "type": "Organization",
  "abstractKey": null
};

(node as any).hash = "74284fbb2a1e5b8c55bf5437b1d4e491";

export default node;
