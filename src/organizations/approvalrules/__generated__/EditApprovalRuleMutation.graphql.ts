/**
 * @generated SignedSource<<a730b2bb83ca79ac5775c2c8aab07790>>
 * @lightSyntaxTransform
 * @nogrep
 */

/* tslint:disable */
/* eslint-disable */
// @ts-nocheck

import { ConcreteRequest, Mutation } from 'relay-runtime';
import { FragmentRefs } from "relay-runtime";
export type ProblemType = "BAD_REQUEST" | "CONFLICT" | "FORBIDDEN" | "NOT_FOUND" | "%future added value";
export type UpdateApprovalRuleInput = {
  clientMutationId?: string | null | undefined;
  description?: string | null | undefined;
  id: string;
  metadata?: ResourceMetadataInput | null | undefined;
  serviceAccounts?: ReadonlyArray<string> | null | undefined;
  teams?: ReadonlyArray<string> | null | undefined;
  users?: ReadonlyArray<string> | null | undefined;
};
export type ResourceMetadataInput = {
  version: string;
};
export type EditApprovalRuleMutation$variables = {
  input: UpdateApprovalRuleInput;
};
export type EditApprovalRuleMutation$data = {
  readonly updateApprovalRule: {
    readonly approvalRule: {
      readonly " $fragmentSpreads": FragmentRefs<"ApprovalRuleListItemFragment_approvalRule">;
    } | null | undefined;
    readonly problems: ReadonlyArray<{
      readonly field: ReadonlyArray<string> | null | undefined;
      readonly message: string;
      readonly type: ProblemType;
    }>;
  };
};
export type EditApprovalRuleMutation = {
  response: EditApprovalRuleMutation$data;
  variables: EditApprovalRuleMutation$variables;
};

const node: ConcreteRequest = (function(){
var v0 = [
  {
    "defaultValue": null,
    "kind": "LocalArgument",
    "name": "input"
  }
],
v1 = [
  {
    "kind": "Variable",
    "name": "input",
    "variableName": "input"
  }
],
v2 = {
  "alias": null,
  "args": null,
  "concreteType": "Problem",
  "kind": "LinkedField",
  "name": "problems",
  "plural": true,
  "selections": [
    {
      "alias": null,
      "args": null,
      "kind": "ScalarField",
      "name": "message",
      "storageKey": null
    },
    {
      "alias": null,
      "args": null,
      "kind": "ScalarField",
      "name": "field",
      "storageKey": null
    },
    {
      "alias": null,
      "args": null,
      "kind": "ScalarField",
      "name": "type",
      "storageKey": null
    }
  ],
  "storageKey": null
},
v3 = {
  "alias": null,
  "args": null,
  "kind": "ScalarField",
  "name": "id",
  "storageKey": null
},
v4 = {
  "alias": null,
  "args": null,
  "kind": "ScalarField",
  "name": "name",
  "storageKey": null
},
v5 = [
  (v3/*: any*/),
  (v4/*: any*/)
];
return {
  "fragment": {
    "argumentDefinitions": (v0/*: any*/),
    "kind": "Fragment",
    "metadata": null,
    "name": "EditApprovalRuleMutation",
    "selections": [
      {
        "alias": null,
        "args": (v1/*: any*/),
        "concreteType": "UpdateApprovalRulePayload",
        "kind": "LinkedField",
        "name": "updateApprovalRule",
        "plural": false,
        "selections": [
          {
            "alias": null,
            "args": null,
            "concreteType": "ApprovalRule",
            "kind": "LinkedField",
            "name": "approvalRule",
            "plural": false,
            "selections": [
              {
                "args": null,
                "kind": "FragmentSpread",
                "name": "ApprovalRuleListItemFragment_approvalRule"
              }
            ],
            "storageKey": null
          },
          (v2/*: any*/)
        ],
        "storageKey": null
      }
    ],
    "type": "Mutation",
    "abstractKey": null
  },
  "kind": "Request",
  "operation": {
    "argumentDefinitions": (v0/*: any*/),
    "kind": "Operation",
    "name": "EditApprovalRuleMutation",
    "selections": [
      {
        "alias": null,
        "args": (v1/*: any*/),
        "concreteType": "UpdateApprovalRulePayload",
        "kind": "LinkedField",
        "name": "updateApprovalRule",
        "plural": false,
        "selections": [
          {
            "alias": null,
            "args": null,
            "concreteType": "ApprovalRule",
            "kind": "LinkedField",
            "name": "approvalRule",
            "plural": false,
            "selections": [
              (v3/*: any*/),
              (v4/*: any*/),
              {
                "alias": null,
                "args": null,
                "kind": "ScalarField",
                "name": "description",
                "storageKey": null
              },
              {
                "alias": null,
                "args": null,
                "kind": "ScalarField",
                "name": "approvalsRequired",
                "storageKey": null
              },
              {
                "alias": null,
                "args": null,
                "concreteType": "User",
                "kind": "LinkedField",
                "name": "users",
                "plural": true,
                "selections": [
                  (v3/*: any*/),
                  {
                    "alias": null,
                    "args": null,
                    "kind": "ScalarField",
                    "name": "username",
                    "storageKey": null
                  },
                  {
                    "alias": null,
                    "args": null,
                    "kind": "ScalarField",
                    "name": "email",
                    "storageKey": null
                  }
                ],
                "storageKey": null
              },
              {
                "alias": null,
                "args": null,
                "concreteType": "ServiceAccount",
                "kind": "LinkedField",
                "name": "serviceAccounts",
                "plural": true,
                "selections": (v5/*: any*/),
                "storageKey": null
              },
              {
                "alias": null,
                "args": null,
                "concreteType": "Team",
                "kind": "LinkedField",
                "name": "teams",
                "plural": true,
                "selections": (v5/*: any*/),
                "storageKey": null
              },
              {
                "alias": null,
                "args": null,
                "concreteType": "ResourceMetadata",
                "kind": "LinkedField",
                "name": "metadata",
                "plural": false,
                "selections": [
                  {
                    "alias": null,
                    "args": null,
                    "kind": "ScalarField",
                    "name": "prn",
                    "storageKey": null
                  }
                ],
                "storageKey": null
              }
            ],
            "storageKey": null
          },
          (v2/*: any*/)
        ],
        "storageKey": null
      }
    ]
  },
  "params": {
    "cacheID": "a527560dd32159eb45b5da7ebc0e3cb6",
    "id": null,
    "metadata": {},
    "name": "EditApprovalRuleMutation",
    "operationKind": "mutation",
    "text": "mutation EditApprovalRuleMutation(\n  $input: UpdateApprovalRuleInput!\n) {\n  updateApprovalRule(input: $input) {\n    approvalRule {\n      ...ApprovalRuleListItemFragment_approvalRule\n      id\n    }\n    problems {\n      message\n      field\n      type\n    }\n  }\n}\n\nfragment ApprovalRuleListItemFragment_approvalRule on ApprovalRule {\n  id\n  name\n  description\n  approvalsRequired\n  users {\n    id\n    username\n    email\n  }\n  serviceAccounts {\n    id\n    name\n  }\n  teams {\n    id\n    name\n  }\n  metadata {\n    prn\n  }\n}\n"
  }
};
})();

(node as any).hash = "2cc767b237f42531100d8da13aedcc53";

export default node;
