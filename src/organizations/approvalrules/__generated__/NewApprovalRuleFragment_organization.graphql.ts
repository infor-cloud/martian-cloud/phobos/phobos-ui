/**
 * @generated SignedSource<<539a7ce3cd8bcff16ffa930f20ecf63d>>
 * @lightSyntaxTransform
 * @nogrep
 */

/* tslint:disable */
/* eslint-disable */
// @ts-nocheck

import { Fragment, ReaderFragment } from 'relay-runtime';
import { FragmentRefs } from "relay-runtime";
export type NewApprovalRuleFragment_organization$data = {
  readonly id: string;
  readonly name: string;
  readonly " $fragmentType": "NewApprovalRuleFragment_organization";
};
export type NewApprovalRuleFragment_organization$key = {
  readonly " $data"?: NewApprovalRuleFragment_organization$data;
  readonly " $fragmentSpreads": FragmentRefs<"NewApprovalRuleFragment_organization">;
};

const node: ReaderFragment = {
  "argumentDefinitions": [],
  "kind": "Fragment",
  "metadata": null,
  "name": "NewApprovalRuleFragment_organization",
  "selections": [
    {
      "alias": null,
      "args": null,
      "kind": "ScalarField",
      "name": "id",
      "storageKey": null
    },
    {
      "alias": null,
      "args": null,
      "kind": "ScalarField",
      "name": "name",
      "storageKey": null
    }
  ],
  "type": "Organization",
  "abstractKey": null
};

(node as any).hash = "53a62b3fd15c8d896899d3e8e242fd49";

export default node;
