/**
 * @generated SignedSource<<0aa434c5fbe4b8b7efb715b7578982ce>>
 * @lightSyntaxTransform
 * @nogrep
 */

/* tslint:disable */
/* eslint-disable */
// @ts-nocheck

import { Fragment, ReaderFragment } from 'relay-runtime';
import { FragmentRefs } from "relay-runtime";
export type OrgApprovalRulesFragment_organization$data = {
  readonly id: string;
  readonly name: string;
  readonly " $fragmentType": "OrgApprovalRulesFragment_organization";
};
export type OrgApprovalRulesFragment_organization$key = {
  readonly " $data"?: OrgApprovalRulesFragment_organization$data;
  readonly " $fragmentSpreads": FragmentRefs<"OrgApprovalRulesFragment_organization">;
};

const node: ReaderFragment = {
  "argumentDefinitions": [],
  "kind": "Fragment",
  "metadata": null,
  "name": "OrgApprovalRulesFragment_organization",
  "selections": [
    {
      "alias": null,
      "args": null,
      "kind": "ScalarField",
      "name": "id",
      "storageKey": null
    },
    {
      "alias": null,
      "args": null,
      "kind": "ScalarField",
      "name": "name",
      "storageKey": null
    }
  ],
  "type": "Organization",
  "abstractKey": null
};

(node as any).hash = "7a50d2158187eb1f78b0aaf479004ae7";

export default node;
