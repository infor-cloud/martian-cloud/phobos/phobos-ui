import Box from '@mui/material/Box';
import { useOutletContext, useParams } from 'react-router-dom';
import graphql from 'babel-plugin-relay/macro';
import { useFragment } from 'react-relay/hooks';
import OrganizationBreadcrumbs from '../OrganizationBreadcrumbs';
import EditApprovalRule from '../../approvalrules/EditApprovalRule';
import { EditOrgApprovalRuleFragment_organization$key } from './__generated__/EditOrgApprovalRuleFragment_organization.graphql';

function EditOrgApprovalRule() {
    const context = useOutletContext<EditOrgApprovalRuleFragment_organization$key>();
    const approvalRuleId = useParams().approvalRuleId as string;

    const organization = useFragment<EditOrgApprovalRuleFragment_organization$key>(
        graphql`
            fragment EditOrgApprovalRuleFragment_organization on Organization
            {
                id
                name
            }
        `, context
    );

    return (
        <Box>
            <OrganizationBreadcrumbs
                orgName={organization.name}
                childRoutes={[
                    { title: "approval rules", path: 'approval_rules' },
                    { title: "edit", path: `${approvalRuleId}/edit` },
                ]}
            />
            <EditApprovalRule orgId={organization.id} />
        </Box>
    );
}

export default EditOrgApprovalRule;
