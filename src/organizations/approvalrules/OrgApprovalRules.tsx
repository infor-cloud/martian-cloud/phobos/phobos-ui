import { Box, Button, Typography } from '@mui/material';
import graphql from 'babel-plugin-relay/macro';
import { ConnectionHandler, useFragment, useLazyLoadQuery, usePaginationFragment } from 'react-relay/hooks';
import { Link as RouterLink, useOutletContext } from 'react-router-dom';
import OrganizationBreadcrumbs from '../OrganizationBreadcrumbs';
import ApprovalRuleList from '../../approvalrules/ApprovalRuleList';
import { OrgApprovalRulesFragment_organization$key } from './__generated__/OrgApprovalRulesFragment_organization.graphql';
import { OrgApprovalRulesQuery } from './__generated__/OrgApprovalRulesQuery.graphql';
import { OrgApprovalRulesPaginationQuery } from './__generated__/OrgApprovalRulesPaginationQuery.graphql';
import { OrgApprovalRulesFragment_approvalRules$key } from './__generated__/OrgApprovalRulesFragment_approvalRules.graphql';

const INITIAL_ITEM_COUNT = 100;

const DESCRIPTION = 'Approval rules specify the users, service accounts, and teams authorized to approve pipeline tasks.';

const query = graphql`
    query OrgApprovalRulesQuery($id: String!, $first: Int!, $last: Int, $after: String, $before: String) {
        ...OrgApprovalRulesFragment_approvalRules
    }`;

export function GetConnections(orgId: string): [string] {
    const connectionId = ConnectionHandler.getConnectionID(
        orgId,
        'OrgApprovalRules_approvalRules'
    );
    return [connectionId];
}

function OrgApprovalRules() {
    const context = useOutletContext<OrgApprovalRulesFragment_organization$key>();

    const organization = useFragment<OrgApprovalRulesFragment_organization$key>(graphql`
        fragment OrgApprovalRulesFragment_organization on Organization
        {
            id
            name
        }
    `, context);

    const queryData = useLazyLoadQuery<OrgApprovalRulesQuery>(query, { id: `prn:organization:${organization.name}`, first: INITIAL_ITEM_COUNT },
        { fetchPolicy: 'store-and-network' });

    const { data, loadNext, hasNext } = usePaginationFragment<OrgApprovalRulesPaginationQuery, OrgApprovalRulesFragment_approvalRules$key>(
        graphql`
    fragment OrgApprovalRulesFragment_approvalRules on Query
    @refetchable(queryName: "OrgApprovalRulesPaginationQuery") {
        node(id: $id) {
            ...on Organization {
                approvalRules(
                    after: $after
                    before: $before
                    first: $first
                    last: $last
                ) @connection(key: "OrgApprovalRules_approvalRules") {
                    totalCount
                    edges {
                        node {
                            id
                        }
                    }
                    ...ApprovalRuleListFragment_approvalRules
                }
            }
        }
    }`, queryData
    );

    return (
        <Box>
            <OrganizationBreadcrumbs
                orgName={organization.name}
                childRoutes={[
                    { title: "approval rules", path: 'approval_rules' }
                ]}
            />
            <Box>{data.node?.approvalRules?.edges && data.node.approvalRules?.edges.length > 0 ? <ApprovalRuleList
                loadNext={loadNext}
                hasNext={hasNext}
                getConnections={() => GetConnections(organization.id)}
                fragmentRef={data.node?.approvalRules}
            />
                : <Box sx={{ mt: 4 }} display="flex" justifyContent="center">
                    <Box padding={4} display="flex" flexDirection="column" justifyContent="center" alignItems="center" sx={{ maxWidth: 600 }}>
                        <Typography variant="h6">Add an approval rule</Typography>
                        <Typography color="textSecondary" align="center" sx={{ mb: 2 }}>
                            {DESCRIPTION}
                        </Typography>
                        <Button component={RouterLink} variant="outlined" to="new">Add Approval Rule</Button>
                    </Box>
                </Box>}
            </Box>
        </Box>
    );
}

export default OrgApprovalRules;
