/**
 * @generated SignedSource<<2bb4f9b5f9337262fc5ba9471417180c>>
 * @lightSyntaxTransform
 * @nogrep
 */

/* tslint:disable */
/* eslint-disable */
// @ts-nocheck

import { ConcreteRequest, Query } from 'relay-runtime';
import { FragmentRefs } from "relay-runtime";
export type OrganizationDetailsContainerQuery$variables = {
  name: string;
};
export type OrganizationDetailsContainerQuery$data = {
  readonly organization: {
    readonly " $fragmentSpreads": FragmentRefs<"OrganizationDetailsContainerFragment_organization">;
  } | null;
};
export type OrganizationDetailsContainerQuery = {
  response: OrganizationDetailsContainerQuery$data;
  variables: OrganizationDetailsContainerQuery$variables;
};

const node: ConcreteRequest = (function(){
var v0 = [
  {
    "defaultValue": null,
    "kind": "LocalArgument",
    "name": "name"
  }
],
v1 = [
  {
    "kind": "Variable",
    "name": "name",
    "variableName": "name"
  }
];
return {
  "fragment": {
    "argumentDefinitions": (v0/*: any*/),
    "kind": "Fragment",
    "metadata": null,
    "name": "OrganizationDetailsContainerQuery",
    "selections": [
      {
        "alias": null,
        "args": (v1/*: any*/),
        "concreteType": "Organization",
        "kind": "LinkedField",
        "name": "organization",
        "plural": false,
        "selections": [
          {
            "args": null,
            "kind": "FragmentSpread",
            "name": "OrganizationDetailsContainerFragment_organization"
          }
        ],
        "storageKey": null
      }
    ],
    "type": "Query",
    "abstractKey": null
  },
  "kind": "Request",
  "operation": {
    "argumentDefinitions": (v0/*: any*/),
    "kind": "Operation",
    "name": "OrganizationDetailsContainerQuery",
    "selections": [
      {
        "alias": null,
        "args": (v1/*: any*/),
        "concreteType": "Organization",
        "kind": "LinkedField",
        "name": "organization",
        "plural": false,
        "selections": [
          {
            "alias": null,
            "args": null,
            "kind": "ScalarField",
            "name": "id",
            "storageKey": null
          },
          {
            "alias": null,
            "args": null,
            "kind": "ScalarField",
            "name": "name",
            "storageKey": null
          },
          {
            "alias": null,
            "args": null,
            "kind": "ScalarField",
            "name": "description",
            "storageKey": null
          }
        ],
        "storageKey": null
      }
    ]
  },
  "params": {
    "cacheID": "1b709546e5f1a768788040f637b14442",
    "id": null,
    "metadata": {},
    "name": "OrganizationDetailsContainerQuery",
    "operationKind": "query",
    "text": "query OrganizationDetailsContainerQuery(\n  $name: String!\n) {\n  organization(name: $name) {\n    ...OrganizationDetailsContainerFragment_organization\n    id\n  }\n}\n\nfragment OrganizationDetailsContainerFragment_organization on Organization {\n  id\n  name\n  description\n}\n"
  }
};
})();

(node as any).hash = "f570ba01848fc288c6dca126443822c9";

export default node;
