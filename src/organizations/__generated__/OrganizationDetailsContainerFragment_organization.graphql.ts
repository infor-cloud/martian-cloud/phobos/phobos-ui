/**
 * @generated SignedSource<<f406024eae0c969429e6ff5f76e5b757>>
 * @lightSyntaxTransform
 * @nogrep
 */

/* tslint:disable */
/* eslint-disable */
// @ts-nocheck

import { Fragment, ReaderFragment } from 'relay-runtime';
import { FragmentRefs } from "relay-runtime";
export type OrganizationDetailsContainerFragment_organization$data = {
  readonly description: string;
  readonly id: string;
  readonly name: string;
  readonly " $fragmentType": "OrganizationDetailsContainerFragment_organization";
};
export type OrganizationDetailsContainerFragment_organization$key = {
  readonly " $data"?: OrganizationDetailsContainerFragment_organization$data;
  readonly " $fragmentSpreads": FragmentRefs<"OrganizationDetailsContainerFragment_organization">;
};

const node: ReaderFragment = {
  "argumentDefinitions": [],
  "kind": "Fragment",
  "metadata": null,
  "name": "OrganizationDetailsContainerFragment_organization",
  "selections": [
    {
      "alias": null,
      "args": null,
      "kind": "ScalarField",
      "name": "id",
      "storageKey": null
    },
    {
      "alias": null,
      "args": null,
      "kind": "ScalarField",
      "name": "name",
      "storageKey": null
    },
    {
      "alias": null,
      "args": null,
      "kind": "ScalarField",
      "name": "description",
      "storageKey": null
    }
  ],
  "type": "Organization",
  "abstractKey": null
};

(node as any).hash = "1f2462af7a5e8a4f4e21f21cf7ebda08";

export default node;
