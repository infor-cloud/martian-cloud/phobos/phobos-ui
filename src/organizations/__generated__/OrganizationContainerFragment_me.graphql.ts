/**
 * @generated SignedSource<<e56c85e2583ad09977026194924403ce>>
 * @lightSyntaxTransform
 * @nogrep
 */

/* tslint:disable */
/* eslint-disable */
// @ts-nocheck

import { Fragment, ReaderFragment } from 'relay-runtime';
import { FragmentRefs } from "relay-runtime";
export type OrganizationContainerFragment_me$data = {
  readonly me: {
    readonly admin?: boolean;
  } | null;
  readonly " $fragmentType": "OrganizationContainerFragment_me";
};
export type OrganizationContainerFragment_me$key = {
  readonly " $data"?: OrganizationContainerFragment_me$data;
  readonly " $fragmentSpreads": FragmentRefs<"OrganizationContainerFragment_me">;
};

const node: ReaderFragment = {
  "argumentDefinitions": [],
  "kind": "Fragment",
  "metadata": null,
  "name": "OrganizationContainerFragment_me",
  "selections": [
    {
      "alias": null,
      "args": null,
      "concreteType": null,
      "kind": "LinkedField",
      "name": "me",
      "plural": false,
      "selections": [
        {
          "kind": "InlineFragment",
          "selections": [
            {
              "alias": null,
              "args": null,
              "kind": "ScalarField",
              "name": "admin",
              "storageKey": null
            }
          ],
          "type": "User",
          "abstractKey": null
        }
      ],
      "storageKey": null
    }
  ],
  "type": "Query",
  "abstractKey": null
};

(node as any).hash = "76be9b5bb45c0b331467101889965cd3";

export default node;
