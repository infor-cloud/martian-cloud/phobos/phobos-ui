/**
 * @generated SignedSource<<c329a1c423b368e4e732b4d0c6e984f3>>
 * @lightSyntaxTransform
 * @nogrep
 */

/* tslint:disable */
/* eslint-disable */
// @ts-nocheck

import { ConcreteRequest, Query } from 'relay-runtime';
import { FragmentRefs } from "relay-runtime";
export type OrganizationDetailsQuery$variables = {
  after?: string | null | undefined;
  before?: string | null | undefined;
  first: number;
  id: string;
  last?: number | null | undefined;
};
export type OrganizationDetailsQuery$data = {
  readonly node: {
    readonly " $fragmentSpreads": FragmentRefs<"OrganizationDetailsFragment_organization">;
  } | null | undefined;
};
export type OrganizationDetailsQuery = {
  response: OrganizationDetailsQuery$data;
  variables: OrganizationDetailsQuery$variables;
};

const node: ConcreteRequest = (function(){
var v0 = {
  "defaultValue": null,
  "kind": "LocalArgument",
  "name": "after"
},
v1 = {
  "defaultValue": null,
  "kind": "LocalArgument",
  "name": "before"
},
v2 = {
  "defaultValue": null,
  "kind": "LocalArgument",
  "name": "first"
},
v3 = {
  "defaultValue": null,
  "kind": "LocalArgument",
  "name": "id"
},
v4 = {
  "defaultValue": null,
  "kind": "LocalArgument",
  "name": "last"
},
v5 = [
  {
    "kind": "Variable",
    "name": "id",
    "variableName": "id"
  }
],
v6 = {
  "alias": null,
  "args": null,
  "kind": "ScalarField",
  "name": "__typename",
  "storageKey": null
},
v7 = {
  "alias": null,
  "args": null,
  "kind": "ScalarField",
  "name": "id",
  "storageKey": null
},
v8 = {
  "alias": null,
  "args": null,
  "kind": "ScalarField",
  "name": "name",
  "storageKey": null
},
v9 = {
  "alias": null,
  "args": null,
  "kind": "ScalarField",
  "name": "prn",
  "storageKey": null
},
v10 = [
  {
    "kind": "Variable",
    "name": "after",
    "variableName": "after"
  },
  {
    "kind": "Variable",
    "name": "before",
    "variableName": "before"
  },
  {
    "kind": "Variable",
    "name": "first",
    "variableName": "first"
  },
  {
    "kind": "Variable",
    "name": "last",
    "variableName": "last"
  }
],
v11 = {
  "alias": null,
  "args": null,
  "kind": "ScalarField",
  "name": "createdAt",
  "storageKey": null
},
v12 = [
  (v7/*: any*/),
  (v8/*: any*/)
],
v13 = {
  "alias": null,
  "args": null,
  "kind": "ScalarField",
  "name": "cursor",
  "storageKey": null
},
v14 = {
  "alias": null,
  "args": null,
  "concreteType": "PageInfo",
  "kind": "LinkedField",
  "name": "pageInfo",
  "plural": false,
  "selections": [
    {
      "alias": null,
      "args": null,
      "kind": "ScalarField",
      "name": "endCursor",
      "storageKey": null
    },
    {
      "alias": null,
      "args": null,
      "kind": "ScalarField",
      "name": "hasNextPage",
      "storageKey": null
    },
    {
      "alias": null,
      "args": null,
      "kind": "ScalarField",
      "name": "hasPreviousPage",
      "storageKey": null
    },
    {
      "alias": null,
      "args": null,
      "kind": "ScalarField",
      "name": "startCursor",
      "storageKey": null
    }
  ],
  "storageKey": null
};
return {
  "fragment": {
    "argumentDefinitions": [
      (v0/*: any*/),
      (v1/*: any*/),
      (v2/*: any*/),
      (v3/*: any*/),
      (v4/*: any*/)
    ],
    "kind": "Fragment",
    "metadata": null,
    "name": "OrganizationDetailsQuery",
    "selections": [
      {
        "alias": null,
        "args": (v5/*: any*/),
        "concreteType": null,
        "kind": "LinkedField",
        "name": "node",
        "plural": false,
        "selections": [
          {
            "kind": "InlineFragment",
            "selections": [
              {
                "args": null,
                "kind": "FragmentSpread",
                "name": "OrganizationDetailsFragment_organization"
              }
            ],
            "type": "Organization",
            "abstractKey": null
          }
        ],
        "storageKey": null
      }
    ],
    "type": "Query",
    "abstractKey": null
  },
  "kind": "Request",
  "operation": {
    "argumentDefinitions": [
      (v3/*: any*/),
      (v2/*: any*/),
      (v4/*: any*/),
      (v0/*: any*/),
      (v1/*: any*/)
    ],
    "kind": "Operation",
    "name": "OrganizationDetailsQuery",
    "selections": [
      {
        "alias": null,
        "args": (v5/*: any*/),
        "concreteType": null,
        "kind": "LinkedField",
        "name": "node",
        "plural": false,
        "selections": [
          (v6/*: any*/),
          (v7/*: any*/),
          {
            "kind": "InlineFragment",
            "selections": [
              (v8/*: any*/),
              {
                "alias": null,
                "args": null,
                "kind": "ScalarField",
                "name": "description",
                "storageKey": null
              },
              {
                "alias": null,
                "args": null,
                "concreteType": "ResourceMetadata",
                "kind": "LinkedField",
                "name": "metadata",
                "plural": false,
                "selections": [
                  (v9/*: any*/)
                ],
                "storageKey": null
              },
              {
                "alias": null,
                "args": (v10/*: any*/),
                "concreteType": "MembershipConnection",
                "kind": "LinkedField",
                "name": "memberships",
                "plural": false,
                "selections": [
                  {
                    "alias": null,
                    "args": null,
                    "concreteType": "MembershipEdge",
                    "kind": "LinkedField",
                    "name": "edges",
                    "plural": true,
                    "selections": [
                      {
                        "alias": null,
                        "args": null,
                        "concreteType": "Membership",
                        "kind": "LinkedField",
                        "name": "node",
                        "plural": false,
                        "selections": [
                          (v7/*: any*/),
                          (v6/*: any*/),
                          {
                            "alias": null,
                            "args": null,
                            "kind": "ScalarField",
                            "name": "scope",
                            "storageKey": null
                          },
                          {
                            "alias": null,
                            "args": null,
                            "concreteType": "ResourceMetadata",
                            "kind": "LinkedField",
                            "name": "metadata",
                            "plural": false,
                            "selections": [
                              (v9/*: any*/),
                              (v11/*: any*/),
                              {
                                "alias": null,
                                "args": null,
                                "kind": "ScalarField",
                                "name": "updatedAt",
                                "storageKey": null
                              }
                            ],
                            "storageKey": null
                          },
                          {
                            "alias": null,
                            "args": null,
                            "concreteType": "Role",
                            "kind": "LinkedField",
                            "name": "role",
                            "plural": false,
                            "selections": [
                              (v8/*: any*/),
                              (v7/*: any*/)
                            ],
                            "storageKey": null
                          },
                          {
                            "alias": null,
                            "args": null,
                            "concreteType": null,
                            "kind": "LinkedField",
                            "name": "member",
                            "plural": false,
                            "selections": [
                              (v6/*: any*/),
                              {
                                "kind": "InlineFragment",
                                "selections": [
                                  (v7/*: any*/),
                                  {
                                    "alias": null,
                                    "args": null,
                                    "kind": "ScalarField",
                                    "name": "username",
                                    "storageKey": null
                                  },
                                  {
                                    "alias": null,
                                    "args": null,
                                    "kind": "ScalarField",
                                    "name": "email",
                                    "storageKey": null
                                  }
                                ],
                                "type": "User",
                                "abstractKey": null
                              },
                              {
                                "kind": "InlineFragment",
                                "selections": (v12/*: any*/),
                                "type": "Team",
                                "abstractKey": null
                              },
                              {
                                "kind": "InlineFragment",
                                "selections": (v12/*: any*/),
                                "type": "ServiceAccount",
                                "abstractKey": null
                              },
                              {
                                "kind": "InlineFragment",
                                "selections": [
                                  (v7/*: any*/)
                                ],
                                "type": "Node",
                                "abstractKey": "__isNode"
                              }
                            ],
                            "storageKey": null
                          }
                        ],
                        "storageKey": null
                      },
                      (v13/*: any*/)
                    ],
                    "storageKey": null
                  },
                  (v14/*: any*/)
                ],
                "storageKey": null
              },
              {
                "alias": null,
                "args": (v10/*: any*/),
                "filters": null,
                "handle": "connection",
                "key": "OrganizationMemberships_memberships",
                "kind": "LinkedHandle",
                "name": "memberships"
              },
              {
                "alias": null,
                "args": (v10/*: any*/),
                "concreteType": "AgentConnection",
                "kind": "LinkedField",
                "name": "agents",
                "plural": false,
                "selections": [
                  {
                    "alias": null,
                    "args": null,
                    "concreteType": "AgentEdge",
                    "kind": "LinkedField",
                    "name": "edges",
                    "plural": true,
                    "selections": [
                      {
                        "alias": null,
                        "args": null,
                        "concreteType": "Agent",
                        "kind": "LinkedField",
                        "name": "node",
                        "plural": false,
                        "selections": [
                          (v7/*: any*/),
                          (v6/*: any*/),
                          {
                            "alias": null,
                            "args": null,
                            "concreteType": "ResourceMetadata",
                            "kind": "LinkedField",
                            "name": "metadata",
                            "plural": false,
                            "selections": [
                              (v11/*: any*/)
                            ],
                            "storageKey": null
                          },
                          {
                            "alias": null,
                            "args": null,
                            "kind": "ScalarField",
                            "name": "tags",
                            "storageKey": null
                          },
                          (v8/*: any*/),
                          {
                            "alias": null,
                            "args": null,
                            "kind": "ScalarField",
                            "name": "disabled",
                            "storageKey": null
                          },
                          {
                            "alias": null,
                            "args": null,
                            "kind": "ScalarField",
                            "name": "createdBy",
                            "storageKey": null
                          }
                        ],
                        "storageKey": null
                      },
                      (v13/*: any*/)
                    ],
                    "storageKey": null
                  },
                  (v14/*: any*/)
                ],
                "storageKey": null
              },
              {
                "alias": null,
                "args": (v10/*: any*/),
                "filters": null,
                "handle": "connection",
                "key": "OrganizationAgents_agents",
                "kind": "LinkedHandle",
                "name": "agents"
              },
              {
                "alias": null,
                "args": null,
                "kind": "ScalarField",
                "name": "environmentNames",
                "storageKey": null
              }
            ],
            "type": "Organization",
            "abstractKey": null
          }
        ],
        "storageKey": null
      }
    ]
  },
  "params": {
    "cacheID": "46ed37e8fd939e4df4bb448561e33b4f",
    "id": null,
    "metadata": {},
    "name": "OrganizationDetailsQuery",
    "operationKind": "query",
    "text": "query OrganizationDetailsQuery(\n  $id: String!\n  $first: Int!\n  $last: Int\n  $after: String\n  $before: String\n) {\n  node(id: $id) {\n    __typename\n    ... on Organization {\n      ...OrganizationDetailsFragment_organization\n    }\n    id\n  }\n}\n\nfragment AgentListFragment_agents on AgentConnection {\n  edges {\n    node {\n      id\n      ...AgentListItemFragment_agent\n    }\n  }\n}\n\nfragment AgentListItemFragment_agent on Agent {\n  metadata {\n    createdAt\n  }\n  tags\n  id\n  name\n  disabled\n  createdBy\n}\n\nfragment EditOrgApprovalRuleFragment_organization on Organization {\n  id\n  name\n}\n\nfragment EditOrgEnvironmentRuleFragment_organization on Organization {\n  id\n  name\n}\n\nfragment EditOrgReleaseLifecycleFragment_organization on Organization {\n  id\n  name\n}\n\nfragment EditOrgServiceAccountFragment_organization on Organization {\n  id\n  name\n}\n\nfragment EditOrgVCSProviderAuthSettingsFragment_organization on Organization {\n  name\n}\n\nfragment EditOrgVCSProviderFragment_organization on Organization {\n  id\n  name\n}\n\nfragment EditOrganizationAgentFragment_organization on Organization {\n  id\n  name\n}\n\nfragment MembershipListFragment_memberships on MembershipConnection {\n  edges {\n    node {\n      id\n      ...MembershipListItemFragment_membership\n    }\n  }\n}\n\nfragment MembershipListItemFragment_membership on Membership {\n  id\n  scope\n  metadata {\n    prn\n    createdAt\n    updatedAt\n  }\n  role {\n    name\n    id\n  }\n  member {\n    __typename\n    ... on User {\n      id\n      username\n      email\n    }\n    ... on Team {\n      id\n      name\n    }\n    ... on ServiceAccount {\n      id\n      name\n    }\n    ... on Node {\n      __isNode: __typename\n      id\n    }\n  }\n}\n\nfragment NewOrgApprovalRuleFragment_organization on Organization {\n  id\n  name\n}\n\nfragment NewOrgEnvironmentRuleFragment_organization on Organization {\n  id\n  name\n  environmentNames\n}\n\nfragment NewOrgReleaseLifecycleFragment_organization on Organization {\n  id\n  name\n}\n\nfragment NewOrgServiceAccountFragment_organization on Organization {\n  id\n  name\n}\n\nfragment NewOrgVCSProviderFragment_organization on Organization {\n  id\n  name\n}\n\nfragment NewOrganizationAgentFragment_organization on Organization {\n  id\n  name\n}\n\nfragment NewOrganizationMembershipFragment_memberships on Organization {\n  id\n  name\n}\n\nfragment NewProjectFragment_organization on Organization {\n  id\n  name\n}\n\nfragment OrgApprovalRulesFragment_organization on Organization {\n  id\n  name\n}\n\nfragment OrgEnvironmentRules_organization on Organization {\n  id\n  name\n}\n\nfragment OrgReleaseLifecycleDetailsFragment_organization on Organization {\n  id\n  name\n}\n\nfragment OrgReleaseLifecyclesFragment_organization on Organization {\n  name\n}\n\nfragment OrgServiceAccountDetailsFragment_organization on Organization {\n  id\n  name\n}\n\nfragment OrgServiceAccountsFragment_organization on Organization {\n  id\n  name\n}\n\nfragment OrgVCSProviderDetailsFragment_organization on Organization {\n  name\n}\n\nfragment OrganizationActivityFragment_organization on Organization {\n  id\n  name\n}\n\nfragment OrganizationAdvancedSettingsFragment_organization on Organization {\n  id\n  name\n}\n\nfragment OrganizationAgentDetailsFragment_organization on Organization {\n  id\n  name\n}\n\nfragment OrganizationAgentsFragment_agents on Organization {\n  name\n  agents(after: $after, before: $before, first: $first, last: $last) {\n    edges {\n      node {\n        id\n        __typename\n      }\n      cursor\n    }\n    ...AgentListFragment_agents\n    pageInfo {\n      endCursor\n      hasNextPage\n      hasPreviousPage\n      startCursor\n    }\n  }\n  id\n}\n\nfragment OrganizationDetailsFragment_organization on Organization {\n  name\n  ...OrganizationDetailsIndexFragment_organization\n  ...OrganizationSettingsFragment_organization\n  ...NewProjectFragment_organization\n  ...OrgReleaseLifecyclesFragment_organization\n  ...OrgReleaseLifecycleDetailsFragment_organization\n  ...NewOrgReleaseLifecycleFragment_organization\n  ...EditOrgReleaseLifecycleFragment_organization\n  ...OrgServiceAccountsFragment_organization\n  ...OrgServiceAccountDetailsFragment_organization\n  ...NewOrgServiceAccountFragment_organization\n  ...EditOrgServiceAccountFragment_organization\n  ...OrganizationMembershipsFragment_memberships\n  ...NewOrganizationMembershipFragment_memberships\n  ...OrganizationAgentsFragment_agents\n  ...NewOrganizationAgentFragment_organization\n  ...OrganizationAgentDetailsFragment_organization\n  ...EditOrganizationAgentFragment_organization\n  ...OrganizationActivityFragment_organization\n  ...OrgApprovalRulesFragment_organization\n  ...NewOrgApprovalRuleFragment_organization\n  ...EditOrgApprovalRuleFragment_organization\n  ...OrganizationVCSProvidersFragment_organization\n  ...NewOrgVCSProviderFragment_organization\n  ...OrgVCSProviderDetailsFragment_organization\n  ...EditOrgVCSProviderAuthSettingsFragment_organization\n  ...EditOrgVCSProviderFragment_organization\n  ...NewOrgEnvironmentRuleFragment_organization\n  ...EditOrgEnvironmentRuleFragment_organization\n  ...OrganizationInsightsFragment_organization\n}\n\nfragment OrganizationDetailsIndexFragment_organization on Organization {\n  name\n  description\n  metadata {\n    prn\n  }\n}\n\nfragment OrganizationGeneralSettingsFragment_organization on Organization {\n  id\n  name\n  description\n}\n\nfragment OrganizationInsightsFragment_organization on Organization {\n  id\n  name\n  environmentNames\n}\n\nfragment OrganizationMembershipsFragment_memberships on Organization {\n  id\n  name\n  memberships(first: $first, last: $last, after: $after, before: $before) {\n    edges {\n      node {\n        id\n        __typename\n      }\n      cursor\n    }\n    ...MembershipListFragment_memberships\n    pageInfo {\n      endCursor\n      hasNextPage\n      hasPreviousPage\n      startCursor\n    }\n  }\n}\n\nfragment OrganizationSettingsFragment_organization on Organization {\n  name\n  ...OrganizationGeneralSettingsFragment_organization\n  ...OrganizationAdvancedSettingsFragment_organization\n  ...OrgEnvironmentRules_organization\n}\n\nfragment OrganizationVCSProvidersFragment_organization on Organization {\n  id\n  name\n}\n"
  }
};
})();

(node as any).hash = "13c314ff198881f9e4f56f2deb93075c";

export default node;
