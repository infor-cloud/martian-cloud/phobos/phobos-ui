/**
 * @generated SignedSource<<00061a7cf9f369ae32be72185bc6ef70>>
 * @lightSyntaxTransform
 * @nogrep
 */

/* tslint:disable */
/* eslint-disable */
// @ts-nocheck

import { Fragment, ReaderFragment } from 'relay-runtime';
import { FragmentRefs } from "relay-runtime";
export type OrganizationActivityFragment_organization$data = {
  readonly id: string;
  readonly name: string;
  readonly " $fragmentType": "OrganizationActivityFragment_organization";
};
export type OrganizationActivityFragment_organization$key = {
  readonly " $data"?: OrganizationActivityFragment_organization$data;
  readonly " $fragmentSpreads": FragmentRefs<"OrganizationActivityFragment_organization">;
};

const node: ReaderFragment = {
  "argumentDefinitions": [],
  "kind": "Fragment",
  "metadata": null,
  "name": "OrganizationActivityFragment_organization",
  "selections": [
    {
      "alias": null,
      "args": null,
      "kind": "ScalarField",
      "name": "id",
      "storageKey": null
    },
    {
      "alias": null,
      "args": null,
      "kind": "ScalarField",
      "name": "name",
      "storageKey": null
    }
  ],
  "type": "Organization",
  "abstractKey": null
};

(node as any).hash = "3acac3baa6ce669f5164157d31b31740";

export default node;
