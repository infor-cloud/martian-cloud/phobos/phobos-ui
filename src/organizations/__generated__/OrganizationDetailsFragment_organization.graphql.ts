/**
 * @generated SignedSource<<e4bd44d81aad9a6d8bfc991798d610da>>
 * @lightSyntaxTransform
 * @nogrep
 */

/* tslint:disable */
/* eslint-disable */
// @ts-nocheck

import { Fragment, ReaderFragment } from 'relay-runtime';
import { FragmentRefs } from "relay-runtime";
export type OrganizationDetailsFragment_organization$data = {
  readonly name: string;
  readonly " $fragmentSpreads": FragmentRefs<"EditOrgApprovalRuleFragment_organization" | "EditOrgEnvironmentRuleFragment_organization" | "EditOrgReleaseLifecycleFragment_organization" | "EditOrgServiceAccountFragment_organization" | "EditOrgVCSProviderAuthSettingsFragment_organization" | "EditOrgVCSProviderFragment_organization" | "EditOrganizationAgentFragment_organization" | "NewOrgApprovalRuleFragment_organization" | "NewOrgEnvironmentRuleFragment_organization" | "NewOrgReleaseLifecycleFragment_organization" | "NewOrgServiceAccountFragment_organization" | "NewOrgVCSProviderFragment_organization" | "NewOrganizationAgentFragment_organization" | "NewOrganizationMembershipFragment_memberships" | "NewProjectFragment_organization" | "OrgApprovalRulesFragment_organization" | "OrgReleaseLifecycleDetailsFragment_organization" | "OrgReleaseLifecyclesFragment_organization" | "OrgServiceAccountDetailsFragment_organization" | "OrgServiceAccountsFragment_organization" | "OrgVCSProviderDetailsFragment_organization" | "OrganizationActivityFragment_organization" | "OrganizationAgentDetailsFragment_organization" | "OrganizationAgentsFragment_agents" | "OrganizationDetailsIndexFragment_organization" | "OrganizationInsightsFragment_organization" | "OrganizationMembershipsFragment_memberships" | "OrganizationSettingsFragment_organization" | "OrganizationVCSProvidersFragment_organization">;
  readonly " $fragmentType": "OrganizationDetailsFragment_organization";
};
export type OrganizationDetailsFragment_organization$key = {
  readonly " $data"?: OrganizationDetailsFragment_organization$data;
  readonly " $fragmentSpreads": FragmentRefs<"OrganizationDetailsFragment_organization">;
};

const node: ReaderFragment = {
  "argumentDefinitions": [],
  "kind": "Fragment",
  "metadata": null,
  "name": "OrganizationDetailsFragment_organization",
  "selections": [
    {
      "alias": null,
      "args": null,
      "kind": "ScalarField",
      "name": "name",
      "storageKey": null
    },
    {
      "args": null,
      "kind": "FragmentSpread",
      "name": "OrganizationDetailsIndexFragment_organization"
    },
    {
      "args": null,
      "kind": "FragmentSpread",
      "name": "OrganizationSettingsFragment_organization"
    },
    {
      "args": null,
      "kind": "FragmentSpread",
      "name": "NewProjectFragment_organization"
    },
    {
      "args": null,
      "kind": "FragmentSpread",
      "name": "OrgReleaseLifecyclesFragment_organization"
    },
    {
      "args": null,
      "kind": "FragmentSpread",
      "name": "OrgReleaseLifecycleDetailsFragment_organization"
    },
    {
      "args": null,
      "kind": "FragmentSpread",
      "name": "NewOrgReleaseLifecycleFragment_organization"
    },
    {
      "args": null,
      "kind": "FragmentSpread",
      "name": "EditOrgReleaseLifecycleFragment_organization"
    },
    {
      "args": null,
      "kind": "FragmentSpread",
      "name": "OrgServiceAccountsFragment_organization"
    },
    {
      "args": null,
      "kind": "FragmentSpread",
      "name": "OrgServiceAccountDetailsFragment_organization"
    },
    {
      "args": null,
      "kind": "FragmentSpread",
      "name": "NewOrgServiceAccountFragment_organization"
    },
    {
      "args": null,
      "kind": "FragmentSpread",
      "name": "EditOrgServiceAccountFragment_organization"
    },
    {
      "args": null,
      "kind": "FragmentSpread",
      "name": "OrganizationMembershipsFragment_memberships"
    },
    {
      "args": null,
      "kind": "FragmentSpread",
      "name": "NewOrganizationMembershipFragment_memberships"
    },
    {
      "args": null,
      "kind": "FragmentSpread",
      "name": "OrganizationAgentsFragment_agents"
    },
    {
      "args": null,
      "kind": "FragmentSpread",
      "name": "NewOrganizationAgentFragment_organization"
    },
    {
      "args": null,
      "kind": "FragmentSpread",
      "name": "OrganizationAgentDetailsFragment_organization"
    },
    {
      "args": null,
      "kind": "FragmentSpread",
      "name": "EditOrganizationAgentFragment_organization"
    },
    {
      "args": null,
      "kind": "FragmentSpread",
      "name": "OrganizationActivityFragment_organization"
    },
    {
      "args": null,
      "kind": "FragmentSpread",
      "name": "OrgApprovalRulesFragment_organization"
    },
    {
      "args": null,
      "kind": "FragmentSpread",
      "name": "NewOrgApprovalRuleFragment_organization"
    },
    {
      "args": null,
      "kind": "FragmentSpread",
      "name": "EditOrgApprovalRuleFragment_organization"
    },
    {
      "args": null,
      "kind": "FragmentSpread",
      "name": "OrganizationVCSProvidersFragment_organization"
    },
    {
      "args": null,
      "kind": "FragmentSpread",
      "name": "NewOrgVCSProviderFragment_organization"
    },
    {
      "args": null,
      "kind": "FragmentSpread",
      "name": "OrgVCSProviderDetailsFragment_organization"
    },
    {
      "args": null,
      "kind": "FragmentSpread",
      "name": "EditOrgVCSProviderAuthSettingsFragment_organization"
    },
    {
      "args": null,
      "kind": "FragmentSpread",
      "name": "EditOrgVCSProviderFragment_organization"
    },
    {
      "args": null,
      "kind": "FragmentSpread",
      "name": "NewOrgEnvironmentRuleFragment_organization"
    },
    {
      "args": null,
      "kind": "FragmentSpread",
      "name": "EditOrgEnvironmentRuleFragment_organization"
    },
    {
      "args": null,
      "kind": "FragmentSpread",
      "name": "OrganizationInsightsFragment_organization"
    }
  ],
  "type": "Organization",
  "abstractKey": null
};

(node as any).hash = "67403885749dd3a8cf95090553b03352";

export default node;
