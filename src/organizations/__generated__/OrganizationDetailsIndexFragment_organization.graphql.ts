/**
 * @generated SignedSource<<aba993d9f06e8bb5e6a434182ca050c8>>
 * @lightSyntaxTransform
 * @nogrep
 */

/* tslint:disable */
/* eslint-disable */
// @ts-nocheck

import { Fragment, ReaderFragment } from 'relay-runtime';
import { FragmentRefs } from "relay-runtime";
export type OrganizationDetailsIndexFragment_organization$data = {
  readonly description: string;
  readonly metadata: {
    readonly prn: string;
  };
  readonly name: string;
  readonly " $fragmentType": "OrganizationDetailsIndexFragment_organization";
};
export type OrganizationDetailsIndexFragment_organization$key = {
  readonly " $data"?: OrganizationDetailsIndexFragment_organization$data;
  readonly " $fragmentSpreads": FragmentRefs<"OrganizationDetailsIndexFragment_organization">;
};

const node: ReaderFragment = {
  "argumentDefinitions": [],
  "kind": "Fragment",
  "metadata": null,
  "name": "OrganizationDetailsIndexFragment_organization",
  "selections": [
    {
      "alias": null,
      "args": null,
      "kind": "ScalarField",
      "name": "name",
      "storageKey": null
    },
    {
      "alias": null,
      "args": null,
      "kind": "ScalarField",
      "name": "description",
      "storageKey": null
    },
    {
      "alias": null,
      "args": null,
      "concreteType": "ResourceMetadata",
      "kind": "LinkedField",
      "name": "metadata",
      "plural": false,
      "selections": [
        {
          "alias": null,
          "args": null,
          "kind": "ScalarField",
          "name": "prn",
          "storageKey": null
        }
      ],
      "storageKey": null
    }
  ],
  "type": "Organization",
  "abstractKey": null
};

(node as any).hash = "8e2d37fdd92d879bad9ecbb012f024d0";

export default node;
