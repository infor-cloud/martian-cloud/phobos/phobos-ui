/**
 * @generated SignedSource<<1731b9892e822cd44bffbcd829926260>>
 * @lightSyntaxTransform
 * @nogrep
 */

/* tslint:disable */
/* eslint-disable */
// @ts-nocheck

import { ConcreteRequest, Query } from 'relay-runtime';
export type EditOrganizationQuery$variables = {
  name: string;
};
export type EditOrganizationQuery$data = {
  readonly organization: {
    readonly description: string;
    readonly id: string;
    readonly name: string;
  } | null;
};
export type EditOrganizationQuery = {
  response: EditOrganizationQuery$data;
  variables: EditOrganizationQuery$variables;
};

const node: ConcreteRequest = (function(){
var v0 = [
  {
    "defaultValue": null,
    "kind": "LocalArgument",
    "name": "name"
  }
],
v1 = [
  {
    "alias": null,
    "args": [
      {
        "kind": "Variable",
        "name": "name",
        "variableName": "name"
      }
    ],
    "concreteType": "Organization",
    "kind": "LinkedField",
    "name": "organization",
    "plural": false,
    "selections": [
      {
        "alias": null,
        "args": null,
        "kind": "ScalarField",
        "name": "id",
        "storageKey": null
      },
      {
        "alias": null,
        "args": null,
        "kind": "ScalarField",
        "name": "name",
        "storageKey": null
      },
      {
        "alias": null,
        "args": null,
        "kind": "ScalarField",
        "name": "description",
        "storageKey": null
      }
    ],
    "storageKey": null
  }
];
return {
  "fragment": {
    "argumentDefinitions": (v0/*: any*/),
    "kind": "Fragment",
    "metadata": null,
    "name": "EditOrganizationQuery",
    "selections": (v1/*: any*/),
    "type": "Query",
    "abstractKey": null
  },
  "kind": "Request",
  "operation": {
    "argumentDefinitions": (v0/*: any*/),
    "kind": "Operation",
    "name": "EditOrganizationQuery",
    "selections": (v1/*: any*/)
  },
  "params": {
    "cacheID": "182248520979cbddd914c897fa3d6983",
    "id": null,
    "metadata": {},
    "name": "EditOrganizationQuery",
    "operationKind": "query",
    "text": "query EditOrganizationQuery(\n  $name: String!\n) {\n  organization(name: $name) {\n    id\n    name\n    description\n  }\n}\n"
  }
};
})();

(node as any).hash = "e7201aeea3db822ee152b26737afc10c";

export default node;
