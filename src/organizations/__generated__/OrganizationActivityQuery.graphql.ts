/**
 * @generated SignedSource<<61bd5ca9ee77960b46eb7062f5df4bcc>>
 * @lightSyntaxTransform
 * @nogrep
 */

/* tslint:disable */
/* eslint-disable */
// @ts-nocheck

import { ConcreteRequest, Query } from 'relay-runtime';
import { FragmentRefs } from "relay-runtime";
export type OrganizationActivityQuery$variables = {
  after?: string | null | undefined;
  before?: string | null | undefined;
  first?: number | null | undefined;
  last?: number | null | undefined;
  organizationId?: string | null | undefined;
};
export type OrganizationActivityQuery$data = {
  readonly " $fragmentSpreads": FragmentRefs<"OrganizationActivityFragment_activity">;
};
export type OrganizationActivityQuery = {
  response: OrganizationActivityQuery$data;
  variables: OrganizationActivityQuery$variables;
};

const node: ConcreteRequest = (function(){
var v0 = {
  "defaultValue": null,
  "kind": "LocalArgument",
  "name": "after"
},
v1 = {
  "defaultValue": null,
  "kind": "LocalArgument",
  "name": "before"
},
v2 = {
  "defaultValue": null,
  "kind": "LocalArgument",
  "name": "first"
},
v3 = {
  "defaultValue": null,
  "kind": "LocalArgument",
  "name": "last"
},
v4 = {
  "defaultValue": null,
  "kind": "LocalArgument",
  "name": "organizationId"
},
v5 = [
  {
    "kind": "Variable",
    "name": "after",
    "variableName": "after"
  },
  {
    "kind": "Variable",
    "name": "before",
    "variableName": "before"
  },
  {
    "kind": "Variable",
    "name": "first",
    "variableName": "first"
  },
  {
    "kind": "Variable",
    "name": "last",
    "variableName": "last"
  },
  {
    "kind": "Variable",
    "name": "organizationId",
    "variableName": "organizationId"
  },
  {
    "kind": "Literal",
    "name": "sort",
    "value": "CREATED_AT_DESC"
  }
],
v6 = {
  "alias": null,
  "args": null,
  "kind": "ScalarField",
  "name": "id",
  "storageKey": null
},
v7 = {
  "alias": null,
  "args": null,
  "kind": "ScalarField",
  "name": "__typename",
  "storageKey": null
},
v8 = {
  "alias": null,
  "args": null,
  "kind": "ScalarField",
  "name": "name",
  "storageKey": null
},
v9 = {
  "alias": null,
  "args": null,
  "concreteType": "ResourceMetadata",
  "kind": "LinkedField",
  "name": "metadata",
  "plural": false,
  "selections": [
    {
      "alias": null,
      "args": null,
      "kind": "ScalarField",
      "name": "prn",
      "storageKey": null
    }
  ],
  "storageKey": null
},
v10 = [
  (v8/*: any*/),
  {
    "alias": null,
    "args": null,
    "kind": "ScalarField",
    "name": "description",
    "storageKey": null
  },
  (v9/*: any*/)
],
v11 = {
  "alias": null,
  "args": null,
  "kind": "ScalarField",
  "name": "scope",
  "storageKey": null
},
v12 = [
  (v8/*: any*/),
  (v11/*: any*/),
  (v9/*: any*/)
],
v13 = [
  (v8/*: any*/),
  (v9/*: any*/),
  (v6/*: any*/)
],
v14 = {
  "alias": null,
  "args": null,
  "kind": "ScalarField",
  "name": "username",
  "storageKey": null
},
v15 = {
  "kind": "InlineFragment",
  "selections": [
    (v14/*: any*/)
  ],
  "type": "User",
  "abstractKey": null
},
v16 = [
  (v8/*: any*/)
],
v17 = {
  "kind": "InlineFragment",
  "selections": (v16/*: any*/),
  "type": "Team",
  "abstractKey": null
},
v18 = {
  "kind": "InlineFragment",
  "selections": [
    (v6/*: any*/),
    (v8/*: any*/),
    (v9/*: any*/)
  ],
  "type": "ServiceAccount",
  "abstractKey": null
},
v19 = [
  (v6/*: any*/)
],
v20 = {
  "kind": "InlineFragment",
  "selections": (v19/*: any*/),
  "type": "Node",
  "abstractKey": "__isNode"
},
v21 = {
  "alias": null,
  "args": null,
  "kind": "ScalarField",
  "name": "organizationName",
  "storageKey": null
},
v22 = {
  "alias": null,
  "args": null,
  "kind": "ScalarField",
  "name": "semanticVersion",
  "storageKey": null
},
v23 = {
  "alias": null,
  "args": null,
  "kind": "ScalarField",
  "name": "type",
  "storageKey": null
},
v24 = {
  "alias": null,
  "args": null,
  "kind": "ScalarField",
  "name": "version",
  "storageKey": null
},
v25 = [
  (v8/*: any*/),
  (v21/*: any*/),
  (v6/*: any*/)
],
v26 = {
  "alias": null,
  "args": null,
  "kind": "ScalarField",
  "name": "text",
  "storageKey": null
},
v27 = {
  "alias": "releaseType",
  "args": null,
  "concreteType": "Release",
  "kind": "LinkedField",
  "name": "release",
  "plural": false,
  "selections": [
    (v6/*: any*/),
    (v22/*: any*/)
  ],
  "storageKey": null
},
v28 = {
  "alias": "pipelineType",
  "args": null,
  "concreteType": "Pipeline",
  "kind": "LinkedField",
  "name": "pipeline",
  "plural": false,
  "selections": (v19/*: any*/),
  "storageKey": null
},
v29 = {
  "alias": null,
  "args": null,
  "concreteType": null,
  "kind": "LinkedField",
  "name": "member",
  "plural": false,
  "selections": [
    (v7/*: any*/),
    (v15/*: any*/),
    (v18/*: any*/),
    (v17/*: any*/),
    (v20/*: any*/)
  ],
  "storageKey": null
},
v30 = {
  "alias": null,
  "args": null,
  "kind": "ScalarField",
  "name": "changeType",
  "storageKey": null
},
v31 = {
  "alias": null,
  "args": null,
  "kind": "ScalarField",
  "name": "nodePath",
  "storageKey": null
},
v32 = {
  "alias": null,
  "args": null,
  "kind": "ScalarField",
  "name": "nodeType",
  "storageKey": null
},
v33 = [
  (v31/*: any*/),
  (v32/*: any*/)
];
return {
  "fragment": {
    "argumentDefinitions": [
      (v0/*: any*/),
      (v1/*: any*/),
      (v2/*: any*/),
      (v3/*: any*/),
      (v4/*: any*/)
    ],
    "kind": "Fragment",
    "metadata": null,
    "name": "OrganizationActivityQuery",
    "selections": [
      {
        "args": null,
        "kind": "FragmentSpread",
        "name": "OrganizationActivityFragment_activity"
      }
    ],
    "type": "Query",
    "abstractKey": null
  },
  "kind": "Request",
  "operation": {
    "argumentDefinitions": [
      (v4/*: any*/),
      (v2/*: any*/),
      (v3/*: any*/),
      (v0/*: any*/),
      (v1/*: any*/)
    ],
    "kind": "Operation",
    "name": "OrganizationActivityQuery",
    "selections": [
      {
        "alias": null,
        "args": (v5/*: any*/),
        "concreteType": "ActivityEventConnection",
        "kind": "LinkedField",
        "name": "activityEvents",
        "plural": false,
        "selections": [
          {
            "alias": null,
            "args": null,
            "concreteType": "ActivityEventEdge",
            "kind": "LinkedField",
            "name": "edges",
            "plural": true,
            "selections": [
              {
                "alias": null,
                "args": null,
                "concreteType": "ActivityEvent",
                "kind": "LinkedField",
                "name": "node",
                "plural": false,
                "selections": [
                  (v6/*: any*/),
                  (v7/*: any*/),
                  {
                    "alias": null,
                    "args": null,
                    "kind": "ScalarField",
                    "name": "action",
                    "storageKey": null
                  },
                  {
                    "alias": null,
                    "args": null,
                    "concreteType": null,
                    "kind": "LinkedField",
                    "name": "target",
                    "plural": false,
                    "selections": [
                      (v7/*: any*/),
                      (v6/*: any*/),
                      {
                        "kind": "InlineFragment",
                        "selections": (v10/*: any*/),
                        "type": "Organization",
                        "abstractKey": null
                      },
                      {
                        "kind": "InlineFragment",
                        "selections": (v10/*: any*/),
                        "type": "Project",
                        "abstractKey": null
                      },
                      {
                        "kind": "InlineFragment",
                        "selections": [
                          (v9/*: any*/),
                          {
                            "alias": null,
                            "args": null,
                            "kind": "ScalarField",
                            "name": "revision",
                            "storageKey": null
                          }
                        ],
                        "type": "ProjectVariableSet",
                        "abstractKey": null
                      },
                      {
                        "kind": "InlineFragment",
                        "selections": (v12/*: any*/),
                        "type": "ReleaseLifecycle",
                        "abstractKey": null
                      },
                      {
                        "kind": "InlineFragment",
                        "selections": (v12/*: any*/),
                        "type": "ServiceAccount",
                        "abstractKey": null
                      },
                      {
                        "kind": "InlineFragment",
                        "selections": [
                          (v11/*: any*/),
                          {
                            "alias": null,
                            "args": null,
                            "concreteType": "Organization",
                            "kind": "LinkedField",
                            "name": "organization",
                            "plural": false,
                            "selections": (v13/*: any*/),
                            "storageKey": null
                          },
                          {
                            "alias": null,
                            "args": null,
                            "concreteType": "Project",
                            "kind": "LinkedField",
                            "name": "project",
                            "plural": false,
                            "selections": (v13/*: any*/),
                            "storageKey": null
                          },
                          {
                            "alias": null,
                            "args": null,
                            "concreteType": null,
                            "kind": "LinkedField",
                            "name": "member",
                            "plural": false,
                            "selections": [
                              (v7/*: any*/),
                              (v15/*: any*/),
                              (v17/*: any*/),
                              (v18/*: any*/),
                              (v20/*: any*/)
                            ],
                            "storageKey": null
                          }
                        ],
                        "type": "Membership",
                        "abstractKey": null
                      },
                      {
                        "kind": "InlineFragment",
                        "selections": [
                          (v8/*: any*/),
                          {
                            "alias": "agentType",
                            "args": null,
                            "kind": "ScalarField",
                            "name": "type",
                            "storageKey": null
                          },
                          (v9/*: any*/)
                        ],
                        "type": "Agent",
                        "abstractKey": null
                      },
                      {
                        "kind": "InlineFragment",
                        "selections": [
                          (v8/*: any*/),
                          (v9/*: any*/)
                        ],
                        "type": "Environment",
                        "abstractKey": null
                      },
                      {
                        "kind": "InlineFragment",
                        "selections": [
                          (v11/*: any*/),
                          (v21/*: any*/),
                          {
                            "alias": null,
                            "args": null,
                            "kind": "ScalarField",
                            "name": "projectName",
                            "storageKey": null
                          },
                          {
                            "alias": "environmentNameType",
                            "args": null,
                            "kind": "ScalarField",
                            "name": "environmentName",
                            "storageKey": null
                          }
                        ],
                        "type": "EnvironmentRule",
                        "abstractKey": null
                      },
                      {
                        "kind": "InlineFragment",
                        "selections": [
                          (v22/*: any*/),
                          (v9/*: any*/)
                        ],
                        "type": "Release",
                        "abstractKey": null
                      },
                      {
                        "kind": "InlineFragment",
                        "selections": [
                          (v9/*: any*/),
                          (v23/*: any*/),
                          {
                            "alias": null,
                            "args": null,
                            "kind": "ScalarField",
                            "name": "environmentName",
                            "storageKey": null
                          }
                        ],
                        "type": "Pipeline",
                        "abstractKey": null
                      },
                      {
                        "kind": "InlineFragment",
                        "selections": [
                          {
                            "alias": null,
                            "args": null,
                            "kind": "ScalarField",
                            "name": "versioned",
                            "storageKey": null
                          },
                          {
                            "alias": "templateName",
                            "args": null,
                            "kind": "ScalarField",
                            "name": "name",
                            "storageKey": null
                          },
                          {
                            "alias": "templateSemanticVersion",
                            "args": null,
                            "kind": "ScalarField",
                            "name": "semanticVersion",
                            "storageKey": null
                          },
                          (v9/*: any*/)
                        ],
                        "type": "PipelineTemplate",
                        "abstractKey": null
                      },
                      {
                        "kind": "InlineFragment",
                        "selections": (v12/*: any*/),
                        "type": "ApprovalRule",
                        "abstractKey": null
                      },
                      (v17/*: any*/),
                      {
                        "kind": "InlineFragment",
                        "selections": (v16/*: any*/),
                        "type": "Role",
                        "abstractKey": null
                      },
                      {
                        "kind": "InlineFragment",
                        "selections": [
                          (v8/*: any*/),
                          (v21/*: any*/),
                          {
                            "alias": null,
                            "args": null,
                            "concreteType": "PluginVersion",
                            "kind": "LinkedField",
                            "name": "latestVersion",
                            "plural": false,
                            "selections": [
                              (v24/*: any*/),
                              (v6/*: any*/)
                            ],
                            "storageKey": null
                          }
                        ],
                        "type": "Plugin",
                        "abstractKey": null
                      },
                      {
                        "kind": "InlineFragment",
                        "selections": [
                          (v24/*: any*/),
                          {
                            "alias": null,
                            "args": null,
                            "concreteType": "Plugin",
                            "kind": "LinkedField",
                            "name": "plugin",
                            "plural": false,
                            "selections": (v25/*: any*/),
                            "storageKey": null
                          }
                        ],
                        "type": "PluginVersion",
                        "abstractKey": null
                      },
                      {
                        "kind": "InlineFragment",
                        "selections": [
                          (v26/*: any*/),
                          {
                            "alias": null,
                            "args": null,
                            "concreteType": "Thread",
                            "kind": "LinkedField",
                            "name": "thread",
                            "plural": false,
                            "selections": [
                              (v27/*: any*/),
                              (v28/*: any*/),
                              (v6/*: any*/)
                            ],
                            "storageKey": null
                          }
                        ],
                        "type": "Comment",
                        "abstractKey": null
                      },
                      {
                        "kind": "InlineFragment",
                        "selections": [
                          {
                            "alias": null,
                            "args": null,
                            "concreteType": "Comment",
                            "kind": "LinkedField",
                            "name": "rootComment",
                            "plural": false,
                            "selections": [
                              (v6/*: any*/),
                              (v26/*: any*/)
                            ],
                            "storageKey": null
                          },
                          (v27/*: any*/),
                          (v28/*: any*/)
                        ],
                        "type": "Thread",
                        "abstractKey": null
                      },
                      {
                        "kind": "InlineFragment",
                        "selections": (v12/*: any*/),
                        "type": "VCSProvider",
                        "abstractKey": null
                      }
                    ],
                    "storageKey": null
                  },
                  {
                    "alias": null,
                    "args": null,
                    "concreteType": null,
                    "kind": "LinkedField",
                    "name": "payload",
                    "plural": false,
                    "selections": [
                      (v7/*: any*/),
                      {
                        "kind": "InlineFragment",
                        "selections": [
                          (v8/*: any*/),
                          (v23/*: any*/)
                        ],
                        "type": "ActivityEventDeleteResourcePayload",
                        "abstractKey": null
                      },
                      {
                        "kind": "InlineFragment",
                        "selections": [
                          (v29/*: any*/),
                          {
                            "alias": null,
                            "args": null,
                            "kind": "ScalarField",
                            "name": "role",
                            "storageKey": null
                          }
                        ],
                        "type": "ActivityEventCreateMembershipPayload",
                        "abstractKey": null
                      },
                      {
                        "kind": "InlineFragment",
                        "selections": [
                          (v29/*: any*/)
                        ],
                        "type": "ActivityEventRemoveMembershipPayload",
                        "abstractKey": null
                      },
                      {
                        "kind": "InlineFragment",
                        "selections": [
                          {
                            "alias": null,
                            "args": null,
                            "kind": "ScalarField",
                            "name": "prevRole",
                            "storageKey": null
                          },
                          {
                            "alias": null,
                            "args": null,
                            "kind": "ScalarField",
                            "name": "newRole",
                            "storageKey": null
                          }
                        ],
                        "type": "ActivityEventUpdateMembershipPayload",
                        "abstractKey": null
                      },
                      {
                        "kind": "InlineFragment",
                        "selections": [
                          (v30/*: any*/),
                          (v23/*: any*/),
                          {
                            "alias": null,
                            "args": null,
                            "concreteType": null,
                            "kind": "LinkedField",
                            "name": "resource",
                            "plural": false,
                            "selections": [
                              (v7/*: any*/),
                              (v17/*: any*/),
                              (v15/*: any*/),
                              (v6/*: any*/)
                            ],
                            "storageKey": null
                          }
                        ],
                        "type": "ActivityEventUpdateReleasePayload",
                        "abstractKey": null
                      },
                      {
                        "kind": "InlineFragment",
                        "selections": (v33/*: any*/),
                        "type": "ActivityEventUpdatePipelineNodePayload",
                        "abstractKey": null
                      },
                      {
                        "kind": "InlineFragment",
                        "selections": (v33/*: any*/),
                        "type": "ActivityEventApprovePipelineNodePayload",
                        "abstractKey": null
                      },
                      {
                        "kind": "InlineFragment",
                        "selections": (v33/*: any*/),
                        "type": "ActivityEventRevokeApprovalPipelineNodePayload",
                        "abstractKey": null
                      },
                      {
                        "kind": "InlineFragment",
                        "selections": (v33/*: any*/),
                        "type": "ActivityEventRetryPipelineNodePayload",
                        "abstractKey": null
                      },
                      {
                        "kind": "InlineFragment",
                        "selections": [
                          (v31/*: any*/),
                          (v32/*: any*/),
                          {
                            "alias": null,
                            "args": null,
                            "kind": "ScalarField",
                            "name": "startTime",
                            "storageKey": null
                          }
                        ],
                        "type": "ActivityEventSchedulePipelineNodePayload",
                        "abstractKey": null
                      },
                      {
                        "kind": "InlineFragment",
                        "selections": (v33/*: any*/),
                        "type": "ActivityEventUnschedulePipelineNodePayload",
                        "abstractKey": null
                      },
                      {
                        "kind": "InlineFragment",
                        "selections": (v33/*: any*/),
                        "type": "ActivityEventStartPipelineNodePayload",
                        "abstractKey": null
                      },
                      {
                        "kind": "InlineFragment",
                        "selections": [
                          (v31/*: any*/),
                          (v32/*: any*/),
                          {
                            "alias": null,
                            "args": null,
                            "kind": "ScalarField",
                            "name": "reason",
                            "storageKey": null
                          }
                        ],
                        "type": "ActivityEventDeferPipelineNodePayload",
                        "abstractKey": null
                      },
                      {
                        "kind": "InlineFragment",
                        "selections": (v33/*: any*/),
                        "type": "ActivityEventUndeferPipelineNodePayload",
                        "abstractKey": null
                      },
                      {
                        "kind": "InlineFragment",
                        "selections": [
                          {
                            "alias": null,
                            "args": null,
                            "concreteType": "User",
                            "kind": "LinkedField",
                            "name": "user",
                            "plural": false,
                            "selections": [
                              (v14/*: any*/),
                              (v6/*: any*/)
                            ],
                            "storageKey": null
                          },
                          (v30/*: any*/),
                          {
                            "alias": null,
                            "args": null,
                            "kind": "ScalarField",
                            "name": "maintainer",
                            "storageKey": null
                          }
                        ],
                        "type": "ActivityEventUpdateTeamPayload",
                        "abstractKey": null
                      }
                    ],
                    "storageKey": null
                  },
                  {
                    "alias": null,
                    "args": null,
                    "concreteType": "ResourceMetadata",
                    "kind": "LinkedField",
                    "name": "metadata",
                    "plural": false,
                    "selections": [
                      {
                        "alias": null,
                        "args": null,
                        "kind": "ScalarField",
                        "name": "createdAt",
                        "storageKey": null
                      }
                    ],
                    "storageKey": null
                  },
                  {
                    "alias": null,
                    "args": null,
                    "concreteType": null,
                    "kind": "LinkedField",
                    "name": "initiator",
                    "plural": false,
                    "selections": [
                      (v7/*: any*/),
                      {
                        "kind": "InlineFragment",
                        "selections": [
                          (v14/*: any*/),
                          {
                            "alias": null,
                            "args": null,
                            "kind": "ScalarField",
                            "name": "email",
                            "storageKey": null
                          }
                        ],
                        "type": "User",
                        "abstractKey": null
                      },
                      {
                        "kind": "InlineFragment",
                        "selections": (v16/*: any*/),
                        "type": "ServiceAccount",
                        "abstractKey": null
                      },
                      (v20/*: any*/)
                    ],
                    "storageKey": null
                  },
                  {
                    "alias": null,
                    "args": null,
                    "kind": "ScalarField",
                    "name": "targetType",
                    "storageKey": null
                  },
                  {
                    "alias": null,
                    "args": null,
                    "concreteType": "Project",
                    "kind": "LinkedField",
                    "name": "project",
                    "plural": false,
                    "selections": (v25/*: any*/),
                    "storageKey": null
                  }
                ],
                "storageKey": null
              },
              {
                "alias": null,
                "args": null,
                "kind": "ScalarField",
                "name": "cursor",
                "storageKey": null
              }
            ],
            "storageKey": null
          },
          {
            "alias": null,
            "args": null,
            "concreteType": "PageInfo",
            "kind": "LinkedField",
            "name": "pageInfo",
            "plural": false,
            "selections": [
              {
                "alias": null,
                "args": null,
                "kind": "ScalarField",
                "name": "endCursor",
                "storageKey": null
              },
              {
                "alias": null,
                "args": null,
                "kind": "ScalarField",
                "name": "hasNextPage",
                "storageKey": null
              },
              {
                "alias": null,
                "args": null,
                "kind": "ScalarField",
                "name": "hasPreviousPage",
                "storageKey": null
              },
              {
                "alias": null,
                "args": null,
                "kind": "ScalarField",
                "name": "startCursor",
                "storageKey": null
              }
            ],
            "storageKey": null
          }
        ],
        "storageKey": null
      },
      {
        "alias": null,
        "args": (v5/*: any*/),
        "filters": [
          "organizationId",
          "sort"
        ],
        "handle": "connection",
        "key": "OrganizationActivityFragment_activityEvents",
        "kind": "LinkedHandle",
        "name": "activityEvents"
      }
    ]
  },
  "params": {
    "cacheID": "7d72b24f8bcabf02dbb2459f3f12f83a",
    "id": null,
    "metadata": {},
    "name": "OrganizationActivityQuery",
    "operationKind": "query",
    "text": "query OrganizationActivityQuery(\n  $organizationId: String\n  $first: Int\n  $last: Int\n  $after: String\n  $before: String\n) {\n  ...OrganizationActivityFragment_activity\n}\n\nfragment ActivityEventAgentTargetFragment_event on ActivityEvent {\n  action\n  target {\n    __typename\n    ... on Agent {\n      id\n      name\n      agentType: type\n      metadata {\n        prn\n      }\n    }\n    id\n  }\n  ...ActivityEventListItemFragment_event\n}\n\nfragment ActivityEventApprovalRuleTargetFragment_event on ActivityEvent {\n  action\n  target {\n    __typename\n    ... on ApprovalRule {\n      name\n      scope\n      metadata {\n        prn\n      }\n    }\n    id\n  }\n  ...ActivityEventListItemFragment_event\n}\n\nfragment ActivityEventCommentTargetFragment_event on ActivityEvent {\n  action\n  project {\n    name\n    organizationName\n    id\n  }\n  target {\n    __typename\n    ... on Comment {\n      text\n      thread {\n        releaseType: release {\n          id\n          semanticVersion\n        }\n        pipelineType: pipeline {\n          id\n        }\n        id\n      }\n    }\n    id\n  }\n  ...ActivityEventListItemFragment_event\n}\n\nfragment ActivityEventEnvironmentRuleTargetFragment_event on ActivityEvent {\n  action\n  target {\n    __typename\n    ... on EnvironmentRule {\n      id\n      scope\n      organizationName\n      projectName\n      environmentNameType: environmentName\n    }\n    id\n  }\n  ...ActivityEventListItemFragment_event\n}\n\nfragment ActivityEventEnvironmentTargetFragment_event on ActivityEvent {\n  action\n  target {\n    __typename\n    ... on Environment {\n      id\n      name\n      metadata {\n        prn\n      }\n    }\n    id\n  }\n  ...ActivityEventListItemFragment_event\n}\n\nfragment ActivityEventListFragment_connection on ActivityEventConnection {\n  edges {\n    node {\n      id\n      action\n      target {\n        __typename\n        id\n      }\n      ...ActivityEventOrganizationTargetFragment_event\n      ...ActivityEventProjectTargetFragment_event\n      ...ActivityEventProjectVariableSetTargetFragment_event\n      ...ActivityEventReleaseLifecycleTargetFragment_event\n      ...ActivityEventServiceAccountTargetFragment_event\n      ...ActivityEventMembershipTargetFragment_event\n      ...ActivityEventAgentTargetFragment_event\n      ...ActivityEventEnvironmentTargetFragment_event\n      ...ActivityEventEnvironmentRuleTargetFragment_event\n      ...ActivityEventReleaseTargetFragment_event\n      ...ActivityEventPipelineTargetFragment_event\n      ...ActivityEventPipelineTemplateTargetFragment_event\n      ...ActivityEventApprovalRuleTargetFragment_event\n      ...ActivityEventTeamTargetFragment_event\n      ...ActivityEventRoleTargetFragment_event\n      ...ActivityEventPluginTargetFragment_event\n      ...ActivityEventPluginVersionTargetFragment_event\n      ...ActivityEventCommentTargetFragment_event\n      ...ActivityEventThreadTargetFragment_event\n      ...ActivityEventVCSProviderTargetFragment_event\n    }\n  }\n}\n\nfragment ActivityEventListItemFragment_event on ActivityEvent {\n  metadata {\n    createdAt\n  }\n  id\n  initiator {\n    __typename\n    ... on User {\n      username\n      email\n    }\n    ... on ServiceAccount {\n      name\n    }\n    ... on Node {\n      __isNode: __typename\n      id\n    }\n  }\n}\n\nfragment ActivityEventMembershipTargetFragment_event on ActivityEvent {\n  action\n  target {\n    __typename\n    ... on Membership {\n      scope\n      organization {\n        name\n        metadata {\n          prn\n        }\n        id\n      }\n      project {\n        name\n        metadata {\n          prn\n        }\n        id\n      }\n      member {\n        __typename\n        ... on User {\n          username\n        }\n        ... on Team {\n          name\n        }\n        ... on ServiceAccount {\n          id\n          name\n          metadata {\n            prn\n          }\n        }\n        ... on Node {\n          __isNode: __typename\n          id\n        }\n      }\n    }\n    id\n  }\n  targetType\n  payload {\n    __typename\n    ... on ActivityEventUpdateMembershipPayload {\n      prevRole\n      newRole\n    }\n  }\n  ...ActivityEventListItemFragment_event\n}\n\nfragment ActivityEventOrganizationTargetFragment_event on ActivityEvent {\n  action\n  target {\n    __typename\n    ... on Organization {\n      name\n      description\n      metadata {\n        prn\n      }\n    }\n    id\n  }\n  payload {\n    __typename\n    ... on ActivityEventDeleteResourcePayload {\n      name\n      type\n    }\n    ... on ActivityEventCreateMembershipPayload {\n      member {\n        __typename\n        ... on User {\n          username\n        }\n        ... on ServiceAccount {\n          id\n          name\n          metadata {\n            prn\n          }\n        }\n        ... on Team {\n          name\n        }\n        ... on Node {\n          __isNode: __typename\n          id\n        }\n      }\n      role\n    }\n    ... on ActivityEventRemoveMembershipPayload {\n      member {\n        __typename\n        ... on User {\n          username\n        }\n        ... on ServiceAccount {\n          id\n          name\n          metadata {\n            prn\n          }\n        }\n        ... on Team {\n          name\n        }\n        ... on Node {\n          __isNode: __typename\n          id\n        }\n      }\n    }\n  }\n  ...ActivityEventListItemFragment_event\n}\n\nfragment ActivityEventPipelineTargetFragment_event on ActivityEvent {\n  action\n  target {\n    __typename\n    ... on Pipeline {\n      id\n      metadata {\n        prn\n      }\n      type\n      environmentName\n    }\n    id\n  }\n  payload {\n    __typename\n    ... on ActivityEventDeleteResourcePayload {\n      type\n    }\n    ... on ActivityEventUpdatePipelineNodePayload {\n      nodePath\n      nodeType\n    }\n    ... on ActivityEventApprovePipelineNodePayload {\n      nodePath\n      nodeType\n    }\n    ... on ActivityEventRevokeApprovalPipelineNodePayload {\n      nodePath\n      nodeType\n    }\n    ... on ActivityEventRetryPipelineNodePayload {\n      nodePath\n      nodeType\n    }\n    ... on ActivityEventSchedulePipelineNodePayload {\n      nodePath\n      nodeType\n      startTime\n    }\n    ... on ActivityEventUnschedulePipelineNodePayload {\n      nodePath\n      nodeType\n    }\n    ... on ActivityEventStartPipelineNodePayload {\n      nodePath\n      nodeType\n    }\n    ... on ActivityEventDeferPipelineNodePayload {\n      nodePath\n      nodeType\n      reason\n    }\n    ... on ActivityEventUndeferPipelineNodePayload {\n      nodePath\n      nodeType\n    }\n  }\n  ...ActivityEventListItemFragment_event\n}\n\nfragment ActivityEventPipelineTemplateTargetFragment_event on ActivityEvent {\n  action\n  target {\n    __typename\n    ... on PipelineTemplate {\n      id\n      versioned\n      templateName: name\n      templateSemanticVersion: semanticVersion\n      metadata {\n        prn\n      }\n    }\n    id\n  }\n  ...ActivityEventListItemFragment_event\n}\n\nfragment ActivityEventPluginTargetFragment_event on ActivityEvent {\n  action\n  target {\n    __typename\n    ... on Plugin {\n      name\n      organizationName\n      latestVersion {\n        version\n        id\n      }\n    }\n    id\n  }\n  ...ActivityEventListItemFragment_event\n}\n\nfragment ActivityEventPluginVersionTargetFragment_event on ActivityEvent {\n  action\n  target {\n    __typename\n    ... on PluginVersion {\n      version\n      plugin {\n        name\n        organizationName\n        id\n      }\n    }\n    id\n  }\n  ...ActivityEventListItemFragment_event\n}\n\nfragment ActivityEventProjectTargetFragment_event on ActivityEvent {\n  action\n  target {\n    __typename\n    ... on Project {\n      name\n      description\n      metadata {\n        prn\n      }\n    }\n    id\n  }\n  payload {\n    __typename\n    ... on ActivityEventDeleteResourcePayload {\n      name\n      type\n    }\n    ... on ActivityEventCreateMembershipPayload {\n      member {\n        __typename\n        ... on User {\n          username\n        }\n        ... on ServiceAccount {\n          id\n          name\n          metadata {\n            prn\n          }\n        }\n        ... on Team {\n          name\n        }\n        ... on Node {\n          __isNode: __typename\n          id\n        }\n      }\n      role\n    }\n    ... on ActivityEventRemoveMembershipPayload {\n      member {\n        __typename\n        ... on User {\n          username\n        }\n        ... on ServiceAccount {\n          id\n          name\n          metadata {\n            prn\n          }\n        }\n        ... on Team {\n          name\n        }\n        ... on Node {\n          __isNode: __typename\n          id\n        }\n      }\n    }\n  }\n  ...ActivityEventListItemFragment_event\n}\n\nfragment ActivityEventProjectVariableSetTargetFragment_event on ActivityEvent {\n  action\n  target {\n    __typename\n    ... on ProjectVariableSet {\n      metadata {\n        prn\n      }\n      revision\n    }\n    id\n  }\n  ...ActivityEventListItemFragment_event\n}\n\nfragment ActivityEventReleaseLifecycleTargetFragment_event on ActivityEvent {\n  action\n  target {\n    __typename\n    ... on ReleaseLifecycle {\n      id\n      name\n      scope\n      metadata {\n        prn\n      }\n    }\n    id\n  }\n  ...ActivityEventListItemFragment_event\n}\n\nfragment ActivityEventReleaseTargetFragment_event on ActivityEvent {\n  project {\n    name\n    organizationName\n    id\n  }\n  target {\n    __typename\n    ... on Release {\n      id\n      semanticVersion\n      metadata {\n        prn\n      }\n    }\n    id\n  }\n  action\n  payload {\n    __typename\n    ... on ActivityEventUpdateReleasePayload {\n      changeType\n      type\n      resource {\n        __typename\n        ... on Team {\n          name\n        }\n        ... on User {\n          username\n        }\n        id\n      }\n    }\n    ... on ActivityEventDeleteResourcePayload {\n      type\n    }\n  }\n  ...ActivityEventListItemFragment_event\n}\n\nfragment ActivityEventRoleTargetFragment_event on ActivityEvent {\n  action\n  target {\n    __typename\n    ... on Role {\n      name\n    }\n    id\n  }\n  ...ActivityEventListItemFragment_event\n}\n\nfragment ActivityEventServiceAccountTargetFragment_event on ActivityEvent {\n  action\n  target {\n    __typename\n    ... on ServiceAccount {\n      id\n      name\n      scope\n      metadata {\n        prn\n      }\n    }\n    id\n  }\n  ...ActivityEventListItemFragment_event\n}\n\nfragment ActivityEventTeamTargetFragment_event on ActivityEvent {\n  action\n  target {\n    __typename\n    ... on Team {\n      name\n    }\n    id\n  }\n  payload {\n    __typename\n    ... on ActivityEventUpdateTeamPayload {\n      user {\n        username\n        id\n      }\n      changeType\n      maintainer\n    }\n  }\n  ...ActivityEventListItemFragment_event\n}\n\nfragment ActivityEventThreadTargetFragment_event on ActivityEvent {\n  action\n  project {\n    name\n    organizationName\n    id\n  }\n  target {\n    __typename\n    ... on Thread {\n      rootComment {\n        id\n        text\n      }\n      releaseType: release {\n        id\n        semanticVersion\n      }\n      pipelineType: pipeline {\n        id\n      }\n    }\n    id\n  }\n  ...ActivityEventListItemFragment_event\n}\n\nfragment ActivityEventVCSProviderTargetFragment_event on ActivityEvent {\n  action\n  target {\n    __typename\n    ... on VCSProvider {\n      id\n      name\n      scope\n      metadata {\n        prn\n      }\n    }\n    id\n  }\n  ...ActivityEventListItemFragment_event\n}\n\nfragment OrganizationActivityFragment_activity on Query {\n  activityEvents(organizationId: $organizationId, first: $first, last: $last, after: $after, before: $before, sort: CREATED_AT_DESC) {\n    edges {\n      node {\n        id\n        __typename\n      }\n      cursor\n    }\n    ...ActivityEventListFragment_connection\n    pageInfo {\n      endCursor\n      hasNextPage\n      hasPreviousPage\n      startCursor\n    }\n  }\n}\n"
  }
};
})();

(node as any).hash = "41a84fb2062df2e5028c5fe78f422f9b";

export default node;
