import Box from '@mui/material/Box';
import graphql from 'babel-plugin-relay/macro';
import { ConnectionHandler, usePaginationFragment } from "react-relay/hooks";
import { useOutletContext } from 'react-router-dom';
import OrganizationBreadcrumbs from "../OrganizationBreadcrumbs";
import AgentList from '../../agents/AgentList';
import { OrganizationPaginationQuery } from '../../nav/__generated__/OrganizationPaginationQuery.graphql';
import { OrganizationAgentsFragment_agents$key } from './__generated__/OrganizationAgentsFragment_agents.graphql';

export function GetConnections(orgId: string): [string] {
    const connectionId = ConnectionHandler.getConnectionID(
        orgId,
        'OrganizationAgents_agents',
    );
    return [connectionId];
}

function OrganizationAgents() {
    const context = useOutletContext<OrganizationAgentsFragment_agents$key>();

    const { data, loadNext, hasNext } = usePaginationFragment<OrganizationPaginationQuery, OrganizationAgentsFragment_agents$key>(
        graphql`
            fragment OrganizationAgentsFragment_agents on Organization
            @refetchable(queryName: "OrganizationAgentsPaginationQuery") {
                name
                agents(
                    after: $after
                    before: $before
                    first: $first
                    last: $last
                ) @connection(key: "OrganizationAgents_agents") {
                    edges {
                        node {
                            id
                        }
                    }
                    ...AgentListFragment_agents
                }
            }
        `,
    context);

    return (
        <Box>
            <OrganizationBreadcrumbs
                orgName={data.name}
                childRoutes={[
                    { title: "agents", path: 'agents' }
                ]}
            />
            <Box>
                <AgentList
                    fragmentRef={data.agents}
                    hasNext={hasNext}
                    loadNext={loadNext}
                />
            </Box>
        </Box>
    );
}

export default OrganizationAgents
