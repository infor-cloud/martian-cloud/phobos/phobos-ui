/**
 * @generated SignedSource<<bbd779eea27a3e59d6151215716152cc>>
 * @lightSyntaxTransform
 * @nogrep
 */

/* tslint:disable */
/* eslint-disable */
// @ts-nocheck

import { Fragment, ReaderFragment } from 'relay-runtime';
import { FragmentRefs } from "relay-runtime";
export type AgentListFragment_organization$data = {
  readonly description: string;
  readonly name: string;
  readonly " $fragmentType": "AgentListFragment_organization";
};
export type AgentListFragment_organization$key = {
  readonly " $data"?: AgentListFragment_organization$data;
  readonly " $fragmentSpreads": FragmentRefs<"AgentListFragment_organization">;
};

const node: ReaderFragment = {
  "argumentDefinitions": [],
  "kind": "Fragment",
  "metadata": null,
  "name": "AgentListFragment_organization",
  "selections": [
    {
      "alias": null,
      "args": null,
      "kind": "ScalarField",
      "name": "name",
      "storageKey": null
    },
    {
      "alias": null,
      "args": null,
      "kind": "ScalarField",
      "name": "description",
      "storageKey": null
    }
  ],
  "type": "Organization",
  "abstractKey": null
};

(node as any).hash = "72ca80bd4e58631358e10c6d163bd09b";

export default node;
