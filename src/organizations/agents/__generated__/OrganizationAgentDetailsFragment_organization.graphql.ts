/**
 * @generated SignedSource<<08ef7004d82696e29a935a1da62d9256>>
 * @lightSyntaxTransform
 * @nogrep
 */

/* tslint:disable */
/* eslint-disable */
// @ts-nocheck

import { Fragment, ReaderFragment } from 'relay-runtime';
import { FragmentRefs } from "relay-runtime";
export type OrganizationAgentDetailsFragment_organization$data = {
  readonly id: string;
  readonly name: string;
  readonly " $fragmentType": "OrganizationAgentDetailsFragment_organization";
};
export type OrganizationAgentDetailsFragment_organization$key = {
  readonly " $data"?: OrganizationAgentDetailsFragment_organization$data;
  readonly " $fragmentSpreads": FragmentRefs<"OrganizationAgentDetailsFragment_organization">;
};

const node: ReaderFragment = {
  "argumentDefinitions": [],
  "kind": "Fragment",
  "metadata": null,
  "name": "OrganizationAgentDetailsFragment_organization",
  "selections": [
    {
      "alias": null,
      "args": null,
      "kind": "ScalarField",
      "name": "id",
      "storageKey": null
    },
    {
      "alias": null,
      "args": null,
      "kind": "ScalarField",
      "name": "name",
      "storageKey": null
    }
  ],
  "type": "Organization",
  "abstractKey": null
};

(node as any).hash = "bbe7c55a7ae87d86307fb9bc9eae0375";

export default node;
