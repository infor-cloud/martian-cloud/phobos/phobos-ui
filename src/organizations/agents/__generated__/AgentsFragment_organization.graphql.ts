/**
 * @generated SignedSource<<33e3c70c7e9c38e5b33753111fd88e66>>
 * @lightSyntaxTransform
 * @nogrep
 */

/* tslint:disable */
/* eslint-disable */
// @ts-nocheck

import { Fragment, ReaderFragment } from 'relay-runtime';
import { FragmentRefs } from "relay-runtime";
export type AgentsFragment_organization$data = {
  readonly name: string;
  readonly " $fragmentSpreads": FragmentRefs<"AgentListFragment_organization">;
  readonly " $fragmentType": "AgentsFragment_organization";
};
export type AgentsFragment_organization$key = {
  readonly " $data"?: AgentsFragment_organization$data;
  readonly " $fragmentSpreads": FragmentRefs<"AgentsFragment_organization">;
};

const node: ReaderFragment = {
  "argumentDefinitions": [],
  "kind": "Fragment",
  "metadata": null,
  "name": "AgentsFragment_organization",
  "selections": [
    {
      "alias": null,
      "args": null,
      "kind": "ScalarField",
      "name": "name",
      "storageKey": null
    },
    {
      "args": null,
      "kind": "FragmentSpread",
      "name": "AgentListFragment_organization"
    }
  ],
  "type": "Organization",
  "abstractKey": null
};

(node as any).hash = "708baa8db19201f3528c64f323957668";

export default node;
