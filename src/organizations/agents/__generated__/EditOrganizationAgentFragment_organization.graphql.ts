/**
 * @generated SignedSource<<ccda3274bd23a52a87631a96bb2c9955>>
 * @lightSyntaxTransform
 * @nogrep
 */

/* tslint:disable */
/* eslint-disable */
// @ts-nocheck

import { Fragment, ReaderFragment } from 'relay-runtime';
import { FragmentRefs } from "relay-runtime";
export type EditOrganizationAgentFragment_organization$data = {
  readonly id: string;
  readonly name: string;
  readonly " $fragmentType": "EditOrganizationAgentFragment_organization";
};
export type EditOrganizationAgentFragment_organization$key = {
  readonly " $data"?: EditOrganizationAgentFragment_organization$data;
  readonly " $fragmentSpreads": FragmentRefs<"EditOrganizationAgentFragment_organization">;
};

const node: ReaderFragment = {
  "argumentDefinitions": [],
  "kind": "Fragment",
  "metadata": null,
  "name": "EditOrganizationAgentFragment_organization",
  "selections": [
    {
      "alias": null,
      "args": null,
      "kind": "ScalarField",
      "name": "id",
      "storageKey": null
    },
    {
      "alias": null,
      "args": null,
      "kind": "ScalarField",
      "name": "name",
      "storageKey": null
    }
  ],
  "type": "Organization",
  "abstractKey": null
};

(node as any).hash = "f88606d2f87d75223e02faaa5bbb260d";

export default node;
