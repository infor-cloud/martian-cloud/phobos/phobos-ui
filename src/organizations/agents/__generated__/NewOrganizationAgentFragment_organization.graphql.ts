/**
 * @generated SignedSource<<8569a20f221a6e8eaa1d6f19c6eef52f>>
 * @lightSyntaxTransform
 * @nogrep
 */

/* tslint:disable */
/* eslint-disable */
// @ts-nocheck

import { Fragment, ReaderFragment } from 'relay-runtime';
import { FragmentRefs } from "relay-runtime";
export type NewOrganizationAgentFragment_organization$data = {
  readonly id: string;
  readonly name: string;
  readonly " $fragmentType": "NewOrganizationAgentFragment_organization";
};
export type NewOrganizationAgentFragment_organization$key = {
  readonly " $data"?: NewOrganizationAgentFragment_organization$data;
  readonly " $fragmentSpreads": FragmentRefs<"NewOrganizationAgentFragment_organization">;
};

const node: ReaderFragment = {
  "argumentDefinitions": [],
  "kind": "Fragment",
  "metadata": null,
  "name": "NewOrganizationAgentFragment_organization",
  "selections": [
    {
      "alias": null,
      "args": null,
      "kind": "ScalarField",
      "name": "id",
      "storageKey": null
    },
    {
      "alias": null,
      "args": null,
      "kind": "ScalarField",
      "name": "name",
      "storageKey": null
    }
  ],
  "type": "Organization",
  "abstractKey": null
};

(node as any).hash = "4dd3bf82cf32fd56b033a5ea1164b668";

export default node;
