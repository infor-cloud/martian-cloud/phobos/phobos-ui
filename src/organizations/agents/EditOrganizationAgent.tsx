import { useState } from 'react';
import { Box, Button, Divider, Typography } from '@mui/material';
import LoadingButton from '@mui/lab/LoadingButton';
import graphql from 'babel-plugin-relay/macro';
import { useFragment, useLazyLoadQuery, useMutation } from 'react-relay/hooks';
import { Link as RouterLink, useNavigate, useOutletContext, useParams } from 'react-router-dom';
import { MutationError } from '../../common/error';
import OrganizationBreadcrumbs from '../OrganizationBreadcrumbs';
import AgentForm, { FormData } from '../../agents/AgentForm';
import { EditOrganizationAgentFragment_organization$key } from './__generated__/EditOrganizationAgentFragment_organization.graphql';
import { EditOrganizationAgentQuery } from './__generated__/EditOrganizationAgentQuery.graphql';
import { EditOrganizationAgentMutation } from './__generated__/EditOrganizationAgentMutation.graphql';

function EditOrganizationAgent() {
    const context = useOutletContext<EditOrganizationAgentFragment_organization$key>();
    const agentId = useParams().agentId as string;
    const navigate = useNavigate();

    const organization = useFragment<EditOrganizationAgentFragment_organization$key>(
        graphql`
            fragment EditOrganizationAgentFragment_organization on Organization
            {
                id
                name
            }
        `, context
    );

    const queryData = useLazyLoadQuery<EditOrganizationAgentQuery>(graphql`
        query EditOrganizationAgentQuery($id: String!) {
            node(id: $id) {
                ... on Agent {
                    id
                    name
                    description
                    disabled
                    tags
                    runUntaggedJobs
                }
            }
        }
    `, { id: agentId });

    const [commit, isInFlight] = useMutation<EditOrganizationAgentMutation>(graphql`
        mutation EditOrganizationAgentMutation($input: UpdateAgentInput!) {
            updateAgent(input: $input) {
                agent {
                    ...AgentDetailsFragment_agent
                }
                problems {
                    message
                    field
                    type
                }
            }
        }
    `);

    const agent = queryData.node as any;

    const [error, setError] = useState<MutationError>();
    const [formData, setFormData] = useState<FormData>({
        name: agent.name,
        description: agent.description,
        disabled: agent.disabled,
        runUntaggedJobs: agent.runUntaggedJobs,
        tags: agent.tags
    });

    const onUpdate = () => {
        if (formData) {
            commit({
                variables: {
                    input: {
                        id: agent.id,
                        description: formData.description,
                        disabled: formData.disabled,
                        runUntaggedJobs: formData.runUntaggedJobs,
                        tags: formData.tags
                    }
                },
                onCompleted: data => {
                    if (data.updateAgent.problems.length) {
                        setError({
                            severity: 'warning',
                            message: data.updateAgent.problems.map((problem: any) => problem.message).join('; ')
                        });
                    } else if (!data.updateAgent.agent) {
                        setError({
                            severity: 'error',
                            message: "Unexpected error occurred"
                        });
                    } else {
                        navigate(`../`);
                    }
                },
                onError: error => {
                    setError({
                        severity: 'error',
                        message: `Unexpected error occurred: ${error.message}`
                    });
                }
            });
        }
    };

    return (
        <Box>
            <OrganizationBreadcrumbs
                orgName={organization.name}
                childRoutes={[
                    { title: "agents", path: 'agents' },
                    { title: formData.name, path: agent.id },
                    { title: "edit", path: 'edit' },
                ]}
            />
            <Typography variant="h5">Edit Agent</Typography>
            <AgentForm
                editMode
                data={formData}
                onChange={(data: FormData) => setFormData(data)}
                error={error}
            />
            <Divider sx={{ opacity: 0.6 }}/>
            <Box mt={2}>
                <LoadingButton
                    loading={isInFlight}
                    variant="outlined"
                    color="primary"
                    sx={{ mr: 2 }}
                    onClick={onUpdate}>
                    Update Agent
                </LoadingButton>
                <Button component={RouterLink} color="inherit" to={-1 as any}>Cancel</Button>
            </Box>
        </Box>
    );
}

export default EditOrganizationAgent
