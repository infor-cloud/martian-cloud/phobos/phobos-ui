import { Box, Typography } from '@mui/material';
import graphql from 'babel-plugin-relay/macro';
import { useFragment, useLazyLoadQuery } from 'react-relay/hooks';
import {  useOutletContext, useParams } from 'react-router-dom';
import AgentDetails from '../../agents/AgentDetails';
import OrganizationBreadcrumbs from '../OrganizationBreadcrumbs';
import { OrganizationAgentDetailsQuery } from './__generated__/OrganizationAgentDetailsQuery.graphql';
import { OrganizationAgentDetailsFragment_organization$key } from './__generated__/OrganizationAgentDetailsFragment_organization.graphql';

function OrganizationAgentDetails() {
    const context = useOutletContext<OrganizationAgentDetailsFragment_organization$key>();
    const agentId = useParams().agentId as string;

    const organization = useFragment<OrganizationAgentDetailsFragment_organization$key>(graphql`
        fragment OrganizationAgentDetailsFragment_organization on Organization
        {
            id
            name
        }
    `, context);

    const data = useLazyLoadQuery<OrganizationAgentDetailsQuery>(graphql`
        query OrganizationAgentDetailsQuery($id: String!) {
            node(id: $id) {
                ... on Agent {
                    id
                    name
                    ...AgentDetailsFragment_agent
                }
            }
        }`, { id: agentId }, { fetchPolicy: 'store-and-network' });

    const agent = data.node as any;

    if (data.node) {
        return (
            <Box>
                <OrganizationBreadcrumbs
                    orgName={organization.name}
                    childRoutes={[
                        { title: "agents", path: "agents" },
                        { title: agent.name, path: agent.id }
                    ]}
                />
                <AgentDetails fragmentRef={data.node} orgId={organization.id} />
            </Box>
        );
    } else {
        return <Box display="flex" justifyContent="center" mt={4}>
            <Typography color="textSecondary">Agent with ID {agentId} not found</Typography>
        </Box>;
    }
}

export default OrganizationAgentDetails
