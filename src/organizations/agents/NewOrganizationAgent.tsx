import { useState } from 'react';
import { Box, Button, Divider, Typography } from '@mui/material';
import LoadingButton from '@mui/lab/LoadingButton';
import graphql from 'babel-plugin-relay/macro';
import { useFragment, useMutation } from 'react-relay/hooks';
import { Link as RouterLink, useNavigate, useOutletContext } from 'react-router-dom';
import { MutationError } from '../../common/error';
import OrganizationBreadcrumbs from '../OrganizationBreadcrumbs';
import AgentForm, { FormData } from '../../agents/AgentForm';
import { GetConnections } from './OrganizationAgents';
import { NewOrganizationAgentFragment_organization$key } from './__generated__/NewOrganizationAgentFragment_organization.graphql';
import { NewOrganizationAgentMutation } from './__generated__/NewOrganizationAgentMutation.graphql';

function NewOrganizationAgent() {
    const navigate = useNavigate();
    const context = useOutletContext<NewOrganizationAgentFragment_organization$key>();

    const organization = useFragment<NewOrganizationAgentFragment_organization$key>(graphql`
        fragment NewOrganizationAgentFragment_organization on Organization
        {
            id
            name
        }
    `, context);

    const [commit, isInFlight] = useMutation<NewOrganizationAgentMutation>(graphql`
        mutation NewOrganizationAgentMutation($input: CreateAgentInput!, $connections: [ID!]!) {
            createAgent(input: $input) {
                # Use @prependNode to add the node to the connection
                agent  @prependNode(connections: $connections, edgeTypeName: "AgentEdge") {
                    id
                    ...AgentDetailsFragment_agent
                }
                problems {
                    message
                    field
                    type
                }
            }
        }
    `);

    const [error, setError] = useState<MutationError>()
    const [formData, setFormData] = useState<FormData>({
        name: '',
        description: '',
        disabled: false,
        runUntaggedJobs: true,
        tags: []
    });

    const onSave = () => {
        commit({
            variables: {
                input: {
                    name: formData.name,
                    description: formData.description,
                    organizationId: organization.id,
                    type: 'ORGANIZATION',
                    disabled: formData.disabled,
                    runUntaggedJobs: formData.runUntaggedJobs,
                    tags: formData.tags
                },
                connections: GetConnections(organization.id)
            },
            onCompleted: data => {
                if (data.createAgent.problems.length) {
                    setError({
                        severity: 'warning',
                        message: data.createAgent.problems.map((problem: any) => problem.message).join('; ')
                    });
                } else if (!data.createAgent.agent) {
                    setError({
                        severity: 'error',
                        message: "Unexpected error occurred"
                    });
                } else {
                    navigate(`../${data.createAgent.agent.id}`);
                }
            },
            onError: error => {
                setError({
                    severity: 'error',
                    message: `Unexpected error occurred: ${error.message}`
                });
            }
        });
    };

    return (
        <Box>
            <OrganizationBreadcrumbs
                orgName={organization.name}
                childRoutes={[
                    { title: "agents", path: 'agents' },
                    { title: "new", path: 'new' },
                ]}
            />
            <Typography variant="h5">New Agent</Typography>
            <AgentForm
                data={formData}
                onChange={(data: FormData) => setFormData(data)}
                error={error}
            />
            <Divider sx={{ opacity: 0.6 }} />
            <Box mt={2}>
                <LoadingButton
                    disabled={!formData.name}
                    loading={isInFlight}
                    variant="outlined"
                    color="primary"
                    sx={{ mr: 2 }}
                    onClick={onSave}
                >
                    Create Agent
                </LoadingButton>
                <Button component={RouterLink} color="inherit" to={-1 as any}>Cancel</Button>
            </Box>
        </Box>
    );
}

export default NewOrganizationAgent
