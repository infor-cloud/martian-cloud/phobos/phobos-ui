import { Avatar, Box, Button, Stack, Typography, useTheme } from "@mui/material";
import { purple } from "@mui/material/colors";
import graphql from 'babel-plugin-relay/macro';
import { useFragment } from "react-relay/hooks";
import { Link as RouterLink, useOutletContext } from "react-router-dom";
import ProjectList from "./projects/ProjectList";
import PRNButton from "../common/PRNButton";
import OrganizationBreadcrumbs from "./OrganizationBreadcrumbs";
import { OrganizationDetailsIndexFragment_organization$key } from "./__generated__/OrganizationDetailsIndexFragment_organization.graphql";

export function OrganizationDetailsIndex() {
    const theme = useTheme();
    const context = useOutletContext<OrganizationDetailsIndexFragment_organization$key>();

    const data = useFragment(graphql`
        fragment OrganizationDetailsIndexFragment_organization on Organization
        {
            name
            description
            metadata {
                prn
            }
        }
    `, context);

    return (
        <Box>
            <OrganizationBreadcrumbs
                orgName={data.name}
                childRoutes={[
                    { title: "projects", path: 'projects' }
                ]}
            />
            <Box sx={{
                display: 'flex',
                flexDirection: 'row',
                justifyContent: 'space-between',
                [theme.breakpoints.down('sm')]: {
                    flexDirection: 'column',
                    alignItems: 'flex-start',
                    '& > *': { marginBottom: 2 },
                }
            }}>
                <Box display="flex" marginBottom={4} alignItems="center">
                    <Avatar sx={{ width: 56, height: 56, marginRight: 2, bgcolor: purple[300] }} variant="rounded">{data.name[0].toUpperCase()}</Avatar>
                    <Stack>
                        <Typography noWrap variant="h5" sx={{ maxWidth: 400, fontWeight: "bold" }}>{data.name}</Typography>
                        <Typography color="textSecondary" variant="subtitle2">{`${data.description.slice(0, 50)}${data.description.length > 50 ? '...' : ''}`}</Typography>
                    </Stack>
                </Box>
                <Stack direction="row" spacing={1}>
                    <PRNButton prn={data.metadata.prn} />
                    <Box>
                        <Button
                            sx={{ minWidth: 150 }}
                            variant="outlined"
                            component={RouterLink}
                            to="new"
                        >
                            New Project
                        </Button>
                    </Box>
                </Stack>
            </Box>
            <ProjectList />
        </Box>
    );
}

export default OrganizationDetailsIndex
