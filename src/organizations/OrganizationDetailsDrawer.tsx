import { Avatar, Box, List, ListItem, ListItemAvatar, ListItemButton, ListItemIcon, ListItemText, styled, Toolbar, Typography, useMediaQuery, useTheme } from '@mui/material';
import { purple } from "@mui/material/colors";
import MuiDrawer, { DrawerProps } from '@mui/material/Drawer';
import { useLocation, useNavigate } from 'react-router-dom';
import {
    ActivitiesIcon,
    AgentIcon,
    ApprovalRuleIcon,
    MemberIcon,
    MetricIcon,
    ProjectIcon,
    ReleaseLifecycleIcon,
    ServiceAccountIcon,
    SettingsIcon,
    VCSProviderIcon
} from '../common/Icons';

const LIST_ITEMS = [
    { route: 'activity', label: 'Activity', icon: <ActivitiesIcon /> },
    { route: 'projects', label: 'Projects', icon: <ProjectIcon /> },
    { route: 'release_lifecycles', label: 'Release Lifecycles', icon: <ReleaseLifecycleIcon /> },
    { route: 'approval_rules', label: 'Approval Rules', icon: <ApprovalRuleIcon /> },
    { route: 'vcs_providers', label: 'VCS Providers', icon: <VCSProviderIcon /> },
    { route: 'insights', label: 'Insights', icon: <MetricIcon /> },
    { route: 'service_accounts', label: 'Service Accounts', icon: <ServiceAccountIcon /> },
    { route: 'members', label: 'Members', icon: <MemberIcon /> },
    { route: 'agents', label: 'Agents', icon: <AgentIcon /> },
    { route: 'settings', label: 'Settings', icon: <SettingsIcon /> },
]

const DRAWER_WIDTH = 240;

const Drawer = styled(MuiDrawer)<DrawerProps>(({ theme }) => ({
    flexShrink: 0,
    overflowX: 'hidden',
    [`& .MuiDrawer-paper`]: {
        overflowX: 'hidden',
        width: `calc(${theme.spacing(7)} + 1px)`,
        boxSizing: 'border-box'
    },
    width: `calc(${theme.spacing(7)} + 1px)`,
    [theme.breakpoints.up('md')]: {
        width: DRAWER_WIDTH,
        [`& .MuiDrawer-paper`]: {
            width: DRAWER_WIDTH
        },
    }
}));

interface Props {
    name: string
}

function OrganizationDetailsDrawer({ name }: Props) {
    const theme = useTheme();
    const fullSize = useMediaQuery(theme.breakpoints.up('md'));
    const navigate = useNavigate();
    const location = useLocation();
    const route = location.pathname as string;

    return (
        <Drawer
            variant="permanent"
        >
            <Toolbar />
            <Box>
                <List>
                    {fullSize && <ListItem dense>
                        <Typography variant="subtitle2" color="textSecondary">Organization</Typography>
                    </ListItem>}
                    <ListItemButton
                        onClick={() => navigate(``)}>
                        <ListItemAvatar>
                            <Avatar sx={{ width: 24, height: 24, bgcolor: purple[300] }} variant="rounded">{name[0].toUpperCase()}</Avatar>
                        </ListItemAvatar>
                        {fullSize && <ListItemText sx={{ wordBreak: 'break-word' }} primary={name} />}
                    </ListItemButton>
                    {LIST_ITEMS.map(item => (
                        <ListItemButton
                            key={item.route}
                            selected={route.includes(item.route)}
                            onClick={() => navigate(`-/${item.route}`)}
                        >
                            <ListItemIcon>{item.icon}</ListItemIcon>
                            <ListItemText>{item.label}</ListItemText>
                        </ListItemButton>)
                    )}
                </List>
            </Box>
        </Drawer>
    );
}

export default OrganizationDetailsDrawer
