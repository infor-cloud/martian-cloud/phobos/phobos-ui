/**
 * @generated SignedSource<<98172309816d58e2a135b15c52e72906>>
 * @lightSyntaxTransform
 * @nogrep
 */

/* tslint:disable */
/* eslint-disable */
// @ts-nocheck

import { ConcreteRequest, Query } from 'relay-runtime';
export type OrganizationTeamContainerQuery$variables = {
  name: string;
};
export type OrganizationTeamContainerQuery$data = {
  readonly team: {
    readonly name: string;
  } | null;
};
export type OrganizationTeamContainerQuery = {
  response: OrganizationTeamContainerQuery$data;
  variables: OrganizationTeamContainerQuery$variables;
};

const node: ConcreteRequest = (function(){
var v0 = [
  {
    "defaultValue": null,
    "kind": "LocalArgument",
    "name": "name"
  }
],
v1 = [
  {
    "kind": "Variable",
    "name": "name",
    "variableName": "name"
  }
],
v2 = {
  "alias": null,
  "args": null,
  "kind": "ScalarField",
  "name": "name",
  "storageKey": null
};
return {
  "fragment": {
    "argumentDefinitions": (v0/*: any*/),
    "kind": "Fragment",
    "metadata": null,
    "name": "OrganizationTeamContainerQuery",
    "selections": [
      {
        "alias": null,
        "args": (v1/*: any*/),
        "concreteType": "Team",
        "kind": "LinkedField",
        "name": "team",
        "plural": false,
        "selections": [
          (v2/*: any*/)
        ],
        "storageKey": null
      }
    ],
    "type": "Query",
    "abstractKey": null
  },
  "kind": "Request",
  "operation": {
    "argumentDefinitions": (v0/*: any*/),
    "kind": "Operation",
    "name": "OrganizationTeamContainerQuery",
    "selections": [
      {
        "alias": null,
        "args": (v1/*: any*/),
        "concreteType": "Team",
        "kind": "LinkedField",
        "name": "team",
        "plural": false,
        "selections": [
          (v2/*: any*/),
          {
            "alias": null,
            "args": null,
            "kind": "ScalarField",
            "name": "id",
            "storageKey": null
          }
        ],
        "storageKey": null
      }
    ]
  },
  "params": {
    "cacheID": "b7198b34e2fa0a6b1096e4a9e20cafb1",
    "id": null,
    "metadata": {},
    "name": "OrganizationTeamContainerQuery",
    "operationKind": "query",
    "text": "query OrganizationTeamContainerQuery(\n  $name: String!\n) {\n  team(name: $name) {\n    name\n    id\n  }\n}\n"
  }
};
})();

(node as any).hash = "ddb40d3c54a11e871b25f06fbc26ba38";

export default node;
