/**
 * @generated SignedSource<<061867ee53203aebf0a38ee48c8f9189>>
 * @lightSyntaxTransform
 * @nogrep
 */

/* tslint:disable */
/* eslint-disable */
// @ts-nocheck

import { ReaderFragment, RefetchableFragment } from 'relay-runtime';
import { FragmentRefs } from "relay-runtime";
export type OrganizationMembershipsPaginationFragment_memberships$data = {
  readonly id: string;
  readonly memberships: {
    readonly edges: ReadonlyArray<{
      readonly node: {
        readonly id: string;
        readonly member: {
          readonly __typename: "ServiceAccount";
          readonly name: string;
        } | {
          readonly __typename: "Team";
          readonly name: string;
        } | {
          readonly __typename: "User";
          readonly email: string;
          readonly username: string;
        } | {
          // This will never be '%other', but we need some
          // value in case none of the concrete values match.
          readonly __typename: "%other";
        } | null;
        readonly " $fragmentSpreads": FragmentRefs<"MembershipListFragment_memberships">;
      } | null;
    } | null> | null;
  };
  readonly name: string;
  readonly " $fragmentType": "OrganizationMembershipsPaginationFragment_memberships";
};
export type OrganizationMembershipsPaginationFragment_memberships$key = {
  readonly " $data"?: OrganizationMembershipsPaginationFragment_memberships$data;
  readonly " $fragmentSpreads": FragmentRefs<"OrganizationMembershipsPaginationFragment_memberships">;
};

const node: ReaderFragment = (function(){
var v0 = [
  "memberships"
],
v1 = {
  "alias": null,
  "args": null,
  "kind": "ScalarField",
  "name": "id",
  "storageKey": null
},
v2 = {
  "alias": null,
  "args": null,
  "kind": "ScalarField",
  "name": "name",
  "storageKey": null
},
v3 = {
  "alias": null,
  "args": null,
  "kind": "ScalarField",
  "name": "__typename",
  "storageKey": null
},
v4 = [
  (v2/*: any*/)
];
return {
  "argumentDefinitions": [
    {
      "kind": "RootArgument",
      "name": "after"
    },
    {
      "kind": "RootArgument",
      "name": "first"
    }
  ],
  "kind": "Fragment",
  "metadata": {
    "connection": [
      {
        "count": "first",
        "cursor": "after",
        "direction": "forward",
        "path": (v0/*: any*/)
      }
    ],
    "refetch": {
      "connection": {
        "forward": {
          "count": "first",
          "cursor": "after"
        },
        "backward": null,
        "path": (v0/*: any*/)
      },
      "fragmentPathInResult": [
        "node"
      ],
      "operation": require('./OrganizationMembershipsPaginationQuery.graphql'),
      "identifierField": "id"
    }
  },
  "name": "OrganizationMembershipsPaginationFragment_memberships",
  "selections": [
    (v1/*: any*/),
    (v2/*: any*/),
    {
      "alias": "memberships",
      "args": null,
      "concreteType": "MembershipConnection",
      "kind": "LinkedField",
      "name": "__OrganizationMemberships_memberships_connection",
      "plural": false,
      "selections": [
        {
          "alias": null,
          "args": null,
          "concreteType": "MembershipEdge",
          "kind": "LinkedField",
          "name": "edges",
          "plural": true,
          "selections": [
            {
              "alias": null,
              "args": null,
              "concreteType": "Membership",
              "kind": "LinkedField",
              "name": "node",
              "plural": false,
              "selections": [
                (v1/*: any*/),
                {
                  "alias": null,
                  "args": null,
                  "concreteType": null,
                  "kind": "LinkedField",
                  "name": "member",
                  "plural": false,
                  "selections": [
                    (v3/*: any*/),
                    {
                      "kind": "InlineFragment",
                      "selections": [
                        {
                          "alias": null,
                          "args": null,
                          "kind": "ScalarField",
                          "name": "username",
                          "storageKey": null
                        },
                        {
                          "alias": null,
                          "args": null,
                          "kind": "ScalarField",
                          "name": "email",
                          "storageKey": null
                        }
                      ],
                      "type": "User",
                      "abstractKey": null
                    },
                    {
                      "kind": "InlineFragment",
                      "selections": (v4/*: any*/),
                      "type": "Team",
                      "abstractKey": null
                    },
                    {
                      "kind": "InlineFragment",
                      "selections": (v4/*: any*/),
                      "type": "ServiceAccount",
                      "abstractKey": null
                    }
                  ],
                  "storageKey": null
                },
                {
                  "args": null,
                  "kind": "FragmentSpread",
                  "name": "MembershipListFragment_memberships"
                },
                (v3/*: any*/)
              ],
              "storageKey": null
            },
            {
              "alias": null,
              "args": null,
              "kind": "ScalarField",
              "name": "cursor",
              "storageKey": null
            }
          ],
          "storageKey": null
        },
        {
          "alias": null,
          "args": null,
          "concreteType": "PageInfo",
          "kind": "LinkedField",
          "name": "pageInfo",
          "plural": false,
          "selections": [
            {
              "alias": null,
              "args": null,
              "kind": "ScalarField",
              "name": "endCursor",
              "storageKey": null
            },
            {
              "alias": null,
              "args": null,
              "kind": "ScalarField",
              "name": "hasNextPage",
              "storageKey": null
            }
          ],
          "storageKey": null
        }
      ],
      "storageKey": null
    }
  ],
  "type": "Organization",
  "abstractKey": null
};
})();

(node as any).hash = "67d27ccb527e213e7bb6bf3d8d35b5c8";

export default node;
