/**
 * @generated SignedSource<<d880b94ec776620c08aa3abac7c14173>>
 * @lightSyntaxTransform
 * @nogrep
 */

/* tslint:disable */
/* eslint-disable */
// @ts-nocheck

import { Fragment, ReaderFragment } from 'relay-runtime';
import { FragmentRefs } from "relay-runtime";
export type NewOrganizationMembershipFragment_memberships$data = {
  readonly id: string;
  readonly name: string;
  readonly " $fragmentType": "NewOrganizationMembershipFragment_memberships";
};
export type NewOrganizationMembershipFragment_memberships$key = {
  readonly " $data"?: NewOrganizationMembershipFragment_memberships$data;
  readonly " $fragmentSpreads": FragmentRefs<"NewOrganizationMembershipFragment_memberships">;
};

const node: ReaderFragment = {
  "argumentDefinitions": [],
  "kind": "Fragment",
  "metadata": null,
  "name": "NewOrganizationMembershipFragment_memberships",
  "selections": [
    {
      "alias": null,
      "args": null,
      "kind": "ScalarField",
      "name": "id",
      "storageKey": null
    },
    {
      "alias": null,
      "args": null,
      "kind": "ScalarField",
      "name": "name",
      "storageKey": null
    }
  ],
  "type": "Organization",
  "abstractKey": null
};

(node as any).hash = "39761e80d7e79f3dde1350e163532f9e";

export default node;
