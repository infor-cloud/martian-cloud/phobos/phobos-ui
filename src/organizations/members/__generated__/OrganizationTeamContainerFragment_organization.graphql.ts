/**
 * @generated SignedSource<<725efc46a12d1d0e35a73b85aadbc7ed>>
 * @lightSyntaxTransform
 * @nogrep
 */

/* tslint:disable */
/* eslint-disable */
// @ts-nocheck

import { Fragment, ReaderFragment } from 'relay-runtime';
import { FragmentRefs } from "relay-runtime";
export type OrganizationTeamContainerFragment_organization$data = {
  readonly name: string;
  readonly " $fragmentType": "OrganizationTeamContainerFragment_organization";
};
export type OrganizationTeamContainerFragment_organization$key = {
  readonly " $data"?: OrganizationTeamContainerFragment_organization$data;
  readonly " $fragmentSpreads": FragmentRefs<"OrganizationTeamContainerFragment_organization">;
};

const node: ReaderFragment = {
  "argumentDefinitions": [],
  "kind": "Fragment",
  "metadata": null,
  "name": "OrganizationTeamContainerFragment_organization",
  "selections": [
    {
      "alias": null,
      "args": null,
      "kind": "ScalarField",
      "name": "name",
      "storageKey": null
    }
  ],
  "type": "Organization",
  "abstractKey": null
};

(node as any).hash = "2468e6f5fe0d765fef012c2822ac5292";

export default node;
