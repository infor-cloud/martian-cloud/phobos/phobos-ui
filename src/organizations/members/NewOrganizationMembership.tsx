import { useState } from 'react';
import { Box } from '@mui/material';
import { MutationError } from '../../common/error';
import graphql from 'babel-plugin-relay/macro';
import { useFragment, useMutation } from 'react-relay/hooks';
import { useNavigate, useOutletContext } from 'react-router-dom';
import OrganizationBreadcrumbs from '../OrganizationBreadcrumbs';
import { NewOrganizationMembershipFragment_memberships$key } from './__generated__/NewOrganizationMembershipFragment_memberships.graphql';
import NewMembership from '../../members/NewMembership';
import { NewOrganizationMembershipCreateMembershipMutation } from './__generated__/NewOrganizationMembershipCreateMembershipMutation.graphql';
import { GetOrgConnections } from './OrganizationMemberships';

function NewOrganizationMembership() {
    const context = useOutletContext<NewOrganizationMembershipFragment_memberships$key>();
    const navigate = useNavigate();
    const [error, setError] = useState<MutationError>();

    const data = useFragment<NewOrganizationMembershipFragment_memberships$key>(
        graphql`
        fragment NewOrganizationMembershipFragment_memberships on Organization
        {
            id
            name
        }
    `, context);

    const [commit, isInFlight] = useMutation<NewOrganizationMembershipCreateMembershipMutation>(graphql`
        mutation NewOrganizationMembershipCreateMembershipMutation($input: CreateMembershipInput!, $connections: [ID!]!) {
            createMembership(input: $input) {
                membership @prependNode(connections: $connections, edgeTypeName: "MembershipEdge") {
                    ...MembershipListItemFragment_membership
                }
                problems {
                    message
                    field
                    type
                }
            }
        }
    `);

    const onCreate = (member: string | undefined, role: string | undefined, memberType: string) => {
        if (member && role) {
            const input = {
                organizationId: data.id,
                scope: 'ORGANIZATION',
                role
            } as any;

            if (memberType === 'user') {
                input.username = member;
            } else if (memberType === 'team') {
                input.teamName = member;
            } else if (memberType === 'serviceAccount') {
                input.serviceAccountId = member;
            }
            else {
                throw new Error(`Invalid member type ${memberType}`);
            }

            commit({
                variables: {
                    input,
                    connections: GetOrgConnections(data.id)
                },
                onCompleted: data => {
                    if (data.createMembership.problems.length) {
                        setError({
                            severity: 'warning',
                            message: data.createMembership.problems.map(problem => problem.message).join('; ')
                        });
                    } else {
                        navigate(`..`);
                    }
                },
                onError: error => {
                    setError({
                        severity: 'error',
                        message: `Unexpected Error Occurred: ${error.message}`
                    });
                }
            });
        }
    };

    return (
        <Box>
            <OrganizationBreadcrumbs
                orgName={data.name}
                childRoutes={[
                    { title: "members", path: 'members' },
                    { title: "new", path: 'new' }
                ]}
            />
            <NewMembership
                orgId={data.id}
                error={error}
                onCreate={onCreate}
                loading={isInFlight}
            />
        </Box>
    );
}

export default NewOrganizationMembership;
