import { useState } from 'react';
import { Box, Button, Typography, useTheme } from '@mui/material';
import OrganizationBreadcrumbs from '../OrganizationBreadcrumbs';
import { ConnectionHandler, useMutation, usePaginationFragment } from 'react-relay/hooks';
import { useSnackbar } from 'notistack';
import { Link as RouterLink, useOutletContext } from 'react-router-dom';
import graphql from 'babel-plugin-relay/macro';
import { OrganizationMembershipsPaginationQuery } from './__generated__/OrganizationMembershipsPaginationQuery.graphql';
import { OrganizationMembershipsFragment_memberships$key } from './__generated__/OrganizationMembershipsFragment_memberships.graphql';
import MembershipList from '../../members/MembershipList';
import MembershipDeleteConfirmationDialog from '../../members/MembershipDeleteConfirmationDialog';
import { OrganizationMembershipsDeleteMembershipMutation } from './__generated__/OrganizationMembershipsDeleteMembershipMutation.graphql';

export function GetOrgConnections(orgId: string): [string] {
    const connectionId = ConnectionHandler.getConnectionID(
        orgId,
        'OrganizationMemberships_memberships'
    );
    return [connectionId];
}

function OrganizationMemberships() {
    const theme = useTheme();
    const { enqueueSnackbar } = useSnackbar();
    const context = useOutletContext<OrganizationMembershipsFragment_memberships$key>();

    const { data, loadNext, hasNext } = usePaginationFragment<OrganizationMembershipsPaginationQuery, OrganizationMembershipsFragment_memberships$key>(
        graphql`
            fragment OrganizationMembershipsFragment_memberships on Organization
            @refetchable(queryName: "OrganizationMembershipsPaginationQuery")
             {
                id
                name
                memberships(
                    first: $first
                    last: $last
                    after: $after
                    before: $before
                ) @connection(key: "OrganizationMemberships_memberships") {
                    edges{
                        node {
                            id
                        }
                    }
                    ...MembershipListFragment_memberships
                }
            }
        `, context);

    const [commitDeleteMembership, deleteInFlight] = useMutation<OrganizationMembershipsDeleteMembershipMutation>(graphql`
        mutation OrganizationMembershipsDeleteMembershipMutation($input: DeleteMembershipInput!, $connections: [ID!]!) {
            deleteMembership(input: $input) {
                membership {
                    id @deleteEdge(connections: $connections)
                }
                problems {
                    message
                    field
                    type
                }
            }
        }
    `);

    const [membershipToDelete, setMembershipToDelete] = useState<any>(null);

    const onShowDeleteConfirmationDialog = (membership: any) => {
        setMembershipToDelete(membership);
    };

    const onDelete = (confirm?: boolean) => {
        if (confirm && membershipToDelete) {
            commitDeleteMembership({
                variables: {
                    input: {
                        id: membershipToDelete.id
                    },
                    connections: GetOrgConnections(data.id)
                },
                onCompleted: data => {
                    if (data.deleteMembership.problems.length) {
                        enqueueSnackbar(data.deleteMembership.problems.map(problem => problem.message).join('; '), { variant: 'warning' });
                    }
                    setMembershipToDelete(null);
                },
                onError: error => {
                    enqueueSnackbar(`Unexpected error occurred: ${error.message}`, { variant: 'error' });
                    setMembershipToDelete(null);
                }
            });
        } else {
            setMembershipToDelete(null);
        }
    };

    return (
        <Box>
            <OrganizationBreadcrumbs
                orgName={data.name}
                childRoutes={[
                    { title: "members", path: 'members' }
                ]}
            />
            <Box sx={{
                display: 'flex',
                flexDirection: 'row',
                justifyContent: 'space-between',
                marginBottom: 2,
                [theme.breakpoints.down('md')]: {
                    flexDirection: 'column',
                    alignItems: 'flex-start',
                    '& > *:not(:last-child)': { marginBottom: 2 },
                }
            }}>
                <Box>
                    <Typography variant="h5" gutterBottom>Members</Typography>
                    <Typography variant="body2">
                        A member is associated with a role and can be a user, team, or service account.
                    </Typography>
                </Box>
                <Box>
                    <Button component={RouterLink} variant="outlined" color="primary" to="new">Add Member</Button>
                </Box>
            </Box>
            <MembershipList
                hideSourceCol
                orgName={data.name}
                hasNext={hasNext}
                loadNext={loadNext}
                fragmentRef={data.memberships}
                onDelete={onShowDeleteConfirmationDialog}
            />
            {membershipToDelete && <MembershipDeleteConfirmationDialog
                membership={membershipToDelete}
                deleteInProgress={deleteInFlight}
                onClose={onDelete}
            />}
        </Box>
    );
}

export default OrganizationMemberships
