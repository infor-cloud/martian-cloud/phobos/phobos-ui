import { Box, CircularProgress, Typography } from '@mui/material';
import graphql from 'babel-plugin-relay/macro';
import { Suspense, useEffect } from 'react';
import { PreloadedQuery, useFragment, usePreloadedQuery, useQueryLoader } from 'react-relay/hooks';
import { Outlet, useParams } from 'react-router-dom';
import OrganizationDetailsDrawer from './OrganizationDetailsDrawer';
import { OrganizationDetailsFragment_organization$key } from './__generated__/OrganizationDetailsFragment_organization.graphql';
import { OrganizationDetailsQuery } from './__generated__/OrganizationDetailsQuery.graphql';

const query = graphql`
    query OrganizationDetailsQuery($id: String!, $first: Int!, $last: Int, $after: String, $before: String) {
        node(id: $id) {
            ...on Organization {
                ...OrganizationDetailsFragment_organization
            }
        }
    }
`;

function OrganizationDetailsEntryPoint() {
    const orgName = useParams().orgName as string;
    const [queryRef, loadQuery] = useQueryLoader<OrganizationDetailsQuery>(query);

    useEffect(() => {
        loadQuery({ id: `prn:organization:${orgName}`, first: 50 }, { fetchPolicy: 'store-and-network' })
    }, [loadQuery, orgName]);

    return queryRef != null ? <OrganizationDetailsContainer queryRef={queryRef} orgName={orgName} /> : null;
}

interface OrganizationDetailsContainerProps {
    orgName: string;
    queryRef: PreloadedQuery<OrganizationDetailsQuery>;
}

function OrganizationDetailsContainer({ orgName, queryRef }: OrganizationDetailsContainerProps) {
    const queryData = usePreloadedQuery<OrganizationDetailsQuery>(query, queryRef);

    return queryData.node ? (
        <OrganizationDetails fragmentRef={queryData.node} />
    ) : (
        <Box display="flex" justifyContent="center" paddingTop={4}>
            <Typography color="textSecondary">Organization with name {orgName} not found</Typography>
        </Box>
    );
}

interface OrganizationDetailsProps {
    fragmentRef: OrganizationDetailsFragment_organization$key;
}

function OrganizationDetails({ fragmentRef }: OrganizationDetailsProps) {

    const data = useFragment<OrganizationDetailsFragment_organization$key>(
        graphql`
        fragment OrganizationDetailsFragment_organization on Organization
        {
            name
            ...OrganizationDetailsIndexFragment_organization
            ...OrganizationSettingsFragment_organization
            ...NewProjectFragment_organization
            ...OrgReleaseLifecyclesFragment_organization
            ...OrgReleaseLifecycleDetailsFragment_organization
            ...NewOrgReleaseLifecycleFragment_organization
            ...EditOrgReleaseLifecycleFragment_organization
            ...OrgServiceAccountsFragment_organization
            ...OrgServiceAccountDetailsFragment_organization
            ...NewOrgServiceAccountFragment_organization
            ...EditOrgServiceAccountFragment_organization
            ...OrganizationMembershipsFragment_memberships
            ...NewOrganizationMembershipFragment_memberships
            ...OrganizationAgentsFragment_agents
            ...NewOrganizationAgentFragment_organization
            ...OrganizationAgentDetailsFragment_organization
            ...EditOrganizationAgentFragment_organization
            ...OrganizationActivityFragment_organization
            ...OrgApprovalRulesFragment_organization
            ...NewOrgApprovalRuleFragment_organization
            ...EditOrgApprovalRuleFragment_organization
            ...OrganizationVCSProvidersFragment_organization
            ...NewOrgVCSProviderFragment_organization
            ...OrgVCSProviderDetailsFragment_organization
            ...EditOrgVCSProviderAuthSettingsFragment_organization
            ...EditOrgVCSProviderFragment_organization
            ...NewOrgEnvironmentRuleFragment_organization
            ...EditOrgEnvironmentRuleFragment_organization
            ...OrganizationInsightsFragment_organization
        }
    `, fragmentRef);

    return (
        <Box display="flex">
            <OrganizationDetailsDrawer name={data.name} />
            <Box component="main" sx={{ flexGrow: 1 }}>
                <Suspense fallback={<Box
                    sx={{
                        width: '100%',
                        height: `calc(100vh - 64px)`,
                        display: 'flex',
                        alignItems: 'center',
                        justifyContent: 'center'
                    }}
                >
                    <CircularProgress />
                </Box>}>
                    <Box maxWidth={1200} margin="auto" padding={2}>
                        <Outlet context={data} />
                    </Box>
                </Suspense>
            </Box>
        </Box>
    );
}

export default OrganizationDetailsEntryPoint
