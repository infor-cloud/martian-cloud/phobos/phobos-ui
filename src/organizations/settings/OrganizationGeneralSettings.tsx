import { useState } from 'react';
import LoadingButton from '@mui/lab/LoadingButton';
import { Box, Collapse } from "@mui/material";
import graphql from "babel-plugin-relay/macro";
import { useSnackbar } from 'notistack';
import GeneralSettingsForm, { FormData } from '../../settings/GeneralSettingsForm';
import SettingsToggleButton from '../../settings/SettingsToggleButton';
import { useFragment, useMutation } from 'react-relay/hooks';
import { MutationError } from '../../common/error';
import { OrganizationGeneralSettingsFragment_organization$key } from './__generated__/OrganizationGeneralSettingsFragment_organization.graphql';
import { OrganizationGeneralSettingsUpdateMutation } from './__generated__/OrganizationGeneralSettingsUpdateMutation.graphql';

interface Props {
    fragmentRef: OrganizationGeneralSettingsFragment_organization$key
}

function OrganizationGeneralSettings({ fragmentRef }: Props) {
    const { enqueueSnackbar } = useSnackbar();
    const [showSettings, setShowSettings] = useState<boolean>(false);

    const data = useFragment<OrganizationGeneralSettingsFragment_organization$key>(
        graphql`
        fragment OrganizationGeneralSettingsFragment_organization on Organization
        {
            id
            name
            description
        }
    `, fragmentRef);

    const [error, setError] = useState<MutationError>()
    const [inputForm, setInputForm] = useState<{ name: string, description: string }>(
        {
            name: data.name,
            description: data.description
        }
    );

    const [commit, isInFlight] = useMutation<OrganizationGeneralSettingsUpdateMutation>(graphql`
        mutation OrganizationGeneralSettingsUpdateMutation($input: UpdateOrganizationInput!) {
            updateOrganization(input: $input) {
                organization {
                    id
                    description
                }
                problems {
                    message
                    field
                    type
                }
            }
        }
    `);

    const onUpdate = () => {
        commit({
            variables: {
                input: {
                    id: data.id,
                    description: inputForm.description.trim()
                }
            },
            onCompleted: (data) => {
                if (data.updateOrganization.problems.length) {
                    setError({
                        severity: 'warning',
                        message: data.updateOrganization.problems.map((problem: any) => problem.message).join('; ')
                    });
                } else if (!data.updateOrganization.organization) {
                    setError({
                        severity: 'error',
                        message: "Unexpected error occurred"
                    });
                } else {
                    enqueueSnackbar('Organization settings updated', { variant: 'success' });
                }
            },
            onError: error => {
                setError({
                    severity: 'error',
                    message: `Unexpected error occurred: ${error.message}`
                });
            }
        });
    };

    return (
        <Box>
            <SettingsToggleButton
                title="General Settings"
                showSettings={showSettings}
                onToggle={() => setShowSettings(!showSettings)}
            />
            <Collapse
                in={showSettings}
                timeout="auto"
                unmountOnExit
            >
                <GeneralSettingsForm
                    editMode
                    error={error}
                    data={inputForm}
                    onChange={(data: FormData) => setInputForm(data)}
                />
                <LoadingButton
                    sx={{ mt: 1 }}
                    size="small"
                    disabled={data.description === inputForm.description}
                    loading={isInFlight}
                    variant="outlined"
                    color="primary"
                    onClick={onUpdate}
                >Save changes
                </LoadingButton>
            </Collapse>
        </Box>
    );
}

export default OrganizationGeneralSettings;
