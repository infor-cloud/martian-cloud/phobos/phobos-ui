import { Box, Divider, styled, Typography } from "@mui/material";
import { useFragment } from "react-relay/hooks";
import graphql from "babel-plugin-relay/macro";
import OrganizationGeneralSettings from "./OrganizationGeneralSettings";
import OrganizationAdvancedSettings from "./OrganizationAdvancedSettings";
import { OrganizationSettingsFragment_organization$key } from "./__generated__/OrganizationSettingsFragment_organization.graphql";
import OrganizationBreadcrumbs from "../OrganizationBreadcrumbs";
import { useOutletContext } from "react-router-dom";
import OrgEnvironmentRules from "./environmentrules/OrgEnvironmentRules";

const StyledDivider = styled(
    Divider
)(() => ({
    margin: "24px 0"
}))

function OrganizationSettings() {
    const context = useOutletContext<OrganizationSettingsFragment_organization$key>();

    const data = useFragment<OrganizationSettingsFragment_organization$key>(
        graphql`
        fragment OrganizationSettingsFragment_organization on Organization
        {
            name
            ...OrganizationGeneralSettingsFragment_organization
            ...OrganizationAdvancedSettingsFragment_organization
            ...OrgEnvironmentRules_organization
        }
    `, context);

    return (
        <Box>
            <OrganizationBreadcrumbs
                orgName={data.name}
                childRoutes={[
                    { title: "settings", path: 'settings' }
                ]}
            />
            <Typography mb={4} variant="h5" gutterBottom>Organization Settings</Typography>
            <StyledDivider />
            <OrganizationGeneralSettings fragmentRef={data} />
            <StyledDivider />
            <OrgEnvironmentRules fragmentRef={data} />
            <StyledDivider />
            <OrganizationAdvancedSettings fragmentRef={data} />
        </Box>
    );
}

export default OrganizationSettings
