/**
 * @generated SignedSource<<c679bc153eb51b27482c2d87d70ad28d>>
 * @lightSyntaxTransform
 * @nogrep
 */

/* tslint:disable */
/* eslint-disable */
// @ts-nocheck

import { Fragment, ReaderFragment } from 'relay-runtime';
import { FragmentRefs } from "relay-runtime";
export type OrganizationAdvancedSettingsFragment_organization$data = {
  readonly id: string;
  readonly name: string;
  readonly " $fragmentType": "OrganizationAdvancedSettingsFragment_organization";
};
export type OrganizationAdvancedSettingsFragment_organization$key = {
  readonly " $data"?: OrganizationAdvancedSettingsFragment_organization$data;
  readonly " $fragmentSpreads": FragmentRefs<"OrganizationAdvancedSettingsFragment_organization">;
};

const node: ReaderFragment = {
  "argumentDefinitions": [],
  "kind": "Fragment",
  "metadata": null,
  "name": "OrganizationAdvancedSettingsFragment_organization",
  "selections": [
    {
      "alias": null,
      "args": null,
      "kind": "ScalarField",
      "name": "id",
      "storageKey": null
    },
    {
      "alias": null,
      "args": null,
      "kind": "ScalarField",
      "name": "name",
      "storageKey": null
    }
  ],
  "type": "Organization",
  "abstractKey": null
};

(node as any).hash = "3a2cc9dd29b573d3a682531b5d5dea7d";

export default node;
