/**
 * @generated SignedSource<<1c85496c5da25ff27cdee8c9f20d20b8>>
 * @lightSyntaxTransform
 * @nogrep
 */

/* tslint:disable */
/* eslint-disable */
// @ts-nocheck

import { Fragment, ReaderFragment } from 'relay-runtime';
import { FragmentRefs } from "relay-runtime";
export type OrganizationSettingsFragment_organization$data = {
  readonly name: string;
  readonly " $fragmentSpreads": FragmentRefs<"OrgEnvironmentRules_organization" | "OrganizationAdvancedSettingsFragment_organization" | "OrganizationGeneralSettingsFragment_organization">;
  readonly " $fragmentType": "OrganizationSettingsFragment_organization";
};
export type OrganizationSettingsFragment_organization$key = {
  readonly " $data"?: OrganizationSettingsFragment_organization$data;
  readonly " $fragmentSpreads": FragmentRefs<"OrganizationSettingsFragment_organization">;
};

const node: ReaderFragment = {
  "argumentDefinitions": [],
  "kind": "Fragment",
  "metadata": null,
  "name": "OrganizationSettingsFragment_organization",
  "selections": [
    {
      "alias": null,
      "args": null,
      "kind": "ScalarField",
      "name": "name",
      "storageKey": null
    },
    {
      "args": null,
      "kind": "FragmentSpread",
      "name": "OrganizationGeneralSettingsFragment_organization"
    },
    {
      "args": null,
      "kind": "FragmentSpread",
      "name": "OrganizationAdvancedSettingsFragment_organization"
    },
    {
      "args": null,
      "kind": "FragmentSpread",
      "name": "OrgEnvironmentRules_organization"
    }
  ],
  "type": "Organization",
  "abstractKey": null
};

(node as any).hash = "be808445b0ab8539a6af05996cf75e33";

export default node;
