/**
 * @generated SignedSource<<ede9d209b3a352d2c543544c540ab8a0>>
 * @lightSyntaxTransform
 * @nogrep
 */

/* tslint:disable */
/* eslint-disable */
// @ts-nocheck

import { Fragment, ReaderFragment } from 'relay-runtime';
import { FragmentRefs } from "relay-runtime";
export type OrganizationGeneralSettingsFragment_organization$data = {
  readonly description: string;
  readonly id: string;
  readonly name: string;
  readonly " $fragmentType": "OrganizationGeneralSettingsFragment_organization";
};
export type OrganizationGeneralSettingsFragment_organization$key = {
  readonly " $data"?: OrganizationGeneralSettingsFragment_organization$data;
  readonly " $fragmentSpreads": FragmentRefs<"OrganizationGeneralSettingsFragment_organization">;
};

const node: ReaderFragment = {
  "argumentDefinitions": [],
  "kind": "Fragment",
  "metadata": null,
  "name": "OrganizationGeneralSettingsFragment_organization",
  "selections": [
    {
      "alias": null,
      "args": null,
      "kind": "ScalarField",
      "name": "id",
      "storageKey": null
    },
    {
      "alias": null,
      "args": null,
      "kind": "ScalarField",
      "name": "name",
      "storageKey": null
    },
    {
      "alias": null,
      "args": null,
      "kind": "ScalarField",
      "name": "description",
      "storageKey": null
    }
  ],
  "type": "Organization",
  "abstractKey": null
};

(node as any).hash = "15766b1f5872e0f28d641e5636aa567b";

export default node;
