import { useState } from 'react';
import { Link as RouterLink } from 'react-router-dom';
import { Box, Button, Collapse } from '@mui/material';
import { ConnectionHandler, useFragment, useLazyLoadQuery, usePaginationFragment } from 'react-relay/hooks';
import graphql from 'babel-plugin-relay/macro';
import SettingsToggleButton from '../../../settings/SettingsToggleButton';
import EnvironmentRuleList from '../../../environmentrules/EnvironmentRuleList';
import { OrgEnvironmentRulesQuery } from './__generated__/OrgEnvironmentRulesQuery.graphql';
import { OrgEnvironmentRules_organization$key } from './__generated__/OrgEnvironmentRules_organization.graphql';
import { OrgEnvironmentRulesPaginationQuery } from './__generated__/OrgEnvironmentRulesPaginationQuery.graphql';
import { OrgEnvironmentRules_environmentRules$key } from './__generated__/OrgEnvironmentRules_environmentRules.graphql';

export function GetConnections(orgId: string): [string] {
    const connectionId = ConnectionHandler.getConnectionID(
        orgId,
        'OrgEnvironmentRules_environmentRules',
        { sort: 'ENVIRONMENT_NAME_ASC' }
    );
    return [connectionId];
}

const query = graphql`
    query OrgEnvironmentRulesQuery($first: Int!, $last: Int, $after: String, $before: String, $orgId: String!) {
        node(id: $orgId) {
            ...on Organization {
                ...OrgEnvironmentRules_environmentRules
            }
        }
    }
`;

interface Props {
    fragmentRef: OrgEnvironmentRules_organization$key
}

function OrgEnvironmentRules({ fragmentRef }: Props) {
    const [showSettings, setShowSettings] = useState<boolean>(false);

    const organization = useFragment<OrgEnvironmentRules_organization$key>(graphql`
        fragment OrgEnvironmentRules_organization on Organization
        {
            id
            name
        }
    `, fragmentRef);

    const queryData = useLazyLoadQuery<OrgEnvironmentRulesQuery>(query, { orgId: organization.id, first: 10 }, { fetchPolicy: 'store-and-network' });

    const { data, loadNext, hasNext } = usePaginationFragment<OrgEnvironmentRulesPaginationQuery, OrgEnvironmentRules_environmentRules$key>(
        graphql`
            fragment OrgEnvironmentRules_environmentRules on Organization
            @refetchable(queryName: "OrgEnvironmentRulesPaginationQuery")
             {
                id
                name
                environmentRules(
                    first: $first
                    last: $last
                    after: $after
                    before: $before
                    sort: ENVIRONMENT_NAME_ASC
                ) @connection(key: "OrgEnvironmentRules_environmentRules") {
                    edges{
                        node {
                            id
                        }
                    }
                    ...EnvironmentRuleListFragment_environmentRules
                }
            }
        `, queryData.node);

    return (
        <Box>
            <SettingsToggleButton
                title="Environments"
                showSettings={showSettings}
                onToggle={() => setShowSettings(!showSettings)}
            />
            <Collapse
                in={showSettings}
                timeout="auto"
                unmountOnExit
            >
                <Box mb={4}>
                    {data?.environmentRules && <EnvironmentRuleList
                        getConnections={() => GetConnections(data.id)}
                        fragmentRef={data.environmentRules}
                        loadNext={loadNext}
                        hasNext={hasNext}
                    />}
                    <Button
                        color="secondary"
                        variant="outlined"
                        component={RouterLink}
                        to={'new_environment_rule'}
                    >
                        Add Rule
                    </Button>
                </Box>
            </Collapse>
        </Box>
    );
}

export default OrgEnvironmentRules;
