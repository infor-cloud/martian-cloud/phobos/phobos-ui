import Box from "@mui/material/Box";
import { useOutletContext } from "react-router-dom";
import OrganizationBreadcrumbs from "../../OrganizationBreadcrumbs";
import { useFragment } from "react-relay/hooks";
import graphql from "babel-plugin-relay/macro";
import { GetConnections } from "./OrgEnvironmentRules";
import NewEnvironmentRule from "../../../environmentrules/NewEnvironmentRule";
import { NewOrgEnvironmentRuleFragment_organization$key } from "./__generated__/NewOrgEnvironmentRuleFragment_organization.graphql";

function NewOrgEnvironmentRule() {
    const context = useOutletContext<NewOrgEnvironmentRuleFragment_organization$key>();

    const organization = useFragment<NewOrgEnvironmentRuleFragment_organization$key>(
        graphql`
            fragment NewOrgEnvironmentRuleFragment_organization on Organization
            {
                id
                name
                environmentNames
            }
        `, context
    );

    return (
        <Box>
            <OrganizationBreadcrumbs
                orgName={organization.name}
                childRoutes={[
                    { title: "settings", path: 'settings' },
                    { title: "new environment rule", path: 'new_environment_rule' },
                ]}
            />
            <NewEnvironmentRule
                orgId={organization.id}
                getConnections={GetConnections}
                environmentNames={organization.environmentNames}
            />
        </Box>
    );
}

export default NewOrgEnvironmentRule;
