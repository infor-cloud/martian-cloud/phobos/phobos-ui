import { Box, Typography } from '@mui/material';
import { useOutletContext, useParams } from 'react-router-dom';
import OrganizationBreadcrumbs from '../../OrganizationBreadcrumbs';
import graphql from 'babel-plugin-relay/macro';
import { useFragment, useLazyLoadQuery } from "react-relay/hooks";
import EditEnvironmentRule from '../../../environmentrules/EditEnvironmentRule';
import { EditOrgEnvironmentRuleFragment_organization$key } from './__generated__/EditOrgEnvironmentRuleFragment_organization.graphql';
import { EditOrgEnvironmentRuleQuery } from './__generated__/EditOrgEnvironmentRuleQuery.graphql';

function EditOrgEnvironmentRule() {
    const environmentRuleId = useParams().environmentRuleId as string;
    const context = useOutletContext<EditOrgEnvironmentRuleFragment_organization$key>();

    const organization = useFragment<EditOrgEnvironmentRuleFragment_organization$key>(
        graphql`
            fragment EditOrgEnvironmentRuleFragment_organization on Organization
            {
                id
                name
            }
        `, context
    );

    const queryData = useLazyLoadQuery<EditOrgEnvironmentRuleQuery>(graphql`
        query EditOrgEnvironmentRuleQuery($id: String!) {
            node(id: $id) {
                ... on EnvironmentRule {
                    id
                    ...EditEnvironmentRuleFragment_environmentRule
                }
            }
        }
    `, { id: environmentRuleId }, { fetchPolicy: 'store-and-network' });

    return queryData.node ? (
        <Box>
            <OrganizationBreadcrumbs
                orgName={organization.name}
                childRoutes={[
                    { title: "settings", path: 'settings' },
                    { title: "edit environment rule", path: 'edit_environment_rule' }
                ]}
            />
            <EditEnvironmentRule
                orgId={organization.id}
                fragmentRef={queryData.node} />
        </Box>
    ) : <Box display="flex" justifyContent="center" mt={4}>
        <Typography color="textSecondary">Environment rule not found</Typography>
    </Box>;
}

export default EditOrgEnvironmentRule;
