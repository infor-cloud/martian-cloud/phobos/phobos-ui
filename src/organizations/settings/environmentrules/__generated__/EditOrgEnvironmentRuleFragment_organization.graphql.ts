/**
 * @generated SignedSource<<5778c673e62a757b10b53e077f697f3f>>
 * @lightSyntaxTransform
 * @nogrep
 */

/* tslint:disable */
/* eslint-disable */
// @ts-nocheck

import { Fragment, ReaderFragment } from 'relay-runtime';
import { FragmentRefs } from "relay-runtime";
export type EditOrgEnvironmentRuleFragment_organization$data = {
  readonly id: string;
  readonly name: string;
  readonly " $fragmentType": "EditOrgEnvironmentRuleFragment_organization";
};
export type EditOrgEnvironmentRuleFragment_organization$key = {
  readonly " $data"?: EditOrgEnvironmentRuleFragment_organization$data;
  readonly " $fragmentSpreads": FragmentRefs<"EditOrgEnvironmentRuleFragment_organization">;
};

const node: ReaderFragment = {
  "argumentDefinitions": [],
  "kind": "Fragment",
  "metadata": null,
  "name": "EditOrgEnvironmentRuleFragment_organization",
  "selections": [
    {
      "alias": null,
      "args": null,
      "kind": "ScalarField",
      "name": "id",
      "storageKey": null
    },
    {
      "alias": null,
      "args": null,
      "kind": "ScalarField",
      "name": "name",
      "storageKey": null
    }
  ],
  "type": "Organization",
  "abstractKey": null
};

(node as any).hash = "62bceb3a13fb03e07d2801d55e6aebb9";

export default node;
