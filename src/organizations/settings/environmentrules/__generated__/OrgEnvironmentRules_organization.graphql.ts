/**
 * @generated SignedSource<<943a47f2f2e955076692795320d0dc89>>
 * @lightSyntaxTransform
 * @nogrep
 */

/* tslint:disable */
/* eslint-disable */
// @ts-nocheck

import { Fragment, ReaderFragment } from 'relay-runtime';
import { FragmentRefs } from "relay-runtime";
export type OrgEnvironmentRules_organization$data = {
  readonly id: string;
  readonly name: string;
  readonly " $fragmentType": "OrgEnvironmentRules_organization";
};
export type OrgEnvironmentRules_organization$key = {
  readonly " $data"?: OrgEnvironmentRules_organization$data;
  readonly " $fragmentSpreads": FragmentRefs<"OrgEnvironmentRules_organization">;
};

const node: ReaderFragment = {
  "argumentDefinitions": [],
  "kind": "Fragment",
  "metadata": null,
  "name": "OrgEnvironmentRules_organization",
  "selections": [
    {
      "alias": null,
      "args": null,
      "kind": "ScalarField",
      "name": "id",
      "storageKey": null
    },
    {
      "alias": null,
      "args": null,
      "kind": "ScalarField",
      "name": "name",
      "storageKey": null
    }
  ],
  "type": "Organization",
  "abstractKey": null
};

(node as any).hash = "7de2b7936007a80be0d00e044262c28b";

export default node;
