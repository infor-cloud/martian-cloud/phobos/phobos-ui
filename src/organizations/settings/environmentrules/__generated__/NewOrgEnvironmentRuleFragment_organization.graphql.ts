/**
 * @generated SignedSource<<aa7914aea4e6f8a55d8ee86a1522df06>>
 * @lightSyntaxTransform
 * @nogrep
 */

/* tslint:disable */
/* eslint-disable */
// @ts-nocheck

import { Fragment, ReaderFragment } from 'relay-runtime';
import { FragmentRefs } from "relay-runtime";
export type NewOrgEnvironmentRuleFragment_organization$data = {
  readonly environmentNames: ReadonlyArray<string>;
  readonly id: string;
  readonly name: string;
  readonly " $fragmentType": "NewOrgEnvironmentRuleFragment_organization";
};
export type NewOrgEnvironmentRuleFragment_organization$key = {
  readonly " $data"?: NewOrgEnvironmentRuleFragment_organization$data;
  readonly " $fragmentSpreads": FragmentRefs<"NewOrgEnvironmentRuleFragment_organization">;
};

const node: ReaderFragment = {
  "argumentDefinitions": [],
  "kind": "Fragment",
  "metadata": null,
  "name": "NewOrgEnvironmentRuleFragment_organization",
  "selections": [
    {
      "alias": null,
      "args": null,
      "kind": "ScalarField",
      "name": "id",
      "storageKey": null
    },
    {
      "alias": null,
      "args": null,
      "kind": "ScalarField",
      "name": "name",
      "storageKey": null
    },
    {
      "alias": null,
      "args": null,
      "kind": "ScalarField",
      "name": "environmentNames",
      "storageKey": null
    }
  ],
  "type": "Organization",
  "abstractKey": null
};

(node as any).hash = "3a6f3a16e3c49a383aa998756f3da902";

export default node;
