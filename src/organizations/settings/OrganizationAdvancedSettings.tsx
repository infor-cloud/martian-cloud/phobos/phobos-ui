import { useState } from 'react';
import { Alert, AlertTitle, Box, Button, Collapse, Dialog, DialogActions, DialogContent, DialogTitle, TextField, Typography } from '@mui/material';
import { useFragment, useMutation } from 'react-relay/hooks';
import graphql from 'babel-plugin-relay/macro';
import { useSnackbar } from 'notistack';
import { LoadingButton } from '@mui/lab';
import { Prism as SyntaxHighlighter } from 'react-syntax-highlighter';
import { useNavigate } from 'react-router';
import SettingsToggleButton from '../../settings/SettingsToggleButton';
import { atomDark as prismTheme } from 'react-syntax-highlighter/dist/esm/styles/prism';
import { OrganizationAdvancedSettingsDeleteMutation } from './__generated__/OrganizationAdvancedSettingsDeleteMutation.graphql';
import { OrganizationAdvancedSettingsFragment_organization$key } from './__generated__/OrganizationAdvancedSettingsFragment_organization.graphql';
import { GetConnections } from '../../administrator/organizations/AdminAreaOrganizationList';
import { GetMenuButtonConnections } from '../OrganizationMenu';

interface ConfirmationDialogProps {
    orgName: string
    keepMounted: boolean
    open: boolean
    onClose: (confirm?: boolean) => void
    deleteInProgress: boolean
}

function DeleteConfirmationDialog(props: ConfirmationDialogProps) {
    const { orgName, deleteInProgress, onClose, open, ...other } = props;
    const [deleteInput, setDeleteInput] = useState<string>('');

    return (
        <Dialog
            maxWidth="sm"
            open={open}
            {...other}
        >
            <DialogTitle>Delete Organization</DialogTitle>
            <DialogContent dividers>
                <Alert sx={{ mb: 2 }} severity="warning">
                    <AlertTitle>Warning</AlertTitle>
                    Deleting an organization is an <strong><ins>irreversible</ins></strong> operation. All data associated with this organization will be deleted and <strong><ins>cannot be recovered</ins></strong>.
                </Alert>
                <Typography variant="subtitle2">Enter the following to confirm deletion:</Typography>
                <SyntaxHighlighter style={prismTheme} customStyle={{ fontSize: 14, marginBottom: 14 }} children={orgName} />
                <TextField
                    fullWidth
                    autoComplete="off"
                    size="small"
                    placeholder={orgName}
                    value={deleteInput}
                    onChange={(event: any) => setDeleteInput(event.target.value)}
                />
            </DialogContent>
            <DialogActions>
                <Button
                    color="inherit"
                    onClick={() => onClose()}
                >Cancel</Button>
                <LoadingButton
                    color="error"
                    loading={deleteInProgress}
                    disabled={orgName !== deleteInput}
                    onClick={() => onClose(true)}
                >Delete</LoadingButton>
            </DialogActions>
        </Dialog>
    );
}

interface Props {
    fragmentRef: OrganizationAdvancedSettingsFragment_organization$key
}

function OrganizationAdvancedSettings({ fragmentRef }: Props) {
    const { enqueueSnackbar } = useSnackbar();
    const navigate = useNavigate();
    const [showDeleteConfirmationDialog, setShowDeleteConfirmationDialog] = useState<boolean>(false);
    const [showSettings, setShowSettings] = useState<boolean>(false);

    const data = useFragment<OrganizationAdvancedSettingsFragment_organization$key>(
        graphql`
        fragment OrganizationAdvancedSettingsFragment_organization on Organization
        {
            id
            name
        }
    `, fragmentRef);

    const [commit, commitInFlight] = useMutation<OrganizationAdvancedSettingsDeleteMutation>(graphql`
        mutation OrganizationAdvancedSettingsDeleteMutation($input: DeleteOrganizationInput!, $connections: [ID!]!) {
            deleteOrganization(input: $input) {
                organization {
                    id @deleteEdge(connections: $connections)
                }
                problems {
                    message
                    field
                    type
                }
            }
        }
    `);

    const onDeleteConfirmationDialogClosed = (confirm?: boolean) => {
        if (confirm) {
            commit({
                variables: {
                    input: {
                        id: data.id
                    },
                    connections: GetConnections() && GetMenuButtonConnections()
                },
                onCompleted: data => {
                    setShowDeleteConfirmationDialog(false);

                    if (data.deleteOrganization.problems.length) {
                        enqueueSnackbar(data.deleteOrganization.problems.map((problem: any) => problem.message).join('; '), { variant: 'warning' });
                    } else {
                        enqueueSnackbar('Organization deleted', { variant: 'success' });
                        navigate('/');
                    }
                },
                onError: error => {
                    setShowDeleteConfirmationDialog(false);
                    enqueueSnackbar(`Unexpected error occurred: ${error.message}`, { variant: 'error' });
                }
            });
        } else {
            setShowDeleteConfirmationDialog(false);
        }
    };

    return (
        <Box>
            <SettingsToggleButton
                title="Advanced Settings"
                showSettings={showSettings}
                onToggle={() => setShowSettings(!showSettings)}
            />
            <Collapse
                in={showSettings}
                timeout="auto"
                unmountOnExit
            >
            <Typography variant="subtitle1" gutterBottom>Delete Organization</Typography>
            <Alert sx={{ mb: 2 }} severity="error">Deleting an organization is a permanent action that cannot be undone.</Alert>
            <Box>
                <Button
                    variant="outlined"
                    color="error"
                    onClick={() => setShowDeleteConfirmationDialog(true)}
                >Delete Organization</Button>
            </Box>
            </Collapse>
            <DeleteConfirmationDialog
                keepMounted
                orgName={data.name}
                open={showDeleteConfirmationDialog}
                onClose={onDeleteConfirmationDialogClosed}
                deleteInProgress={commitInFlight}
            />
        </Box>
    );
}

export default OrganizationAdvancedSettings;
