import { Stack, Breadcrumbs } from '@mui/material';
import Link from '../routes/Link';

interface Route {
    title: string
    path: string
}

interface Props {
    childRoutes: Route[]
    orgName: string
}

function OrganizationBreadcrumbs({ childRoutes, orgName }: Props) {
    const childRoutePaths = childRoutes.map(r => r.path)

    return (
        <Stack direction="row" spacing={2} marginBottom={2}>
            <Breadcrumbs aria-label="organization breadcrumb">
                <Link color="inherit" to={`/organizations/${orgName}/-/projects`}>{orgName}</Link>
                {childRoutes.map(({ title }, i) => (
                    <Link
                        key={i}
                        color="inherit"
                        to={`/organizations/${orgName}/-/${childRoutePaths.slice(0, i + 1).join('/')}`}
                    >
                        {title}
                    </Link>
                ))}


            </Breadcrumbs>
        </Stack>
    );
}

export default OrganizationBreadcrumbs
