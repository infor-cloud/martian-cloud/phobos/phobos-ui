import { Avatar, Box, List, ListItemButton, ListItemText, TextField, Typography } from "@mui/material";
import { purple } from "@mui/material/colors";
import graphql from "babel-plugin-relay/macro";
import throttle from 'lodash.throttle';
import React, { useEffect, useMemo, useState } from "react";
import { ConnectionHandler, fetchQuery, useLazyLoadQuery, usePaginationFragment, useRelayEnvironment } from "react-relay/hooks";
import { useNavigate } from "react-router";
import { OrganizationMenuFragment_organizations$key } from "./__generated__/OrganizationMenuFragment_organizations.graphql";
import { OrganizationMenuPaginationQuery } from "./__generated__/OrganizationMenuPaginationQuery.graphql";

export function GetMenuButtonConnections() {
    const connectionId = ConnectionHandler.getConnectionID(
        "root",
        "OrganizationMenu_organizations",
    );
    return [connectionId]
}

const ITEM_COUNT = 10;

const query = graphql`
    query OrganizationMenuQuery($first: Int!, $after: String, $search: String) {
        ...OrganizationMenuFragment_organizations
    }`

interface Props {
    onClose: () => void;
}

function OrganizationMenu({ onClose }: Props) {
    const navigate = useNavigate();
    const [search, setSearch] = useState<string | undefined>('');
    const [isRefreshing, setIsRefreshing] = useState(false);

    const queryData = useLazyLoadQuery<OrganizationMenuPaginationQuery>(
        query,
        { first: ITEM_COUNT, search: '' },
        { fetchPolicy: 'store-and-network' }
    );

    const { data, refetch } = usePaginationFragment<OrganizationMenuPaginationQuery, OrganizationMenuFragment_organizations$key>(
        graphql`
        fragment OrganizationMenuFragment_organizations on Query
        @refetchable(queryName: "OrganizationMenuPaginationQuery") {
            organizations(
                first: $first
                after: $after
                search: $search
                sort: UPDATED_AT_DESC
                ) @connection(key: "OrganizationMenu_organizations") {
                    edges {
                        node {
                            id
                            name
                        }
                    }
                }
            }
        `, queryData);

    const environment = useRelayEnvironment();

    const fetch = useMemo(
        () =>
            throttle(
                (input?: string) => {
                    setSearch(input);
                    setIsRefreshing(true);

                    fetchQuery(environment, query, { first: ITEM_COUNT, search: input }, { fetchPolicy: 'store-or-network' })
                        .subscribe({
                            complete: () => {
                                setIsRefreshing(false);
                                setSearch(input);

                                // *After* the query has been fetched, we call
                                // refetch again to re-render with the updated data.
                                // At this point the data for the query should
                                // be cached, so we use the 'store-only'
                                // fetchPolicy to avoid suspending.
                                refetch({ first: ITEM_COUNT, search: input }, { fetchPolicy: 'store-only' });
                            },
                            error: () => {
                                setIsRefreshing(false);
                            }
                        });
                },
                2000,
                { leading: false, trailing: true }
            ),
        [environment, refetch],
    );

    useEffect(() => {
        return () => {
            fetch.cancel()
        }
    }, [fetch]);

    const onSearchChange = (event: React.ChangeEvent<HTMLInputElement>) => {
        fetch(event.target.value.toLowerCase().trim());
    };

    const onKeyDown = (event: React.KeyboardEvent<HTMLInputElement>) => {
        // Only handle enter key type
        if (event.key === 'Enter') {
            fetch.flush();
        }
    };

    const edges = data?.organizations?.edges || [];

    return (
        <Box padding="16px 24px">
            <TextField
                fullWidth
                size="small"
                margin="none"
                autoComplete='off'
                sx={{ marginBottom: 1 }}
                placeholder="search by org name"
                onChange={onSearchChange}
                onKeyDown={onKeyDown} />
            <List dense disablePadding sx={isRefreshing ? { opacity: 0.5 } : null}>
                {edges.map((edge: any) => (
                    <ListItemButton
                        disableGutters
                        key={edge.node.id}
                        onClick={() => {
                            navigate(`/organizations/${edge.node.name}`);
                            onClose();
                        }}
                    >
                        <Avatar variant="rounded" sx={{ width: 24, height: 24, bgcolor: purple[300], marginLeft: 1, marginRight: 2 }}>
                            {edge.node.name[0].toUpperCase()}
                        </Avatar>
                        <ListItemText sx={{ wordWrap: 'break-word' }} primary={edge.node.name} />
                    </ListItemButton>
                ))}
            </List>
            {(edges.length === 0) && <Typography
                padding={2}
                align="center"
                color="textSecondary"
            >
                {search !== '' && <React.Fragment>
                    No organizations matching search <strong>{search}</strong>
                </React.Fragment>}
                {search === '' && <React.Fragment>
                    No organizations
                </React.Fragment>}
            </Typography>}
        </Box>
    );
}

export default OrganizationMenu
