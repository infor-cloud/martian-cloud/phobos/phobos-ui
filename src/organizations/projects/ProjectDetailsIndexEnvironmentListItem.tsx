import { Box, Chip, TableCell, TableRow, Tooltip, Typography, useTheme } from '@mui/material';
import graphql from 'babel-plugin-relay/macro';
import { useFragment } from 'react-relay/hooks';
import Timestamp from '../../common/Timestamp';
import Link from '../../routes/Link';
import { ProjectDetailsIndexEnvironmentListItemFragment_environment$key } from './__generated__/ProjectDetailsIndexEnvironmentListItemFragment_environment.graphql';
import PipelineAnnotations from './pipelines/PipelineAnnotations';
import PipelineStatusType from './pipelines/PipelineStatusType';

interface Props {
    fragmentRef: ProjectDetailsIndexEnvironmentListItemFragment_environment$key;
}

function ProjectDetailsIndexEnvironmentListItem({ fragmentRef }: Props) {
    const theme = useTheme();

    const environment = useFragment<ProjectDetailsIndexEnvironmentListItemFragment_environment$key>(
        graphql`
            fragment ProjectDetailsIndexEnvironmentListItemFragment_environment on Environment {
                id
                name
                pipelines(first: 1, sort: COMPLETED_AT_DESC, completed: true, pipelineTypes: [DEPLOYMENT]) {
                    edges {
                        node {
                            id
                            type
                            environmentName
                            environment {
                                id
                            }
                            timestamps {
                                startedAt
                                completedAt
                            }
                            release {
                                id
                                semanticVersion
                            }
                            status
                            annotations {
                                key
                            }
                            ...PipelineAnnotationsFragment_pipeline
                        }
                    }
                }
            }
        `,
        fragmentRef,
    );

    const deployment = environment.pipelines?.edges && environment.pipelines?.edges.length > 0 ? environment.pipelines?.edges[0]?.node : null;

    return (
        <TableRow sx={{ '&:last-child td, &:last-child th': { border: 0 } }}>
            <TableCell width={100}>
                <Link to={`-/environments/${environment.id}`} color="textPrimary" variant="subtitle1" fontWeight={500}>{environment.name}</Link>
            </TableCell>
            <TableCell>
                {deployment && (
                    <Box display="flex" alignItems="center" overflow="hidden">
                        <Tooltip title={PipelineStatusType[deployment.status]?.label}>
                            {PipelineStatusType[deployment.status]?.icon}
                        </Tooltip>
                        <Box flex={1} overflow="hidden" ml={2}>
                            <Box display="flex" alignItems="center" mb={0.5}>
                                <Link
                                    to={`-/pipelines/${deployment.id}`}
                                    component="span"
                                    fontWeight={500}
                                    variant="body2"
                                    color="textPrimary"
                                    mr={1}
                                >
                                    {deployment.id.substring(0, 8)}...
                                </Link>
                                {deployment.release && <Chip
                                    component={Link}
                                    to={`-/releases/${deployment.release.id}`}
                                    variant="outlined"
                                    size="small"
                                    sx={{ fontWeight: 500, cursor: 'pointer', overflow: 'hidden' }}
                                    label={`v${deployment.release.semanticVersion}`}
                                />}
                            </Box>
                            <Typography variant="body2" color="textSecondary" mb={deployment.annotations.length > 0 ? 1 : 0}>
                                {deployment.timestamps.completedAt ? 'Deployed' : 'Started'} <Timestamp component="span" timestamp={deployment.timestamps.completedAt || deployment.timestamps.startedAt} />
                            </Typography>
                            <PipelineAnnotations fragmentRef={deployment} />
                        </Box>
                    </Box>
                )}
                {!deployment && <Chip
                    sx={{ color: theme.palette.text.secondary, borderRadius: 1, ml: 5 }}
                    size="small"
                    label="no deployments"
                />}
            </TableCell>
        </TableRow >
    );
}

export default ProjectDetailsIndexEnvironmentListItem;
