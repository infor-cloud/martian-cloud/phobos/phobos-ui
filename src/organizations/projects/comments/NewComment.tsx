import { LoadingButton } from '@mui/lab';
import { Box, Link, Tooltip, Typography } from '@mui/material';
import { useAuth } from 'react-oidc-context';
import { ConnectionHandler, ConnectionInterface, RecordProxy, RecordSourceProxy } from 'relay-runtime';
import Gravatar from '../../../common/Gravatar';
import CommentForm from './CommentForm';
import { SortType } from './CommentOptionsBar';

export const addThreadToConnection = (store: RecordSourceProxy, record: RecordProxy, recordId: string, sort: SortType, connectionId: string) => {
    const { NODE, EDGES } = ConnectionInterface.get();

    const connectionRecord = store.get(connectionId);
    if (connectionRecord) {
        const nodeAlreadyExistsInConnection = connectionRecord
            .getLinkedRecords(EDGES)
            ?.some(
                edge => edge?.getLinkedRecord(NODE)?.getDataID() === recordId,
            );
        if (!nodeAlreadyExistsInConnection) {
            const edge = ConnectionHandler.createEdge(
                store,
                connectionRecord,
                record,
                'CommentEdge'
            );
            if (edge) {
                if (sort === 'CREATED_AT_ASC') {
                    ConnectionHandler.insertEdgeAfter(connectionRecord, edge);
                } else {
                    ConnectionHandler.insertEdgeBefore(connectionRecord, edge);
                }
            } else {
                console.warn('Failed to create edge');
            }
        }
    } else {
        console.warn('Connection record not found');
    }
};


interface Props {
    isInFlight: boolean
    tab: 'write' | 'preview'
    formData: string
    onChange: (data: string) => void
    onTabChange: (event: React.SyntheticEvent, newValue: 'write' | 'preview') => void
    onSave: (text: string, type: 'THREAD' | 'REPLY', replyToCommentId: string | null) => void
}

function NewComment({ onSave, isInFlight, tab, formData, onTabChange, onChange }: Props) {
    const { user } = useAuth();
    const email = user?.profile.email || 'user';

    return (
        <Box sx={{ display: "flex", mb: 4 }}>
            <Box mr={2}>
                <Tooltip title={email}>
                    <Box>
                        <Gravatar width={32} height={32} email={email} />
                    </Box>
                </Tooltip>
            </Box>
            <Box sx={{ width: "100%" }}>
                <CommentForm
                    tab={tab}
                    disableAutoFocus
                    onTabChange={onTabChange}
                    data={formData}
                    placeholder="Start a conversation..."
                    onChange={onChange}
                />
                <LoadingButton
                    sx={{ mr: 1 }}
                    loading={isInFlight}
                    variant="outlined"
                    color="primary"
                    disabled={formData.length === 0}
                    onClick={() => onSave(formData, 'THREAD', null)}
                >
                    Add
                </LoadingButton>
                <Typography
                    sx={{ p: 1 }}
                    variant="caption">Select markdown features are supported. Learn more about{' '}
                    <Link
                        href='https://commonmark.org/help/'
                        target='_blank'
                        rel='noopener noreferrer'
                        underline='hover'
                    >markdown
                    </Link>.
                </Typography>
            </Box>
        </Box>
    );
}

export default NewComment
