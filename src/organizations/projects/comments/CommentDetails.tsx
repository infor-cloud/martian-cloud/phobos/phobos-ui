import { Box, Collapse, IconButton, ListItemAvatar, Tooltip, Typography } from '@mui/material';
import { ExpandLess, ExpandMore, ModeEdit as ModeEditIcon, Delete as DeleteIcon, Reply as ReplyIcon } from '@mui/icons-material';
import { useFragment } from 'react-relay/hooks';
import graphql from 'babel-plugin-relay/macro';
import { useAuth } from 'react-oidc-context';
import remarkGfm from 'remark-gfm';
import ServiceAccountAvatar from '../../../common/ServiceAccountAvatar';
import Gravatar from '../../../common/Gravatar';
import MuiMarkdown from '../../../common/Markdown';
import EditComment from './EditComment';
import ConfirmationDialog from '../../../common/ConfirmationDialog';
import Timestamp from '../../../common/Timestamp';
import { CommentDetailsFragment_comment$key } from './__generated__/CommentDetailsFragment_comment.graphql';

interface Props {
    title: string
    message: string
    showDeleteDialog: boolean
    opInProgress: boolean
    showEditForm: boolean
    showCollapse?: boolean
    fragmentRef: CommentDetailsFragment_comment$key
    onShowEditForm: (show: boolean) => void
    onEdit: (text: string) => void
    onOpenDeleteDialog: () => void
    onCloseDeleteDialog: () => void
    onToggleReplyInput?: () => void
    onDelete: (commentId: string) => void
}

function CommentDetails({
    title,
    message,
    showDeleteDialog,
    opInProgress,
    showEditForm,
    showCollapse,
    fragmentRef,
    onShowEditForm,
    onEdit,
    onOpenDeleteDialog,
    onCloseDeleteDialog,
    onToggleReplyInput,
    onDelete
}: Props) {
    const auth = useAuth();

    const data = useFragment<CommentDetailsFragment_comment$key>(
        graphql`
            fragment CommentDetailsFragment_comment on Comment {
                id
                metadata {
                    createdAt
                    updatedAt
                }
                text
                thread {
                    comments {
                        id
                    }
                }
                creator {
                    __typename
                    ... on User {
                        email
                        username
                    }
                    __typename
                    ... on ServiceAccount {
                        name
                    }
                }
                ...EditCommentFragment_comment
            }
        `, fragmentRef);

    const email = auth.user?.profile.email?.toLowerCase() || '';
    const showOptions = data.creator && (data.creator.__typename === 'User' && data.creator.email === email);
    const showEditCaption = data.metadata.createdAt !== data.metadata.updatedAt;

    const isRootComment = data.thread.comments[0]?.id === data.id;

    return (
        <Box sx={{ alignItems: "flex-start", flexDirection: "column", width: "100%" }}>
            <Box sx={{ display: "flex" }}>
                <ListItemAvatar>
                    {data.creator && (
                        data.creator.__typename === 'User' ? (
                            <Tooltip title={data.creator.email || ''}>
                                <Box mr={2}>
                                    <Gravatar width={32} height={32} email={data.creator.email || ''} />
                                </Box>
                            </Tooltip>
                        ) : (
                            data.creator.__typename === 'ServiceAccount' && (
                                <ServiceAccountAvatar
                                    sx={{ ml: 1 }}
                                    name={data.creator.name}
                                />
                            )
                        )
                    )}
                </ListItemAvatar>
                <Box flex={1}>
                    <Typography
                        color='textSecondary'
                        variant='subtitle2'
                    >
                        {data.creator && (
                            data.creator.__typename === 'User' ? (
                                <strong>{data.creator.username}</strong>
                            ) : (
                                data.creator.__typename === 'ServiceAccount' && (
                                    <strong>{data.creator.name}</strong>
                                )
                            )
                        )} {isRootComment ? 'started a conversation' : 'commented'}{' '}
                        <Timestamp timestamp={data.metadata.createdAt} />
                    </Typography>
                    {!showEditForm &&
                        <MuiMarkdown
                            remarkPlugins={[remarkGfm]}
                            children={data.text}
                        />
                    }
                </Box>
                {onToggleReplyInput &&
                    <Box sx={{ display: "flex", alignItems: "center" }}>
                        <Tooltip
                            title={data.thread.comments.length === 1 ?
                                'Reply to conversation' : showCollapse ? 'Hide replies'
                                    :
                                    'Show replies'}
                        >
                            <IconButton
                                onClick={onToggleReplyInput}
                            >
                                {data.thread.comments.length === 1 ?
                                    <ReplyIcon sx={{ width: 16, height: 16 }} /> : showCollapse ? <ExpandLess />
                                        :
                                        <ExpandMore />
                                }
                            </IconButton>
                        </Tooltip>
                        {showOptions && <>
                            <Tooltip title="Edit conversation">
                                <IconButton
                                    onClick={() => onShowEditForm(!showEditForm)}
                                >
                                    <ModeEditIcon sx={{ width: 16, height: 16 }} />
                                </IconButton>
                            </Tooltip>
                            <Tooltip title="Delete this conversation and all comments">
                                <IconButton
                                    onClick={onOpenDeleteDialog}
                                >
                                    <DeleteIcon sx={{ width: 16, height: 16 }} />
                                </IconButton>
                            </Tooltip>
                        </>}
                    </Box>
                }

                {(!isRootComment && showOptions) && <Box>
                    <Tooltip title="Edit comment">
                        <IconButton
                            onClick={() => onShowEditForm(!showEditForm)}
                        >
                            <ModeEditIcon sx={{ width: 16, height: 16 }} />
                        </IconButton>
                    </Tooltip>
                    <Tooltip title="Delete comment">
                        <IconButton
                            onClick={onOpenDeleteDialog}
                        >
                            <DeleteIcon sx={{ width: 16, height: 16 }} />
                        </IconButton>
                    </Tooltip>
                </Box>}
            </Box>
            {(showEditCaption && !showEditForm) &&
                <Box sx={{ ml: 7, mb: 1 }}>
                    <Typography
                        color='textSecondary'
                        variant='caption'
                    >Edited {<Timestamp timestamp={data.metadata.updatedAt} />}
                    </Typography>
                </Box>}
            <Collapse
                sx={{ mt: 2, width: "100%" }}
                in={showEditForm}
                timeout="auto"
                unmountOnExit
            >
                <EditComment
                    isInFlight={opInProgress}
                    onEdit={onEdit}
                    onCancel={() => onShowEditForm(false)}
                    fragmentRef={data} />
            </Collapse>
            {
                showDeleteDialog && <ConfirmationDialog
                    title={title}
                    message={message}
                    confirmButtonLabel='Delete'
                    opInProgress={opInProgress}
                    onConfirm={() => onDelete(data.id)}
                    onClose={onCloseDeleteDialog}
                />
            }
        </Box>
    );
}

export default CommentDetails;
