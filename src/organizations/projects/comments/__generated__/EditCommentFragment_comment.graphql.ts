/**
 * @generated SignedSource<<4ab784a3d6fbc359772c46c25a8fa0f6>>
 * @lightSyntaxTransform
 * @nogrep
 */

/* tslint:disable */
/* eslint-disable */
// @ts-nocheck

import { Fragment, ReaderFragment } from 'relay-runtime';
import { FragmentRefs } from "relay-runtime";
export type EditCommentFragment_comment$data = {
  readonly id: string;
  readonly text: string;
  readonly " $fragmentType": "EditCommentFragment_comment";
};
export type EditCommentFragment_comment$key = {
  readonly " $data"?: EditCommentFragment_comment$data;
  readonly " $fragmentSpreads": FragmentRefs<"EditCommentFragment_comment">;
};

const node: ReaderFragment = {
  "argumentDefinitions": [],
  "kind": "Fragment",
  "metadata": null,
  "name": "EditCommentFragment_comment",
  "selections": [
    {
      "alias": null,
      "args": null,
      "kind": "ScalarField",
      "name": "id",
      "storageKey": null
    },
    {
      "alias": null,
      "args": null,
      "kind": "ScalarField",
      "name": "text",
      "storageKey": null
    }
  ],
  "type": "Comment",
  "abstractKey": null
};

(node as any).hash = "c353454412f2163729e0bfd4763fc662";

export default node;
