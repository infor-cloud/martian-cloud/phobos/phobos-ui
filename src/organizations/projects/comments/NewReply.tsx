import { LoadingButton } from '@mui/lab';
import { Box, Button } from '@mui/material';
import { useState } from 'react';
import CommentForm from './CommentForm';

interface Props {
    opInFlight: boolean
    onSave: (text: string) => void
    onCancel: () => void
}

function NewReply({ opInFlight, onCancel, onSave }: Props) {
    const [tab, setTab] = useState<'write' | 'preview'>('write');
    const [formData, setFormData] = useState<string>('');

    return (
        <Box sx={{ display: "flex", mb: 2 }}>
            <Box sx={{ width: '100%' }}>
                <CommentForm
                    tab={tab}
                    onTabChange={(event, newValue) => setTab(newValue)}
                    data={formData}
                    placeholder="Add a comment"
                    onChange={(data: string) => setFormData(data)}
                />
                <LoadingButton
                    sx={{ m: 1 }}
                    loading={opInFlight}
                    variant="outlined"
                    color="primary"
                    disabled={formData.length === 0}
                    onClick={() => onSave(formData)}
                >
                    Add
                </LoadingButton>
                <Button
                    sx={{ m: 1 }}
                    color="inherit"
                    onClick={onCancel}
                >
                    Cancel
                </Button>
            </Box>
        </Box>
    );
}

export default NewReply
