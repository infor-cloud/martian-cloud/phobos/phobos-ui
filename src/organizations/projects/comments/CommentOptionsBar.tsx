import { Box, Button, Tooltip } from '@mui/material';
import { SortAscending, SortDescending } from 'mdi-material-ui';

export type SortType = 'CREATED_AT_ASC' | 'CREATED_AT_DESC';

interface Props {
    sortOption: SortType
    onChange: () => void
}

function CommentOptionsBar({ sortOption, onChange }: Props) {

    return (
        <Box sx={{
            mb: 2,
            display: "flex",
            justifyContent: "flex-end",
            alignItems: "center"
        }}>
            <Tooltip title='Reverse sort order'>
                <Button
                    size="small"
                    color="inherit"
                    variant="contained"
                    onClick={onChange}
                >
                    {sortOption === 'CREATED_AT_ASC' ? <SortAscending/> : <SortDescending/>}
                </Button>
            </Tooltip>
        </Box>
    );
}

export default CommentOptionsBar
