import { useState } from 'react';
import { Box, Button } from '@mui/material';
import { LoadingButton } from '@mui/lab';
import { useFragment } from 'react-relay/hooks';
import graphql from 'babel-plugin-relay/macro';
import CommentForm from './CommentForm';
import { EditCommentFragment_comment$key } from './__generated__/EditCommentFragment_comment.graphql';

interface Props {
    isInFlight: boolean
    onEdit: (text: string, commentId: string) => void
    onCancel: () => void
    fragmentRef: EditCommentFragment_comment$key
}

function EditComment({ fragmentRef, onCancel, onEdit, isInFlight }: Props) {
    const [tab, setTab] = useState<'write' | 'preview'>('write');

    const data = useFragment<EditCommentFragment_comment$key>(graphql`
        fragment EditCommentFragment_comment on Comment
        {
            id
            text
        }
    `, fragmentRef);

    const [formData, setFormData] = useState<string>(data.text);

    return (
        <Box sx={{ mb: 2 }}>
            <CommentForm
                tab={tab}
                onTabChange={(event, newValue) => setTab(newValue)}
                data={formData}
                placeholder="Edit conversation"
                onChange={(data: string) => setFormData(data)}
            />
            <LoadingButton
                sx={{ m: 1 }}
                loading={isInFlight}
                variant="outlined"
                color="primary"
                disabled={formData.length === 0 || formData === data.text}
                onClick={() => onEdit(formData, data.id)}
            >
                Edit
            </LoadingButton>
            <Button
                sx={{ m: 1 }}
                color="inherit"
                onClick={onCancel}
            >
                Cancel
            </Button>
        </Box>
    );
}

export default EditComment
