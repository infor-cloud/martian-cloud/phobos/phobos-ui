import { Box, Tab, Tabs, TextField, useTheme } from '@mui/material';
import MuiMarkdown from '../../../common/Markdown';
import remarkGfm from 'remark-gfm';

interface Props {
    disableAutoFocus?: boolean
    placeholder: string
    tab: 'write' | 'preview'
    onTabChange: (event: React.SyntheticEvent, newValue: 'write' | 'preview') => void
    data: string
    onChange: (data: string ) => void
}

function CommentForm({ placeholder, data, onChange, tab, onTabChange, disableAutoFocus }: Props) {
    const theme = useTheme();

    return (
        <Box>
            <Box
                sx={{
                    flex: 1,
                    mb: 2,
                    border: 1.5,
                    display: "flex",
                    borderBottom: `1px solid ${theme.palette.divider}`,
                    borderRadius: 2,
                    borderColor: 'divider'
                }}>
                <Tabs value={tab} onChange={onTabChange}>
                    <Tab label="Write" value="write" />
                    <Tab label="Preview" value="preview" />
                </Tabs>
            </Box>
            {tab === 'write' ?
                <TextField
                    autoFocus={!disableAutoFocus}
                    sx={{ mb: 2 }}
                    fullWidth
                    value={data}
                    multiline
                    minRows="5"
                    placeholder={placeholder}
                    onChange={event => onChange(event.target.value)}/>
                :
                <Box
                    sx={{
                        p: 2,
                        mb: 2,
                        minHeight: 150,
                        border: 1,
                        borderRadius: 1,
                        borderColor: 'divider',
                    }}
                >
                    <MuiMarkdown
                        remarkPlugins={[remarkGfm]}
                        children={data}
                    />
                </Box>
            }
        </Box>
    );
}

export default CommentForm
