import { useState } from 'react';
import { Box, Button, Divider, Typography } from '@mui/material';
import LoadingButton from '@mui/lab/LoadingButton';
import graphql from 'babel-plugin-relay/macro';
import { MutationError } from '../../common/error';
import {Link as RouterLink, useNavigate, useOutletContext } from 'react-router-dom';
import ProjectForm, { FormData } from '../../settings/GeneralSettingsForm';
import { GetConnections } from './ProjectList';
import OrganizationBreadcrumbs from '../OrganizationBreadcrumbs';
import { useFragment, useMutation } from 'react-relay/hooks';
import { NewProjectMutation } from './__generated__/NewProjectMutation.graphql';
import { NewProjectFragment_organization$key } from './__generated__/NewProjectFragment_organization.graphql';

function NewProject() {
    const navigate = useNavigate();
    const context = useOutletContext() as NewProjectFragment_organization$key
    const [error, setError] = useState<MutationError>()
    const [formData, setFormData] = useState<FormData>({
        name: '',
        description: ''
    })

    const data = useFragment<NewProjectFragment_organization$key>(graphql`
        fragment NewProjectFragment_organization on Organization
            {
                id
                name
            }
        `,
        context);

    const [commit, isInFlight] = useMutation<NewProjectMutation>(graphql`
        mutation NewProjectMutation($input: CreateProjectInput!, $connections: [ID!]!) {
            createProject(input: $input) {
                # Use @prependNode to add the node to the connection
                project @prependNode(connections: $connections, edgeTypeName: "ProjectEdge") {
                    id
                    name
                    ...ProjectListItemFragment_project
                }
                problems{
                    message
                    field
                    type
                }
            }
        }
    `);

    const onSave = () => {
        commit({
            variables: {
                input: {
                    name: formData.name,
                    description: formData.description,
                    orgName: data.name
                },
                connections: GetConnections(data.id)
            },
            onCompleted: data => {
                if (data.createProject.problems.length) {
                    setError({
                        severity: 'warning',
                        message: data.createProject.problems.map((problem: any) => problem.message).join(', ')
                    });
                } else if (!data.createProject.project) {
                    setError({
                        severity: 'error',
                        message: 'Unexpected error occurred'
                    });
                } else {
                    navigate(`../../projects/${data.createProject.project.name}`)
                }
            },
            onError: (error) => {
                setError({
                    severity: 'error',
                    message: `Unexpected error occurred: ${error.message}`
                })
            }
        });
    };

    return (
        <Box>
            <OrganizationBreadcrumbs
                orgName={data.name}
                childRoutes={[
                    { title: "projects", path: 'projects' },
                    { title: "new", path: 'new' }
                ]}
            />
            <Typography variant="h5">New Project</Typography>
            <ProjectForm
                data={formData}
                onChange={(data: FormData) => setFormData(data)}
                error={error}
            />
            <Divider light />
            <Box marginTop={2}>
                <LoadingButton
                    sx={{ mr: 2 }}
                    loading={isInFlight}
                    variant="outlined"
                    color="primary"
                    onClick={onSave}
                >Create Project</LoadingButton>
                <Button component={RouterLink} color="inherit" to={-1 as any}>Cancel</Button>
            </Box>
        </Box>
    );
}

export default NewProject
