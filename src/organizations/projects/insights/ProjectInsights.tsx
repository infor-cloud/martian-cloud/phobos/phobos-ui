import { Alert, Box, Grid, Tab, Tabs, Typography, useTheme } from '@mui/material';
import Checkbox from '@mui/material/Checkbox';
import FormControl from '@mui/material/FormControl';
import ListItemText from '@mui/material/ListItemText';
import MenuItem from '@mui/material/MenuItem';
import OutlinedInput from '@mui/material/OutlinedInput';
import Select, { SelectChangeEvent } from '@mui/material/Select';
import graphql from 'babel-plugin-relay/macro';
import { useMemo, useState } from 'react';
import { useFragment } from 'react-relay/hooks';
import { useOutletContext, useSearchParams } from 'react-router-dom';
import TabContent from '../../../common/TabContent';
import { MAX_SERIES } from '../../../insights/InsightsChart';
import InsightsChartApprovalRequestCompleted from '../../../insights/InsightsChartApprovalRequestCompleted';
import InsightsChartApprovalRequestCreated from '../../../insights/InsightsChartApprovalRequestCreated';
import InsightsChartApprovalRequestWaitTime from '../../../insights/InsightsChartApprovalRequestWaitTime';
import InsightsChartDeploymentFailureRate from '../../../insights/InsightsChartDeploymentFailureRate';
import InsightsChartDeploymentFrequency from '../../../insights/InsightsChartDeploymentFrequency';
import InsightsChartDeploymentLeadTime from '../../../insights/InsightsChartDeploymentLeadTime';
import InsightsChartDeploymentRecoveryTime from '../../../insights/InsightsChartDeploymentRecoveryTime';
import InsightsChartDeploymentUpdates from '../../../insights/InsightsChartDeploymentUpdates';
import InsightsChartPipelineDuration from '../../../insights/InsightsChartPipelineDuration';
import InsightsChartPipelinesCompleted from '../../../insights/InsightsChartPipelinesCompleted';
import InsightsTimeRangeSelector from '../../../insights/InsightsTimeRangeSelector';
import ProjectBreadcrumbs from '../ProjectBreadcrumbs';
import { ProjectInsightsFragment_project$key } from './__generated__/ProjectInsightsFragment_project.graphql';

function ProjectInsights() {
    const theme = useTheme();
    const context = useOutletContext<ProjectInsightsFragment_project$key>();
    const [searchParams, setSearchParams] = useSearchParams();
    const timeRange = searchParams.get('range') || '30d';
    const tab = searchParams.get('tab') || 'deployments';
    const [environments, setEnvironments] = useState<string[]>([]);

    const project = useFragment<ProjectInsightsFragment_project$key>(
        graphql`
            fragment ProjectInsightsFragment_project on Project
            {
                id
                metadata {
                    prn
                }
                environmentNames
            }
        `, context
    );

    const onTimeRangeChange = (value: string) => {
        searchParams.set('range', value);
        setSearchParams(searchParams, { replace: true });
    };

    const onEnvironmentsChange = ({ target: { value } }: SelectChangeEvent<string[]>) => {
        const parsedValue = typeof value === 'string' ? value.split(',') : value;
        if (parsedValue.includes('')) {
            // Select all
            setEnvironments(environments.length === project.environmentNames.length ? [] : project.environmentNames.map((name) => name));
        } else {
            setEnvironments(parsedValue);
        }
    };

    const onTabChange = (event: React.SyntheticEvent, newValue: string) => {
        searchParams.set('tab', newValue);
        setSearchParams(searchParams, { replace: true });
    };

    const filters = useMemo(() => {
        if (environments.length) {
            return environments.map(environmentName => ({ environmentName, label: environmentName }));
        } else {
            return [{ label: '' }];
        }
    }, [environments]);

    const showLegend = useMemo(() => environments.length > 0, [environments]);

    return <Box>
        <ProjectBreadcrumbs
            prn={project.metadata.prn}
            childRoutes={[
                { title: "insights", path: 'insights' }
            ]}
        />
        <Typography variant="h5" mb={2}>Project Insights</Typography>
        <Box sx={{
            marginBottom: 2,
            display: 'flex',
            flexDirection: 'row',
            justifyContent: 'space-between',
            [theme.breakpoints.down('sm')]: {
                flexDirection: 'column',
                alignItems: 'flex-start',
                '& > *': { marginBottom: 2 },
            }
        }}>
            <InsightsTimeRangeSelector value={timeRange} onChange={onTimeRangeChange} />
            <FormControl sx={{ width: 300 }}>
                <Select
                    multiple
                    displayEmpty
                    value={environments}
                    onChange={onEnvironmentsChange}
                    input={<OutlinedInput size="small" />}
                    renderValue={(selected) => {
                        if (selected.length === 0) {
                            return <Typography color="textSecondary">Filter by environment</Typography>;
                        }

                        return selected.join(', ');
                    }}
                >
                    {project.environmentNames.length > 0 && <MenuItem value="">
                        <Typography color="textSecondary">
                            {environments.length === project.environmentNames.length ? 'Unselect all ' : 'Select all'} environments
                        </Typography>
                    </MenuItem>}
                    {project.environmentNames.map((name) => (
                        <MenuItem key={name} value={name}>
                            <Checkbox checked={environments.indexOf(name) > -1} />
                            <ListItemText primary={name} />
                        </MenuItem>
                    ))}
                    {project.environmentNames.length === 0 && <MenuItem disabled>No Environments</MenuItem>}
                </Select>
            </FormControl>
        </Box>
        {(filters.length * 2) > MAX_SERIES && <Alert severity="warning" sx={{ marginBottom: 2 }}>
            <Typography variant="body2">
                Some data is not being displayed because there are too many filters selected. Please reduce the number of filters to see all the data.
            </Typography>
        </Alert>}
        <Box sx={{ display: "flex", border: 1, borderColor: 'divider', borderTopLeftRadius: 4, borderTopRightRadius: 4, mb: 1 }}>
            <Tabs value={tab} onChange={onTabChange}>
                <Tab label="Deployments" value="deployments" />
                <Tab label="Pipelines" value="pipelines" />
                <Tab label="Approvals" value="approvals" />
            </Tabs>
        </Box>
        <TabContent>
            {tab === 'deployments' && <Grid container spacing={2}>
                <Grid item xs={12} md={6}>
                    <InsightsChartDeploymentFrequency
                        timeRange={timeRange}
                        projectId={project.id}
                        filters={filters}
                        showLegend={showLegend}
                    />
                </Grid>
                <Grid item xs={12} md={6}>
                    <InsightsChartDeploymentLeadTime
                        timeRange={timeRange}
                        projectId={project.id}
                        filters={filters}
                        showLegend={showLegend}
                    />
                </Grid>
                <Grid item xs={12} md={6}>
                    <InsightsChartDeploymentFailureRate
                        timeRange={timeRange}
                        projectId={project.id}
                        filters={filters}
                        showLegend={showLegend}
                    />
                </Grid>
                <Grid item xs={12} md={6}>
                    <InsightsChartDeploymentRecoveryTime
                        timeRange={timeRange}
                        projectId={project.id}
                        filters={filters}
                        showLegend={showLegend}
                    />
                </Grid>
                <Grid item xs={12}>
                    <InsightsChartDeploymentUpdates
                        timeRange={timeRange}
                        projectId={project.id}
                        filters={filters}
                        showLegend={showLegend}
                    />
                </Grid>
            </Grid>}
            {tab === 'pipelines' && <Grid container spacing={2}>
                <Grid item xs={12}>
                    <InsightsChartPipelinesCompleted
                        timeRange={timeRange}
                        projectId={project.id}
                        filters={filters}
                        showLegend={showLegend}
                    />
                </Grid>
                <Grid item xs={12}>
                    <InsightsChartPipelineDuration
                        timeRange={timeRange}
                        projectId={project.id}
                        filters={filters}
                        showLegend={showLegend}
                    />
                </Grid>
            </Grid>}
            {tab === 'approvals' && <Grid container spacing={2}>
                <Grid item xs={12} md={6}>
                    <InsightsChartApprovalRequestCreated
                        timeRange={timeRange}
                        projectId={project.id}
                        filters={filters}
                        showLegend={showLegend}
                    />
                </Grid>
                <Grid item xs={12} md={6}>
                    <InsightsChartApprovalRequestCompleted
                        timeRange={timeRange}
                        projectId={project.id}
                        filters={filters}
                        showLegend={showLegend}
                    />
                </Grid>
                <Grid item xs={12}>
                    <InsightsChartApprovalRequestWaitTime
                        timeRange={timeRange}
                        projectId={project.id}
                        filters={filters}
                        showLegend={showLegend}
                    />
                </Grid>
            </Grid>}
        </TabContent>
    </Box >;
}

export default ProjectInsights;
