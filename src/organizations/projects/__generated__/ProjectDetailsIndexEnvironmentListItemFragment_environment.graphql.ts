/**
 * @generated SignedSource<<7b54c6abf9aa8c6012f3fa1ef5dd8ab4>>
 * @lightSyntaxTransform
 * @nogrep
 */

/* tslint:disable */
/* eslint-disable */
// @ts-nocheck

import { Fragment, ReaderFragment } from 'relay-runtime';
export type PipelineNodeStatus = "APPROVAL_PENDING" | "CANCELED" | "CANCELING" | "CREATED" | "DEFERRED" | "FAILED" | "PENDING" | "READY" | "RUNNING" | "SKIPPED" | "SUCCEEDED" | "WAITING" | "%future added value";
export type PipelineType = "DEPLOYMENT" | "NESTED" | "RELEASE_LIFECYCLE" | "RUNBOOK" | "%future added value";
import { FragmentRefs } from "relay-runtime";
export type ProjectDetailsIndexEnvironmentListItemFragment_environment$data = {
  readonly id: string;
  readonly name: string;
  readonly pipelines: {
    readonly edges: ReadonlyArray<{
      readonly node: {
        readonly annotations: ReadonlyArray<{
          readonly key: string;
        }>;
        readonly environment: {
          readonly id: string;
        } | null | undefined;
        readonly environmentName: string | null | undefined;
        readonly id: string;
        readonly release: {
          readonly id: string;
          readonly semanticVersion: string;
        } | null | undefined;
        readonly status: PipelineNodeStatus;
        readonly timestamps: {
          readonly completedAt: any | null | undefined;
          readonly startedAt: any | null | undefined;
        };
        readonly type: PipelineType;
        readonly " $fragmentSpreads": FragmentRefs<"PipelineAnnotationsFragment_pipeline">;
      } | null | undefined;
    } | null | undefined> | null | undefined;
  };
  readonly " $fragmentType": "ProjectDetailsIndexEnvironmentListItemFragment_environment";
};
export type ProjectDetailsIndexEnvironmentListItemFragment_environment$key = {
  readonly " $data"?: ProjectDetailsIndexEnvironmentListItemFragment_environment$data;
  readonly " $fragmentSpreads": FragmentRefs<"ProjectDetailsIndexEnvironmentListItemFragment_environment">;
};

const node: ReaderFragment = (function(){
var v0 = {
  "alias": null,
  "args": null,
  "kind": "ScalarField",
  "name": "id",
  "storageKey": null
};
return {
  "argumentDefinitions": [],
  "kind": "Fragment",
  "metadata": null,
  "name": "ProjectDetailsIndexEnvironmentListItemFragment_environment",
  "selections": [
    (v0/*: any*/),
    {
      "alias": null,
      "args": null,
      "kind": "ScalarField",
      "name": "name",
      "storageKey": null
    },
    {
      "alias": null,
      "args": [
        {
          "kind": "Literal",
          "name": "completed",
          "value": true
        },
        {
          "kind": "Literal",
          "name": "first",
          "value": 1
        },
        {
          "kind": "Literal",
          "name": "pipelineTypes",
          "value": [
            "DEPLOYMENT"
          ]
        },
        {
          "kind": "Literal",
          "name": "sort",
          "value": "COMPLETED_AT_DESC"
        }
      ],
      "concreteType": "PipelineConnection",
      "kind": "LinkedField",
      "name": "pipelines",
      "plural": false,
      "selections": [
        {
          "alias": null,
          "args": null,
          "concreteType": "PipelineEdge",
          "kind": "LinkedField",
          "name": "edges",
          "plural": true,
          "selections": [
            {
              "alias": null,
              "args": null,
              "concreteType": "Pipeline",
              "kind": "LinkedField",
              "name": "node",
              "plural": false,
              "selections": [
                (v0/*: any*/),
                {
                  "alias": null,
                  "args": null,
                  "kind": "ScalarField",
                  "name": "type",
                  "storageKey": null
                },
                {
                  "alias": null,
                  "args": null,
                  "kind": "ScalarField",
                  "name": "environmentName",
                  "storageKey": null
                },
                {
                  "alias": null,
                  "args": null,
                  "concreteType": "Environment",
                  "kind": "LinkedField",
                  "name": "environment",
                  "plural": false,
                  "selections": [
                    (v0/*: any*/)
                  ],
                  "storageKey": null
                },
                {
                  "alias": null,
                  "args": null,
                  "concreteType": "PipelineTimestamps",
                  "kind": "LinkedField",
                  "name": "timestamps",
                  "plural": false,
                  "selections": [
                    {
                      "alias": null,
                      "args": null,
                      "kind": "ScalarField",
                      "name": "startedAt",
                      "storageKey": null
                    },
                    {
                      "alias": null,
                      "args": null,
                      "kind": "ScalarField",
                      "name": "completedAt",
                      "storageKey": null
                    }
                  ],
                  "storageKey": null
                },
                {
                  "alias": null,
                  "args": null,
                  "concreteType": "Release",
                  "kind": "LinkedField",
                  "name": "release",
                  "plural": false,
                  "selections": [
                    (v0/*: any*/),
                    {
                      "alias": null,
                      "args": null,
                      "kind": "ScalarField",
                      "name": "semanticVersion",
                      "storageKey": null
                    }
                  ],
                  "storageKey": null
                },
                {
                  "alias": null,
                  "args": null,
                  "kind": "ScalarField",
                  "name": "status",
                  "storageKey": null
                },
                {
                  "alias": null,
                  "args": null,
                  "concreteType": "PipelineAnnotation",
                  "kind": "LinkedField",
                  "name": "annotations",
                  "plural": true,
                  "selections": [
                    {
                      "alias": null,
                      "args": null,
                      "kind": "ScalarField",
                      "name": "key",
                      "storageKey": null
                    }
                  ],
                  "storageKey": null
                },
                {
                  "args": null,
                  "kind": "FragmentSpread",
                  "name": "PipelineAnnotationsFragment_pipeline"
                }
              ],
              "storageKey": null
            }
          ],
          "storageKey": null
        }
      ],
      "storageKey": "pipelines(completed:true,first:1,pipelineTypes:[\"DEPLOYMENT\"],sort:\"COMPLETED_AT_DESC\")"
    }
  ],
  "type": "Environment",
  "abstractKey": null
};
})();

(node as any).hash = "6a6d0ff1eebcbe42024f600595ab9f04";

export default node;
