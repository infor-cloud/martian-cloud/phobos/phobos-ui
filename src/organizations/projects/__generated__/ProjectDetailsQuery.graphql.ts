/**
 * @generated SignedSource<<0413cc6fc7b4682a36bab070a3262aab>>
 * @lightSyntaxTransform
 * @nogrep
 */

/* tslint:disable */
/* eslint-disable */
// @ts-nocheck

import { ConcreteRequest, Query } from 'relay-runtime';
import { FragmentRefs } from "relay-runtime";
export type ProjectDetailsQuery$variables = {
  prn: string;
};
export type ProjectDetailsQuery$data = {
  readonly node: {
    readonly " $fragmentSpreads": FragmentRefs<"ProjectDetailsFragment_project">;
  } | null | undefined;
};
export type ProjectDetailsQuery = {
  response: ProjectDetailsQuery$data;
  variables: ProjectDetailsQuery$variables;
};

const node: ConcreteRequest = (function(){
var v0 = [
  {
    "defaultValue": null,
    "kind": "LocalArgument",
    "name": "prn"
  }
],
v1 = [
  {
    "kind": "Variable",
    "name": "id",
    "variableName": "prn"
  }
],
v2 = {
  "alias": null,
  "args": null,
  "kind": "ScalarField",
  "name": "id",
  "storageKey": null
};
return {
  "fragment": {
    "argumentDefinitions": (v0/*: any*/),
    "kind": "Fragment",
    "metadata": null,
    "name": "ProjectDetailsQuery",
    "selections": [
      {
        "alias": null,
        "args": (v1/*: any*/),
        "concreteType": null,
        "kind": "LinkedField",
        "name": "node",
        "plural": false,
        "selections": [
          {
            "kind": "InlineFragment",
            "selections": [
              {
                "args": null,
                "kind": "FragmentSpread",
                "name": "ProjectDetailsFragment_project"
              }
            ],
            "type": "Project",
            "abstractKey": null
          }
        ],
        "storageKey": null
      }
    ],
    "type": "Query",
    "abstractKey": null
  },
  "kind": "Request",
  "operation": {
    "argumentDefinitions": (v0/*: any*/),
    "kind": "Operation",
    "name": "ProjectDetailsQuery",
    "selections": [
      {
        "alias": null,
        "args": (v1/*: any*/),
        "concreteType": null,
        "kind": "LinkedField",
        "name": "node",
        "plural": false,
        "selections": [
          {
            "alias": null,
            "args": null,
            "kind": "ScalarField",
            "name": "__typename",
            "storageKey": null
          },
          (v2/*: any*/),
          {
            "kind": "InlineFragment",
            "selections": [
              {
                "alias": null,
                "args": null,
                "kind": "ScalarField",
                "name": "name",
                "storageKey": null
              },
              {
                "alias": null,
                "args": null,
                "kind": "ScalarField",
                "name": "description",
                "storageKey": null
              },
              {
                "alias": null,
                "args": null,
                "concreteType": "ResourceMetadata",
                "kind": "LinkedField",
                "name": "metadata",
                "plural": false,
                "selections": [
                  {
                    "alias": null,
                    "args": null,
                    "kind": "ScalarField",
                    "name": "prn",
                    "storageKey": null
                  }
                ],
                "storageKey": null
              },
              {
                "alias": null,
                "args": null,
                "kind": "ScalarField",
                "name": "organizationId",
                "storageKey": null
              },
              {
                "alias": null,
                "args": null,
                "kind": "ScalarField",
                "name": "organizationName",
                "storageKey": null
              },
              {
                "alias": null,
                "args": null,
                "kind": "ScalarField",
                "name": "environmentNames",
                "storageKey": null
              },
              {
                "alias": null,
                "args": null,
                "concreteType": "ProjectVariableSet",
                "kind": "LinkedField",
                "name": "latestVariableSet",
                "plural": false,
                "selections": [
                  {
                    "alias": null,
                    "args": null,
                    "kind": "ScalarField",
                    "name": "revision",
                    "storageKey": null
                  },
                  (v2/*: any*/)
                ],
                "storageKey": null
              }
            ],
            "type": "Project",
            "abstractKey": null
          }
        ],
        "storageKey": null
      }
    ]
  },
  "params": {
    "cacheID": "a0bb25c1ea513ed1da98f9feb4f87c65",
    "id": null,
    "metadata": {},
    "name": "ProjectDetailsQuery",
    "operationKind": "query",
    "text": "query ProjectDetailsQuery(\n  $prn: String!\n) {\n  node(id: $prn) {\n    __typename\n    ... on Project {\n      ...ProjectDetailsFragment_project\n    }\n    id\n  }\n}\n\nfragment CreatePipelineFragment_project on Project {\n  id\n  name\n  organizationName\n  metadata {\n    prn\n  }\n  environmentNames\n}\n\nfragment DeploymentTemplateListItem_project on Project {\n  id\n}\n\nfragment EditEnvironmentFragment_project on Project {\n  metadata {\n    prn\n  }\n}\n\nfragment EditProjectApprovalRuleFragment_project on Project {\n  id\n  metadata {\n    prn\n  }\n}\n\nfragment EditProjectEnvironmentRuleFragment_project on Project {\n  id\n  metadata {\n    prn\n  }\n}\n\nfragment EditProjectReleaseLifecycleFragment_project on Project {\n  id\n  metadata {\n    prn\n  }\n}\n\nfragment EditProjectServiceAccountFragment_project on Project {\n  id\n  metadata {\n    prn\n  }\n}\n\nfragment EditProjectVCSProviderAuthSettingsFragment_project on Project {\n  name\n  metadata {\n    prn\n  }\n}\n\nfragment EditProjectVCSProviderFragment_project on Project {\n  metadata {\n    prn\n  }\n}\n\nfragment EditProjectVariablesFragment_project on Project {\n  id\n  name\n  latestVariableSet {\n    revision\n    id\n  }\n  organizationName\n}\n\nfragment EditReleaseDeploymentFragment_project on Project {\n  id\n  name\n  organizationName\n  metadata {\n    prn\n  }\n}\n\nfragment EditReleaseFragment_project on Project {\n  id\n  organizationName\n  metadata {\n    prn\n  }\n}\n\nfragment EnvironmentDetailsFragment_project on Project {\n  id\n  metadata {\n    prn\n  }\n}\n\nfragment EnvironmentListFragment_project on Project {\n  id\n  description\n}\n\nfragment EnvironmentsFragment_project on Project {\n  metadata {\n    prn\n  }\n  ...EnvironmentListFragment_project\n}\n\nfragment NewEnvironmentFragment_project on Project {\n  id\n  metadata {\n    prn\n  }\n}\n\nfragment NewPipelineTemplateFragment_pipelines on Project {\n  id\n  metadata {\n    prn\n  }\n}\n\nfragment NewPipelineTemplateVersionFragment_project on Project {\n  id\n  metadata {\n    prn\n  }\n}\n\nfragment NewProjectApprovalRuleFragment_project on Project {\n  id\n  metadata {\n    prn\n  }\n}\n\nfragment NewProjectEnvironmentRuleFragment_project on Project {\n  id\n  metadata {\n    prn\n  }\n  environmentNames\n}\n\nfragment NewProjectMembershipFragment_memberships on Project {\n  id\n  metadata {\n    prn\n  }\n}\n\nfragment NewProjectReleaseLifecycleFragment_project on Project {\n  id\n  metadata {\n    prn\n  }\n}\n\nfragment NewProjectServiceAccountFragment_project on Project {\n  id\n  metadata {\n    prn\n  }\n}\n\nfragment NewProjectVCSProviderFragment_project on Project {\n  id\n  name\n  metadata {\n    prn\n  }\n}\n\nfragment NewProjectVariablesFragment_project on Project {\n  id\n  metadata {\n    prn\n  }\n}\n\nfragment NewReleaseFragment_releases on Project {\n  id\n  name\n  organizationName\n  metadata {\n    prn\n  }\n}\n\nfragment PipelineDetailsFragment_details on Project {\n  metadata {\n    prn\n  }\n}\n\nfragment PipelineListFragment_project on Project {\n  id\n}\n\nfragment PipelineTemplateDetailsFragment_pipelines on Project {\n  id\n  metadata {\n    prn\n  }\n  ...PipelineTemplateVersionListFragment_project\n}\n\nfragment PipelineTemplateListFragment_project on Project {\n  id\n}\n\nfragment PipelineTemplateVersionListFragment_project on Project {\n  id\n}\n\nfragment PipelineTemplatesFragment_pipelines on Project {\n  metadata {\n    prn\n  }\n  ...PipelineTemplateListFragment_project\n}\n\nfragment PipelinesFragment_pipelines on Project {\n  metadata {\n    prn\n  }\n  ...PipelineListFragment_project\n}\n\nfragment ProjectActivityFragment_project on Project {\n  id\n  metadata {\n    prn\n  }\n}\n\nfragment ProjectAdvancedSettingsDeleteDialogFragment_project on Project {\n  name\n  organizationName\n}\n\nfragment ProjectAdvancedSettingsFragment_project on Project {\n  id\n  name\n  organizationId\n  ...ProjectAdvancedSettingsDeleteDialogFragment_project\n}\n\nfragment ProjectApprovalRulesFragment_project on Project {\n  id\n  name\n  metadata {\n    prn\n  }\n}\n\nfragment ProjectDetailsFragment_project on Project {\n  name\n  ...ProjectDetailsIndexFragment_project\n  ...ProjectSettingsFragment_project\n  ...ReleasesFragment_releases\n  ...NewReleaseFragment_releases\n  ...ProjectReleaseLifecyclesFragment_project\n  ...ProjectReleaseLifecycleDetailsFragment_project\n  ...NewProjectReleaseLifecycleFragment_project\n  ...EditProjectReleaseLifecycleFragment_project\n  ...ProjectServiceAccountsFragment_project\n  ...NewProjectServiceAccountFragment_project\n  ...ProjectServiceAccountDetailsFragment_project\n  ...EditProjectServiceAccountFragment_project\n  ...ProjectApprovalRulesFragment_project\n  ...NewProjectApprovalRuleFragment_project\n  ...EditProjectApprovalRuleFragment_project\n  ...EditReleaseFragment_project\n  ...EditReleaseDeploymentFragment_project\n  ...ProjectMembershipsFragment_project\n  ...NewProjectMembershipFragment_memberships\n  ...PipelinesFragment_pipelines\n  ...CreatePipelineFragment_project\n  ...PipelineDetailsFragment_details\n  ...PipelineTemplatesFragment_pipelines\n  ...PipelineTemplateDetailsFragment_pipelines\n  ...NewPipelineTemplateFragment_pipelines\n  ...NewPipelineTemplateVersionFragment_project\n  ...DeploymentTemplateListItem_project\n  ...ProjectActivityFragment_project\n  ...EnvironmentsFragment_project\n  ...NewEnvironmentFragment_project\n  ...EnvironmentDetailsFragment_project\n  ...EditEnvironmentFragment_project\n  ...ProjectVCSProvidersFragment_project\n  ...NewProjectVCSProviderFragment_project\n  ...ProjectVCSProviderDetailsFragment_project\n  ...EditProjectVCSProviderFragment_project\n  ...EditProjectVCSProviderAuthSettingsFragment_project\n  ...NewProjectEnvironmentRuleFragment_project\n  ...EditProjectEnvironmentRuleFragment_project\n  ...ProjectInsightsFragment_project\n  ...ProjectVariablesFragment_project\n  ...EditProjectVariablesFragment_project\n  ...NewProjectVariablesFragment_project\n}\n\nfragment ProjectDetailsIndexFragment_project on Project {\n  id\n  name\n  description\n  metadata {\n    prn\n  }\n  ...ProjectActivityFragment_project\n}\n\nfragment ProjectEnvironmentRules_project on Project {\n  id\n}\n\nfragment ProjectGeneralSettingsFragment_project on Project {\n  id\n  name\n  description\n}\n\nfragment ProjectInsightsFragment_project on Project {\n  id\n  metadata {\n    prn\n  }\n  environmentNames\n}\n\nfragment ProjectMembershipsFragment_project on Project {\n  id\n  metadata {\n    prn\n  }\n}\n\nfragment ProjectReleaseLifecycleDetailsFragment_project on Project {\n  id\n  name\n  metadata {\n    prn\n  }\n}\n\nfragment ProjectReleaseLifecyclesFragment_project on Project {\n  id\n  metadata {\n    prn\n  }\n}\n\nfragment ProjectServiceAccountDetailsFragment_project on Project {\n  id\n  metadata {\n    prn\n  }\n}\n\nfragment ProjectServiceAccountsFragment_project on Project {\n  id\n  metadata {\n    prn\n  }\n}\n\nfragment ProjectSettingsFragment_project on Project {\n  metadata {\n    prn\n  }\n  ...ProjectAdvancedSettingsFragment_project\n  ...ProjectGeneralSettingsFragment_project\n  ...ProjectEnvironmentRules_project\n}\n\nfragment ProjectVCSProviderDetailsFragment_project on Project {\n  metadata {\n    prn\n  }\n}\n\nfragment ProjectVCSProvidersFragment_project on Project {\n  id\n  metadata {\n    prn\n  }\n}\n\nfragment ProjectVariableSetFragment_project on Project {\n  name\n  organizationName\n  environmentNames\n}\n\nfragment ProjectVariablesFragment_project on Project {\n  metadata {\n    prn\n  }\n  id\n  latestVariableSet {\n    revision\n    id\n  }\n  environmentNames\n  ...ProjectVariableSetFragment_project\n  ...EditProjectVariablesFragment_project\n}\n\nfragment ReleaseListFragment_project on Project {\n  id\n}\n\nfragment ReleasesFragment_releases on Project {\n  metadata {\n    prn\n  }\n  ...ReleaseListFragment_project\n}\n"
  }
};
})();

(node as any).hash = "ebf40d384a59e1b56e2181f83a2493c9";

export default node;
