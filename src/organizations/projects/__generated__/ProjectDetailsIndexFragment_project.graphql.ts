/**
 * @generated SignedSource<<f6032b772985e528d5ded40694be4a45>>
 * @lightSyntaxTransform
 * @nogrep
 */

/* tslint:disable */
/* eslint-disable */
// @ts-nocheck

import { Fragment, ReaderFragment } from 'relay-runtime';
import { FragmentRefs } from "relay-runtime";
export type ProjectDetailsIndexFragment_project$data = {
  readonly description: string;
  readonly id: string;
  readonly metadata: {
    readonly prn: string;
  };
  readonly name: string;
  readonly " $fragmentSpreads": FragmentRefs<"ProjectActivityFragment_project">;
  readonly " $fragmentType": "ProjectDetailsIndexFragment_project";
};
export type ProjectDetailsIndexFragment_project$key = {
  readonly " $data"?: ProjectDetailsIndexFragment_project$data;
  readonly " $fragmentSpreads": FragmentRefs<"ProjectDetailsIndexFragment_project">;
};

const node: ReaderFragment = {
  "argumentDefinitions": [],
  "kind": "Fragment",
  "metadata": null,
  "name": "ProjectDetailsIndexFragment_project",
  "selections": [
    {
      "alias": null,
      "args": null,
      "kind": "ScalarField",
      "name": "id",
      "storageKey": null
    },
    {
      "alias": null,
      "args": null,
      "kind": "ScalarField",
      "name": "name",
      "storageKey": null
    },
    {
      "alias": null,
      "args": null,
      "kind": "ScalarField",
      "name": "description",
      "storageKey": null
    },
    {
      "alias": null,
      "args": null,
      "concreteType": "ResourceMetadata",
      "kind": "LinkedField",
      "name": "metadata",
      "plural": false,
      "selections": [
        {
          "alias": null,
          "args": null,
          "kind": "ScalarField",
          "name": "prn",
          "storageKey": null
        }
      ],
      "storageKey": null
    },
    {
      "args": null,
      "kind": "FragmentSpread",
      "name": "ProjectActivityFragment_project"
    }
  ],
  "type": "Project",
  "abstractKey": null
};

(node as any).hash = "d8a2aeaf4b7f7f69971556d6a301110c";

export default node;
