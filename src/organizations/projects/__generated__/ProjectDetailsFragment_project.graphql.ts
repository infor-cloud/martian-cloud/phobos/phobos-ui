/**
 * @generated SignedSource<<91485880710ecac4925b0a0e7b5104a1>>
 * @lightSyntaxTransform
 * @nogrep
 */

/* tslint:disable */
/* eslint-disable */
// @ts-nocheck

import { Fragment, ReaderFragment } from 'relay-runtime';
import { FragmentRefs } from "relay-runtime";
export type ProjectDetailsFragment_project$data = {
  readonly name: string;
  readonly " $fragmentSpreads": FragmentRefs<"CreatePipelineFragment_project" | "DeploymentTemplateListItem_project" | "EditEnvironmentFragment_project" | "EditProjectApprovalRuleFragment_project" | "EditProjectEnvironmentRuleFragment_project" | "EditProjectReleaseLifecycleFragment_project" | "EditProjectServiceAccountFragment_project" | "EditProjectVCSProviderAuthSettingsFragment_project" | "EditProjectVCSProviderFragment_project" | "EditProjectVariablesFragment_project" | "EditReleaseDeploymentFragment_project" | "EditReleaseFragment_project" | "EnvironmentDetailsFragment_project" | "EnvironmentsFragment_project" | "NewEnvironmentFragment_project" | "NewPipelineTemplateFragment_pipelines" | "NewPipelineTemplateVersionFragment_project" | "NewProjectApprovalRuleFragment_project" | "NewProjectEnvironmentRuleFragment_project" | "NewProjectMembershipFragment_memberships" | "NewProjectReleaseLifecycleFragment_project" | "NewProjectServiceAccountFragment_project" | "NewProjectVCSProviderFragment_project" | "NewProjectVariablesFragment_project" | "NewReleaseFragment_releases" | "PipelineDetailsFragment_details" | "PipelineTemplateDetailsFragment_pipelines" | "PipelineTemplatesFragment_pipelines" | "PipelinesFragment_pipelines" | "ProjectActivityFragment_project" | "ProjectApprovalRulesFragment_project" | "ProjectDetailsIndexFragment_project" | "ProjectInsightsFragment_project" | "ProjectMembershipsFragment_project" | "ProjectReleaseLifecycleDetailsFragment_project" | "ProjectReleaseLifecyclesFragment_project" | "ProjectServiceAccountDetailsFragment_project" | "ProjectServiceAccountsFragment_project" | "ProjectSettingsFragment_project" | "ProjectVCSProviderDetailsFragment_project" | "ProjectVCSProvidersFragment_project" | "ProjectVariablesFragment_project" | "ReleasesFragment_releases">;
  readonly " $fragmentType": "ProjectDetailsFragment_project";
};
export type ProjectDetailsFragment_project$key = {
  readonly " $data"?: ProjectDetailsFragment_project$data;
  readonly " $fragmentSpreads": FragmentRefs<"ProjectDetailsFragment_project">;
};

const node: ReaderFragment = {
  "argumentDefinitions": [],
  "kind": "Fragment",
  "metadata": null,
  "name": "ProjectDetailsFragment_project",
  "selections": [
    {
      "alias": null,
      "args": null,
      "kind": "ScalarField",
      "name": "name",
      "storageKey": null
    },
    {
      "args": null,
      "kind": "FragmentSpread",
      "name": "ProjectDetailsIndexFragment_project"
    },
    {
      "args": null,
      "kind": "FragmentSpread",
      "name": "ProjectSettingsFragment_project"
    },
    {
      "args": null,
      "kind": "FragmentSpread",
      "name": "ReleasesFragment_releases"
    },
    {
      "args": null,
      "kind": "FragmentSpread",
      "name": "NewReleaseFragment_releases"
    },
    {
      "args": null,
      "kind": "FragmentSpread",
      "name": "ProjectReleaseLifecyclesFragment_project"
    },
    {
      "args": null,
      "kind": "FragmentSpread",
      "name": "ProjectReleaseLifecycleDetailsFragment_project"
    },
    {
      "args": null,
      "kind": "FragmentSpread",
      "name": "NewProjectReleaseLifecycleFragment_project"
    },
    {
      "args": null,
      "kind": "FragmentSpread",
      "name": "EditProjectReleaseLifecycleFragment_project"
    },
    {
      "args": null,
      "kind": "FragmentSpread",
      "name": "ProjectServiceAccountsFragment_project"
    },
    {
      "args": null,
      "kind": "FragmentSpread",
      "name": "NewProjectServiceAccountFragment_project"
    },
    {
      "args": null,
      "kind": "FragmentSpread",
      "name": "ProjectServiceAccountDetailsFragment_project"
    },
    {
      "args": null,
      "kind": "FragmentSpread",
      "name": "EditProjectServiceAccountFragment_project"
    },
    {
      "args": null,
      "kind": "FragmentSpread",
      "name": "ProjectApprovalRulesFragment_project"
    },
    {
      "args": null,
      "kind": "FragmentSpread",
      "name": "NewProjectApprovalRuleFragment_project"
    },
    {
      "args": null,
      "kind": "FragmentSpread",
      "name": "EditProjectApprovalRuleFragment_project"
    },
    {
      "args": null,
      "kind": "FragmentSpread",
      "name": "EditReleaseFragment_project"
    },
    {
      "args": null,
      "kind": "FragmentSpread",
      "name": "EditReleaseDeploymentFragment_project"
    },
    {
      "args": null,
      "kind": "FragmentSpread",
      "name": "ProjectMembershipsFragment_project"
    },
    {
      "args": null,
      "kind": "FragmentSpread",
      "name": "NewProjectMembershipFragment_memberships"
    },
    {
      "args": null,
      "kind": "FragmentSpread",
      "name": "PipelinesFragment_pipelines"
    },
    {
      "args": null,
      "kind": "FragmentSpread",
      "name": "CreatePipelineFragment_project"
    },
    {
      "args": null,
      "kind": "FragmentSpread",
      "name": "PipelineDetailsFragment_details"
    },
    {
      "args": null,
      "kind": "FragmentSpread",
      "name": "PipelineTemplatesFragment_pipelines"
    },
    {
      "args": null,
      "kind": "FragmentSpread",
      "name": "PipelineTemplateDetailsFragment_pipelines"
    },
    {
      "args": null,
      "kind": "FragmentSpread",
      "name": "NewPipelineTemplateFragment_pipelines"
    },
    {
      "args": null,
      "kind": "FragmentSpread",
      "name": "NewPipelineTemplateVersionFragment_project"
    },
    {
      "args": null,
      "kind": "FragmentSpread",
      "name": "DeploymentTemplateListItem_project"
    },
    {
      "args": null,
      "kind": "FragmentSpread",
      "name": "ProjectActivityFragment_project"
    },
    {
      "args": null,
      "kind": "FragmentSpread",
      "name": "EnvironmentsFragment_project"
    },
    {
      "args": null,
      "kind": "FragmentSpread",
      "name": "NewEnvironmentFragment_project"
    },
    {
      "args": null,
      "kind": "FragmentSpread",
      "name": "EnvironmentDetailsFragment_project"
    },
    {
      "args": null,
      "kind": "FragmentSpread",
      "name": "EditEnvironmentFragment_project"
    },
    {
      "args": null,
      "kind": "FragmentSpread",
      "name": "ProjectVCSProvidersFragment_project"
    },
    {
      "args": null,
      "kind": "FragmentSpread",
      "name": "NewProjectVCSProviderFragment_project"
    },
    {
      "args": null,
      "kind": "FragmentSpread",
      "name": "ProjectVCSProviderDetailsFragment_project"
    },
    {
      "args": null,
      "kind": "FragmentSpread",
      "name": "EditProjectVCSProviderFragment_project"
    },
    {
      "args": null,
      "kind": "FragmentSpread",
      "name": "EditProjectVCSProviderAuthSettingsFragment_project"
    },
    {
      "args": null,
      "kind": "FragmentSpread",
      "name": "NewProjectEnvironmentRuleFragment_project"
    },
    {
      "args": null,
      "kind": "FragmentSpread",
      "name": "EditProjectEnvironmentRuleFragment_project"
    },
    {
      "args": null,
      "kind": "FragmentSpread",
      "name": "ProjectInsightsFragment_project"
    },
    {
      "args": null,
      "kind": "FragmentSpread",
      "name": "ProjectVariablesFragment_project"
    },
    {
      "args": null,
      "kind": "FragmentSpread",
      "name": "EditProjectVariablesFragment_project"
    },
    {
      "args": null,
      "kind": "FragmentSpread",
      "name": "NewProjectVariablesFragment_project"
    }
  ],
  "type": "Project",
  "abstractKey": null
};

(node as any).hash = "14102dd1fe2c9b8632e084cccbeb4cf4";

export default node;
