/**
 * @generated SignedSource<<00e4c120f949cfdc20bcdb71259316f9>>
 * @lightSyntaxTransform
 * @nogrep
 */

/* tslint:disable */
/* eslint-disable */
// @ts-nocheck

import { Fragment, ReaderFragment } from 'relay-runtime';
import { FragmentRefs } from "relay-runtime";
export type NewProjectFragment_organization$data = {
  readonly id: string;
  readonly name: string;
  readonly " $fragmentType": "NewProjectFragment_organization";
};
export type NewProjectFragment_organization$key = {
  readonly " $data"?: NewProjectFragment_organization$data;
  readonly " $fragmentSpreads": FragmentRefs<"NewProjectFragment_organization">;
};

const node: ReaderFragment = {
  "argumentDefinitions": [],
  "kind": "Fragment",
  "metadata": null,
  "name": "NewProjectFragment_organization",
  "selections": [
    {
      "alias": null,
      "args": null,
      "kind": "ScalarField",
      "name": "id",
      "storageKey": null
    },
    {
      "alias": null,
      "args": null,
      "kind": "ScalarField",
      "name": "name",
      "storageKey": null
    }
  ],
  "type": "Organization",
  "abstractKey": null
};

(node as any).hash = "ae3a5d1291d0bc1134b2ef2f9ccd9cbf";

export default node;
