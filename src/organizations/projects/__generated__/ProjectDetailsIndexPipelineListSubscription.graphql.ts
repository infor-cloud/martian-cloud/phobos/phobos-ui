/**
 * @generated SignedSource<<c0a8a295d875fe94e6d24dd66b6ae809>>
 * @lightSyntaxTransform
 * @nogrep
 */

/* tslint:disable */
/* eslint-disable */
// @ts-nocheck

import { ConcreteRequest, GraphQLSubscription } from 'relay-runtime';
import { FragmentRefs } from "relay-runtime";
export type ProjectPipelineEventsSubscriptionInput = {
  projectId: string;
};
export type ProjectDetailsIndexPipelineListSubscription$variables = {
  input: ProjectPipelineEventsSubscriptionInput;
};
export type ProjectDetailsIndexPipelineListSubscription$data = {
  readonly projectPipelineEvents: {
    readonly action: string;
    readonly pipeline: {
      readonly id: string;
      readonly " $fragmentSpreads": FragmentRefs<"ProjectDetailsIndexPipelineListItemFragment_pipeline">;
    };
  };
};
export type ProjectDetailsIndexPipelineListSubscription = {
  response: ProjectDetailsIndexPipelineListSubscription$data;
  variables: ProjectDetailsIndexPipelineListSubscription$variables;
};

const node: ConcreteRequest = (function(){
var v0 = [
  {
    "defaultValue": null,
    "kind": "LocalArgument",
    "name": "input"
  }
],
v1 = [
  {
    "kind": "Variable",
    "name": "input",
    "variableName": "input"
  }
],
v2 = {
  "alias": null,
  "args": null,
  "kind": "ScalarField",
  "name": "action",
  "storageKey": null
},
v3 = {
  "alias": null,
  "args": null,
  "kind": "ScalarField",
  "name": "id",
  "storageKey": null
};
return {
  "fragment": {
    "argumentDefinitions": (v0/*: any*/),
    "kind": "Fragment",
    "metadata": null,
    "name": "ProjectDetailsIndexPipelineListSubscription",
    "selections": [
      {
        "alias": null,
        "args": (v1/*: any*/),
        "concreteType": "PipelineEvent",
        "kind": "LinkedField",
        "name": "projectPipelineEvents",
        "plural": false,
        "selections": [
          (v2/*: any*/),
          {
            "alias": null,
            "args": null,
            "concreteType": "Pipeline",
            "kind": "LinkedField",
            "name": "pipeline",
            "plural": false,
            "selections": [
              (v3/*: any*/),
              {
                "args": null,
                "kind": "FragmentSpread",
                "name": "ProjectDetailsIndexPipelineListItemFragment_pipeline"
              }
            ],
            "storageKey": null
          }
        ],
        "storageKey": null
      }
    ],
    "type": "Subscription",
    "abstractKey": null
  },
  "kind": "Request",
  "operation": {
    "argumentDefinitions": (v0/*: any*/),
    "kind": "Operation",
    "name": "ProjectDetailsIndexPipelineListSubscription",
    "selections": [
      {
        "alias": null,
        "args": (v1/*: any*/),
        "concreteType": "PipelineEvent",
        "kind": "LinkedField",
        "name": "projectPipelineEvents",
        "plural": false,
        "selections": [
          (v2/*: any*/),
          {
            "alias": null,
            "args": null,
            "concreteType": "Pipeline",
            "kind": "LinkedField",
            "name": "pipeline",
            "plural": false,
            "selections": [
              (v3/*: any*/),
              {
                "alias": null,
                "args": null,
                "kind": "ScalarField",
                "name": "status",
                "storageKey": null
              },
              {
                "alias": null,
                "args": null,
                "kind": "ScalarField",
                "name": "type",
                "storageKey": null
              },
              {
                "alias": null,
                "args": null,
                "kind": "ScalarField",
                "name": "createdBy",
                "storageKey": null
              },
              {
                "alias": null,
                "args": null,
                "concreteType": "Release",
                "kind": "LinkedField",
                "name": "release",
                "plural": false,
                "selections": [
                  (v3/*: any*/),
                  {
                    "alias": null,
                    "args": null,
                    "kind": "ScalarField",
                    "name": "semanticVersion",
                    "storageKey": null
                  }
                ],
                "storageKey": null
              },
              {
                "alias": null,
                "args": null,
                "kind": "ScalarField",
                "name": "environmentName",
                "storageKey": null
              },
              {
                "alias": null,
                "args": null,
                "concreteType": "Environment",
                "kind": "LinkedField",
                "name": "environment",
                "plural": false,
                "selections": [
                  (v3/*: any*/)
                ],
                "storageKey": null
              },
              {
                "alias": null,
                "args": null,
                "concreteType": "PipelineTimestamps",
                "kind": "LinkedField",
                "name": "timestamps",
                "plural": false,
                "selections": [
                  {
                    "alias": null,
                    "args": null,
                    "kind": "ScalarField",
                    "name": "startedAt",
                    "storageKey": null
                  },
                  {
                    "alias": null,
                    "args": null,
                    "kind": "ScalarField",
                    "name": "completedAt",
                    "storageKey": null
                  }
                ],
                "storageKey": null
              },
              {
                "alias": null,
                "args": null,
                "concreteType": "PipelineAnnotation",
                "kind": "LinkedField",
                "name": "annotations",
                "plural": true,
                "selections": [
                  {
                    "alias": null,
                    "args": null,
                    "kind": "ScalarField",
                    "name": "key",
                    "storageKey": null
                  },
                  {
                    "alias": null,
                    "args": null,
                    "kind": "ScalarField",
                    "name": "value",
                    "storageKey": null
                  },
                  {
                    "alias": null,
                    "args": null,
                    "kind": "ScalarField",
                    "name": "link",
                    "storageKey": null
                  }
                ],
                "storageKey": null
              }
            ],
            "storageKey": null
          }
        ],
        "storageKey": null
      }
    ]
  },
  "params": {
    "cacheID": "7ca2de4a92ddf917d556b704fb90a5b7",
    "id": null,
    "metadata": {},
    "name": "ProjectDetailsIndexPipelineListSubscription",
    "operationKind": "subscription",
    "text": "subscription ProjectDetailsIndexPipelineListSubscription(\n  $input: ProjectPipelineEventsSubscriptionInput!\n) {\n  projectPipelineEvents(input: $input) {\n    action\n    pipeline {\n      id\n      ...ProjectDetailsIndexPipelineListItemFragment_pipeline\n    }\n  }\n}\n\nfragment PipelineAnnotationsFragment_pipeline on Pipeline {\n  annotations {\n    key\n    value\n    link\n  }\n}\n\nfragment ProjectDetailsIndexPipelineListItemFragment_pipeline on Pipeline {\n  id\n  status\n  type\n  createdBy\n  release {\n    id\n    semanticVersion\n  }\n  environmentName\n  environment {\n    id\n  }\n  timestamps {\n    startedAt\n    completedAt\n  }\n  annotations {\n    key\n  }\n  ...PipelineAnnotationsFragment_pipeline\n}\n"
  }
};
})();

(node as any).hash = "8231518554062a1cb03e284298967b32";

export default node;
