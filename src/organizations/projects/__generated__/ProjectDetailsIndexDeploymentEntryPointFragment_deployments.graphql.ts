/**
 * @generated SignedSource<<76f35bcefddd3ca0ffa991495fc9ce08>>
 * @lightSyntaxTransform
 * @nogrep
 */

/* tslint:disable */
/* eslint-disable */
// @ts-nocheck

import { ReaderFragment, RefetchableFragment } from 'relay-runtime';
import { FragmentRefs } from "relay-runtime";
export type ProjectDetailsIndexDeploymentEntryPointFragment_deployments$data = {
  readonly deployments: {
    readonly edges: ReadonlyArray<{
      readonly node: {
        readonly id: string;
        readonly timestamps: {
          readonly completedAt: any | null;
        };
      } | null;
    } | null> | null;
    readonly " $fragmentSpreads": FragmentRefs<"DeploymentListFragment_deployments">;
  };
  readonly id: string;
  readonly " $fragmentType": "ProjectDetailsIndexDeploymentEntryPointFragment_deployments";
};
export type ProjectDetailsIndexDeploymentEntryPointFragment_deployments$key = {
  readonly " $data"?: ProjectDetailsIndexDeploymentEntryPointFragment_deployments$data;
  readonly " $fragmentSpreads": FragmentRefs<"ProjectDetailsIndexDeploymentEntryPointFragment_deployments">;
};

const node: ReaderFragment = (function(){
var v0 = [
  "deployments"
],
v1 = {
  "alias": null,
  "args": null,
  "kind": "ScalarField",
  "name": "id",
  "storageKey": null
};
return {
  "argumentDefinitions": [
    {
      "kind": "RootArgument",
      "name": "after"
    },
    {
      "kind": "RootArgument",
      "name": "first"
    }
  ],
  "kind": "Fragment",
  "metadata": {
    "connection": [
      {
        "count": "first",
        "cursor": "after",
        "direction": "forward",
        "path": (v0/*: any*/)
      }
    ],
    "refetch": {
      "connection": {
        "forward": {
          "count": "first",
          "cursor": "after"
        },
        "backward": null,
        "path": (v0/*: any*/)
      },
      "fragmentPathInResult": [
        "node"
      ],
      "operation": require('./ProjectDetailsIndexDeploymentEntryPointFragmentPaginationQuery.graphql'),
      "identifierField": "id"
    }
  },
  "name": "ProjectDetailsIndexDeploymentEntryPointFragment_deployments",
  "selections": [
    {
      "alias": "deployments",
      "args": [
        {
          "kind": "Literal",
          "name": "sort",
          "value": "COMPLETED_AT_DESC"
        }
      ],
      "concreteType": "DeploymentConnection",
      "kind": "LinkedField",
      "name": "__ProjectDeploymentEntryPointFragment_deployments_connection",
      "plural": false,
      "selections": [
        {
          "alias": null,
          "args": null,
          "concreteType": "DeploymentEdge",
          "kind": "LinkedField",
          "name": "edges",
          "plural": true,
          "selections": [
            {
              "alias": null,
              "args": null,
              "concreteType": "Deployment",
              "kind": "LinkedField",
              "name": "node",
              "plural": false,
              "selections": [
                (v1/*: any*/),
                {
                  "alias": null,
                  "args": null,
                  "concreteType": "DeploymentTimestamps",
                  "kind": "LinkedField",
                  "name": "timestamps",
                  "plural": false,
                  "selections": [
                    {
                      "alias": null,
                      "args": null,
                      "kind": "ScalarField",
                      "name": "completedAt",
                      "storageKey": null
                    }
                  ],
                  "storageKey": null
                },
                {
                  "alias": null,
                  "args": null,
                  "kind": "ScalarField",
                  "name": "__typename",
                  "storageKey": null
                }
              ],
              "storageKey": null
            },
            {
              "alias": null,
              "args": null,
              "kind": "ScalarField",
              "name": "cursor",
              "storageKey": null
            }
          ],
          "storageKey": null
        },
        {
          "args": null,
          "kind": "FragmentSpread",
          "name": "DeploymentListFragment_deployments"
        },
        {
          "alias": null,
          "args": null,
          "concreteType": "PageInfo",
          "kind": "LinkedField",
          "name": "pageInfo",
          "plural": false,
          "selections": [
            {
              "alias": null,
              "args": null,
              "kind": "ScalarField",
              "name": "endCursor",
              "storageKey": null
            },
            {
              "alias": null,
              "args": null,
              "kind": "ScalarField",
              "name": "hasNextPage",
              "storageKey": null
            }
          ],
          "storageKey": null
        }
      ],
      "storageKey": "__ProjectDeploymentEntryPointFragment_deployments_connection(sort:\"COMPLETED_AT_DESC\")"
    },
    (v1/*: any*/)
  ],
  "type": "Project",
  "abstractKey": null
};
})();

(node as any).hash = "7dac836360290c7c22a66954fc80666a";

export default node;
