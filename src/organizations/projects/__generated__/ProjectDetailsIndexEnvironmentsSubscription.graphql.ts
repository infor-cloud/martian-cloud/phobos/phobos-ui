/**
 * @generated SignedSource<<e2f30f85f30bf5be410ac4be9f60deea>>
 * @lightSyntaxTransform
 * @nogrep
 */

/* tslint:disable */
/* eslint-disable */
// @ts-nocheck

import { ConcreteRequest, GraphQLSubscription } from 'relay-runtime';
import { FragmentRefs } from "relay-runtime";
export type ProjectPipelineEventsSubscriptionInput = {
  projectId: string;
};
export type ProjectDetailsIndexEnvironmentsSubscription$variables = {
  input: ProjectPipelineEventsSubscriptionInput;
};
export type ProjectDetailsIndexEnvironmentsSubscription$data = {
  readonly projectPipelineEvents: {
    readonly action: string;
    readonly pipeline: {
      readonly environment: {
        readonly " $fragmentSpreads": FragmentRefs<"ProjectDetailsIndexEnvironmentListItemFragment_environment">;
      } | null | undefined;
      readonly id: string;
    };
  };
};
export type ProjectDetailsIndexEnvironmentsSubscription = {
  response: ProjectDetailsIndexEnvironmentsSubscription$data;
  variables: ProjectDetailsIndexEnvironmentsSubscription$variables;
};

const node: ConcreteRequest = (function(){
var v0 = [
  {
    "defaultValue": null,
    "kind": "LocalArgument",
    "name": "input"
  }
],
v1 = [
  {
    "kind": "Variable",
    "name": "input",
    "variableName": "input"
  }
],
v2 = {
  "alias": null,
  "args": null,
  "kind": "ScalarField",
  "name": "action",
  "storageKey": null
},
v3 = {
  "alias": null,
  "args": null,
  "kind": "ScalarField",
  "name": "id",
  "storageKey": null
};
return {
  "fragment": {
    "argumentDefinitions": (v0/*: any*/),
    "kind": "Fragment",
    "metadata": null,
    "name": "ProjectDetailsIndexEnvironmentsSubscription",
    "selections": [
      {
        "alias": null,
        "args": (v1/*: any*/),
        "concreteType": "PipelineEvent",
        "kind": "LinkedField",
        "name": "projectPipelineEvents",
        "plural": false,
        "selections": [
          (v2/*: any*/),
          {
            "alias": null,
            "args": null,
            "concreteType": "Pipeline",
            "kind": "LinkedField",
            "name": "pipeline",
            "plural": false,
            "selections": [
              (v3/*: any*/),
              {
                "alias": null,
                "args": null,
                "concreteType": "Environment",
                "kind": "LinkedField",
                "name": "environment",
                "plural": false,
                "selections": [
                  {
                    "args": null,
                    "kind": "FragmentSpread",
                    "name": "ProjectDetailsIndexEnvironmentListItemFragment_environment"
                  }
                ],
                "storageKey": null
              }
            ],
            "storageKey": null
          }
        ],
        "storageKey": null
      }
    ],
    "type": "Subscription",
    "abstractKey": null
  },
  "kind": "Request",
  "operation": {
    "argumentDefinitions": (v0/*: any*/),
    "kind": "Operation",
    "name": "ProjectDetailsIndexEnvironmentsSubscription",
    "selections": [
      {
        "alias": null,
        "args": (v1/*: any*/),
        "concreteType": "PipelineEvent",
        "kind": "LinkedField",
        "name": "projectPipelineEvents",
        "plural": false,
        "selections": [
          (v2/*: any*/),
          {
            "alias": null,
            "args": null,
            "concreteType": "Pipeline",
            "kind": "LinkedField",
            "name": "pipeline",
            "plural": false,
            "selections": [
              (v3/*: any*/),
              {
                "alias": null,
                "args": null,
                "concreteType": "Environment",
                "kind": "LinkedField",
                "name": "environment",
                "plural": false,
                "selections": [
                  (v3/*: any*/),
                  {
                    "alias": null,
                    "args": null,
                    "kind": "ScalarField",
                    "name": "name",
                    "storageKey": null
                  },
                  {
                    "alias": null,
                    "args": [
                      {
                        "kind": "Literal",
                        "name": "completed",
                        "value": true
                      },
                      {
                        "kind": "Literal",
                        "name": "first",
                        "value": 1
                      },
                      {
                        "kind": "Literal",
                        "name": "pipelineTypes",
                        "value": [
                          "DEPLOYMENT"
                        ]
                      },
                      {
                        "kind": "Literal",
                        "name": "sort",
                        "value": "COMPLETED_AT_DESC"
                      }
                    ],
                    "concreteType": "PipelineConnection",
                    "kind": "LinkedField",
                    "name": "pipelines",
                    "plural": false,
                    "selections": [
                      {
                        "alias": null,
                        "args": null,
                        "concreteType": "PipelineEdge",
                        "kind": "LinkedField",
                        "name": "edges",
                        "plural": true,
                        "selections": [
                          {
                            "alias": null,
                            "args": null,
                            "concreteType": "Pipeline",
                            "kind": "LinkedField",
                            "name": "node",
                            "plural": false,
                            "selections": [
                              (v3/*: any*/),
                              {
                                "alias": null,
                                "args": null,
                                "kind": "ScalarField",
                                "name": "type",
                                "storageKey": null
                              },
                              {
                                "alias": null,
                                "args": null,
                                "kind": "ScalarField",
                                "name": "environmentName",
                                "storageKey": null
                              },
                              {
                                "alias": null,
                                "args": null,
                                "concreteType": "Environment",
                                "kind": "LinkedField",
                                "name": "environment",
                                "plural": false,
                                "selections": [
                                  (v3/*: any*/)
                                ],
                                "storageKey": null
                              },
                              {
                                "alias": null,
                                "args": null,
                                "concreteType": "PipelineTimestamps",
                                "kind": "LinkedField",
                                "name": "timestamps",
                                "plural": false,
                                "selections": [
                                  {
                                    "alias": null,
                                    "args": null,
                                    "kind": "ScalarField",
                                    "name": "startedAt",
                                    "storageKey": null
                                  },
                                  {
                                    "alias": null,
                                    "args": null,
                                    "kind": "ScalarField",
                                    "name": "completedAt",
                                    "storageKey": null
                                  }
                                ],
                                "storageKey": null
                              },
                              {
                                "alias": null,
                                "args": null,
                                "concreteType": "Release",
                                "kind": "LinkedField",
                                "name": "release",
                                "plural": false,
                                "selections": [
                                  (v3/*: any*/),
                                  {
                                    "alias": null,
                                    "args": null,
                                    "kind": "ScalarField",
                                    "name": "semanticVersion",
                                    "storageKey": null
                                  }
                                ],
                                "storageKey": null
                              },
                              {
                                "alias": null,
                                "args": null,
                                "kind": "ScalarField",
                                "name": "status",
                                "storageKey": null
                              },
                              {
                                "alias": null,
                                "args": null,
                                "concreteType": "PipelineAnnotation",
                                "kind": "LinkedField",
                                "name": "annotations",
                                "plural": true,
                                "selections": [
                                  {
                                    "alias": null,
                                    "args": null,
                                    "kind": "ScalarField",
                                    "name": "key",
                                    "storageKey": null
                                  },
                                  {
                                    "alias": null,
                                    "args": null,
                                    "kind": "ScalarField",
                                    "name": "value",
                                    "storageKey": null
                                  },
                                  {
                                    "alias": null,
                                    "args": null,
                                    "kind": "ScalarField",
                                    "name": "link",
                                    "storageKey": null
                                  }
                                ],
                                "storageKey": null
                              }
                            ],
                            "storageKey": null
                          }
                        ],
                        "storageKey": null
                      }
                    ],
                    "storageKey": "pipelines(completed:true,first:1,pipelineTypes:[\"DEPLOYMENT\"],sort:\"COMPLETED_AT_DESC\")"
                  }
                ],
                "storageKey": null
              }
            ],
            "storageKey": null
          }
        ],
        "storageKey": null
      }
    ]
  },
  "params": {
    "cacheID": "a747dece19e44f97e734a0718a483041",
    "id": null,
    "metadata": {},
    "name": "ProjectDetailsIndexEnvironmentsSubscription",
    "operationKind": "subscription",
    "text": "subscription ProjectDetailsIndexEnvironmentsSubscription(\n  $input: ProjectPipelineEventsSubscriptionInput!\n) {\n  projectPipelineEvents(input: $input) {\n    action\n    pipeline {\n      id\n      environment {\n        ...ProjectDetailsIndexEnvironmentListItemFragment_environment\n        id\n      }\n    }\n  }\n}\n\nfragment PipelineAnnotationsFragment_pipeline on Pipeline {\n  annotations {\n    key\n    value\n    link\n  }\n}\n\nfragment ProjectDetailsIndexEnvironmentListItemFragment_environment on Environment {\n  id\n  name\n  pipelines(first: 1, sort: COMPLETED_AT_DESC, completed: true, pipelineTypes: [DEPLOYMENT]) {\n    edges {\n      node {\n        id\n        type\n        environmentName\n        environment {\n          id\n        }\n        timestamps {\n          startedAt\n          completedAt\n        }\n        release {\n          id\n          semanticVersion\n        }\n        status\n        annotations {\n          key\n        }\n        ...PipelineAnnotationsFragment_pipeline\n      }\n    }\n  }\n}\n"
  }
};
})();

(node as any).hash = "5130a127835942a2e87969782ba3b230";

export default node;
