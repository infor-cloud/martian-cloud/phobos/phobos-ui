import { Box, Button, List, Paper, Typography, useTheme } from '@mui/material';
import graphql from 'babel-plugin-relay/macro';
import throttle from 'lodash.throttle';
import React, { useMemo, useState } from 'react';
import InfiniteScroll from 'react-infinite-scroll-component';
import { ConnectionHandler, fetchQuery, useFragment, useLazyLoadQuery, usePaginationFragment, useRelayEnvironment } from 'react-relay/hooks';
import { Link as RouterLink } from 'react-router-dom';
import { DESCRIPTION } from './PipelineTemplates';
import SearchInput from '../../../common/SearchInput';
import ListSkeleton from '../../../skeletons/ListSkeleton';
import PipelineTemplateListItem from './PipelineTemplateListItem';
import { PipelineTemplateListFragment_pipelineTemplates$key } from './__generated__/PipelineTemplateListFragment_pipelineTemplates.graphql';
import { PipelineTemplateListPaginationQuery } from './__generated__/PipelineTemplateListPaginationQuery.graphql';
import { PipelineTemplateListQuery } from './__generated__/PipelineTemplateListQuery.graphql';
import { PipelineTemplateListFragment_project$key } from './__generated__/PipelineTemplateListFragment_project.graphql';

export const INITIAL_ITEM_COUNT = 50;

export function GetConnections(projectId: string): [string] {
    const connectionId = ConnectionHandler.getConnectionID(
        projectId,
        "PipelineTemplateList_pipelineTemplates",
        { latest: true, versioned: true, sort: "UPDATED_AT_ASC" }
    );
    return [connectionId];
}

const query = graphql`
    query PipelineTemplateListQuery($id: String!, $first: Int!, $versioned: Boolean!, $search: String, $after: String) {
        node(id: $id) {
            ... on Project {
                ...PipelineTemplateListFragment_pipelineTemplates
            }
        }
    }`

interface Props {
    fragmentRef: PipelineTemplateListFragment_project$key
}

function PipelineTemplateList({ fragmentRef }: Props) {
    const [search, setSearch] = useState<string | undefined>('');
    const [isRefreshing, setIsRefreshing] = useState(false);
    const environment = useRelayEnvironment();
    const theme = useTheme();

    const project = useFragment<PipelineTemplateListFragment_project$key>(
        graphql`
        fragment PipelineTemplateListFragment_project on Project
        {
            id
        }
    `, fragmentRef);

    const queryData = useLazyLoadQuery<PipelineTemplateListQuery>(query, { first: 50, id: project.id, versioned: true }, { fetchPolicy: "store-and-network" })

    const { data, loadNext, hasNext, refetch } = usePaginationFragment<PipelineTemplateListPaginationQuery, PipelineTemplateListFragment_pipelineTemplates$key>(
        graphql`
    fragment PipelineTemplateListFragment_pipelineTemplates on Project
    @refetchable(queryName: "PipelineTemplateListPaginationQuery") {
        id
        pipelineTemplates(
            after: $after
            first: $first
            search: $search
            versioned: $versioned
            latest: true
            sort: UPDATED_AT_ASC
      ) @connection(key: "PipelineTemplateList_pipelineTemplates") {
        totalCount
          edges {
              node {
                  id
                  ...PipelineTemplateListItemFragment_pipelinetemplate
              }
          }
      }
    }
  `, queryData.node);

    const fetch = useMemo(
        () =>
            throttle(
                (input?: string) => {
                    setIsRefreshing(true);

                    const normalizedInput = input?.trim();

                    fetchQuery(environment, query, { id: project.id, first: INITIAL_ITEM_COUNT, search: normalizedInput, versioned: true })
                        .subscribe({
                            complete: () => {
                                setIsRefreshing(false);
                                setSearch(input);
                                // *After* the query has been fetched, we call
                                // refetch again to re-render with the updated data.
                                // At this point the data for the query should
                                // be cached, so we use the 'store-only'
                                // fetchPolicy to avoid suspending.
                                refetch({ first: INITIAL_ITEM_COUNT, search: normalizedInput }, { fetchPolicy: 'store-only' });
                            },
                            error: () => {
                                setIsRefreshing(false);
                            }
                        });
                },
                2000,
                { leading: false, trailing: true }
            ),
        [environment, refetch],
    );

    const onSearchChange = (event: React.ChangeEvent<HTMLInputElement>) => {
        fetch(event.target.value.toLowerCase());
    };

    const onKeyDown = (event: React.KeyboardEvent<HTMLInputElement>) => {
        // Only handle enter key type
        if (event.key === 'Enter') {
            fetch.flush();
        }
    };

    return (
        <Box >
            {(search !== '' || data?.pipelineTemplates?.edges?.length !== 0) && <Box>
                <Box>
                    <Box sx={{
                        display: 'flex',
                        flexDirection: 'row',
                        justifyContent: 'space-between',
                        [theme.breakpoints.down('md')]: {
                            flexDirection: 'column',
                            alignItems: 'flex-start',
                            '& > *': { mb: 2 },
                        }
                    }}>
                        <Box>
                            <Typography variant="h5" gutterBottom>Pipeline Templates</Typography>
                            <Typography variant="body2">
                                {DESCRIPTION}
                            </Typography>
                        </Box>
                        <Box>
                            <Button sx={{ minWidth: 200 }}
                                component={RouterLink}
                                variant="outlined"
                                to="new"
                            >
                                New Pipeline Template
                            </Button>
                        </Box>
                    </Box>
                    <SearchInput
                        sx={{ mt: 2, mb: 2 }}
                        placeholder="search for pipeline templates"
                        fullWidth
                        onChange={onSearchChange}
                        onKeyDown={onKeyDown}
                    />
                </Box>
                <Paper sx={{ borderBottomLeftRadius: 0, borderBottomRightRadius: 0, border: `1px solid ${theme.palette.divider}` }}>
                    <Box padding={2} display="flex" alignItems="center" justifyContent="space-between">
                        <Typography variant="subtitle1">
                            {data?.pipelineTemplates?.totalCount} pipeline template{data?.pipelineTemplates?.totalCount === 1 ? '' : 's'}
                        </Typography>
                    </Box>
                </Paper>
                {(!data?.pipelineTemplates?.edges || data?.pipelineTemplates?.edges.length === 0) && search !== '' && <Typography
                    sx={{
                        padding: 4,
                        borderBottom: `1px solid ${theme.palette.divider}`,
                        borderLeft: `1px solid ${theme.palette.divider}`,
                        borderRight: `1px solid ${theme.palette.divider}`,
                        borderBottomLeftRadius: 4,
                        borderBottomRightRadius: 4
                    }}
                    align="center"
                    color="textSecondary"
                >
                    No pipeline templates matching search <strong>{search}</strong>
                </Typography>}
                <InfiniteScroll
                    dataLength={data?.pipelineTemplates?.edges?.length ?? 0}
                    next={() => loadNext(20)}
                    hasMore={hasNext}
                    loader={<ListSkeleton rowCount={3} />}
                >
                    <List disablePadding sx={isRefreshing ? { opacity: 0.5 } : null}>
                        {data?.pipelineTemplates?.edges?.map((edge: any) => <PipelineTemplateListItem
                            key={edge.node.id}
                            fragmentRef={edge.node}
                        />)}
                    </List>
                </InfiniteScroll>
            </Box>}
            {!search && data?.pipelineTemplates?.edges?.length === 0 && <Box sx={{ mt: 4 }} display="flex" justifyContent="center">
                <Box p={4} display="flex" flexDirection="column" justifyContent="center" alignItems="center" sx={{ maxWidth: 600 }}>
                    <Typography variant="h6">Get started by creating a pipeline template</Typography>
                    <Typography color="textSecondary" align="center" sx={{ mb: 2 }}>
                        {DESCRIPTION}
                    </Typography>
                    <Button sx={{ minWidth: 200 }} component={RouterLink} variant="outlined" to="new">New Pipeline Template</Button>
                </Box>
            </Box>}
        </Box>
    );
}

export default PipelineTemplateList
