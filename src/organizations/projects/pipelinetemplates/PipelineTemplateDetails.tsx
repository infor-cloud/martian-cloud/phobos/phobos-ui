import ArrowDropDownIcon from "@mui/icons-material/ArrowDropDown";
import CopyIcon from '@mui/icons-material/ContentCopy';
import LoadingButton from '@mui/lab/LoadingButton';
import { Alert, Box, Button, ButtonGroup, Dialog, DialogActions, DialogContent, DialogTitle, IconButton, Menu, MenuItem, Stack, Tab, Tabs, Typography, useMediaQuery, useTheme } from '@mui/material';
import graphql from 'babel-plugin-relay/macro';
import { enqueueSnackbar } from 'notistack';
import React, { Suspense, useState } from 'react';
import { useFragment, useLazyLoadQuery, useMutation } from 'react-relay/hooks';
import { Link as RouterLink, useNavigate, useOutletContext, useParams, useSearchParams } from 'react-router-dom';
import { Prism as SyntaxHighlighter } from 'react-syntax-highlighter';
import { atomDark as prismTheme } from 'react-syntax-highlighter/dist/esm/styles/prism';
import PRNButton from '../../../common/PRNButton';
import ListSkeleton from '../../../skeletons/ListSkeleton';
import ProjectBreadcrumbs from '../ProjectBreadcrumbs';
import PipelineTemplateDetailsSidebar, { SidebarWidth } from './PipelineTemplateDetailsSidebar';
import PipelineTemplateVersionList, { GetConnections as GetVersionedPipelineTemplateConnections } from './PipelineTemplateVersionList';
import { GetConnections as GetPipelineTemplateConnections } from './PipelineTemplateList';
import { PipelineTemplateDetailsDeleteMutation } from './__generated__/PipelineTemplateDetailsDeleteMutation.graphql';
import { PipelineTemplateDetailsFragment_details$key } from './__generated__/PipelineTemplateDetailsFragment_details.graphql';
import { PipelineTemplateDetailsFragment_pipelines$key } from './__generated__/PipelineTemplateDetailsFragment_pipelines.graphql';
import { PipelineTemplateDetailsQuery } from './__generated__/PipelineTemplateDetailsQuery.graphql';

const query = graphql`
    query PipelineTemplateDetailsQuery($id: String!) {
        node(id: $id) {
            ... on PipelineTemplate {
                id
                ...PipelineTemplateDetailsFragment_details
            }
        }
    }`

interface ConfirmationDialogProps {
    deleteInProgress: boolean;
    onClose: (confirm?: boolean) => void;
}

function DeleteConfirmationDialog({ deleteInProgress, onClose, ...other }: ConfirmationDialogProps) {
    return (
        <Dialog maxWidth="xs" open={true} {...other}>
            <DialogTitle>Delete Pipeline Template</DialogTitle>
            <DialogContent dividers>
                Are you sure you want to delete this pipeline template?
            </DialogContent>
            <DialogActions>
                <Button color="inherit" onClick={() => onClose()}>
                    Cancel
                </Button>
                <LoadingButton
                    color="error"
                    loading={deleteInProgress}
                    onClick={() => onClose(true)}
                >
                    Delete
                </LoadingButton>
            </DialogActions>
        </Dialog>
    );
}

function PipelineTemplateDetails() {
    const { pipelineTemplateId } = useParams() as { pipelineTemplateId: string };
    const context = useOutletContext<PipelineTemplateDetailsFragment_pipelines$key>();
    const theme = useTheme();
    const navigate = useNavigate();
    const mobile = useMediaQuery(theme.breakpoints.down('md'));
    const [searchParams, setSearchParams] = useSearchParams();
    const [sidebarOpen, setSidebarOpen] = useState(false);
    const [menuAnchorEl, setMenuAnchorEl] = useState<Element | null>(null);
    const [showDeleteConfirmationDialog, setShowDeleteConfirmationDialog] = useState<boolean>(false);

    const queryData = useLazyLoadQuery<PipelineTemplateDetailsQuery>(query, { id: pipelineTemplateId }, { fetchPolicy: 'store-and-network' })

    const project = useFragment<PipelineTemplateDetailsFragment_pipelines$key>(
        graphql`
            fragment PipelineTemplateDetailsFragment_pipelines on Project
            {
                id
                metadata {
                    prn
                }
                ...PipelineTemplateVersionListFragment_project
            }
        `, context);

    const pipelineTemplate = useFragment<PipelineTemplateDetailsFragment_details$key>(
        graphql`
            fragment PipelineTemplateDetailsFragment_details on PipelineTemplate
            {
                id
                name
                status
                hclData
                semanticVersion
                metadata {
                    prn
                }
                ...PipelineTemplateDetailsSidebarFragment_details
            }
        `,
        queryData.node);

    const [commitDelete, commitDeleteInFlight] = useMutation<PipelineTemplateDetailsDeleteMutation>(graphql`
        mutation PipelineTemplateDetailsDeleteMutation(
            $input: DeletePipelineTemplateInput!
            $connections: [ID!]!
        ) {
            deletePipelineTemplate(input: $input) {
                pipelineTemplate {
                    id @deleteEdge(connections: $connections)
                }
                problems {
                    message
                    field
                    type
                }
            }
        }
    `);

    const onDeleteConfirmationDialogClosed = (confirm?: boolean) => {
        if (pipelineTemplate && confirm) {
            commitDelete({
                variables: {
                    input: {
                        id: pipelineTemplate.id,
                    },
                    connections: [
                        ...GetPipelineTemplateConnections(project.id),
                        ...(pipelineTemplate.name ? GetVersionedPipelineTemplateConnections(project.id, pipelineTemplate.name) : [])
                    ],
                },
                onCompleted: (data) => {
                    setShowDeleteConfirmationDialog(false);

                    if (data.deletePipelineTemplate.problems.length) {
                        enqueueSnackbar(
                            data.deletePipelineTemplate.problems
                                .map((problem: any) => problem.message)
                                .join("; "),
                            { variant: "warning" }
                        );
                    } else {
                        navigate(`..`);
                    }
                },
                onError: (error) => {
                    setShowDeleteConfirmationDialog(false);
                    enqueueSnackbar(`Unexpected error occurred: ${error.message}`, {
                        variant: "error",
                    });
                },
            });
        } else {
            setShowDeleteConfirmationDialog(false);
        }
    };

    const onOpenMenu = (event: React.MouseEvent<HTMLButtonElement>) => {
        setMenuAnchorEl(event.currentTarget);
    };

    const onMenuClose = () => {
        setMenuAnchorEl(null);
    };

    const onMenuAction = (actionCallback: () => void) => {
        setMenuAnchorEl(null);
        actionCallback();
    };

    const tab = searchParams.get('tab') || 'template';

    const onToggleSidebar = () => {
        setSidebarOpen(prev => !prev);
    };

    const onTabChange = (event: React.SyntheticEvent, newValue: string) => {
        searchParams.set('tab', newValue);
        setSearchParams(searchParams, { replace: true });
    };

    const showBanner = pipelineTemplate?.status !== 'uploaded';

    return pipelineTemplate ? (
        <Box>
            <PipelineTemplateDetailsSidebar
                fragmentRef={pipelineTemplate}
                open={sidebarOpen}
                temporary={mobile}
                onClose={onToggleSidebar}
            />
            <Box
                paddingRight={!mobile ? `${SidebarWidth}px` : 0}
            >
                <ProjectBreadcrumbs
                    prn={project.metadata.prn}
                    childRoutes={[
                        { title: "pipeline templates", path: 'pipeline_templates' },
                        { title: `${pipelineTemplateId.substring(0, 8)}...`, path: pipelineTemplateId }
                    ]}
                />
                {showBanner && <Alert sx={{ mb: 2, mt: 2 }} severity="warning" variant="outlined">No template data has been uploaded.
                </Alert>}
                <Box
                    sx={{
                        mb: 2,
                        display: 'flex',
                        flexDirection: 'row',
                        alignItems: 'flex-start',
                        justifyContent: 'space-between',
                        [theme.breakpoints.down('md')]: {
                            flexDirection: 'column',
                            '& > *': { mb: 2 },
                        }
                    }}>
                    <Box overflow="hidden" mr={2}>
                        <Typography variant="h5" noWrap>{pipelineTemplate.name}</Typography>
                    </Box>
                    <Stack direction="row" spacing={1}>
                        <PRNButton prn={pipelineTemplate.metadata.prn} />
                        {pipelineTemplate.semanticVersion && <ButtonGroup>
                            <Button
                                sx={{ width: 200 }}
                                component={RouterLink}
                                variant="outlined"
                                to={"new_version"}>
                                Create New Version
                            </Button>
                            <Button
                                color="primary"
                                size="small"
                                aria-label="more options menu"
                                aria-haspopup="menu"
                                onClick={onOpenMenu}
                            >
                                <ArrowDropDownIcon fontSize="small" />
                            </Button>
                        </ButtonGroup>}
                    </Stack>
                    <Menu
                        id="pipeline-template-more-options-menu"
                        anchorEl={menuAnchorEl}
                        open={Boolean(menuAnchorEl)}
                        onClose={onMenuClose}
                        anchorOrigin={{
                            vertical: "bottom",
                            horizontal: "right",
                        }}
                        transformOrigin={{
                            vertical: "top",
                            horizontal: "right",
                        }}
                    >
                        <MenuItem
                            onClick={() =>
                                onMenuAction(() => setShowDeleteConfirmationDialog(true))
                            }
                        >
                            Delete Pipeline Template
                        </MenuItem>
                    </Menu>
                </Box>
                <Box>
                    <Box>
                        {pipelineTemplate.name && <Box sx={{ border: 1, borderColor: 'divider', mb: 2 }}>
                            <Tabs value={tab} onChange={onTabChange}>
                                <Tab label="Template" value="template" />
                                {pipelineTemplate.semanticVersion && <Tab label="Versions" value="versions" />}
                            </Tabs>
                        </Box>}
                        <Box>
                            {tab === 'template' &&
                                <Box mt={2} position="relative">
                                    <IconButton
                                        sx={{
                                            p: 2,
                                            position: 'absolute',
                                            top: 0,
                                            right: 0
                                        }}
                                        onClick={() => navigator.clipboard.writeText(pipelineTemplate.hclData)}>
                                        <CopyIcon sx={{ width: 16, height: 16 }} />
                                    </IconButton>
                                    <SyntaxHighlighter
                                        wrapLines
                                        customStyle={{ fontSize: 13, minHeight: '60px' }}
                                        language="hcl"
                                        style={prismTheme}
                                        children={pipelineTemplate.hclData}
                                    />
                                </Box>
                            }
                            {pipelineTemplate.semanticVersion && (tab === 'versions' && <Box mt={2}>
                                <Suspense fallback={<ListSkeleton rowCount={3} />}>
                                    <PipelineTemplateVersionList fragmentRef={project} pipelineTemplateName={pipelineTemplate.name as string} />
                                </Suspense>
                            </Box>)}
                        </Box>
                    </Box>
                </Box>
            </Box>
            {showDeleteConfirmationDialog && <DeleteConfirmationDialog
                deleteInProgress={commitDeleteInFlight}
                onClose={onDeleteConfirmationDialogClosed}
            />}
        </Box>
    ) : null;
}

export default PipelineTemplateDetails
