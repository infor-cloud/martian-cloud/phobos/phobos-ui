import { Box, ListItemButton, Tooltip, Typography } from '@mui/material';
import Link from '@mui/material/Link';
import { useTheme } from '@mui/material/styles';
import graphql from 'babel-plugin-relay/macro';
import moment from 'moment';
import { useFragment } from 'react-relay/hooks';
import { Link as LinkRouter } from 'react-router-dom';
import Gravatar from '../../../common/Gravatar';
import { PipelineTemplateListItemFragment_pipelinetemplate$key } from './__generated__/PipelineTemplateListItemFragment_pipelinetemplate.graphql';

interface Props {
    fragmentRef: PipelineTemplateListItemFragment_pipelinetemplate$key
}

function PipelineTemplateListItem({ fragmentRef }: Props) {
    const theme = useTheme();

    const data = useFragment<PipelineTemplateListItemFragment_pipelinetemplate$key>(graphql`
        fragment PipelineTemplateListItemFragment_pipelinetemplate on PipelineTemplate {
            id
            name
            semanticVersion
            createdBy
            metadata {
                createdAt
            }
        }
    `, fragmentRef);

    return (
        <ListItemButton
            dense
            component={LinkRouter}
            to={data.id}
            sx={{
                borderBottom: `1px solid ${theme.palette.divider}`,
                borderLeft: `1px solid ${theme.palette.divider}`,
                borderRight: `1px solid ${theme.palette.divider}`,
                '&:last-child': {
                    borderBottomLeftRadius: 4,
                    borderBottomRightRadius: 4
                }
            }}
        >
            <Box flex={1} display="flex" justifyContent="space-between" alignItems="center">
                <Box>
                    <Link
                        component="div"
                        underline="hover"
                        variant="body1"
                        color="textPrimary"
                        sx={{ fontWeight: "500" }}
                    >
                        {data.name}
                    </Link>
                    <Box>
                        <Box display="flex" alignItems="center">
                            <Typography variant="body2" color="textSecondary">
                                {data.semanticVersion} created {moment(data.metadata.createdAt as moment.MomentInput).fromNow()} by
                            </Typography>
                            <Tooltip title={data.createdBy}>
                                <Box>
                                    <Gravatar width={16} height={16} sx={{ ml: 1, mr: 1 }} email={data.createdBy} />
                                </Box>
                            </Tooltip>
                        </Box>
                    </Box>
                </Box>
            </Box>
        </ListItemButton>
    );
}

export default PipelineTemplateListItem
