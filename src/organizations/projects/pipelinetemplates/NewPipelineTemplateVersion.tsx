import { useState } from 'react';
import { Box, Button, Divider, Typography } from '@mui/material';
import LoadingButton from '@mui/lab/LoadingButton';
import { MutationError } from '../../../common/error';
import graphql from 'babel-plugin-relay/macro';
import { useLazyLoadQuery, useMutation } from 'react-relay/hooks';
import ProjectBreadcrumbs from '../ProjectBreadcrumbs';
import { Link as RouterLink, useNavigate, useOutletContext, useParams } from 'react-router-dom';
import { useFragment } from 'react-relay/hooks';
import PipelineTemplateForm, { FormData } from './PipelineTemplateForm';
import { NewPipelineTemplateVersionQuery } from './__generated__/NewPipelineTemplateVersionQuery.graphql';
import { NewPipelineTemplateVersionLatestVersionQuery } from './__generated__/NewPipelineTemplateVersionLatestVersionQuery.graphql';
import { NewPipelineTemplateVersionFragment_pipelineTemplate$key } from './__generated__/NewPipelineTemplateVersionFragment_pipelineTemplate.graphql';
import { NewPipelineTemplateVersionFragment_project$key } from './__generated__/NewPipelineTemplateVersionFragment_project.graphql';
import { NewPipelineTemplateVersionMutation } from './__generated__/NewPipelineTemplateVersionMutation.graphql';

const query = graphql`
    query NewPipelineTemplateVersionQuery($id: String!) {
        node(id: $id) {
            ... on PipelineTemplate {
                id
                ...NewPipelineTemplateVersionFragment_pipelineTemplate
            }
        }
    }`

const latestVersionQuery = graphql`
    query NewPipelineTemplateVersionLatestVersionQuery($id: String!, $name: String!, $latest: Boolean!) {
        node(id: $id) {
            ... on Project {
                pipelineTemplates(first: 1, latest: $latest, name: $name) {
                    edges {
                        node {
                            semanticVersion
                        }
                    }
                }
            }
        }
    }`

function NewPipelineTemplateVersion() {
    const { pipelineTemplateId } = useParams() as { pipelineTemplateId: string };
    const navigate = useNavigate();
    const context = useOutletContext<NewPipelineTemplateVersionFragment_project$key>();

    const queryData = useLazyLoadQuery<NewPipelineTemplateVersionQuery>(query, { id: pipelineTemplateId }, { fetchPolicy: 'store-and-network' });

    const project = useFragment<NewPipelineTemplateVersionFragment_project$key>(
        graphql`
            fragment NewPipelineTemplateVersionFragment_project on Project
            {
                id
                metadata {
                    prn
                }
            }
        `, context
    );

    const data = useFragment<NewPipelineTemplateVersionFragment_pipelineTemplate$key>(
        graphql`
            fragment NewPipelineTemplateVersionFragment_pipelineTemplate on PipelineTemplate
            {
                name
                hclData
                semanticVersion
                ...PipelineTemplateDetailsSidebarFragment_details
            }
        `,
        queryData.node);

    const pipelineTemplate = data as any;

    const latestVersionData = useLazyLoadQuery<NewPipelineTemplateVersionLatestVersionQuery>(latestVersionQuery, { id: project.id, name: pipelineTemplate.name, latest: true }, { fetchPolicy: 'store-and-network' })

    const latestVersion = latestVersionData.node?.pipelineTemplates?.edges ? latestVersionData.node.pipelineTemplates?.edges[0]?.node?.semanticVersion : '';

    const [error, setError] = useState<MutationError>()
    const [formData, setFormData] = useState<FormData>({
        name: pipelineTemplate.name,
        hclData: pipelineTemplate.hclData,
        semanticVersion: ''
    });

    const [commit, isInFlight] = useMutation<NewPipelineTemplateVersionMutation>(graphql`
        mutation NewPipelineTemplateVersionMutation($input: CreatePipelineTemplateInput!) {
            createPipelineTemplate(input: $input) {
                pipelineTemplate {
                    id
                    name
                    hclData
                }
                problems {
                    message
                    field
                    type
                }
            }
        }`
    );

    const onSave = () => {
        commit({
            variables: {
                input: {
                    projectId: project.id,
                    name: formData.name,
                    hclData: formData.hclData || '',
                    versioned: true,
                    semanticVersion: formData.semanticVersion,
                }
            },
            onCompleted: (data) => {
                if (data.createPipelineTemplate.problems.length) {
                    setError({
                        severity: 'warning',
                        message: data.createPipelineTemplate.problems.map((problem: any) => problem.message).join('; ')
                    });
                } else if (!data.createPipelineTemplate.pipelineTemplate) {
                    setError({
                        severity: 'error',
                        message: "Unexpected error occurred"
                    });
                }
                else {
                    navigate(`../${data.createPipelineTemplate.pipelineTemplate.id}`);
                }
            },
            onError: error => {
                setError({
                    severity: 'error',
                    message: `Unexpected error occurred: ${error.message}`
                });
            }
        });
    };

    const disabled = !formData.hclData || !formData.semanticVersion;

    return (
        <Box>
            <ProjectBreadcrumbs
                prn={project.metadata.prn}
                childRoutes={[
                    { title: "pipeline templates", path: 'pipeline_templates' },
                    { title: `${pipelineTemplateId.substring(0, 8)}...`, path: pipelineTemplateId },
                    { title: "new version", path: 'new_version' }
                ]}
            />
            <Typography variant="h5">Create New Version</Typography>
            <PipelineTemplateForm
                data={formData}
                onChange={(data: FormData) => setFormData(data)}
                error={error}
                latestVersion={latestVersion}
            />
            <Divider sx={{ opacity: 0.6 }} />
            <Box mt={2}>
                <LoadingButton
                    loading={isInFlight}
                    disabled={disabled}
                    variant="outlined"
                    color="primary"
                    sx={{ mr: 2 }}
                    onClick={onSave}
                >
                    Create New Version
                </LoadingButton>
                <Button component={RouterLink} color="inherit" to={-1 as any}>Cancel</Button>
            </Box>
        </Box>
    );
}

export default NewPipelineTemplateVersion
