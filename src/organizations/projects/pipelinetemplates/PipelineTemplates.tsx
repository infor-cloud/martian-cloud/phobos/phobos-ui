import Box from '@mui/material/Box';
import graphql from 'babel-plugin-relay/macro';
import ProjectBreadcrumbs from '../ProjectBreadcrumbs';
import PipelineTemplateList from './PipelineTemplateList';
import { useOutletContext } from 'react-router-dom';
import { useFragment } from 'react-relay/hooks';
import { PipelineTemplatesFragment_pipelines$key } from './__generated__/PipelineTemplatesFragment_pipelines.graphql';

export const DESCRIPTION = 'Pipeline templates define the orchestration required to deploy a release to a particular environment.';

function PipelineTemplates() {
    const context = useOutletContext<PipelineTemplatesFragment_pipelines$key>();

    const data = useFragment<PipelineTemplatesFragment_pipelines$key>(
        graphql`
            fragment PipelineTemplatesFragment_pipelines on Project
            {
                metadata {
                    prn
                }
                ...PipelineTemplateListFragment_project
            }
        `, context);

    return (
        <Box>
            <ProjectBreadcrumbs
                prn={data.metadata.prn}
                childRoutes={[
                    { title: "pipeline templates", path: 'pipeline_templates' }
                ]}
            />
            <PipelineTemplateList fragmentRef={data} />
        </Box>
    );
}

export default PipelineTemplates
