import React from 'react';
import { Chip, styled, Toolbar, Tooltip, Typography, TypographyProps } from '@mui/material';
import Box from '@mui/material/Box';
import MuiDrawer, { DrawerProps } from '@mui/material/Drawer';
import graphql from 'babel-plugin-relay/macro';
import moment from 'moment';
import { useFragment } from 'react-relay/hooks';
import Gravatar from '../../../common/Gravatar';
import { PipelineTemplateDetailsSidebarFragment_details$key } from './__generated__/PipelineTemplateDetailsSidebarFragment_details.graphql';

interface Props {
    fragmentRef: PipelineTemplateDetailsSidebarFragment_details$key
    open: boolean
    temporary: boolean
    onClose: () => void
}

export const SidebarWidth = 300;

const Section = styled(Box)(() => ({
    marginBottom: 24,
}));

const FieldLabel = styled(
    ({ ...props }: TypographyProps) => <Typography color="textSecondary" variant="subtitle2" {...props} />
)(() => ({
    fontSize: 16,
    marginBottom: 1,
}));

const Drawer = styled(MuiDrawer)<DrawerProps>(() => ({
    flexShrink: 0,
    overflowX: 'hidden',
    [`& .MuiDrawer-paper`]: {
        overflowX: 'hidden',
        width: SidebarWidth,
        boxSizing: 'border-box'
    },
    width: SidebarWidth,
}));

function PipelineTemplateDetailsSidebar({ open, temporary, onClose, fragmentRef }: Props) {

    const data = useFragment<PipelineTemplateDetailsSidebarFragment_details$key>(graphql`
        fragment PipelineTemplateDetailsSidebarFragment_details on PipelineTemplate
        {
            name
            latest
            semanticVersion
            createdBy
            metadata {
                prn
                createdAt
            }
        }
    ` , fragmentRef);

    return (
        <Drawer
            variant={temporary ? 'temporary' : 'permanent'}
            open={open}
            hideBackdrop={false}
            anchor='right'
            onClose={onClose}
        >
            <Toolbar />
            <Box p={2}>
                {data.semanticVersion && <Section>
                    <FieldLabel>Version</FieldLabel>
                    <Box display="flex" alignItems="center">
                        <Typography>
                            {data.semanticVersion}
                        </Typography>
                        {data.latest && <Chip size="small" color="secondary" sx={{ ml: 1 }} label="latest" />}
                    </Box>
                </Section>}
                {data && <React.Fragment>
                    <Section>
                        <FieldLabel>Created</FieldLabel>
                        <Box display="flex" alignItems="center">
                            <Tooltip title={data.metadata.createdAt as string}>
                                <Typography sx={{ mr: 1 }}>
                                    {moment(data.metadata.createdAt as moment.MomentInput).fromNow()} by
                                </Typography>
                            </Tooltip>
                            <Tooltip title={data.createdBy}>
                                <Box>
                                    <Gravatar width={20} height={20} email={data.createdBy} />
                                </Box>
                            </Tooltip>
                        </Box>
                    </Section>
                </React.Fragment>}
            </Box>
        </Drawer>
    );
}

export default PipelineTemplateDetailsSidebar
