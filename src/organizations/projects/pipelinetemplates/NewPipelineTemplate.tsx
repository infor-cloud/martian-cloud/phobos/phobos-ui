import { useState } from 'react';
import { Box, Button, Divider, Typography } from '@mui/material';
import LoadingButton from '@mui/lab/LoadingButton';
import { MutationError } from '../../../common/error';
import graphql from 'babel-plugin-relay/macro';
import { useMutation } from 'react-relay/hooks';
import ProjectBreadcrumbs from '../ProjectBreadcrumbs';
import { Link as RouterLink, useNavigate, useOutletContext } from 'react-router-dom';
import { useFragment } from 'react-relay/hooks';
import { NewPipelineTemplateFragment_pipelines$key } from './__generated__/NewPipelineTemplateFragment_pipelines.graphql';
import { NewPipelineTemplateMutation } from './__generated__/NewPipelineTemplateMutation.graphql';
import PipelineTemplateForm, { FormData } from './PipelineTemplateForm';

function NewPipelineTemplate() {
    const navigate = useNavigate();
    const context = useOutletContext<NewPipelineTemplateFragment_pipelines$key>();

    const data = useFragment<NewPipelineTemplateFragment_pipelines$key>(
        graphql`
            fragment NewPipelineTemplateFragment_pipelines on Project
            {
                id
                metadata {
                    prn
                }
            }
        `, context);

    const [error, setError] = useState<MutationError>()
    const [formData, setFormData] = useState<FormData>({
        name: '',
        hclData: '',
        semanticVersion: ''
    });

    const [commit, isInFlight] = useMutation<NewPipelineTemplateMutation>(graphql`
        mutation NewPipelineTemplateMutation($input: CreatePipelineTemplateInput!) {
            createPipelineTemplate(input: $input) {
                pipelineTemplate {
                    id
                    name
                    hclData
                }
                problems {
                    message
                    field
                    type
                }
            }
        }`);

    const onSave = () => {
        commit({
            variables: {
                input: {
                    projectId: data.id,
                    name: formData.name,
                    hclData: formData.hclData || '',
                    versioned: true,
                    semanticVersion: formData.semanticVersion,
                }
            },
            onCompleted: (data) => {
                if (data.createPipelineTemplate.problems.length) {
                    setError({
                        severity: 'warning',
                        message: data.createPipelineTemplate.problems.map((problem: any) => problem.message).join('; ')
                    });
                } else if (!data.createPipelineTemplate.pipelineTemplate) {
                    setError({
                        severity: 'error',
                        message: "Unexpected error occurred"
                    });
                }
                else {
                    navigate(`../${data.createPipelineTemplate.pipelineTemplate.id}`);
                }
            },
            onError: error => {
                setError({
                    severity: 'error',
                    message: `Unexpected error occurred: ${error.message}`
                });
            }
        });
    };

    const disabled = formData.name === '' || !formData.hclData || formData.semanticVersion === '';

    return (
        <Box>
            <ProjectBreadcrumbs
                prn={data.metadata.prn}
                childRoutes={[
                    { title: "pipeline templates", path: 'pipeline_templates' },
                    { title: "new", path: 'new' }
                ]}
            />
            <Typography variant="h5">New Pipeline Template</Typography>
            <PipelineTemplateForm
                data={formData}
                onChange={(data: FormData) => setFormData(data)}
                error={error}
            />
            <Divider sx={{ opacity: 0.6 }} />
            <Box mt={2}>
                <LoadingButton
                    loading={isInFlight}
                    disabled={disabled}
                    variant="outlined"
                    color="primary"
                    sx={{ mr: 2 }}
                    onClick={onSave}
                >
                    Create Pipeline Template
                </LoadingButton>
                <Button component={RouterLink} color="inherit" to={-1 as any}>Cancel</Button>
            </Box>
        </Box>
    );
}

export default NewPipelineTemplate
