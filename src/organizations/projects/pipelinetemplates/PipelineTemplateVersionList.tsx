import { Box, List, Paper, Typography, useTheme } from '@mui/material';
import graphql from 'babel-plugin-relay/macro';
import InfiniteScroll from 'react-infinite-scroll-component';
import { ConnectionHandler, useFragment, useLazyLoadQuery, usePaginationFragment } from "react-relay/hooks";
import ListSkeleton from '../../../skeletons/ListSkeleton';
import PipelineTemplateVersionListItem from './PipelineTemplateVersionListItem';
import { PipelineTemplateVersionListFragment_project$key } from './__generated__/PipelineTemplateVersionListFragment_project.graphql';
import { PipelineTemplateVersionListFragment_pipelineTemplates$key } from './__generated__/PipelineTemplateVersionListFragment_pipelineTemplates.graphql';
import { PipelineTemplateVersionListPaginationQuery } from './__generated__/PipelineTemplateVersionListPaginationQuery.graphql';
import { PipelineTemplateVersionListQuery } from './__generated__/PipelineTemplateVersionListQuery.graphql';

const INITIAL_ITEM_COUNT = 100;

export function GetConnections(projectId: string, templateName: string): [string] {
    const connectionId = ConnectionHandler.getConnectionID(
        projectId,
        "PipelineTemplateVersionList_pipelineTemplates",
        { versioned: true, sort: "UPDATED_AT_DESC", name: templateName }
    );
    return [connectionId];
}

const query = graphql`
    query PipelineTemplateVersionListQuery($id: String!, $first: Int!, $last: Int, $after: String, $before: String, $versioned: Boolean!, $name: String!) {
        ...PipelineTemplateVersionListFragment_pipelineTemplates
    }
`;

interface Props {
    fragmentRef: PipelineTemplateVersionListFragment_project$key
    pipelineTemplateName: string
}

function PipelineTemplateVersionList({ fragmentRef, pipelineTemplateName }: Props) {
    const theme = useTheme();

    const project = useFragment<PipelineTemplateVersionListFragment_project$key>(
        graphql`
        fragment PipelineTemplateVersionListFragment_project on Project
        {
            id
        }
    `, fragmentRef);

    const queryData = useLazyLoadQuery<PipelineTemplateVersionListQuery>(query, { id: project.id, first: INITIAL_ITEM_COUNT, versioned: true, name: pipelineTemplateName }, { fetchPolicy: 'store-and-network' })

    const { data, loadNext, hasNext } = usePaginationFragment<PipelineTemplateVersionListPaginationQuery, PipelineTemplateVersionListFragment_pipelineTemplates$key>(
        graphql`
      fragment PipelineTemplateVersionListFragment_pipelineTemplates on Query
      @refetchable(queryName: "PipelineTemplateVersionListPaginationQuery") {
        node(id: $id) {
            ...on Project {
                pipelineTemplates(
                    after: $after
                    before: $before
                    first: $first
                    last: $last
                    versioned: $versioned
                    name: $name
                    sort: UPDATED_AT_DESC
                ) @connection(key: "PipelineTemplateVersionList_pipelineTemplates") {
                    totalCount
                    edges {
                        node {
                            id
                            ...PipelineTemplateVersionListItemFragment_version
                        }
                    }
                }
            }
        }
      }
    `, queryData);

    return (
        <Box>
            <Paper sx={{ borderBottomLeftRadius: 0, borderBottomRightRadius: 0, border: `1px solid ${theme.palette.divider}` }}>
                <Box padding={2} display="flex" alignItems="center" justifyContent="space-between">
                    <Typography variant="subtitle1">
                        {data.node?.pipelineTemplates?.edges?.length} version{data.node?.pipelineTemplates?.edges?.length === 1 ? '' : 's'}
                    </Typography>
                </Box>
            </Paper>
            <InfiniteScroll
                dataLength={data.node?.pipelineTemplates?.edges?.length ?? 0}
                next={() => loadNext(20)}
                hasMore={hasNext}
                loader={<ListSkeleton rowCount={3} />}
            >
                <List disablePadding>
                    {data.node?.pipelineTemplates?.edges?.map((edge: any) => <PipelineTemplateVersionListItem
                        key={edge.node.id}
                        fragmentRef={edge.node}
                    />)}
                </List>
            </InfiniteScroll>
        </Box>
    );
}

export default PipelineTemplateVersionList
