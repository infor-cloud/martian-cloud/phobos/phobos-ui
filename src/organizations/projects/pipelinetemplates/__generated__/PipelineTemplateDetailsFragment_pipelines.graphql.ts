/**
 * @generated SignedSource<<320ac777b900c43787f22545a55891d0>>
 * @lightSyntaxTransform
 * @nogrep
 */

/* tslint:disable */
/* eslint-disable */
// @ts-nocheck

import { Fragment, ReaderFragment } from 'relay-runtime';
import { FragmentRefs } from "relay-runtime";
export type PipelineTemplateDetailsFragment_pipelines$data = {
  readonly id: string;
  readonly metadata: {
    readonly prn: string;
  };
  readonly " $fragmentSpreads": FragmentRefs<"PipelineTemplateVersionListFragment_project">;
  readonly " $fragmentType": "PipelineTemplateDetailsFragment_pipelines";
};
export type PipelineTemplateDetailsFragment_pipelines$key = {
  readonly " $data"?: PipelineTemplateDetailsFragment_pipelines$data;
  readonly " $fragmentSpreads": FragmentRefs<"PipelineTemplateDetailsFragment_pipelines">;
};

const node: ReaderFragment = {
  "argumentDefinitions": [],
  "kind": "Fragment",
  "metadata": null,
  "name": "PipelineTemplateDetailsFragment_pipelines",
  "selections": [
    {
      "alias": null,
      "args": null,
      "kind": "ScalarField",
      "name": "id",
      "storageKey": null
    },
    {
      "alias": null,
      "args": null,
      "concreteType": "ResourceMetadata",
      "kind": "LinkedField",
      "name": "metadata",
      "plural": false,
      "selections": [
        {
          "alias": null,
          "args": null,
          "kind": "ScalarField",
          "name": "prn",
          "storageKey": null
        }
      ],
      "storageKey": null
    },
    {
      "args": null,
      "kind": "FragmentSpread",
      "name": "PipelineTemplateVersionListFragment_project"
    }
  ],
  "type": "Project",
  "abstractKey": null
};

(node as any).hash = "0a394a459cb2bc140891aafe5c36a2f4";

export default node;
