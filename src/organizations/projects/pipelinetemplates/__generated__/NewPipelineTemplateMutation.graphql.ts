/**
 * @generated SignedSource<<57d383823f600b60ad261fd21b3a6126>>
 * @lightSyntaxTransform
 * @nogrep
 */

/* tslint:disable */
/* eslint-disable */
// @ts-nocheck

import { ConcreteRequest, Mutation } from 'relay-runtime';
export type ProblemType = "BAD_REQUEST" | "CONFLICT" | "FORBIDDEN" | "NOT_FOUND" | "%future added value";
export type CreatePipelineTemplateInput = {
  clientMutationId?: string | null | undefined;
  hclData: string;
  name?: string | null | undefined;
  projectId: string;
  semanticVersion?: string | null | undefined;
  versioned: boolean;
};
export type NewPipelineTemplateMutation$variables = {
  input: CreatePipelineTemplateInput;
};
export type NewPipelineTemplateMutation$data = {
  readonly createPipelineTemplate: {
    readonly pipelineTemplate: {
      readonly hclData: string;
      readonly id: string;
      readonly name: string | null | undefined;
    } | null | undefined;
    readonly problems: ReadonlyArray<{
      readonly field: ReadonlyArray<string> | null | undefined;
      readonly message: string;
      readonly type: ProblemType;
    }>;
  };
};
export type NewPipelineTemplateMutation = {
  response: NewPipelineTemplateMutation$data;
  variables: NewPipelineTemplateMutation$variables;
};

const node: ConcreteRequest = (function(){
var v0 = [
  {
    "defaultValue": null,
    "kind": "LocalArgument",
    "name": "input"
  }
],
v1 = [
  {
    "alias": null,
    "args": [
      {
        "kind": "Variable",
        "name": "input",
        "variableName": "input"
      }
    ],
    "concreteType": "CreatePipelineTemplatePayload",
    "kind": "LinkedField",
    "name": "createPipelineTemplate",
    "plural": false,
    "selections": [
      {
        "alias": null,
        "args": null,
        "concreteType": "PipelineTemplate",
        "kind": "LinkedField",
        "name": "pipelineTemplate",
        "plural": false,
        "selections": [
          {
            "alias": null,
            "args": null,
            "kind": "ScalarField",
            "name": "id",
            "storageKey": null
          },
          {
            "alias": null,
            "args": null,
            "kind": "ScalarField",
            "name": "name",
            "storageKey": null
          },
          {
            "alias": null,
            "args": null,
            "kind": "ScalarField",
            "name": "hclData",
            "storageKey": null
          }
        ],
        "storageKey": null
      },
      {
        "alias": null,
        "args": null,
        "concreteType": "Problem",
        "kind": "LinkedField",
        "name": "problems",
        "plural": true,
        "selections": [
          {
            "alias": null,
            "args": null,
            "kind": "ScalarField",
            "name": "message",
            "storageKey": null
          },
          {
            "alias": null,
            "args": null,
            "kind": "ScalarField",
            "name": "field",
            "storageKey": null
          },
          {
            "alias": null,
            "args": null,
            "kind": "ScalarField",
            "name": "type",
            "storageKey": null
          }
        ],
        "storageKey": null
      }
    ],
    "storageKey": null
  }
];
return {
  "fragment": {
    "argumentDefinitions": (v0/*: any*/),
    "kind": "Fragment",
    "metadata": null,
    "name": "NewPipelineTemplateMutation",
    "selections": (v1/*: any*/),
    "type": "Mutation",
    "abstractKey": null
  },
  "kind": "Request",
  "operation": {
    "argumentDefinitions": (v0/*: any*/),
    "kind": "Operation",
    "name": "NewPipelineTemplateMutation",
    "selections": (v1/*: any*/)
  },
  "params": {
    "cacheID": "d08719f51aba53400bdd1134d48e553b",
    "id": null,
    "metadata": {},
    "name": "NewPipelineTemplateMutation",
    "operationKind": "mutation",
    "text": "mutation NewPipelineTemplateMutation(\n  $input: CreatePipelineTemplateInput!\n) {\n  createPipelineTemplate(input: $input) {\n    pipelineTemplate {\n      id\n      name\n      hclData\n    }\n    problems {\n      message\n      field\n      type\n    }\n  }\n}\n"
  }
};
})();

(node as any).hash = "956971396b85139c8b2b5a945c1b00c5";

export default node;
