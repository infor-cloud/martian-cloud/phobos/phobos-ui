/**
 * @generated SignedSource<<f8eb55973f6288e8dc3da29196feab75>>
 * @lightSyntaxTransform
 * @nogrep
 */

/* tslint:disable */
/* eslint-disable */
// @ts-nocheck

import { Fragment, ReaderFragment } from 'relay-runtime';
import { FragmentRefs } from "relay-runtime";
export type NewPipelineTemplateVersionFragment_pipelineTemplate$data = {
  readonly hclData: string;
  readonly name: string | null | undefined;
  readonly semanticVersion: string | null | undefined;
  readonly " $fragmentSpreads": FragmentRefs<"PipelineTemplateDetailsSidebarFragment_details">;
  readonly " $fragmentType": "NewPipelineTemplateVersionFragment_pipelineTemplate";
};
export type NewPipelineTemplateVersionFragment_pipelineTemplate$key = {
  readonly " $data"?: NewPipelineTemplateVersionFragment_pipelineTemplate$data;
  readonly " $fragmentSpreads": FragmentRefs<"NewPipelineTemplateVersionFragment_pipelineTemplate">;
};

const node: ReaderFragment = {
  "argumentDefinitions": [],
  "kind": "Fragment",
  "metadata": null,
  "name": "NewPipelineTemplateVersionFragment_pipelineTemplate",
  "selections": [
    {
      "alias": null,
      "args": null,
      "kind": "ScalarField",
      "name": "name",
      "storageKey": null
    },
    {
      "alias": null,
      "args": null,
      "kind": "ScalarField",
      "name": "hclData",
      "storageKey": null
    },
    {
      "alias": null,
      "args": null,
      "kind": "ScalarField",
      "name": "semanticVersion",
      "storageKey": null
    },
    {
      "args": null,
      "kind": "FragmentSpread",
      "name": "PipelineTemplateDetailsSidebarFragment_details"
    }
  ],
  "type": "PipelineTemplate",
  "abstractKey": null
};

(node as any).hash = "8135415148490cdb26b4bb12e62e1b50";

export default node;
