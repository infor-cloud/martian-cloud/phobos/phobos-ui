/**
 * @generated SignedSource<<53b45d069e6a65e6518db3884e0bcf3e>>
 * @lightSyntaxTransform
 * @nogrep
 */

/* tslint:disable */
/* eslint-disable */
// @ts-nocheck

import { Fragment, ReaderFragment } from 'relay-runtime';
import { FragmentRefs } from "relay-runtime";
export type PipelineTemplateSearchListItemFragment_pipelinetemplate$data = {
  readonly createdBy: string;
  readonly id: string;
  readonly latest: boolean;
  readonly metadata: {
    readonly createdAt: any;
  };
  readonly name: string | null;
  readonly semanticVersion: string | null;
  readonly versioned: boolean;
  readonly " $fragmentType": "PipelineTemplateSearchListItemFragment_pipelinetemplate";
};
export type PipelineTemplateSearchListItemFragment_pipelinetemplate$key = {
  readonly " $data"?: PipelineTemplateSearchListItemFragment_pipelinetemplate$data;
  readonly " $fragmentSpreads": FragmentRefs<"PipelineTemplateSearchListItemFragment_pipelinetemplate">;
};

const node: ReaderFragment = {
  "argumentDefinitions": [],
  "kind": "Fragment",
  "metadata": null,
  "name": "PipelineTemplateSearchListItemFragment_pipelinetemplate",
  "selections": [
    {
      "alias": null,
      "args": null,
      "kind": "ScalarField",
      "name": "id",
      "storageKey": null
    },
    {
      "alias": null,
      "args": null,
      "kind": "ScalarField",
      "name": "name",
      "storageKey": null
    },
    {
      "alias": null,
      "args": null,
      "kind": "ScalarField",
      "name": "versioned",
      "storageKey": null
    },
    {
      "alias": null,
      "args": null,
      "kind": "ScalarField",
      "name": "semanticVersion",
      "storageKey": null
    },
    {
      "alias": null,
      "args": null,
      "kind": "ScalarField",
      "name": "latest",
      "storageKey": null
    },
    {
      "alias": null,
      "args": null,
      "kind": "ScalarField",
      "name": "createdBy",
      "storageKey": null
    },
    {
      "alias": null,
      "args": null,
      "concreteType": "ResourceMetadata",
      "kind": "LinkedField",
      "name": "metadata",
      "plural": false,
      "selections": [
        {
          "alias": null,
          "args": null,
          "kind": "ScalarField",
          "name": "createdAt",
          "storageKey": null
        }
      ],
      "storageKey": null
    }
  ],
  "type": "PipelineTemplate",
  "abstractKey": null
};

(node as any).hash = "208f6ba999ee94b7bdc595494727a15a";

export default node;
