/**
 * @generated SignedSource<<4e3cfb245a6eb2e4d747a61ed25a1aa4>>
 * @lightSyntaxTransform
 * @nogrep
 */

/* tslint:disable */
/* eslint-disable */
// @ts-nocheck

import { Fragment, ReaderFragment } from 'relay-runtime';
import { FragmentRefs } from "relay-runtime";
export type NewPipelineTemplateVersionFragment_pipelines$data = {
  readonly id: string;
  readonly metadata: {
    readonly prn: string;
  };
  readonly " $fragmentType": "NewPipelineTemplateVersionFragment_pipelines";
};
export type NewPipelineTemplateVersionFragment_pipelines$key = {
  readonly " $data"?: NewPipelineTemplateVersionFragment_pipelines$data;
  readonly " $fragmentSpreads": FragmentRefs<"NewPipelineTemplateVersionFragment_pipelines">;
};

const node: ReaderFragment = {
  "argumentDefinitions": [],
  "kind": "Fragment",
  "metadata": null,
  "name": "NewPipelineTemplateVersionFragment_pipelines",
  "selections": [
    {
      "alias": null,
      "args": null,
      "kind": "ScalarField",
      "name": "id",
      "storageKey": null
    },
    {
      "alias": null,
      "args": null,
      "concreteType": "ResourceMetadata",
      "kind": "LinkedField",
      "name": "metadata",
      "plural": false,
      "selections": [
        {
          "alias": null,
          "args": null,
          "kind": "ScalarField",
          "name": "prn",
          "storageKey": null
        }
      ],
      "storageKey": null
    }
  ],
  "type": "Project",
  "abstractKey": null
};

(node as any).hash = "5e2791de9123a6c19027cb75593ceada";

export default node;
