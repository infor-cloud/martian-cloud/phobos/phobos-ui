/**
 * @generated SignedSource<<687c48f882e1a88f47bcec2b3069a35e>>
 * @lightSyntaxTransform
 * @nogrep
 */

/* tslint:disable */
/* eslint-disable */
// @ts-nocheck

import { Fragment, ReaderFragment } from 'relay-runtime';
import { FragmentRefs } from "relay-runtime";
export type PipelineTemplateVersionListItemFragment_version$data = {
  readonly createdBy: string;
  readonly id: string;
  readonly latest: boolean;
  readonly metadata: {
    readonly createdAt: any;
  };
  readonly semanticVersion: string | null | undefined;
  readonly " $fragmentType": "PipelineTemplateVersionListItemFragment_version";
};
export type PipelineTemplateVersionListItemFragment_version$key = {
  readonly " $data"?: PipelineTemplateVersionListItemFragment_version$data;
  readonly " $fragmentSpreads": FragmentRefs<"PipelineTemplateVersionListItemFragment_version">;
};

const node: ReaderFragment = {
  "argumentDefinitions": [],
  "kind": "Fragment",
  "metadata": null,
  "name": "PipelineTemplateVersionListItemFragment_version",
  "selections": [
    {
      "alias": null,
      "args": null,
      "kind": "ScalarField",
      "name": "id",
      "storageKey": null
    },
    {
      "alias": null,
      "args": null,
      "kind": "ScalarField",
      "name": "semanticVersion",
      "storageKey": null
    },
    {
      "alias": null,
      "args": null,
      "kind": "ScalarField",
      "name": "createdBy",
      "storageKey": null
    },
    {
      "alias": null,
      "args": null,
      "kind": "ScalarField",
      "name": "latest",
      "storageKey": null
    },
    {
      "alias": null,
      "args": null,
      "concreteType": "ResourceMetadata",
      "kind": "LinkedField",
      "name": "metadata",
      "plural": false,
      "selections": [
        {
          "alias": null,
          "args": null,
          "kind": "ScalarField",
          "name": "createdAt",
          "storageKey": null
        }
      ],
      "storageKey": null
    }
  ],
  "type": "PipelineTemplate",
  "abstractKey": null
};

(node as any).hash = "895b85809dfa38ceae2e5e1f36d49625";

export default node;
