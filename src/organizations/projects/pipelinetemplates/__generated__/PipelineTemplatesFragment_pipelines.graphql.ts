/**
 * @generated SignedSource<<f6a415d644716aae7c98df5a9cd825b6>>
 * @lightSyntaxTransform
 * @nogrep
 */

/* tslint:disable */
/* eslint-disable */
// @ts-nocheck

import { Fragment, ReaderFragment } from 'relay-runtime';
import { FragmentRefs } from "relay-runtime";
export type PipelineTemplatesFragment_pipelines$data = {
  readonly metadata: {
    readonly prn: string;
  };
  readonly " $fragmentSpreads": FragmentRefs<"PipelineTemplateListFragment_project">;
  readonly " $fragmentType": "PipelineTemplatesFragment_pipelines";
};
export type PipelineTemplatesFragment_pipelines$key = {
  readonly " $data"?: PipelineTemplatesFragment_pipelines$data;
  readonly " $fragmentSpreads": FragmentRefs<"PipelineTemplatesFragment_pipelines">;
};

const node: ReaderFragment = {
  "argumentDefinitions": [],
  "kind": "Fragment",
  "metadata": null,
  "name": "PipelineTemplatesFragment_pipelines",
  "selections": [
    {
      "alias": null,
      "args": null,
      "concreteType": "ResourceMetadata",
      "kind": "LinkedField",
      "name": "metadata",
      "plural": false,
      "selections": [
        {
          "alias": null,
          "args": null,
          "kind": "ScalarField",
          "name": "prn",
          "storageKey": null
        }
      ],
      "storageKey": null
    },
    {
      "args": null,
      "kind": "FragmentSpread",
      "name": "PipelineTemplateListFragment_project"
    }
  ],
  "type": "Project",
  "abstractKey": null
};

(node as any).hash = "b264620ef0f25feebfc7fec5a165dbd8";

export default node;
