/**
 * @generated SignedSource<<5460436a820bdf7787c090fe88691de3>>
 * @lightSyntaxTransform
 * @nogrep
 */

/* tslint:disable */
/* eslint-disable */
// @ts-nocheck

import { Fragment, ReaderFragment } from 'relay-runtime';
import { FragmentRefs } from "relay-runtime";
export type PipelineTemplateVersionListFragment_project$data = {
  readonly id: string;
  readonly " $fragmentType": "PipelineTemplateVersionListFragment_project";
};
export type PipelineTemplateVersionListFragment_project$key = {
  readonly " $data"?: PipelineTemplateVersionListFragment_project$data;
  readonly " $fragmentSpreads": FragmentRefs<"PipelineTemplateVersionListFragment_project">;
};

const node: ReaderFragment = {
  "argumentDefinitions": [],
  "kind": "Fragment",
  "metadata": null,
  "name": "PipelineTemplateVersionListFragment_project",
  "selections": [
    {
      "alias": null,
      "args": null,
      "kind": "ScalarField",
      "name": "id",
      "storageKey": null
    }
  ],
  "type": "Project",
  "abstractKey": null
};

(node as any).hash = "6c60cf23061a44dc53fdce5d0b59a652";

export default node;
