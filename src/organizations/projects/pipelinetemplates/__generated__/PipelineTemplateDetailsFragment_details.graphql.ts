/**
 * @generated SignedSource<<d49d3000c2594d46373b6855931ff639>>
 * @lightSyntaxTransform
 * @nogrep
 */

/* tslint:disable */
/* eslint-disable */
// @ts-nocheck

import { Fragment, ReaderFragment } from 'relay-runtime';
import { FragmentRefs } from "relay-runtime";
export type PipelineTemplateDetailsFragment_details$data = {
  readonly hclData: string;
  readonly id: string;
  readonly metadata: {
    readonly prn: string;
  };
  readonly name: string | null | undefined;
  readonly semanticVersion: string | null | undefined;
  readonly status: string;
  readonly " $fragmentSpreads": FragmentRefs<"PipelineTemplateDetailsSidebarFragment_details">;
  readonly " $fragmentType": "PipelineTemplateDetailsFragment_details";
};
export type PipelineTemplateDetailsFragment_details$key = {
  readonly " $data"?: PipelineTemplateDetailsFragment_details$data;
  readonly " $fragmentSpreads": FragmentRefs<"PipelineTemplateDetailsFragment_details">;
};

const node: ReaderFragment = {
  "argumentDefinitions": [],
  "kind": "Fragment",
  "metadata": null,
  "name": "PipelineTemplateDetailsFragment_details",
  "selections": [
    {
      "alias": null,
      "args": null,
      "kind": "ScalarField",
      "name": "id",
      "storageKey": null
    },
    {
      "alias": null,
      "args": null,
      "kind": "ScalarField",
      "name": "name",
      "storageKey": null
    },
    {
      "alias": null,
      "args": null,
      "kind": "ScalarField",
      "name": "status",
      "storageKey": null
    },
    {
      "alias": null,
      "args": null,
      "kind": "ScalarField",
      "name": "hclData",
      "storageKey": null
    },
    {
      "alias": null,
      "args": null,
      "kind": "ScalarField",
      "name": "semanticVersion",
      "storageKey": null
    },
    {
      "alias": null,
      "args": null,
      "concreteType": "ResourceMetadata",
      "kind": "LinkedField",
      "name": "metadata",
      "plural": false,
      "selections": [
        {
          "alias": null,
          "args": null,
          "kind": "ScalarField",
          "name": "prn",
          "storageKey": null
        }
      ],
      "storageKey": null
    },
    {
      "args": null,
      "kind": "FragmentSpread",
      "name": "PipelineTemplateDetailsSidebarFragment_details"
    }
  ],
  "type": "PipelineTemplate",
  "abstractKey": null
};

(node as any).hash = "e759413272755571288fee10bd7b82a1";

export default node;
