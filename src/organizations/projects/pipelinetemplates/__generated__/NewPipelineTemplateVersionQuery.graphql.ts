/**
 * @generated SignedSource<<ad682a2e5c32b99f35bd19a53bbc78b4>>
 * @lightSyntaxTransform
 * @nogrep
 */

/* tslint:disable */
/* eslint-disable */
// @ts-nocheck

import { ConcreteRequest, Query } from 'relay-runtime';
import { FragmentRefs } from "relay-runtime";
export type NewPipelineTemplateVersionQuery$variables = {
  id: string;
};
export type NewPipelineTemplateVersionQuery$data = {
  readonly node: {
    readonly id?: string;
    readonly " $fragmentSpreads": FragmentRefs<"NewPipelineTemplateVersionFragment_pipelineTemplate">;
  } | null | undefined;
};
export type NewPipelineTemplateVersionQuery = {
  response: NewPipelineTemplateVersionQuery$data;
  variables: NewPipelineTemplateVersionQuery$variables;
};

const node: ConcreteRequest = (function(){
var v0 = [
  {
    "defaultValue": null,
    "kind": "LocalArgument",
    "name": "id"
  }
],
v1 = [
  {
    "kind": "Variable",
    "name": "id",
    "variableName": "id"
  }
],
v2 = {
  "alias": null,
  "args": null,
  "kind": "ScalarField",
  "name": "id",
  "storageKey": null
};
return {
  "fragment": {
    "argumentDefinitions": (v0/*: any*/),
    "kind": "Fragment",
    "metadata": null,
    "name": "NewPipelineTemplateVersionQuery",
    "selections": [
      {
        "alias": null,
        "args": (v1/*: any*/),
        "concreteType": null,
        "kind": "LinkedField",
        "name": "node",
        "plural": false,
        "selections": [
          {
            "kind": "InlineFragment",
            "selections": [
              (v2/*: any*/),
              {
                "args": null,
                "kind": "FragmentSpread",
                "name": "NewPipelineTemplateVersionFragment_pipelineTemplate"
              }
            ],
            "type": "PipelineTemplate",
            "abstractKey": null
          }
        ],
        "storageKey": null
      }
    ],
    "type": "Query",
    "abstractKey": null
  },
  "kind": "Request",
  "operation": {
    "argumentDefinitions": (v0/*: any*/),
    "kind": "Operation",
    "name": "NewPipelineTemplateVersionQuery",
    "selections": [
      {
        "alias": null,
        "args": (v1/*: any*/),
        "concreteType": null,
        "kind": "LinkedField",
        "name": "node",
        "plural": false,
        "selections": [
          {
            "alias": null,
            "args": null,
            "kind": "ScalarField",
            "name": "__typename",
            "storageKey": null
          },
          (v2/*: any*/),
          {
            "kind": "InlineFragment",
            "selections": [
              {
                "alias": null,
                "args": null,
                "kind": "ScalarField",
                "name": "name",
                "storageKey": null
              },
              {
                "alias": null,
                "args": null,
                "kind": "ScalarField",
                "name": "hclData",
                "storageKey": null
              },
              {
                "alias": null,
                "args": null,
                "kind": "ScalarField",
                "name": "semanticVersion",
                "storageKey": null
              },
              {
                "alias": null,
                "args": null,
                "kind": "ScalarField",
                "name": "latest",
                "storageKey": null
              },
              {
                "alias": null,
                "args": null,
                "kind": "ScalarField",
                "name": "createdBy",
                "storageKey": null
              },
              {
                "alias": null,
                "args": null,
                "concreteType": "ResourceMetadata",
                "kind": "LinkedField",
                "name": "metadata",
                "plural": false,
                "selections": [
                  {
                    "alias": null,
                    "args": null,
                    "kind": "ScalarField",
                    "name": "prn",
                    "storageKey": null
                  },
                  {
                    "alias": null,
                    "args": null,
                    "kind": "ScalarField",
                    "name": "createdAt",
                    "storageKey": null
                  }
                ],
                "storageKey": null
              }
            ],
            "type": "PipelineTemplate",
            "abstractKey": null
          }
        ],
        "storageKey": null
      }
    ]
  },
  "params": {
    "cacheID": "8d0030ec534b4fba66c1668c5675d523",
    "id": null,
    "metadata": {},
    "name": "NewPipelineTemplateVersionQuery",
    "operationKind": "query",
    "text": "query NewPipelineTemplateVersionQuery(\n  $id: String!\n) {\n  node(id: $id) {\n    __typename\n    ... on PipelineTemplate {\n      id\n      ...NewPipelineTemplateVersionFragment_pipelineTemplate\n    }\n    id\n  }\n}\n\nfragment NewPipelineTemplateVersionFragment_pipelineTemplate on PipelineTemplate {\n  name\n  hclData\n  semanticVersion\n  ...PipelineTemplateDetailsSidebarFragment_details\n}\n\nfragment PipelineTemplateDetailsSidebarFragment_details on PipelineTemplate {\n  name\n  latest\n  semanticVersion\n  createdBy\n  metadata {\n    prn\n    createdAt\n  }\n}\n"
  }
};
})();

(node as any).hash = "5b0c14f23bd64163ca2e76dad478075b";

export default node;
