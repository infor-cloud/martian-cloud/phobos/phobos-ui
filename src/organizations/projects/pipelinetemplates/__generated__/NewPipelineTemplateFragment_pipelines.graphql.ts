/**
 * @generated SignedSource<<f307c7c731f38665650210c8b9a0761e>>
 * @lightSyntaxTransform
 * @nogrep
 */

/* tslint:disable */
/* eslint-disable */
// @ts-nocheck

import { Fragment, ReaderFragment } from 'relay-runtime';
import { FragmentRefs } from "relay-runtime";
export type NewPipelineTemplateFragment_pipelines$data = {
  readonly id: string;
  readonly metadata: {
    readonly prn: string;
  };
  readonly " $fragmentType": "NewPipelineTemplateFragment_pipelines";
};
export type NewPipelineTemplateFragment_pipelines$key = {
  readonly " $data"?: NewPipelineTemplateFragment_pipelines$data;
  readonly " $fragmentSpreads": FragmentRefs<"NewPipelineTemplateFragment_pipelines">;
};

const node: ReaderFragment = {
  "argumentDefinitions": [],
  "kind": "Fragment",
  "metadata": null,
  "name": "NewPipelineTemplateFragment_pipelines",
  "selections": [
    {
      "alias": null,
      "args": null,
      "kind": "ScalarField",
      "name": "id",
      "storageKey": null
    },
    {
      "alias": null,
      "args": null,
      "concreteType": "ResourceMetadata",
      "kind": "LinkedField",
      "name": "metadata",
      "plural": false,
      "selections": [
        {
          "alias": null,
          "args": null,
          "kind": "ScalarField",
          "name": "prn",
          "storageKey": null
        }
      ],
      "storageKey": null
    }
  ],
  "type": "Project",
  "abstractKey": null
};

(node as any).hash = "807bfbeb938373886a71ae9b5fc75149";

export default node;
