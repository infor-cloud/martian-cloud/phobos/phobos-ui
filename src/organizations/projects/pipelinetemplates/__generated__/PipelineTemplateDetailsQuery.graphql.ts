/**
 * @generated SignedSource<<206c366f94aec9994231b23ee9b68716>>
 * @lightSyntaxTransform
 * @nogrep
 */

/* tslint:disable */
/* eslint-disable */
// @ts-nocheck

import { ConcreteRequest, Query } from 'relay-runtime';
import { FragmentRefs } from "relay-runtime";
export type PipelineTemplateDetailsQuery$variables = {
  id: string;
};
export type PipelineTemplateDetailsQuery$data = {
  readonly node: {
    readonly id?: string;
    readonly " $fragmentSpreads": FragmentRefs<"PipelineTemplateDetailsFragment_details">;
  } | null | undefined;
};
export type PipelineTemplateDetailsQuery = {
  response: PipelineTemplateDetailsQuery$data;
  variables: PipelineTemplateDetailsQuery$variables;
};

const node: ConcreteRequest = (function(){
var v0 = [
  {
    "defaultValue": null,
    "kind": "LocalArgument",
    "name": "id"
  }
],
v1 = [
  {
    "kind": "Variable",
    "name": "id",
    "variableName": "id"
  }
],
v2 = {
  "alias": null,
  "args": null,
  "kind": "ScalarField",
  "name": "id",
  "storageKey": null
};
return {
  "fragment": {
    "argumentDefinitions": (v0/*: any*/),
    "kind": "Fragment",
    "metadata": null,
    "name": "PipelineTemplateDetailsQuery",
    "selections": [
      {
        "alias": null,
        "args": (v1/*: any*/),
        "concreteType": null,
        "kind": "LinkedField",
        "name": "node",
        "plural": false,
        "selections": [
          {
            "kind": "InlineFragment",
            "selections": [
              (v2/*: any*/),
              {
                "args": null,
                "kind": "FragmentSpread",
                "name": "PipelineTemplateDetailsFragment_details"
              }
            ],
            "type": "PipelineTemplate",
            "abstractKey": null
          }
        ],
        "storageKey": null
      }
    ],
    "type": "Query",
    "abstractKey": null
  },
  "kind": "Request",
  "operation": {
    "argumentDefinitions": (v0/*: any*/),
    "kind": "Operation",
    "name": "PipelineTemplateDetailsQuery",
    "selections": [
      {
        "alias": null,
        "args": (v1/*: any*/),
        "concreteType": null,
        "kind": "LinkedField",
        "name": "node",
        "plural": false,
        "selections": [
          {
            "alias": null,
            "args": null,
            "kind": "ScalarField",
            "name": "__typename",
            "storageKey": null
          },
          (v2/*: any*/),
          {
            "kind": "InlineFragment",
            "selections": [
              {
                "alias": null,
                "args": null,
                "kind": "ScalarField",
                "name": "name",
                "storageKey": null
              },
              {
                "alias": null,
                "args": null,
                "kind": "ScalarField",
                "name": "status",
                "storageKey": null
              },
              {
                "alias": null,
                "args": null,
                "kind": "ScalarField",
                "name": "hclData",
                "storageKey": null
              },
              {
                "alias": null,
                "args": null,
                "kind": "ScalarField",
                "name": "semanticVersion",
                "storageKey": null
              },
              {
                "alias": null,
                "args": null,
                "concreteType": "ResourceMetadata",
                "kind": "LinkedField",
                "name": "metadata",
                "plural": false,
                "selections": [
                  {
                    "alias": null,
                    "args": null,
                    "kind": "ScalarField",
                    "name": "prn",
                    "storageKey": null
                  },
                  {
                    "alias": null,
                    "args": null,
                    "kind": "ScalarField",
                    "name": "createdAt",
                    "storageKey": null
                  }
                ],
                "storageKey": null
              },
              {
                "alias": null,
                "args": null,
                "kind": "ScalarField",
                "name": "latest",
                "storageKey": null
              },
              {
                "alias": null,
                "args": null,
                "kind": "ScalarField",
                "name": "createdBy",
                "storageKey": null
              }
            ],
            "type": "PipelineTemplate",
            "abstractKey": null
          }
        ],
        "storageKey": null
      }
    ]
  },
  "params": {
    "cacheID": "d0384bf714f47c5a8963ca5ceee7bd7c",
    "id": null,
    "metadata": {},
    "name": "PipelineTemplateDetailsQuery",
    "operationKind": "query",
    "text": "query PipelineTemplateDetailsQuery(\n  $id: String!\n) {\n  node(id: $id) {\n    __typename\n    ... on PipelineTemplate {\n      id\n      ...PipelineTemplateDetailsFragment_details\n    }\n    id\n  }\n}\n\nfragment PipelineTemplateDetailsFragment_details on PipelineTemplate {\n  id\n  name\n  status\n  hclData\n  semanticVersion\n  metadata {\n    prn\n  }\n  ...PipelineTemplateDetailsSidebarFragment_details\n}\n\nfragment PipelineTemplateDetailsSidebarFragment_details on PipelineTemplate {\n  name\n  latest\n  semanticVersion\n  createdBy\n  metadata {\n    prn\n    createdAt\n  }\n}\n"
  }
};
})();

(node as any).hash = "f1804284653de24d2eab9d55e149445f";

export default node;
