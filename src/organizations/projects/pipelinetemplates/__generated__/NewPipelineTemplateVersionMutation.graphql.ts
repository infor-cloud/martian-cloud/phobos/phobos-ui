/**
 * @generated SignedSource<<f03a1c64196cb90eddd921bb60b506c9>>
 * @lightSyntaxTransform
 * @nogrep
 */

/* tslint:disable */
/* eslint-disable */
// @ts-nocheck

import { ConcreteRequest, Mutation } from 'relay-runtime';
export type ProblemType = "BAD_REQUEST" | "CONFLICT" | "FORBIDDEN" | "NOT_FOUND" | "%future added value";
export type CreatePipelineTemplateInput = {
  clientMutationId?: string | null | undefined;
  hclData: string;
  name?: string | null | undefined;
  projectId: string;
  semanticVersion?: string | null | undefined;
  versioned: boolean;
};
export type NewPipelineTemplateVersionMutation$variables = {
  input: CreatePipelineTemplateInput;
};
export type NewPipelineTemplateVersionMutation$data = {
  readonly createPipelineTemplate: {
    readonly pipelineTemplate: {
      readonly hclData: string;
      readonly id: string;
      readonly name: string | null | undefined;
    } | null | undefined;
    readonly problems: ReadonlyArray<{
      readonly field: ReadonlyArray<string> | null | undefined;
      readonly message: string;
      readonly type: ProblemType;
    }>;
  };
};
export type NewPipelineTemplateVersionMutation = {
  response: NewPipelineTemplateVersionMutation$data;
  variables: NewPipelineTemplateVersionMutation$variables;
};

const node: ConcreteRequest = (function(){
var v0 = [
  {
    "defaultValue": null,
    "kind": "LocalArgument",
    "name": "input"
  }
],
v1 = [
  {
    "alias": null,
    "args": [
      {
        "kind": "Variable",
        "name": "input",
        "variableName": "input"
      }
    ],
    "concreteType": "CreatePipelineTemplatePayload",
    "kind": "LinkedField",
    "name": "createPipelineTemplate",
    "plural": false,
    "selections": [
      {
        "alias": null,
        "args": null,
        "concreteType": "PipelineTemplate",
        "kind": "LinkedField",
        "name": "pipelineTemplate",
        "plural": false,
        "selections": [
          {
            "alias": null,
            "args": null,
            "kind": "ScalarField",
            "name": "id",
            "storageKey": null
          },
          {
            "alias": null,
            "args": null,
            "kind": "ScalarField",
            "name": "name",
            "storageKey": null
          },
          {
            "alias": null,
            "args": null,
            "kind": "ScalarField",
            "name": "hclData",
            "storageKey": null
          }
        ],
        "storageKey": null
      },
      {
        "alias": null,
        "args": null,
        "concreteType": "Problem",
        "kind": "LinkedField",
        "name": "problems",
        "plural": true,
        "selections": [
          {
            "alias": null,
            "args": null,
            "kind": "ScalarField",
            "name": "message",
            "storageKey": null
          },
          {
            "alias": null,
            "args": null,
            "kind": "ScalarField",
            "name": "field",
            "storageKey": null
          },
          {
            "alias": null,
            "args": null,
            "kind": "ScalarField",
            "name": "type",
            "storageKey": null
          }
        ],
        "storageKey": null
      }
    ],
    "storageKey": null
  }
];
return {
  "fragment": {
    "argumentDefinitions": (v0/*: any*/),
    "kind": "Fragment",
    "metadata": null,
    "name": "NewPipelineTemplateVersionMutation",
    "selections": (v1/*: any*/),
    "type": "Mutation",
    "abstractKey": null
  },
  "kind": "Request",
  "operation": {
    "argumentDefinitions": (v0/*: any*/),
    "kind": "Operation",
    "name": "NewPipelineTemplateVersionMutation",
    "selections": (v1/*: any*/)
  },
  "params": {
    "cacheID": "fb2a0a9cfd27cbd6fd4f55e3af8d4a9c",
    "id": null,
    "metadata": {},
    "name": "NewPipelineTemplateVersionMutation",
    "operationKind": "mutation",
    "text": "mutation NewPipelineTemplateVersionMutation(\n  $input: CreatePipelineTemplateInput!\n) {\n  createPipelineTemplate(input: $input) {\n    pipelineTemplate {\n      id\n      name\n      hclData\n    }\n    problems {\n      message\n      field\n      type\n    }\n  }\n}\n"
  }
};
})();

(node as any).hash = "525cb89511750c454813d0900f4a02e0";

export default node;
