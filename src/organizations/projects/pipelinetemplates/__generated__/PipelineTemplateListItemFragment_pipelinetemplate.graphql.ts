/**
 * @generated SignedSource<<f2e5af175611e7cac8ddc8be24b2a144>>
 * @lightSyntaxTransform
 * @nogrep
 */

/* tslint:disable */
/* eslint-disable */
// @ts-nocheck

import { Fragment, ReaderFragment } from 'relay-runtime';
import { FragmentRefs } from "relay-runtime";
export type PipelineTemplateListItemFragment_pipelinetemplate$data = {
  readonly createdBy: string;
  readonly id: string;
  readonly metadata: {
    readonly createdAt: any;
  };
  readonly name: string | null | undefined;
  readonly semanticVersion: string | null | undefined;
  readonly " $fragmentType": "PipelineTemplateListItemFragment_pipelinetemplate";
};
export type PipelineTemplateListItemFragment_pipelinetemplate$key = {
  readonly " $data"?: PipelineTemplateListItemFragment_pipelinetemplate$data;
  readonly " $fragmentSpreads": FragmentRefs<"PipelineTemplateListItemFragment_pipelinetemplate">;
};

const node: ReaderFragment = {
  "argumentDefinitions": [],
  "kind": "Fragment",
  "metadata": null,
  "name": "PipelineTemplateListItemFragment_pipelinetemplate",
  "selections": [
    {
      "alias": null,
      "args": null,
      "kind": "ScalarField",
      "name": "id",
      "storageKey": null
    },
    {
      "alias": null,
      "args": null,
      "kind": "ScalarField",
      "name": "name",
      "storageKey": null
    },
    {
      "alias": null,
      "args": null,
      "kind": "ScalarField",
      "name": "semanticVersion",
      "storageKey": null
    },
    {
      "alias": null,
      "args": null,
      "kind": "ScalarField",
      "name": "createdBy",
      "storageKey": null
    },
    {
      "alias": null,
      "args": null,
      "concreteType": "ResourceMetadata",
      "kind": "LinkedField",
      "name": "metadata",
      "plural": false,
      "selections": [
        {
          "alias": null,
          "args": null,
          "kind": "ScalarField",
          "name": "createdAt",
          "storageKey": null
        }
      ],
      "storageKey": null
    }
  ],
  "type": "PipelineTemplate",
  "abstractKey": null
};

(node as any).hash = "c6ad268d040896a999f5b016ab2d4184";

export default node;
