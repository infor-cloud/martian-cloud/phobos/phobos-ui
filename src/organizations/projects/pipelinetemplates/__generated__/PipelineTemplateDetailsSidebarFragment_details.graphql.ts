/**
 * @generated SignedSource<<3e6986b38dd4dfe1682413837033f563>>
 * @lightSyntaxTransform
 * @nogrep
 */

/* tslint:disable */
/* eslint-disable */
// @ts-nocheck

import { Fragment, ReaderFragment } from 'relay-runtime';
import { FragmentRefs } from "relay-runtime";
export type PipelineTemplateDetailsSidebarFragment_details$data = {
  readonly createdBy: string;
  readonly latest: boolean;
  readonly metadata: {
    readonly createdAt: any;
    readonly prn: string;
  };
  readonly name: string | null | undefined;
  readonly semanticVersion: string | null | undefined;
  readonly " $fragmentType": "PipelineTemplateDetailsSidebarFragment_details";
};
export type PipelineTemplateDetailsSidebarFragment_details$key = {
  readonly " $data"?: PipelineTemplateDetailsSidebarFragment_details$data;
  readonly " $fragmentSpreads": FragmentRefs<"PipelineTemplateDetailsSidebarFragment_details">;
};

const node: ReaderFragment = {
  "argumentDefinitions": [],
  "kind": "Fragment",
  "metadata": null,
  "name": "PipelineTemplateDetailsSidebarFragment_details",
  "selections": [
    {
      "alias": null,
      "args": null,
      "kind": "ScalarField",
      "name": "name",
      "storageKey": null
    },
    {
      "alias": null,
      "args": null,
      "kind": "ScalarField",
      "name": "latest",
      "storageKey": null
    },
    {
      "alias": null,
      "args": null,
      "kind": "ScalarField",
      "name": "semanticVersion",
      "storageKey": null
    },
    {
      "alias": null,
      "args": null,
      "kind": "ScalarField",
      "name": "createdBy",
      "storageKey": null
    },
    {
      "alias": null,
      "args": null,
      "concreteType": "ResourceMetadata",
      "kind": "LinkedField",
      "name": "metadata",
      "plural": false,
      "selections": [
        {
          "alias": null,
          "args": null,
          "kind": "ScalarField",
          "name": "prn",
          "storageKey": null
        },
        {
          "alias": null,
          "args": null,
          "kind": "ScalarField",
          "name": "createdAt",
          "storageKey": null
        }
      ],
      "storageKey": null
    }
  ],
  "type": "PipelineTemplate",
  "abstractKey": null
};

(node as any).hash = "505ddaf475ff1fac7fd3d6b65247e075";

export default node;
