/**
 * @generated SignedSource<<84015b120859af685359cb9d3d02e4a6>>
 * @lightSyntaxTransform
 * @nogrep
 */

/* tslint:disable */
/* eslint-disable */
// @ts-nocheck

import { Fragment, ReaderFragment } from 'relay-runtime';
import { FragmentRefs } from "relay-runtime";
export type PipelineTemplateListFragment_project$data = {
  readonly id: string;
  readonly " $fragmentType": "PipelineTemplateListFragment_project";
};
export type PipelineTemplateListFragment_project$key = {
  readonly " $data"?: PipelineTemplateListFragment_project$data;
  readonly " $fragmentSpreads": FragmentRefs<"PipelineTemplateListFragment_project">;
};

const node: ReaderFragment = {
  "argumentDefinitions": [],
  "kind": "Fragment",
  "metadata": null,
  "name": "PipelineTemplateListFragment_project",
  "selections": [
    {
      "alias": null,
      "args": null,
      "kind": "ScalarField",
      "name": "id",
      "storageKey": null
    }
  ],
  "type": "Project",
  "abstractKey": null
};

(node as any).hash = "d5fb822151e9ec60baa3cb2760ca3f4a";

export default node;
