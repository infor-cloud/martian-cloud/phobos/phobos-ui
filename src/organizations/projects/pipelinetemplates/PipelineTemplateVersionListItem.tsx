import { Box, ListItemButton, ListItemText, Typography, useTheme, Tooltip, Chip } from '@mui/material';
import graphql from 'babel-plugin-relay/macro';
import moment from 'moment';
import React from 'react';
import { useFragment } from "react-relay/hooks";
import { Link as RouterLink } from 'react-router-dom';
import Gravatar from '../../../common/Gravatar';
import { PipelineTemplateVersionListItemFragment_version$key } from './__generated__/PipelineTemplateVersionListItemFragment_version.graphql';

interface Props {
    fragmentRef: PipelineTemplateVersionListItemFragment_version$key
}

function PipelineTemplateVersionListItem({ fragmentRef }: Props) {
    const theme = useTheme();

    const data = useFragment<PipelineTemplateVersionListItemFragment_version$key>(graphql`
        fragment PipelineTemplateVersionListItemFragment_version on PipelineTemplate {
            id
            semanticVersion
            createdBy
            latest
            metadata {
                createdAt
            }
        }
    `, fragmentRef);

    return (
        <ListItemButton
            component={RouterLink}
            to={`../${data.id}`}
            sx={{
                borderBottom: `1px solid ${theme.palette.divider}`,
                borderLeft: `1px solid ${theme.palette.divider}`,
                borderRight: `1px solid ${theme.palette.divider}`,
                '&:last-child': {
                    borderBottomLeftRadius: 4,
                    borderBottomRightRadius: 4
                }
            }}>
            <ListItemText
                primary={<Box display="flex" alignItems="center">
                    <Typography>{data.semanticVersion}</Typography>
                    {data.latest && <Chip size="small" color="secondary" sx={{ ml: 1 }} label="latest" />}
                </Box>} />
            <Box display="flex" alignItems="center">
                <Typography variant="body2" color="textSecondary" sx={{ marginRight: 1 }}>
                    {moment(data.metadata.createdAt as moment.MomentInput).fromNow()} by
                </Typography>
                <Tooltip title={data.createdBy}>
                    <Box>
                        <Gravatar width={20} height={20} email={data.createdBy} />
                    </Box>
                </Tooltip>
            </Box>
        </ListItemButton>
    );
}

export default PipelineTemplateVersionListItem
