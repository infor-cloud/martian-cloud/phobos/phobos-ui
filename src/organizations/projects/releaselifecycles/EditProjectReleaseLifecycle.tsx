import { Box, Typography } from '@mui/material';
import { useOutletContext, useParams } from 'react-router-dom';
import ProjectBreadcrumbs from '../ProjectBreadcrumbs';
import graphql from 'babel-plugin-relay/macro';
import { useFragment, useLazyLoadQuery } from "react-relay/hooks";
import EditReleaseLifecycle from '../../../releaselifecycles/EditReleaseLifecycle';
import { EditProjectReleaseLifecycleFragment_project$key } from './__generated__/EditProjectReleaseLifecycleFragment_project.graphql';
import { EditProjectReleaseLifecycleQuery } from './__generated__/EditProjectReleaseLifecycleQuery.graphql';

function EditProjectReleaseLifecycle() {
    const releaseLifecycleId = useParams().releaseLifecycleId as string;
    const context = useOutletContext<EditProjectReleaseLifecycleFragment_project$key>();

    const project = useFragment<EditProjectReleaseLifecycleFragment_project$key>(
        graphql`
            fragment EditProjectReleaseLifecycleFragment_project on Project
            {
                id
                metadata {
                    prn
                }
            }
        `, context
    );

    const queryData = useLazyLoadQuery<EditProjectReleaseLifecycleQuery>(graphql`
        query EditProjectReleaseLifecycleQuery($id: String!) {
            node(id: $id) {
                ... on ReleaseLifecycle {
                    name
                    scope
                }
            }
        }
    `, { id: releaseLifecycleId });

    const releaseLifecycle = queryData.node as any;

    return queryData.node ? (
        <Box>
            <ProjectBreadcrumbs
                prn={project.metadata.prn}
                childRoutes={[
                    { title: "release lifecycles", path: 'release_lifecycles' },
                    { title: releaseLifecycle.name, path: releaseLifecycleId },
                    { title: "edit", path: 'edit' },
                ]}
            />
            <EditReleaseLifecycle projectId={project.id} />
        </Box>
    ) : <Box display="flex" justifyContent="center" mt={4}>
        <Typography color="textSecondary">Release lifecycle not found</Typography>
    </Box>;
}

export default EditProjectReleaseLifecycle;
