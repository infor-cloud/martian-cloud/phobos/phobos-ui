import { Box, Typography } from '@mui/material';
import ProjectBreadcrumbs from '../ProjectBreadcrumbs';
import { useFragment, useLazyLoadQuery } from 'react-relay/hooks';
import { useOutletContext, useParams } from 'react-router-dom';
import graphql from 'babel-plugin-relay/macro';
import ReleaseLifecycleDetails from '../../../releaselifecycles/ReleaseLifecycleDetails';
import { GetConnections } from './ProjectReleaseLifecycles';
import { ProjectReleaseLifecycleDetailsQuery } from './__generated__/ProjectReleaseLifecycleDetailsQuery.graphql';
import { ProjectReleaseLifecycleDetailsFragment_project$key } from './__generated__/ProjectReleaseLifecycleDetailsFragment_project.graphql';

function ProjectReleaseLifecycleDetails() {
    const context = useOutletContext<ProjectReleaseLifecycleDetailsFragment_project$key>();
    const releaseLifecycleId = useParams().releaseLifecycleId as string;

    const project = useFragment<ProjectReleaseLifecycleDetailsFragment_project$key>(
        graphql`
            fragment ProjectReleaseLifecycleDetailsFragment_project on Project
            {
                id
                name
                metadata {
                    prn
                }
            }
        `, context
    );

    const data = useLazyLoadQuery<ProjectReleaseLifecycleDetailsQuery>(graphql`
        query ProjectReleaseLifecycleDetailsQuery($id: String!) {
            node(id: $id) {
                ... on ReleaseLifecycle {
                    id
                    name
                }
            }
        }
    `, { id: releaseLifecycleId }, { fetchPolicy: 'store-and-network' });

    if (data.node) {

        const releaseLifecycle = data.node as any;

        return (
            <Box>
                <ProjectBreadcrumbs
                    prn={project.metadata.prn}
                    childRoutes={[
                        { title: "release lifecycles", path: 'release_lifecycles' },
                        { title: releaseLifecycle.name, path: releaseLifecycle.id }
                    ]}
                />
                <ReleaseLifecycleDetails getConnections={() => GetConnections(project.id)} />
            </Box>
        );
    }
    else {
        return <Box display="flex" justifyContent="center" marginTop={4}>
            <Typography color="textSecondary">Release lifecycle with ID {releaseLifecycleId} not found</Typography>
        </Box>;
    }
}

export default ProjectReleaseLifecycleDetails;
