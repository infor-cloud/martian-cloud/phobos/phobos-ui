import Box from '@mui/material/Box';
import { useOutletContext } from 'react-router-dom';
import { useFragment } from 'react-relay/hooks';
import graphql from 'babel-plugin-relay/macro';
import ProjectBreadcrumbs from '../ProjectBreadcrumbs';
import NewReleaseLifecycle from '../../../releaselifecycles/NewReleaseLifecycle';
import { GetConnections } from './ProjectReleaseLifecycles';
import { NewProjectReleaseLifecycleFragment_project$key } from './__generated__/NewProjectReleaseLifecycleFragment_project.graphql';

function NewProjectReleaseLifecycle() {
    const context = useOutletContext<NewProjectReleaseLifecycleFragment_project$key>();

    const project = useFragment<NewProjectReleaseLifecycleFragment_project$key>(
        graphql`
            fragment NewProjectReleaseLifecycleFragment_project on Project
            {
                id
                metadata {
                    prn
                }
            }
        `, context
    );

    return (
        <Box>
            <ProjectBreadcrumbs
                prn={project.metadata.prn}
                childRoutes={[
                    { title: "release lifecycles", path: 'release_lifecycles' },
                    { title: "new", path: 'new' },
                ]}
            />
            <NewReleaseLifecycle
                projectId={project.id}
                getConnections={GetConnections}
            />
        </Box>
    );
}

export default NewProjectReleaseLifecycle;
