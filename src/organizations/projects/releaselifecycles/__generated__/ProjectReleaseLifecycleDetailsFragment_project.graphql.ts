/**
 * @generated SignedSource<<f76b66849f183fc9976a87a1114c5c8d>>
 * @lightSyntaxTransform
 * @nogrep
 */

/* tslint:disable */
/* eslint-disable */
// @ts-nocheck

import { Fragment, ReaderFragment } from 'relay-runtime';
import { FragmentRefs } from "relay-runtime";
export type ProjectReleaseLifecycleDetailsFragment_project$data = {
  readonly id: string;
  readonly metadata: {
    readonly prn: string;
  };
  readonly name: string;
  readonly " $fragmentType": "ProjectReleaseLifecycleDetailsFragment_project";
};
export type ProjectReleaseLifecycleDetailsFragment_project$key = {
  readonly " $data"?: ProjectReleaseLifecycleDetailsFragment_project$data;
  readonly " $fragmentSpreads": FragmentRefs<"ProjectReleaseLifecycleDetailsFragment_project">;
};

const node: ReaderFragment = {
  "argumentDefinitions": [],
  "kind": "Fragment",
  "metadata": null,
  "name": "ProjectReleaseLifecycleDetailsFragment_project",
  "selections": [
    {
      "alias": null,
      "args": null,
      "kind": "ScalarField",
      "name": "id",
      "storageKey": null
    },
    {
      "alias": null,
      "args": null,
      "kind": "ScalarField",
      "name": "name",
      "storageKey": null
    },
    {
      "alias": null,
      "args": null,
      "concreteType": "ResourceMetadata",
      "kind": "LinkedField",
      "name": "metadata",
      "plural": false,
      "selections": [
        {
          "alias": null,
          "args": null,
          "kind": "ScalarField",
          "name": "prn",
          "storageKey": null
        }
      ],
      "storageKey": null
    }
  ],
  "type": "Project",
  "abstractKey": null
};

(node as any).hash = "c2c703f2534cac9624f49fc598d199af";

export default node;
