/**
 * @generated SignedSource<<42dd25ec5419ed23ff03eeefaa4bfc0e>>
 * @lightSyntaxTransform
 * @nogrep
 */

/* tslint:disable */
/* eslint-disable */
// @ts-nocheck

import { Fragment, ReaderFragment } from 'relay-runtime';
import { FragmentRefs } from "relay-runtime";
export type NewProjectReleaseLifecycleFragment_project$data = {
  readonly id: string;
  readonly metadata: {
    readonly prn: string;
  };
  readonly " $fragmentType": "NewProjectReleaseLifecycleFragment_project";
};
export type NewProjectReleaseLifecycleFragment_project$key = {
  readonly " $data"?: NewProjectReleaseLifecycleFragment_project$data;
  readonly " $fragmentSpreads": FragmentRefs<"NewProjectReleaseLifecycleFragment_project">;
};

const node: ReaderFragment = {
  "argumentDefinitions": [],
  "kind": "Fragment",
  "metadata": null,
  "name": "NewProjectReleaseLifecycleFragment_project",
  "selections": [
    {
      "alias": null,
      "args": null,
      "kind": "ScalarField",
      "name": "id",
      "storageKey": null
    },
    {
      "alias": null,
      "args": null,
      "concreteType": "ResourceMetadata",
      "kind": "LinkedField",
      "name": "metadata",
      "plural": false,
      "selections": [
        {
          "alias": null,
          "args": null,
          "kind": "ScalarField",
          "name": "prn",
          "storageKey": null
        }
      ],
      "storageKey": null
    }
  ],
  "type": "Project",
  "abstractKey": null
};

(node as any).hash = "094b91c6e94f99be9c18e2c64e2cce5e";

export default node;
