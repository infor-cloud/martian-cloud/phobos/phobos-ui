import { Box, Typography } from '@mui/material';
import graphql from 'babel-plugin-relay/macro';
import InfiniteScroll from 'react-infinite-scroll-component';
import { useFragment, useLazyLoadQuery, usePaginationFragment } from "react-relay/hooks";
import ActivityEventList from '../../activity/ActivityEventList';
import ListSkeleton from '../../skeletons/ListSkeleton';
import { ProjectActivityFragment_activity$key } from './__generated__/ProjectActivityFragment_activity.graphql';
import { ProjectActivityFragment_project$key } from './__generated__/ProjectActivityFragment_project.graphql';
import { ProjectActivityPaginationQuery } from './__generated__/ProjectActivityPaginationQuery.graphql';
import { ProjectActivityQuery } from './__generated__/ProjectActivityQuery.graphql';

interface Props {
    userId?: string;
    fragmentRef: ProjectActivityFragment_project$key;
}

function ProjectActivity({ userId, fragmentRef }: Props) {
    const project = useFragment<ProjectActivityFragment_project$key>(graphql`
        fragment ProjectActivityFragment_project on Project
        {
            id
            metadata {
                prn
            }
        }
    `, fragmentRef);

    const queryData = useLazyLoadQuery<ProjectActivityQuery>(graphql`
        query ProjectActivityQuery($projectId: String, $first: Int, $last: Int, $after: String, $before: String, $userId: String) {
            ...ProjectActivityFragment_activity
        }
    `, { first: 20, projectId: project.id, userId }, { fetchPolicy: 'store-and-network' });

    const { data, loadNext, hasNext } = usePaginationFragment<ProjectActivityPaginationQuery, ProjectActivityFragment_activity$key>(
        graphql`
        fragment ProjectActivityFragment_activity on Query
            @refetchable(queryName: "ProjectActivityPaginationQuery") {
                activityEvents(
                    projectId: $projectId,
                    first: $first,
                    last: $last,
                    after: $after,
                    before: $before,
                    sort: CREATED_AT_DESC,
                    userId: $userId
                    ) @connection(key: "ProjectActivityFragment_activityEvents") {
                    edges {
                        node {
                            id
                        }
                    }
                    ...ActivityEventListFragment_connection
                }
            }
        `,
        queryData);

    const eventCount = data.activityEvents?.edges?.length || 0;

    return (
        <Box>
            {eventCount > 0 &&
                <InfiniteScroll
                    dataLength={data.activityEvents?.edges?.length ?? 0}
                    next={() => loadNext(20)}
                    hasMore={hasNext}
                    loader={<ListSkeleton rowCount={3} />}
                >
                    <ActivityEventList fragmentRef={data.activityEvents} />
                </InfiniteScroll>
            }
            {eventCount === 0 && <Box sx={{ mt: 8 }} display="flex" justifyContent="center">
                <Typography variant="h6" color="textSecondary">
                    This project does not have any activity events
                </Typography>
            </Box>}
        </Box>
    );
}

export default ProjectActivity;
