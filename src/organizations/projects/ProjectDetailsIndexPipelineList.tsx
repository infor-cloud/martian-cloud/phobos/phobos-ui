import { Box, Link, List, ListItem, Typography, useTheme } from '@mui/material';
import graphql from 'babel-plugin-relay/macro';
import { useMemo } from 'react';
import { useLazyLoadQuery, usePaginationFragment, useSubscription } from 'react-relay/hooks';
import { ConnectionHandler, ConnectionInterface, GraphQLSubscriptionConfig, RecordSourceProxy } from 'relay-runtime';
import ProjectDetailsIndexPipelineListItem from './ProjectDetailsIndexPipelineListItem';
import { ProjectDetailsIndexPipelineListFragment_pipelines$key } from './__generated__/ProjectDetailsIndexPipelineListFragment_pipelines.graphql';
import { ProjectDetailsIndexPipelineListPaginationQuery } from './__generated__/ProjectDetailsIndexPipelineListPaginationQuery.graphql';
import { ProjectDetailsIndexPipelineListQuery } from './__generated__/ProjectDetailsIndexPipelineListQuery.graphql';
import { ProjectDetailsIndexPipelineListSubscription, ProjectDetailsIndexPipelineListSubscription$data } from './__generated__/ProjectDetailsIndexPipelineListSubscription.graphql';

const INITIAL_ITEM_COUNT = 5;

const query = graphql`
    query ProjectDetailsIndexPipelineListQuery($first: Int!, $after: String, $projectId: String!) {
        node(id: $projectId) {
            ...on Project {
                ...ProjectDetailsIndexPipelineListFragment_pipelines
            }
        }
    }
`;

const pipelineSubscription = graphql`subscription ProjectDetailsIndexPipelineListSubscription($input: ProjectPipelineEventsSubscriptionInput!) {
    projectPipelineEvents(input: $input) {
      action
      pipeline {
        id
        ...ProjectDetailsIndexPipelineListItemFragment_pipeline
      }
    }
  }`;

function GetConnections(projectId: string): [string] {
    const connectionId = ConnectionHandler.getConnectionID(
        projectId,
        'ProjectDetailsIndexPipelineList_pipelines',
        { sort: 'CREATED_AT_DESC', started: true }
    );
    return [connectionId];
}

interface Props {
    projectId: string;
}

function ProjectDetailsIndexPipelineList({ projectId }: Props) {
    const theme = useTheme();
    const queryData = useLazyLoadQuery<ProjectDetailsIndexPipelineListQuery>(query, { first: INITIAL_ITEM_COUNT, projectId }, { fetchPolicy: 'store-and-network' });

    const { data, loadNext, hasNext } = usePaginationFragment<ProjectDetailsIndexPipelineListPaginationQuery, ProjectDetailsIndexPipelineListFragment_pipelines$key>(
        graphql`
        fragment ProjectDetailsIndexPipelineListFragment_pipelines on Project
        @refetchable(queryName: "ProjectDetailsIndexPipelineListPaginationQuery") {
            name
            organizationName
            pipelines(
                first: $first
                after: $after
                sort: CREATED_AT_DESC
                started: true
                ) @connection(key: "ProjectDetailsIndexPipelineList_pipelines") {
                    totalCount
                    edges {
                        node {
                            id
                            ...ProjectDetailsIndexPipelineListItemFragment_pipeline
                        }
                    }
                }
            }
        `, queryData.node);

    const pipelineSubscriptionConfig = useMemo<GraphQLSubscriptionConfig<ProjectDetailsIndexPipelineListSubscription>>(() => ({
        variables: { input: { projectId } },
        subscription: pipelineSubscription,
        onCompleted: () => console.log("Subscription completed"),
        onError: () => console.warn("Subscription error"),
        updater: (store: RecordSourceProxy, payload: ProjectDetailsIndexPipelineListSubscription$data) => {
            if (payload.projectPipelineEvents.action !== 'INSERT') {
                return;
            }
            const record = store.get(payload.projectPipelineEvents.pipeline.id);
            if (record == null) {
                return;
            }
            GetConnections(projectId).forEach(id => {
                const connectionRecord = store.get(id);
                if (connectionRecord) {
                    const { NODE, EDGES } = ConnectionInterface.get();

                    const recordId = record.getDataID();
                    // Check if edge already exists in connection
                    const nodeAlreadyExistsInConnection = connectionRecord
                        .getLinkedRecords(EDGES)
                        ?.some(
                            edge => edge?.getLinkedRecord(NODE)?.getDataID() === recordId,
                        );
                    if (!nodeAlreadyExistsInConnection) {
                        // Create Edge
                        const edge = ConnectionHandler.createEdge(
                            store,
                            connectionRecord,
                            record,
                            'PipelineEdge'
                        );
                        if (edge) {
                            // Add edge to the beginning of the connection
                            ConnectionHandler.insertEdgeBefore(
                                connectionRecord,
                                edge,
                            );
                        }
                    }
                }
            });
        }
    }), [projectId]);

    useSubscription<ProjectDetailsIndexPipelineListSubscription>(pipelineSubscriptionConfig);

    const edgeCount = data?.pipelines?.edges?.length ?? 0;

    return (
        <Box>
            <Typography variant="subtitle1" fontWeight={600}>Recent Pipelines</Typography>
            {edgeCount === 0 && <Box sx={{ mt: 2, mb: 2, p: 2, border: `1px dashed ${theme.palette.divider}`, borderRadius: 2 }}>
                <Typography color="textSecondary" variant="body2">
                    This project does not have any pipelines.
                </Typography>
            </Box>}
            {edgeCount > 0 && <List>
                {data?.pipelines?.edges?.map((edge, index: number) => edge?.node ? <ProjectDetailsIndexPipelineListItem
                    key={edge.node.id}
                    fragmentRef={edge.node}
                    last={index === edgeCount - 1} /> : null)}
                {hasNext && <ListItem>
                    <Link
                        variant="body2"
                        color="textSecondary"
                        sx={{ cursor: 'pointer' }}
                        underline="hover"
                        onClick={() => loadNext(INITIAL_ITEM_COUNT)}
                    >
                        Show more
                    </Link>
                </ListItem>}
            </List>}
        </Box>
    );
}

export default ProjectDetailsIndexPipelineList
