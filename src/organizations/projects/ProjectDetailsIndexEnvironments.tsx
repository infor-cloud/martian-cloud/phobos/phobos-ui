import { Box, Link as MuiLink, Table, TableBody, TableContainer, Typography, useTheme } from '@mui/material';
import graphql from 'babel-plugin-relay/macro';
import { useMemo } from 'react';
import { useLazyLoadQuery, usePaginationFragment, useSubscription } from 'react-relay/hooks';
import { GraphQLSubscriptionConfig } from 'relay-runtime';
import ProjectDetailsIndexEnvironmentListItem from './ProjectDetailsIndexEnvironmentListItem';
import { ProjectDetailsIndexEnvironmentsFragment_project$key } from './__generated__/ProjectDetailsIndexEnvironmentsFragment_project.graphql';
import { ProjectDetailsIndexEnvironmentsPaginationQuery } from './__generated__/ProjectDetailsIndexEnvironmentsPaginationQuery.graphql';
import { ProjectDetailsIndexEnvironmentsQuery } from './__generated__/ProjectDetailsIndexEnvironmentsQuery.graphql';
import { ProjectDetailsIndexEnvironmentsSubscription } from './__generated__/ProjectDetailsIndexEnvironmentsSubscription.graphql';

const INITIAL_ITEM_COUNT = 5;

const pipelineSubscription = graphql`subscription ProjectDetailsIndexEnvironmentsSubscription($input: ProjectPipelineEventsSubscriptionInput!) {
    projectPipelineEvents(input: $input) {
      action
      pipeline {
        id
        environment {
            ...ProjectDetailsIndexEnvironmentListItemFragment_environment
        }
      }
    }
  }`;

interface Props {
    projectId: string;
}

function ProjectDetailsIndexEnvironments({ projectId }: Props) {
    const theme = useTheme();

    const query = useLazyLoadQuery<ProjectDetailsIndexEnvironmentsQuery>(graphql`
        query ProjectDetailsIndexEnvironmentsQuery($first: Int, $after: String, $projectId: String!) {
            node(id: $projectId) {
                ... on Project {
                    ...ProjectDetailsIndexEnvironmentsFragment_project
                }
            }
        }`, { first: INITIAL_ITEM_COUNT, projectId }, { fetchPolicy: 'store-and-network' });

    const { data, loadNext, hasNext } = usePaginationFragment<ProjectDetailsIndexEnvironmentsPaginationQuery, ProjectDetailsIndexEnvironmentsFragment_project$key>(
        graphql`
            fragment ProjectDetailsIndexEnvironmentsFragment_project on Project
                @refetchable(queryName: "ProjectDetailsIndexEnvironmentsPaginationQuery") {
                    id
                    name
                    environments(first: $first, after: $after, sort: NAME_ASC) @connection(key: "ProjectDetailsIndexEnvironments_environments") {
                        totalCount
                        edges {
                            node {
                                ...ProjectDetailsIndexEnvironmentListItemFragment_environment
                                id
                            }
                        }
                    }
                }
            `,
        query.node
    );

    const pipelineSubscriptionConfig = useMemo<GraphQLSubscriptionConfig<ProjectDetailsIndexEnvironmentsSubscription>>(() => ({
        variables: { input: { projectId } },
        subscription: pipelineSubscription,
        onCompleted: () => console.log("Subscription completed"),
        onError: () => console.warn("Subscription error"),
    }), [projectId]);

    useSubscription<ProjectDetailsIndexEnvironmentsSubscription>(pipelineSubscriptionConfig);

    const count = data?.environments?.totalCount ?? 0;

    return (
        <Box overflow="hidden">
            <Typography variant="subtitle1" sx={{ fontWeight: 600 }}>{count > 0 ? `${count} ` : ''}Environment{count === 1 ? '' : 's'}</Typography>
            {count === 0 && <Box sx={{ mt: 2, mb: 2, p: 2, border: `1px dashed ${theme.palette.divider}`, borderRadius: 2 }}>
                <Typography color="textSecondary" variant="body2">
                    This project does not have any environments.
                </Typography>
            </Box>}
            {count > 0 && <TableContainer>
                <Table sx={{ tableLayout: 'fixed' }}>
                    <TableBody>
                        {data?.environments?.edges?.map(edge => edge?.node ? <ProjectDetailsIndexEnvironmentListItem key={edge.node.id} fragmentRef={edge.node} /> : null)}
                    </TableBody>
                </Table>
            </TableContainer>}
            {hasNext &&
                <MuiLink
                    pl={2}
                    variant="body2"
                    color="textSecondary"
                    sx={{ cursor: 'pointer' }}
                    underline="hover"
                    onClick={() => loadNext(INITIAL_ITEM_COUNT)}
                >
                    Show more
                </MuiLink>}
        </Box>
    );
}

export default ProjectDetailsIndexEnvironments;
