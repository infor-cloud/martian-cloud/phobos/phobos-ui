import { useState } from 'react';
import { Alert, AlertTitle, Box, Button, Collapse, Dialog, DialogActions, DialogContent, DialogTitle,  TextField, Typography } from '@mui/material';
import { useSnackbar } from 'notistack';
import { useNavigate } from 'react-router-dom';
import { Prism as SyntaxHighlighter } from 'react-syntax-highlighter';
import { atomDark as prismTheme } from 'react-syntax-highlighter/dist/esm/styles/prism';
import { LoadingButton } from '@mui/lab';
import { useFragment, useMutation } from 'react-relay/hooks';
import graphql from 'babel-plugin-relay/macro';
import { GetConnections } from '../ProjectList';
import SettingsToggleButton from '../../../settings/SettingsToggleButton';
import { ProjectAdvancedSettingsFragment_project$key } from './__generated__/ProjectAdvancedSettingsFragment_project.graphql';
import { ProjectAdvancedSettingsDeleteMutation } from './__generated__/ProjectAdvancedSettingsDeleteMutation.graphql';
import { ProjectAdvancedSettingsDeleteDialogFragment_project$key } from './__generated__/ProjectAdvancedSettingsDeleteDialogFragment_project.graphql';

interface ConfirmationDialogProps {
    open: boolean
    keepMounted: boolean
    deleteInProgress: boolean
    onClose: (confirm?: boolean) => void
    fragmentRef: ProjectAdvancedSettingsDeleteDialogFragment_project$key
}

function DeleteConfirmationDialog(props: ConfirmationDialogProps) {
    const { fragmentRef, deleteInProgress, onClose, open, ...other } = props;
    const [deleteInput, setDeleteInput] = useState<string>('');

    const data = useFragment<ProjectAdvancedSettingsDeleteDialogFragment_project$key>(graphql`
    fragment ProjectAdvancedSettingsDeleteDialogFragment_project on Project
    {
        name
        organizationName
    }
`, fragmentRef);

    return (
        <Dialog
            maxWidth="sm"
            open={open}
            {...other}
        >
            <DialogTitle>Delete Project</DialogTitle>
            <DialogContent dividers>
                <Alert sx={{ mb: 2 }} severity="warning">
                    <AlertTitle>Warning</AlertTitle>
                    Deleting a project is an <strong><ins>irreversible</ins></strong> operation. All data and resources associated with this project will be deleted and <strong><ins>cannot be recovered</ins></strong>.
                </Alert>
                <Typography variant="subtitle2">Enter the following to confirm deletion:</Typography>
                <SyntaxHighlighter style={prismTheme} customStyle={{ fontSize: 14, marginBottom: 14 }} children={`${data.organizationName}/${data.name}`} />
                <TextField
                    fullWidth
                    autoComplete="off"
                    size="small"
                    placeholder={`${data.organizationName}/${data.name}`}
                    value={deleteInput}
                    onChange={(event: any) => setDeleteInput(event.target.value)}
                />
            </DialogContent>
            <DialogActions>
                <Button
                    color="inherit"
                    onClick={() => {
                        setDeleteInput('')
                        onClose()
                    }}
                >Cancel</Button>
                <LoadingButton
                    color="error"
                    loading={deleteInProgress}
                    disabled={`${data.organizationName}/${data.name}` !== deleteInput}
                    onClick={() => onClose(true)}
                >Delete</LoadingButton>
            </DialogActions>
        </Dialog>
    );
}

interface Props {
    fragmentRef: ProjectAdvancedSettingsFragment_project$key
}

function ProjectAdvancedSettings({ fragmentRef }: Props) {
    const { enqueueSnackbar } = useSnackbar();
    const navigate = useNavigate();
    const [showDeleteConfirmationDialog, setShowDeleteConfirmationDialog] = useState<boolean>(false);
    const [showSettings, setShowSettings] = useState<boolean>(false);

    const data = useFragment<ProjectAdvancedSettingsFragment_project$key>(graphql`
        fragment ProjectAdvancedSettingsFragment_project on Project
        {
            id
            name
            organizationId
            ...ProjectAdvancedSettingsDeleteDialogFragment_project
        }
    `, fragmentRef);

    const [commit, commitInFlight] = useMutation<ProjectAdvancedSettingsDeleteMutation>(graphql`
        mutation ProjectAdvancedSettingsDeleteMutation($input: DeleteProjectInput!, $connections: [ID!]!) {
            deleteProject(input: $input) {
                project {
                    id @deleteEdge(connections: $connections)
                    organizationName
                }
                problems {
                    message
                    field
                    type
                }
            }
        }
    `);

    const onDeleteConfirmationDialogClosed = (confirm?: boolean) => {
        if (confirm) {
            commit({
                variables: {
                    input: {
                        id: data.id
                    },
                    connections: GetConnections(data.organizationId)
                },
                onCompleted: data => {
                    setShowDeleteConfirmationDialog(false);

                    if (data.deleteProject.problems.length) {
                        enqueueSnackbar(data.deleteProject.problems.map((problem: any) => problem.message).join('; '), { variant: 'warning' });
                    } else {
                        enqueueSnackbar('Project deleted', { variant: 'success' });
                        navigate(`../../organizations/${data.deleteProject.project?.organizationName}/-/projects`);
                    }
                },
                onError: error => {
                    setShowDeleteConfirmationDialog(false);
                    enqueueSnackbar(`Unexpected error: ${error.message}`, { variant: 'error' });
                }
            });
        } else {
            setShowDeleteConfirmationDialog(false);
        }
    }

    return (
        <Box>
            <SettingsToggleButton
                title="Advanced Settings"
                showSettings={showSettings}
                onToggle={() => setShowSettings(!showSettings)}
            />
            <Collapse
                in={showSettings}
                timeout="auto"
                unmountOnExit
            >
                <Typography variant="subtitle1" gutterBottom>Delete Project</Typography>
                <Alert sx={{ mb: 2 }} severity="error">Deleting a project is a permanent action that cannot be undone.</Alert>
                <Box>
                    <Button
                        variant="outlined"
                        color="error"
                        onClick={() => setShowDeleteConfirmationDialog(true)}
                    >Delete Project</Button>
                </Box>
            </Collapse>
            <DeleteConfirmationDialog
                keepMounted
                open={showDeleteConfirmationDialog}
                deleteInProgress={commitInFlight}
                fragmentRef={data}
                onClose={onDeleteConfirmationDialogClosed}
            />
        </Box>
    );
}

export default ProjectAdvancedSettings
