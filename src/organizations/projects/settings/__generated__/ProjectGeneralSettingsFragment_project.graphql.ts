/**
 * @generated SignedSource<<ca8c4f8d3cef7ebad0298b60af54848c>>
 * @lightSyntaxTransform
 * @nogrep
 */

/* tslint:disable */
/* eslint-disable */
// @ts-nocheck

import { Fragment, ReaderFragment } from 'relay-runtime';
import { FragmentRefs } from "relay-runtime";
export type ProjectGeneralSettingsFragment_project$data = {
  readonly description: string;
  readonly id: string;
  readonly name: string;
  readonly " $fragmentType": "ProjectGeneralSettingsFragment_project";
};
export type ProjectGeneralSettingsFragment_project$key = {
  readonly " $data"?: ProjectGeneralSettingsFragment_project$data;
  readonly " $fragmentSpreads": FragmentRefs<"ProjectGeneralSettingsFragment_project">;
};

const node: ReaderFragment = {
  "argumentDefinitions": [],
  "kind": "Fragment",
  "metadata": null,
  "name": "ProjectGeneralSettingsFragment_project",
  "selections": [
    {
      "alias": null,
      "args": null,
      "kind": "ScalarField",
      "name": "id",
      "storageKey": null
    },
    {
      "alias": null,
      "args": null,
      "kind": "ScalarField",
      "name": "name",
      "storageKey": null
    },
    {
      "alias": null,
      "args": null,
      "kind": "ScalarField",
      "name": "description",
      "storageKey": null
    }
  ],
  "type": "Project",
  "abstractKey": null
};

(node as any).hash = "c9ab732ff67716d00bfb989f4b34e768";

export default node;
