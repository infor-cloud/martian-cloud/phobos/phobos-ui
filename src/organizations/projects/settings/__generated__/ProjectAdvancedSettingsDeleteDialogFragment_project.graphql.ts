/**
 * @generated SignedSource<<a776b22642d9aec533c699102be3ce39>>
 * @lightSyntaxTransform
 * @nogrep
 */

/* tslint:disable */
/* eslint-disable */
// @ts-nocheck

import { Fragment, ReaderFragment } from 'relay-runtime';
import { FragmentRefs } from "relay-runtime";
export type ProjectAdvancedSettingsDeleteDialogFragment_project$data = {
  readonly name: string;
  readonly organizationName: string;
  readonly " $fragmentType": "ProjectAdvancedSettingsDeleteDialogFragment_project";
};
export type ProjectAdvancedSettingsDeleteDialogFragment_project$key = {
  readonly " $data"?: ProjectAdvancedSettingsDeleteDialogFragment_project$data;
  readonly " $fragmentSpreads": FragmentRefs<"ProjectAdvancedSettingsDeleteDialogFragment_project">;
};

const node: ReaderFragment = {
  "argumentDefinitions": [],
  "kind": "Fragment",
  "metadata": null,
  "name": "ProjectAdvancedSettingsDeleteDialogFragment_project",
  "selections": [
    {
      "alias": null,
      "args": null,
      "kind": "ScalarField",
      "name": "name",
      "storageKey": null
    },
    {
      "alias": null,
      "args": null,
      "kind": "ScalarField",
      "name": "organizationName",
      "storageKey": null
    }
  ],
  "type": "Project",
  "abstractKey": null
};

(node as any).hash = "cf3453828ecf5fdea23a9571f650f3a2";

export default node;
