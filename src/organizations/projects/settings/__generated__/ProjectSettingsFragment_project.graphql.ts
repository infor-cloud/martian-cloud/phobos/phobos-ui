/**
 * @generated SignedSource<<56fff6817a4f82092e7f31440ba81928>>
 * @lightSyntaxTransform
 * @nogrep
 */

/* tslint:disable */
/* eslint-disable */
// @ts-nocheck

import { Fragment, ReaderFragment } from 'relay-runtime';
import { FragmentRefs } from "relay-runtime";
export type ProjectSettingsFragment_project$data = {
  readonly metadata: {
    readonly prn: string;
  };
  readonly " $fragmentSpreads": FragmentRefs<"ProjectAdvancedSettingsFragment_project" | "ProjectEnvironmentRules_project" | "ProjectGeneralSettingsFragment_project">;
  readonly " $fragmentType": "ProjectSettingsFragment_project";
};
export type ProjectSettingsFragment_project$key = {
  readonly " $data"?: ProjectSettingsFragment_project$data;
  readonly " $fragmentSpreads": FragmentRefs<"ProjectSettingsFragment_project">;
};

const node: ReaderFragment = {
  "argumentDefinitions": [],
  "kind": "Fragment",
  "metadata": null,
  "name": "ProjectSettingsFragment_project",
  "selections": [
    {
      "alias": null,
      "args": null,
      "concreteType": "ResourceMetadata",
      "kind": "LinkedField",
      "name": "metadata",
      "plural": false,
      "selections": [
        {
          "alias": null,
          "args": null,
          "kind": "ScalarField",
          "name": "prn",
          "storageKey": null
        }
      ],
      "storageKey": null
    },
    {
      "args": null,
      "kind": "FragmentSpread",
      "name": "ProjectAdvancedSettingsFragment_project"
    },
    {
      "args": null,
      "kind": "FragmentSpread",
      "name": "ProjectGeneralSettingsFragment_project"
    },
    {
      "args": null,
      "kind": "FragmentSpread",
      "name": "ProjectEnvironmentRules_project"
    }
  ],
  "type": "Project",
  "abstractKey": null
};

(node as any).hash = "390b3d418dbc541e2ce3a6f46d3d504d";

export default node;
