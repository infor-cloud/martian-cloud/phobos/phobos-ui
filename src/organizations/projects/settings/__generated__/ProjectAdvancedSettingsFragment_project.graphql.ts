/**
 * @generated SignedSource<<896921505ea598633c462c96f49040b4>>
 * @lightSyntaxTransform
 * @nogrep
 */

/* tslint:disable */
/* eslint-disable */
// @ts-nocheck

import { Fragment, ReaderFragment } from 'relay-runtime';
import { FragmentRefs } from "relay-runtime";
export type ProjectAdvancedSettingsFragment_project$data = {
  readonly id: string;
  readonly name: string;
  readonly organizationId: string;
  readonly " $fragmentSpreads": FragmentRefs<"ProjectAdvancedSettingsDeleteDialogFragment_project">;
  readonly " $fragmentType": "ProjectAdvancedSettingsFragment_project";
};
export type ProjectAdvancedSettingsFragment_project$key = {
  readonly " $data"?: ProjectAdvancedSettingsFragment_project$data;
  readonly " $fragmentSpreads": FragmentRefs<"ProjectAdvancedSettingsFragment_project">;
};

const node: ReaderFragment = {
  "argumentDefinitions": [],
  "kind": "Fragment",
  "metadata": null,
  "name": "ProjectAdvancedSettingsFragment_project",
  "selections": [
    {
      "alias": null,
      "args": null,
      "kind": "ScalarField",
      "name": "id",
      "storageKey": null
    },
    {
      "alias": null,
      "args": null,
      "kind": "ScalarField",
      "name": "name",
      "storageKey": null
    },
    {
      "alias": null,
      "args": null,
      "kind": "ScalarField",
      "name": "organizationId",
      "storageKey": null
    },
    {
      "args": null,
      "kind": "FragmentSpread",
      "name": "ProjectAdvancedSettingsDeleteDialogFragment_project"
    }
  ],
  "type": "Project",
  "abstractKey": null
};

(node as any).hash = "16a6528971f1a2ec8b0f028be63802e3";

export default node;
