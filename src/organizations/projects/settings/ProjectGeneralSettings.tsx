import { useState } from 'react';
import { Box, Collapse } from '@mui/material';
import { LoadingButton } from '@mui/lab';
import { useFragment, useMutation } from 'react-relay/hooks';
import graphql from "babel-plugin-relay/macro";
import { useSnackbar } from 'notistack';
import SettingsToggleButton from '../../../settings/SettingsToggleButton';
import { MutationError } from '../../../common/error';
import GeneralSettingsForm, { FormData } from '../../../settings/GeneralSettingsForm';
import { ProjectGeneralSettingsFragment_project$key } from './__generated__/ProjectGeneralSettingsFragment_project.graphql';
import { ProjectGeneralSettingsUpdateMutation } from './__generated__/ProjectGeneralSettingsUpdateMutation.graphql';

interface Props {
    fragmentRef: ProjectGeneralSettingsFragment_project$key
}

function ProjectGeneralSettings({ fragmentRef }: Props) {
    const { enqueueSnackbar } = useSnackbar();
    const [showSettings, setShowSettings] = useState<boolean>(false);

    const data = useFragment<ProjectGeneralSettingsFragment_project$key>(graphql`
        fragment ProjectGeneralSettingsFragment_project on Project
        {
            id
            name
            description
        }
    `, fragmentRef)

    const [error, setError] = useState<MutationError>()
    const [inputForm, setInputForm] = useState<{ name: string, description: string }>(
        {
            name: data.name,
            description: data.description
        }
    );

    const [commit, isInFlight] = useMutation<ProjectGeneralSettingsUpdateMutation>(graphql`
        mutation ProjectGeneralSettingsUpdateMutation($input: UpdateProjectInput!) {
            updateProject(input: $input) {
                project {
                    description
                }
                problems {
                    message
                    field
                    type
                }
            }
        }
    `);

    const onUpdate = () => {
        commit({
            variables: {
                input: {
                    id: data.id,
                    description: inputForm.description.trim()
                }
            },
            onCompleted: (data) => {
                if (data.updateProject.problems.length) {
                    setError({
                        severity: 'warning',
                        message: data.updateProject.problems.map((problem: any) => problem.message).join('; ')
                    });
                } else if (!data.updateProject.project) {
                    setError({
                        severity: 'error',
                        message: "Unexpected error occurred"
                    });
                } else {
                    enqueueSnackbar('Project settings updated', { variant: 'success' });
                }
            },
            onError: error => {
                setError({
                    severity: 'error',
                    message: `Unexpected error occurred: ${error.message}`
                });
            }
        });
    };

    return (
        <Box>
            <SettingsToggleButton
                title="General Settings"
                showSettings={showSettings}
                onToggle={() => setShowSettings(!showSettings)}
            />
            <Collapse
                in={showSettings}
                timeout="auto"
                unmountOnExit
            >
                <GeneralSettingsForm
                    editMode
                    error={error}
                    data={inputForm}
                    onChange={(data: FormData) => setInputForm(data)}
                />
                <LoadingButton
                    sx={{ mt: 1 }}
                    size="small"
                    disabled={data.description === inputForm.description}
                    loading={isInFlight}
                    variant="outlined"
                    color="primary"
                    onClick={onUpdate}
                >Save changes
                </LoadingButton>
            </Collapse>
        </Box>
    );
}

export default ProjectGeneralSettings
