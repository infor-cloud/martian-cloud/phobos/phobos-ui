import { Link as RouterLink } from 'react-router-dom';
import { useState } from 'react';
import { Box, Button, Collapse } from '@mui/material';
import { ConnectionHandler, usePaginationFragment, useFragment, useLazyLoadQuery } from 'react-relay/hooks';
import graphql from 'babel-plugin-relay/macro';
import SettingsToggleButton from '../../../../settings/SettingsToggleButton';
import EnvironmentRuleList from '../../../../environmentrules/EnvironmentRuleList';
import { ProjectEnvironmentRules_project$key } from './__generated__/ProjectEnvironmentRules_project.graphql';
import { ProjectEnvironmentRulesQuery } from './__generated__/ProjectEnvironmentRulesQuery.graphql';
import { ProjectEnvironmentRules_environmentRules$key } from './__generated__/ProjectEnvironmentRules_environmentRules.graphql';
import { ProjectEnvironmentRulesPaginationQuery } from './__generated__/ProjectEnvironmentRulesPaginationQuery.graphql';

export function GetConnections(projectId: string): [string] {
    const connectionId = ConnectionHandler.getConnectionID(
        projectId,
        'ProjectEnvironmentRules_environmentRules',
        { sort: 'ENVIRONMENT_NAME_ASC' }
    );
    return [connectionId];
}

const query = graphql`
    query ProjectEnvironmentRulesQuery($first: Int!, $after: String, $id: String!) {
        node(id: $id) {
            ... on Project {
                id
                name
                ...ProjectEnvironmentRules_environmentRules
            }
        }
    }
`;

interface Props {
    fragmentRef: ProjectEnvironmentRules_project$key
}

function ProjectEnvironmentRules({ fragmentRef }: Props) {
    const [showSettings, setShowSettings] = useState<boolean>(false);

    const project = useFragment<ProjectEnvironmentRules_project$key>(graphql`
        fragment ProjectEnvironmentRules_project on Project
        {
            id
        }
    `, fragmentRef);

    const queryData = useLazyLoadQuery<ProjectEnvironmentRulesQuery>(query, { id: project.id, first: 10 }, { fetchPolicy: 'store-and-network' });

    const { data, loadNext, hasNext } = usePaginationFragment<ProjectEnvironmentRulesPaginationQuery, ProjectEnvironmentRules_environmentRules$key>(
        graphql`
            fragment ProjectEnvironmentRules_environmentRules on Project
            @refetchable(queryName: "ProjectEnvironmentRulesPaginationQuery")
             {
                id
                name
                environmentRules(
                    first: $first
                    after: $after
                    sort: ENVIRONMENT_NAME_ASC
                ) @connection(key: "ProjectEnvironmentRules_environmentRules") {
                    edges{
                        node {
                            id
                        }
                    }
                    ...EnvironmentRuleListFragment_environmentRules
                }
            }
        `, queryData.node);

    return (
        <Box>
            <SettingsToggleButton
                title="Environments"
                showSettings={showSettings}
                onToggle={() => setShowSettings(!showSettings)}
            />
            <Collapse
                in={showSettings}
                timeout="auto"
                unmountOnExit
            >
                <Box mb={4}>
                    {data?.environmentRules && <EnvironmentRuleList
                        getConnections={() => GetConnections(data.id)}
                        fragmentRef={data.environmentRules}
                        loadNext={loadNext}
                        hasNext={hasNext}
                    />}
                    <Button
                        color="secondary"
                        variant="outlined"
                        component={RouterLink}
                        to={'new_environment_rule'}
                    >
                        Add Rule
                    </Button>
                </Box>
            </Collapse>
        </Box>
    );
}

export default ProjectEnvironmentRules;
