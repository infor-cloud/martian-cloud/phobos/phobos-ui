/**
 * @generated SignedSource<<629c6f1ed7262a7d4c8f44e25a42d9a1>>
 * @lightSyntaxTransform
 * @nogrep
 */

/* tslint:disable */
/* eslint-disable */
// @ts-nocheck

import { Fragment, ReaderFragment } from 'relay-runtime';
import { FragmentRefs } from "relay-runtime";
export type NewProjectEnvironmentRuleFragment_project$data = {
  readonly environmentNames: ReadonlyArray<string>;
  readonly id: string;
  readonly metadata: {
    readonly prn: string;
  };
  readonly " $fragmentType": "NewProjectEnvironmentRuleFragment_project";
};
export type NewProjectEnvironmentRuleFragment_project$key = {
  readonly " $data"?: NewProjectEnvironmentRuleFragment_project$data;
  readonly " $fragmentSpreads": FragmentRefs<"NewProjectEnvironmentRuleFragment_project">;
};

const node: ReaderFragment = {
  "argumentDefinitions": [],
  "kind": "Fragment",
  "metadata": null,
  "name": "NewProjectEnvironmentRuleFragment_project",
  "selections": [
    {
      "alias": null,
      "args": null,
      "kind": "ScalarField",
      "name": "id",
      "storageKey": null
    },
    {
      "alias": null,
      "args": null,
      "concreteType": "ResourceMetadata",
      "kind": "LinkedField",
      "name": "metadata",
      "plural": false,
      "selections": [
        {
          "alias": null,
          "args": null,
          "kind": "ScalarField",
          "name": "prn",
          "storageKey": null
        }
      ],
      "storageKey": null
    },
    {
      "alias": null,
      "args": null,
      "kind": "ScalarField",
      "name": "environmentNames",
      "storageKey": null
    }
  ],
  "type": "Project",
  "abstractKey": null
};

(node as any).hash = "b18e524921a61781e02f6fee1a4e5677";

export default node;
