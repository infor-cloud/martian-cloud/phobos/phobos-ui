/**
 * @generated SignedSource<<dbb815d7f9172b00ff810b1aa6abc767>>
 * @lightSyntaxTransform
 * @nogrep
 */

/* tslint:disable */
/* eslint-disable */
// @ts-nocheck

import { ReaderFragment, RefetchableFragment } from 'relay-runtime';
import { FragmentRefs } from "relay-runtime";
export type ProjectEnvironmentRules_environmentRules$data = {
  readonly environmentRules: {
    readonly edges: ReadonlyArray<{
      readonly node: {
        readonly id: string;
      } | null | undefined;
    } | null | undefined> | null | undefined;
    readonly " $fragmentSpreads": FragmentRefs<"EnvironmentRuleListFragment_environmentRules">;
  };
  readonly id: string;
  readonly name: string;
  readonly " $fragmentType": "ProjectEnvironmentRules_environmentRules";
};
export type ProjectEnvironmentRules_environmentRules$key = {
  readonly " $data"?: ProjectEnvironmentRules_environmentRules$data;
  readonly " $fragmentSpreads": FragmentRefs<"ProjectEnvironmentRules_environmentRules">;
};

const node: ReaderFragment = (function(){
var v0 = [
  "environmentRules"
],
v1 = {
  "alias": null,
  "args": null,
  "kind": "ScalarField",
  "name": "id",
  "storageKey": null
};
return {
  "argumentDefinitions": [
    {
      "kind": "RootArgument",
      "name": "after"
    },
    {
      "kind": "RootArgument",
      "name": "first"
    }
  ],
  "kind": "Fragment",
  "metadata": {
    "connection": [
      {
        "count": "first",
        "cursor": "after",
        "direction": "forward",
        "path": (v0/*: any*/)
      }
    ],
    "refetch": {
      "connection": {
        "forward": {
          "count": "first",
          "cursor": "after"
        },
        "backward": null,
        "path": (v0/*: any*/)
      },
      "fragmentPathInResult": [
        "node"
      ],
      "operation": require('./ProjectEnvironmentRulesPaginationQuery.graphql'),
      "identifierInfo": {
        "identifierField": "id",
        "identifierQueryVariableName": "id"
      }
    }
  },
  "name": "ProjectEnvironmentRules_environmentRules",
  "selections": [
    (v1/*: any*/),
    {
      "alias": null,
      "args": null,
      "kind": "ScalarField",
      "name": "name",
      "storageKey": null
    },
    {
      "alias": "environmentRules",
      "args": [
        {
          "kind": "Literal",
          "name": "sort",
          "value": "ENVIRONMENT_NAME_ASC"
        }
      ],
      "concreteType": "EnvironmentRuleConnection",
      "kind": "LinkedField",
      "name": "__ProjectEnvironmentRules_environmentRules_connection",
      "plural": false,
      "selections": [
        {
          "alias": null,
          "args": null,
          "concreteType": "EnvironmentRuleEdge",
          "kind": "LinkedField",
          "name": "edges",
          "plural": true,
          "selections": [
            {
              "alias": null,
              "args": null,
              "concreteType": "EnvironmentRule",
              "kind": "LinkedField",
              "name": "node",
              "plural": false,
              "selections": [
                (v1/*: any*/),
                {
                  "alias": null,
                  "args": null,
                  "kind": "ScalarField",
                  "name": "__typename",
                  "storageKey": null
                }
              ],
              "storageKey": null
            },
            {
              "alias": null,
              "args": null,
              "kind": "ScalarField",
              "name": "cursor",
              "storageKey": null
            }
          ],
          "storageKey": null
        },
        {
          "args": null,
          "kind": "FragmentSpread",
          "name": "EnvironmentRuleListFragment_environmentRules"
        },
        {
          "alias": null,
          "args": null,
          "concreteType": "PageInfo",
          "kind": "LinkedField",
          "name": "pageInfo",
          "plural": false,
          "selections": [
            {
              "alias": null,
              "args": null,
              "kind": "ScalarField",
              "name": "endCursor",
              "storageKey": null
            },
            {
              "alias": null,
              "args": null,
              "kind": "ScalarField",
              "name": "hasNextPage",
              "storageKey": null
            }
          ],
          "storageKey": null
        }
      ],
      "storageKey": "__ProjectEnvironmentRules_environmentRules_connection(sort:\"ENVIRONMENT_NAME_ASC\")"
    }
  ],
  "type": "Project",
  "abstractKey": null
};
})();

(node as any).hash = "17028cebff79f74ff05d38000f3d3e6a";

export default node;
