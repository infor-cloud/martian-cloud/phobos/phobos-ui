/**
 * @generated SignedSource<<871b34bf98d2b718ed9e78aef28af94a>>
 * @lightSyntaxTransform
 * @nogrep
 */

/* tslint:disable */
/* eslint-disable */
// @ts-nocheck

import { Fragment, ReaderFragment } from 'relay-runtime';
import { FragmentRefs } from "relay-runtime";
export type ProjectEnvironmentRules_project$data = {
  readonly id: string;
  readonly " $fragmentType": "ProjectEnvironmentRules_project";
};
export type ProjectEnvironmentRules_project$key = {
  readonly " $data"?: ProjectEnvironmentRules_project$data;
  readonly " $fragmentSpreads": FragmentRefs<"ProjectEnvironmentRules_project">;
};

const node: ReaderFragment = {
  "argumentDefinitions": [],
  "kind": "Fragment",
  "metadata": null,
  "name": "ProjectEnvironmentRules_project",
  "selections": [
    {
      "alias": null,
      "args": null,
      "kind": "ScalarField",
      "name": "id",
      "storageKey": null
    }
  ],
  "type": "Project",
  "abstractKey": null
};

(node as any).hash = "0eeec5e700e503888da92cecff58c391";

export default node;
