import { Box, Typography } from '@mui/material';
import { useOutletContext, useParams } from 'react-router-dom';
import ProjectBreadcrumbs from '../../ProjectBreadcrumbs';
import graphql from 'babel-plugin-relay/macro';
import { useFragment, useLazyLoadQuery } from "react-relay/hooks";
import EditEnvironmentRule from '../../../../environmentrules/EditEnvironmentRule';
import { EditProjectEnvironmentRuleFragment_project$key } from './__generated__/EditProjectEnvironmentRuleFragment_project.graphql';
import { EditProjectEnvironmentRuleQuery } from './__generated__/EditProjectEnvironmentRuleQuery.graphql';

function EditProjectEnvironmentRule() {
    const environmentRuleId = useParams().environmentRuleId as string;
    const context = useOutletContext<EditProjectEnvironmentRuleFragment_project$key>();

    const project = useFragment<EditProjectEnvironmentRuleFragment_project$key>(
        graphql`
            fragment EditProjectEnvironmentRuleFragment_project on Project
            {
                id
                metadata {
                    prn
                }
            }
        `, context
    );

    const queryData = useLazyLoadQuery<EditProjectEnvironmentRuleQuery>(graphql`
        query EditProjectEnvironmentRuleQuery($id: String!) {
            node(id: $id) {
                ... on EnvironmentRule {
                    id
                    ...EditEnvironmentRuleFragment_environmentRule
                }
            }
        }
    `, { id: environmentRuleId }, { fetchPolicy: 'store-and-network' });

    return queryData.node ? (
        <Box>
            <ProjectBreadcrumbs
                prn={project.metadata.prn}
                childRoutes={[
                    { title: "settings", path: 'settings' },
                    { title: "edit environment rule", path: 'edit_environment_rule' }
                ]}
            />
            <EditEnvironmentRule
                projectId={project.id}
                fragmentRef={queryData.node} />
        </Box>
    ) : <Box display="flex" justifyContent="center" mt={4}>
        <Typography color="textSecondary">Environment rule not found</Typography>
    </Box>;
}

export default EditProjectEnvironmentRule;
