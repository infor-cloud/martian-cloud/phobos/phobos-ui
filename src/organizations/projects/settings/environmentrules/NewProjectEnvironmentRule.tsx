import Box from "@mui/material/Box";
import { useOutletContext } from "react-router-dom";
import ProjectBreadcrumbs from "../../ProjectBreadcrumbs";
import { useFragment } from "react-relay/hooks";
import graphql from "babel-plugin-relay/macro";
import { GetConnections } from "./ProjectEnvironmentRules";
import NewEnvironmentRule from "../../../../environmentrules/NewEnvironmentRule";
import { NewProjectEnvironmentRuleFragment_project$key } from "./__generated__/NewProjectEnvironmentRuleFragment_project.graphql";

function NewProjectEnvironmentRule() {
    const context = useOutletContext<NewProjectEnvironmentRuleFragment_project$key>();

    const project = useFragment<NewProjectEnvironmentRuleFragment_project$key>(
        graphql`
            fragment NewProjectEnvironmentRuleFragment_project on Project
            {
                id
                metadata {
                    prn
                }
                environmentNames
            }
        `, context
    );

    return (
        <Box>
            <ProjectBreadcrumbs
                prn={project.metadata.prn}
                childRoutes={[
                    { title: "settings", path: 'settings' },
                    { title: "new environment rule", path: 'new_environment_rule' },
                ]}
            />
            <NewEnvironmentRule
                projectId={project.id}
                getConnections={GetConnections}
                environmentNames={project.environmentNames}
            />
        </Box>
    );
}

export default NewProjectEnvironmentRule;
