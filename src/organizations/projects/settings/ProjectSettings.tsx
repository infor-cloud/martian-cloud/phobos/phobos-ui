import { Box, Divider, Typography, styled } from '@mui/material';
import { useOutletContext } from 'react-router-dom';
import { useFragment } from 'react-relay/hooks';
import graphql from "babel-plugin-relay/macro";
import ProjectBreadcrumbs from '../ProjectBreadcrumbs';
import ProjectGeneralSettings from './ProjectGeneralSettings';
import ProjectAdvancedSettings from './ProjectAdvancedSettings';
import { ProjectSettingsFragment_project$key } from './__generated__/ProjectSettingsFragment_project.graphql';
import ProjectEnvironmentRules from './environmentrules/ProjectEnvironmentRules';

const StyledDivider = styled(
    Divider
)(() => ({
    margin: "24px 0"
}));

function ProjectSettings() {
    const context = useOutletContext<ProjectSettingsFragment_project$key>()

    const data = useFragment<ProjectSettingsFragment_project$key>(
        graphql`
        fragment ProjectSettingsFragment_project on Project
        {
            metadata {
                prn
            }
            ...ProjectAdvancedSettingsFragment_project
            ...ProjectGeneralSettingsFragment_project
            ...ProjectEnvironmentRules_project
        }
    `, context);

    return (
        <Box>
            <ProjectBreadcrumbs
                prn={data.metadata.prn}
                childRoutes={[{ title: "settings", path: 'settings' }]}
            />
            <Typography mb={4} variant="h5" gutterBottom>Project Settings</Typography>
            <StyledDivider />
            <ProjectGeneralSettings fragmentRef={data} />
            <StyledDivider />
            <ProjectEnvironmentRules fragmentRef={data} />
            <StyledDivider />
            <ProjectAdvancedSettings fragmentRef={data} />
        </Box>
    );
}

export default ProjectSettings
