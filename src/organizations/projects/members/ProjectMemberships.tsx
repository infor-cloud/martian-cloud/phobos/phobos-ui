import { useState } from 'react';
import { Box, Button, Typography, useTheme } from '@mui/material';
import { useOutletContext } from 'react-router-dom';
import { ConnectionHandler, useFragment, useLazyLoadQuery, useMutation, usePaginationFragment } from 'react-relay/hooks';
import { useSnackbar } from 'notistack';
import { Link as RouterLink } from 'react-router-dom';
import graphql from 'babel-plugin-relay/macro';
import ProjectBreadcrumbs from '../ProjectBreadcrumbs';
import MembershipList from '../../../members/MembershipList';
import MembershipDeleteConfirmationDialog from '../../../members/MembershipDeleteConfirmationDialog';
import { ProjectMembershipsPaginationQuery } from './__generated__/ProjectMembershipsPaginationQuery.graphql';
import { ProjectMembershipsFragment_memberships$key } from './__generated__/ProjectMembershipsFragment_memberships.graphql';
import { ProjectMembershipsDeleteMembershipMutation } from './__generated__/ProjectMembershipsDeleteMembershipMutation.graphql';
import { ProjectMembershipsQuery } from './__generated__/ProjectMembershipsQuery.graphql';
import { ProjectMembershipsFragment_project$key } from './__generated__/ProjectMembershipsFragment_project.graphql';

export function GetProjectConnections(projectId: string): [string] {
    const connectionId = ConnectionHandler.getConnectionID(
        projectId,
        'ProjectMemberships_memberships'
    );
    return [connectionId];
}

const query = graphql`
    query ProjectMembershipsQuery($id: String!, $first: Int!, $after: String) {
        node(id: $id) {
            ... on Project {
                ...ProjectMembershipsFragment_memberships
            }
        }
    }
`;

function ProjectMemberships() {
    const theme = useTheme();
    const { enqueueSnackbar } = useSnackbar();
    const context = useOutletContext<ProjectMembershipsFragment_project$key>();

    const project = useFragment<ProjectMembershipsFragment_project$key>(
        graphql`
            fragment ProjectMembershipsFragment_project on Project
            {
                id
                metadata {
                    prn
                }
            }
        `, context);

    const queryData = useLazyLoadQuery<ProjectMembershipsQuery>(query, { id: project.id, first: 100 }, { fetchPolicy: 'store-and-network' });

    const { data, loadNext, hasNext } = usePaginationFragment<ProjectMembershipsPaginationQuery, ProjectMembershipsFragment_memberships$key>(
        graphql`
            fragment ProjectMembershipsFragment_memberships on Project
            @refetchable(queryName: "ProjectMembershipsPaginationQuery")
             {
                memberships(
                    first: $first
                    after: $after
                ) @connection(key: "ProjectMemberships_memberships") {
                    edges{
                        node {
                            id
                        }
                    }
                    ...MembershipListFragment_memberships
                }
            }
        `, queryData.node);

    const [commitDeleteMembership, deleteInFlight] = useMutation<ProjectMembershipsDeleteMembershipMutation>(graphql`
        mutation ProjectMembershipsDeleteMembershipMutation($input: DeleteMembershipInput!, $connections: [ID!]!) {
            deleteMembership(input: $input) {
                membership {
                    id @deleteEdge(connections: $connections)
                }
                problems {
                    message
                    field
                    type
                }
            }
        }
    `);

    const [membershipToDelete, setMembershipToDelete] = useState<any>(null);

    const onShowDeleteConfirmationDialog = (membership: any) => {
        setMembershipToDelete(membership);
    };

    const onDelete = (confirm?: boolean) => {
        if (confirm && membershipToDelete) {
            commitDeleteMembership({
                variables: {
                    input: {
                        id: membershipToDelete.id
                    },
                    connections: GetProjectConnections(project.id)
                },
                onCompleted: data => {
                    if (data.deleteMembership.problems.length) {
                        enqueueSnackbar(data.deleteMembership.problems.map(problem => problem.message).join('; '), { variant: 'warning' });
                    }
                    setMembershipToDelete(null);
                },
                onError: error => {
                    enqueueSnackbar(`Unexpected error occurred: ${error.message}`, { variant: 'error' });
                    setMembershipToDelete(null);
                }
            });
        } else {
            setMembershipToDelete(null);
        }
    };

    const memberships = data?.memberships as any;
    const orgName = project.metadata.prn.split(':')[2].split('/')[0] as string;

    return (
        <Box>
            <ProjectBreadcrumbs
                prn={project.metadata.prn}
                childRoutes={[
                    { title: "members", path: 'members' }
                ]} />
            <Box sx={{
                display: 'flex',
                flexDirection: 'row',
                justifyContent: 'space-between',
                marginBottom: 2,
                [theme.breakpoints.down('md')]: {
                    flexDirection: 'column',
                    alignItems: 'flex-start',
                    '& > *:not(:last-child)': { marginBottom: 2 },
                }
            }}>
                <Box>
                    <Typography variant="h5" gutterBottom>Members</Typography>
                    <Typography variant="body2">
                        A member is associated with a role and can be a user, team, or service account.
                    </Typography>
                </Box>
                <Box>
                    <Button component={RouterLink} variant="outlined" color="primary" to="new">Add Member</Button>
                </Box>
            </Box>
            <MembershipList
                orgName={orgName}
                hasNext={hasNext}
                loadNext={loadNext}
                fragmentRef={memberships}
                onDelete={onShowDeleteConfirmationDialog}
            />
            {membershipToDelete && <MembershipDeleteConfirmationDialog
                membership={membershipToDelete}
                deleteInProgress={deleteInFlight}
                onClose={onDelete}
            />}
        </Box>
    );
}

export default ProjectMemberships
