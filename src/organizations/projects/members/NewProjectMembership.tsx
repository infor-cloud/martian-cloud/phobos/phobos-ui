import { useState } from 'react';
import { Box } from '@mui/material';
import graphql from 'babel-plugin-relay/macro';
import { MutationError } from '../../../common/error';
import { useFragment, useMutation } from 'react-relay/hooks';
import { useNavigate, useOutletContext } from 'react-router-dom';
import ProjectBreadcrumbs from '../ProjectBreadcrumbs';
import { GetProjectConnections } from './ProjectMemberships';
import { NewProjectMembershipFragment_memberships$key } from './__generated__/NewProjectMembershipFragment_memberships.graphql';
import NewMembership from '../../../members/NewMembership';
import { NewProjectMembershipCreateMembershipMutation } from './__generated__/NewProjectMembershipCreateMembershipMutation.graphql';

function NewProjectMembership() {
    const context = useOutletContext<NewProjectMembershipFragment_memberships$key>();
    const navigate = useNavigate();
    const [error, setError] = useState<MutationError>();

    const data = useFragment(graphql`
        fragment NewProjectMembershipFragment_memberships on Project
        {
            id
            metadata {
                prn
            }
        }
    `, context);

    const [commit, isInFlight] = useMutation<NewProjectMembershipCreateMembershipMutation>(graphql`
        mutation NewProjectMembershipCreateMembershipMutation($input: CreateMembershipInput!, $connections: [ID!]!) {
            createMembership(input: $input) {
                membership @prependNode(connections: $connections, edgeTypeName: "MembershipEdge") {
                    ...MembershipListItemFragment_membership
                }
                problems {
                    message
                    field
                    type
                }
            }
        }
    `);

    const onCreate = (member: string | undefined, role: string | undefined, memberType: string) => {
        if (member && role) {
            const input = {
                projectId: data.id,
                scope: 'PROJECT',
                role
            } as any;

            if (memberType === 'user') {
                input.username = member;
            } else if (memberType === 'team') {
                input.teamName = member;
            } else if (memberType === 'serviceAccount') {
                input.serviceAccountId = member;
            }
            else {
                throw new Error(`Invalid member type ${memberType}`);
            }
            commit({
                variables: {
                    input,
                    connections: GetProjectConnections(data.id)
                },
                onCompleted: data => {
                    if (data.createMembership.problems.length) {
                        setError({
                            severity: 'warning',
                            message: data.createMembership.problems.map(problem => problem.message).join('; ')
                        });
                    } else {
                        navigate(`..`);
                    }
                },
                onError: (error) => {
                    setError({
                        severity: 'error',
                        message: `Unexpected Error Occurred: ${error.message}`
                    });
                }
            });
        }
    };

    return (
        <Box>
            <ProjectBreadcrumbs
                prn={data.metadata.prn}
                childRoutes={[
                    { title: "members", path: 'members' },
                    { title: "new", path: 'new' }
                ]}
            />
            <NewMembership
                projectId={data.id}
                error={error}
                onCreate={onCreate}
                loading={isInFlight}
            />
        </Box>
    );
}

export default NewProjectMembership;
