import { Box, Chip, Link } from '@mui/material';
import graphql from 'babel-plugin-relay/macro';
import { useFragment } from 'react-relay/hooks';
import { PipelineAnnotationsFragment_pipeline$key } from './__generated__/PipelineAnnotationsFragment_pipeline.graphql';
import React from 'react';

const monoFontFamily = 'ui-monospace,SFMono-Regular,SF Mono,Menlo,Consolas,Liberation Mono,monospace !important';

interface Props {
    fragmentRef: PipelineAnnotationsFragment_pipeline$key;
}

function PipelineAnnotations({ fragmentRef }: Props) {
    const pipeline = useFragment<PipelineAnnotationsFragment_pipeline$key>(
        graphql`
        fragment PipelineAnnotationsFragment_pipeline on Pipeline {
            annotations {
                key
                value
                link
            }
        }
        `, fragmentRef);

    return pipeline.annotations.length > 0 ? (
        <Box display="flex" flexWrap="wrap">
            {pipeline.annotations.map(label => <Chip
                component={label.link ? Link : 'div'}
                href={label.link ? label.link : undefined}
                underline={label.link ? 'hover' : 'none'}
                target='_blank'
                rel='noopener noreferrer'
                sx={{ mb: 0.5, mr: 0.5, cursor: label.link ? 'pointer' : 'default', fontFamily: monoFontFamily }}
                size="xs"
                key={label.key}
                label={<React.Fragment><strong>{label.key}</strong>: {label.value}</React.Fragment>}
            />)}
        </Box>
    ) : null;
}

export default PipelineAnnotations;
