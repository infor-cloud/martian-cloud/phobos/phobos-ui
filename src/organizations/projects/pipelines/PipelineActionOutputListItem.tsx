import { TableCell } from '@mui/material';
import TableRow from '@mui/material/TableRow';
import graphql from 'babel-plugin-relay/macro';
import { useFragment } from 'react-relay/hooks';
import { PipelineActionOutputListItemFragment_output$key } from './__generated__/PipelineActionOutputListItemFragment_output.graphql';

interface Props {
    actionName: string;
    fragmentRef: PipelineActionOutputListItemFragment_output$key;
}

function PipelineActionOutputListItem(props: Props) {
    const { fragmentRef } = props;
    const data = useFragment<PipelineActionOutputListItemFragment_output$key>(
        graphql`
        fragment PipelineActionOutputListItemFragment_output on PipelineActionOutput
        {
            name
            value
            type
        }
      `, fragmentRef);

    const value = data.type === '"string"' ? data.value.slice(1, -1) : data.value;

    return (
        <TableRow
            sx={{ '&:last-child td, &:last-child th': { border: 0 } }}
        >
            <TableCell sx={{ wordBreak: 'break-all' }}>
                {props.actionName}
            </TableCell>
            <TableCell sx={{ wordBreak: 'break-all' }}>
                {data.name}
            </TableCell>
            <TableCell sx={{ wordBreak: 'break-all' }} >
                {value}
            </TableCell>
        </TableRow>
    );
}

export default PipelineActionOutputListItem;
