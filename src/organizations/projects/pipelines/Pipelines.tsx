import { Box, Button, Typography, useTheme } from '@mui/material';
import graphql from 'babel-plugin-relay/macro';
import ProjectBreadcrumbs from '../ProjectBreadcrumbs';
import PipelineList from './PipelineList';
import { Link as RouterLink, useOutletContext } from 'react-router-dom';
import { useFragment } from 'react-relay/hooks';
import { PipelinesFragment_pipelines$key } from './__generated__/PipelinesFragment_pipelines.graphql';

function Pipelines() {
    const theme = useTheme();
    const context = useOutletContext<PipelinesFragment_pipelines$key>();

    const data = useFragment<PipelinesFragment_pipelines$key>(
        graphql`
            fragment PipelinesFragment_pipelines on Project
            {
                metadata {
                    prn
                }
                ...PipelineListFragment_project
            }
        `, context);

    return (
        <Box>
            <ProjectBreadcrumbs
                prn={data.metadata.prn}
                childRoutes={[
                    { title: "pipelines", path: 'pipelines' }
                ]}
            />
            <Box
                sx={{
                    mb: 2,
                    display: 'flex',
                    flexDirection: 'row',
                    justifyContent: 'space-between',
                    [theme.breakpoints.down('md')]: {
                        flexDirection: 'column',
                        alignItems: 'flex-start',
                        '& > *': { marginBottom: 2 },
                    }
                }}>
                <Box>
                    <Typography variant="h5">Pipelines</Typography>
                </Box>
                <Box>
                    <Button
                        sx={{ minWidth: 200 }}
                        component={RouterLink}
                        variant="outlined"
                        to="create"
                    >
                        Create Pipeline
                    </Button>
                </Box>
            </Box>
            <PipelineList fragmentRef={data} />
        </Box>
    );
}

export default Pipelines
