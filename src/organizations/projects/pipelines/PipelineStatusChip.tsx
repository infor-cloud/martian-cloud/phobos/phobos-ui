import { Chip } from '@mui/material';
import { blue, green, grey, orange, purple, red, teal, yellow } from '@mui/material/colors';
import { Link as RouterLink } from 'react-router-dom';

// update when 'status' is added to the pipeline type
export const PIPELINE_STATUS_TYPES = {
    "SUCCEEDED": {
        label: "Succeeded",
        color: green[500]
    },
    "RUNNING": {
        label: "In Progress",
        color: blue[500]
    },
    "CANCELED": {
        label: "Canceled",
        color: red[500]
    },
    "FAILED": {
        label: "Failed",
        color: red[500]
    },
    "PENDING": {
        label: "Pending",
        color: orange[500]
    },
    "READY": {
        label: "Ready",
        color: orange[400]
    },
    "CREATED": {
        label: "Blocked",
        color: grey[400]
    },
    "SKIPPED": {
        label: "Skipped",
        color: yellow[500]
    },
    "WAITING": {
        label: "Scheduled",
        color: orange[400]
    },
    "APPROVAL_PENDING": {
        label: "Approval Pending",
        color: purple[300]
    },
    "CANCELING": {
        label: "Canceling",
        color: orange[400]
    },
    "DEFERRED": {
        label: "Deferred",
        color: teal[500]
    },
} as any;

interface Props {
    to?: string
    status: string
    onClick?: () => void
}

function PipelineStatusChip({ to, status, onClick }: Props) {
    const type = PIPELINE_STATUS_TYPES[status] ?? { label: 'Unknown', color: grey[500] }
    return to ? (
        <Chip
            to={to}
            component={RouterLink}
            clickable
            size="small"
            variant="outlined"
            label={type.label}
            sx={{ color: type.color, borderColor: type.color, fontWeight: 500 }}
        />
    ) : (
        <Chip
            onClick={onClick}
            clickable
            size="small"
            variant="outlined"
            label={type.label}
            sx={{ color: type.color, borderColor: type.color, fontWeight: 500 }}
        />
    );
}

export default PipelineStatusChip
