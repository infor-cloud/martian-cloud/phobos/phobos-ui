import { Box, Button, Paper, Table, TableBody, TableCell, TableContainer, TableHead, TableRow, Typography, useTheme } from '@mui/material';
import graphql from 'babel-plugin-relay/macro';
import { useLazyLoadQuery, usePaginationFragment } from 'react-relay/hooks';
import PipelineNestedPipelineListItem from './PipelineNestedPipelineListItem';
import { PipelineNestedPipelineListFragment_pipelines$key } from './__generated__/PipelineNestedPipelineListFragment_pipelines.graphql';
import { PipelineNestedPipelineListPaginationQuery } from './__generated__/PipelineNestedPipelineListPaginationQuery.graphql';
import { PipelineNestedPipelineListQuery } from './__generated__/PipelineNestedPipelineListQuery.graphql';

const query = graphql`
    query PipelineNestedPipelineListQuery($pipelineId: String!, $first: Int!, $after: String, $nestedPipelinePath: String!) {
        ...PipelineNestedPipelineListFragment_pipelines
    }`;

interface Props {
    pipelineId: string
    nestedPipelinePath: string
    onPipelineSelected: (id: string) => void
}

function PipelineNestedPipelineList({ pipelineId, nestedPipelinePath, onPipelineSelected }: Props) {
    const theme = useTheme();

    const queryData = useLazyLoadQuery<PipelineNestedPipelineListQuery>(query, { pipelineId: pipelineId, nestedPipelinePath, first: 10 }, { fetchPolicy: 'store-and-network' });

    const { data, loadNext, hasNext } = usePaginationFragment<PipelineNestedPipelineListPaginationQuery, PipelineNestedPipelineListFragment_pipelines$key>(
        graphql`
        fragment PipelineNestedPipelineListFragment_pipelines on Query
        @refetchable(queryName: "PipelineNestedPipelineListPaginationQuery") {
            pipelineNode(pipelineId: $pipelineId, nodePath: $nestedPipelinePath) {
                ... on NestedPipeline {
                    pipelines(
                        first: $first
                        after: $after
                        sort: CREATED_AT_DESC
                    ) @connection(key: "PipelineNestedPipelineList_pipelines") {
                        totalCount
                        edges {
                            node {
                                id
                                ...PipelineNestedPipelineListItemFragment
                            }
                        }
                    }
                }
            }
        }
        `, queryData
    );

    return data.pipelineNode?.pipelines? (
        <Box>
            <Box>
                <Paper sx={{ borderBottomLeftRadius: 0, borderBottomRightRadius: 0, border: `1px solid ${theme.palette.divider}` }}>
                    <Box padding={2}>
                        <Typography variant="subtitle1">
                            {data.pipelineNode.pipelines.totalCount} pipeline{data.pipelineNode.pipelines.totalCount !== 1 && 's'}
                        </Typography>
                    </Box>
                </Paper>
                <TableContainer sx={{
                    borderLeft: `1px solid ${theme.palette.divider}`,
                    borderRight: `1px solid ${theme.palette.divider}`,
                    borderBottom: `1px solid ${theme.palette.divider}`,
                    borderBottomLeftRadius: 4,
                    borderBottomRightRadius: 4,
                }}>
                    <Table
                        sx={{ minWidth: 650, tableLayout: 'fixed' }}
                        aria-label="pipelines"
                    >
                        <TableHead>
                            <TableRow>
                                <TableCell>Status</TableCell>
                                <TableCell>ID</TableCell>
                                <TableCell>Created</TableCell>
                            </TableRow>
                        </TableHead>
                        <TableBody>
                            {data.pipelineNode?.pipelines?.edges?.map((edge: any) => (
                                <PipelineNestedPipelineListItem
                                    key={edge.node.id}
                                    fragmentRef={edge.node}
                                    onPipelineSelected={onPipelineSelected}
                                />
                            ))}
                        </TableBody>
                    </Table>
                </TableContainer>
                {hasNext && <Box marginTop={2} display="flex" alignItems="center" justifyContent="center">
                    <Button color="inherit" onClick={() => loadNext(10)}>
                        Load More
                    </Button>
                </Box>}
            </Box>
        </Box>
    ) : null;
}

export default PipelineNestedPipelineList
