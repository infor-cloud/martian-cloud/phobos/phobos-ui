import Box from '@mui/material/Box';
import Paper from '@mui/material/Paper';
import Table from '@mui/material/Table';
import TableBody from '@mui/material/TableBody';
import TableCell from '@mui/material/TableCell';
import TableContainer from '@mui/material/TableContainer';
import TableHead from '@mui/material/TableHead';
import TableRow from '@mui/material/TableRow';
import Typography from '@mui/material/Typography';
import graphql from 'babel-plugin-relay/macro';
import { useFragment } from 'react-relay/hooks';
import PipelineApprovalListItem from './PipelineApprovalListItem';
import { PipelineApprovalsFragment_approvals$key } from './__generated__/PipelineApprovalsFragment_approvals.graphql';

interface Props {
    fragmentRef: PipelineApprovalsFragment_approvals$key
}

function PipelineApprovals(props: Props) {
    const { fragmentRef } = props;

    const data = useFragment<PipelineApprovalsFragment_approvals$key>(
        graphql`
        fragment PipelineApprovalsFragment_approvals on ApprovablePipelineNode
        {
            __typename
            approvals {
                id
                ...PipelineApprovalListItemFragment_approval
            }
        }
      `, fragmentRef)

    return (
        <Box>
            {data.approvals.length === 0 && <Paper variant="outlined" sx={{ marginTop: 4, display: 'flex', justifyContent: 'center' }}>
                <Box padding={4} display="flex" flexDirection="column" justifyContent="center" alignItems="center">
                    <Typography color="textSecondary" align="center">
                        This {data.__typename === 'Pipeline' ? 'pipeline' : 'task'} does not have any approvals
                    </Typography>
                </Box>
            </Paper>}
            {data.approvals.length > 0 && <TableContainer>
                <Table sx={{ tableLayout: 'fixed' }}>
                    <TableHead>
                        <TableRow>
                            <TableCell>
                                <Typography color="textSecondary">Approval Rules</Typography>
                            </TableCell>
                            <TableCell>
                                <Typography color="textSecondary">Approver</Typography>
                            </TableCell>
                            <TableCell>
                                <Typography color="textSecondary">Approved On</Typography>
                            </TableCell>
                        </TableRow>
                    </TableHead>
                    <TableBody>
                        {data.approvals.map((approval: any) => <PipelineApprovalListItem
                            key={approval.id}
                            fragmentRef={approval}
                        />)}
                    </TableBody>
                </Table>
            </TableContainer>}
        </Box>
    );
}

export default PipelineApprovals;
