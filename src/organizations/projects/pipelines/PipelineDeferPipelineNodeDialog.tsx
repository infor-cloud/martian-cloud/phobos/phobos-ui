import { LoadingButton } from "@mui/lab";
import { Button, Dialog, DialogActions, DialogContent, DialogTitle, TextField, Typography } from "@mui/material";
import React, { useState } from "react";

const maxReasonLength = 255;

interface Props {
    title: string;
    message: React.ReactNode;
    onClose: () => void;
    onDefer: (reason: string) => void;
    inProgress: boolean;
}

function PipelineDeferPipelineNodeDialog({ title, message, onClose, onDefer, inProgress }: Props) {
    const [deferReason, setDeferReason] = useState("");

    const onDialogClose = () => {
        setDeferReason("");
        onClose();
    }

    return (
        <Dialog open={true} maxWidth="sm" fullWidth>
            <DialogTitle>{title}</DialogTitle>
            <DialogContent>
                <Typography>
                    {message}
                </Typography>
                <TextField
                    fullWidth
                    label="Enter reason for deferring"
                    value={deferReason}
                    onChange={(e) => setDeferReason(e.target.value)}
                    margin="normal"
                    inputProps={{ maxLength: maxReasonLength }}
                    helperText={`${maxReasonLength - deferReason.length} characters remaining`}
                />
            </DialogContent>
            <DialogActions>
                <Button
                    color="inherit"
                    variant="text"
                    onClick={onDialogClose}>
                    Cancel
                </Button>
                <LoadingButton
                    variant="text"
                    onClick={() => onDefer(deferReason)}
                    disabled={deferReason.length === 0}
                    loading={inProgress}>
                    Defer
                </LoadingButton>
            </DialogActions>
        </Dialog>
    )
}

export default PipelineDeferPipelineNodeDialog;
