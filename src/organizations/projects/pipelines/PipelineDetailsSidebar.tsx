import ArrowBackIcon from '@mui/icons-material/ArrowBack';
import CloseIcon from '@mui/icons-material/Close';
import { IconButton, Paper, styled, Toolbar, Tooltip, Typography } from '@mui/material';
import Box from '@mui/material/Box';
import MuiDrawer, { DrawerProps } from '@mui/material/Drawer';
import graphql from 'babel-plugin-relay/macro';
import moment from 'moment';
import React from 'react';
import { useFragment } from 'react-relay/hooks';
import { Link as RouterLink, useNavigate } from 'react-router-dom';
import Gravatar from '../../../common/Gravatar';
import Link from '../../../routes/Link';
import { PipelineDetailsSidebarFragment_details$key } from './__generated__/PipelineDetailsSidebarFragment_details.graphql';
import { PIPELINE_TYPE_TEXT } from './PipelineDetailsIndex';
import PipelineStages from './PipelineStages';
import PipelineStatusChip from './PipelineStatusChip';
import { ReleaseIcon } from '../../../common/Icons';

interface Props {
    fragmentRef: PipelineDetailsSidebarFragment_details$key
    selectedNodePath?: string
    open: boolean
    temporary: boolean
    onClose: () => void
}

export const SidebarWidth = 400;

const Drawer = styled(MuiDrawer)<DrawerProps>(() => ({
    flexShrink: 0,
    overflowX: 'hidden',
    [`& .MuiDrawer-paper`]: {
        overflowX: 'hidden',
        width: SidebarWidth,
        boxSizing: 'border-box'
    },
    width: SidebarWidth,
}));

function PipelineDetailsSidebar(props: Props) {
    const { open, temporary, onClose, selectedNodePath } = props;
    const navigate = useNavigate();

    const data = useFragment<PipelineDetailsSidebarFragment_details$key>(
        graphql`
    fragment PipelineDetailsSidebarFragment_details on Pipeline
    {
        id
        status
        createdBy
        type
        environmentName
        environment {
            id
        }
        metadata {
            createdAt
        }
        pipelineTemplate {
            id
            name
            versioned
            semanticVersion
        }
        variables {
            category
        }
        parentPipeline {
            id
        }
        release {
            id
            semanticVersion
        }
        ...PipelineStagesFragment_stages
    }

  `, props.fragmentRef);

    const onTaskSelected = (taskPath: string) => {
        navigate(`task/${encodeURIComponent(taskPath)}`, { replace: true });
    };

    const onNestedPipelineSelected = (pipelinePath: string) => {
        navigate(`nested_pipeline/${encodeURIComponent(pipelinePath)}`, { replace: true });
    };

    const { name, semanticVersion, id, versioned } = data.pipelineTemplate;
    const pipelineTemplateLinkText = versioned
        ? name && name.length > 20 ? `${name}:${semanticVersion}`.substring(0, 20) + '...' : `${name}:${semanticVersion}`
        : `${id.substring(0, 8)}...`;

    return (
        <Drawer
            variant={temporary ? 'temporary' : 'permanent'}
            open={open}
            hideBackdrop={false}
            anchor='left'
            onClose={onClose}
        >
            <Toolbar />
            <Box padding={2}>
                <Box marginBottom={2}>
                    <Box marginBottom={2} display="flex" justifyContent="space-between">
                        <Link display="flex" alignItems="center" color="textSecondary" variant="body2" component={RouterLink} to={`../${data.id}`}>
                            <ArrowBackIcon sx={{ fontSize: '1.25rem' }} /> Pipeline Details
                        </Link>
                        {temporary && <IconButton
                            color="inherit"
                            size="small"
                            onClick={() => onClose()}
                        >
                            <CloseIcon />
                        </IconButton>}
                    </Box>
                    <Box display="flex" alignItems="center" marginBottom={1}>
                        <Typography color="textPrimary" mr={1}>Pipeline</Typography>
                        <PipelineStatusChip to={`../${data.id}`} status={data.status} />
                    </Box>
                    <Box display="flex" alignItems="center" marginBottom={1}>
                        <Tooltip title={data.metadata.createdAt as string}>
                            <Typography variant="caption" sx={{ marginRight: 1 }}>
                                Created {moment(data.metadata.createdAt as moment.MomentInput).fromNow()} by
                            </Typography>
                        </Tooltip>
                        <Tooltip title={data.createdBy}>
                            <Box>
                                <Gravatar width={20} height={20} email={data.createdBy} />
                            </Box>
                        </Tooltip>
                    </Box>
                    {data.parentPipeline && <Typography variant="body2" color="textSecondary" component="div">
                        Parent pipeline is
                        <Link to={`../${data.parentPipeline.id}`} variant="body2" sx={{ marginLeft: '4px' }}>
                            {`${data.parentPipeline.id.substring(0, 8)}...`}
                        </Link>
                    </Typography>}
                </Box>
                {data.release && <Paper variant="outlined" sx={{ display: 'flex', alignItems: 'center', p: 1, mb: 2 }}>
                    <ReleaseIcon />
                    <Typography component="div" variant="body2" sx={{ ml: 1 }}>
                        Release
                        {' '}
                        <Link to={`../../-/releases/${data.release.id}`} sx={{ fontWeight: 600 }}>
                            v{data.release.semanticVersion}
                        </Link>
                    </Typography>
                </Paper>}
                <Paper variant="outlined" sx={{ padding: 1, mb: 2 }}>
                    <Typography component="div" variant="body2">
                        {PIPELINE_TYPE_TEXT[data.type]} pipeline created with pipeline template
                        <Link to={`../../-/pipeline_templates/${data.pipelineTemplate.id}`} variant="body2" sx={{ margin: '0 4px', fontWeight: 600 }}>
                            {pipelineTemplateLinkText}
                        </Link>
                        {data.type === 'DEPLOYMENT' && <React.Fragment>
                            {' '}and deployed to environment
                            {
                                data.environment ?
                                    <Link
                                        sx={{ ml: '4px', fontWeight: 600 }}
                                        to={`../../-/environments/${data.environment.id}`}
                                        variant="body2"
                                    >
                                        {data.environmentName}
                                    </Link>
                                    :
                                    <Typography
                                        sx={{ ml: '4px', fontWeight: 600 }}
                                        component="span"
                                        variant="body2">
                                        {data.environmentName}
                                    </Typography>
                            }
                        </React.Fragment>}
                    </Typography>
                </Paper>
                <Box>
                    <Typography sx={{ marginBottom: 2 }}>Stages</Typography>
                    <PipelineStages
                        fragmentRef={data}
                        selectedNodePath={selectedNodePath}
                        onTaskSelected={onTaskSelected}
                        onNestedPipelineSelected={onNestedPipelineSelected}
                    />
                </Box>
            </Box>
        </Drawer>
    );
}

export default PipelineDetailsSidebar;
