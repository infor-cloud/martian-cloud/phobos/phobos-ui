import { Chip, List, ListItemButton, Paper, Typography, useTheme } from '@mui/material';
import Box from '@mui/material/Box';
import ListItemIcon from '@mui/material/ListItemIcon';
import { alpha } from '@mui/material/styles';
import graphql from 'babel-plugin-relay/macro';
import { useFragment } from 'react-relay/hooks';
import PipelineStatusType from './PipelineStatusType';
import { PipelineStagesFragment_stages$key } from './__generated__/PipelineStagesFragment_stages.graphql';

interface PipelineNodeListItemProps {
    selected: boolean;
    type: 'task' | 'pipeline';
    node: { path: string, name: string, status: string, when: string, dependencies: readonly string[] };
    onSelected: () => void;
}

function PipelineNodeListItem({ node, type, selected, onSelected }: PipelineNodeListItemProps) {
    const theme = useTheme();
    const sx = {
        paddingTop: 1,
        paddingBottom: 1,
        justifyContent: 'space-between',
        borderBottom: `1px solid ${theme.palette.divider}`,
        '&:last-child': {
            borderBottom: 0,
        }
    };
    return (
        <ListItemButton sx={
            selected ? {
                ...sx,
                border: `1px solid ${alpha(theme.palette.primary.main, 0.5)}`,
                backgroundColor: alpha(theme.palette.primary.main, 0.1),
                '&:last-child': {
                    borderBottom: `1px solid ${alpha(theme.palette.primary.main, 0.5)}`,
                }
            } : sx}
            onClick={() => onSelected()}
        >
            <Box display="flex" alignItems="center" minWidth={0}>
                <ListItemIcon sx={{ minWidth: 40 }}>
                    {PipelineStatusType[node.status].icon}
                </ListItemIcon>
                <Box minWidth={0}>
                    <Typography sx={{ wordWrap: 'break-word' }} variant="body2" fontWeight={500}>{node.name}</Typography>
                    {node.dependencies.length > 0 && <Typography color="textSecondary" variant="caption">{node.dependencies.length} dependenc{node.dependencies.length === 1 ? 'y' : 'ies'}</Typography>}
                    <Box>
                        {node.status === 'APPROVAL_PENDING' && <Chip
                            sx={{ cursor: 'inherit', mt: 1 }}
                            variant="outlined"
                            size="xs"
                            color="warning"
                            label="requires approval"
                        />}
                        {node.status === 'READY' && node.when === 'manual' && <Chip
                            sx={{ cursor: 'inherit', mt: 1 }}
                            variant="outlined"
                            size="xs"
                            color="warning"
                            label="manual"
                        />}
                    </Box>
                </Box>
            </Box>
            <Box display="flex" alignItems="center" marginLeft={1}>
                <Chip sx={{ color: theme.palette.text.secondary }} size="xs" label={type === 'task' ? 'Task' : 'Pipeline'} variant="outlined" />
            </Box>
        </ListItemButton>
    );
}

interface Props {
    fragmentRef: PipelineStagesFragment_stages$key
    selectedNodePath?: string
    onTaskSelected: (path: string) => void
    onNestedPipelineSelected: (path: string) => void
}

function PipelineStages({ fragmentRef, selectedNodePath, onTaskSelected, onNestedPipelineSelected }: Props) {
    const theme = useTheme();

    const data = useFragment<PipelineStagesFragment_stages$key>(
        graphql`
    fragment PipelineStagesFragment_stages on Pipeline
    {
        stages {
            path
            name
            status
            tasks {
                path
                name
                status
                when
                dependencies
            }
            nestedPipelines {
                path
                name
                status
                when
                dependencies
            }
        }
    }

  `, fragmentRef);

    return (
        <Box>
            {data.stages.map(stage => (<Box key={stage.path}>
                <Box marginBottom={2}>
                    <Paper sx={{ padding: 1, borderBottomLeftRadius: 0, borderBottomRightRadius: 0, border: `1px solid ${theme.palette.divider}` }}>
                        <Typography variant="body1" fontWeight="500">{stage.name}</Typography>
                    </Paper>
                    <Box sx={{ border: `1px solid ${theme.palette.divider}`, borderTop: 0, borderBottomLeftRadius: 4, borderBottomRightRadius: 4 }}>
                        <List dense disablePadding>
                            {stage.tasks.map(task => (<PipelineNodeListItem
                                node={task}
                                type="task"
                                selected={selectedNodePath === task.path}
                                onSelected={() => onTaskSelected(task.path)}
                                key={task.path}
                            />))}
                            {stage.nestedPipelines.map(pipeline => (<PipelineNodeListItem
                                node={pipeline}
                                type="pipeline"
                                selected={selectedNodePath === pipeline.path}
                                onSelected={() => onNestedPipelineSelected(pipeline.path)}
                                key={pipeline.path}
                            />))}
                        </List>
                    </Box>
                </Box>
            </Box>))}
        </Box>
    );
}

export default PipelineStages;
