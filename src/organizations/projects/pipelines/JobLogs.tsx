import AutoScrollIcon from '@mui/icons-material/ArrowCircleDown';
import { Alert, Box, Button, LinearProgress, Paper, ToggleButton, Tooltip, Typography } from '@mui/material';
import graphql from 'babel-plugin-relay/macro';
import humanizeDuration from 'humanize-duration';
import moment from 'moment';
import React, { useEffect, useMemo, useState } from 'react';
import { useLazyLoadQuery, useRelayEnvironment, useSubscription } from 'react-relay/hooks';
import { GraphQLSubscriptionConfig, RecordSourceProxy, fetchQuery } from 'relay-runtime';
import Timestamp from '../../../common/Timestamp';
import { JobLogsOnlyQuery } from './__generated__/JobLogsOnlyQuery.graphql';
import { JobLogsQuery } from './__generated__/JobLogsQuery.graphql';
import { JobLogsSubscription, JobLogsSubscription$data } from './__generated__/JobLogsSubscription.graphql';
import JobStatusChip from './JobStatusChip';
import LogViewer from './LogViewer';

const query = graphql`
    query JobLogsQuery($id: String!, $startOffset: Int!, $limit: Int!) {
        node(id: $id) {
            ...on Job {
                metadata {
                    createdAt
                }
                timestamps {
                    queuedAt
                    pendingAt
                    runningAt
                    finishedAt
                }
                id
                status
                cancelRequestedAt
                forceCanceled
                forceCancelAvailableAt
                logLastUpdatedAt
                logSize
                logs(startOffset:$startOffset, limit:$limit)
            }
        }
    }
`;

const logsOnlyQuery = graphql`
    query JobLogsOnlyQuery($id: String!, $startOffset: Int!, $limit: Int!) {
        node(id: $id) {
            ...on Job {
                id
                logSize
                logs(startOffset:$startOffset, limit:$limit)
            }
        }
    }
`;

const subscription = graphql`subscription JobLogsSubscription($input: JobLogStreamSubscriptionInput!) {
    jobLogStreamEvents(input: $input) {
      size
    }
  }`;

interface Props {
    jobId: string
    latestJobId: string | undefined
    onCancelJob: (force: boolean) => void
}

const bytes = (str: string) => {
    const size = new Blob([str]).size;
    return size;
}

const LOG_CHUNK_SIZE_BYTES = 1024 * 1024;
const FINAL_JOB_STATES = ['succeeded', 'failed', 'canceled'];

function JobLogs(props: Props) {
    const queryData = useLazyLoadQuery<JobLogsQuery>(query, { id: props.jobId, startOffset: 0, limit: 51200 }, { fetchPolicy: 'store-and-network' });

    const job = queryData.node as any;

    const [logs, setLogs] = useState(job.logs);
    const [currentLogSize, setCurrentLogSize] = useState(bytes(job.logs));
    const [actualLogSize, setActualLogSize] = useState(job.logSize);
    const [lastLogEventSize, setLastLogEventSize] = useState(job.logSize);
    const [loading, setLoading] = useState<boolean>(false);
    const [autoScroll, setAutoScroll] = useState(!FINAL_JOB_STATES.includes(job.status));
    const environment = useRelayEnvironment();

    const config = useMemo<GraphQLSubscriptionConfig<JobLogsSubscription>>(() => ({
        variables: { input: { jobId: job.id, lastSeenLogSize: job.logSize } },
        subscription,
        onCompleted: () => console.log("Subscription completed"),
        onError: () => console.warn("Subscription error"),
        updater: (store: RecordSourceProxy, payload: JobLogsSubscription$data) => {
            setLastLogEventSize(payload.jobLogStreamEvents.size)
        }
    }), [job.id]);
    useSubscription<JobLogsSubscription>(config);

    useEffect(() => {
        setLogs(job.logs);
        setCurrentLogSize(bytes(job.logs));
        setActualLogSize(job.logSize);
        setLastLogEventSize(job.logSize);
        setLoading(false);
    }, [job.id]);

    useEffect(() => {
        if (lastLogEventSize > actualLogSize) {
            setActualLogSize(lastLogEventSize);
        }
    }, [lastLogEventSize, actualLogSize]);

    useEffect(() => {
        if (loading || currentLogSize >= actualLogSize) {
            return;
        }

        setLoading(true);

        fetchQuery<JobLogsOnlyQuery>(
            environment,
            logsOnlyQuery,
            { id: job.id, startOffset: currentLogSize, limit: LOG_CHUNK_SIZE_BYTES },
            { fetchPolicy: 'network-only' }
        ).toPromise().then(async response => {
            const fetchedJob = response?.node as any;
            if (fetchedJob && fetchedJob.id === job.id) {
                setLoading(false);
                setLogs(logs + fetchedJob.logs);
                setActualLogSize(fetchedJob.logSize);
                setCurrentLogSize(prev => prev + bytes(fetchedJob.logs));
            }
        });
    }, [job.id, actualLogSize, currentLogSize, logs, loading, environment]);

    useEffect(() => {
        if (autoScroll) {
            scrollToBottom();
        }
    }, [logs, autoScroll]);

    const loadedPercent = useMemo(() => (currentLogSize / actualLogSize) * 100, [currentLogSize, actualLogSize]);

    const scrollToBottom = () => {
        window.scrollTo(0, document.body.scrollHeight);
    };

    const forceCancelAvailable = useMemo(() => {
        return job.forceCancelAvailableAt && moment().isAfter(job.forceCancelAvailableAt);
    }, [job.forceCancelAvailableAt]);

    const duration = useMemo(() => {
        const timestamps = job.timestamps;
        return timestamps?.finishedAt ?
            moment.duration(moment(timestamps.finishedAt as moment.MomentInput).diff(moment(timestamps.runningAt as moment.MomentInput))) : null;
    }, [job.timestamps]);

    return (
        <Box>
            {props.jobId !== props.latestJobId && <Alert color="warning" sx={{ mb: 2 }}>A more recent job is available</Alert>}
            <Paper variant="outlined" sx={{ padding: 2, marginBottom: 2, display: 'flex', alignItems: 'center', justifyContent: 'space-between' }}>
                <Box display="flex" alignItems="center">
                    <JobStatusChip status={job.status} />
                    <Typography sx={{ ml: 1 }} variant="body1">
                        Job <code>{job.id.substring(0, 8)}...</code>{' '}
                        {['queued', 'pending'].includes(job.status) && <React.Fragment>queued <Timestamp component="span" timestamp={job.timestamps.queuedAt} /></React.Fragment>}
                        {['succeeded', 'failed', 'canceled'].includes(job.status) && <React.Fragment>
                            {job.status} <Timestamp component="span" timestamp={job.timestamps.finishedAt} /> {duration && `with a duration of ${humanizeDuration(duration.asMilliseconds())}`}
                        </React.Fragment>}
                        {job.status === 'running' && <React.Fragment>started <Timestamp component="span" timestamp={job.timestamps.runningAt} /></React.Fragment>}
                        {job.status === 'canceling' && <React.Fragment>
                            graceful cancel started <Timestamp component="span" timestamp={job.cancelRequestedAt} />
                            {!forceCancelAvailable && <React.Fragment>, force cancel available <Timestamp component="span" timestamp={job.forceCancelAvailableAt} /></React.Fragment>}
                        </React.Fragment>}
                    </Typography>
                </Box>
                {job.status === 'running' && <Button size="small" variant="outlined" color="warning" onClick={() => props.onCancelJob(false)}>Cancel</Button>}
                {job.status === 'canceling' && forceCancelAvailable && <Button size="small" variant="outlined" color="warning" onClick={() => props.onCancelJob(true)}>Force Cancel</Button>}
            </Paper>
            <Paper square>
                <Box
                    display="flex"
                    alignItems="center"
                    justifyContent="space-between"
                    paddingLeft={2}
                    paddingRight={2}
                    paddingTop={1}
                    paddingBottom={1}
                >
                    {job.logLastUpdatedAt && <Typography color="textSecondary">
                        last updated {moment(job.logLastUpdatedAt as moment.MomentInput).fromNow()}
                    </Typography>}
                    <Tooltip title={autoScroll ? 'Disable auto scroll' : 'Enable auto scroll'}>
                        <ToggleButton
                            size="small"
                            value="check"
                            selected={autoScroll}
                            onChange={() => setAutoScroll(!autoScroll)}
                        >
                            <AutoScrollIcon />
                        </ToggleButton>
                    </Tooltip>
                </Box>
            </Paper>
            {loadedPercent < 100 && FINAL_JOB_STATES.includes(job.status) && <LinearProgress variant="determinate" value={loadedPercent} />}
            <LogViewer logs={logs} />
        </Box>
    );
}

export default JobLogs;
