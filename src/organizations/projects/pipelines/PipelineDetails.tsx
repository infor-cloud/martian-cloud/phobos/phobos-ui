import DoubleArrowIcon from '@mui/icons-material/DoubleArrow';
import { Box, IconButton, useMediaQuery, useTheme } from "@mui/material";
import graphql from 'babel-plugin-relay/macro';
import { useMemo, useState } from "react";
import { useFragment, useLazyLoadQuery, useSubscription } from "react-relay/hooks";
import { Outlet, useLocation, useOutletContext, useParams } from "react-router-dom";
import { GraphQLSubscriptionConfig } from "relay-runtime";
import ProjectBreadcrumbs from "../ProjectBreadcrumbs";
import PipelineDetailsSidebar, { SidebarWidth } from "./PipelineDetailsSidebar";
import { PipelineDetailsFragment_details$key } from "./__generated__/PipelineDetailsFragment_details.graphql";
import { PipelineDetailsFragment_pipeline$key } from "./__generated__/PipelineDetailsFragment_pipeline.graphql";
import { PipelineDetailsJobEventsSubscription } from "./__generated__/PipelineDetailsJobEventsSubscription.graphql";
import { PipelineDetailsQuery } from "./__generated__/PipelineDetailsQuery.graphql";
import { PipelineDetailsSubscription } from "./__generated__/PipelineDetailsSubscription.graphql";

const query = graphql`
    query PipelineDetailsQuery($pipelineId: String!) {
        node(id: $pipelineId) {
            ...on Pipeline {
                ...PipelineDetailsFragment_pipeline
            }
        }
    }
`;

const pipelineSubscription = graphql`subscription PipelineDetailsSubscription($input: PipelineEventsSubscriptionInput!) {
    pipelineEvents(input: $input) {
      action
      pipeline {
        id
        ...PipelineDetailsFragment_pipeline
      }
    }
  }`;

const jobEventsSubscription = graphql`subscription PipelineDetailsJobEventsSubscription($input: JobSubscriptionInput!) {
    jobEvents(input: $input) {
      action
      job {
        id
        status
        ...PipelineTaskJobListItemFragment
      }
    }
  }`;

function PipelineDetails() {
    const context = useOutletContext<PipelineDetailsFragment_details$key>();
    const { pipelineId, nodePath } = useParams() as { pipelineId: string, nodePath?: string };
    const theme = useTheme();
    const mobile = useMediaQuery(theme.breakpoints.down('md'));
    const [sidebarOpen, setSidebarOpen] = useState(false);
    const location = useLocation();

    const data = useFragment<PipelineDetailsFragment_details$key>(
        graphql`
            fragment PipelineDetailsFragment_details on Project
            {
                metadata {
                    prn
                }
            }
        `, context);

    const queryData = useLazyLoadQuery<PipelineDetailsQuery>(query, { pipelineId }, { fetchPolicy: 'store-and-network' });

    const pipeline = useFragment<PipelineDetailsFragment_pipeline$key>(
        graphql`
            fragment PipelineDetailsFragment_pipeline on Pipeline
            {
                metadata {
                    version
                }
                id
                status
                stages {
                    path
                    name
                    status
                    tasks {
                        path
                    }
                }
                ...PipelineTaskDetailsFragment_details
                ...PipelineDetailsSidebarFragment_details
                ...PipelineDetailsIndexFragment_details
                ...PipelineNestedPipelineDetailsFragment_details
            }
        `, queryData.node);

    const pipelineSubscriptionConfig = useMemo<GraphQLSubscriptionConfig<PipelineDetailsSubscription>>(() => ({
        variables: { input: { pipelineId, lastSeenVersion: pipeline?.metadata.version } },
        subscription: pipelineSubscription,
        onCompleted: () => console.log("Subscription completed"),
        onError: () => console.warn("Subscription error"),
    }), [pipelineId]);

    useSubscription<PipelineDetailsSubscription>(pipelineSubscriptionConfig);

    const jobSubscriptionConfig = useMemo<GraphQLSubscriptionConfig<PipelineDetailsJobEventsSubscription>>(() => ({
        variables: { input: { pipelineId } },
        subscription: jobEventsSubscription,
        onCompleted: () => console.log("Subscription completed"),
        onError: () => console.warn("Subscription error")
    }), [pipelineId]);

    useSubscription<PipelineDetailsJobEventsSubscription>(jobSubscriptionConfig);

    const onToggleSidebar = () => {
        setSidebarOpen(prev => !prev);
    };

    const displayFullPipeline = location.pathname.endsWith(`/${pipelineId}`);

    return pipeline ? (
        <Box>
            {!displayFullPipeline && <PipelineDetailsSidebar
                selectedNodePath={nodePath}
                fragmentRef={pipeline}
                open={sidebarOpen}
                temporary={mobile}
                onClose={onToggleSidebar}
            />}
            <Box paddingLeft={!mobile && !displayFullPipeline ? `${SidebarWidth}px` : 0}>
                {mobile && !displayFullPipeline && <Box display="flex" mb={1}>
                    <IconButton onClick={onToggleSidebar}><DoubleArrowIcon /></IconButton>
                </Box>}
                <ProjectBreadcrumbs
                    prn={data.metadata.prn}
                    childRoutes={[
                        { title: "pipelines", path: 'pipelines' },
                        { title: `${pipelineId.substring(0, 8)}...`, path: pipelineId }
                    ]}
                />
                <Outlet context={pipeline} />
            </Box>
        </Box>
    ) : null;
}

export default PipelineDetails;
