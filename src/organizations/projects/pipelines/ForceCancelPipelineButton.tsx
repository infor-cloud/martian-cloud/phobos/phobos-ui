import { useMemo, useState } from "react";
import { useFragment, useMutation } from "react-relay";
import graphql from "babel-plugin-relay/macro";
import { atomDark as prismTheme } from 'react-syntax-highlighter/dist/esm/styles/prism';
import { Alert, AlertTitle, Button, Dialog, DialogActions, DialogContent, DialogTitle, TextField, Typography } from "@mui/material";
import SyntaxHighlighter from "react-syntax-highlighter";
import { LoadingButton } from "@mui/lab";
import { useSnackbar } from "notistack";
import { ForceCancelPipelineButtonFragment_pipeline$key } from "./__generated__/ForceCancelPipelineButtonFragment_pipeline.graphql";
import { ForceCancelPipelineButtonDialogFragment_pipeline$key } from "./__generated__/ForceCancelPipelineButtonDialogFragment_pipeline.graphql";
import { ForceCancelPipelineButtonCancelPipelineMutation } from "./__generated__/ForceCancelPipelineButtonCancelPipelineMutation.graphql";

interface Props {
    fragmentRef: ForceCancelPipelineButtonFragment_pipeline$key;
}

interface ForceCancelPipelineDialogProps {
    fragmentRef: ForceCancelPipelineButtonDialogFragment_pipeline$key;
    open: boolean;
    closeDialog: () => void;
    onClose: (confirm?: boolean) => void;
    cancelInProgress: boolean;
}

function ForceCancelPipelineConfirmationDialog(props: ForceCancelPipelineDialogProps) {
    const { fragmentRef, open, closeDialog, onClose, cancelInProgress } = props;
    const [deleteInput, setDeleteInput] = useState<string>('');

    const data = useFragment<ForceCancelPipelineButtonDialogFragment_pipeline$key>(
        graphql`
        fragment ForceCancelPipelineButtonDialogFragment_pipeline on Pipeline {
            project {
                name
                organizationName
            }
        }
        `, fragmentRef
    )

    const projectPath = useMemo(() => data.project.organizationName + '/' + data.project.name, [data.project.organizationName, data.project.name]);

    return (
        <Dialog
            maxWidth="sm"
            open={open}
        >
            <DialogTitle>Force Cancel Pipeline</DialogTitle>
            <DialogContent>
                <Alert sx={{ mb: 2 }} severity="warning">
                    <AlertTitle>Warning</AlertTitle>
                    Force canceling this pipeline will <strong>immediately</strong> terminate any in-progress tasks and deployments. This could result in orphaned cloud resources, incomplete infrastructure changes, or partial deployments. Please ensure you understand the risks before proceeding.
                </Alert>
                <Typography variant="subtitle2">Enter the following to confirm force cancelling this pipeline:</Typography>
                <SyntaxHighlighter style={prismTheme} customStyle={{ fontSize: 14, marginBottom: 14 }} children={projectPath} />
                <TextField
                    autoComplete="off"
                    fullWidth
                    size="small"
                    placeholder={projectPath}
                    value={deleteInput}
                    onChange={(event: any) => setDeleteInput(event.target.value)}
                ></TextField>
            </DialogContent>
            <DialogActions>
                <Button color="inherit"
                    onClick={() => {
                        closeDialog()
                        setDeleteInput('')
                    }}>Cancel
                </Button>
                <LoadingButton
                    color="error"
                    variant="outlined"
                    loading={cancelInProgress}
                    disabled={projectPath !== deleteInput}
                    onClick={() => {
                        onClose(true)
                        setDeleteInput('')
                    }}
                >Force Cancel</LoadingButton>
            </DialogActions>
        </Dialog>
    );
}

function ForceCancelPipelineButton(props: Props) {
    const [openDialog, setOpenDialog] = useState(false);
    const { enqueueSnackbar } = useSnackbar();

    const data = useFragment<ForceCancelPipelineButtonFragment_pipeline$key>(
        graphql`
        fragment ForceCancelPipelineButtonFragment_pipeline on Pipeline {
            id
            ...ForceCancelPipelineButtonDialogFragment_pipeline
        }
    `, props.fragmentRef)

    const [commitForceCancelPipeline, commitForceCancelPipelineInFlight] = useMutation<ForceCancelPipelineButtonCancelPipelineMutation>(graphql`
        mutation ForceCancelPipelineButtonCancelPipelineMutation($input: CancelPipelineInput!) {
            cancelPipeline(input: $input) {
                problems {
                    message
                    field
                    type
                }
            }
        }
    `);

    const forceCancelPipeline = (confirm?: boolean) => {
        if (confirm) {
            commitForceCancelPipeline({
                variables: {
                    input: {
                        id: data.id,
                        force: true
                    },
                },
                onCompleted: data => {
                    setOpenDialog(false)

                    if (data.cancelPipeline.problems.length) {
                        enqueueSnackbar(data.cancelPipeline.problems.map(problem => problem.message).join('; '), { variant: 'warning' });
                    }
                },
                onError: error => {
                    setOpenDialog(false)
                    enqueueSnackbar(`An unexpected error occurred: ${error.message}`, { variant: 'error' });
                }
            })
        }
        else {
            setOpenDialog(false)
        }
    }

    return (
        <>
            <Button
                size="small"
                variant="outlined"
                color="warning"
                onClick={() => setOpenDialog(true)}
            >
                Force Cancel
            </Button>
            <ForceCancelPipelineConfirmationDialog
                fragmentRef={data}
                cancelInProgress={commitForceCancelPipelineInFlight}
                open={openDialog}
                closeDialog={() => setOpenDialog(false)}
                onClose={forceCancelPipeline}
            />
        </>
    );
}

export default ForceCancelPipelineButton;
