import { Box, TableCell } from '@mui/material';
import TableRow from '@mui/material/TableRow';
import graphql from 'babel-plugin-relay/macro';
import { LanConnect as ServiceAccountIcon } from 'mdi-material-ui';
import { useFragment } from 'react-relay/hooks';
import Gravatar from '../../../common/Gravatar';
import Timestamp from '../../../common/Timestamp';
import { PipelineApprovalListItemFragment_approval$key } from './__generated__/PipelineApprovalListItemFragment_approval.graphql';

interface Props {
    fragmentRef: PipelineApprovalListItemFragment_approval$key;
}

function PipelineApprovalListItem({ fragmentRef }: Props) {
    const data = useFragment<PipelineApprovalListItemFragment_approval$key>(
        graphql`
        fragment PipelineApprovalListItemFragment_approval on PipelineApproval
        {
            metadata {
                createdAt
            }
            id
            approver {
                __typename
                ...on User {
                    username
                    email
                }
                ...on ServiceAccount {
                    name
                }
            }
            approvalRules {
                name
            }
        }
      `, fragmentRef);

    const approverType = data.approver?.__typename;
    const principal = approverType === 'User' ? data.approver?.username : approverType === 'ServiceAccount' ? data.approver?.name : 'Unknown';

    return (
        <TableRow
            sx={{ '&:last-child td, &:last-child th': { border: 0 } }}
        >
            <TableCell sx={{ wordBreak: 'break-all' }}>
                {data.approvalRules.map(approvalRule => approvalRule.name).join(', ')}
            </TableCell>
            <TableCell sx={{ wordBreak: 'break-all' }}>
                <Box display="flex" alignItems="center">
                    {approverType === 'User' && <Gravatar width={24} height={24} email={data.approver?.email as string} />}
                    {approverType === 'ServiceAccount' && <ServiceAccountIcon fontSize="small" />}
                    <Box marginLeft={1}>{principal}</Box>
                </Box>
            </TableCell>
            <TableCell sx={{ wordBreak: 'break-all' }}>
                <Timestamp component="span" format="absolute" timestamp={data.metadata.createdAt} />
            </TableCell>
        </TableRow>
    );
}

export default PipelineApprovalListItem;
