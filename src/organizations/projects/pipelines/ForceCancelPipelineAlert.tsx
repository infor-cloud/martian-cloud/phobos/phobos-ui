import { Alert, Stack, Typography, SxProps, Theme } from "@mui/material";
import graphql from "babel-plugin-relay/macro";
import moment from "moment";
import { useFragment } from "react-relay"
import ForceCancelPipelineButton from "./ForceCancelPipelineButton";
import { ForceCancelPipelineAlertFragment_pipeline$key } from './__generated__/ForceCancelPipelineAlertFragment_pipeline.graphql'
import Timestamp from "../../../common/Timestamp";

interface Props {
    fragmentRef: ForceCancelPipelineAlertFragment_pipeline$key;
    sx?: SxProps<Theme>;
}

function ForceCancelPipelineAlert(props: Props) {
    const data = useFragment<ForceCancelPipelineAlertFragment_pipeline$key>(
        graphql`
        fragment ForceCancelPipelineAlertFragment_pipeline on Pipeline {
            forceCancelAvailableAt
            ...ForceCancelPipelineButtonFragment_pipeline
        }
        `, props.fragmentRef
    )

    const forceCancelAvailable = moment(data.forceCancelAvailableAt as moment.MomentInput).isSameOrBefore();

    return (
        <Alert severity='warning' variant='outlined' sx={props.sx}>
            <Stack direction="column" spacing={1}>
                <Typography>Cancellation is in progress...</Typography>
                {!forceCancelAvailable && <Typography variant="caption">If the graceful cancellation fails, this pipeline can be force canceled <Timestamp format="relative" component="span" timestamp={data.forceCancelAvailableAt} />.</Typography>}
                {forceCancelAvailable && <ForceCancelPipelineButton fragmentRef={data} />}
            </Stack>
        </Alert>
    );
}

export default ForceCancelPipelineAlert;
