/**
 * @generated SignedSource<<31e0e87a890f66b075b49d9123d73bfd>>
 * @lightSyntaxTransform
 * @nogrep
 */

/* tslint:disable */
/* eslint-disable */
// @ts-nocheck

import { Fragment, ReaderFragment } from 'relay-runtime';
import { FragmentRefs } from "relay-runtime";
export type PipelineThreadListFragment_pipeline$data = {
  readonly id: string;
  readonly " $fragmentType": "PipelineThreadListFragment_pipeline";
};
export type PipelineThreadListFragment_pipeline$key = {
  readonly " $data"?: PipelineThreadListFragment_pipeline$data;
  readonly " $fragmentSpreads": FragmentRefs<"PipelineThreadListFragment_pipeline">;
};

const node: ReaderFragment = {
  "argumentDefinitions": [],
  "kind": "Fragment",
  "metadata": null,
  "name": "PipelineThreadListFragment_pipeline",
  "selections": [
    {
      "alias": null,
      "args": null,
      "kind": "ScalarField",
      "name": "id",
      "storageKey": null
    }
  ],
  "type": "Pipeline",
  "abstractKey": null
};

(node as any).hash = "6ba0b50e9957bdf3ad96338ea6a3ea14";

export default node;
