/**
 * @generated SignedSource<<3acbfba19c4cfbac73cd464c2def5c82>>
 * @lightSyntaxTransform
 * @nogrep
 */

/* tslint:disable */
/* eslint-disable */
// @ts-nocheck

import { ConcreteRequest, Mutation } from 'relay-runtime';
export type ProblemType = "BAD_REQUEST" | "CONFLICT" | "FORBIDDEN" | "NOT_FOUND" | "%future added value";
export type DeletePipelineCommentInput = {
  clientMutationId?: string | null | undefined;
  id: string;
  metadata?: ResourceMetadataInput | null | undefined;
};
export type ResourceMetadataInput = {
  version: string;
};
export type PipelineCommentDetails_DeleteMutation$variables = {
  input: DeletePipelineCommentInput;
};
export type PipelineCommentDetails_DeleteMutation$data = {
  readonly deletePipelineComment: {
    readonly problems: ReadonlyArray<{
      readonly field: ReadonlyArray<string> | null | undefined;
      readonly message: string;
      readonly type: ProblemType;
    }>;
  };
};
export type PipelineCommentDetails_DeleteMutation = {
  response: PipelineCommentDetails_DeleteMutation$data;
  variables: PipelineCommentDetails_DeleteMutation$variables;
};

const node: ConcreteRequest = (function(){
var v0 = [
  {
    "defaultValue": null,
    "kind": "LocalArgument",
    "name": "input"
  }
],
v1 = [
  {
    "alias": null,
    "args": [
      {
        "kind": "Variable",
        "name": "input",
        "variableName": "input"
      }
    ],
    "concreteType": "DeletePipelineCommentPayload",
    "kind": "LinkedField",
    "name": "deletePipelineComment",
    "plural": false,
    "selections": [
      {
        "alias": null,
        "args": null,
        "concreteType": "Problem",
        "kind": "LinkedField",
        "name": "problems",
        "plural": true,
        "selections": [
          {
            "alias": null,
            "args": null,
            "kind": "ScalarField",
            "name": "message",
            "storageKey": null
          },
          {
            "alias": null,
            "args": null,
            "kind": "ScalarField",
            "name": "field",
            "storageKey": null
          },
          {
            "alias": null,
            "args": null,
            "kind": "ScalarField",
            "name": "type",
            "storageKey": null
          }
        ],
        "storageKey": null
      }
    ],
    "storageKey": null
  }
];
return {
  "fragment": {
    "argumentDefinitions": (v0/*: any*/),
    "kind": "Fragment",
    "metadata": null,
    "name": "PipelineCommentDetails_DeleteMutation",
    "selections": (v1/*: any*/),
    "type": "Mutation",
    "abstractKey": null
  },
  "kind": "Request",
  "operation": {
    "argumentDefinitions": (v0/*: any*/),
    "kind": "Operation",
    "name": "PipelineCommentDetails_DeleteMutation",
    "selections": (v1/*: any*/)
  },
  "params": {
    "cacheID": "3c3de1a370faf60a554c666f33b0463d",
    "id": null,
    "metadata": {},
    "name": "PipelineCommentDetails_DeleteMutation",
    "operationKind": "mutation",
    "text": "mutation PipelineCommentDetails_DeleteMutation(\n  $input: DeletePipelineCommentInput!\n) {\n  deletePipelineComment(input: $input) {\n    problems {\n      message\n      field\n      type\n    }\n  }\n}\n"
  }
};
})();

(node as any).hash = "404af8f3dfc7d4d9350dfc68c28834f8";

export default node;
