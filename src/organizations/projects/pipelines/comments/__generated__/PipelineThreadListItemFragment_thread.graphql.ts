/**
 * @generated SignedSource<<a5a4a27d16b3c4b3cb4f824f7a0b2ced>>
 * @lightSyntaxTransform
 * @nogrep
 */

/* tslint:disable */
/* eslint-disable */
// @ts-nocheck

import { Fragment, ReaderFragment } from 'relay-runtime';
import { FragmentRefs } from "relay-runtime";
export type PipelineThreadListItemFragment_thread$data = {
  readonly comments: ReadonlyArray<{
    readonly id: string;
    readonly " $fragmentSpreads": FragmentRefs<"PipelineCommentDetailsFragment_comment">;
  }>;
  readonly id: string;
  readonly pipeline: {
    readonly id: string;
  } | null | undefined;
  readonly " $fragmentType": "PipelineThreadListItemFragment_thread";
};
export type PipelineThreadListItemFragment_thread$key = {
  readonly " $data"?: PipelineThreadListItemFragment_thread$data;
  readonly " $fragmentSpreads": FragmentRefs<"PipelineThreadListItemFragment_thread">;
};

const node: ReaderFragment = (function(){
var v0 = {
  "alias": null,
  "args": null,
  "kind": "ScalarField",
  "name": "id",
  "storageKey": null
};
return {
  "argumentDefinitions": [],
  "kind": "Fragment",
  "metadata": null,
  "name": "PipelineThreadListItemFragment_thread",
  "selections": [
    (v0/*: any*/),
    {
      "alias": null,
      "args": null,
      "concreteType": "Pipeline",
      "kind": "LinkedField",
      "name": "pipeline",
      "plural": false,
      "selections": [
        (v0/*: any*/)
      ],
      "storageKey": null
    },
    {
      "alias": null,
      "args": null,
      "concreteType": "Comment",
      "kind": "LinkedField",
      "name": "comments",
      "plural": true,
      "selections": [
        (v0/*: any*/),
        {
          "args": null,
          "kind": "FragmentSpread",
          "name": "PipelineCommentDetailsFragment_comment"
        }
      ],
      "storageKey": null
    }
  ],
  "type": "Thread",
  "abstractKey": null
};
})();

(node as any).hash = "bae4c78c5f3755eef95d41739b3a7c0b";

export default node;
