/**
 * @generated SignedSource<<7f143119129d8c347060948b1871d5a8>>
 * @lightSyntaxTransform
 * @nogrep
 */

/* tslint:disable */
/* eslint-disable */
// @ts-nocheck

import { Fragment, ReaderFragment } from 'relay-runtime';
import { FragmentRefs } from "relay-runtime";
export type PipelineCommentDetailsFragment_comment$data = {
  readonly id: string;
  readonly thread: {
    readonly id: string;
  };
  readonly " $fragmentSpreads": FragmentRefs<"CommentDetailsFragment_comment">;
  readonly " $fragmentType": "PipelineCommentDetailsFragment_comment";
};
export type PipelineCommentDetailsFragment_comment$key = {
  readonly " $data"?: PipelineCommentDetailsFragment_comment$data;
  readonly " $fragmentSpreads": FragmentRefs<"PipelineCommentDetailsFragment_comment">;
};

const node: ReaderFragment = (function(){
var v0 = {
  "alias": null,
  "args": null,
  "kind": "ScalarField",
  "name": "id",
  "storageKey": null
};
return {
  "argumentDefinitions": [],
  "kind": "Fragment",
  "metadata": null,
  "name": "PipelineCommentDetailsFragment_comment",
  "selections": [
    (v0/*: any*/),
    {
      "alias": null,
      "args": null,
      "concreteType": "Thread",
      "kind": "LinkedField",
      "name": "thread",
      "plural": false,
      "selections": [
        (v0/*: any*/)
      ],
      "storageKey": null
    },
    {
      "args": null,
      "kind": "FragmentSpread",
      "name": "CommentDetailsFragment_comment"
    }
  ],
  "type": "Comment",
  "abstractKey": null
};
})();

(node as any).hash = "bbe8511a9fcdc5bd5ea8fdfdb1e13472";

export default node;
