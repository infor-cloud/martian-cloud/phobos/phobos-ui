import graphql from 'babel-plugin-relay/macro';
import { enqueueSnackbar } from 'notistack';
import { useState } from 'react';
import { useFragment, useMutation } from 'react-relay/hooks';
import { RecordSourceProxy } from 'relay-runtime';
import CommentDetails from '../../comments/CommentDetails';
import { PipelineCommentDetailsFragment_comment$key } from './__generated__/PipelineCommentDetailsFragment_comment.graphql';
import { PipelineCommentDetails_DeleteMutation } from './__generated__/PipelineCommentDetails_DeleteMutation.graphql';
import { PipelineCommentDetails_EditMutation } from './__generated__/PipelineCommentDetails_EditMutation.graphql';

interface Props {
    fragmentRef: PipelineCommentDetailsFragment_comment$key
    showCollapse?: boolean
    onToggleReplyInput?: () => void
}

function PipelineCommentDetails({ fragmentRef, showCollapse, onToggleReplyInput }: Props) {
    const [showEditForm, setShowEditForm] = useState<boolean>(false);
    const [showDeleteDialog, setShowDeleteDialog] = useState<boolean>(false);

    const data = useFragment<PipelineCommentDetailsFragment_comment$key>(
        graphql`
            fragment PipelineCommentDetailsFragment_comment on Comment {
                id
                thread {
                    id
                }
                ...CommentDetailsFragment_comment
            }
        `, fragmentRef
    );

    const [commitEdit, editIsInFlight] = useMutation<PipelineCommentDetails_EditMutation>(graphql`
        mutation PipelineCommentDetails_EditMutation($input: UpdatePipelineCommentInput!) {
            updatePipelineComment(input: $input) {
                comment {
                    id
                    thread {
                        ...PipelineThreadListItemFragment_thread
                    }
                }
                problems {
                    message
                    field
                    type
                }
            }
        }
    `);

    const onEdit = (text: string) => {
        commitEdit({
            variables: {
                input: {
                    id: data.id,
                    text: text
                }
            },
            onCompleted: (data) => {
                if (data.updatePipelineComment.problems.length) {
                    enqueueSnackbar(data.updatePipelineComment.problems.map((problem: { message: any; }) =>
                        problem.message).join(', '), { variant: 'warning' });
                } else if (!data.updatePipelineComment.comment) {
                    enqueueSnackbar("Unexpected error occurred", { variant: 'error' });
                } else {
                    setShowEditForm(false);
                }
            },
            onError: (error) => {
                enqueueSnackbar(error.message, { variant: 'error' });
            }
        });
    };

    const [commitDelete, deleteIsInFlight] = useMutation<PipelineCommentDetails_DeleteMutation>(graphql`
        mutation PipelineCommentDetails_DeleteMutation($input: DeletePipelineCommentInput!) {
            deletePipelineComment(input: $input) {
                problems {
                    message
                    field
                    type
                }
            }
        }
    `);

    const onDelete = (commentId: string) => {
        commitDelete({
            variables: {
                input: {
                    id: commentId
                }
            },
            onCompleted: (data) => {
                if (data.deletePipelineComment.problems.length) {
                    enqueueSnackbar(data.deletePipelineComment.problems.map((problem: { message: any; }) => problem.message).join('; '), { variant: 'warning' });
                } else {
                    setShowDeleteDialog(false);
                }
            },
            updater: (store: RecordSourceProxy) => {
                const record = store.get(data.thread.id);
                if (record == null) {
                    return;
                }

                const commentRecords = record.getLinkedRecords("comments");
                if (commentRecords == null) {
                    return;
                }

                if (commentRecords.length === 1 && commentRecords[0].getValue("id") === commentId) {
                    // Delete record if it's the only comment in the thread
                    store.delete(record.getDataID())
                } else {
                    // Delete comment from the thread
                    const comments = commentRecords.filter((comment) => comment.getValue("id") !== commentId);
                    record.setLinkedRecords(comments, "comments");
                }
            },
            onError: (error) => {
                enqueueSnackbar(`Unexpected error occurred: ${error.message}`, { variant: 'error' });
            }
        });
    };

    return (
        <CommentDetails
            title='Delete comment'
            message='Are you sure you want to delete this comment?'
            showDeleteDialog={showDeleteDialog}
            opInProgress={editIsInFlight || deleteIsInFlight}
            showEditForm={showEditForm}
            fragmentRef={data}
            showCollapse={showCollapse}
            onToggleReplyInput={onToggleReplyInput}
            onEdit={onEdit}
            onOpenDeleteDialog={() => setShowDeleteDialog(true)}
            onCloseDeleteDialog={() => setShowDeleteDialog(false)}
            onShowEditForm={(show: boolean) => setShowEditForm(show)}
            onDelete={(commentId: string) => onDelete(commentId)}
        />
    );
}

export default PipelineCommentDetails
