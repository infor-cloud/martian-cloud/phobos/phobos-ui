import { Box, List } from '@mui/material';
import graphql from 'babel-plugin-relay/macro';
import { enqueueSnackbar } from 'notistack';
import { useMemo, useState } from 'react';
import InfiniteScroll from 'react-infinite-scroll-component';
import { ConnectionHandler, useFragment, useLazyLoadQuery, useMutation, usePaginationFragment, useSubscription } from 'react-relay/hooks';
import { GraphQLSubscriptionConfig, RecordSourceProxy } from 'relay-runtime';
import ListSkeleton from '../../../../skeletons/ListSkeleton';
import CommentOptionsBar, { SortType } from '../../comments/CommentOptionsBar';
import NewComment, { addThreadToConnection } from '../../comments/NewComment';
import PipelineThreadListItem from './PipelineThreadListItem';
import { PipelineThreadListFragmentPaginationQuery } from './__generated__/PipelineThreadListFragmentPaginationQuery.graphql';
import { PipelineThreadListFragment_comments$key } from './__generated__/PipelineThreadListFragment_comments.graphql';
import { PipelineThreadListFragment_pipeline$key } from './__generated__/PipelineThreadListFragment_pipeline.graphql';
import { PipelineThreadListQuery } from './__generated__/PipelineThreadListQuery.graphql';
import { PipelineThreadListSubscription, PipelineThreadListSubscription$data } from './__generated__/PipelineThreadListSubscription.graphql';
import { PipelineThreadList_CreateMutation } from './__generated__/PipelineThreadList_CreateMutation.graphql';

const query = graphql`
    query PipelineThreadListQuery($id: String!, $first: Int!, $after: String, $sort: ThreadSort!) {
        node(id: $id) {
            ... on Pipeline {
                ...PipelineThreadListFragment_comments
            }
        }
    }`;

const pipelineCommentSubscription = graphql`
    subscription PipelineThreadListSubscription($input: CommentEventsSubscriptionInput!) {
        commentEvents(input: $input) {
            action
            comment {
                id
                thread {
                    id
                    pipeline {
                        id
                    }
                    ...PipelineThreadListItemFragment_thread
                }
            }
        }
    }`;

interface Props {
    fragmentRef: PipelineThreadListFragment_pipeline$key
}

function PipelineThreadList({ fragmentRef }: Props) {
    const [sortOption, setSortOption] = useState<SortType>('CREATED_AT_ASC');
    const [tab, setTab] = useState<'write' | 'preview'>('write');
    const [formData, setFormData] = useState<string>('');

    const pipeline = useFragment<PipelineThreadListFragment_pipeline$key>(graphql`
        fragment PipelineThreadListFragment_pipeline on Pipeline
        {
            id
        }
    `, fragmentRef);

    const queryData = useLazyLoadQuery<PipelineThreadListQuery>(query,
        { id: pipeline.id, first: 20, sort: sortOption }, { fetchPolicy: 'store-and-network' });

    const { data, loadNext, hasNext } =
        usePaginationFragment<PipelineThreadListFragmentPaginationQuery, PipelineThreadListFragment_comments$key>(
            graphql`
            fragment PipelineThreadListFragment_comments on Pipeline
            @refetchable(queryName: "PipelineThreadListFragmentPaginationQuery") {
                id
                threads(
                    first: $first
                    after: $after
                    sort: $sort
                    ) @connection(key: "PipelineThreadListFragment_threads") {
                    edges {
                        node {
                            id
                            ...PipelineThreadListItemFragment_thread
                        }
                    }
                }
            }
        `, queryData.node
        );

    const pipelineCommentSubscriptionConfig = useMemo<GraphQLSubscriptionConfig<PipelineThreadListSubscription>>(() => ({
        subscription: pipelineCommentSubscription,
        variables: { input: { pipelineId: pipeline.id } },
        onCompleted: () => console.log('Subscription completed'),
        onError: () => console.warn('Subscription error'),
        updater: (store: RecordSourceProxy, payload: PipelineThreadListSubscription$data) => {
            const record = store.get(payload.commentEvents.comment.thread.id);
            if (record == null) {
                return;
            }
            const connectionId = ConnectionHandler.getConnectionID(
                pipeline.id,
                'PipelineThreadListFragment_threads',
                { sort: sortOption }
            );
            addThreadToConnection(store, record, record.getDataID(), sortOption, connectionId);
        }
    }), [pipeline.id, sortOption]);

    useSubscription<PipelineThreadListSubscription>(pipelineCommentSubscriptionConfig);

    const [commitNew, newIsInFlight] = useMutation<PipelineThreadList_CreateMutation>(
        graphql`
        mutation PipelineThreadList_CreateMutation($input: CreatePipelineCommentInput!) {
            createPipelineComment(input: $input) {
                comment {
                    id
                    thread {
                        ...PipelineThreadListItemFragment_thread
                    }
                }
                problems {
                    message
                    field
                    type
                }
            }
        }
    `);

    const onSave = (text: string) => {
        commitNew({
            variables: {
                input: {
                    pipelineId: pipeline.id,
                    text
                },
            },
            onCompleted: (data: any) => {
                if (data.createPipelineComment.problems.length) {
                    enqueueSnackbar(data.createPipelineComment.problems.map((problem: any) => problem.message).join('; '), { variant: 'warning' });
                } else if (!data.createPipelineComment.comment) {
                    enqueueSnackbar("Unexpected error occurred", { variant: 'error' });
                } else {
                    setFormData('');
                    setTab('write');
                }
            },
            onError: (error: any) => {
                enqueueSnackbar(error.message, { variant: 'error' });
            }
        });
    };

    const comments = data?.threads?.edges ?? [];

    return (
        <Box>
            <Box sx={{ minWidth: 120 }}>
                {comments.length > 1 &&
                    <CommentOptionsBar
                        sortOption={sortOption}
                        onChange={() =>
                            setSortOption(sortOption === 'CREATED_AT_ASC' ? 'CREATED_AT_DESC' : 'CREATED_AT_ASC')}
                    />
                }
            </Box>
            <Box>
                {(sortOption === 'CREATED_AT_DESC' && comments.length !== 1) &&
                    <NewComment
                        tab={tab}
                        formData={formData}
                        isInFlight={newIsInFlight}
                        onSave={onSave}
                        onTabChange={(event: React.SyntheticEvent, newValue: 'write' | 'preview') => setTab(newValue)}
                        onChange={(data: string) => setFormData(data)}
                    />}
                <InfiniteScroll
                    dataLength={(comments.length) ?? 0}
                    next={() => loadNext(20)}
                    hasMore={hasNext}
                    loader={<ListSkeleton rowCount={3} />}
                >
                    <List disablePadding sx={{ mb: 2 }}>
                        {comments.map((edge) => edge?.node ? (
                            <PipelineThreadListItem
                                key={edge.node.id}
                                fragmentRef={edge.node}
                            />
                        ) : null)}
                    </List>
                </InfiniteScroll>
                {(sortOption === 'CREATED_AT_ASC' || comments.length === 1) &&
                    <NewComment
                        tab={tab}
                        formData={formData}
                        isInFlight={newIsInFlight}
                        onSave={onSave}
                        onTabChange={(event: React.SyntheticEvent, newValue: 'write' | 'preview') => setTab(newValue)}
                        onChange={(data: string) => setFormData(data)}
                    />}
            </Box>
        </Box>
    );
}

export default PipelineThreadList
