import { Box, Collapse, Divider, List, ListItem, Paper, TextField } from '@mui/material';
import graphql from 'babel-plugin-relay/macro';
import { enqueueSnackbar } from 'notistack';
import { useEffect, useState } from 'react';
import { useFragment, useMutation } from 'react-relay/hooks';
import NewReply from '../../comments/NewReply';
import { PipelineThreadListItemFragment_thread$key } from '../comments/__generated__/PipelineThreadListItemFragment_thread.graphql';
import { PipelineThreadListItem_CreateMutation } from '../comments/__generated__/PipelineThreadListItem_CreateMutation.graphql';
import PipelineCommentDetails from './PipelineCommentDetails';

interface Props {
    fragmentRef: PipelineThreadListItemFragment_thread$key
}

function PipelineThreadListItem({ fragmentRef }: Props) {
    const [showNewReply, setShowNewReply] = useState<boolean>(false);

    const data = useFragment<PipelineThreadListItemFragment_thread$key>(
        graphql`
            fragment PipelineThreadListItemFragment_thread on Thread {
                id
                pipeline {
                    id
                }
                comments {
                    id
                    ...PipelineCommentDetailsFragment_comment
                }
            }
        `, fragmentRef
    );

    const [showCollapse, setShowCollapse] = useState<boolean>(data.comments.length > 1);

    useEffect(() => {
        setShowCollapse(data.comments.length > 1);
    }, [data.comments.length]);

    const [commitNew, newIsInFlight] = useMutation<PipelineThreadListItem_CreateMutation>(
        graphql`
        mutation PipelineThreadListItem_CreateMutation($input: CreatePipelineCommentInput!) {
            createPipelineComment(input: $input) {
                comment {
                    id
                    thread {
                        ...PipelineThreadListItemFragment_thread
                    }
                }
                problems {
                    message
                    field
                    type
                }
            }
        }
    `);

    const onNewComment = (text: string) => {
        commitNew({
            variables: {
                input: {
                    pipelineId: data.pipeline?.id ?? '',
                    text,
                    threadId: data.id
                },
            },
            onCompleted: (data: any) => {
                if (data.createPipelineComment.problems.length) {
                    enqueueSnackbar(data.createPipelineComment.problems.map((problem: any) => problem.message).join('; '), { variant: 'warning' });
                } else {
                    setShowNewReply(false);
                }
            },
            onError: (error: any) => {
                enqueueSnackbar(error.message, { variant: 'error' });
            }
        });
    };

    return (
        <ListItem
            disableGutters
            sx={{ mb: 1, pt: 0 }}
        >
            <Paper sx={{ width: '100%', p: 2 }}>
                <PipelineCommentDetails
                    showCollapse={showCollapse}
                    fragmentRef={data.comments[0]}
                    onToggleReplyInput={() => setShowCollapse(!showCollapse)}
                />
                {data.comments.length === 1 && <Collapse
                    in={showCollapse}
                    timeout="auto"
                    unmountOnExit
                >
                    <Box mt={2}>
                        <NewReply
                            opInFlight={newIsInFlight}
                            onSave={onNewComment}
                            onCancel={() => setShowCollapse(false)}
                        />
                    </Box>
                </Collapse>}
                {data.comments.length > 1 && <Collapse
                    in={showCollapse}
                    timeout="auto"
                    unmountOnExit
                >
                    <Box sx={{ width: "100%", mt: 1 }}>
                        <Divider sx={{ mb: 2, ml: -2, mr: -2, opacity: 0.6 }} />
                        <List dense disablePadding>
                            {data.comments.slice(1).map((reply: any) => (
                                <ListItem disableGutters key={reply.id}>
                                    <PipelineCommentDetails fragmentRef={reply} />
                                </ListItem>
                            ))}
                        </List>
                        <Box>
                            {!showNewReply && <TextField
                                sx={{ width: "100%", mt: 2 }}
                                size="small"
                                placeholder='Reply to this conversation...'
                                onClick={() => setShowNewReply(true)}
                            />}
                            <Collapse
                                sx={{ mt: 2, width: "100%" }}
                                in={showNewReply}
                                timeout="auto"
                                unmountOnExit
                            >
                                <NewReply
                                    opInFlight={newIsInFlight}
                                    onSave={onNewComment}
                                    onCancel={() => setShowNewReply(false)}
                                />
                            </Collapse>
                        </Box>
                    </Box>
                </Collapse>}
            </Paper>
        </ListItem>
    );
}

export default PipelineThreadListItem
