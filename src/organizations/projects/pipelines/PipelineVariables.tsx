import { Box, Button, Paper, Stack, TextField, ToggleButton, ToggleButtonGroup, Typography, useTheme } from "@mui/material";
import Table from '@mui/material/Table';
import TableBody from '@mui/material/TableBody';
import TableCell from '@mui/material/TableCell';
import TableContainer from '@mui/material/TableContainer';
import TableHead from '@mui/material/TableHead';
import TableRow from '@mui/material/TableRow';
import { darken } from '@mui/material/styles';
import graphql from 'babel-plugin-relay/macro';
import React, { useState } from "react";
import { useFragment } from "react-relay/hooks";
import PipelineVariableListItem from "./PipelineVariableListItem";
import { PipelineVariablesFragment$key } from "./__generated__/PipelineVariablesFragment.graphql";

interface Props {
    fragmentRef: PipelineVariablesFragment$key
}

const variableSearchFilter = (search: string) => (variable: any) => {
    return variable.key.toLowerCase().includes(search) || variable.value.toLowerCase().includes(search);
};

function PipelineVariables({ fragmentRef }: Props) {
    const theme = useTheme();
    const [search, setSearch] = useState('');
    const [showValues, setShowValues] = useState(false);
    const [variableCategory, setVariableCategory] = useState('HCL');

    const pipeline = useFragment<PipelineVariablesFragment$key>(
        graphql`
            fragment PipelineVariablesFragment on Pipeline
            {
                id
                variables {
                    key
                    value
                    category
                    ...PipelineVariableListItemFragment_variable
                }
            }
        `, fragmentRef);

    const onSearchChange = (event: React.ChangeEvent<HTMLInputElement>) => {
        setSearch(event.target.value.toLowerCase());
    };

    const onVariableCategoryChange = (event: React.MouseEvent<HTMLElement>, newCategory: string) => {
        if (newCategory) {
            setVariableCategory(newCategory);
        }
    };

    const variables = pipeline.variables.filter((variable: any) => variable.category === variableCategory);
    const filteredVariables = search ? variables.filter(variableSearchFilter(search)) : variables;

    return (
        <Box>
            <Box sx={{
                marginBottom: 4,
                display: 'flex',
                flexDirection: 'row',
                justifyContent: 'space-between',
                [theme.breakpoints.down('lg')]: {
                    flexDirection: 'column',
                    alignItems: 'flex-start',
                    '& > *:not(:last-child)': {
                        marginBottom: 2
                    },
                }
            }}>
                <ToggleButtonGroup
                    size="small"
                    color="primary"
                    value={variableCategory}
                    exclusive
                    onChange={onVariableCategoryChange}
                    sx={{ height: '100%' }}
                >
                    <ToggleButton value="HCL" size="small">HCL</ToggleButton>
                    <ToggleButton value="ENVIRONMENT" size="small">Environment</ToggleButton>
                </ToggleButtonGroup>
                <Stack direction="row" spacing={2}>
                    <TextField
                        size="small"
                        margin='none'
                        placeholder="search for variables"
                        InputProps={{
                            sx: { background: darken(theme.palette.background.default, 0.5) }
                        }}
                        sx={{ width: 300 }}
                        onChange={onSearchChange}
                        autoComplete="off"
                    />
                    <Button
                        size="small"
                        color="info"
                        sx={{ height: '100%' }}
                        onClick={() => setShowValues(!showValues)}
                    >
                        {showValues ? 'Hide Values' : 'Show Values'}
                    </Button>
                </Stack>
            </Box>
            <Box>
                {(filteredVariables.length === 0 && search !== '') && <Typography sx={{ padding: 2, marginTop: 4 }} align="center" color="textSecondary">
                    No {variableCategory} variables matching search <strong>{search}</strong>
                </Typography>}
                {(filteredVariables.length === 0 && search === '') && <Paper variant="outlined" sx={{ marginTop: 4, display: 'flex', justifyContent: 'center' }}>
                    <Box padding={4} display="flex" flexDirection="column" justifyContent="center" alignItems="center">
                        <Typography color="textSecondary" align="center">
                            This task does not have any {variableCategory} variables
                        </Typography>
                    </Box>
                </Paper>}
                {filteredVariables.length > 0 && <TableContainer>
                    <Table sx={{ tableLayout: 'fixed' }}>
                        <TableHead>
                            <TableRow>
                                <TableCell>
                                    <Typography color="textSecondary">Key</Typography>
                                </TableCell>
                                <TableCell>
                                    <Typography color="textSecondary">Value</Typography>
                                </TableCell>
                            </TableRow>
                        </TableHead>
                        <TableBody>
                            {filteredVariables.map((o: any) => <PipelineVariableListItem
                                key={`${o.action}-${o.name}`}
                                fragmentRef={o}
                                showValues={showValues}
                            />)}
                        </TableBody>
                    </Table>
                </TableContainer>}
            </Box>
        </Box>
    );
}

export default PipelineVariables;
