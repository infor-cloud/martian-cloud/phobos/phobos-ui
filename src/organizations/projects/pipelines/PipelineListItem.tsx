import { Box, TableCell, TableRow, Tooltip, Typography } from "@mui/material";
import graphql from "babel-plugin-relay/macro";
import moment from 'moment';
import { useFragment } from "react-relay/hooks";
import Gravatar from "../../../common/Gravatar";
import Timestamp from "../../../common/Timestamp";
import Link from "../../../routes/Link";
import PipelineAnnotations from "./PipelineAnnotations";
import PipelineStageIcons from "./PipelineStageIcons";
import PipelineStatusChip from "./PipelineStatusChip";
import { PipelineListItem_pipeline$key } from "./__generated__/PipelineListItem_pipeline.graphql";

interface Props {
    fragmentRef: PipelineListItem_pipeline$key
}

function PipelineListItem({ fragmentRef }: Props) {

    const data = useFragment<PipelineListItem_pipeline$key>(graphql`
        fragment PipelineListItem_pipeline on Pipeline
        {
            id
            createdBy
            status
            type
            metadata {
                createdAt
            }
            release {
                semanticVersion
            }
            annotations {
                key
            }
            timestamps {
                startedAt
                completedAt
            }
            ...PipelineStageIcons_stages
            ...PipelineAnnotationsFragment_pipeline
        }
    `, fragmentRef);

    return (
        <TableRow
            sx={{ '&:last-child td, &:last-child th': { border: 0 } }}
        >
            <TableCell>
                <PipelineStatusChip to={data.id} status={data.status} />
                {(data.timestamps.completedAt || data.timestamps.startedAt) && (
                    <Box marginTop={"4px"}>
                        {!data.timestamps.completedAt && (
                            <Box>
                                {data.timestamps.startedAt && (
                                    <Typography variant="caption" color="textSecondary">
                                        started{" "}
                                        <Timestamp component="span" timestamp={data.timestamps.startedAt} />
                                    </Typography>
                                )}
                            </Box>
                        )}
                        {data.timestamps.completedAt && (
                            <Typography variant="caption" color="textSecondary">
                                finished{" "}
                                <Timestamp component="span" timestamp={data.timestamps.completedAt} />
                            </Typography>
                        )}
                    </Box>
                )}
            </TableCell>
            <TableCell>
                <Link color="inherit" to={data.id}>{data.id.substring(0, 8)}...</Link>
            </TableCell>
            <TableCell>
                {data.type}
            </TableCell>
            <TableCell>
                {data.annotations.length === 0 && '--'}
                <PipelineAnnotations fragmentRef={data} />
            </TableCell>
            <TableCell>
                {data.release?.semanticVersion || '--'}
            </TableCell>
            <TableCell>
                <Tooltip title={data.createdBy}>
                    <Box display="flex" alignItems="center">
                        <Gravatar width={24} height={24} email={data.createdBy} />
                        <Box marginLeft={1}>
                            {moment(data.metadata.createdAt as moment.MomentInput).fromNow()}
                        </Box>
                    </Box>
                </Tooltip>
            </TableCell>
            <TableCell>
                <PipelineStageIcons fragmentRef={data} />
            </TableCell>
        </TableRow >
    );
}

export default PipelineListItem
