import LoadingButton from '@mui/lab/LoadingButton';
import { Alert, Autocomplete, Box, Button, Checkbox, Dialog, DialogActions, DialogContent, DialogTitle, Divider, FormControlLabel, Link, Stack, TextField, Typography } from '@mui/material';
import { AdapterMoment } from '@mui/x-date-pickers/AdapterMoment';
import { DateTimePicker } from '@mui/x-date-pickers/DateTimePicker';
import { LocalizationProvider } from '@mui/x-date-pickers/LocalizationProvider';
import moment, { Moment } from 'moment-timezone';
import { useMemo, useState } from 'react';
import { MutationError } from '../../../common/error';
import PanelButton from '../../../common/PanelButton';

// Timezone list used for cron schedule
const timezones = moment.tz.names();

export type CronSchedule = {
    expression: string;
    timezone?: string | null | undefined;
};

interface Props {
    scheduledStartTime?: string | null
    cronSchedule?: CronSchedule | null
    nodeType: 'pipeline' | 'deployment' | 'task'
    error?: MutationError | null
    commitInFlight: boolean
    onClose: () => void
    onSetSchedule: (scheduledTime: Moment | null, cronSchedule: CronSchedule | null) => void
    onClearSchedule: () => void
}

function PipelineScheduleNodeDialog({ scheduledStartTime, cronSchedule, onClose, onSetSchedule, onClearSchedule, commitInFlight, error, nodeType }: Props) {
    const [selectedDate, setSelectedDate] = useState<Moment | null>(scheduledStartTime ? moment(scheduledStartTime) : null);
    const [selectedCronSchedule, setSelectedCronSchedule] = useState<CronSchedule>({
        expression: cronSchedule?.expression || '* * * * *',
        timezone: cronSchedule?.timezone || moment.tz.guess()
    });
    const [cancelSchedule, setCancelSchedule] = useState<boolean>(false);
    // Default to datetime if scheduled start time is set otherwise default to cron if cron schedule is set
    const [scheduleType, setScheduleType] = useState<string>((scheduledStartTime || !cronSchedule) ? 'datetime' : 'cron');

    const onDateTimeChange = (date: Moment | null) => {
        setSelectedDate(date);
    };

    const validDate = useMemo(() => selectedDate?.isValid(), [selectedDate]);

    // We're in edit mode if either scheduled start time or cron schedule is set
    const editMode = useMemo(() => scheduledStartTime || cronSchedule, [scheduledStartTime, cronSchedule]);

    const formattedTimestamp = useMemo(() => {
        if (!selectedDate) return '';
        const zoneName = moment.tz(moment.tz.guess()).zoneName();
        return `${selectedDate.format('MMMM Do YYYY, h:mm:ss A')} ${zoneName}`;
    }, [selectedDate]);

    const formValid = useMemo(() => {
        if (cancelSchedule) return true;
        return scheduleType === 'datetime' ? !!validDate : selectedCronSchedule.expression !== '' && selectedCronSchedule.timezone;
    }, [scheduleType, validDate, selectedCronSchedule, cancelSchedule]);

    return (
        <Dialog
            fullWidth
            maxWidth="md"
            open>
            <DialogTitle>
                {editMode ? 'Update Schedule' : 'Create Schedule'}
            </DialogTitle>
            <DialogContent dividers>
                {error && <Alert sx={{ mb: 2 }} severity={error.severity}>
                    {error.message}
                </Alert>}
                {!cancelSchedule && <Box mb={3}>
                    <Typography variant="subtitle1" gutterBottom>Select type of schedule</Typography>
                    <Divider sx={{ opacity: 0.6 }} />
                    <Stack marginTop={2} direction="row" spacing={2}>
                        <PanelButton
                            selected={scheduleType === 'datetime'}
                            onClick={() => setScheduleType('datetime')}
                        >
                            <Typography variant="subtitle1">Specific Date</Typography>
                            <Typography variant="caption" align="center">
                                Schedule using a specific date and time
                            </Typography>
                        </PanelButton>
                        <PanelButton
                            selected={scheduleType === 'cron'}
                            onClick={() => setScheduleType('cron')}
                        >
                            <Typography variant="subtitle1">CRON</Typography>
                            <Typography variant="caption" align="center">
                                Schedule using a CRON expression
                            </Typography>
                        </PanelButton>
                    </Stack>
                </Box>}
                {!cancelSchedule && <>
                    <Box mb={3}>
                        {scheduleType === 'datetime' && <>
                            <Typography variant="body2" gutterBottom>
                                Select a scheduled date and time for this {nodeType}
                            </Typography>
                            <LocalizationProvider dateAdapter={AdapterMoment}>
                                <DateTimePicker sx={{ width: '100%' }} value={selectedDate} onChange={onDateTimeChange} closeOnSelect={false} />
                            </LocalizationProvider>
                            {validDate && selectedDate && <Typography mt={2} variant="body2" color="textSecondary" gutterBottom>
                                This {nodeType} will run at {formattedTimestamp} which is approximately in {selectedDate.fromNow(true)}
                            </Typography>}
                        </>}
                        {scheduleType === 'cron' && <>
                            <Typography variant="body2" gutterBottom>
                                Enter a CRON expression which will be used to calculate the scheduled start time for this {nodeType}
                            </Typography>
                            <Typography component="div" variant="caption" color="textSecondary" gutterBottom>
                                The CRON expression uses the following
                                {' '}
                                <Link
                                    color="secondary"
                                    underline="none"
                                    target='_blank'
                                    rel='noopener noreferrer'
                                    href="https://en.wikipedia.org/wiki/Cron">
                                    format
                                </Link>
                                {' '}
                                (minute, hour, day of month, month, and day of week).
                            </Typography>
                            <Box mb={2}>
                                <TextField
                                    fullWidth
                                    margin="normal"
                                    autoComplete="off"
                                    size="small"
                                    label="CRON Expression"
                                    value={selectedCronSchedule.expression}
                                    onChange={event => setSelectedCronSchedule({ ...selectedCronSchedule, expression: event.target.value })}
                                />
                            </Box>
                            <Autocomplete
                                size="small"
                                options={timezones}
                                sx={{ width: 300 }}
                                value={selectedCronSchedule.timezone}
                                onChange={(_, value) => setSelectedCronSchedule({ ...selectedCronSchedule, timezone: value })}
                                renderInput={(params) => <TextField {...params} label="Timezone" />}
                            />
                        </>}
                    </Box>
                </>}
                {editMode && <FormControlLabel
                    control={<Checkbox
                        color="warning"
                        checked={cancelSchedule}
                        onChange={event => setCancelSchedule(event.target.checked)}
                    />}
                    label="Remove Schedule"
                />}
                {cancelSchedule && <Typography mt={1} variant="body2" color="textSecondary" gutterBottom>
                    The schedule for this {nodeType} will be removed. If the {nodeType} is currently in the waiting state, then it'll
                    be moved back to the ready state.
                </Typography>}
            </DialogContent>
            <DialogActions>
                <Button
                    size="small"
                    variant="outlined"
                    onClick={() => onClose()}
                    color="inherit"
                >
                    Cancel</Button>
                <LoadingButton
                    disabled={!formValid}
                    loading={commitInFlight}
                    size="small"
                    variant="contained"
                    color="primary"
                    sx={{ ml: 2 }}
                    onClick={() => cancelSchedule ? onClearSchedule() : onSetSchedule(
                        scheduleType === 'datetime' ? selectedDate : null, scheduleType === 'cron' ? selectedCronSchedule : null
                    )}
                >
                    {editMode ? 'Update' : 'Set'} Schedule
                </LoadingButton>
            </DialogActions>
        </Dialog>
    );
}

export default PipelineScheduleNodeDialog;
