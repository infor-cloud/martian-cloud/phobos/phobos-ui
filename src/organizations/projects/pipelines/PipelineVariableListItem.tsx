import TableRow from '@mui/material/TableRow';
import graphql from 'babel-plugin-relay/macro';
import { useFragment } from 'react-relay/hooks';
import DataTableCell from '../../../common/DataTableCell';
import { PipelineVariableListItemFragment_variable$key } from './__generated__/PipelineVariableListItemFragment_variable.graphql';

interface Props {
    fragmentRef: PipelineVariableListItemFragment_variable$key;
    showValues: boolean;
}

function PipelineVariableListItem({ fragmentRef, showValues }: Props) {
    const data = useFragment<PipelineVariableListItemFragment_variable$key>(
        graphql`
        fragment PipelineVariableListItemFragment_variable on PipelineVariable
        {
            key
            value
            category
        }
      `, fragmentRef);

    return (
        <TableRow
            sx={{ '&:last-child td, &:last-child th': { border: 0 } }}
        >
            <DataTableCell sx={{ wordBreak: 'break-all' }}>
                {data.key}
            </DataTableCell>
            <DataTableCell sx={{ wordBreak: 'break-all' }} mask={!showValues}>
                {data.value}
            </DataTableCell>
        </TableRow>
    );
}

export default PipelineVariableListItem;
