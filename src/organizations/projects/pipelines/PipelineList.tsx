import { Box, Paper, Table, TableBody, TableCell, TableContainer, TableHead, TableRow, Typography } from "@mui/material";
import graphql from "babel-plugin-relay/macro";
import { ConnectionHandler, useFragment, useLazyLoadQuery, usePaginationFragment, useSubscription } from "react-relay/hooks";
import PipelineListItem from "./PipelineListItem";
import InfiniteScroll from 'react-infinite-scroll-component';
import ListSkeleton from "../../../skeletons/ListSkeleton";
import { PipelineListQuery } from "./__generated__/PipelineListQuery.graphql";
import { PipelineListPaginationQuery } from "./__generated__/PipelineListPaginationQuery.graphql";
import { PipelineListFragment_project$key } from "./__generated__/PipelineListFragment_project.graphql";
import { PipelineListFragment_pipelines$key } from "./__generated__/PipelineListFragment_pipelines.graphql";
import { useMemo } from "react";
import { ConnectionInterface, GraphQLSubscriptionConfig, RecordSourceProxy } from "relay-runtime";
import { PipelineListSubscription, PipelineListSubscription$data } from "./__generated__/PipelineListSubscription.graphql";

const query = graphql`
    query PipelineListQuery($id: String!, $first: Int, $after: String) {
        node(id: $id) {
            ... on Project {
                ...PipelineListFragment_pipelines
            }
        }
    }
`;

const pipelineSubscription = graphql`subscription PipelineListSubscription($input: ProjectPipelineEventsSubscriptionInput!) {
    projectPipelineEvents(input: $input) {
      action
      pipeline {
        id
        ...PipelineListItem_pipeline
      }
    }
  }`;

export function GetConnections(projectId: string): [string] {
    const connectionId = ConnectionHandler.getConnectionID(
        projectId,
        'PipelineList_pipelines',
        { sort: 'CREATED_AT_DESC' }
    );
    return [connectionId];
}

interface Props {
    fragmentRef: PipelineListFragment_project$key
}

function PipelineList({ fragmentRef }: Props) {

    const project = useFragment<PipelineListFragment_project$key>(
        graphql`
        fragment PipelineListFragment_project on Project
        {
            id
        }
    `, fragmentRef);

    const queryData = useLazyLoadQuery<PipelineListQuery>(query, { first: 50, id: project.id }, { fetchPolicy: "store-and-network" })

    const { data, loadNext, hasNext } = usePaginationFragment<PipelineListPaginationQuery, PipelineListFragment_pipelines$key>(
        graphql`
          fragment PipelineListFragment_pipelines on Project
          @refetchable(queryName: "PipelineListPaginationQuery") {
            pipelines(
                first: $first
                after: $after
                sort: CREATED_AT_DESC
            ) @connection(key: "PipelineList_pipelines") {
                totalCount
                edges {
                    node {
                        id
                        ...PipelineListItem_pipeline
                    }
                }
            }
        }
    `, queryData.node);

    const pipelineSubscriptionConfig = useMemo<GraphQLSubscriptionConfig<PipelineListSubscription>>(() => ({
        variables: { input: { projectId: project.id } },
        subscription: pipelineSubscription,
        onCompleted: () => console.log("Subscription completed"),
        onError: () => console.warn("Subscription error"),
        updater: (store: RecordSourceProxy, payload: PipelineListSubscription$data) => {
            const record = store.get(payload.projectPipelineEvents.pipeline.id);
            if (record == null) {
                return;
            }
            GetConnections(project.id).forEach(id => {
                const connectionRecord = store.get(id);
                if (connectionRecord) {
                    const { NODE, EDGES } = ConnectionInterface.get();

                    const recordId = record.getDataID();
                    // Check if edge already exists in connection
                    const nodeAlreadyExistsInConnection = connectionRecord
                        .getLinkedRecords(EDGES)
                        ?.some(
                            edge => edge?.getLinkedRecord(NODE)?.getDataID() === recordId,
                        );
                    if (!nodeAlreadyExistsInConnection) {
                        // Create Edge
                        const edge = ConnectionHandler.createEdge(
                            store,
                            connectionRecord,
                            record,
                            'PipelineEdge'
                        );
                        if (edge) {
                            // Add edge to the beginning of the connection
                            ConnectionHandler.insertEdgeBefore(
                                connectionRecord,
                                edge,
                            );
                        }
                    }
                }
            });
        }
    }), [project.id]);

    useSubscription<PipelineListSubscription>(pipelineSubscriptionConfig);

    return (
        <Box>
            {data?.pipelines?.edges && data.pipelines?.edges.length > 0 ?
                <InfiniteScroll
                    dataLength={data.pipelines?.edges.length}
                    next={() => loadNext(20)}
                    hasMore={hasNext}
                    loader={<ListSkeleton rowCount={3} />}
                >
                    <TableContainer>
                        <Table
                            sx={{ minWidth: 650, tableLayout: 'fixed' }}
                            aria-label="project pipelines"
                        >
                            <TableHead>
                                <TableRow>
                                    <TableCell>Status</TableCell>
                                    <TableCell>ID</TableCell>
                                    <TableCell>Type</TableCell>
                                    <TableCell>Annotations</TableCell>
                                    <TableCell>Release</TableCell>
                                    <TableCell>Triggerer</TableCell>
                                    <TableCell>Stages</TableCell>
                                </TableRow>
                            </TableHead>
                            <TableBody>
                                {data.pipelines?.edges?.map((edge: any) => (
                                    <PipelineListItem key={edge.node.id} fragmentRef={edge.node} />
                                ))}
                            </TableBody>
                        </Table>
                    </TableContainer>
                </InfiniteScroll> : <Paper variant="outlined" sx={{ display: "flex", justifyContent: "center" }}>
                    <Box sx={{ p: 4 }}>
                        <Typography variant="h6" color="textSecondary" align="center">No pipelines have been created for this project.</Typography>
                    </Box>
                </Paper>}
        </Box>
    );
}

export default PipelineList
