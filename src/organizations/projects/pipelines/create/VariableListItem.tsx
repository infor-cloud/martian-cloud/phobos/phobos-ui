import DeleteIcon from '@mui/icons-material/CloseOutlined';
import EditIcon from '@mui/icons-material/EditOutlined';
import { Button, Stack, TableCell, TableRow } from '@mui/material';
import DataTableCell from '../../../../common/DataTableCell';
import { Variable } from './VariableDialog';

interface Props {
    showEnvironment?: boolean
    variable: Variable
    onEdit: (variable: Variable) => void
    onDelete: (variable: Variable) => void
}

function VariableListItem({ showEnvironment, variable, onEdit, onDelete }: Props) {

    return (
        <TableRow
            sx={{ '&:last-child td, &:last-child th': { border: 0 }, height: 64 }}>
            <DataTableCell sx={{ fontWeight: 'bold', wordBreak: 'break-all' }}>
                {variable.key}
            </DataTableCell>
            <DataTableCell sx={{ wordBreak: 'break-all' }}>
                {variable.value}
            </DataTableCell>
            {showEnvironment && <TableCell sx={{ wordBreak: 'break-all' }}>
                {variable.environmentName || 'All Environments'}
            </TableCell>}
            <TableCell align='right'>
                <Stack direction="row" spacing={1} justifyContent="flex-end">
                    <Button
                        onClick={() => onEdit(variable)}
                        sx={{ minWidth: 40, padding: '2px' }}
                        size="small"
                        color="info"
                        variant="outlined">
                        <EditIcon />
                    </Button>
                    <Button
                        onClick={() => onDelete(variable)}
                        sx={{ minWidth: 40, padding: '2px' }}
                        size="small"
                        color="info"
                        variant="outlined">
                        <DeleteIcon />
                    </Button>
                </Stack>
            </TableCell>
        </TableRow>
    );
}

export default VariableListItem
