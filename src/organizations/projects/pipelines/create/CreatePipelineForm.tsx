import { CloudUpload, PlaylistPlay } from '@mui/icons-material';
import { Alert, Box, Button, Divider, Grid, Paper, Stack, Typography, useMediaQuery, useTheme } from '@mui/material';
import { nanoid } from 'nanoid';
import { useState } from 'react';
import { Link as RouterLink } from 'react-router-dom';
import PanelButton from '../../../../common/PanelButton';
import { MutationError } from '../../../../common/error';
import EnvironmentAutocomplete from '../../../../environmentrules/EnvironmentAutocomplete';
import PipelineTemplateAutocomplete, { Option as PipelineTemplateAutocompleteOption } from '../../releases/create/PipelineTemplateAutocomplete';
import PipelineTemplateVersionAutocomplete from '../../releases/create/PipelineTemplateVersionAutocomplete';
import ProjectVariableSetAutocomplete, { ProjectVariableSetOption } from '../../variables/ProjectVariableSetAutocomplete';
import VariableDialog, { Variable } from './VariableDialog';
import VariableList from './VariableList';
import { PipelineType } from './__generated__/CreatePipelineMutation.graphql';

export interface FormData {
    pipelineTemplateName: string | null
    pipelineTemplateVersion: string | null
    pipelineType: PipelineType
    environmentName: string | null
    variables: Variable[]
    variableSetRevision: string | null
}

interface PipelineTypes {
    type: PipelineType
    title: string
    description: string
}

interface PanelIconTypes {
    [key: string]: JSX.Element
}

const PANEL_ICONS: PanelIconTypes = {
    RUNBOOK: <PlaylistPlay />,
    DEPLOYMENT: <CloudUpload />,
};

const PIPELINE_TYPES: PipelineTypes[] = [
    {
        type: 'DEPLOYMENT',
        title: 'Deployment',
        description: 'Deployment pipelines deploy to a designated environment.'
    },
    {
        type: 'RUNBOOK',
        title: 'Runbook',
        description: 'Runbook pipelines are for specific tasks that do not require them to be associated with an environment or a release.'
    }
];

interface Props {
    error?: MutationError
    data: FormData
    projectId: string
    environmentNames: readonly string[]
    onChange: (data: FormData) => void
}

function CreatePipelineForm({ error, data, projectId, environmentNames, onChange }: Props) {
    const theme = useTheme();
    const isXs = useMediaQuery(theme.breakpoints.down('sm'));
    const [selectedType, setSelectedType] = useState<PipelineType | undefined>(undefined);
    const [variableToAdd, setVariableToAdd] = useState<Variable | null>(null);

    const onOptionSelected = (option: PipelineTypes) => {
        setSelectedType(option.type);
        onChange({ ...data, pipelineType: option.type, environmentName: null });
    };

    const onNewVariable = () => {
        setVariableToAdd({
            id: '',
            key: '',
            value: '',
            category: 'HCL'
        });
    };

    const updateVariable = (variable: Variable | null, keepOpen: boolean) => {
        if (!variable) {
            setVariableToAdd(null);
            return;
        }

        const updatedVariables = [...data.variables];

        if (variable.id === '') {
            updatedVariables.push({ ...variable, id: nanoid() });
        } else {
            const index = updatedVariables.findIndex((v) => v.id === variable.id);
            updatedVariables[index] = { ...variable, id: variable.id }
        }
        onChange({ ...data, variables: updatedVariables });

        if (keepOpen) {
            setVariableToAdd({
                id: '',
                key: '',
                value: '',
                category: 'HCL'
            });
        } else {
            setVariableToAdd(null);
        }
    };

    const deleteVariable = (variable: Variable) => {
        const index = data.variables.findIndex((v) => ((v.id === variable.id)));

        if (index !== -1) {
            const updatedVariables = [...data.variables];
            updatedVariables.splice(index, 1);
            onChange({ ...data, variables: updatedVariables })
        }
    };

    const onEditVariable = (variable: Variable) => {
        setVariableToAdd(variable)
    };

    const onPipelineTemplateVersionSelected = (semanticVersion: string | null) => {
        onChange({ ...data, pipelineTemplateVersion: semanticVersion || null })
    };

    const onPipelineTemplateSelected = (option: PipelineTemplateAutocompleteOption | null) => {
        onChange({ ...data, pipelineTemplateName: option?.name || null, pipelineTemplateVersion: option?.semanticVersion || null })
    };

    const onVariableSetRevisionSelected = (value: ProjectVariableSetOption | null) => {
        onChange({ ...data, variableSetRevision: value?.revision || null })
    };

    return (
        <Box sx={{ mt: 2, mb: 2 }}>
            {error && <Alert sx={{ mt: 2, mb: 2 }} severity={error.severity}>
                {error.message}
            </Alert>}
            <Typography variant="subtitle1" gutterBottom>Select pipeline type</Typography>
            <Divider sx={{ opacity: 0.6 }} />
            <Stack
                direction={isXs ? 'column' : 'row'}
                spacing={2}
                sx={{
                    mt: 2,
                    mb: 4
                }}>
                {PIPELINE_TYPES.map(option => <PanelButton
                    key={option.type}
                    selected={selectedType === option.type}
                    onClick={() => onOptionSelected(option)}>
                    {PANEL_ICONS[option.type]}
                    <Typography variant="subtitle1">{option.title}</Typography>
                    <Typography variant="caption" align="center">
                        {option.description}
                    </Typography>
                </PanelButton>)}
            </Stack>
            {selectedType &&
                <Box>
                    {selectedType === 'DEPLOYMENT' &&
                        <Box sx={{ mb: 4 }}>
                            <Typography variant="subtitle1" gutterBottom>Select environment</Typography>
                            <Typography
                                variant="subtitle2" gutterBottom>
                                Select an existing environment or enter the name of the environment if it hasn't been created yet.
                            </Typography>
                            <Divider sx={{ opacity: 0.6 }} />
                            <Box sx={{ mt: 2 }}>
                                <EnvironmentAutocomplete
                                    options={environmentNames}
                                    onSelected={(environmentName: string | null) => onChange({ ...data, environmentName })}
                                />
                            </Box>
                        </Box>}
                    <Box sx={{ mb: 4 }}>
                        <Typography variant="subtitle1" gutterBottom>Select pipeline template</Typography>
                        <Divider sx={{ opacity: 0.6 }} />
                        <Grid container sx={{ mt: 0.5 }} spacing={2}>
                            <Grid item xs={12}>
                                <PipelineTemplateAutocomplete
                                    pipelineTemplateName={data.pipelineTemplateName}
                                    projectId={projectId}
                                    onSelected={onPipelineTemplateSelected}
                                />
                            </Grid>
                            <Grid item xs={12}>
                                <PipelineTemplateVersionAutocomplete
                                    projectId={projectId}
                                    name={data.pipelineTemplateName}
                                    version={data.pipelineTemplateVersion}
                                    onSelected={onPipelineTemplateVersionSelected}
                                />
                            </Grid>
                        </Grid>
                    </Box>
                    <Box mb={4}>
                        <Typography variant="subtitle1" gutterBottom>Variable Set Revision</Typography>
                        <Divider sx={{ opacity: 0.6, mb: 2 }} />
                        <Box display="flex" alignItems="center">
                            <ProjectVariableSetAutocomplete
                                projectId={projectId}
                                revision={data.variableSetRevision}
                                onSelected={onVariableSetRevisionSelected}
                            />
                            {data.variableSetRevision && <Button
                                sx={{ ml: 1, minWidth: 120 }}
                                component={RouterLink}
                                color="inherit"
                                target='_blank'
                                rel='noopener noreferrer'
                                to={`../../-/variables?revision=${data.variableSetRevision}`}>
                                View Revision
                            </Button>}
                        </Box>
                    </Box>
                    <Box sx={{ mb: 4 }}>
                        <Typography variant="subtitle1" gutterBottom>Pipeline Variables</Typography>
                        <Divider sx={{ opacity: 0.6 }} />
                        <Stack sx={{ mt: 2 }}>
                            {data.variables.length === 0 ?
                                <Paper
                                    variant='outlined'
                                    sx={{ display: 'flex', justifyContent: 'center' }}
                                >
                                    <Box
                                        sx={{ p: 4, display: 'flex', flexDirection: 'column', justifyContent: 'center', alignItems: 'center' }}>
                                        {<Typography color="textSecondary" align="center" sx={{ mb: 2 }}>
                                            Add variables which will be automatically set when executing pipelines
                                        </Typography>}
                                        <Button variant="outlined" color="primary" onClick={onNewVariable}>Add Variable</Button>
                                    </Box>
                                </Paper>
                                : <Box>
                                    <Paper>
                                        <Box
                                            sx={{ p: 2, display: 'flex', alignItems: 'center', justifyContent: 'space-between' }}>
                                            <Typography
                                                variant="subtitle1">
                                                {data.variables.length} variable{data.variables.length === 1 ? '' : 's'}
                                            </Typography>
                                            <Button
                                                size="small"
                                                variant="outlined"
                                                color="secondary"
                                                onClick={onNewVariable}>
                                                Add Variable
                                            </Button>
                                        </Box>
                                    </Paper>
                                    <VariableList
                                        variables={data.variables}
                                        onDeleteVariable={deleteVariable}
                                        onEditVariable={onEditVariable}
                                    />
                                </Box>
                            }
                        </Stack>
                    </Box>
                </Box>
            }
            {variableToAdd && <VariableDialog
                variable={variableToAdd}
                onClose={updateVariable}
            />}
        </Box>
    );
}

export default CreatePipelineForm;
