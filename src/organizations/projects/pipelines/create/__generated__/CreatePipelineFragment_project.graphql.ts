/**
 * @generated SignedSource<<deb15c414ea7f55fab0e7992a828e922>>
 * @lightSyntaxTransform
 * @nogrep
 */

/* tslint:disable */
/* eslint-disable */
// @ts-nocheck

import { Fragment, ReaderFragment } from 'relay-runtime';
import { FragmentRefs } from "relay-runtime";
export type CreatePipelineFragment_project$data = {
  readonly environmentNames: ReadonlyArray<string>;
  readonly id: string;
  readonly metadata: {
    readonly prn: string;
  };
  readonly name: string;
  readonly organizationName: string;
  readonly " $fragmentType": "CreatePipelineFragment_project";
};
export type CreatePipelineFragment_project$key = {
  readonly " $data"?: CreatePipelineFragment_project$data;
  readonly " $fragmentSpreads": FragmentRefs<"CreatePipelineFragment_project">;
};

const node: ReaderFragment = {
  "argumentDefinitions": [],
  "kind": "Fragment",
  "metadata": null,
  "name": "CreatePipelineFragment_project",
  "selections": [
    {
      "alias": null,
      "args": null,
      "kind": "ScalarField",
      "name": "id",
      "storageKey": null
    },
    {
      "alias": null,
      "args": null,
      "kind": "ScalarField",
      "name": "name",
      "storageKey": null
    },
    {
      "alias": null,
      "args": null,
      "kind": "ScalarField",
      "name": "organizationName",
      "storageKey": null
    },
    {
      "alias": null,
      "args": null,
      "concreteType": "ResourceMetadata",
      "kind": "LinkedField",
      "name": "metadata",
      "plural": false,
      "selections": [
        {
          "alias": null,
          "args": null,
          "kind": "ScalarField",
          "name": "prn",
          "storageKey": null
        }
      ],
      "storageKey": null
    },
    {
      "alias": null,
      "args": null,
      "kind": "ScalarField",
      "name": "environmentNames",
      "storageKey": null
    }
  ],
  "type": "Project",
  "abstractKey": null
};

(node as any).hash = "b98f0009003b9d25cdbf204b27003fc1";

export default node;
