/**
 * @generated SignedSource<<8ac7a52d497c64f673bde4ba66577c7a>>
 * @lightSyntaxTransform
 * @nogrep
 */

/* tslint:disable */
/* eslint-disable */
// @ts-nocheck

import { ConcreteRequest, Mutation } from 'relay-runtime';
export type PipelineType = "DEPLOYMENT" | "NESTED" | "RELEASE_LIFECYCLE" | "RUNBOOK" | "%future added value";
export type ProblemType = "BAD_REQUEST" | "CONFLICT" | "FORBIDDEN" | "NOT_FOUND" | "%future added value";
export type VariableCategory = "ENVIRONMENT" | "HCL" | "%future added value";
export type CreatePipelineInput = {
  clientMutationId?: string | null | undefined;
  environmentName?: string | null | undefined;
  pipelineTemplateId: string;
  pipelineType: PipelineType;
  variableSetRevision?: string | null | undefined;
  variables?: ReadonlyArray<PipelineVariableInput> | null | undefined;
};
export type PipelineVariableInput = {
  category: VariableCategory;
  key: string;
  value: string;
};
export type CreatePipelineMutation$variables = {
  input: CreatePipelineInput;
};
export type CreatePipelineMutation$data = {
  readonly createPipeline: {
    readonly pipeline: {
      readonly id: string;
    } | null | undefined;
    readonly problems: ReadonlyArray<{
      readonly field: ReadonlyArray<string> | null | undefined;
      readonly message: string;
      readonly type: ProblemType;
    }>;
  };
};
export type CreatePipelineMutation = {
  response: CreatePipelineMutation$data;
  variables: CreatePipelineMutation$variables;
};

const node: ConcreteRequest = (function(){
var v0 = [
  {
    "defaultValue": null,
    "kind": "LocalArgument",
    "name": "input"
  }
],
v1 = [
  {
    "alias": null,
    "args": [
      {
        "kind": "Variable",
        "name": "input",
        "variableName": "input"
      }
    ],
    "concreteType": "PipelineMutationPayload",
    "kind": "LinkedField",
    "name": "createPipeline",
    "plural": false,
    "selections": [
      {
        "alias": null,
        "args": null,
        "concreteType": "Pipeline",
        "kind": "LinkedField",
        "name": "pipeline",
        "plural": false,
        "selections": [
          {
            "alias": null,
            "args": null,
            "kind": "ScalarField",
            "name": "id",
            "storageKey": null
          }
        ],
        "storageKey": null
      },
      {
        "alias": null,
        "args": null,
        "concreteType": "Problem",
        "kind": "LinkedField",
        "name": "problems",
        "plural": true,
        "selections": [
          {
            "alias": null,
            "args": null,
            "kind": "ScalarField",
            "name": "message",
            "storageKey": null
          },
          {
            "alias": null,
            "args": null,
            "kind": "ScalarField",
            "name": "field",
            "storageKey": null
          },
          {
            "alias": null,
            "args": null,
            "kind": "ScalarField",
            "name": "type",
            "storageKey": null
          }
        ],
        "storageKey": null
      }
    ],
    "storageKey": null
  }
];
return {
  "fragment": {
    "argumentDefinitions": (v0/*: any*/),
    "kind": "Fragment",
    "metadata": null,
    "name": "CreatePipelineMutation",
    "selections": (v1/*: any*/),
    "type": "Mutation",
    "abstractKey": null
  },
  "kind": "Request",
  "operation": {
    "argumentDefinitions": (v0/*: any*/),
    "kind": "Operation",
    "name": "CreatePipelineMutation",
    "selections": (v1/*: any*/)
  },
  "params": {
    "cacheID": "0884951f54bf1fe1fd7619146a141cd4",
    "id": null,
    "metadata": {},
    "name": "CreatePipelineMutation",
    "operationKind": "mutation",
    "text": "mutation CreatePipelineMutation(\n  $input: CreatePipelineInput!\n) {\n  createPipeline(input: $input) {\n    pipeline {\n      id\n    }\n    problems {\n      message\n      field\n      type\n    }\n  }\n}\n"
  }
};
})();

(node as any).hash = "c3dbe60b2a59abbe2edee01057f03b35";

export default node;
