import { Table, TableBody, TableCell, TableContainer, TableHead, TableRow, Typography } from '@mui/material';
import { Variable } from './VariableDialog';
import VariableListItem from './VariableListItem';

interface Props {
    showEnvironment?: boolean
    variables: Variable[]
    onEditVariable: (variable: Variable) => void
    onDeleteVariable: (variable: Variable) => void
}

function VariableList({ showEnvironment, variables, onEditVariable, onDeleteVariable } : Props) {
    return (
        <TableContainer>
            <Table sx={{ tableLayout: 'fixed' }}>
                <TableHead>
                    <TableRow>
                        <TableCell>
                            <Typography color="textSecondary">Key</Typography>
                        </TableCell>
                        <TableCell>
                            <Typography color="textSecondary">Value</Typography>
                        </TableCell>
                        {showEnvironment && <TableCell>
                            <Typography color="textSecondary">Environment</Typography>
                        </TableCell>}
                        <TableCell></TableCell>
                    </TableRow>
                </TableHead>
                <TableBody>
                    {variables.map((v: Variable) => <VariableListItem
                        key={v.id}
                        showEnvironment={showEnvironment}
                        variable={v}
                        onEdit={onEditVariable}
                        onDelete={onDeleteVariable}
                    />)}
                </TableBody>
            </Table>
        </TableContainer>
    );
}

export default VariableList
