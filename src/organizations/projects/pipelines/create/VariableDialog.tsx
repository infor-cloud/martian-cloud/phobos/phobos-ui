import {
    Box,
    Button,
    Checkbox,
    Dialog,
    DialogActions,
    DialogContent,
    FormControlLabel,
    Stack,
    TextField,
    Typography,
    useMediaQuery,
    useTheme
} from '@mui/material';
import { useEffect, useState } from 'react';
import EnvironmentSelect from '../../../../organizations/projects/releases/create/EnvironmentSelect';

export interface Variable {
    readonly id: string
    key: string
    value: any
    category: 'HCL'
    environmentName?: string | null
}

interface Props {
    showEnvironment?: boolean
    environmentNames?: string[]
    variable: Variable
    onClose: (variable: Variable | null, keepOpen: boolean) => void
    disableEnvironmentSelection?: boolean
}

function VariableDialog({ showEnvironment, environmentNames, variable, disableEnvironmentSelection, onClose }: Props) {
    const [newVariable, setNewVariable] = useState<Variable>(variable);
    const [environmentName, setEnvironmentName] = useState(variable.environmentName || 'All Environments');
    const [keepOpen, setKeepOpen] = useState(false);
    const theme = useTheme();
    const fullScreen = useMediaQuery(theme.breakpoints.down('md'));

    const disabled = !newVariable.key || !newVariable.value;

    useEffect(() => {
        setNewVariable(variable);
    }, [variable]);

    return (
        <Dialog
            fullWidth
            maxWidth="md"
            fullScreen={fullScreen}
            open={!!variable}
        >
            <Box
                display="flex"
                justifyContent="space-between"
                sx={{ pt: 2, pl: 3, pr: 3 }}
            >
                <Box>
                    <Typography variant="h6">
                        {variable.id === '' ? 'Add' : 'Edit'} Variable
                    </Typography>
                </Box>
            </Box>
            <DialogContent>
                {showEnvironment && <EnvironmentSelect
                    disabled={disableEnvironmentSelection}
                    environmentNames={environmentNames || []}
                    data={environmentName}
                    onChange={(selectedEnvironment: string) => setEnvironmentName(selectedEnvironment)}
                />}
                <TextField
                    value={newVariable.key}
                    label="Key"
                    name="key"
                    size="small"
                    margin="normal"
                    fullWidth
                    onChange={event => setNewVariable({ ...newVariable, key: event.target.value })}
                    autoComplete="off"
                />
                <TextField
                    value={newVariable.value}
                    label="Value"
                    name="value"
                    size="small"
                    margin="normal"
                    rows={6}
                    multiline
                    fullWidth
                    onChange={event => setNewVariable({ ...newVariable, value: event.target.value })}
                    autoComplete="off"
                />
            </DialogContent>
            <DialogActions sx={{ pl: 3, pr: 3, justifyContent: 'space-between' }}>
                <FormControlLabel
                    control={<Checkbox
                        color="secondary"
                        checked={keepOpen}
                        onChange={event => setKeepOpen(event.target.checked)}
                    />}
                    label="Keep open to add another"
                />
                <Stack direction="row" spacing={2}>
                    <Button onClick={() => onClose(null, false)} color="inherit">
                        Cancel
                    </Button>
                    <Button
                        disabled={disabled}
                        onClick={() => onClose({
                            ...newVariable,
                            key: newVariable.key.trim(),
                            value: newVariable.value.trim(),
                            environmentName: environmentName === "All Environments" ? null : environmentName
                        }, keepOpen)}
                        variant="contained">
                        Save
                    </Button>
                </Stack>
            </DialogActions>
        </Dialog>
    );
}

export default VariableDialog
