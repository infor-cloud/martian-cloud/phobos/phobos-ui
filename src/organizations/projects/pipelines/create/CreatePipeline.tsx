import { useMemo, useState } from 'react';
import { Box, Button, Divider, Typography } from '@mui/material';
import graphql from 'babel-plugin-relay/macro';
import { Link as RouterLink, useNavigate, useOutletContext } from 'react-router-dom';
import { useFragment, useMutation } from 'react-relay/hooks';
import { LoadingButton } from '@mui/lab';
import ProjectBreadcrumbs from '../../ProjectBreadcrumbs';
import { MutationError } from '../../../../common/error';
import CreatePipelineForm, { FormData } from './CreatePipelineForm';
import { CreatePipelineFragment_project$key } from './__generated__/CreatePipelineFragment_project.graphql';
import { CreatePipelineMutation } from './__generated__/CreatePipelineMutation.graphql';

function CreatePipeline() {
    const navigate = useNavigate();
    const context = useOutletContext<CreatePipelineFragment_project$key>();

    const data = useFragment<CreatePipelineFragment_project$key>(
        graphql`
            fragment CreatePipelineFragment_project on Project
            {
                id
                name
                organizationName
                metadata {
                    prn
                }
                environmentNames
            }
        `, context
    );

    const [error, setError] = useState<MutationError>();
    const [formData, setFormData] = useState<FormData>({
        pipelineTemplateName: null,
        pipelineTemplateVersion: null,
        pipelineType: 'RUNBOOK',
        environmentName: null,
        variables: [],
        variableSetRevision: null
    });

    const [commit, isInFlight] = useMutation<CreatePipelineMutation>(graphql`
        mutation CreatePipelineMutation($input: CreatePipelineInput!) {
            createPipeline(input: $input) {
                pipeline {
                    id
                }
                problems {
                    message
                    field
                    type
                }
            }
        }
    `);

    const onSave = () => {
        commit({
            variables: {
                input: {
                    pipelineTemplateId:
                        `prn:pipeline_template:${data.organizationName}/${data.name}/${formData.pipelineTemplateName}/${formData.pipelineTemplateVersion}`,
                    pipelineType: formData.pipelineType,
                    environmentName: formData.environmentName,
                    variables: formData.variables,
                    variableSetRevision: formData.variableSetRevision
                }
            },
            onCompleted: (data) => {
                if (data.createPipeline.problems.length) {
                    setError({
                        severity: 'warning',
                        message: data.createPipeline.problems.map((problem) => problem.message).join(', ')
                    });
                } else if (!data.createPipeline.pipeline) {
                    setError({
                        severity: 'error',
                        message: 'Unexpected error occurred'
                    });
                }
                else {
                    navigate('../');
                }
            },
            onError: (error) => {
                setError({
                    severity: 'error',
                    message: `Unexpected error occurred: ${error.message}`
                });
            }
        });
    };

    const disabled = useMemo(() => {
        return !formData.pipelineTemplateVersion ||
            (formData.pipelineType === 'DEPLOYMENT' && !formData.environmentName);
    }, [formData.pipelineTemplateVersion, formData.pipelineType, formData.environmentName]);

    return (
        <Box>
            <ProjectBreadcrumbs
                prn={data.metadata.prn}
                childRoutes={[
                    { title: 'pipelines', path: 'pipelines' },
                    { title: 'create', path: 'create' }
                ]}
            />
            <Typography variant="h5">Create Pipeline</Typography>
            <CreatePipelineForm
                error={error}
                data={formData}
                projectId={data.id}
                environmentNames={data.environmentNames}
                onChange={(data: FormData) => setFormData(data)}
            />
            <Divider sx={{ opacity: 0.6 }} />
            <Box mt={2}>
                <LoadingButton
                    sx={{ mr: 2 }}
                    loading={isInFlight}
                    disabled={disabled}
                    variant="outlined"
                    color="primary"
                    onClick={onSave}
                >
                    Create Pipeline
                </LoadingButton>
                <Button component={RouterLink} color="inherit" to={-1 as any}>Cancel</Button>
            </Box>
        </Box>
    );
}

export default CreatePipeline;
