import { Box, Link, TableCell, TableRow, Tooltip } from '@mui/material';
import graphql from 'babel-plugin-relay/macro';
import moment from 'moment';
import { useFragment } from 'react-relay/hooks';
import PipelineStatusChip from './PipelineStatusChip';
import { PipelineNestedPipelineListItemFragment$key } from './__generated__/PipelineNestedPipelineListItemFragment.graphql';

interface Props {
    fragmentRef: PipelineNestedPipelineListItemFragment$key
    onPipelineSelected: (id: string) => void
}

function PipelineNestedPipelineListItem({ fragmentRef, onPipelineSelected }: Props) {
    const data = useFragment(graphql`
        fragment PipelineNestedPipelineListItemFragment on Pipeline {
            id
            status
            metadata {
                createdAt
            }
        }
    `, fragmentRef);


    return (
        <TableRow sx={{ '&:last-child td, &:last-child th': { border: 0 } }}>
            <TableCell>
                <PipelineStatusChip status={data.status} onClick={() => onPipelineSelected(data.id)} />
            </TableCell>
            <TableCell>
                <Link sx={{ cursor: 'pointer' }} color="textPrimary" underline="hover" onClick={() => onPipelineSelected(data.id)}>{data.id.substring(0, 8)}...</Link>
            </TableCell>
            <TableCell>
                <Tooltip title={data.metadata.createdAt}>
                    <Box>{moment(data.metadata.createdAt as moment.MomentInput).fromNow()}</Box>
                </Tooltip>
            </TableCell>
        </TableRow >
    );
}

export default PipelineNestedPipelineListItem;
