import { default as ArrowDropDownIcon } from '@mui/icons-material/ArrowDropDown';
import { default as CalendarIcon } from '@mui/icons-material/CalendarMonthOutlined';
import { Alert, Box, Button, ButtonGroup, Chip, CircularProgress, Link, Menu, MenuItem, Paper, Stack, Tab, Tabs, Typography } from "@mui/material";
import graphql from 'babel-plugin-relay/macro';
import { Moment } from 'moment';
import { useSnackbar } from "notistack";
import React, { Suspense, useMemo, useState } from "react";
import { useFragment, useMutation } from "react-relay/hooks";
import { useOutletContext, useParams, useSearchParams } from "react-router-dom";
import ConfirmationDialog from '../../../common/ConfirmationDialog';
import { PipelineTaskIcon } from '../../../common/Icons';
import TabContent from "../../../common/TabContent";
import Timestamp from '../../../common/Timestamp';
import JobLogs from "./JobLogs";
import PipelineActionOutputs from "./PipelineActionOutputs";
import PipelineApprovalActions from './PipelineApprovalActions';
import PipelineApprovals from "./PipelineApprovals";
import PipelineJobsDialog from "./PipelineJobsDialog";
import PipelineScheduleNodeDialog, { CronSchedule } from './PipelineScheduleNodeDialog';
import PipelineStatusType from "./PipelineStatusType";
import { PipelineTaskDetailsApproveTaskMutation } from "./__generated__/PipelineTaskDetailsApproveTaskMutation.graphql";
import { PipelineTaskDetailsCancelJobMutation } from "./__generated__/PipelineTaskDetailsCancelJobMutation.graphql";
import { PipelineTaskDetailsCancelScheduledTaskMutation } from './__generated__/PipelineTaskDetailsCancelScheduledTaskMutation.graphql';
import { PipelineTaskDetailsFragment_details$key } from "./__generated__/PipelineTaskDetailsFragment_details.graphql";
import { PipelineTaskDetailsRetryTaskMutation } from "./__generated__/PipelineTaskDetailsRetryTaskMutation.graphql";
import { PipelineTaskDetailsRevokeTaskApprovalMutation } from './__generated__/PipelineTaskDetailsRevokeTaskApprovalMutation.graphql';
import { PipelineTaskDetailsRunTaskMutation } from "./__generated__/PipelineTaskDetailsRunTaskMutation.graphql";
import { PipelineTaskDetailsScheduleTaskMutation } from './__generated__/PipelineTaskDetailsScheduleTaskMutation.graphql';
import { PipelineTaskDetailsDeferTaskMutation } from './__generated__/PipelineTaskDetailsDeferTaskMutation.graphql';
import { PipelineTaskDetailsUndeferTaskMutation } from './__generated__/PipelineTaskDetailsUndeferTaskMutation.graphql';
import moment from 'moment';
import { StyledCode } from '../../../common/StyledCode';
import { MutationError } from '../../../common/error';
import PipelineDeferPipelineNodeDialog from './PipelineDeferPipelineNodeDialog';

function PipelineTaskDetails() {
    const [searchParams, setSearchParams] = useSearchParams();
    const { nodePath: taskPath } = useParams() as { nodePath: string };
    const tab = searchParams.get('tab') || 'logs';
    const [menuAnchorEl, setMenuAnchorEl] = useState<Element | null>(null);
    const [jobsDialogOpen, setJobsDialogOpen] = useState(false);
    const [showRetryTaskConfirmation, setShowRetryTaskConfirmation] = useState(false);
    const [showCancelJobConfirmation, setShowCancelJobConfirmation] = useState(false);
    const [showForceCancelJobConfirmation, setShowForceCancelJobConfirmation] = useState(false);
    const [showRunTaskConfirmation, setShowRunTaskConfirmation] = useState(false);
    const [showScheduleTaskDialog, setShowScheduleTaskDialog] = useState(false);
    const [scheduleTaskError, setScheduleTaskError] = useState<MutationError | null>(null);
    const [showDeferTaskDialog, setShowDeferTaskDialog] = useState(false);
    const [showUndeferTaskConfirmation, setShowUndeferTaskConfirmation] = useState(false);
    const { enqueueSnackbar } = useSnackbar();

    const context = useOutletContext<PipelineTaskDetailsFragment_details$key>();

    const [commitApproveTask, commitApproveTaskInFlight] = useMutation<PipelineTaskDetailsApproveTaskMutation>(graphql`
        mutation PipelineTaskDetailsApproveTaskMutation($input: ApprovePipelineTaskInput!) {
            approvePipelineTask(input: $input) {
                pipeline {
                    ...PipelineDetailsFragment_pipeline
                }
                problems {
                    message
                    field
                    type
                }
            }
        }
    `);

    const [commitRevokeTaskApproval, commitRevokeTaskApprovalInFlight] = useMutation<PipelineTaskDetailsRevokeTaskApprovalMutation>(graphql`
        mutation PipelineTaskDetailsRevokeTaskApprovalMutation($input: RevokePipelineTaskApprovalInput!) {
            revokePipelineTaskApproval(input: $input) {
                pipeline {
                    ...PipelineDetailsFragment_pipeline
                }
                problems {
                    message
                    field
                    type
                }
            }
        }
    `);

    const [commitCancelJob, commitCancelJobInFlight] = useMutation<PipelineTaskDetailsCancelJobMutation>(graphql`
        mutation PipelineTaskDetailsCancelJobMutation($input: CancelJobInput!) {
            cancelJob(input: $input) {
                job {
                    cancelRequestedAt
                    forceCancelAvailableAt
                }
                problems {
                    message
                    field
                    type
                }
            }
        }
    `);

    const [commitRetryTask, commitRetryTaskInFlight] = useMutation<PipelineTaskDetailsRetryTaskMutation>(graphql`
        mutation PipelineTaskDetailsRetryTaskMutation($input: RetryPipelineTaskInput!) {
            retryPipelineTask(input: $input) {
                problems {
                    message
                    field
                    type
                }
            }
        }
    `);

    const [commitRunTask, commitRunTaskInFlight] = useMutation<PipelineTaskDetailsRunTaskMutation>(graphql`
        mutation PipelineTaskDetailsRunTaskMutation($input: RunPipelineTaskInput!) {
            runPipelineTask(input: $input) {
                problems {
                    message
                    field
                    type
                }
            }
        }
    `);

    const [commitScheduleTask, commitScheduleTaskInFlight] = useMutation<PipelineTaskDetailsScheduleTaskMutation>(graphql`
        mutation PipelineTaskDetailsScheduleTaskMutation($input: SchedulePipelineNodeInput!) {
            schedulePipelineNode(input: $input) {
                problems {
                    message
                    field
                    type
                }
            }
        }
    `);

    const [commitCancelScheduledTask, commitCancelScheduledTaskInFlight] = useMutation<PipelineTaskDetailsCancelScheduledTaskMutation>(graphql`
        mutation PipelineTaskDetailsCancelScheduledTaskMutation($input: CancelPipelineNodeScheduleInput!) {
            cancelPipelineNodeSchedule(input: $input) {
                problems {
                    message
                    field
                    type
                }
            }
        }
    `);

    const [commitDeferTask, commitDeferTaskInFlight] = useMutation<PipelineTaskDetailsDeferTaskMutation>(graphql`
        mutation PipelineTaskDetailsDeferTaskMutation($input: DeferPipelineNodeInput!) {
            deferPipelineNode(input: $input) {
                problems {
                    message
                    field
                    type
                }
            }
        }
    `);

    const [commitUndeferTask, commitUndeferTaskInFlight] = useMutation<PipelineTaskDetailsUndeferTaskMutation>(graphql`
        mutation PipelineTaskDetailsUndeferTaskMutation($input: UndeferPipelineNodeInput!) {
            undeferPipelineNode(input: $input) {
                problems {
                    message
                    field
                    type
                }
            }
        }
    `);

    const pipeline = useFragment<PipelineTaskDetailsFragment_details$key>(
        graphql`
            fragment PipelineTaskDetailsFragment_details on Pipeline
            {
                id
                status
                stages {
                    tasks {
                        ...PipelineActionOutputsFragment_outputs
                        ...PipelineApprovalsFragment_approvals
                        ...PipelineApprovalActionsFragment_approvableNode
                        path
                        name
                        status
                        when
                        agentTags
                        scheduledStartTime
                        cronSchedule {
                            expression
                            timezone
                        }
                        maxAttempts
                        attemptCount
                        interval
                        lastAttemptFinishedAt
                        currentJob {
                            id
                        }
                        jobs(first: 0) {
                            totalCount
                        }
                        approvalStatus
                        errors
                    }
                }
            }
        `, context);

    const handleMutationError = (error: Error, completedCallback?: () => void) => {
        if (completedCallback) {
            completedCallback();
        }
        enqueueSnackbar(`Unexpected error: ${error.message}`, { variant: 'error' });
    }

    const handleMutationProblems = (problems: any, completedCallback?: () => void) => {
        if (completedCallback) {
            completedCallback();
        }
        if (problems && problems.length > 0) {
            enqueueSnackbar(problems.map((problem: any) => problem.message).join('; '), { variant: 'warning' });
        }
    }

    const onApproveTask = (taskPath: string) => {
        commitApproveTask({
            variables: {
                input: {
                    pipelineId: pipeline.id,
                    taskPath: taskPath,
                },
            },
            onCompleted: (data) => handleMutationProblems(data.approvePipelineTask?.problems),
            onError: handleMutationError
        });
    }

    const onRevokeTaskApproval = (taskPath: string) => {
        commitRevokeTaskApproval({
            variables: {
                input: {
                    pipelineId: pipeline.id,
                    taskPath: taskPath,
                },
            },
            onCompleted: (data) => handleMutationProblems(data.revokePipelineTaskApproval?.problems),
            onError: handleMutationError
        });
    }

    const onCancelJob = (jobId: string, force: boolean) => {
        commitCancelJob({
            variables: {
                input: {
                    id: jobId,
                    force
                },
            },
            onCompleted: data => handleMutationProblems(data.cancelJob?.problems, () => {
                if (force) {
                    setShowForceCancelJobConfirmation(false);
                } else {
                    setShowCancelJobConfirmation(false);
                }
            }),
            onError: error => handleMutationError(error, () => {
                if (force) {
                    setShowForceCancelJobConfirmation(false);
                } else {
                    setShowCancelJobConfirmation(false);
                }
            })
        });
    }

    const onRetryTask = (taskPath: string) => {
        commitRetryTask({
            variables: {
                input: {
                    pipelineId: pipeline.id,
                    taskPath: taskPath,
                },
            },
            onCompleted: data => handleMutationProblems(data.retryPipelineTask?.problems, () => setShowRetryTaskConfirmation(false)),
            onError: error => handleMutationError(error, () => setShowRetryTaskConfirmation(false))
        });
    }

    const onRunTask = (taskPath: string) => {
        commitRunTask({
            variables: {
                input: {
                    pipelineId: pipeline.id,
                    taskPath: taskPath,
                },
            },
            onCompleted: data => handleMutationProblems(data.runPipelineTask?.problems, () => setShowRunTaskConfirmation(false)),
            onError: error => handleMutationError(error, () => setShowRunTaskConfirmation(false))
        });
    }

    const onScheduleTask = (taskPath: string, scheduledTime: Moment | null, cronSchedule: CronSchedule | null) => {
        commitScheduleTask({
            variables: {
                input: {
                    pipelineId: pipeline.id,
                    nodePath: taskPath,
                    nodeType: 'TASK',
                    scheduledStartTime: scheduledTime?.utc().toISOString(),
                    cronSchedule,
                },
            },
            onCompleted: data => {
                if (data.schedulePipelineNode?.problems.length) {
                    setScheduleTaskError({
                        severity: 'warning',
                        message: data.schedulePipelineNode.problems.map((problem: any) => problem.message).join(', ')
                    });
                } else {
                    setScheduleTaskError(null);
                    setShowScheduleTaskDialog(false);
                }
            },
            onError: error => handleMutationError(error, () => setShowScheduleTaskDialog(false))
        });
    }

    const onCancelScheduledTask = (taskPath: string) => {
        commitCancelScheduledTask({
            variables: {
                input: {
                    pipelineId: pipeline.id,
                    nodePath: taskPath,
                    nodeType: 'TASK',
                },
            },
            onCompleted: data => handleMutationProblems(data.cancelPipelineNodeSchedule?.problems, () => setShowScheduleTaskDialog(false)),
            onError: handleMutationError
        });
    }

    const onDeferTask = (reason: string) => {
        commitDeferTask({
            variables: {
                input: {
                    pipelineId: pipeline.id,
                    nodePath: selectedTask?.path as string,
                    nodeType: 'TASK',
                    reason: reason,
                },
            },
            onCompleted: data => handleMutationProblems(data.deferPipelineNode?.problems, () => setShowDeferTaskDialog(false)),
            onError: error => handleMutationError(error, () => setShowDeferTaskDialog(false))
        });
    }

    const onUndeferTask = (taskPath: string) => {
        commitUndeferTask({
            variables: {
                input: {
                    pipelineId: pipeline.id,
                    nodePath: taskPath,
                    nodeType: 'TASK',
                },
            },
            onCompleted: data => handleMutationProblems(data.undeferPipelineNode?.problems, () => setShowUndeferTaskConfirmation(false)),
            onError: error => handleMutationError(error, () => setShowUndeferTaskConfirmation(false))
        });
    }

    const onJobSelected = (jobId: string) => {
        searchParams.set('job', jobId);
        setSearchParams(searchParams, { replace: true });
        setJobsDialogOpen(false);
    };

    const onTabChange = (event: React.SyntheticEvent, newValue: string) => {
        searchParams.set('tab', newValue);
        setSearchParams(searchParams, { replace: true });
    };

    const selectedTask = useMemo(() => {
        return pipeline.stages.flatMap(stage => stage.tasks).find(task => task.path === taskPath);
    }, [pipeline, taskPath]);

    const jobId = searchParams.get('job') ?? (selectedTask ? selectedTask?.currentJob?.id : null);

    const actions = useMemo(() => [
        {
            label: 'Retry Task',
            condition: ['SUCCEEDED', 'FAILED', 'CANCELED'].includes(selectedTask?.status ?? ''),
            handler: () => setShowRetryTaskConfirmation(true)
        },
        {
            label: 'Run Task',
            condition: selectedTask?.when === 'manual' && selectedTask?.status === 'READY',
            handler: () => setShowRunTaskConfirmation(true)
        },
        {
            label: 'Restore Task',
            condition: selectedTask?.status === 'DEFERRED' && pipeline.status !== 'DEFERRED',
            handler: () => setShowUndeferTaskConfirmation(true)
        },
        {
            label: 'Update Schedule',
            condition: selectedTask?.status === 'WAITING' && selectedTask?.attemptCount === 0,
            handler: () => setShowScheduleTaskDialog(true)
        },
        {
            label: 'Set Schedule',
            condition: ['CREATED', 'APPROVAL_PENDING'].includes(selectedTask?.status ?? '') && selectedTask?.attemptCount === 0,
            handler: () => setShowScheduleTaskDialog(true)
        },
        {
            label: 'Schedule Task',
            condition: selectedTask?.when === 'manual' && selectedTask?.status === 'READY',
            handler: () => setShowScheduleTaskDialog(true)
        },
        {
            label: 'Defer Task',
            condition: selectedTask?.status === 'WAITING' || selectedTask?.status === 'READY',
            handler: () => setShowDeferTaskDialog(true)
        },
    ], [selectedTask, pipeline]);

    const mainButton = useMemo(() => actions.find(action => action.condition), [actions]);
    const menuItems = useMemo(() => actions.filter(action => action !== mainButton && action.condition), [actions, mainButton]);

    return selectedTask ? (
        <Box>
            <Box display="flex" alignItems="center" justifyContent="space-between" marginBottom={2}>
                <Box display="flex" alignItems="center">
                    <PipelineTaskIcon />
                    <Typography ml={1} variant="h6">
                        Task
                    </Typography>
                </Box>
                {mainButton && menuItems.length === 0 && (
                    <Button variant="outlined" onClick={mainButton.handler}>
                        {mainButton.label}
                    </Button>
                )}
                {mainButton && menuItems.length > 0 && <>
                    <ButtonGroup variant="outlined" color="primary">
                        <Button variant="outlined" onClick={mainButton.handler}>
                            {mainButton.label}
                        </Button>
                        <Button
                            color="primary"
                            size="small"
                            aria-label="more options menu"
                            aria-haspopup="menu"
                            onClick={(event) => setMenuAnchorEl(event.currentTarget)}
                        >
                            <ArrowDropDownIcon fontSize="small" />
                        </Button>
                    </ButtonGroup>
                    <Menu
                        id="task-more-options-menu"
                        anchorEl={menuAnchorEl}
                        open={Boolean(menuAnchorEl)}
                        onClose={() => setMenuAnchorEl(null)}
                        anchorOrigin={{
                            vertical: 'bottom',
                            horizontal: 'right',
                        }}
                        transformOrigin={{
                            vertical: 'top',
                            horizontal: 'right',
                        }}
                    >
                        {menuItems.map((item, index) => (
                            <MenuItem
                                key={index}
                                onClick={() => {
                                    item.handler();
                                    setMenuAnchorEl(null);
                                }}
                            >
                                {item.label}
                            </MenuItem>
                        ))}
                    </Menu>
                </>}
            </Box>
            <Paper variant="outlined" sx={{ padding: 2, display: 'flex', alignItems: 'center', marginBottom: 2 }}>
                {PipelineStatusType[selectedTask.status].icon}
                <Typography marginLeft={1} variant="body2">
                    Task <strong>{selectedTask.name}</strong> {PipelineStatusType[selectedTask.status].tooltip}
                </Typography>
                {selectedTask.attemptCount > 0 && selectedTask.status === 'WAITING' && <Box marginLeft={1}>
                    <Typography variant="body2">
                        Task <strong>{selectedTask.name}</strong> will run again on <Timestamp format="absolute" component="span" timestamp={moment(selectedTask.lastAttemptFinishedAt).add(selectedTask.interval, 'second').format()} />
                    </Typography>
                    <Typography color="textSecondary" variant="body2">
                        {selectedTask.maxAttempts - selectedTask.attemptCount} attempt{(selectedTask.maxAttempts - selectedTask.attemptCount) === 1 ? '' : 's'} remaining
                    </Typography>
                </Box>}
            </Paper>
            {selectedTask.attemptCount === 0 && (selectedTask.scheduledStartTime || selectedTask.cronSchedule) && <Paper variant="outlined" sx={{ padding: 2, display: 'flex', alignItems: 'center', marginBottom: 2 }}>
                <CalendarIcon />
                {selectedTask.scheduledStartTime && <Box ml={1}>
                    <Typography variant="body2">
                        Scheduled to run <Timestamp format="absolute" component="span" timestamp={selectedTask.scheduledStartTime} />
                    </Typography>
                    {selectedTask.cronSchedule && <Typography component="div" variant="caption" color="textSecondary" mt={0.5}>
                        Scheduled start time was calculated using the cron expression
                        {' '}
                        <StyledCode>{selectedTask.cronSchedule.expression}</StyledCode>
                        {' '}
                        in the <strong>{selectedTask.cronSchedule.timezone}</strong> timezone
                    </Typography>}
                </Box>}
                {!selectedTask.scheduledStartTime && selectedTask.cronSchedule && <Typography component="div" marginLeft={1} variant="body2">
                    This task will be scheduled based on the cron expression
                    {' '}
                    <StyledCode>{selectedTask.cronSchedule.expression}</StyledCode>
                    {' '}
                    in the <strong>{selectedTask.cronSchedule.timezone}</strong> timezone
                </Typography>}
            </Paper>}
            <Paper variant="outlined" sx={{ padding: 2, marginBottom: 2 }}>
                {selectedTask.agentTags.length > 0 && <Box display="flex" alignItems="center" marginBottom={1}>
                    <Typography variant="body2" marginRight={1} fontWeight={500}>Agent Tags:</Typography>
                    <Stack direction="row" spacing={1}>
                        {selectedTask.agentTags.map(tag => <Chip key={tag} size="small" color="secondary" label={tag} />)}
                    </Stack>
                </Box>}
                <Typography variant="body2" component="div">
                    This task has
                    <Link component="button" underline="hover" onClick={() => setJobsDialogOpen(true)} variant="body2" sx={{ marginLeft: '4px', fontWeight: 600 }}>
                        {selectedTask.jobs.totalCount} Job{selectedTask.jobs.totalCount === 1 ? '' : 's'}
                    </Link>
                </Typography>
            </Paper>
            {selectedTask.errors.length > 0 && <Alert severity='error' variant='outlined' sx={{ mb: 2 }}>
                This task has {selectedTask.errors.length} error{selectedTask.errors.length > 1 ? 's' : ''}
                <ul>{selectedTask.errors.map((err, index) => (<Typography component="li" variant="body2" key={index}>{err}</Typography>))}</ul>
            </Alert>}
            {(selectedTask.status === 'APPROVAL_PENDING' || selectedTask.approvalStatus === 'APPROVED') && <PipelineApprovalActions
                fragmentRef={selectedTask}
                approved={selectedTask.approvalStatus === 'APPROVED'}
                commitInFlight={commitApproveTaskInFlight || commitRevokeTaskApprovalInFlight}
                onApprove={() => onApproveTask(selectedTask.path)}
                onRevoke={() => onRevokeTaskApproval(selectedTask.path)}
            />}
            <Box sx={{ display: "flex", border: 1, borderColor: 'divider', borderTopLeftRadius: 4, borderTopRightRadius: 4, mb: 2 }}>
                <Tabs value={tab} onChange={onTabChange}>
                    <Tab label="Logs" value="logs" />
                    <Tab label="Approvals" value="approvals" />
                    <Tab label="Outputs" value="outputs" />
                </Tabs>
            </Box>
            <TabContent>
                {tab === 'logs' && <Box>
                    {!jobId && <Paper variant="outlined" sx={{ marginTop: 4, display: 'flex', justifyContent: 'center' }}>
                        <Box padding={4} display="flex" flexDirection="column" justifyContent="center" alignItems="center">
                            <Typography color="textSecondary" align="center">
                                This task does not have any logs
                            </Typography>
                        </Box>
                    </Paper>}
                    {jobId && <Box>
                        <Suspense fallback={<Box
                            sx={{
                                position: 'absolute',
                                top: 0,
                                left: 0,
                                width: '100%',
                                height: '100vh',
                                display: 'flex',
                                alignItems: 'center',
                                justifyContent: 'center'
                            }}>
                            <CircularProgress />
                        </Box>}>
                            <JobLogs
                                jobId={jobId}
                                latestJobId={selectedTask?.currentJob?.id}
                                onCancelJob={(force: boolean) => force ? setShowForceCancelJobConfirmation(true) : setShowCancelJobConfirmation(true)}
                            />
                        </Suspense>
                    </Box>}
                </Box>}
                {tab === 'approvals' && <PipelineApprovals fragmentRef={selectedTask} />}
                {tab === 'outputs' && <PipelineActionOutputs fragmentRef={selectedTask} />}
            </TabContent>
            {jobsDialogOpen && <PipelineJobsDialog
                pipelineId={pipeline.id}
                taskPath={selectedTask?.path as string}
                onJobSelected={onJobSelected}
                onClose={() => setJobsDialogOpen(false)}
            />}
            {showRetryTaskConfirmation && <ConfirmationDialog
                title="Retry Task"
                message={<React.Fragment>Are you sure you want to retry this task?</React.Fragment>}
                confirmButtonLabel="Retry"
                opInProgress={commitRetryTaskInFlight}
                onConfirm={() => onRetryTask(selectedTask.path)}
                onClose={() => setShowRetryTaskConfirmation(false)}
            />}
            {showRunTaskConfirmation && <ConfirmationDialog
                title="Run Task"
                message={<React.Fragment>Are you sure you want to run this task?</React.Fragment>}
                confirmButtonLabel="Run"
                opInProgress={commitRunTaskInFlight}
                onConfirm={() => onRunTask(selectedTask.path)}
                onClose={() => setShowRunTaskConfirmation(false)}
            />}
            {showCancelJobConfirmation && jobId && <ConfirmationDialog
                title="Cancel Job"
                message={<React.Fragment>Are you sure you want to cancel this job?</React.Fragment>}
                confirmButtonLabel="Cancel"
                opInProgress={commitCancelJobInFlight}
                onConfirm={() => onCancelJob(jobId, false)}
                onClose={() => setShowCancelJobConfirmation(false)}
            />}
            {showForceCancelJobConfirmation && jobId && <ConfirmationDialog
                title="Cancel Job"
                message={<React.Fragment>Are you sure you want to force cancel this job?</React.Fragment>}
                confirmButtonLabel="Cancel"
                opInProgress={commitCancelJobInFlight}
                onConfirm={() => onCancelJob(jobId, true)}
                onClose={() => setShowForceCancelJobConfirmation(false)}
            />}
            {showScheduleTaskDialog && <PipelineScheduleNodeDialog
                scheduledStartTime={selectedTask.scheduledStartTime}
                cronSchedule={selectedTask.cronSchedule}
                nodeType="task"
                commitInFlight={commitScheduleTaskInFlight || commitCancelScheduledTaskInFlight}
                error={scheduleTaskError}
                onClose={() => setShowScheduleTaskDialog(false)}
                onSetSchedule={(scheduledTime: Moment | null, cronSchedule: CronSchedule | null) => onScheduleTask(selectedTask.path, scheduledTime, cronSchedule)}
                onClearSchedule={() => onCancelScheduledTask(selectedTask.path)}
            />}
            {showDeferTaskDialog && <PipelineDeferPipelineNodeDialog
                title="Defer Task"
                message={<React.Fragment>Are you sure you want to defer this task?</React.Fragment>}
                onClose={() => setShowDeferTaskDialog(false)}
                onDefer={onDeferTask}
                inProgress={commitDeferTaskInFlight}
            />}
            {showUndeferTaskConfirmation && <ConfirmationDialog
                title="Restore Task"
                message={<React.Fragment>Are you sure you want to restore this deferred task?</React.Fragment>}
                confirmButtonLabel="Restore"
                opInProgress={commitUndeferTaskInFlight}
                onConfirm={() => onUndeferTask(selectedTask.path)}
                onClose={() => setShowUndeferTaskConfirmation(false)}
            />}
        </Box>
    ) : <Box display="flex" justifyContent="center" pt={4}>
        <Typography color="textSecondary">Task {taskPath} not found</Typography>
    </Box>;
}

export default PipelineTaskDetails;
