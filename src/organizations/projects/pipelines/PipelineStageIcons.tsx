import { Box, Tooltip } from '@mui/material';
import PipelineStatusType from './PipelineStatusType';
import { PipelineStageIcons_stages$key } from './__generated__/PipelineStageIcons_stages.graphql';
import { useFragment } from 'react-relay/hooks';
import graphql from "babel-plugin-relay/macro";

interface Props {
    fragmentRef: PipelineStageIcons_stages$key
}

function PipelineStageIcons({ fragmentRef }: Props) {
    const data = useFragment<PipelineStageIcons_stages$key>(graphql`
        fragment PipelineStageIcons_stages on Pipeline
        {
            stages {
                path
                name
                status
            }
        }
    `, fragmentRef);

    return (
        <Box display="flex" flexWrap="wrap">
            {data.stages.map(stage => (
                <Tooltip key={stage.path} title={`stage ${stage.name} ${PipelineStatusType[stage.status]?.tooltip}`}>
                    {PipelineStatusType[stage.status]?.icon}
                </Tooltip>
            ))}
        </Box>
    );
}

export default PipelineStageIcons
