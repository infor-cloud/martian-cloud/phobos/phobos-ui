import NotStartedIcon from '@mui/icons-material/AdjustOutlined';
import CheckCircleIcon from '@mui/icons-material/CheckCircleOutline';
import ErrorIcon from '@mui/icons-material/Error';
import PendingIcon from '@mui/icons-material/PauseCircleOutline';
import WaitingIcon from '@mui/icons-material/ScheduleOutlined';
import InProgressIcon from '@mui/icons-material/TimelapseOutlined';
import WarningIcon from '@mui/icons-material/Warning';
import { blue, green, grey, orange, red, teal, yellow } from '@mui/material/colors';

// update when stage status is added to the pipeline type
export default {
  CREATED: {
    label: 'Blocked',
    color: grey[500],
    icon: <NotStartedIcon sx={{ color: grey[500] }} />,
    tooltip: 'is blocked and cannot be started yet'
  },
  READY: {
    label: 'Not Started',
    color: grey[500],
    icon: <NotStartedIcon sx={{ color: grey[500] }} />,
    tooltip: 'has not started'
  },
  CANCELED: {
    label: 'Canceled',
    color: red[500],
    icon: <WarningIcon sx={{ color: red[500] }} />,
    tooltip: 'was canceled while in progress'
  },
  SKIPPED: {
    label: 'Skipped',
    color: red[500],
    icon: <WarningIcon sx={{ color: yellow[500] }} />,
    tooltip: 'was skipped'
  },
  FAILED: {
    label: 'Failed',
    color: red[500],
    icon: <ErrorIcon sx={{ color: red[500] }} />,
    tooltip: 'returned an error'
  },
  SUCCEEDED: {
    label: 'Succeeded',
    color: green[400],
    icon: <CheckCircleIcon sx={{ color: green[400] }} />,
    tooltip: 'has completed'
  },
  PENDING: {
    label: 'Pending',
    color: orange[500],
    icon: <PendingIcon sx={{ color: orange[500] }} />,
    tooltip: 'is pending'
  },
  APPROVAL_PENDING: {
    label: 'Approval Pending',
    color: 'rgb(255, 255, 255)',
    icon: <NotStartedIcon sx={{ color: grey[500] }} />,
    tooltip: 'is pending approval'
  },
  WAITING: {
    label: 'Waiting',
    color: 'rgb(255, 255, 255)',
    icon: <WaitingIcon sx={{ color: orange[500] }} />,
    tooltip: 'is waiting'
  },
  RUNNING: {
    label: 'In Progress',
    color: blue[500],
    icon: <InProgressIcon sx={{ color: blue[500] }} />,
    tooltip: 'is in progress'
  },
  CANCELING: {
    label: 'Canceling',
    color: grey[500],
    icon: <WarningIcon sx={{ color: grey[500] }} />,
    tooltip: 'is canceling'
  },
  DEFERRED: {
    label: 'Deferred',
    color: teal[500],
    icon: <WarningIcon sx={{ color: teal[500] }} />,
    tooltip: 'was deferred'
  },
} as any;
