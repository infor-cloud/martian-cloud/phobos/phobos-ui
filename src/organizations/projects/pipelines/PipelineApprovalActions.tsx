import ApprovalIcon from '@mui/icons-material/HowToReg';
import LoadingButton from "@mui/lab/LoadingButton";
import { Alert, Box, List, ListItem, ListItemIcon, Paper, Tooltip, Typography } from "@mui/material";
import graphql from 'babel-plugin-relay/macro';
import React, { useContext, useMemo } from "react";
import { useFragment } from "react-relay/hooks";
import { UserContext } from '../../../UserContext';
import Gravatar from "../../../common/Gravatar";
import StyledAvatar from '../../../common/StyledAvatar';
import StackedContainer from '../../../common/StackedContainer';
import { ApprovalRuleIcon } from '../../../common/Icons';
import { PipelineApprovalActionsFragment_approvableNode$key } from "./__generated__/PipelineApprovalActionsFragment_approvableNode.graphql";

interface Props {
    fragmentRef: PipelineApprovalActionsFragment_approvableNode$key;
    approved: boolean;
    commitInFlight: boolean;
    onApprove: () => void;
    onRevoke: () => void;
}

function PipelineApprovalActions({ fragmentRef, approved, commitInFlight, onApprove, onRevoke }: Props) {
    const user = useContext(UserContext);

    const node = useFragment<PipelineApprovalActionsFragment_approvableNode$key>(
        graphql`
            fragment PipelineApprovalActionsFragment_approvableNode on ApprovablePipelineNode
            {
                __typename
                approvals {
                    id
                    approvalRules {
                        id
                    }
                    approver {
                        __typename
                        ...on User {
                            id
                            email
                        }
                        ...on ServiceAccount {
                            id
                            name
                        }
                    }
                }
                approvalRules {
                    id
                    name
                    approvalsRequired
                }
            }
        `, fragmentRef);

    const userAlreadyApproved = useMemo(() => {
        return node?.approvals.map((approval) => approval.approver).find(approver => approver?.__typename === 'User' && approver.id === user.id) ? true : false;
    }, [node]);

    const ruleMap = useMemo(() => {
        return node?.approvalRules.reduce((acc, rule) => {
            acc[rule.id] = rule;
            return acc;
        }, {} as any);
    }, [node.approvalRules]);

    const approvalsRemaningPerRule = useMemo(() => {
        const approvalsRemaining = node?.approvalRules.reduce((acc, rule) => {
            acc[rule.id] = rule.approvalsRequired;
            return acc;
        }, {} as any);
        return node?.approvals.flatMap(approval => approval.approvalRules).reduce((acc, rule) => {
            acc[rule.id] = acc[rule.id] - 1;
            return acc;
        }, approvalsRemaining);
    }, [node]);

    return (
        <Box>
            {!approved && <Alert severity="warning" sx={{ mb: 2 }}>
                This {node.__typename === 'Pipeline' ? 'pipeline' : 'task'} requires approvals
            </Alert>}
            <Paper sx={{ padding: 2, marginBottom: 2, display: 'flex', alignItems: 'center' }} variant="outlined">
                <Box>
                    <List dense sx={{ padding: 0 }}>
                        {node.approvals.length > 0 && <ListItem disableGutters>
                            <Box display="flex" alignItems="center">
                                <ListItemIcon sx={{ minWidth: 42 }}>
                                    <ApprovalIcon />
                                </ListItemIcon>
                                <Typography variant="body2">{node.__typename === 'Pipeline' ? 'Pipeline' : 'Task'} approved by </Typography>
                                <StackedContainer sx={{ ml: 0.5 }}>
                                    {node.approvals.map((approval) => (
                                        <React.Fragment key={approval.id}>
                                            {approval.approver?.__typename === 'User' && <Tooltip title={approval.approver?.email}>
                                                <Box>
                                                    <Gravatar width={24} height={24} email={approval.approver?.email} />
                                                </Box>
                                            </Tooltip>}
                                            {approval.approver?.__typename === 'ServiceAccount' && <StyledAvatar>{approval.approver.name[0].toUpperCase()}</StyledAvatar>}
                                        </React.Fragment>
                                    ))}
                                </StackedContainer>
                            </Box>
                        </ListItem>}
                        {!approved && <React.Fragment>
                            {Object.keys(approvalsRemaningPerRule).filter((ruleId: any) => approvalsRemaningPerRule[ruleId] > 0).map((ruleId: any) => <ListItem key={ruleId} disableGutters>
                                <ListItemIcon sx={{ minWidth: 42 }}>
                                    <ApprovalRuleIcon />
                                </ListItemIcon>
                                <Typography
                                    key={ruleId}
                                    variant="body2"
                                >
                                    Approval rule <strong>{ruleMap[ruleId].name}</strong> requires {approvalsRemaningPerRule[ruleId]} approval{approvalsRemaningPerRule[ruleId] > 1 ? 's' : ''}
                                </Typography>
                            </ListItem>)}
                        </React.Fragment>}
                    </List>
                </Box>
                <Box flex={1} />
                {!approved && <Box>
                    {!userAlreadyApproved && <LoadingButton loading={commitInFlight} variant="outlined" color="secondary" onClick={onApprove}>Approve</LoadingButton>}
                    {userAlreadyApproved && <LoadingButton loading={commitInFlight} variant="outlined" color="warning" onClick={onRevoke}>Revoke Approval</LoadingButton>}
                </Box>}
            </Paper>
        </Box>
    );
}

export default PipelineApprovalActions;
