import CalendarIcon from '@mui/icons-material/CalendarMonthOutlined';
import CopyIcon from '@mui/icons-material/ContentCopy';
import { Alert, Button, Chip, IconButton, Paper, Stack, Tab, Tabs, Tooltip, Typography } from '@mui/material';
import Box from '@mui/material/Box';
import { grey } from '@mui/material/colors';
import graphql from 'babel-plugin-relay/macro';
import { enqueueSnackbar } from 'notistack';
import React, { useState } from 'react';
import { useFragment, useMutation } from 'react-relay/hooks';
import { Link as RouterLink, useNavigate, useOutletContext, useSearchParams } from 'react-router-dom';
import { Prism as SyntaxHighlighter } from 'react-syntax-highlighter';
import { atomDark as prismTheme } from 'react-syntax-highlighter/dist/esm/styles/prism';
import ConfirmationDialog from '../../../common/ConfirmationDialog';
import Gravatar from '../../../common/Gravatar';
import { PipelineTemplateIcon, ReleaseIcon } from '../../../common/Icons';
import PRNButton from '../../../common/PRNButton';
import TabContent from '../../../common/TabContent';
import Timestamp from '../../../common/Timestamp';
import Link from '../../../routes/Link';
import PipelineAnnotations from './PipelineAnnotations';
import PipelineApprovalActions from './PipelineApprovalActions';
import PipelineApprovals from './PipelineApprovals';
import PipelineStages from './PipelineStages';
import { PIPELINE_STATUS_TYPES } from './PipelineStatusChip';
import PipelineVariables from './PipelineVariables';
import ForceCancelPipelineAlert from './ForceCancelPipelineAlert';
import { PipelineDetailsIndexApprovePipelineMutation } from "./__generated__/PipelineDetailsIndexApprovePipelineMutation.graphql";
import { PipelineDetailsIndexCancelPipelineMutation } from './__generated__/PipelineDetailsIndexCancelPipelineMutation.graphql';
import { PipelineDetailsIndexFragment_details$key } from './__generated__/PipelineDetailsIndexFragment_details.graphql';
import { PipelineDetailsIndexRevokePipelineApprovalMutation } from "./__generated__/PipelineDetailsIndexRevokePipelineApprovalMutation.graphql";
import { PipelineDetailsIndexRunPipelineMutation } from './__generated__/PipelineDetailsIndexRunPipelineMutation.graphql';
import PipelineThreadList from './comments/PipelineThreadList';
import { StyledCode } from '../../../common/StyledCode';

export const PIPELINE_TYPE_TEXT = {
    'RUNBOOK': 'Runbook',
    'NESTED': 'Nested',
    'RELEASE_LIFECYCLE': 'Release lifecycle',
    'DEPLOYMENT': 'Deployment'
} as any;

function PipelineDetailsIndex() {
    const [searchParams, setSearchParams] = useSearchParams();
    const navigate = useNavigate();
    const tab = searchParams.get('tab') || 'stages';
    const [showCancelPipelineConfirmation, setShowCancelPipelineConfirmation] = useState(false);
    const [showRunPipelineConfirmation, setShowRunPipelineConfirmation] = useState(false);

    const context = useOutletContext<PipelineDetailsIndexFragment_details$key>();

    const data = useFragment<PipelineDetailsIndexFragment_details$key>(
        graphql`
    fragment PipelineDetailsIndexFragment_details on Pipeline
    {
        id
        status
        createdBy
        type
        environmentName
        environment {
            id
        }
        when
        superseded
        scheduledStartTime
        forceCancelAvailableAt
        cronSchedule {
            expression
            timezone
        }
        metadata {
            createdAt
            prn
        }
        pipelineTemplate {
            id
            name
            versioned
            semanticVersion
            hclData
        }
        variables {
            category
        }
        parentPipeline {
            id
        }
        release {
            id
            semanticVersion
        }
        annotations {
            key
        }
        approvalStatus
        ...PipelineVariablesFragment
        ...PipelineStagesFragment_stages
        ...PipelineVariablesFragment
        ...PipelineThreadListFragment_pipeline
        ...PipelineAnnotationsFragment_pipeline
        ...PipelineApprovalsFragment_approvals
        ...PipelineApprovalActionsFragment_approvableNode
        ...ForceCancelPipelineAlertFragment_pipeline
    }

  `, context);

    const [commitCancelPipeline, commitCancelPipelineInFlight] = useMutation<PipelineDetailsIndexCancelPipelineMutation>(graphql`
        mutation PipelineDetailsIndexCancelPipelineMutation($input: CancelPipelineInput!) {
            cancelPipeline(input: $input) {
                problems {
                    message
                    field
                    type
                }
            }
        }
    `);

    const [commitRunPipeline, commitRunPipelineInFlight] = useMutation<PipelineDetailsIndexRunPipelineMutation>(graphql`
        mutation PipelineDetailsIndexRunPipelineMutation($input: RunPipelineInput!) {
            runPipeline(input: $input) {
                problems {
                    message
                    field
                    type
                }
            }
        }
    `);

    const [commitApprovePipeline, commitApprovePipelineInFlight] = useMutation<PipelineDetailsIndexApprovePipelineMutation>(graphql`
        mutation PipelineDetailsIndexApprovePipelineMutation($input: ApprovePipelineInput!) {
            approvePipeline(input: $input) {
                pipeline {
                    ...PipelineDetailsIndexFragment_details
                }
                problems {
                    message
                    field
                    type
                }
            }
        }
    `);

    const [commitRevokePipelineApproval, commitRevokePipelineApprovalInFlight] = useMutation<PipelineDetailsIndexRevokePipelineApprovalMutation>(graphql`
        mutation PipelineDetailsIndexRevokePipelineApprovalMutation($input: RevokePipelineApprovalInput!) {
            revokePipelineApproval(input: $input) {
                pipeline {
                    ...PipelineDetailsIndexFragment_details
                }
                problems {
                    message
                    field
                    type
                }
            }
        }
    `);

    const onApprovePipeline = () => {
        commitApprovePipeline({
            variables: {
                input: {
                    pipelineId: data.id,
                },
            },
            onCompleted: (data) => handleMutationProblems(data.approvePipeline?.problems),
            onError: handleMutationError
        });
    }

    const onRevokePipelineApproval = () => {
        commitRevokePipelineApproval({
            variables: {
                input: {
                    pipelineId: data.id,
                },
            },
            onCompleted: (data) => handleMutationProblems(data.revokePipelineApproval?.problems),
            onError: handleMutationError
        });
    }

    const handleMutationError = (error: Error, completedCallback?: () => void) => {
        if (completedCallback) {
            completedCallback();
        }
        enqueueSnackbar(`Unexpected error: ${error.message}`, { variant: 'error' });
    }

    const handleMutationProblems = (problems: any, completedCallback?: () => void) => {
        if (completedCallback) {
            completedCallback();
        }
        if (problems && problems.length > 0) {
            enqueueSnackbar(problems.map((problem: any) => problem.message).join('; '), { variant: 'warning' });
        }
    }

    const onCancelPipeline = () => {
        commitCancelPipeline({
            variables: {
                input: {
                    id: data.id
                },
            },
            onCompleted: data => handleMutationProblems(data.cancelPipeline?.problems, () => setShowCancelPipelineConfirmation(false)),
            onError: error => handleMutationError(error, () => setShowCancelPipelineConfirmation(false))
        });
    }

    const onRunPipeline = () => {
        commitRunPipeline({
            variables: {
                input: {
                    id: data.id,
                },
            },
            onCompleted: data => handleMutationProblems(data.runPipeline?.problems, () => setShowRunPipelineConfirmation(false)),
            onError: error => handleMutationError(error, () => setShowRunPipelineConfirmation(false))
        });
    }

    const onTabChange = (event: React.SyntheticEvent, newValue: string) => {
        searchParams.set('tab', newValue);
        setSearchParams(searchParams, { replace: true });
    };

    const onNestedPipelineSelected = (pipelinePath: string) => {
        navigate(`nested_pipeline/${encodeURIComponent(pipelinePath)}`);
    };

    const pipelineStatus = PIPELINE_STATUS_TYPES[data.status] ?? { label: 'Unknown', color: grey[500] };
    const { name, semanticVersion, id, versioned } = data.pipelineTemplate;
    const pipelineTemplateLinkText = versioned
        ? name && name.length > 20 ? `${name}`.substring(0, 20) + '...' : `${name}:${semanticVersion}`
        : `${id.substring(0, 8)}...`;

    return (
        <Box>
            {!data.superseded && data.status === 'CANCELING' && data.forceCancelAvailableAt && <ForceCancelPipelineAlert fragmentRef={data} sx={{ mb: 2 }} />}
            {data.superseded && <Alert severity="info" color='warning' variant='outlined' sx={{ mb: 2 }}>{PIPELINE_TYPE_TEXT[data.type]} pipeline has been superseded</Alert>}
            <Box marginBottom={2}>
                <Box display="flex" alignItems="center" justifyContent="space-between" marginBottom={1}>
                    <Box display="flex" alignItems="center">
                        <Typography variant="h6" mr={2}>Pipeline</Typography>
                        <Chip
                            size="small"
                            variant="outlined"
                            label={pipelineStatus.label}
                            sx={{ color: pipelineStatus.color, borderColor: pipelineStatus.color, fontWeight: 500 }}
                        />
                    </Box>
                    <Stack direction="row" spacing={1}>
                        <PRNButton prn={data.metadata.prn} />
                        {!data.superseded && data.status === 'RUNNING' && <Button variant="outlined" onClick={() => setShowCancelPipelineConfirmation(true)}>Cancel Pipeline</Button>}
                        {!data.superseded && data.status === 'READY' && data.when === 'manual' && <Button variant="outlined" onClick={() => setShowRunPipelineConfirmation(true)}>Run Pipeline</Button>}
                    </Stack>
                </Box>
                <Box display="flex" alignItems="center" mb={1}>
                    <Typography variant="body2" sx={{ marginRight: 1, fontWeight: 500 }}>
                        Created <Timestamp format="relative" component="span" timestamp={data.metadata.createdAt} /> by
                    </Typography>
                    <Tooltip title={data.createdBy}>
                        <Box>
                            <Gravatar width={20} height={20} email={data.createdBy} />
                        </Box>
                    </Tooltip>
                </Box>
                {data.parentPipeline && <Typography variant="body2" color="textSecondary" component="div">
                    Parent pipeline is
                    <Link to={`../${data.parentPipeline.id}`} variant="body2" sx={{ marginLeft: 0.5 }}>
                        {`${data.parentPipeline.id.substring(0, 8)}...`}
                    </Link>
                </Typography>}
                <Box mt={data.annotations.length > 0 ? 2 : 0} mb={2}>
                    <PipelineAnnotations fragmentRef={data} />
                </Box>
                {!data.superseded && (data.scheduledStartTime || data.cronSchedule) &&
                    ['CREATED', 'APPROVAL_PENDING', 'WAITING'].includes(data.status) &&
                    <Paper variant="outlined" sx={{ padding: 2, display: 'flex', alignItems: 'center', marginBottom: 2 }}>
                        <CalendarIcon />
                        {data.scheduledStartTime && <Box ml={1}>
                            <Typography variant="body2">
                                Scheduled to run <Timestamp format="absolute" component="span" timestamp={data.scheduledStartTime} />
                            </Typography>
                            {data.cronSchedule && <Typography component="div" variant="caption" color="textSecondary" mt={0.5}>
                                Scheduled start time was calculated using the cron expression
                                {' '}
                                <StyledCode>{data.cronSchedule.expression}</StyledCode>
                                {' '}
                                in the <strong>{data.cronSchedule.timezone}</strong> timezone
                            </Typography>}
                        </Box>}
                        {!data.scheduledStartTime && data.cronSchedule && <Typography component="div" marginLeft={1} variant="body2">
                            This pipeline will be scheduled based on the cron expression
                            {' '}
                            <StyledCode>{data.cronSchedule.expression}</StyledCode>
                            {' '}
                            in the <strong>{data.cronSchedule.timezone}</strong> timezone
                        </Typography>}
                    </Paper>}
                {data.release && <Paper variant="outlined" sx={{ display: 'flex', alignItems: 'center', p: 2, mb: 2 }}>
                    <ReleaseIcon />
                    <Typography component="div" variant="body2" sx={{ ml: 1 }}>
                        Release
                        {' '}
                        <Link to={`../../-/releases/${data.release.id}`} sx={{ fontWeight: 600 }}>
                            v{data.release.semanticVersion}
                        </Link>
                    </Typography>
                </Paper>}
                <Paper variant="outlined" sx={{ padding: 2, mb: 2 }}>
                    <Typography component="div" variant="body2">
                        {PIPELINE_TYPE_TEXT[data.type]} pipeline created with pipeline template
                        <Link to={`../../-/pipeline_templates/${data.pipelineTemplate.id}`} variant="body2" sx={{ margin: '0 4px', fontWeight: 600 }}>
                            {pipelineTemplateLinkText}
                        </Link>
                        {data.type === 'DEPLOYMENT' && <React.Fragment>
                            {' '}and deployed to environment
                            {
                                data.environment ?
                                    <Link
                                        sx={{ ml: '4px', fontWeight: 600 }}
                                        to={`../../-/environments/${data.environment.id}`}
                                        variant="body2"
                                    >
                                        {data.environmentName}
                                    </Link>
                                    :
                                    <Typography sx={{ ml: '4px', fontWeight: 600 }}
                                        component="span"
                                        variant="body2">{data.environmentName}
                                    </Typography>
                            }
                        </React.Fragment>}
                    </Typography>
                </Paper>
                {(data.status === 'APPROVAL_PENDING' || data.approvalStatus === 'APPROVED') && <PipelineApprovalActions
                    fragmentRef={data}
                    approved={data.approvalStatus === 'APPROVED'}
                    commitInFlight={commitApprovePipelineInFlight || commitRevokePipelineApprovalInFlight}
                    onApprove={() => onApprovePipeline()}
                    onRevoke={() => onRevokePipelineApproval()}
                />}
                <Box sx={{ display: "flex", border: 1, borderColor: 'divider', borderTopLeftRadius: 4, borderTopRightRadius: 4, mb: 2 }}>
                    <Tabs value={tab} onChange={onTabChange}>
                        <Tab label="Stages" value="stages" />
                        <Tab label="Approvals" value="approvals" />
                        <Tab label="Input Variables" value="variables" />
                        <Tab label="Template" value="template" />
                        <Tab label="Comments" value="comments" />
                    </Tabs>
                </Box>
                <TabContent>
                    {tab === 'stages' && <PipelineStages
                        fragmentRef={data}
                        onTaskSelected={(taskPath: string) => navigate(`task/${encodeURIComponent(taskPath)}`)}
                        onNestedPipelineSelected={onNestedPipelineSelected}
                    />}
                    {tab === 'approvals' && <PipelineApprovals fragmentRef={data} />}
                    {tab === 'variables' && <PipelineVariables fragmentRef={data} />}
                    {tab === 'template' && <Box>
                        <Box mt={2} position="relative">
                            <Box sx={{ position: 'absolute', top: 0, right: 0 }} display="flex" alignItems="center">
                                <IconButton component={RouterLink} sx={{ padding: 2 }} to={`../../-/pipeline_templates/${data.pipelineTemplate.id}`}>
                                    <PipelineTemplateIcon sx={{ width: 16, height: 16 }} />
                                </IconButton>
                                <IconButton sx={{ padding: 2 }} onClick={() => navigator.clipboard.writeText(data.pipelineTemplate.hclData)}>
                                    <CopyIcon sx={{ width: 16, height: 16 }} />
                                </IconButton>
                            </Box>
                            <SyntaxHighlighter wrapLines customStyle={{ fontSize: 13, minHeight: '60px' }} language="hcl" style={prismTheme} children={data.pipelineTemplate.hclData} />
                        </Box>
                    </Box>}
                    {tab === 'comments' && <PipelineThreadList fragmentRef={data} />}
                </TabContent>
            </Box>
            {showCancelPipelineConfirmation && <ConfirmationDialog
                title="Cancel Pipeline"
                message={<React.Fragment>Are you sure you want to cancel this pipeline?</React.Fragment>}
                confirmButtonLabel="Cancel"
                opInProgress={commitCancelPipelineInFlight}
                onConfirm={onCancelPipeline}
                onClose={() => setShowCancelPipelineConfirmation(false)}
            />}
            {showRunPipelineConfirmation && <ConfirmationDialog
                title="Run Pipeline"
                message={<React.Fragment>Are you sure you want to run this pipeline?</React.Fragment>}
                confirmButtonLabel="Run"
                opInProgress={commitRunPipelineInFlight}
                onConfirm={onRunPipeline}
                onClose={() => setShowRunPipelineConfirmation(false)}
            />}
        </Box>
    );
}

export default PipelineDetailsIndex;
