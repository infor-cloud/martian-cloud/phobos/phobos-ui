import Box from '@mui/material/Box';
import Paper from '@mui/material/Paper';
import Stack from '@mui/material/Stack';
import Table from '@mui/material/Table';
import TableBody from '@mui/material/TableBody';
import TableCell from '@mui/material/TableCell';
import TableContainer from '@mui/material/TableContainer';
import TableHead from '@mui/material/TableHead';
import TableRow from '@mui/material/TableRow';
import Typography from '@mui/material/Typography';
import graphql from 'babel-plugin-relay/macro';
import React, { useState } from 'react';
import { useFragment } from 'react-relay/hooks';
import SearchInput from '../../../common/SearchInput';
import PipelineActionOutputListItem from './PipelineActionOutputListItem';
import { PipelineActionOutputsFragment_outputs$key } from './__generated__/PipelineActionOutputsFragment_outputs.graphql';

interface Props {
    fragmentRef: PipelineActionOutputsFragment_outputs$key
}

const outputSearchFilter = (search: string) => (output: any) => {
    return output.name.toLowerCase().includes(search);
};

function PipelineActionOutputs(props: Props) {
    const { fragmentRef } = props;

    const data = useFragment<PipelineActionOutputsFragment_outputs$key>(
        graphql`
        fragment PipelineActionOutputsFragment_outputs on PipelineTask
        {
            actions {
                name
                outputs {
                    name
                    ...PipelineActionOutputListItemFragment_output
                }
            }
        }
      `, fragmentRef)

    const [search, setSearch] = useState('');

    const onSearchChange = (event: React.ChangeEvent<HTMLInputElement>) => {
        setSearch(event.target.value.toLowerCase());
    };

    const allOutputs = data.actions.flatMap(action => action.outputs.map((output: any) => ({ ...output, action: action.name })));
    const filteredOutputs = search ? allOutputs.filter(outputSearchFilter(search)) : allOutputs;

    return (
        <Box>
            {allOutputs.length > 0 && <Stack direction="row" spacing={2}>
                <SearchInput
                    fullWidth
                    placeholder="search for outputs"
                    onChange={onSearchChange}
                />
            </Stack>}
            {(filteredOutputs.length === 0 && search !== '') && <Typography sx={{ padding: 2, marginTop: 4 }} align="center" color="textSecondary">
                No outputs matching search <strong>{search}</strong>
            </Typography>}
            {(filteredOutputs.length === 0 && search === '') && <Paper variant="outlined" sx={{ marginTop: 4, display: 'flex', justifyContent: 'center' }}>
                <Box padding={4} display="flex" flexDirection="column" justifyContent="center" alignItems="center">
                    <Typography color="textSecondary" align="center">
                        This task does not have any outputs
                    </Typography>
                </Box>
            </Paper>}
            {filteredOutputs.length > 0 && <TableContainer>
                <Table sx={{ tableLayout: 'fixed' }}>
                    <TableHead>
                        <TableRow>
                            <TableCell>
                                <Typography color="textSecondary">Action</Typography>
                            </TableCell>
                            <TableCell>
                                <Typography color="textSecondary">Name</Typography>
                            </TableCell>
                            <TableCell>
                                <Typography color="textSecondary">Value</Typography>
                            </TableCell>
                        </TableRow>
                    </TableHead>
                    <TableBody>
                        {filteredOutputs.map((o: any) => <PipelineActionOutputListItem
                            key={`${o.action}-${o.name}`}
                            fragmentRef={o}
                            actionName={o.action}
                        />)}
                    </TableBody>
                </Table>
            </TableContainer>}
        </Box>
    );
}

export default PipelineActionOutputs;
