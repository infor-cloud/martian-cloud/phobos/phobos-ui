import { Box, Chip, Link as MuiLink, Stack, TableCell, TableRow, Tooltip, Typography } from '@mui/material';
import graphql from 'babel-plugin-relay/macro';
import humanizeDuration from 'humanize-duration';
import moment from 'moment';
import React from 'react';
import { useFragment } from 'react-relay/hooks';
import Link from '../../../routes/Link';
import JobStatusChip from './JobStatusChip';
import { PipelineTaskJobListItemFragment$key } from './__generated__/PipelineTaskJobListItemFragment.graphql';

interface Props {
    fragmentRef: PipelineTaskJobListItemFragment$key
    onJobSelected: (id: string) => void
}

function PipelineTaskJobListItem({ fragmentRef, onJobSelected }: Props) {
    const data = useFragment(graphql`
        fragment PipelineTaskJobListItemFragment on Job {
            id
            status
            type
            timestamps {
                queuedAt
                pendingAt
                runningAt
                finishedAt
            }
            project {
                organizationName
            }
            data {
                ... on JobTaskData {
                    pipelineId
                    taskPath
                }
            }
            tags
            agentName
            agentType
            agent {
                id
            }
            metadata {
                prn
                createdAt
                updatedAt
            }
        }
    `, fragmentRef);

    const timestamps = data.timestamps;
    const duration = timestamps?.finishedAt ?
        moment.duration(moment(timestamps.finishedAt as moment.MomentInput).diff(moment(timestamps.runningAt as moment.MomentInput))) : null;

    return (
        <TableRow sx={{ '&:last-child td, &:last-child th': { border: 0 } }}>
            <TableCell>
                <JobStatusChip status={data.status} onClick={() => onJobSelected(data.id)} />
            </TableCell>
            <TableCell>
                <MuiLink sx={{ cursor: 'pointer' }} color="textPrimary" underline="hover" onClick={() => onJobSelected(data.id)}>{data.id.substring(0, 8)}...</MuiLink>
            </TableCell>
            <TableCell>
                {data.tags.length > 0 && <Stack direction="row" spacing={1}>
                    {data.tags.map(tag => <Chip key={tag} size="small" color="secondary" label={tag} />)}
                </Stack>}
                {data.tags.length === 0 && <Typography variant="body2" color="textSecondary">None</Typography>}
            </TableCell>
            <TableCell>
                {data.agent && <Link
                    color="primary"
                    sx={{ fontWeight: 500 }}
                    to={data.agentType === 'SHARED' ? `/admin/agents/${data.agent.id}` : `/organizations/${data.project.organizationName}/-/agents/${data.agent.id}`}
                >
                    {data.agentName}
                </Link>}
                {!data.agent && data.agentName && <React.Fragment>{data.agentName} (deleted)</React.Fragment>}
                {!data.agentName && <React.Fragment>--</React.Fragment>}
                {data.timestamps.pendingAt && <Typography component="div" variant="caption">claimed job {moment(data.timestamps.pendingAt as moment.MomentInput).fromNow()}</Typography>}
            </TableCell>
            <TableCell>
                {duration ? humanizeDuration(duration.asMilliseconds()) : '--'}
            </TableCell>
            <TableCell>
                <Tooltip title={data.metadata.createdAt}>
                    <Box>{moment(data.metadata.createdAt as moment.MomentInput).fromNow()}</Box>
                </Tooltip>
            </TableCell>
        </TableRow >
    );
}

export default PipelineTaskJobListItem;
