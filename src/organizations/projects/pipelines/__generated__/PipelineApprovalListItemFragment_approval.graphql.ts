/**
 * @generated SignedSource<<bec26f73813a49d035332f0a0d37f1d2>>
 * @lightSyntaxTransform
 * @nogrep
 */

/* tslint:disable */
/* eslint-disable */
// @ts-nocheck

import { Fragment, ReaderFragment } from 'relay-runtime';
import { FragmentRefs } from "relay-runtime";
export type PipelineApprovalListItemFragment_approval$data = {
  readonly approvalRules: ReadonlyArray<{
    readonly name: string;
  }>;
  readonly approver: {
    readonly __typename: "ServiceAccount";
    readonly name: string;
  } | {
    readonly __typename: "User";
    readonly email: string;
    readonly username: string;
  } | {
    // This will never be '%other', but we need some
    // value in case none of the concrete values match.
    readonly __typename: "%other";
  } | null | undefined;
  readonly id: string;
  readonly metadata: {
    readonly createdAt: any;
  };
  readonly " $fragmentType": "PipelineApprovalListItemFragment_approval";
};
export type PipelineApprovalListItemFragment_approval$key = {
  readonly " $data"?: PipelineApprovalListItemFragment_approval$data;
  readonly " $fragmentSpreads": FragmentRefs<"PipelineApprovalListItemFragment_approval">;
};

const node: ReaderFragment = (function(){
var v0 = [
  {
    "alias": null,
    "args": null,
    "kind": "ScalarField",
    "name": "name",
    "storageKey": null
  }
];
return {
  "argumentDefinitions": [],
  "kind": "Fragment",
  "metadata": null,
  "name": "PipelineApprovalListItemFragment_approval",
  "selections": [
    {
      "alias": null,
      "args": null,
      "concreteType": "ResourceMetadata",
      "kind": "LinkedField",
      "name": "metadata",
      "plural": false,
      "selections": [
        {
          "alias": null,
          "args": null,
          "kind": "ScalarField",
          "name": "createdAt",
          "storageKey": null
        }
      ],
      "storageKey": null
    },
    {
      "alias": null,
      "args": null,
      "kind": "ScalarField",
      "name": "id",
      "storageKey": null
    },
    {
      "alias": null,
      "args": null,
      "concreteType": null,
      "kind": "LinkedField",
      "name": "approver",
      "plural": false,
      "selections": [
        {
          "alias": null,
          "args": null,
          "kind": "ScalarField",
          "name": "__typename",
          "storageKey": null
        },
        {
          "kind": "InlineFragment",
          "selections": [
            {
              "alias": null,
              "args": null,
              "kind": "ScalarField",
              "name": "username",
              "storageKey": null
            },
            {
              "alias": null,
              "args": null,
              "kind": "ScalarField",
              "name": "email",
              "storageKey": null
            }
          ],
          "type": "User",
          "abstractKey": null
        },
        {
          "kind": "InlineFragment",
          "selections": (v0/*: any*/),
          "type": "ServiceAccount",
          "abstractKey": null
        }
      ],
      "storageKey": null
    },
    {
      "alias": null,
      "args": null,
      "concreteType": "ApprovalRule",
      "kind": "LinkedField",
      "name": "approvalRules",
      "plural": true,
      "selections": (v0/*: any*/),
      "storageKey": null
    }
  ],
  "type": "PipelineApproval",
  "abstractKey": null
};
})();

(node as any).hash = "dd3524cbbd67dae7b635d1aeffa03de6";

export default node;
