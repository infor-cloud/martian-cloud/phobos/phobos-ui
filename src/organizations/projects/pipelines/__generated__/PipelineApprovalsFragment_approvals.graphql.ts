/**
 * @generated SignedSource<<085ae2cbf493ebf04810dbccea129f33>>
 * @lightSyntaxTransform
 * @nogrep
 */

/* tslint:disable */
/* eslint-disable */
// @ts-nocheck

import { Fragment, ReaderFragment } from 'relay-runtime';
import { FragmentRefs } from "relay-runtime";
export type PipelineApprovalsFragment_approvals$data = {
  readonly __typename: string;
  readonly approvals: ReadonlyArray<{
    readonly id: string;
    readonly " $fragmentSpreads": FragmentRefs<"PipelineApprovalListItemFragment_approval">;
  }>;
  readonly " $fragmentType": "PipelineApprovalsFragment_approvals";
};
export type PipelineApprovalsFragment_approvals$key = {
  readonly " $data"?: PipelineApprovalsFragment_approvals$data;
  readonly " $fragmentSpreads": FragmentRefs<"PipelineApprovalsFragment_approvals">;
};

const node: ReaderFragment = {
  "argumentDefinitions": [],
  "kind": "Fragment",
  "metadata": null,
  "name": "PipelineApprovalsFragment_approvals",
  "selections": [
    {
      "alias": null,
      "args": null,
      "kind": "ScalarField",
      "name": "__typename",
      "storageKey": null
    },
    {
      "alias": null,
      "args": null,
      "concreteType": "PipelineApproval",
      "kind": "LinkedField",
      "name": "approvals",
      "plural": true,
      "selections": [
        {
          "alias": null,
          "args": null,
          "kind": "ScalarField",
          "name": "id",
          "storageKey": null
        },
        {
          "args": null,
          "kind": "FragmentSpread",
          "name": "PipelineApprovalListItemFragment_approval"
        }
      ],
      "storageKey": null
    }
  ],
  "type": "ApprovablePipelineNode",
  "abstractKey": "__isApprovablePipelineNode"
};

(node as any).hash = "82c0cb9e9799d62a6b6fb677e672ed34";

export default node;
