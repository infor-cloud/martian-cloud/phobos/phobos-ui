/**
 * @generated SignedSource<<503485ae7264e741560a1eaba4cfa5a4>>
 * @lightSyntaxTransform
 * @nogrep
 */

/* tslint:disable */
/* eslint-disable */
// @ts-nocheck

import { Fragment, ReaderFragment } from 'relay-runtime';
import { FragmentRefs } from "relay-runtime";
export type ReplyListItemFragment_reply$data = {
  readonly id: string;
  readonly " $fragmentSpreads": FragmentRefs<"CommentDetailsFragment_comment">;
  readonly " $fragmentType": "ReplyListItemFragment_reply";
};
export type ReplyListItemFragment_reply$key = {
  readonly " $data"?: ReplyListItemFragment_reply$data;
  readonly " $fragmentSpreads": FragmentRefs<"ReplyListItemFragment_reply">;
};

const node: ReaderFragment = {
  "argumentDefinitions": [],
  "kind": "Fragment",
  "metadata": null,
  "name": "ReplyListItemFragment_reply",
  "selections": [
    {
      "alias": null,
      "args": null,
      "kind": "ScalarField",
      "name": "id",
      "storageKey": null
    },
    {
      "args": null,
      "kind": "FragmentSpread",
      "name": "CommentDetailsFragment_comment"
    }
  ],
  "type": "Comment",
  "abstractKey": null
};

(node as any).hash = "0fdf4f1a13d45f8f7214d60a050877a2";

export default node;
