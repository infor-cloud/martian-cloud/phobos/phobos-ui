/**
 * @generated SignedSource<<638f3a7ce4a87eb31274e0d589cf1afd>>
 * @lightSyntaxTransform
 * @nogrep
 */

/* tslint:disable */
/* eslint-disable */
// @ts-nocheck

import { ConcreteRequest, Mutation } from 'relay-runtime';
import { FragmentRefs } from "relay-runtime";
export type ProblemType = "BAD_REQUEST" | "CONFLICT" | "FORBIDDEN" | "NOT_FOUND" | "%future added value";
export type RevokePipelineApprovalInput = {
  clientMutationId?: string | null | undefined;
  pipelineId: string;
};
export type PipelineDetailsIndexRevokePipelineApprovalMutation$variables = {
  input: RevokePipelineApprovalInput;
};
export type PipelineDetailsIndexRevokePipelineApprovalMutation$data = {
  readonly revokePipelineApproval: {
    readonly pipeline: {
      readonly " $fragmentSpreads": FragmentRefs<"PipelineDetailsIndexFragment_details">;
    } | null | undefined;
    readonly problems: ReadonlyArray<{
      readonly field: ReadonlyArray<string> | null | undefined;
      readonly message: string;
      readonly type: ProblemType;
    }>;
  };
};
export type PipelineDetailsIndexRevokePipelineApprovalMutation = {
  response: PipelineDetailsIndexRevokePipelineApprovalMutation$data;
  variables: PipelineDetailsIndexRevokePipelineApprovalMutation$variables;
};

const node: ConcreteRequest = (function(){
var v0 = [
  {
    "defaultValue": null,
    "kind": "LocalArgument",
    "name": "input"
  }
],
v1 = [
  {
    "kind": "Variable",
    "name": "input",
    "variableName": "input"
  }
],
v2 = {
  "alias": null,
  "args": null,
  "kind": "ScalarField",
  "name": "type",
  "storageKey": null
},
v3 = {
  "alias": null,
  "args": null,
  "concreteType": "Problem",
  "kind": "LinkedField",
  "name": "problems",
  "plural": true,
  "selections": [
    {
      "alias": null,
      "args": null,
      "kind": "ScalarField",
      "name": "message",
      "storageKey": null
    },
    {
      "alias": null,
      "args": null,
      "kind": "ScalarField",
      "name": "field",
      "storageKey": null
    },
    (v2/*: any*/)
  ],
  "storageKey": null
},
v4 = {
  "alias": null,
  "args": null,
  "kind": "ScalarField",
  "name": "id",
  "storageKey": null
},
v5 = {
  "alias": null,
  "args": null,
  "kind": "ScalarField",
  "name": "status",
  "storageKey": null
},
v6 = [
  (v4/*: any*/)
],
v7 = {
  "alias": null,
  "args": null,
  "kind": "ScalarField",
  "name": "when",
  "storageKey": null
},
v8 = {
  "alias": null,
  "args": null,
  "kind": "ScalarField",
  "name": "createdAt",
  "storageKey": null
},
v9 = {
  "alias": null,
  "args": null,
  "kind": "ScalarField",
  "name": "name",
  "storageKey": null
},
v10 = {
  "alias": null,
  "args": null,
  "kind": "ScalarField",
  "name": "semanticVersion",
  "storageKey": null
},
v11 = {
  "alias": null,
  "args": null,
  "kind": "ScalarField",
  "name": "key",
  "storageKey": null
},
v12 = {
  "alias": null,
  "args": null,
  "kind": "ScalarField",
  "name": "value",
  "storageKey": null
},
v13 = {
  "alias": null,
  "args": null,
  "kind": "ScalarField",
  "name": "path",
  "storageKey": null
},
v14 = [
  (v13/*: any*/),
  (v9/*: any*/),
  (v5/*: any*/),
  (v7/*: any*/),
  {
    "alias": null,
    "args": null,
    "kind": "ScalarField",
    "name": "dependencies",
    "storageKey": null
  }
],
v15 = {
  "alias": null,
  "args": null,
  "kind": "ScalarField",
  "name": "__typename",
  "storageKey": null
},
v16 = [
  (v9/*: any*/),
  (v4/*: any*/)
];
return {
  "fragment": {
    "argumentDefinitions": (v0/*: any*/),
    "kind": "Fragment",
    "metadata": null,
    "name": "PipelineDetailsIndexRevokePipelineApprovalMutation",
    "selections": [
      {
        "alias": null,
        "args": (v1/*: any*/),
        "concreteType": "PipelineMutationPayload",
        "kind": "LinkedField",
        "name": "revokePipelineApproval",
        "plural": false,
        "selections": [
          {
            "alias": null,
            "args": null,
            "concreteType": "Pipeline",
            "kind": "LinkedField",
            "name": "pipeline",
            "plural": false,
            "selections": [
              {
                "args": null,
                "kind": "FragmentSpread",
                "name": "PipelineDetailsIndexFragment_details"
              }
            ],
            "storageKey": null
          },
          (v3/*: any*/)
        ],
        "storageKey": null
      }
    ],
    "type": "Mutation",
    "abstractKey": null
  },
  "kind": "Request",
  "operation": {
    "argumentDefinitions": (v0/*: any*/),
    "kind": "Operation",
    "name": "PipelineDetailsIndexRevokePipelineApprovalMutation",
    "selections": [
      {
        "alias": null,
        "args": (v1/*: any*/),
        "concreteType": "PipelineMutationPayload",
        "kind": "LinkedField",
        "name": "revokePipelineApproval",
        "plural": false,
        "selections": [
          {
            "alias": null,
            "args": null,
            "concreteType": "Pipeline",
            "kind": "LinkedField",
            "name": "pipeline",
            "plural": false,
            "selections": [
              (v4/*: any*/),
              (v5/*: any*/),
              {
                "alias": null,
                "args": null,
                "kind": "ScalarField",
                "name": "createdBy",
                "storageKey": null
              },
              (v2/*: any*/),
              {
                "alias": null,
                "args": null,
                "kind": "ScalarField",
                "name": "environmentName",
                "storageKey": null
              },
              {
                "alias": null,
                "args": null,
                "concreteType": "Environment",
                "kind": "LinkedField",
                "name": "environment",
                "plural": false,
                "selections": (v6/*: any*/),
                "storageKey": null
              },
              (v7/*: any*/),
              {
                "alias": null,
                "args": null,
                "kind": "ScalarField",
                "name": "superseded",
                "storageKey": null
              },
              {
                "alias": null,
                "args": null,
                "kind": "ScalarField",
                "name": "scheduledStartTime",
                "storageKey": null
              },
              {
                "alias": null,
                "args": null,
                "kind": "ScalarField",
                "name": "forceCancelAvailableAt",
                "storageKey": null
              },
              {
                "alias": null,
                "args": null,
                "concreteType": "PipelineCronSchedule",
                "kind": "LinkedField",
                "name": "cronSchedule",
                "plural": false,
                "selections": [
                  {
                    "alias": null,
                    "args": null,
                    "kind": "ScalarField",
                    "name": "expression",
                    "storageKey": null
                  },
                  {
                    "alias": null,
                    "args": null,
                    "kind": "ScalarField",
                    "name": "timezone",
                    "storageKey": null
                  }
                ],
                "storageKey": null
              },
              {
                "alias": null,
                "args": null,
                "concreteType": "ResourceMetadata",
                "kind": "LinkedField",
                "name": "metadata",
                "plural": false,
                "selections": [
                  (v8/*: any*/),
                  {
                    "alias": null,
                    "args": null,
                    "kind": "ScalarField",
                    "name": "prn",
                    "storageKey": null
                  }
                ],
                "storageKey": null
              },
              {
                "alias": null,
                "args": null,
                "concreteType": "PipelineTemplate",
                "kind": "LinkedField",
                "name": "pipelineTemplate",
                "plural": false,
                "selections": [
                  (v4/*: any*/),
                  (v9/*: any*/),
                  {
                    "alias": null,
                    "args": null,
                    "kind": "ScalarField",
                    "name": "versioned",
                    "storageKey": null
                  },
                  (v10/*: any*/),
                  {
                    "alias": null,
                    "args": null,
                    "kind": "ScalarField",
                    "name": "hclData",
                    "storageKey": null
                  }
                ],
                "storageKey": null
              },
              {
                "alias": null,
                "args": null,
                "concreteType": "PipelineVariable",
                "kind": "LinkedField",
                "name": "variables",
                "plural": true,
                "selections": [
                  {
                    "alias": null,
                    "args": null,
                    "kind": "ScalarField",
                    "name": "category",
                    "storageKey": null
                  },
                  (v11/*: any*/),
                  (v12/*: any*/)
                ],
                "storageKey": null
              },
              {
                "alias": null,
                "args": null,
                "concreteType": "Pipeline",
                "kind": "LinkedField",
                "name": "parentPipeline",
                "plural": false,
                "selections": (v6/*: any*/),
                "storageKey": null
              },
              {
                "alias": null,
                "args": null,
                "concreteType": "Release",
                "kind": "LinkedField",
                "name": "release",
                "plural": false,
                "selections": [
                  (v4/*: any*/),
                  (v10/*: any*/)
                ],
                "storageKey": null
              },
              {
                "alias": null,
                "args": null,
                "concreteType": "PipelineAnnotation",
                "kind": "LinkedField",
                "name": "annotations",
                "plural": true,
                "selections": [
                  (v11/*: any*/),
                  (v12/*: any*/),
                  {
                    "alias": null,
                    "args": null,
                    "kind": "ScalarField",
                    "name": "link",
                    "storageKey": null
                  }
                ],
                "storageKey": null
              },
              {
                "alias": null,
                "args": null,
                "kind": "ScalarField",
                "name": "approvalStatus",
                "storageKey": null
              },
              {
                "alias": null,
                "args": null,
                "concreteType": "PipelineStage",
                "kind": "LinkedField",
                "name": "stages",
                "plural": true,
                "selections": [
                  (v13/*: any*/),
                  (v9/*: any*/),
                  (v5/*: any*/),
                  {
                    "alias": null,
                    "args": null,
                    "concreteType": "PipelineTask",
                    "kind": "LinkedField",
                    "name": "tasks",
                    "plural": true,
                    "selections": (v14/*: any*/),
                    "storageKey": null
                  },
                  {
                    "alias": null,
                    "args": null,
                    "concreteType": "NestedPipeline",
                    "kind": "LinkedField",
                    "name": "nestedPipelines",
                    "plural": true,
                    "selections": (v14/*: any*/),
                    "storageKey": null
                  }
                ],
                "storageKey": null
              },
              {
                "kind": "InlineFragment",
                "selections": [
                  (v15/*: any*/),
                  {
                    "alias": null,
                    "args": null,
                    "concreteType": "PipelineApproval",
                    "kind": "LinkedField",
                    "name": "approvals",
                    "plural": true,
                    "selections": [
                      (v4/*: any*/),
                      {
                        "alias": null,
                        "args": null,
                        "concreteType": "ResourceMetadata",
                        "kind": "LinkedField",
                        "name": "metadata",
                        "plural": false,
                        "selections": [
                          (v8/*: any*/)
                        ],
                        "storageKey": null
                      },
                      {
                        "alias": null,
                        "args": null,
                        "concreteType": null,
                        "kind": "LinkedField",
                        "name": "approver",
                        "plural": false,
                        "selections": [
                          (v15/*: any*/),
                          {
                            "kind": "InlineFragment",
                            "selections": [
                              {
                                "alias": null,
                                "args": null,
                                "kind": "ScalarField",
                                "name": "username",
                                "storageKey": null
                              },
                              {
                                "alias": null,
                                "args": null,
                                "kind": "ScalarField",
                                "name": "email",
                                "storageKey": null
                              },
                              (v4/*: any*/)
                            ],
                            "type": "User",
                            "abstractKey": null
                          },
                          {
                            "kind": "InlineFragment",
                            "selections": (v16/*: any*/),
                            "type": "ServiceAccount",
                            "abstractKey": null
                          },
                          {
                            "kind": "InlineFragment",
                            "selections": (v6/*: any*/),
                            "type": "Node",
                            "abstractKey": "__isNode"
                          }
                        ],
                        "storageKey": null
                      },
                      {
                        "alias": null,
                        "args": null,
                        "concreteType": "ApprovalRule",
                        "kind": "LinkedField",
                        "name": "approvalRules",
                        "plural": true,
                        "selections": (v16/*: any*/),
                        "storageKey": null
                      }
                    ],
                    "storageKey": null
                  },
                  {
                    "alias": null,
                    "args": null,
                    "concreteType": "ApprovalRule",
                    "kind": "LinkedField",
                    "name": "approvalRules",
                    "plural": true,
                    "selections": [
                      (v4/*: any*/),
                      (v9/*: any*/),
                      {
                        "alias": null,
                        "args": null,
                        "kind": "ScalarField",
                        "name": "approvalsRequired",
                        "storageKey": null
                      }
                    ],
                    "storageKey": null
                  }
                ],
                "type": "ApprovablePipelineNode",
                "abstractKey": "__isApprovablePipelineNode"
              },
              {
                "alias": null,
                "args": null,
                "concreteType": "Project",
                "kind": "LinkedField",
                "name": "project",
                "plural": false,
                "selections": [
                  (v9/*: any*/),
                  {
                    "alias": null,
                    "args": null,
                    "kind": "ScalarField",
                    "name": "organizationName",
                    "storageKey": null
                  },
                  (v4/*: any*/)
                ],
                "storageKey": null
              }
            ],
            "storageKey": null
          },
          (v3/*: any*/)
        ],
        "storageKey": null
      }
    ]
  },
  "params": {
    "cacheID": "25c9bcd422e5d533afe95907dfc63034",
    "id": null,
    "metadata": {},
    "name": "PipelineDetailsIndexRevokePipelineApprovalMutation",
    "operationKind": "mutation",
    "text": "mutation PipelineDetailsIndexRevokePipelineApprovalMutation(\n  $input: RevokePipelineApprovalInput!\n) {\n  revokePipelineApproval(input: $input) {\n    pipeline {\n      ...PipelineDetailsIndexFragment_details\n      id\n    }\n    problems {\n      message\n      field\n      type\n    }\n  }\n}\n\nfragment ForceCancelPipelineAlertFragment_pipeline on Pipeline {\n  forceCancelAvailableAt\n  ...ForceCancelPipelineButtonFragment_pipeline\n}\n\nfragment ForceCancelPipelineButtonDialogFragment_pipeline on Pipeline {\n  project {\n    name\n    organizationName\n    id\n  }\n}\n\nfragment ForceCancelPipelineButtonFragment_pipeline on Pipeline {\n  id\n  ...ForceCancelPipelineButtonDialogFragment_pipeline\n}\n\nfragment PipelineAnnotationsFragment_pipeline on Pipeline {\n  annotations {\n    key\n    value\n    link\n  }\n}\n\nfragment PipelineApprovalActionsFragment_approvableNode on ApprovablePipelineNode {\n  __isApprovablePipelineNode: __typename\n  __typename\n  approvals {\n    id\n    approvalRules {\n      id\n    }\n    approver {\n      __typename\n      ... on User {\n        id\n        email\n      }\n      ... on ServiceAccount {\n        id\n        name\n      }\n      ... on Node {\n        __isNode: __typename\n        id\n      }\n    }\n  }\n  approvalRules {\n    id\n    name\n    approvalsRequired\n  }\n}\n\nfragment PipelineApprovalListItemFragment_approval on PipelineApproval {\n  metadata {\n    createdAt\n  }\n  id\n  approver {\n    __typename\n    ... on User {\n      username\n      email\n    }\n    ... on ServiceAccount {\n      name\n    }\n    ... on Node {\n      __isNode: __typename\n      id\n    }\n  }\n  approvalRules {\n    name\n    id\n  }\n}\n\nfragment PipelineApprovalsFragment_approvals on ApprovablePipelineNode {\n  __isApprovablePipelineNode: __typename\n  __typename\n  approvals {\n    id\n    ...PipelineApprovalListItemFragment_approval\n  }\n}\n\nfragment PipelineDetailsIndexFragment_details on Pipeline {\n  id\n  status\n  createdBy\n  type\n  environmentName\n  environment {\n    id\n  }\n  when\n  superseded\n  scheduledStartTime\n  forceCancelAvailableAt\n  cronSchedule {\n    expression\n    timezone\n  }\n  metadata {\n    createdAt\n    prn\n  }\n  pipelineTemplate {\n    id\n    name\n    versioned\n    semanticVersion\n    hclData\n  }\n  variables {\n    category\n  }\n  parentPipeline {\n    id\n  }\n  release {\n    id\n    semanticVersion\n  }\n  annotations {\n    key\n  }\n  approvalStatus\n  ...PipelineVariablesFragment\n  ...PipelineStagesFragment_stages\n  ...PipelineThreadListFragment_pipeline\n  ...PipelineAnnotationsFragment_pipeline\n  ...PipelineApprovalsFragment_approvals\n  ...PipelineApprovalActionsFragment_approvableNode\n  ...ForceCancelPipelineAlertFragment_pipeline\n}\n\nfragment PipelineStagesFragment_stages on Pipeline {\n  stages {\n    path\n    name\n    status\n    tasks {\n      path\n      name\n      status\n      when\n      dependencies\n    }\n    nestedPipelines {\n      path\n      name\n      status\n      when\n      dependencies\n    }\n  }\n}\n\nfragment PipelineThreadListFragment_pipeline on Pipeline {\n  id\n}\n\nfragment PipelineVariableListItemFragment_variable on PipelineVariable {\n  key\n  value\n  category\n}\n\nfragment PipelineVariablesFragment on Pipeline {\n  id\n  variables {\n    key\n    value\n    category\n    ...PipelineVariableListItemFragment_variable\n  }\n}\n"
  }
};
})();

(node as any).hash = "df4a16f9e837295b0d701cf053ceac07";

export default node;
