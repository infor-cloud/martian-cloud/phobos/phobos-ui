/**
 * @generated SignedSource<<9c36403ee177d5121cfa60db566ce265>>
 * @lightSyntaxTransform
 * @nogrep
 */

/* tslint:disable */
/* eslint-disable */
// @ts-nocheck

import { ReaderFragment, RefetchableFragment } from 'relay-runtime';
import { FragmentRefs } from "relay-runtime";
export type PipelineNestedPipelineListFragment_pipelines$data = {
  readonly pipelineNode: {
    readonly pipelines?: {
      readonly edges: ReadonlyArray<{
        readonly node: {
          readonly id: string;
          readonly " $fragmentSpreads": FragmentRefs<"PipelineNestedPipelineListItemFragment">;
        } | null | undefined;
      } | null | undefined> | null | undefined;
      readonly totalCount: number;
    };
  } | null | undefined;
  readonly " $fragmentType": "PipelineNestedPipelineListFragment_pipelines";
};
export type PipelineNestedPipelineListFragment_pipelines$key = {
  readonly " $data"?: PipelineNestedPipelineListFragment_pipelines$data;
  readonly " $fragmentSpreads": FragmentRefs<"PipelineNestedPipelineListFragment_pipelines">;
};

const node: ReaderFragment = (function(){
var v0 = [
  "pipelineNode",
  "pipelines"
];
return {
  "argumentDefinitions": [
    {
      "kind": "RootArgument",
      "name": "after"
    },
    {
      "kind": "RootArgument",
      "name": "first"
    },
    {
      "kind": "RootArgument",
      "name": "nestedPipelinePath"
    },
    {
      "kind": "RootArgument",
      "name": "pipelineId"
    }
  ],
  "kind": "Fragment",
  "metadata": {
    "connection": [
      {
        "count": "first",
        "cursor": "after",
        "direction": "forward",
        "path": (v0/*: any*/)
      }
    ],
    "refetch": {
      "connection": {
        "forward": {
          "count": "first",
          "cursor": "after"
        },
        "backward": null,
        "path": (v0/*: any*/)
      },
      "fragmentPathInResult": [],
      "operation": require('./PipelineNestedPipelineListPaginationQuery.graphql')
    }
  },
  "name": "PipelineNestedPipelineListFragment_pipelines",
  "selections": [
    {
      "alias": null,
      "args": [
        {
          "kind": "Variable",
          "name": "nodePath",
          "variableName": "nestedPipelinePath"
        },
        {
          "kind": "Variable",
          "name": "pipelineId",
          "variableName": "pipelineId"
        }
      ],
      "concreteType": null,
      "kind": "LinkedField",
      "name": "pipelineNode",
      "plural": false,
      "selections": [
        {
          "kind": "InlineFragment",
          "selections": [
            {
              "alias": "pipelines",
              "args": [
                {
                  "kind": "Literal",
                  "name": "sort",
                  "value": "CREATED_AT_DESC"
                }
              ],
              "concreteType": "PipelineConnection",
              "kind": "LinkedField",
              "name": "__PipelineNestedPipelineList_pipelines_connection",
              "plural": false,
              "selections": [
                {
                  "alias": null,
                  "args": null,
                  "kind": "ScalarField",
                  "name": "totalCount",
                  "storageKey": null
                },
                {
                  "alias": null,
                  "args": null,
                  "concreteType": "PipelineEdge",
                  "kind": "LinkedField",
                  "name": "edges",
                  "plural": true,
                  "selections": [
                    {
                      "alias": null,
                      "args": null,
                      "concreteType": "Pipeline",
                      "kind": "LinkedField",
                      "name": "node",
                      "plural": false,
                      "selections": [
                        {
                          "alias": null,
                          "args": null,
                          "kind": "ScalarField",
                          "name": "id",
                          "storageKey": null
                        },
                        {
                          "args": null,
                          "kind": "FragmentSpread",
                          "name": "PipelineNestedPipelineListItemFragment"
                        },
                        {
                          "alias": null,
                          "args": null,
                          "kind": "ScalarField",
                          "name": "__typename",
                          "storageKey": null
                        }
                      ],
                      "storageKey": null
                    },
                    {
                      "alias": null,
                      "args": null,
                      "kind": "ScalarField",
                      "name": "cursor",
                      "storageKey": null
                    }
                  ],
                  "storageKey": null
                },
                {
                  "alias": null,
                  "args": null,
                  "concreteType": "PageInfo",
                  "kind": "LinkedField",
                  "name": "pageInfo",
                  "plural": false,
                  "selections": [
                    {
                      "alias": null,
                      "args": null,
                      "kind": "ScalarField",
                      "name": "endCursor",
                      "storageKey": null
                    },
                    {
                      "alias": null,
                      "args": null,
                      "kind": "ScalarField",
                      "name": "hasNextPage",
                      "storageKey": null
                    }
                  ],
                  "storageKey": null
                }
              ],
              "storageKey": "__PipelineNestedPipelineList_pipelines_connection(sort:\"CREATED_AT_DESC\")"
            }
          ],
          "type": "NestedPipeline",
          "abstractKey": null
        }
      ],
      "storageKey": null
    }
  ],
  "type": "Query",
  "abstractKey": null
};
})();

(node as any).hash = "225529576563fd657d2ef28e9486a576";

export default node;
