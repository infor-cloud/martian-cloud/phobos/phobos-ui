/**
 * @generated SignedSource<<4d85ab20c3232ea860a1d5411e3119bd>>
 * @lightSyntaxTransform
 * @nogrep
 */

/* tslint:disable */
/* eslint-disable */
// @ts-nocheck

import { ConcreteRequest, GraphQLSubscription } from 'relay-runtime';
import { FragmentRefs } from "relay-runtime";
export type PipelineEventsSubscriptionInput = {
  lastSeenVersion?: string | null | undefined;
  pipelineId: string;
};
export type PipelineDetailsSubscription$variables = {
  input: PipelineEventsSubscriptionInput;
};
export type PipelineDetailsSubscription$data = {
  readonly pipelineEvents: {
    readonly action: string;
    readonly pipeline: {
      readonly id: string;
      readonly " $fragmentSpreads": FragmentRefs<"PipelineDetailsFragment_pipeline">;
    };
  };
};
export type PipelineDetailsSubscription = {
  response: PipelineDetailsSubscription$data;
  variables: PipelineDetailsSubscription$variables;
};

const node: ConcreteRequest = (function(){
var v0 = [
  {
    "defaultValue": null,
    "kind": "LocalArgument",
    "name": "input"
  }
],
v1 = [
  {
    "kind": "Variable",
    "name": "input",
    "variableName": "input"
  }
],
v2 = {
  "alias": null,
  "args": null,
  "kind": "ScalarField",
  "name": "action",
  "storageKey": null
},
v3 = {
  "alias": null,
  "args": null,
  "kind": "ScalarField",
  "name": "id",
  "storageKey": null
},
v4 = {
  "alias": null,
  "args": null,
  "kind": "ScalarField",
  "name": "createdAt",
  "storageKey": null
},
v5 = {
  "alias": null,
  "args": null,
  "kind": "ScalarField",
  "name": "status",
  "storageKey": null
},
v6 = {
  "alias": null,
  "args": null,
  "kind": "ScalarField",
  "name": "path",
  "storageKey": null
},
v7 = {
  "alias": null,
  "args": null,
  "kind": "ScalarField",
  "name": "name",
  "storageKey": null
},
v8 = {
  "alias": null,
  "args": null,
  "kind": "ScalarField",
  "name": "value",
  "storageKey": null
},
v9 = {
  "alias": null,
  "args": null,
  "kind": "ScalarField",
  "name": "type",
  "storageKey": null
},
v10 = {
  "alias": null,
  "args": null,
  "kind": "ScalarField",
  "name": "__typename",
  "storageKey": null
},
v11 = [
  (v7/*: any*/),
  (v3/*: any*/)
],
v12 = [
  (v3/*: any*/)
],
v13 = {
  "kind": "InlineFragment",
  "selections": [
    (v10/*: any*/),
    {
      "alias": null,
      "args": null,
      "concreteType": "PipelineApproval",
      "kind": "LinkedField",
      "name": "approvals",
      "plural": true,
      "selections": [
        (v3/*: any*/),
        {
          "alias": null,
          "args": null,
          "concreteType": "ResourceMetadata",
          "kind": "LinkedField",
          "name": "metadata",
          "plural": false,
          "selections": [
            (v4/*: any*/)
          ],
          "storageKey": null
        },
        {
          "alias": null,
          "args": null,
          "concreteType": null,
          "kind": "LinkedField",
          "name": "approver",
          "plural": false,
          "selections": [
            (v10/*: any*/),
            {
              "kind": "InlineFragment",
              "selections": [
                {
                  "alias": null,
                  "args": null,
                  "kind": "ScalarField",
                  "name": "username",
                  "storageKey": null
                },
                {
                  "alias": null,
                  "args": null,
                  "kind": "ScalarField",
                  "name": "email",
                  "storageKey": null
                },
                (v3/*: any*/)
              ],
              "type": "User",
              "abstractKey": null
            },
            {
              "kind": "InlineFragment",
              "selections": (v11/*: any*/),
              "type": "ServiceAccount",
              "abstractKey": null
            },
            {
              "kind": "InlineFragment",
              "selections": (v12/*: any*/),
              "type": "Node",
              "abstractKey": "__isNode"
            }
          ],
          "storageKey": null
        },
        {
          "alias": null,
          "args": null,
          "concreteType": "ApprovalRule",
          "kind": "LinkedField",
          "name": "approvalRules",
          "plural": true,
          "selections": (v11/*: any*/),
          "storageKey": null
        }
      ],
      "storageKey": null
    },
    {
      "alias": null,
      "args": null,
      "concreteType": "ApprovalRule",
      "kind": "LinkedField",
      "name": "approvalRules",
      "plural": true,
      "selections": [
        (v3/*: any*/),
        (v7/*: any*/),
        {
          "alias": null,
          "args": null,
          "kind": "ScalarField",
          "name": "approvalsRequired",
          "storageKey": null
        }
      ],
      "storageKey": null
    }
  ],
  "type": "ApprovablePipelineNode",
  "abstractKey": "__isApprovablePipelineNode"
},
v14 = {
  "alias": null,
  "args": null,
  "kind": "ScalarField",
  "name": "when",
  "storageKey": null
},
v15 = {
  "alias": null,
  "args": null,
  "kind": "ScalarField",
  "name": "scheduledStartTime",
  "storageKey": null
},
v16 = {
  "alias": null,
  "args": null,
  "concreteType": "PipelineCronSchedule",
  "kind": "LinkedField",
  "name": "cronSchedule",
  "plural": false,
  "selections": [
    {
      "alias": null,
      "args": null,
      "kind": "ScalarField",
      "name": "expression",
      "storageKey": null
    },
    {
      "alias": null,
      "args": null,
      "kind": "ScalarField",
      "name": "timezone",
      "storageKey": null
    }
  ],
  "storageKey": null
},
v17 = [
  {
    "kind": "Literal",
    "name": "first",
    "value": 0
  }
],
v18 = [
  {
    "alias": null,
    "args": null,
    "kind": "ScalarField",
    "name": "totalCount",
    "storageKey": null
  }
],
v19 = {
  "alias": null,
  "args": null,
  "kind": "ScalarField",
  "name": "approvalStatus",
  "storageKey": null
},
v20 = {
  "alias": null,
  "args": null,
  "kind": "ScalarField",
  "name": "errors",
  "storageKey": null
},
v21 = {
  "alias": null,
  "args": null,
  "kind": "ScalarField",
  "name": "dependencies",
  "storageKey": null
},
v22 = [
  (v6/*: any*/),
  (v7/*: any*/),
  (v5/*: any*/),
  (v14/*: any*/),
  (v21/*: any*/)
],
v23 = {
  "alias": null,
  "args": null,
  "kind": "ScalarField",
  "name": "semanticVersion",
  "storageKey": null
},
v24 = {
  "alias": null,
  "args": null,
  "kind": "ScalarField",
  "name": "key",
  "storageKey": null
};
return {
  "fragment": {
    "argumentDefinitions": (v0/*: any*/),
    "kind": "Fragment",
    "metadata": null,
    "name": "PipelineDetailsSubscription",
    "selections": [
      {
        "alias": null,
        "args": (v1/*: any*/),
        "concreteType": "PipelineEvent",
        "kind": "LinkedField",
        "name": "pipelineEvents",
        "plural": false,
        "selections": [
          (v2/*: any*/),
          {
            "alias": null,
            "args": null,
            "concreteType": "Pipeline",
            "kind": "LinkedField",
            "name": "pipeline",
            "plural": false,
            "selections": [
              (v3/*: any*/),
              {
                "args": null,
                "kind": "FragmentSpread",
                "name": "PipelineDetailsFragment_pipeline"
              }
            ],
            "storageKey": null
          }
        ],
        "storageKey": null
      }
    ],
    "type": "Subscription",
    "abstractKey": null
  },
  "kind": "Request",
  "operation": {
    "argumentDefinitions": (v0/*: any*/),
    "kind": "Operation",
    "name": "PipelineDetailsSubscription",
    "selections": [
      {
        "alias": null,
        "args": (v1/*: any*/),
        "concreteType": "PipelineEvent",
        "kind": "LinkedField",
        "name": "pipelineEvents",
        "plural": false,
        "selections": [
          (v2/*: any*/),
          {
            "alias": null,
            "args": null,
            "concreteType": "Pipeline",
            "kind": "LinkedField",
            "name": "pipeline",
            "plural": false,
            "selections": [
              (v3/*: any*/),
              {
                "alias": null,
                "args": null,
                "concreteType": "ResourceMetadata",
                "kind": "LinkedField",
                "name": "metadata",
                "plural": false,
                "selections": [
                  {
                    "alias": null,
                    "args": null,
                    "kind": "ScalarField",
                    "name": "version",
                    "storageKey": null
                  },
                  (v4/*: any*/),
                  {
                    "alias": null,
                    "args": null,
                    "kind": "ScalarField",
                    "name": "prn",
                    "storageKey": null
                  }
                ],
                "storageKey": null
              },
              (v5/*: any*/),
              {
                "alias": null,
                "args": null,
                "concreteType": "PipelineStage",
                "kind": "LinkedField",
                "name": "stages",
                "plural": true,
                "selections": [
                  (v6/*: any*/),
                  (v7/*: any*/),
                  (v5/*: any*/),
                  {
                    "alias": null,
                    "args": null,
                    "concreteType": "PipelineTask",
                    "kind": "LinkedField",
                    "name": "tasks",
                    "plural": true,
                    "selections": [
                      (v6/*: any*/),
                      {
                        "alias": null,
                        "args": null,
                        "concreteType": "PipelineAction",
                        "kind": "LinkedField",
                        "name": "actions",
                        "plural": true,
                        "selections": [
                          (v7/*: any*/),
                          {
                            "alias": null,
                            "args": null,
                            "concreteType": "PipelineActionOutput",
                            "kind": "LinkedField",
                            "name": "outputs",
                            "plural": true,
                            "selections": [
                              (v7/*: any*/),
                              (v8/*: any*/),
                              (v9/*: any*/)
                            ],
                            "storageKey": null
                          }
                        ],
                        "storageKey": null
                      },
                      (v13/*: any*/),
                      (v7/*: any*/),
                      (v5/*: any*/),
                      (v14/*: any*/),
                      {
                        "alias": null,
                        "args": null,
                        "kind": "ScalarField",
                        "name": "agentTags",
                        "storageKey": null
                      },
                      (v15/*: any*/),
                      (v16/*: any*/),
                      {
                        "alias": null,
                        "args": null,
                        "kind": "ScalarField",
                        "name": "maxAttempts",
                        "storageKey": null
                      },
                      {
                        "alias": null,
                        "args": null,
                        "kind": "ScalarField",
                        "name": "attemptCount",
                        "storageKey": null
                      },
                      {
                        "alias": null,
                        "args": null,
                        "kind": "ScalarField",
                        "name": "interval",
                        "storageKey": null
                      },
                      {
                        "alias": null,
                        "args": null,
                        "kind": "ScalarField",
                        "name": "lastAttemptFinishedAt",
                        "storageKey": null
                      },
                      {
                        "alias": null,
                        "args": null,
                        "concreteType": "Job",
                        "kind": "LinkedField",
                        "name": "currentJob",
                        "plural": false,
                        "selections": (v12/*: any*/),
                        "storageKey": null
                      },
                      {
                        "alias": null,
                        "args": (v17/*: any*/),
                        "concreteType": "JobConnection",
                        "kind": "LinkedField",
                        "name": "jobs",
                        "plural": false,
                        "selections": (v18/*: any*/),
                        "storageKey": "jobs(first:0)"
                      },
                      (v19/*: any*/),
                      (v20/*: any*/),
                      (v21/*: any*/)
                    ],
                    "storageKey": null
                  },
                  {
                    "alias": null,
                    "args": null,
                    "concreteType": "NestedPipeline",
                    "kind": "LinkedField",
                    "name": "nestedPipelines",
                    "plural": true,
                    "selections": [
                      (v6/*: any*/),
                      (v7/*: any*/),
                      (v5/*: any*/),
                      (v14/*: any*/),
                      (v21/*: any*/),
                      (v15/*: any*/),
                      (v16/*: any*/),
                      {
                        "alias": null,
                        "args": null,
                        "concreteType": "Pipeline",
                        "kind": "LinkedField",
                        "name": "latestPipeline",
                        "plural": false,
                        "selections": [
                          (v3/*: any*/),
                          (v14/*: any*/),
                          (v5/*: any*/),
                          {
                            "alias": null,
                            "args": null,
                            "concreteType": "PipelineStage",
                            "kind": "LinkedField",
                            "name": "stages",
                            "plural": true,
                            "selections": [
                              (v6/*: any*/),
                              (v7/*: any*/),
                              (v5/*: any*/),
                              {
                                "alias": null,
                                "args": null,
                                "concreteType": "PipelineTask",
                                "kind": "LinkedField",
                                "name": "tasks",
                                "plural": true,
                                "selections": (v22/*: any*/),
                                "storageKey": null
                              },
                              {
                                "alias": null,
                                "args": null,
                                "concreteType": "NestedPipeline",
                                "kind": "LinkedField",
                                "name": "nestedPipelines",
                                "plural": true,
                                "selections": (v22/*: any*/),
                                "storageKey": null
                              }
                            ],
                            "storageKey": null
                          }
                        ],
                        "storageKey": null
                      },
                      {
                        "alias": null,
                        "args": (v17/*: any*/),
                        "concreteType": "PipelineConnection",
                        "kind": "LinkedField",
                        "name": "pipelines",
                        "plural": false,
                        "selections": (v18/*: any*/),
                        "storageKey": "pipelines(first:0)"
                      },
                      (v20/*: any*/)
                    ],
                    "storageKey": null
                  }
                ],
                "storageKey": null
              },
              {
                "alias": null,
                "args": null,
                "kind": "ScalarField",
                "name": "createdBy",
                "storageKey": null
              },
              (v9/*: any*/),
              {
                "alias": null,
                "args": null,
                "kind": "ScalarField",
                "name": "environmentName",
                "storageKey": null
              },
              {
                "alias": null,
                "args": null,
                "concreteType": "Environment",
                "kind": "LinkedField",
                "name": "environment",
                "plural": false,
                "selections": (v12/*: any*/),
                "storageKey": null
              },
              {
                "alias": null,
                "args": null,
                "concreteType": "PipelineTemplate",
                "kind": "LinkedField",
                "name": "pipelineTemplate",
                "plural": false,
                "selections": [
                  (v3/*: any*/),
                  (v7/*: any*/),
                  {
                    "alias": null,
                    "args": null,
                    "kind": "ScalarField",
                    "name": "versioned",
                    "storageKey": null
                  },
                  (v23/*: any*/),
                  {
                    "alias": null,
                    "args": null,
                    "kind": "ScalarField",
                    "name": "hclData",
                    "storageKey": null
                  }
                ],
                "storageKey": null
              },
              {
                "alias": null,
                "args": null,
                "concreteType": "PipelineVariable",
                "kind": "LinkedField",
                "name": "variables",
                "plural": true,
                "selections": [
                  {
                    "alias": null,
                    "args": null,
                    "kind": "ScalarField",
                    "name": "category",
                    "storageKey": null
                  },
                  (v24/*: any*/),
                  (v8/*: any*/)
                ],
                "storageKey": null
              },
              {
                "alias": null,
                "args": null,
                "concreteType": "Pipeline",
                "kind": "LinkedField",
                "name": "parentPipeline",
                "plural": false,
                "selections": (v12/*: any*/),
                "storageKey": null
              },
              {
                "alias": null,
                "args": null,
                "concreteType": "Release",
                "kind": "LinkedField",
                "name": "release",
                "plural": false,
                "selections": [
                  (v3/*: any*/),
                  (v23/*: any*/)
                ],
                "storageKey": null
              },
              (v14/*: any*/),
              {
                "alias": null,
                "args": null,
                "kind": "ScalarField",
                "name": "superseded",
                "storageKey": null
              },
              (v15/*: any*/),
              {
                "alias": null,
                "args": null,
                "kind": "ScalarField",
                "name": "forceCancelAvailableAt",
                "storageKey": null
              },
              (v16/*: any*/),
              {
                "alias": null,
                "args": null,
                "concreteType": "PipelineAnnotation",
                "kind": "LinkedField",
                "name": "annotations",
                "plural": true,
                "selections": [
                  (v24/*: any*/),
                  (v8/*: any*/),
                  {
                    "alias": null,
                    "args": null,
                    "kind": "ScalarField",
                    "name": "link",
                    "storageKey": null
                  }
                ],
                "storageKey": null
              },
              (v19/*: any*/),
              (v13/*: any*/),
              {
                "alias": null,
                "args": null,
                "concreteType": "Project",
                "kind": "LinkedField",
                "name": "project",
                "plural": false,
                "selections": [
                  (v7/*: any*/),
                  {
                    "alias": null,
                    "args": null,
                    "kind": "ScalarField",
                    "name": "organizationName",
                    "storageKey": null
                  },
                  (v3/*: any*/)
                ],
                "storageKey": null
              }
            ],
            "storageKey": null
          }
        ],
        "storageKey": null
      }
    ]
  },
  "params": {
    "cacheID": "62dd6bcba79d513c17c2edb69f22eefa",
    "id": null,
    "metadata": {},
    "name": "PipelineDetailsSubscription",
    "operationKind": "subscription",
    "text": "subscription PipelineDetailsSubscription(\n  $input: PipelineEventsSubscriptionInput!\n) {\n  pipelineEvents(input: $input) {\n    action\n    pipeline {\n      id\n      ...PipelineDetailsFragment_pipeline\n    }\n  }\n}\n\nfragment ForceCancelPipelineAlertFragment_pipeline on Pipeline {\n  forceCancelAvailableAt\n  ...ForceCancelPipelineButtonFragment_pipeline\n}\n\nfragment ForceCancelPipelineButtonDialogFragment_pipeline on Pipeline {\n  project {\n    name\n    organizationName\n    id\n  }\n}\n\nfragment ForceCancelPipelineButtonFragment_pipeline on Pipeline {\n  id\n  ...ForceCancelPipelineButtonDialogFragment_pipeline\n}\n\nfragment PipelineActionOutputListItemFragment_output on PipelineActionOutput {\n  name\n  value\n  type\n}\n\nfragment PipelineActionOutputsFragment_outputs on PipelineTask {\n  actions {\n    name\n    outputs {\n      name\n      ...PipelineActionOutputListItemFragment_output\n    }\n  }\n}\n\nfragment PipelineAnnotationsFragment_pipeline on Pipeline {\n  annotations {\n    key\n    value\n    link\n  }\n}\n\nfragment PipelineApprovalActionsFragment_approvableNode on ApprovablePipelineNode {\n  __isApprovablePipelineNode: __typename\n  __typename\n  approvals {\n    id\n    approvalRules {\n      id\n    }\n    approver {\n      __typename\n      ... on User {\n        id\n        email\n      }\n      ... on ServiceAccount {\n        id\n        name\n      }\n      ... on Node {\n        __isNode: __typename\n        id\n      }\n    }\n  }\n  approvalRules {\n    id\n    name\n    approvalsRequired\n  }\n}\n\nfragment PipelineApprovalListItemFragment_approval on PipelineApproval {\n  metadata {\n    createdAt\n  }\n  id\n  approver {\n    __typename\n    ... on User {\n      username\n      email\n    }\n    ... on ServiceAccount {\n      name\n    }\n    ... on Node {\n      __isNode: __typename\n      id\n    }\n  }\n  approvalRules {\n    name\n    id\n  }\n}\n\nfragment PipelineApprovalsFragment_approvals on ApprovablePipelineNode {\n  __isApprovablePipelineNode: __typename\n  __typename\n  approvals {\n    id\n    ...PipelineApprovalListItemFragment_approval\n  }\n}\n\nfragment PipelineDetailsFragment_pipeline on Pipeline {\n  metadata {\n    version\n  }\n  id\n  status\n  stages {\n    path\n    name\n    status\n    tasks {\n      path\n    }\n  }\n  ...PipelineTaskDetailsFragment_details\n  ...PipelineDetailsSidebarFragment_details\n  ...PipelineDetailsIndexFragment_details\n  ...PipelineNestedPipelineDetailsFragment_details\n}\n\nfragment PipelineDetailsIndexFragment_details on Pipeline {\n  id\n  status\n  createdBy\n  type\n  environmentName\n  environment {\n    id\n  }\n  when\n  superseded\n  scheduledStartTime\n  forceCancelAvailableAt\n  cronSchedule {\n    expression\n    timezone\n  }\n  metadata {\n    createdAt\n    prn\n  }\n  pipelineTemplate {\n    id\n    name\n    versioned\n    semanticVersion\n    hclData\n  }\n  variables {\n    category\n  }\n  parentPipeline {\n    id\n  }\n  release {\n    id\n    semanticVersion\n  }\n  annotations {\n    key\n  }\n  approvalStatus\n  ...PipelineVariablesFragment\n  ...PipelineStagesFragment_stages\n  ...PipelineThreadListFragment_pipeline\n  ...PipelineAnnotationsFragment_pipeline\n  ...PipelineApprovalsFragment_approvals\n  ...PipelineApprovalActionsFragment_approvableNode\n  ...ForceCancelPipelineAlertFragment_pipeline\n}\n\nfragment PipelineDetailsSidebarFragment_details on Pipeline {\n  id\n  status\n  createdBy\n  type\n  environmentName\n  environment {\n    id\n  }\n  metadata {\n    createdAt\n  }\n  pipelineTemplate {\n    id\n    name\n    versioned\n    semanticVersion\n  }\n  variables {\n    category\n  }\n  parentPipeline {\n    id\n  }\n  release {\n    id\n    semanticVersion\n  }\n  ...PipelineStagesFragment_stages\n}\n\nfragment PipelineNestedPipelineDetailsFragment_details on Pipeline {\n  id\n  stages {\n    nestedPipelines {\n      path\n      name\n      status\n      scheduledStartTime\n      cronSchedule {\n        expression\n        timezone\n      }\n      latestPipeline {\n        id\n        ...PipelineNestedPipelineDetailsFragment_pipeline\n      }\n      pipelines(first: 0) {\n        totalCount\n      }\n      errors\n    }\n  }\n}\n\nfragment PipelineNestedPipelineDetailsFragment_pipeline on Pipeline {\n  id\n  when\n  status\n  ...PipelineStagesFragment_stages\n}\n\nfragment PipelineStagesFragment_stages on Pipeline {\n  stages {\n    path\n    name\n    status\n    tasks {\n      path\n      name\n      status\n      when\n      dependencies\n    }\n    nestedPipelines {\n      path\n      name\n      status\n      when\n      dependencies\n    }\n  }\n}\n\nfragment PipelineTaskDetailsFragment_details on Pipeline {\n  id\n  status\n  stages {\n    tasks {\n      ...PipelineActionOutputsFragment_outputs\n      ...PipelineApprovalsFragment_approvals\n      ...PipelineApprovalActionsFragment_approvableNode\n      path\n      name\n      status\n      when\n      agentTags\n      scheduledStartTime\n      cronSchedule {\n        expression\n        timezone\n      }\n      maxAttempts\n      attemptCount\n      interval\n      lastAttemptFinishedAt\n      currentJob {\n        id\n      }\n      jobs(first: 0) {\n        totalCount\n      }\n      approvalStatus\n      errors\n    }\n  }\n}\n\nfragment PipelineThreadListFragment_pipeline on Pipeline {\n  id\n}\n\nfragment PipelineVariableListItemFragment_variable on PipelineVariable {\n  key\n  value\n  category\n}\n\nfragment PipelineVariablesFragment on Pipeline {\n  id\n  variables {\n    key\n    value\n    category\n    ...PipelineVariableListItemFragment_variable\n  }\n}\n"
  }
};
})();

(node as any).hash = "5c53347bf628e2577cc1037eca15bc16";

export default node;
