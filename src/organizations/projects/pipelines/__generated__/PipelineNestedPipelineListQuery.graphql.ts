/**
 * @generated SignedSource<<0d40489a4d7d6d3c3c9629e0412c3977>>
 * @lightSyntaxTransform
 * @nogrep
 */

/* tslint:disable */
/* eslint-disable */
// @ts-nocheck

import { ConcreteRequest, Query } from 'relay-runtime';
import { FragmentRefs } from "relay-runtime";
export type PipelineNestedPipelineListQuery$variables = {
  after?: string | null | undefined;
  first: number;
  nestedPipelinePath: string;
  pipelineId: string;
};
export type PipelineNestedPipelineListQuery$data = {
  readonly " $fragmentSpreads": FragmentRefs<"PipelineNestedPipelineListFragment_pipelines">;
};
export type PipelineNestedPipelineListQuery = {
  response: PipelineNestedPipelineListQuery$data;
  variables: PipelineNestedPipelineListQuery$variables;
};

const node: ConcreteRequest = (function(){
var v0 = {
  "defaultValue": null,
  "kind": "LocalArgument",
  "name": "after"
},
v1 = {
  "defaultValue": null,
  "kind": "LocalArgument",
  "name": "first"
},
v2 = {
  "defaultValue": null,
  "kind": "LocalArgument",
  "name": "nestedPipelinePath"
},
v3 = {
  "defaultValue": null,
  "kind": "LocalArgument",
  "name": "pipelineId"
},
v4 = {
  "alias": null,
  "args": null,
  "kind": "ScalarField",
  "name": "__typename",
  "storageKey": null
},
v5 = [
  {
    "kind": "Variable",
    "name": "after",
    "variableName": "after"
  },
  {
    "kind": "Variable",
    "name": "first",
    "variableName": "first"
  },
  {
    "kind": "Literal",
    "name": "sort",
    "value": "CREATED_AT_DESC"
  }
];
return {
  "fragment": {
    "argumentDefinitions": [
      (v0/*: any*/),
      (v1/*: any*/),
      (v2/*: any*/),
      (v3/*: any*/)
    ],
    "kind": "Fragment",
    "metadata": null,
    "name": "PipelineNestedPipelineListQuery",
    "selections": [
      {
        "args": null,
        "kind": "FragmentSpread",
        "name": "PipelineNestedPipelineListFragment_pipelines"
      }
    ],
    "type": "Query",
    "abstractKey": null
  },
  "kind": "Request",
  "operation": {
    "argumentDefinitions": [
      (v3/*: any*/),
      (v1/*: any*/),
      (v0/*: any*/),
      (v2/*: any*/)
    ],
    "kind": "Operation",
    "name": "PipelineNestedPipelineListQuery",
    "selections": [
      {
        "alias": null,
        "args": [
          {
            "kind": "Variable",
            "name": "nodePath",
            "variableName": "nestedPipelinePath"
          },
          {
            "kind": "Variable",
            "name": "pipelineId",
            "variableName": "pipelineId"
          }
        ],
        "concreteType": null,
        "kind": "LinkedField",
        "name": "pipelineNode",
        "plural": false,
        "selections": [
          (v4/*: any*/),
          {
            "kind": "InlineFragment",
            "selections": [
              {
                "alias": null,
                "args": (v5/*: any*/),
                "concreteType": "PipelineConnection",
                "kind": "LinkedField",
                "name": "pipelines",
                "plural": false,
                "selections": [
                  {
                    "alias": null,
                    "args": null,
                    "kind": "ScalarField",
                    "name": "totalCount",
                    "storageKey": null
                  },
                  {
                    "alias": null,
                    "args": null,
                    "concreteType": "PipelineEdge",
                    "kind": "LinkedField",
                    "name": "edges",
                    "plural": true,
                    "selections": [
                      {
                        "alias": null,
                        "args": null,
                        "concreteType": "Pipeline",
                        "kind": "LinkedField",
                        "name": "node",
                        "plural": false,
                        "selections": [
                          {
                            "alias": null,
                            "args": null,
                            "kind": "ScalarField",
                            "name": "id",
                            "storageKey": null
                          },
                          {
                            "alias": null,
                            "args": null,
                            "kind": "ScalarField",
                            "name": "status",
                            "storageKey": null
                          },
                          {
                            "alias": null,
                            "args": null,
                            "concreteType": "ResourceMetadata",
                            "kind": "LinkedField",
                            "name": "metadata",
                            "plural": false,
                            "selections": [
                              {
                                "alias": null,
                                "args": null,
                                "kind": "ScalarField",
                                "name": "createdAt",
                                "storageKey": null
                              }
                            ],
                            "storageKey": null
                          },
                          (v4/*: any*/)
                        ],
                        "storageKey": null
                      },
                      {
                        "alias": null,
                        "args": null,
                        "kind": "ScalarField",
                        "name": "cursor",
                        "storageKey": null
                      }
                    ],
                    "storageKey": null
                  },
                  {
                    "alias": null,
                    "args": null,
                    "concreteType": "PageInfo",
                    "kind": "LinkedField",
                    "name": "pageInfo",
                    "plural": false,
                    "selections": [
                      {
                        "alias": null,
                        "args": null,
                        "kind": "ScalarField",
                        "name": "endCursor",
                        "storageKey": null
                      },
                      {
                        "alias": null,
                        "args": null,
                        "kind": "ScalarField",
                        "name": "hasNextPage",
                        "storageKey": null
                      }
                    ],
                    "storageKey": null
                  }
                ],
                "storageKey": null
              },
              {
                "alias": null,
                "args": (v5/*: any*/),
                "filters": [
                  "sort"
                ],
                "handle": "connection",
                "key": "PipelineNestedPipelineList_pipelines",
                "kind": "LinkedHandle",
                "name": "pipelines"
              }
            ],
            "type": "NestedPipeline",
            "abstractKey": null
          }
        ],
        "storageKey": null
      }
    ]
  },
  "params": {
    "cacheID": "63bb8cde563d2e371e011b00958c3af3",
    "id": null,
    "metadata": {},
    "name": "PipelineNestedPipelineListQuery",
    "operationKind": "query",
    "text": "query PipelineNestedPipelineListQuery(\n  $pipelineId: String!\n  $first: Int!\n  $after: String\n  $nestedPipelinePath: String!\n) {\n  ...PipelineNestedPipelineListFragment_pipelines\n}\n\nfragment PipelineNestedPipelineListFragment_pipelines on Query {\n  pipelineNode(pipelineId: $pipelineId, nodePath: $nestedPipelinePath) {\n    __typename\n    ... on NestedPipeline {\n      pipelines(first: $first, after: $after, sort: CREATED_AT_DESC) {\n        totalCount\n        edges {\n          node {\n            id\n            ...PipelineNestedPipelineListItemFragment\n            __typename\n          }\n          cursor\n        }\n        pageInfo {\n          endCursor\n          hasNextPage\n        }\n      }\n    }\n  }\n}\n\nfragment PipelineNestedPipelineListItemFragment on Pipeline {\n  id\n  status\n  metadata {\n    createdAt\n  }\n}\n"
  }
};
})();

(node as any).hash = "025027b81aea9efacf408221480bda43";

export default node;
