/**
 * @generated SignedSource<<a38916848f3a61e6fb9c391bcdb8f0b0>>
 * @lightSyntaxTransform
 * @nogrep
 */

/* tslint:disable */
/* eslint-disable */
// @ts-nocheck

import { ConcreteRequest, Mutation } from 'relay-runtime';
export type ProblemType = "BAD_REQUEST" | "CONFLICT" | "FORBIDDEN" | "NOT_FOUND" | "%future added value";
export type RetryNestedPipelineInput = {
  clientMutationId?: string | null | undefined;
  parentNestedPipelineNodePath: string;
  parentPipelineId: string;
};
export type PipelineNestedPipelineDetailsRetryNestedPipelineMutation$variables = {
  input: RetryNestedPipelineInput;
};
export type PipelineNestedPipelineDetailsRetryNestedPipelineMutation$data = {
  readonly retryNestedPipeline: {
    readonly problems: ReadonlyArray<{
      readonly field: ReadonlyArray<string> | null | undefined;
      readonly message: string;
      readonly type: ProblemType;
    }>;
  };
};
export type PipelineNestedPipelineDetailsRetryNestedPipelineMutation = {
  response: PipelineNestedPipelineDetailsRetryNestedPipelineMutation$data;
  variables: PipelineNestedPipelineDetailsRetryNestedPipelineMutation$variables;
};

const node: ConcreteRequest = (function(){
var v0 = [
  {
    "defaultValue": null,
    "kind": "LocalArgument",
    "name": "input"
  }
],
v1 = [
  {
    "alias": null,
    "args": [
      {
        "kind": "Variable",
        "name": "input",
        "variableName": "input"
      }
    ],
    "concreteType": "PipelineMutationPayload",
    "kind": "LinkedField",
    "name": "retryNestedPipeline",
    "plural": false,
    "selections": [
      {
        "alias": null,
        "args": null,
        "concreteType": "Problem",
        "kind": "LinkedField",
        "name": "problems",
        "plural": true,
        "selections": [
          {
            "alias": null,
            "args": null,
            "kind": "ScalarField",
            "name": "message",
            "storageKey": null
          },
          {
            "alias": null,
            "args": null,
            "kind": "ScalarField",
            "name": "field",
            "storageKey": null
          },
          {
            "alias": null,
            "args": null,
            "kind": "ScalarField",
            "name": "type",
            "storageKey": null
          }
        ],
        "storageKey": null
      }
    ],
    "storageKey": null
  }
];
return {
  "fragment": {
    "argumentDefinitions": (v0/*: any*/),
    "kind": "Fragment",
    "metadata": null,
    "name": "PipelineNestedPipelineDetailsRetryNestedPipelineMutation",
    "selections": (v1/*: any*/),
    "type": "Mutation",
    "abstractKey": null
  },
  "kind": "Request",
  "operation": {
    "argumentDefinitions": (v0/*: any*/),
    "kind": "Operation",
    "name": "PipelineNestedPipelineDetailsRetryNestedPipelineMutation",
    "selections": (v1/*: any*/)
  },
  "params": {
    "cacheID": "e40fd0c1cbcc8c1257d601e308d656a4",
    "id": null,
    "metadata": {},
    "name": "PipelineNestedPipelineDetailsRetryNestedPipelineMutation",
    "operationKind": "mutation",
    "text": "mutation PipelineNestedPipelineDetailsRetryNestedPipelineMutation(\n  $input: RetryNestedPipelineInput!\n) {\n  retryNestedPipeline(input: $input) {\n    problems {\n      message\n      field\n      type\n    }\n  }\n}\n"
  }
};
})();

(node as any).hash = "57b370ee8b705b3df000d9c52fd822ce";

export default node;
