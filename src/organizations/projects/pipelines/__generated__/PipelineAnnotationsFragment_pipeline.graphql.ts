/**
 * @generated SignedSource<<eb9a23bbd5206b5749b377422a10a9c7>>
 * @lightSyntaxTransform
 * @nogrep
 */

/* tslint:disable */
/* eslint-disable */
// @ts-nocheck

import { Fragment, ReaderFragment } from 'relay-runtime';
import { FragmentRefs } from "relay-runtime";
export type PipelineAnnotationsFragment_pipeline$data = {
  readonly annotations: ReadonlyArray<{
    readonly key: string;
    readonly link: string | null | undefined;
    readonly value: string;
  }>;
  readonly " $fragmentType": "PipelineAnnotationsFragment_pipeline";
};
export type PipelineAnnotationsFragment_pipeline$key = {
  readonly " $data"?: PipelineAnnotationsFragment_pipeline$data;
  readonly " $fragmentSpreads": FragmentRefs<"PipelineAnnotationsFragment_pipeline">;
};

const node: ReaderFragment = {
  "argumentDefinitions": [],
  "kind": "Fragment",
  "metadata": null,
  "name": "PipelineAnnotationsFragment_pipeline",
  "selections": [
    {
      "alias": null,
      "args": null,
      "concreteType": "PipelineAnnotation",
      "kind": "LinkedField",
      "name": "annotations",
      "plural": true,
      "selections": [
        {
          "alias": null,
          "args": null,
          "kind": "ScalarField",
          "name": "key",
          "storageKey": null
        },
        {
          "alias": null,
          "args": null,
          "kind": "ScalarField",
          "name": "value",
          "storageKey": null
        },
        {
          "alias": null,
          "args": null,
          "kind": "ScalarField",
          "name": "link",
          "storageKey": null
        }
      ],
      "storageKey": null
    }
  ],
  "type": "Pipeline",
  "abstractKey": null
};

(node as any).hash = "f7293ed0fe459cc0fb10147e7b175d94";

export default node;
