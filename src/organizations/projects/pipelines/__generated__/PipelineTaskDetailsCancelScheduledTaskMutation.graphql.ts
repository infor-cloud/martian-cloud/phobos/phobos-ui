/**
 * @generated SignedSource<<b2479923dde79a30c8a56b3783239677>>
 * @lightSyntaxTransform
 * @nogrep
 */

/* tslint:disable */
/* eslint-disable */
// @ts-nocheck

import { ConcreteRequest, Mutation } from 'relay-runtime';
export type PipelineNodeType = "ACTION" | "PIPELINE" | "STAGE" | "TASK" | "%future added value";
export type ProblemType = "BAD_REQUEST" | "CONFLICT" | "FORBIDDEN" | "NOT_FOUND" | "%future added value";
export type CancelPipelineNodeScheduleInput = {
  clientMutationId?: string | null | undefined;
  nodePath: string;
  nodeType: PipelineNodeType;
  pipelineId: string;
};
export type PipelineTaskDetailsCancelScheduledTaskMutation$variables = {
  input: CancelPipelineNodeScheduleInput;
};
export type PipelineTaskDetailsCancelScheduledTaskMutation$data = {
  readonly cancelPipelineNodeSchedule: {
    readonly problems: ReadonlyArray<{
      readonly field: ReadonlyArray<string> | null | undefined;
      readonly message: string;
      readonly type: ProblemType;
    }>;
  };
};
export type PipelineTaskDetailsCancelScheduledTaskMutation = {
  response: PipelineTaskDetailsCancelScheduledTaskMutation$data;
  variables: PipelineTaskDetailsCancelScheduledTaskMutation$variables;
};

const node: ConcreteRequest = (function(){
var v0 = [
  {
    "defaultValue": null,
    "kind": "LocalArgument",
    "name": "input"
  }
],
v1 = [
  {
    "alias": null,
    "args": [
      {
        "kind": "Variable",
        "name": "input",
        "variableName": "input"
      }
    ],
    "concreteType": "PipelineMutationPayload",
    "kind": "LinkedField",
    "name": "cancelPipelineNodeSchedule",
    "plural": false,
    "selections": [
      {
        "alias": null,
        "args": null,
        "concreteType": "Problem",
        "kind": "LinkedField",
        "name": "problems",
        "plural": true,
        "selections": [
          {
            "alias": null,
            "args": null,
            "kind": "ScalarField",
            "name": "message",
            "storageKey": null
          },
          {
            "alias": null,
            "args": null,
            "kind": "ScalarField",
            "name": "field",
            "storageKey": null
          },
          {
            "alias": null,
            "args": null,
            "kind": "ScalarField",
            "name": "type",
            "storageKey": null
          }
        ],
        "storageKey": null
      }
    ],
    "storageKey": null
  }
];
return {
  "fragment": {
    "argumentDefinitions": (v0/*: any*/),
    "kind": "Fragment",
    "metadata": null,
    "name": "PipelineTaskDetailsCancelScheduledTaskMutation",
    "selections": (v1/*: any*/),
    "type": "Mutation",
    "abstractKey": null
  },
  "kind": "Request",
  "operation": {
    "argumentDefinitions": (v0/*: any*/),
    "kind": "Operation",
    "name": "PipelineTaskDetailsCancelScheduledTaskMutation",
    "selections": (v1/*: any*/)
  },
  "params": {
    "cacheID": "60b93aa7558e74ea7fe98f045fee9c3c",
    "id": null,
    "metadata": {},
    "name": "PipelineTaskDetailsCancelScheduledTaskMutation",
    "operationKind": "mutation",
    "text": "mutation PipelineTaskDetailsCancelScheduledTaskMutation(\n  $input: CancelPipelineNodeScheduleInput!\n) {\n  cancelPipelineNodeSchedule(input: $input) {\n    problems {\n      message\n      field\n      type\n    }\n  }\n}\n"
  }
};
})();

(node as any).hash = "1258be29abd1426410e8e3b55d0b01a4";

export default node;
