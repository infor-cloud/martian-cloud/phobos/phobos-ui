/**
 * @generated SignedSource<<33a13a6fc1bd1c285d5dead7c09427ee>>
 * @lightSyntaxTransform
 * @nogrep
 */

/* tslint:disable */
/* eslint-disable */
// @ts-nocheck

import { Fragment, ReaderFragment } from 'relay-runtime';
export type VariableCategory = "ENVIRONMENT" | "HCL" | "%future added value";
import { FragmentRefs } from "relay-runtime";
export type PipelineVariablesFragment$data = {
  readonly id: string;
  readonly variables: ReadonlyArray<{
    readonly category: VariableCategory;
    readonly key: string;
    readonly value: string;
    readonly " $fragmentSpreads": FragmentRefs<"PipelineVariableListItemFragment_variable">;
  }>;
  readonly " $fragmentType": "PipelineVariablesFragment";
};
export type PipelineVariablesFragment$key = {
  readonly " $data"?: PipelineVariablesFragment$data;
  readonly " $fragmentSpreads": FragmentRefs<"PipelineVariablesFragment">;
};

const node: ReaderFragment = {
  "argumentDefinitions": [],
  "kind": "Fragment",
  "metadata": null,
  "name": "PipelineVariablesFragment",
  "selections": [
    {
      "alias": null,
      "args": null,
      "kind": "ScalarField",
      "name": "id",
      "storageKey": null
    },
    {
      "alias": null,
      "args": null,
      "concreteType": "PipelineVariable",
      "kind": "LinkedField",
      "name": "variables",
      "plural": true,
      "selections": [
        {
          "alias": null,
          "args": null,
          "kind": "ScalarField",
          "name": "key",
          "storageKey": null
        },
        {
          "alias": null,
          "args": null,
          "kind": "ScalarField",
          "name": "value",
          "storageKey": null
        },
        {
          "alias": null,
          "args": null,
          "kind": "ScalarField",
          "name": "category",
          "storageKey": null
        },
        {
          "args": null,
          "kind": "FragmentSpread",
          "name": "PipelineVariableListItemFragment_variable"
        }
      ],
      "storageKey": null
    }
  ],
  "type": "Pipeline",
  "abstractKey": null
};

(node as any).hash = "24673b2e4ac0577ef3a04c0f1825b319";

export default node;
