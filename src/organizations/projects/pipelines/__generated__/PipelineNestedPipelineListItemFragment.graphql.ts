/**
 * @generated SignedSource<<65983d8473e5468aee1ea16b886fd887>>
 * @lightSyntaxTransform
 * @nogrep
 */

/* tslint:disable */
/* eslint-disable */
// @ts-nocheck

import { Fragment, ReaderFragment } from 'relay-runtime';
export type PipelineNodeStatus = "APPROVAL_PENDING" | "CANCELED" | "CANCELING" | "CREATED" | "DEFERRED" | "FAILED" | "PENDING" | "READY" | "RUNNING" | "SKIPPED" | "SUCCEEDED" | "WAITING" | "%future added value";
import { FragmentRefs } from "relay-runtime";
export type PipelineNestedPipelineListItemFragment$data = {
  readonly id: string;
  readonly metadata: {
    readonly createdAt: any;
  };
  readonly status: PipelineNodeStatus;
  readonly " $fragmentType": "PipelineNestedPipelineListItemFragment";
};
export type PipelineNestedPipelineListItemFragment$key = {
  readonly " $data"?: PipelineNestedPipelineListItemFragment$data;
  readonly " $fragmentSpreads": FragmentRefs<"PipelineNestedPipelineListItemFragment">;
};

const node: ReaderFragment = {
  "argumentDefinitions": [],
  "kind": "Fragment",
  "metadata": null,
  "name": "PipelineNestedPipelineListItemFragment",
  "selections": [
    {
      "alias": null,
      "args": null,
      "kind": "ScalarField",
      "name": "id",
      "storageKey": null
    },
    {
      "alias": null,
      "args": null,
      "kind": "ScalarField",
      "name": "status",
      "storageKey": null
    },
    {
      "alias": null,
      "args": null,
      "concreteType": "ResourceMetadata",
      "kind": "LinkedField",
      "name": "metadata",
      "plural": false,
      "selections": [
        {
          "alias": null,
          "args": null,
          "kind": "ScalarField",
          "name": "createdAt",
          "storageKey": null
        }
      ],
      "storageKey": null
    }
  ],
  "type": "Pipeline",
  "abstractKey": null
};

(node as any).hash = "6334d3a0f1d279d7d37fbaafbe19235b";

export default node;
