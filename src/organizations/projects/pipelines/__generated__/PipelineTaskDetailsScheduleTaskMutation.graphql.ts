/**
 * @generated SignedSource<<00080f23ed0b653bcb698225aa2ee78c>>
 * @lightSyntaxTransform
 * @nogrep
 */

/* tslint:disable */
/* eslint-disable */
// @ts-nocheck

import { ConcreteRequest, Mutation } from 'relay-runtime';
export type PipelineNodeType = "ACTION" | "PIPELINE" | "STAGE" | "TASK" | "%future added value";
export type ProblemType = "BAD_REQUEST" | "CONFLICT" | "FORBIDDEN" | "NOT_FOUND" | "%future added value";
export type SchedulePipelineNodeInput = {
  clientMutationId?: string | null | undefined;
  cronSchedule?: CronScheduleInput | null | undefined;
  nodePath: string;
  nodeType: PipelineNodeType;
  pipelineId: string;
  scheduledStartTime?: any | null | undefined;
};
export type CronScheduleInput = {
  expression: string;
  timezone?: string | null | undefined;
};
export type PipelineTaskDetailsScheduleTaskMutation$variables = {
  input: SchedulePipelineNodeInput;
};
export type PipelineTaskDetailsScheduleTaskMutation$data = {
  readonly schedulePipelineNode: {
    readonly problems: ReadonlyArray<{
      readonly field: ReadonlyArray<string> | null | undefined;
      readonly message: string;
      readonly type: ProblemType;
    }>;
  };
};
export type PipelineTaskDetailsScheduleTaskMutation = {
  response: PipelineTaskDetailsScheduleTaskMutation$data;
  variables: PipelineTaskDetailsScheduleTaskMutation$variables;
};

const node: ConcreteRequest = (function(){
var v0 = [
  {
    "defaultValue": null,
    "kind": "LocalArgument",
    "name": "input"
  }
],
v1 = [
  {
    "alias": null,
    "args": [
      {
        "kind": "Variable",
        "name": "input",
        "variableName": "input"
      }
    ],
    "concreteType": "PipelineMutationPayload",
    "kind": "LinkedField",
    "name": "schedulePipelineNode",
    "plural": false,
    "selections": [
      {
        "alias": null,
        "args": null,
        "concreteType": "Problem",
        "kind": "LinkedField",
        "name": "problems",
        "plural": true,
        "selections": [
          {
            "alias": null,
            "args": null,
            "kind": "ScalarField",
            "name": "message",
            "storageKey": null
          },
          {
            "alias": null,
            "args": null,
            "kind": "ScalarField",
            "name": "field",
            "storageKey": null
          },
          {
            "alias": null,
            "args": null,
            "kind": "ScalarField",
            "name": "type",
            "storageKey": null
          }
        ],
        "storageKey": null
      }
    ],
    "storageKey": null
  }
];
return {
  "fragment": {
    "argumentDefinitions": (v0/*: any*/),
    "kind": "Fragment",
    "metadata": null,
    "name": "PipelineTaskDetailsScheduleTaskMutation",
    "selections": (v1/*: any*/),
    "type": "Mutation",
    "abstractKey": null
  },
  "kind": "Request",
  "operation": {
    "argumentDefinitions": (v0/*: any*/),
    "kind": "Operation",
    "name": "PipelineTaskDetailsScheduleTaskMutation",
    "selections": (v1/*: any*/)
  },
  "params": {
    "cacheID": "cbcf9ccb8956b7b5b9ab109dc46d7aa4",
    "id": null,
    "metadata": {},
    "name": "PipelineTaskDetailsScheduleTaskMutation",
    "operationKind": "mutation",
    "text": "mutation PipelineTaskDetailsScheduleTaskMutation(\n  $input: SchedulePipelineNodeInput!\n) {\n  schedulePipelineNode(input: $input) {\n    problems {\n      message\n      field\n      type\n    }\n  }\n}\n"
  }
};
})();

(node as any).hash = "f1e0662162daa24bef387d13517562a6";

export default node;
