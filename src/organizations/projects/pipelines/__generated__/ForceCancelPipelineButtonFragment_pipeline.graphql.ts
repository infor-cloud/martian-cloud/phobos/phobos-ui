/**
 * @generated SignedSource<<d51db04f31aebfc8349cc053290b73a0>>
 * @lightSyntaxTransform
 * @nogrep
 */

/* tslint:disable */
/* eslint-disable */
// @ts-nocheck

import { Fragment, ReaderFragment } from 'relay-runtime';
import { FragmentRefs } from "relay-runtime";
export type ForceCancelPipelineButtonFragment_pipeline$data = {
  readonly id: string;
  readonly " $fragmentSpreads": FragmentRefs<"ForceCancelPipelineButtonDialogFragment_pipeline">;
  readonly " $fragmentType": "ForceCancelPipelineButtonFragment_pipeline";
};
export type ForceCancelPipelineButtonFragment_pipeline$key = {
  readonly " $data"?: ForceCancelPipelineButtonFragment_pipeline$data;
  readonly " $fragmentSpreads": FragmentRefs<"ForceCancelPipelineButtonFragment_pipeline">;
};

const node: ReaderFragment = {
  "argumentDefinitions": [],
  "kind": "Fragment",
  "metadata": null,
  "name": "ForceCancelPipelineButtonFragment_pipeline",
  "selections": [
    {
      "alias": null,
      "args": null,
      "kind": "ScalarField",
      "name": "id",
      "storageKey": null
    },
    {
      "args": null,
      "kind": "FragmentSpread",
      "name": "ForceCancelPipelineButtonDialogFragment_pipeline"
    }
  ],
  "type": "Pipeline",
  "abstractKey": null
};

(node as any).hash = "59e32978896a2c5e14ea64e9410b9a97";

export default node;
