/**
 * @generated SignedSource<<b1b8a1fc811710ef8970773cedc77042>>
 * @lightSyntaxTransform
 * @nogrep
 */

/* tslint:disable */
/* eslint-disable */
// @ts-nocheck

import { ConcreteRequest, Mutation } from 'relay-runtime';
export type PipelineNodeType = "ACTION" | "PIPELINE" | "STAGE" | "TASK" | "%future added value";
export type ProblemType = "BAD_REQUEST" | "CONFLICT" | "FORBIDDEN" | "NOT_FOUND" | "%future added value";
export type SchedulePipelineNodeInput = {
  clientMutationId?: string | null | undefined;
  cronSchedule?: CronScheduleInput | null | undefined;
  nodePath: string;
  nodeType: PipelineNodeType;
  pipelineId: string;
  scheduledStartTime?: any | null | undefined;
};
export type CronScheduleInput = {
  expression: string;
  timezone?: string | null | undefined;
};
export type PipelineNestedPipelineDetailsScheduleNestedPipelineMutation$variables = {
  input: SchedulePipelineNodeInput;
};
export type PipelineNestedPipelineDetailsScheduleNestedPipelineMutation$data = {
  readonly schedulePipelineNode: {
    readonly problems: ReadonlyArray<{
      readonly field: ReadonlyArray<string> | null | undefined;
      readonly message: string;
      readonly type: ProblemType;
    }>;
  };
};
export type PipelineNestedPipelineDetailsScheduleNestedPipelineMutation = {
  response: PipelineNestedPipelineDetailsScheduleNestedPipelineMutation$data;
  variables: PipelineNestedPipelineDetailsScheduleNestedPipelineMutation$variables;
};

const node: ConcreteRequest = (function(){
var v0 = [
  {
    "defaultValue": null,
    "kind": "LocalArgument",
    "name": "input"
  }
],
v1 = [
  {
    "alias": null,
    "args": [
      {
        "kind": "Variable",
        "name": "input",
        "variableName": "input"
      }
    ],
    "concreteType": "PipelineMutationPayload",
    "kind": "LinkedField",
    "name": "schedulePipelineNode",
    "plural": false,
    "selections": [
      {
        "alias": null,
        "args": null,
        "concreteType": "Problem",
        "kind": "LinkedField",
        "name": "problems",
        "plural": true,
        "selections": [
          {
            "alias": null,
            "args": null,
            "kind": "ScalarField",
            "name": "message",
            "storageKey": null
          },
          {
            "alias": null,
            "args": null,
            "kind": "ScalarField",
            "name": "field",
            "storageKey": null
          },
          {
            "alias": null,
            "args": null,
            "kind": "ScalarField",
            "name": "type",
            "storageKey": null
          }
        ],
        "storageKey": null
      }
    ],
    "storageKey": null
  }
];
return {
  "fragment": {
    "argumentDefinitions": (v0/*: any*/),
    "kind": "Fragment",
    "metadata": null,
    "name": "PipelineNestedPipelineDetailsScheduleNestedPipelineMutation",
    "selections": (v1/*: any*/),
    "type": "Mutation",
    "abstractKey": null
  },
  "kind": "Request",
  "operation": {
    "argumentDefinitions": (v0/*: any*/),
    "kind": "Operation",
    "name": "PipelineNestedPipelineDetailsScheduleNestedPipelineMutation",
    "selections": (v1/*: any*/)
  },
  "params": {
    "cacheID": "a3cb47beb1df3fb1cde2e8af47a4d72e",
    "id": null,
    "metadata": {},
    "name": "PipelineNestedPipelineDetailsScheduleNestedPipelineMutation",
    "operationKind": "mutation",
    "text": "mutation PipelineNestedPipelineDetailsScheduleNestedPipelineMutation(\n  $input: SchedulePipelineNodeInput!\n) {\n  schedulePipelineNode(input: $input) {\n    problems {\n      message\n      field\n      type\n    }\n  }\n}\n"
  }
};
})();

(node as any).hash = "28fa612c897280858d61bae0c440561a";

export default node;
