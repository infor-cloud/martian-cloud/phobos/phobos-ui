/**
 * @generated SignedSource<<466b9521f9a23aae1e5cda52fae98b8f>>
 * @lightSyntaxTransform
 * @nogrep
 */

/* tslint:disable */
/* eslint-disable */
// @ts-nocheck

import { ConcreteRequest, Mutation } from 'relay-runtime';
export type ProblemType = "BAD_REQUEST" | "CONFLICT" | "FORBIDDEN" | "NOT_FOUND" | "%future added value";
export type RunPipelineInput = {
  clientMutationId?: string | null | undefined;
  id: string;
};
export type PipelineNestedPipelineDetailsRunPipelineMutation$variables = {
  input: RunPipelineInput;
};
export type PipelineNestedPipelineDetailsRunPipelineMutation$data = {
  readonly runPipeline: {
    readonly problems: ReadonlyArray<{
      readonly field: ReadonlyArray<string> | null | undefined;
      readonly message: string;
      readonly type: ProblemType;
    }>;
  };
};
export type PipelineNestedPipelineDetailsRunPipelineMutation = {
  response: PipelineNestedPipelineDetailsRunPipelineMutation$data;
  variables: PipelineNestedPipelineDetailsRunPipelineMutation$variables;
};

const node: ConcreteRequest = (function(){
var v0 = [
  {
    "defaultValue": null,
    "kind": "LocalArgument",
    "name": "input"
  }
],
v1 = [
  {
    "alias": null,
    "args": [
      {
        "kind": "Variable",
        "name": "input",
        "variableName": "input"
      }
    ],
    "concreteType": "PipelineMutationPayload",
    "kind": "LinkedField",
    "name": "runPipeline",
    "plural": false,
    "selections": [
      {
        "alias": null,
        "args": null,
        "concreteType": "Problem",
        "kind": "LinkedField",
        "name": "problems",
        "plural": true,
        "selections": [
          {
            "alias": null,
            "args": null,
            "kind": "ScalarField",
            "name": "message",
            "storageKey": null
          },
          {
            "alias": null,
            "args": null,
            "kind": "ScalarField",
            "name": "field",
            "storageKey": null
          },
          {
            "alias": null,
            "args": null,
            "kind": "ScalarField",
            "name": "type",
            "storageKey": null
          }
        ],
        "storageKey": null
      }
    ],
    "storageKey": null
  }
];
return {
  "fragment": {
    "argumentDefinitions": (v0/*: any*/),
    "kind": "Fragment",
    "metadata": null,
    "name": "PipelineNestedPipelineDetailsRunPipelineMutation",
    "selections": (v1/*: any*/),
    "type": "Mutation",
    "abstractKey": null
  },
  "kind": "Request",
  "operation": {
    "argumentDefinitions": (v0/*: any*/),
    "kind": "Operation",
    "name": "PipelineNestedPipelineDetailsRunPipelineMutation",
    "selections": (v1/*: any*/)
  },
  "params": {
    "cacheID": "21a5ad5e4c7436559dcbeb3bbf63b006",
    "id": null,
    "metadata": {},
    "name": "PipelineNestedPipelineDetailsRunPipelineMutation",
    "operationKind": "mutation",
    "text": "mutation PipelineNestedPipelineDetailsRunPipelineMutation(\n  $input: RunPipelineInput!\n) {\n  runPipeline(input: $input) {\n    problems {\n      message\n      field\n      type\n    }\n  }\n}\n"
  }
};
})();

(node as any).hash = "291b8bf5494d163ba167a2f044635dea";

export default node;
