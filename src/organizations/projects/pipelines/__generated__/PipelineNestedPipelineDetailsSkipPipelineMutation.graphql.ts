/**
 * @generated SignedSource<<06b2c224a88e7e105d16eed9768c4ea9>>
 * @lightSyntaxTransform
 * @nogrep
 */

/* tslint:disable */
/* eslint-disable */
// @ts-nocheck

import { ConcreteRequest, Mutation } from 'relay-runtime';
export type PipelineNodeType = "ACTION" | "PIPELINE" | "STAGE" | "TASK" | "%future added value";
export type ProblemType = "BAD_REQUEST" | "CONFLICT" | "FORBIDDEN" | "NOT_FOUND" | "%future added value";
export type SkipPipelineNodeInput = {
  clientMutationId?: string | null | undefined;
  nodePath: string;
  nodeType: PipelineNodeType;
  pipelineId: string;
};
export type PipelineNestedPipelineDetailsSkipPipelineMutation$variables = {
  input: SkipPipelineNodeInput;
};
export type PipelineNestedPipelineDetailsSkipPipelineMutation$data = {
  readonly skipPipelineNode: {
    readonly problems: ReadonlyArray<{
      readonly field: ReadonlyArray<string> | null | undefined;
      readonly message: string;
      readonly type: ProblemType;
    }>;
  };
};
export type PipelineNestedPipelineDetailsSkipPipelineMutation = {
  response: PipelineNestedPipelineDetailsSkipPipelineMutation$data;
  variables: PipelineNestedPipelineDetailsSkipPipelineMutation$variables;
};

const node: ConcreteRequest = (function(){
var v0 = [
  {
    "defaultValue": null,
    "kind": "LocalArgument",
    "name": "input"
  }
],
v1 = [
  {
    "alias": null,
    "args": [
      {
        "kind": "Variable",
        "name": "input",
        "variableName": "input"
      }
    ],
    "concreteType": "PipelineMutationPayload",
    "kind": "LinkedField",
    "name": "skipPipelineNode",
    "plural": false,
    "selections": [
      {
        "alias": null,
        "args": null,
        "concreteType": "Problem",
        "kind": "LinkedField",
        "name": "problems",
        "plural": true,
        "selections": [
          {
            "alias": null,
            "args": null,
            "kind": "ScalarField",
            "name": "message",
            "storageKey": null
          },
          {
            "alias": null,
            "args": null,
            "kind": "ScalarField",
            "name": "field",
            "storageKey": null
          },
          {
            "alias": null,
            "args": null,
            "kind": "ScalarField",
            "name": "type",
            "storageKey": null
          }
        ],
        "storageKey": null
      }
    ],
    "storageKey": null
  }
];
return {
  "fragment": {
    "argumentDefinitions": (v0/*: any*/),
    "kind": "Fragment",
    "metadata": null,
    "name": "PipelineNestedPipelineDetailsSkipPipelineMutation",
    "selections": (v1/*: any*/),
    "type": "Mutation",
    "abstractKey": null
  },
  "kind": "Request",
  "operation": {
    "argumentDefinitions": (v0/*: any*/),
    "kind": "Operation",
    "name": "PipelineNestedPipelineDetailsSkipPipelineMutation",
    "selections": (v1/*: any*/)
  },
  "params": {
    "cacheID": "7154f3fe32a485fd93b0d8a1b7338995",
    "id": null,
    "metadata": {},
    "name": "PipelineNestedPipelineDetailsSkipPipelineMutation",
    "operationKind": "mutation",
    "text": "mutation PipelineNestedPipelineDetailsSkipPipelineMutation(\n  $input: SkipPipelineNodeInput!\n) {\n  skipPipelineNode(input: $input) {\n    problems {\n      message\n      field\n      type\n    }\n  }\n}\n"
  }
};
})();

(node as any).hash = "771aaf4230c5f369f42b354135b74308";

export default node;
