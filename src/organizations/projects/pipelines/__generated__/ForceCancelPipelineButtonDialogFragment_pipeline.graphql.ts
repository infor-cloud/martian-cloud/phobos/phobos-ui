/**
 * @generated SignedSource<<eaf5446f5c5a4a13fe29640761b48a53>>
 * @lightSyntaxTransform
 * @nogrep
 */

/* tslint:disable */
/* eslint-disable */
// @ts-nocheck

import { Fragment, ReaderFragment } from 'relay-runtime';
import { FragmentRefs } from "relay-runtime";
export type ForceCancelPipelineButtonDialogFragment_pipeline$data = {
  readonly project: {
    readonly name: string;
    readonly organizationName: string;
  };
  readonly " $fragmentType": "ForceCancelPipelineButtonDialogFragment_pipeline";
};
export type ForceCancelPipelineButtonDialogFragment_pipeline$key = {
  readonly " $data"?: ForceCancelPipelineButtonDialogFragment_pipeline$data;
  readonly " $fragmentSpreads": FragmentRefs<"ForceCancelPipelineButtonDialogFragment_pipeline">;
};

const node: ReaderFragment = {
  "argumentDefinitions": [],
  "kind": "Fragment",
  "metadata": null,
  "name": "ForceCancelPipelineButtonDialogFragment_pipeline",
  "selections": [
    {
      "alias": null,
      "args": null,
      "concreteType": "Project",
      "kind": "LinkedField",
      "name": "project",
      "plural": false,
      "selections": [
        {
          "alias": null,
          "args": null,
          "kind": "ScalarField",
          "name": "name",
          "storageKey": null
        },
        {
          "alias": null,
          "args": null,
          "kind": "ScalarField",
          "name": "organizationName",
          "storageKey": null
        }
      ],
      "storageKey": null
    }
  ],
  "type": "Pipeline",
  "abstractKey": null
};

(node as any).hash = "89fdcf4cb7525a009f162ebbefe32df0";

export default node;
