/**
 * @generated SignedSource<<a79eca5c7cfb975ca79378eeda4cae33>>
 * @lightSyntaxTransform
 * @nogrep
 */

/* tslint:disable */
/* eslint-disable */
// @ts-nocheck

import { Fragment, ReaderFragment } from 'relay-runtime';
import { FragmentRefs } from "relay-runtime";
export type PipelineDetailsFragment_details$data = {
  readonly metadata: {
    readonly prn: string;
  };
  readonly " $fragmentType": "PipelineDetailsFragment_details";
};
export type PipelineDetailsFragment_details$key = {
  readonly " $data"?: PipelineDetailsFragment_details$data;
  readonly " $fragmentSpreads": FragmentRefs<"PipelineDetailsFragment_details">;
};

const node: ReaderFragment = {
  "argumentDefinitions": [],
  "kind": "Fragment",
  "metadata": null,
  "name": "PipelineDetailsFragment_details",
  "selections": [
    {
      "alias": null,
      "args": null,
      "concreteType": "ResourceMetadata",
      "kind": "LinkedField",
      "name": "metadata",
      "plural": false,
      "selections": [
        {
          "alias": null,
          "args": null,
          "kind": "ScalarField",
          "name": "prn",
          "storageKey": null
        }
      ],
      "storageKey": null
    }
  ],
  "type": "Project",
  "abstractKey": null
};

(node as any).hash = "a93ac6d56d07a1da5935c4cf7fa87ae9";

export default node;
