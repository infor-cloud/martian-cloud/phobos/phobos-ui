/**
 * @generated SignedSource<<2a624afb6aa339c8721567cfe3adefa1>>
 * @lightSyntaxTransform
 * @nogrep
 */

/* tslint:disable */
/* eslint-disable */
// @ts-nocheck

import { ConcreteRequest, Mutation } from 'relay-runtime';
export type ProblemType = "BAD_REQUEST" | "CONFLICT" | "FORBIDDEN" | "NOT_FOUND" | "%future added value";
export type RunNestedPipelineInput = {
  clientMutationId?: string | null;
  id: string;
};
export type PipelineDetailsIndexRunNestedPipelineMutation$variables = {
  input: RunNestedPipelineInput;
};
export type PipelineDetailsIndexRunNestedPipelineMutation$data = {
  readonly runNestedPipeline: {
    readonly problems: ReadonlyArray<{
      readonly field: ReadonlyArray<string> | null;
      readonly message: string;
      readonly type: ProblemType;
    }>;
  };
};
export type PipelineDetailsIndexRunNestedPipelineMutation = {
  response: PipelineDetailsIndexRunNestedPipelineMutation$data;
  variables: PipelineDetailsIndexRunNestedPipelineMutation$variables;
};

const node: ConcreteRequest = (function(){
var v0 = [
  {
    "defaultValue": null,
    "kind": "LocalArgument",
    "name": "input"
  }
],
v1 = [
  {
    "alias": null,
    "args": [
      {
        "kind": "Variable",
        "name": "input",
        "variableName": "input"
      }
    ],
    "concreteType": "RunNestedPipelinePayload",
    "kind": "LinkedField",
    "name": "runNestedPipeline",
    "plural": false,
    "selections": [
      {
        "alias": null,
        "args": null,
        "concreteType": "Problem",
        "kind": "LinkedField",
        "name": "problems",
        "plural": true,
        "selections": [
          {
            "alias": null,
            "args": null,
            "kind": "ScalarField",
            "name": "message",
            "storageKey": null
          },
          {
            "alias": null,
            "args": null,
            "kind": "ScalarField",
            "name": "field",
            "storageKey": null
          },
          {
            "alias": null,
            "args": null,
            "kind": "ScalarField",
            "name": "type",
            "storageKey": null
          }
        ],
        "storageKey": null
      }
    ],
    "storageKey": null
  }
];
return {
  "fragment": {
    "argumentDefinitions": (v0/*: any*/),
    "kind": "Fragment",
    "metadata": null,
    "name": "PipelineDetailsIndexRunNestedPipelineMutation",
    "selections": (v1/*: any*/),
    "type": "Mutation",
    "abstractKey": null
  },
  "kind": "Request",
  "operation": {
    "argumentDefinitions": (v0/*: any*/),
    "kind": "Operation",
    "name": "PipelineDetailsIndexRunNestedPipelineMutation",
    "selections": (v1/*: any*/)
  },
  "params": {
    "cacheID": "2869ede065f23fc13044e4c4fa8b8d28",
    "id": null,
    "metadata": {},
    "name": "PipelineDetailsIndexRunNestedPipelineMutation",
    "operationKind": "mutation",
    "text": "mutation PipelineDetailsIndexRunNestedPipelineMutation(\n  $input: RunNestedPipelineInput!\n) {\n  runNestedPipeline(input: $input) {\n    problems {\n      message\n      field\n      type\n    }\n  }\n}\n"
  }
};
})();

(node as any).hash = "e0ad2c0f67e4bba2f9680c72b6a1cf93";

export default node;
