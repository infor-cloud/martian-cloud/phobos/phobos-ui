/**
 * @generated SignedSource<<7f88257f585a8eaad31b93a68157394b>>
 * @lightSyntaxTransform
 * @nogrep
 */

/* tslint:disable */
/* eslint-disable */
// @ts-nocheck

import { ConcreteRequest, Query } from 'relay-runtime';
import { FragmentRefs } from "relay-runtime";
export type PipelineNestedPipelineListPaginationQuery$variables = {
  after?: string | null | undefined;
  first?: number | null | undefined;
  nestedPipelinePath: string;
  pipelineId: string;
};
export type PipelineNestedPipelineListPaginationQuery$data = {
  readonly " $fragmentSpreads": FragmentRefs<"PipelineNestedPipelineListFragment_pipelines">;
};
export type PipelineNestedPipelineListPaginationQuery = {
  response: PipelineNestedPipelineListPaginationQuery$data;
  variables: PipelineNestedPipelineListPaginationQuery$variables;
};

const node: ConcreteRequest = (function(){
var v0 = [
  {
    "defaultValue": null,
    "kind": "LocalArgument",
    "name": "after"
  },
  {
    "defaultValue": null,
    "kind": "LocalArgument",
    "name": "first"
  },
  {
    "defaultValue": null,
    "kind": "LocalArgument",
    "name": "nestedPipelinePath"
  },
  {
    "defaultValue": null,
    "kind": "LocalArgument",
    "name": "pipelineId"
  }
],
v1 = {
  "alias": null,
  "args": null,
  "kind": "ScalarField",
  "name": "__typename",
  "storageKey": null
},
v2 = [
  {
    "kind": "Variable",
    "name": "after",
    "variableName": "after"
  },
  {
    "kind": "Variable",
    "name": "first",
    "variableName": "first"
  },
  {
    "kind": "Literal",
    "name": "sort",
    "value": "CREATED_AT_DESC"
  }
];
return {
  "fragment": {
    "argumentDefinitions": (v0/*: any*/),
    "kind": "Fragment",
    "metadata": null,
    "name": "PipelineNestedPipelineListPaginationQuery",
    "selections": [
      {
        "args": null,
        "kind": "FragmentSpread",
        "name": "PipelineNestedPipelineListFragment_pipelines"
      }
    ],
    "type": "Query",
    "abstractKey": null
  },
  "kind": "Request",
  "operation": {
    "argumentDefinitions": (v0/*: any*/),
    "kind": "Operation",
    "name": "PipelineNestedPipelineListPaginationQuery",
    "selections": [
      {
        "alias": null,
        "args": [
          {
            "kind": "Variable",
            "name": "nodePath",
            "variableName": "nestedPipelinePath"
          },
          {
            "kind": "Variable",
            "name": "pipelineId",
            "variableName": "pipelineId"
          }
        ],
        "concreteType": null,
        "kind": "LinkedField",
        "name": "pipelineNode",
        "plural": false,
        "selections": [
          (v1/*: any*/),
          {
            "kind": "InlineFragment",
            "selections": [
              {
                "alias": null,
                "args": (v2/*: any*/),
                "concreteType": "PipelineConnection",
                "kind": "LinkedField",
                "name": "pipelines",
                "plural": false,
                "selections": [
                  {
                    "alias": null,
                    "args": null,
                    "kind": "ScalarField",
                    "name": "totalCount",
                    "storageKey": null
                  },
                  {
                    "alias": null,
                    "args": null,
                    "concreteType": "PipelineEdge",
                    "kind": "LinkedField",
                    "name": "edges",
                    "plural": true,
                    "selections": [
                      {
                        "alias": null,
                        "args": null,
                        "concreteType": "Pipeline",
                        "kind": "LinkedField",
                        "name": "node",
                        "plural": false,
                        "selections": [
                          {
                            "alias": null,
                            "args": null,
                            "kind": "ScalarField",
                            "name": "id",
                            "storageKey": null
                          },
                          {
                            "alias": null,
                            "args": null,
                            "kind": "ScalarField",
                            "name": "status",
                            "storageKey": null
                          },
                          {
                            "alias": null,
                            "args": null,
                            "concreteType": "ResourceMetadata",
                            "kind": "LinkedField",
                            "name": "metadata",
                            "plural": false,
                            "selections": [
                              {
                                "alias": null,
                                "args": null,
                                "kind": "ScalarField",
                                "name": "createdAt",
                                "storageKey": null
                              }
                            ],
                            "storageKey": null
                          },
                          (v1/*: any*/)
                        ],
                        "storageKey": null
                      },
                      {
                        "alias": null,
                        "args": null,
                        "kind": "ScalarField",
                        "name": "cursor",
                        "storageKey": null
                      }
                    ],
                    "storageKey": null
                  },
                  {
                    "alias": null,
                    "args": null,
                    "concreteType": "PageInfo",
                    "kind": "LinkedField",
                    "name": "pageInfo",
                    "plural": false,
                    "selections": [
                      {
                        "alias": null,
                        "args": null,
                        "kind": "ScalarField",
                        "name": "endCursor",
                        "storageKey": null
                      },
                      {
                        "alias": null,
                        "args": null,
                        "kind": "ScalarField",
                        "name": "hasNextPage",
                        "storageKey": null
                      }
                    ],
                    "storageKey": null
                  }
                ],
                "storageKey": null
              },
              {
                "alias": null,
                "args": (v2/*: any*/),
                "filters": [
                  "sort"
                ],
                "handle": "connection",
                "key": "PipelineNestedPipelineList_pipelines",
                "kind": "LinkedHandle",
                "name": "pipelines"
              }
            ],
            "type": "NestedPipeline",
            "abstractKey": null
          }
        ],
        "storageKey": null
      }
    ]
  },
  "params": {
    "cacheID": "f696cac246b31d832a928bfa40172218",
    "id": null,
    "metadata": {},
    "name": "PipelineNestedPipelineListPaginationQuery",
    "operationKind": "query",
    "text": "query PipelineNestedPipelineListPaginationQuery(\n  $after: String\n  $first: Int\n  $nestedPipelinePath: String!\n  $pipelineId: String!\n) {\n  ...PipelineNestedPipelineListFragment_pipelines\n}\n\nfragment PipelineNestedPipelineListFragment_pipelines on Query {\n  pipelineNode(pipelineId: $pipelineId, nodePath: $nestedPipelinePath) {\n    __typename\n    ... on NestedPipeline {\n      pipelines(first: $first, after: $after, sort: CREATED_AT_DESC) {\n        totalCount\n        edges {\n          node {\n            id\n            ...PipelineNestedPipelineListItemFragment\n            __typename\n          }\n          cursor\n        }\n        pageInfo {\n          endCursor\n          hasNextPage\n        }\n      }\n    }\n  }\n}\n\nfragment PipelineNestedPipelineListItemFragment on Pipeline {\n  id\n  status\n  metadata {\n    createdAt\n  }\n}\n"
  }
};
})();

(node as any).hash = "225529576563fd657d2ef28e9486a576";

export default node;
