/**
 * @generated SignedSource<<1cc9bf87cb29804bcdb427cebe57fa63>>
 * @lightSyntaxTransform
 * @nogrep
 */

/* tslint:disable */
/* eslint-disable */
// @ts-nocheck

import { Fragment, ReaderFragment } from 'relay-runtime';
export type PipelineNodeStatus = "APPROVAL_PENDING" | "CANCELED" | "CANCELING" | "CREATED" | "DEFERRED" | "FAILED" | "PENDING" | "READY" | "RUNNING" | "SKIPPED" | "SUCCEEDED" | "WAITING" | "%future added value";
import { FragmentRefs } from "relay-runtime";
export type PipelineNestedPipelineDetailsFragment_details$data = {
  readonly id: string;
  readonly stages: ReadonlyArray<{
    readonly nestedPipelines: ReadonlyArray<{
      readonly cronSchedule: {
        readonly expression: string;
        readonly timezone: string;
      } | null | undefined;
      readonly errors: ReadonlyArray<string>;
      readonly latestPipeline: {
        readonly id: string;
        readonly " $fragmentSpreads": FragmentRefs<"PipelineNestedPipelineDetailsFragment_pipeline">;
      };
      readonly name: string;
      readonly path: string;
      readonly pipelines: {
        readonly totalCount: number;
      };
      readonly scheduledStartTime: any | null | undefined;
      readonly status: PipelineNodeStatus;
    }>;
  }>;
  readonly " $fragmentType": "PipelineNestedPipelineDetailsFragment_details";
};
export type PipelineNestedPipelineDetailsFragment_details$key = {
  readonly " $data"?: PipelineNestedPipelineDetailsFragment_details$data;
  readonly " $fragmentSpreads": FragmentRefs<"PipelineNestedPipelineDetailsFragment_details">;
};

const node: ReaderFragment = (function(){
var v0 = {
  "alias": null,
  "args": null,
  "kind": "ScalarField",
  "name": "id",
  "storageKey": null
};
return {
  "argumentDefinitions": [],
  "kind": "Fragment",
  "metadata": null,
  "name": "PipelineNestedPipelineDetailsFragment_details",
  "selections": [
    (v0/*: any*/),
    {
      "alias": null,
      "args": null,
      "concreteType": "PipelineStage",
      "kind": "LinkedField",
      "name": "stages",
      "plural": true,
      "selections": [
        {
          "alias": null,
          "args": null,
          "concreteType": "NestedPipeline",
          "kind": "LinkedField",
          "name": "nestedPipelines",
          "plural": true,
          "selections": [
            {
              "alias": null,
              "args": null,
              "kind": "ScalarField",
              "name": "path",
              "storageKey": null
            },
            {
              "alias": null,
              "args": null,
              "kind": "ScalarField",
              "name": "name",
              "storageKey": null
            },
            {
              "alias": null,
              "args": null,
              "kind": "ScalarField",
              "name": "status",
              "storageKey": null
            },
            {
              "alias": null,
              "args": null,
              "kind": "ScalarField",
              "name": "scheduledStartTime",
              "storageKey": null
            },
            {
              "alias": null,
              "args": null,
              "concreteType": "PipelineCronSchedule",
              "kind": "LinkedField",
              "name": "cronSchedule",
              "plural": false,
              "selections": [
                {
                  "alias": null,
                  "args": null,
                  "kind": "ScalarField",
                  "name": "expression",
                  "storageKey": null
                },
                {
                  "alias": null,
                  "args": null,
                  "kind": "ScalarField",
                  "name": "timezone",
                  "storageKey": null
                }
              ],
              "storageKey": null
            },
            {
              "alias": null,
              "args": null,
              "concreteType": "Pipeline",
              "kind": "LinkedField",
              "name": "latestPipeline",
              "plural": false,
              "selections": [
                (v0/*: any*/),
                {
                  "args": null,
                  "kind": "FragmentSpread",
                  "name": "PipelineNestedPipelineDetailsFragment_pipeline"
                }
              ],
              "storageKey": null
            },
            {
              "alias": null,
              "args": [
                {
                  "kind": "Literal",
                  "name": "first",
                  "value": 0
                }
              ],
              "concreteType": "PipelineConnection",
              "kind": "LinkedField",
              "name": "pipelines",
              "plural": false,
              "selections": [
                {
                  "alias": null,
                  "args": null,
                  "kind": "ScalarField",
                  "name": "totalCount",
                  "storageKey": null
                }
              ],
              "storageKey": "pipelines(first:0)"
            },
            {
              "alias": null,
              "args": null,
              "kind": "ScalarField",
              "name": "errors",
              "storageKey": null
            }
          ],
          "storageKey": null
        }
      ],
      "storageKey": null
    }
  ],
  "type": "Pipeline",
  "abstractKey": null
};
})();

(node as any).hash = "3f92d5477b9914bcad48229bbd8ee662";

export default node;
