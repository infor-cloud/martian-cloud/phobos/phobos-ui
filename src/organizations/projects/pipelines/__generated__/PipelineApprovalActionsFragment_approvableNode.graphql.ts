/**
 * @generated SignedSource<<38325935140baf8b1e31689cf0e7675a>>
 * @lightSyntaxTransform
 * @nogrep
 */

/* tslint:disable */
/* eslint-disable */
// @ts-nocheck

import { Fragment, ReaderFragment } from 'relay-runtime';
import { FragmentRefs } from "relay-runtime";
export type PipelineApprovalActionsFragment_approvableNode$data = {
  readonly __typename: string;
  readonly approvalRules: ReadonlyArray<{
    readonly approvalsRequired: number;
    readonly id: string;
    readonly name: string;
  }>;
  readonly approvals: ReadonlyArray<{
    readonly approvalRules: ReadonlyArray<{
      readonly id: string;
    }>;
    readonly approver: {
      readonly __typename: "ServiceAccount";
      readonly id: string;
      readonly name: string;
    } | {
      readonly __typename: "User";
      readonly email: string;
      readonly id: string;
    } | {
      // This will never be '%other', but we need some
      // value in case none of the concrete values match.
      readonly __typename: "%other";
    } | null | undefined;
    readonly id: string;
  }>;
  readonly " $fragmentType": "PipelineApprovalActionsFragment_approvableNode";
};
export type PipelineApprovalActionsFragment_approvableNode$key = {
  readonly " $data"?: PipelineApprovalActionsFragment_approvableNode$data;
  readonly " $fragmentSpreads": FragmentRefs<"PipelineApprovalActionsFragment_approvableNode">;
};

const node: ReaderFragment = (function(){
var v0 = {
  "alias": null,
  "args": null,
  "kind": "ScalarField",
  "name": "__typename",
  "storageKey": null
},
v1 = {
  "alias": null,
  "args": null,
  "kind": "ScalarField",
  "name": "id",
  "storageKey": null
},
v2 = {
  "alias": null,
  "args": null,
  "kind": "ScalarField",
  "name": "name",
  "storageKey": null
};
return {
  "argumentDefinitions": [],
  "kind": "Fragment",
  "metadata": null,
  "name": "PipelineApprovalActionsFragment_approvableNode",
  "selections": [
    (v0/*: any*/),
    {
      "alias": null,
      "args": null,
      "concreteType": "PipelineApproval",
      "kind": "LinkedField",
      "name": "approvals",
      "plural": true,
      "selections": [
        (v1/*: any*/),
        {
          "alias": null,
          "args": null,
          "concreteType": "ApprovalRule",
          "kind": "LinkedField",
          "name": "approvalRules",
          "plural": true,
          "selections": [
            (v1/*: any*/)
          ],
          "storageKey": null
        },
        {
          "alias": null,
          "args": null,
          "concreteType": null,
          "kind": "LinkedField",
          "name": "approver",
          "plural": false,
          "selections": [
            (v0/*: any*/),
            {
              "kind": "InlineFragment",
              "selections": [
                (v1/*: any*/),
                {
                  "alias": null,
                  "args": null,
                  "kind": "ScalarField",
                  "name": "email",
                  "storageKey": null
                }
              ],
              "type": "User",
              "abstractKey": null
            },
            {
              "kind": "InlineFragment",
              "selections": [
                (v1/*: any*/),
                (v2/*: any*/)
              ],
              "type": "ServiceAccount",
              "abstractKey": null
            }
          ],
          "storageKey": null
        }
      ],
      "storageKey": null
    },
    {
      "alias": null,
      "args": null,
      "concreteType": "ApprovalRule",
      "kind": "LinkedField",
      "name": "approvalRules",
      "plural": true,
      "selections": [
        (v1/*: any*/),
        (v2/*: any*/),
        {
          "alias": null,
          "args": null,
          "kind": "ScalarField",
          "name": "approvalsRequired",
          "storageKey": null
        }
      ],
      "storageKey": null
    }
  ],
  "type": "ApprovablePipelineNode",
  "abstractKey": "__isApprovablePipelineNode"
};
})();

(node as any).hash = "5e99b841c36efc7340e007f8f999ecbe";

export default node;
