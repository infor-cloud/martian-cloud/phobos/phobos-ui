/**
 * @generated SignedSource<<e5ba5caa6867fd013bad2d29426588f5>>
 * @lightSyntaxTransform
 * @nogrep
 */

/* tslint:disable */
/* eslint-disable */
// @ts-nocheck

import { Fragment, ReaderFragment } from 'relay-runtime';
export type VariableCategory = "ENVIRONMENT" | "HCL" | "%future added value";
import { FragmentRefs } from "relay-runtime";
export type PipelineVariableListItemFragment_variable$data = {
  readonly category: VariableCategory;
  readonly key: string;
  readonly value: string;
  readonly " $fragmentType": "PipelineVariableListItemFragment_variable";
};
export type PipelineVariableListItemFragment_variable$key = {
  readonly " $data"?: PipelineVariableListItemFragment_variable$data;
  readonly " $fragmentSpreads": FragmentRefs<"PipelineVariableListItemFragment_variable">;
};

const node: ReaderFragment = {
  "argumentDefinitions": [],
  "kind": "Fragment",
  "metadata": null,
  "name": "PipelineVariableListItemFragment_variable",
  "selections": [
    {
      "alias": null,
      "args": null,
      "kind": "ScalarField",
      "name": "key",
      "storageKey": null
    },
    {
      "alias": null,
      "args": null,
      "kind": "ScalarField",
      "name": "value",
      "storageKey": null
    },
    {
      "alias": null,
      "args": null,
      "kind": "ScalarField",
      "name": "category",
      "storageKey": null
    }
  ],
  "type": "PipelineVariable",
  "abstractKey": null
};

(node as any).hash = "6db261ac0d678d2f973e1aded0c89d35";

export default node;
