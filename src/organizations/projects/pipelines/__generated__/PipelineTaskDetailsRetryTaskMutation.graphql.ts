/**
 * @generated SignedSource<<656767841420b93e7d4ce1d103479554>>
 * @lightSyntaxTransform
 * @nogrep
 */

/* tslint:disable */
/* eslint-disable */
// @ts-nocheck

import { ConcreteRequest, Mutation } from 'relay-runtime';
export type ProblemType = "BAD_REQUEST" | "CONFLICT" | "FORBIDDEN" | "NOT_FOUND" | "%future added value";
export type RetryPipelineTaskInput = {
  clientMutationId?: string | null | undefined;
  pipelineId: string;
  taskPath: string;
};
export type PipelineTaskDetailsRetryTaskMutation$variables = {
  input: RetryPipelineTaskInput;
};
export type PipelineTaskDetailsRetryTaskMutation$data = {
  readonly retryPipelineTask: {
    readonly problems: ReadonlyArray<{
      readonly field: ReadonlyArray<string> | null | undefined;
      readonly message: string;
      readonly type: ProblemType;
    }>;
  };
};
export type PipelineTaskDetailsRetryTaskMutation = {
  response: PipelineTaskDetailsRetryTaskMutation$data;
  variables: PipelineTaskDetailsRetryTaskMutation$variables;
};

const node: ConcreteRequest = (function(){
var v0 = [
  {
    "defaultValue": null,
    "kind": "LocalArgument",
    "name": "input"
  }
],
v1 = [
  {
    "alias": null,
    "args": [
      {
        "kind": "Variable",
        "name": "input",
        "variableName": "input"
      }
    ],
    "concreteType": "PipelineMutationPayload",
    "kind": "LinkedField",
    "name": "retryPipelineTask",
    "plural": false,
    "selections": [
      {
        "alias": null,
        "args": null,
        "concreteType": "Problem",
        "kind": "LinkedField",
        "name": "problems",
        "plural": true,
        "selections": [
          {
            "alias": null,
            "args": null,
            "kind": "ScalarField",
            "name": "message",
            "storageKey": null
          },
          {
            "alias": null,
            "args": null,
            "kind": "ScalarField",
            "name": "field",
            "storageKey": null
          },
          {
            "alias": null,
            "args": null,
            "kind": "ScalarField",
            "name": "type",
            "storageKey": null
          }
        ],
        "storageKey": null
      }
    ],
    "storageKey": null
  }
];
return {
  "fragment": {
    "argumentDefinitions": (v0/*: any*/),
    "kind": "Fragment",
    "metadata": null,
    "name": "PipelineTaskDetailsRetryTaskMutation",
    "selections": (v1/*: any*/),
    "type": "Mutation",
    "abstractKey": null
  },
  "kind": "Request",
  "operation": {
    "argumentDefinitions": (v0/*: any*/),
    "kind": "Operation",
    "name": "PipelineTaskDetailsRetryTaskMutation",
    "selections": (v1/*: any*/)
  },
  "params": {
    "cacheID": "651001d57fe5193c64a33c0c3dbf9f78",
    "id": null,
    "metadata": {},
    "name": "PipelineTaskDetailsRetryTaskMutation",
    "operationKind": "mutation",
    "text": "mutation PipelineTaskDetailsRetryTaskMutation(\n  $input: RetryPipelineTaskInput!\n) {\n  retryPipelineTask(input: $input) {\n    problems {\n      message\n      field\n      type\n    }\n  }\n}\n"
  }
};
})();

(node as any).hash = "29fbcc5d727c4bfaa58b4e70ca6aa185";

export default node;
