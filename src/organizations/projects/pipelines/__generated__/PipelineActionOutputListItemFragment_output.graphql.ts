/**
 * @generated SignedSource<<218b35352f4501ec6864522232fa91a9>>
 * @lightSyntaxTransform
 * @nogrep
 */

/* tslint:disable */
/* eslint-disable */
// @ts-nocheck

import { Fragment, ReaderFragment } from 'relay-runtime';
import { FragmentRefs } from "relay-runtime";
export type PipelineActionOutputListItemFragment_output$data = {
  readonly name: string;
  readonly type: string;
  readonly value: string;
  readonly " $fragmentType": "PipelineActionOutputListItemFragment_output";
};
export type PipelineActionOutputListItemFragment_output$key = {
  readonly " $data"?: PipelineActionOutputListItemFragment_output$data;
  readonly " $fragmentSpreads": FragmentRefs<"PipelineActionOutputListItemFragment_output">;
};

const node: ReaderFragment = {
  "argumentDefinitions": [],
  "kind": "Fragment",
  "metadata": null,
  "name": "PipelineActionOutputListItemFragment_output",
  "selections": [
    {
      "alias": null,
      "args": null,
      "kind": "ScalarField",
      "name": "name",
      "storageKey": null
    },
    {
      "alias": null,
      "args": null,
      "kind": "ScalarField",
      "name": "value",
      "storageKey": null
    },
    {
      "alias": null,
      "args": null,
      "kind": "ScalarField",
      "name": "type",
      "storageKey": null
    }
  ],
  "type": "PipelineActionOutput",
  "abstractKey": null
};

(node as any).hash = "fc6c90771b5943a6e3dc4c14c0b81cbe";

export default node;
