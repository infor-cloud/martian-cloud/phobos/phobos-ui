/**
 * @generated SignedSource<<8ed22f3e9362410957ee4b9f6ff3ca28>>
 * @lightSyntaxTransform
 * @nogrep
 */

/* tslint:disable */
/* eslint-disable */
// @ts-nocheck

import { Fragment, ReaderFragment } from 'relay-runtime';
import { FragmentRefs } from "relay-runtime";
export type PipelineReplyListItemFragment_reply$data = {
  pipeline: any;
  readonly id: string;
  readonly " $fragmentSpreads": FragmentRefs<"CommentDetailsFragment_comment">;
  readonly " $fragmentType": "PipelineReplyListItemFragment_reply";
};
export type PipelineReplyListItemFragment_reply$key = {
  readonly " $data"?: PipelineReplyListItemFragment_reply$data;
  readonly " $fragmentSpreads": FragmentRefs<"PipelineReplyListItemFragment_reply">;
};

const node: ReaderFragment = {
  "argumentDefinitions": [],
  "kind": "Fragment",
  "metadata": null,
  "name": "PipelineReplyListItemFragment_reply",
  "selections": [
    {
      "alias": null,
      "args": null,
      "kind": "ScalarField",
      "name": "id",
      "storageKey": null
    },
    {
      "args": null,
      "kind": "FragmentSpread",
      "name": "CommentDetailsFragment_comment"
    }
  ],
  "type": "Comment",
  "abstractKey": null
};

(node as any).hash = "b4c35d5da9521d86c1bcfe0468e3cfdf";

export default node;
