/**
 * @generated SignedSource<<7b2da0ce74592fad2afa46052a6a493b>>
 * @lightSyntaxTransform
 * @nogrep
 */

/* tslint:disable */
/* eslint-disable */
// @ts-nocheck

import { Fragment, ReaderFragment } from 'relay-runtime';
export type PipelineNodeStatus = "APPROVAL_PENDING" | "CANCELED" | "CANCELING" | "CREATED" | "DEFERRED" | "FAILED" | "PENDING" | "READY" | "RUNNING" | "SKIPPED" | "SUCCEEDED" | "WAITING" | "%future added value";
export type PipelineType = "DEPLOYMENT" | "NESTED" | "RELEASE_LIFECYCLE" | "RUNBOOK" | "%future added value";
export type VariableCategory = "ENVIRONMENT" | "HCL" | "%future added value";
import { FragmentRefs } from "relay-runtime";
export type PipelineDetailsSidebarFragment_details$data = {
  readonly createdBy: string;
  readonly environment: {
    readonly id: string;
  } | null | undefined;
  readonly environmentName: string | null | undefined;
  readonly id: string;
  readonly metadata: {
    readonly createdAt: any;
  };
  readonly parentPipeline: {
    readonly id: string;
  } | null | undefined;
  readonly pipelineTemplate: {
    readonly id: string;
    readonly name: string | null | undefined;
    readonly semanticVersion: string | null | undefined;
    readonly versioned: boolean;
  };
  readonly release: {
    readonly id: string;
    readonly semanticVersion: string;
  } | null | undefined;
  readonly status: PipelineNodeStatus;
  readonly type: PipelineType;
  readonly variables: ReadonlyArray<{
    readonly category: VariableCategory;
  }>;
  readonly " $fragmentSpreads": FragmentRefs<"PipelineStagesFragment_stages">;
  readonly " $fragmentType": "PipelineDetailsSidebarFragment_details";
};
export type PipelineDetailsSidebarFragment_details$key = {
  readonly " $data"?: PipelineDetailsSidebarFragment_details$data;
  readonly " $fragmentSpreads": FragmentRefs<"PipelineDetailsSidebarFragment_details">;
};

const node: ReaderFragment = (function(){
var v0 = {
  "alias": null,
  "args": null,
  "kind": "ScalarField",
  "name": "id",
  "storageKey": null
},
v1 = [
  (v0/*: any*/)
],
v2 = {
  "alias": null,
  "args": null,
  "kind": "ScalarField",
  "name": "semanticVersion",
  "storageKey": null
};
return {
  "argumentDefinitions": [],
  "kind": "Fragment",
  "metadata": null,
  "name": "PipelineDetailsSidebarFragment_details",
  "selections": [
    (v0/*: any*/),
    {
      "alias": null,
      "args": null,
      "kind": "ScalarField",
      "name": "status",
      "storageKey": null
    },
    {
      "alias": null,
      "args": null,
      "kind": "ScalarField",
      "name": "createdBy",
      "storageKey": null
    },
    {
      "alias": null,
      "args": null,
      "kind": "ScalarField",
      "name": "type",
      "storageKey": null
    },
    {
      "alias": null,
      "args": null,
      "kind": "ScalarField",
      "name": "environmentName",
      "storageKey": null
    },
    {
      "alias": null,
      "args": null,
      "concreteType": "Environment",
      "kind": "LinkedField",
      "name": "environment",
      "plural": false,
      "selections": (v1/*: any*/),
      "storageKey": null
    },
    {
      "alias": null,
      "args": null,
      "concreteType": "ResourceMetadata",
      "kind": "LinkedField",
      "name": "metadata",
      "plural": false,
      "selections": [
        {
          "alias": null,
          "args": null,
          "kind": "ScalarField",
          "name": "createdAt",
          "storageKey": null
        }
      ],
      "storageKey": null
    },
    {
      "alias": null,
      "args": null,
      "concreteType": "PipelineTemplate",
      "kind": "LinkedField",
      "name": "pipelineTemplate",
      "plural": false,
      "selections": [
        (v0/*: any*/),
        {
          "alias": null,
          "args": null,
          "kind": "ScalarField",
          "name": "name",
          "storageKey": null
        },
        {
          "alias": null,
          "args": null,
          "kind": "ScalarField",
          "name": "versioned",
          "storageKey": null
        },
        (v2/*: any*/)
      ],
      "storageKey": null
    },
    {
      "alias": null,
      "args": null,
      "concreteType": "PipelineVariable",
      "kind": "LinkedField",
      "name": "variables",
      "plural": true,
      "selections": [
        {
          "alias": null,
          "args": null,
          "kind": "ScalarField",
          "name": "category",
          "storageKey": null
        }
      ],
      "storageKey": null
    },
    {
      "alias": null,
      "args": null,
      "concreteType": "Pipeline",
      "kind": "LinkedField",
      "name": "parentPipeline",
      "plural": false,
      "selections": (v1/*: any*/),
      "storageKey": null
    },
    {
      "alias": null,
      "args": null,
      "concreteType": "Release",
      "kind": "LinkedField",
      "name": "release",
      "plural": false,
      "selections": [
        (v0/*: any*/),
        (v2/*: any*/)
      ],
      "storageKey": null
    },
    {
      "args": null,
      "kind": "FragmentSpread",
      "name": "PipelineStagesFragment_stages"
    }
  ],
  "type": "Pipeline",
  "abstractKey": null
};
})();

(node as any).hash = "e9b2a4d9c2df1f05072a4048e5059826";

export default node;
