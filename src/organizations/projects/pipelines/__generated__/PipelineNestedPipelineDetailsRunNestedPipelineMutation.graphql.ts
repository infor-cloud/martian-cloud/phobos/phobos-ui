/**
 * @generated SignedSource<<d3ffca7fe517b124245c99fc825b40cd>>
 * @lightSyntaxTransform
 * @nogrep
 */

/* tslint:disable */
/* eslint-disable */
// @ts-nocheck

import { ConcreteRequest, Mutation } from 'relay-runtime';
export type ProblemType = "BAD_REQUEST" | "CONFLICT" | "FORBIDDEN" | "NOT_FOUND" | "%future added value";
export type RunNestedPipelineInput = {
  clientMutationId?: string | null;
  id: string;
};
export type PipelineNestedPipelineDetailsRunNestedPipelineMutation$variables = {
  input: RunNestedPipelineInput;
};
export type PipelineNestedPipelineDetailsRunNestedPipelineMutation$data = {
  readonly runNestedPipeline: {
    readonly problems: ReadonlyArray<{
      readonly field: ReadonlyArray<string> | null;
      readonly message: string;
      readonly type: ProblemType;
    }>;
  };
};
export type PipelineNestedPipelineDetailsRunNestedPipelineMutation = {
  response: PipelineNestedPipelineDetailsRunNestedPipelineMutation$data;
  variables: PipelineNestedPipelineDetailsRunNestedPipelineMutation$variables;
};

const node: ConcreteRequest = (function(){
var v0 = [
  {
    "defaultValue": null,
    "kind": "LocalArgument",
    "name": "input"
  }
],
v1 = [
  {
    "alias": null,
    "args": [
      {
        "kind": "Variable",
        "name": "input",
        "variableName": "input"
      }
    ],
    "concreteType": "RunNestedPipelinePayload",
    "kind": "LinkedField",
    "name": "runNestedPipeline",
    "plural": false,
    "selections": [
      {
        "alias": null,
        "args": null,
        "concreteType": "Problem",
        "kind": "LinkedField",
        "name": "problems",
        "plural": true,
        "selections": [
          {
            "alias": null,
            "args": null,
            "kind": "ScalarField",
            "name": "message",
            "storageKey": null
          },
          {
            "alias": null,
            "args": null,
            "kind": "ScalarField",
            "name": "field",
            "storageKey": null
          },
          {
            "alias": null,
            "args": null,
            "kind": "ScalarField",
            "name": "type",
            "storageKey": null
          }
        ],
        "storageKey": null
      }
    ],
    "storageKey": null
  }
];
return {
  "fragment": {
    "argumentDefinitions": (v0/*: any*/),
    "kind": "Fragment",
    "metadata": null,
    "name": "PipelineNestedPipelineDetailsRunNestedPipelineMutation",
    "selections": (v1/*: any*/),
    "type": "Mutation",
    "abstractKey": null
  },
  "kind": "Request",
  "operation": {
    "argumentDefinitions": (v0/*: any*/),
    "kind": "Operation",
    "name": "PipelineNestedPipelineDetailsRunNestedPipelineMutation",
    "selections": (v1/*: any*/)
  },
  "params": {
    "cacheID": "2fcc45af1deada1bafeae70fe6e663b1",
    "id": null,
    "metadata": {},
    "name": "PipelineNestedPipelineDetailsRunNestedPipelineMutation",
    "operationKind": "mutation",
    "text": "mutation PipelineNestedPipelineDetailsRunNestedPipelineMutation(\n  $input: RunNestedPipelineInput!\n) {\n  runNestedPipeline(input: $input) {\n    problems {\n      message\n      field\n      type\n    }\n  }\n}\n"
  }
};
})();

(node as any).hash = "4e6a80b22d7f7c82d49acfa4b1aa927a";

export default node;
