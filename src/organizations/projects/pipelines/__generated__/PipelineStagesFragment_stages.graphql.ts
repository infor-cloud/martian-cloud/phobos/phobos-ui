/**
 * @generated SignedSource<<412028f3640486addf2f153c85eff337>>
 * @lightSyntaxTransform
 * @nogrep
 */

/* tslint:disable */
/* eslint-disable */
// @ts-nocheck

import { Fragment, ReaderFragment } from 'relay-runtime';
export type PipelineNodeStatus = "APPROVAL_PENDING" | "CANCELED" | "CANCELING" | "CREATED" | "DEFERRED" | "FAILED" | "PENDING" | "READY" | "RUNNING" | "SKIPPED" | "SUCCEEDED" | "WAITING" | "%future added value";
import { FragmentRefs } from "relay-runtime";
export type PipelineStagesFragment_stages$data = {
  readonly stages: ReadonlyArray<{
    readonly name: string;
    readonly nestedPipelines: ReadonlyArray<{
      readonly dependencies: ReadonlyArray<string>;
      readonly name: string;
      readonly path: string;
      readonly status: PipelineNodeStatus;
      readonly when: string;
    }>;
    readonly path: string;
    readonly status: PipelineNodeStatus;
    readonly tasks: ReadonlyArray<{
      readonly dependencies: ReadonlyArray<string>;
      readonly name: string;
      readonly path: string;
      readonly status: PipelineNodeStatus;
      readonly when: string;
    }>;
  }>;
  readonly " $fragmentType": "PipelineStagesFragment_stages";
};
export type PipelineStagesFragment_stages$key = {
  readonly " $data"?: PipelineStagesFragment_stages$data;
  readonly " $fragmentSpreads": FragmentRefs<"PipelineStagesFragment_stages">;
};

const node: ReaderFragment = (function(){
var v0 = {
  "alias": null,
  "args": null,
  "kind": "ScalarField",
  "name": "path",
  "storageKey": null
},
v1 = {
  "alias": null,
  "args": null,
  "kind": "ScalarField",
  "name": "name",
  "storageKey": null
},
v2 = {
  "alias": null,
  "args": null,
  "kind": "ScalarField",
  "name": "status",
  "storageKey": null
},
v3 = [
  (v0/*: any*/),
  (v1/*: any*/),
  (v2/*: any*/),
  {
    "alias": null,
    "args": null,
    "kind": "ScalarField",
    "name": "when",
    "storageKey": null
  },
  {
    "alias": null,
    "args": null,
    "kind": "ScalarField",
    "name": "dependencies",
    "storageKey": null
  }
];
return {
  "argumentDefinitions": [],
  "kind": "Fragment",
  "metadata": null,
  "name": "PipelineStagesFragment_stages",
  "selections": [
    {
      "alias": null,
      "args": null,
      "concreteType": "PipelineStage",
      "kind": "LinkedField",
      "name": "stages",
      "plural": true,
      "selections": [
        (v0/*: any*/),
        (v1/*: any*/),
        (v2/*: any*/),
        {
          "alias": null,
          "args": null,
          "concreteType": "PipelineTask",
          "kind": "LinkedField",
          "name": "tasks",
          "plural": true,
          "selections": (v3/*: any*/),
          "storageKey": null
        },
        {
          "alias": null,
          "args": null,
          "concreteType": "NestedPipeline",
          "kind": "LinkedField",
          "name": "nestedPipelines",
          "plural": true,
          "selections": (v3/*: any*/),
          "storageKey": null
        }
      ],
      "storageKey": null
    }
  ],
  "type": "Pipeline",
  "abstractKey": null
};
})();

(node as any).hash = "56b86e1f14f44317657a14df0a256c19";

export default node;
