/**
 * @generated SignedSource<<5ed19bde99d0b8ecd1e4a754582eea1c>>
 * @lightSyntaxTransform
 * @nogrep
 */

/* tslint:disable */
/* eslint-disable */
// @ts-nocheck

import { Fragment, ReaderFragment } from 'relay-runtime';
export type PipelineNodeStatus = "APPROVAL_PENDING" | "CANCELED" | "CANCELING" | "CREATED" | "DEFERRED" | "FAILED" | "PENDING" | "READY" | "RUNNING" | "SKIPPED" | "SUCCEEDED" | "WAITING" | "%future added value";
import { FragmentRefs } from "relay-runtime";
export type PipelineStageIcons_stages$data = {
  readonly stages: ReadonlyArray<{
    readonly name: string;
    readonly path: string;
    readonly status: PipelineNodeStatus;
  }>;
  readonly " $fragmentType": "PipelineStageIcons_stages";
};
export type PipelineStageIcons_stages$key = {
  readonly " $data"?: PipelineStageIcons_stages$data;
  readonly " $fragmentSpreads": FragmentRefs<"PipelineStageIcons_stages">;
};

const node: ReaderFragment = {
  "argumentDefinitions": [],
  "kind": "Fragment",
  "metadata": null,
  "name": "PipelineStageIcons_stages",
  "selections": [
    {
      "alias": null,
      "args": null,
      "concreteType": "PipelineStage",
      "kind": "LinkedField",
      "name": "stages",
      "plural": true,
      "selections": [
        {
          "alias": null,
          "args": null,
          "kind": "ScalarField",
          "name": "path",
          "storageKey": null
        },
        {
          "alias": null,
          "args": null,
          "kind": "ScalarField",
          "name": "name",
          "storageKey": null
        },
        {
          "alias": null,
          "args": null,
          "kind": "ScalarField",
          "name": "status",
          "storageKey": null
        }
      ],
      "storageKey": null
    }
  ],
  "type": "Pipeline",
  "abstractKey": null
};

(node as any).hash = "647ab184e33a9fa0db0825e7e6843894";

export default node;
