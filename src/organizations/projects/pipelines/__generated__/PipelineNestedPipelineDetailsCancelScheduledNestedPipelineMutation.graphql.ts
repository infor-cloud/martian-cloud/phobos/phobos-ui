/**
 * @generated SignedSource<<b015d4965dbe3a8049198441541b86cf>>
 * @lightSyntaxTransform
 * @nogrep
 */

/* tslint:disable */
/* eslint-disable */
// @ts-nocheck

import { ConcreteRequest, Mutation } from 'relay-runtime';
export type PipelineNodeType = "ACTION" | "PIPELINE" | "STAGE" | "TASK" | "%future added value";
export type ProblemType = "BAD_REQUEST" | "CONFLICT" | "FORBIDDEN" | "NOT_FOUND" | "%future added value";
export type CancelPipelineNodeScheduleInput = {
  clientMutationId?: string | null | undefined;
  nodePath: string;
  nodeType: PipelineNodeType;
  pipelineId: string;
};
export type PipelineNestedPipelineDetailsCancelScheduledNestedPipelineMutation$variables = {
  input: CancelPipelineNodeScheduleInput;
};
export type PipelineNestedPipelineDetailsCancelScheduledNestedPipelineMutation$data = {
  readonly cancelPipelineNodeSchedule: {
    readonly problems: ReadonlyArray<{
      readonly field: ReadonlyArray<string> | null | undefined;
      readonly message: string;
      readonly type: ProblemType;
    }>;
  };
};
export type PipelineNestedPipelineDetailsCancelScheduledNestedPipelineMutation = {
  response: PipelineNestedPipelineDetailsCancelScheduledNestedPipelineMutation$data;
  variables: PipelineNestedPipelineDetailsCancelScheduledNestedPipelineMutation$variables;
};

const node: ConcreteRequest = (function(){
var v0 = [
  {
    "defaultValue": null,
    "kind": "LocalArgument",
    "name": "input"
  }
],
v1 = [
  {
    "alias": null,
    "args": [
      {
        "kind": "Variable",
        "name": "input",
        "variableName": "input"
      }
    ],
    "concreteType": "PipelineMutationPayload",
    "kind": "LinkedField",
    "name": "cancelPipelineNodeSchedule",
    "plural": false,
    "selections": [
      {
        "alias": null,
        "args": null,
        "concreteType": "Problem",
        "kind": "LinkedField",
        "name": "problems",
        "plural": true,
        "selections": [
          {
            "alias": null,
            "args": null,
            "kind": "ScalarField",
            "name": "message",
            "storageKey": null
          },
          {
            "alias": null,
            "args": null,
            "kind": "ScalarField",
            "name": "field",
            "storageKey": null
          },
          {
            "alias": null,
            "args": null,
            "kind": "ScalarField",
            "name": "type",
            "storageKey": null
          }
        ],
        "storageKey": null
      }
    ],
    "storageKey": null
  }
];
return {
  "fragment": {
    "argumentDefinitions": (v0/*: any*/),
    "kind": "Fragment",
    "metadata": null,
    "name": "PipelineNestedPipelineDetailsCancelScheduledNestedPipelineMutation",
    "selections": (v1/*: any*/),
    "type": "Mutation",
    "abstractKey": null
  },
  "kind": "Request",
  "operation": {
    "argumentDefinitions": (v0/*: any*/),
    "kind": "Operation",
    "name": "PipelineNestedPipelineDetailsCancelScheduledNestedPipelineMutation",
    "selections": (v1/*: any*/)
  },
  "params": {
    "cacheID": "31db439624f58c8f7285cba3e16ebbc1",
    "id": null,
    "metadata": {},
    "name": "PipelineNestedPipelineDetailsCancelScheduledNestedPipelineMutation",
    "operationKind": "mutation",
    "text": "mutation PipelineNestedPipelineDetailsCancelScheduledNestedPipelineMutation(\n  $input: CancelPipelineNodeScheduleInput!\n) {\n  cancelPipelineNodeSchedule(input: $input) {\n    problems {\n      message\n      field\n      type\n    }\n  }\n}\n"
  }
};
})();

(node as any).hash = "eba021156e0adad56b6fd759ce01d930";

export default node;
