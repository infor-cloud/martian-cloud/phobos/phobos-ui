/**
 * @generated SignedSource<<945390fcacc02519acc705a23c3f31a2>>
 * @lightSyntaxTransform
 * @nogrep
 */

/* tslint:disable */
/* eslint-disable */
// @ts-nocheck

import { Fragment, ReaderFragment } from 'relay-runtime';
export type PipelineNodeStatus = "APPROVAL_PENDING" | "CANCELED" | "CANCELING" | "CREATED" | "DEFERRED" | "FAILED" | "PENDING" | "READY" | "RUNNING" | "SKIPPED" | "SUCCEEDED" | "WAITING" | "%future added value";
import { FragmentRefs } from "relay-runtime";
export type PipelineDetailsFragment_pipeline$data = {
  readonly id: string;
  readonly metadata: {
    readonly version: string;
  };
  readonly stages: ReadonlyArray<{
    readonly name: string;
    readonly path: string;
    readonly status: PipelineNodeStatus;
    readonly tasks: ReadonlyArray<{
      readonly path: string;
    }>;
  }>;
  readonly status: PipelineNodeStatus;
  readonly " $fragmentSpreads": FragmentRefs<"PipelineDetailsIndexFragment_details" | "PipelineDetailsSidebarFragment_details" | "PipelineNestedPipelineDetailsFragment_details" | "PipelineTaskDetailsFragment_details">;
  readonly " $fragmentType": "PipelineDetailsFragment_pipeline";
};
export type PipelineDetailsFragment_pipeline$key = {
  readonly " $data"?: PipelineDetailsFragment_pipeline$data;
  readonly " $fragmentSpreads": FragmentRefs<"PipelineDetailsFragment_pipeline">;
};

const node: ReaderFragment = (function(){
var v0 = {
  "alias": null,
  "args": null,
  "kind": "ScalarField",
  "name": "status",
  "storageKey": null
},
v1 = {
  "alias": null,
  "args": null,
  "kind": "ScalarField",
  "name": "path",
  "storageKey": null
};
return {
  "argumentDefinitions": [],
  "kind": "Fragment",
  "metadata": null,
  "name": "PipelineDetailsFragment_pipeline",
  "selections": [
    {
      "alias": null,
      "args": null,
      "concreteType": "ResourceMetadata",
      "kind": "LinkedField",
      "name": "metadata",
      "plural": false,
      "selections": [
        {
          "alias": null,
          "args": null,
          "kind": "ScalarField",
          "name": "version",
          "storageKey": null
        }
      ],
      "storageKey": null
    },
    {
      "alias": null,
      "args": null,
      "kind": "ScalarField",
      "name": "id",
      "storageKey": null
    },
    (v0/*: any*/),
    {
      "alias": null,
      "args": null,
      "concreteType": "PipelineStage",
      "kind": "LinkedField",
      "name": "stages",
      "plural": true,
      "selections": [
        (v1/*: any*/),
        {
          "alias": null,
          "args": null,
          "kind": "ScalarField",
          "name": "name",
          "storageKey": null
        },
        (v0/*: any*/),
        {
          "alias": null,
          "args": null,
          "concreteType": "PipelineTask",
          "kind": "LinkedField",
          "name": "tasks",
          "plural": true,
          "selections": [
            (v1/*: any*/)
          ],
          "storageKey": null
        }
      ],
      "storageKey": null
    },
    {
      "args": null,
      "kind": "FragmentSpread",
      "name": "PipelineTaskDetailsFragment_details"
    },
    {
      "args": null,
      "kind": "FragmentSpread",
      "name": "PipelineDetailsSidebarFragment_details"
    },
    {
      "args": null,
      "kind": "FragmentSpread",
      "name": "PipelineDetailsIndexFragment_details"
    },
    {
      "args": null,
      "kind": "FragmentSpread",
      "name": "PipelineNestedPipelineDetailsFragment_details"
    }
  ],
  "type": "Pipeline",
  "abstractKey": null
};
})();

(node as any).hash = "94c6634447466a9d343005b034d5a3be";

export default node;
