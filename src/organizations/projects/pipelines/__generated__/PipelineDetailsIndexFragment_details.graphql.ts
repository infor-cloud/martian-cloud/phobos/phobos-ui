/**
 * @generated SignedSource<<09b71a45cd6b4110c965a316c996add0>>
 * @lightSyntaxTransform
 * @nogrep
 */

/* tslint:disable */
/* eslint-disable */
// @ts-nocheck

import { Fragment, ReaderFragment } from 'relay-runtime';
export type PipelineApprovalStatus = "APPROVED" | "NOT_REQUIRED" | "PENDING" | "%future added value";
export type PipelineNodeStatus = "APPROVAL_PENDING" | "CANCELED" | "CANCELING" | "CREATED" | "DEFERRED" | "FAILED" | "PENDING" | "READY" | "RUNNING" | "SKIPPED" | "SUCCEEDED" | "WAITING" | "%future added value";
export type PipelineType = "DEPLOYMENT" | "NESTED" | "RELEASE_LIFECYCLE" | "RUNBOOK" | "%future added value";
export type VariableCategory = "ENVIRONMENT" | "HCL" | "%future added value";
import { FragmentRefs } from "relay-runtime";
export type PipelineDetailsIndexFragment_details$data = {
  readonly annotations: ReadonlyArray<{
    readonly key: string;
  }>;
  readonly approvalStatus: PipelineApprovalStatus;
  readonly createdBy: string;
  readonly cronSchedule: {
    readonly expression: string;
    readonly timezone: string;
  } | null | undefined;
  readonly environment: {
    readonly id: string;
  } | null | undefined;
  readonly environmentName: string | null | undefined;
  readonly forceCancelAvailableAt: any | null | undefined;
  readonly id: string;
  readonly metadata: {
    readonly createdAt: any;
    readonly prn: string;
  };
  readonly parentPipeline: {
    readonly id: string;
  } | null | undefined;
  readonly pipelineTemplate: {
    readonly hclData: string;
    readonly id: string;
    readonly name: string | null | undefined;
    readonly semanticVersion: string | null | undefined;
    readonly versioned: boolean;
  };
  readonly release: {
    readonly id: string;
    readonly semanticVersion: string;
  } | null | undefined;
  readonly scheduledStartTime: any | null | undefined;
  readonly status: PipelineNodeStatus;
  readonly superseded: boolean;
  readonly type: PipelineType;
  readonly variables: ReadonlyArray<{
    readonly category: VariableCategory;
  }>;
  readonly when: string;
  readonly " $fragmentSpreads": FragmentRefs<"ForceCancelPipelineAlertFragment_pipeline" | "PipelineAnnotationsFragment_pipeline" | "PipelineApprovalActionsFragment_approvableNode" | "PipelineApprovalsFragment_approvals" | "PipelineStagesFragment_stages" | "PipelineThreadListFragment_pipeline" | "PipelineVariablesFragment">;
  readonly " $fragmentType": "PipelineDetailsIndexFragment_details";
};
export type PipelineDetailsIndexFragment_details$key = {
  readonly " $data"?: PipelineDetailsIndexFragment_details$data;
  readonly " $fragmentSpreads": FragmentRefs<"PipelineDetailsIndexFragment_details">;
};

const node: ReaderFragment = (function(){
var v0 = {
  "alias": null,
  "args": null,
  "kind": "ScalarField",
  "name": "id",
  "storageKey": null
},
v1 = [
  (v0/*: any*/)
],
v2 = {
  "alias": null,
  "args": null,
  "kind": "ScalarField",
  "name": "semanticVersion",
  "storageKey": null
};
return {
  "argumentDefinitions": [],
  "kind": "Fragment",
  "metadata": null,
  "name": "PipelineDetailsIndexFragment_details",
  "selections": [
    (v0/*: any*/),
    {
      "alias": null,
      "args": null,
      "kind": "ScalarField",
      "name": "status",
      "storageKey": null
    },
    {
      "alias": null,
      "args": null,
      "kind": "ScalarField",
      "name": "createdBy",
      "storageKey": null
    },
    {
      "alias": null,
      "args": null,
      "kind": "ScalarField",
      "name": "type",
      "storageKey": null
    },
    {
      "alias": null,
      "args": null,
      "kind": "ScalarField",
      "name": "environmentName",
      "storageKey": null
    },
    {
      "alias": null,
      "args": null,
      "concreteType": "Environment",
      "kind": "LinkedField",
      "name": "environment",
      "plural": false,
      "selections": (v1/*: any*/),
      "storageKey": null
    },
    {
      "alias": null,
      "args": null,
      "kind": "ScalarField",
      "name": "when",
      "storageKey": null
    },
    {
      "alias": null,
      "args": null,
      "kind": "ScalarField",
      "name": "superseded",
      "storageKey": null
    },
    {
      "alias": null,
      "args": null,
      "kind": "ScalarField",
      "name": "scheduledStartTime",
      "storageKey": null
    },
    {
      "alias": null,
      "args": null,
      "kind": "ScalarField",
      "name": "forceCancelAvailableAt",
      "storageKey": null
    },
    {
      "alias": null,
      "args": null,
      "concreteType": "PipelineCronSchedule",
      "kind": "LinkedField",
      "name": "cronSchedule",
      "plural": false,
      "selections": [
        {
          "alias": null,
          "args": null,
          "kind": "ScalarField",
          "name": "expression",
          "storageKey": null
        },
        {
          "alias": null,
          "args": null,
          "kind": "ScalarField",
          "name": "timezone",
          "storageKey": null
        }
      ],
      "storageKey": null
    },
    {
      "alias": null,
      "args": null,
      "concreteType": "ResourceMetadata",
      "kind": "LinkedField",
      "name": "metadata",
      "plural": false,
      "selections": [
        {
          "alias": null,
          "args": null,
          "kind": "ScalarField",
          "name": "createdAt",
          "storageKey": null
        },
        {
          "alias": null,
          "args": null,
          "kind": "ScalarField",
          "name": "prn",
          "storageKey": null
        }
      ],
      "storageKey": null
    },
    {
      "alias": null,
      "args": null,
      "concreteType": "PipelineTemplate",
      "kind": "LinkedField",
      "name": "pipelineTemplate",
      "plural": false,
      "selections": [
        (v0/*: any*/),
        {
          "alias": null,
          "args": null,
          "kind": "ScalarField",
          "name": "name",
          "storageKey": null
        },
        {
          "alias": null,
          "args": null,
          "kind": "ScalarField",
          "name": "versioned",
          "storageKey": null
        },
        (v2/*: any*/),
        {
          "alias": null,
          "args": null,
          "kind": "ScalarField",
          "name": "hclData",
          "storageKey": null
        }
      ],
      "storageKey": null
    },
    {
      "alias": null,
      "args": null,
      "concreteType": "PipelineVariable",
      "kind": "LinkedField",
      "name": "variables",
      "plural": true,
      "selections": [
        {
          "alias": null,
          "args": null,
          "kind": "ScalarField",
          "name": "category",
          "storageKey": null
        }
      ],
      "storageKey": null
    },
    {
      "alias": null,
      "args": null,
      "concreteType": "Pipeline",
      "kind": "LinkedField",
      "name": "parentPipeline",
      "plural": false,
      "selections": (v1/*: any*/),
      "storageKey": null
    },
    {
      "alias": null,
      "args": null,
      "concreteType": "Release",
      "kind": "LinkedField",
      "name": "release",
      "plural": false,
      "selections": [
        (v0/*: any*/),
        (v2/*: any*/)
      ],
      "storageKey": null
    },
    {
      "alias": null,
      "args": null,
      "concreteType": "PipelineAnnotation",
      "kind": "LinkedField",
      "name": "annotations",
      "plural": true,
      "selections": [
        {
          "alias": null,
          "args": null,
          "kind": "ScalarField",
          "name": "key",
          "storageKey": null
        }
      ],
      "storageKey": null
    },
    {
      "alias": null,
      "args": null,
      "kind": "ScalarField",
      "name": "approvalStatus",
      "storageKey": null
    },
    {
      "args": null,
      "kind": "FragmentSpread",
      "name": "PipelineVariablesFragment"
    },
    {
      "args": null,
      "kind": "FragmentSpread",
      "name": "PipelineStagesFragment_stages"
    },
    {
      "args": null,
      "kind": "FragmentSpread",
      "name": "PipelineThreadListFragment_pipeline"
    },
    {
      "args": null,
      "kind": "FragmentSpread",
      "name": "PipelineAnnotationsFragment_pipeline"
    },
    {
      "args": null,
      "kind": "FragmentSpread",
      "name": "PipelineApprovalsFragment_approvals"
    },
    {
      "args": null,
      "kind": "FragmentSpread",
      "name": "PipelineApprovalActionsFragment_approvableNode"
    },
    {
      "args": null,
      "kind": "FragmentSpread",
      "name": "ForceCancelPipelineAlertFragment_pipeline"
    }
  ],
  "type": "Pipeline",
  "abstractKey": null
};
})();

(node as any).hash = "225269ded5273c1e11e066f584d43c54";

export default node;
