/**
 * @generated SignedSource<<2367087439a21d710c2c71e8f35c3811>>
 * @lightSyntaxTransform
 * @nogrep
 */

/* tslint:disable */
/* eslint-disable */
// @ts-nocheck

import { Fragment, ReaderFragment } from 'relay-runtime';
export type PipelineNodeStatus = "APPROVAL_PENDING" | "CANCELED" | "CANCELING" | "CREATED" | "DEFERRED" | "FAILED" | "PENDING" | "READY" | "RUNNING" | "SKIPPED" | "SUCCEEDED" | "WAITING" | "%future added value";
export type PipelineType = "DEPLOYMENT" | "NESTED" | "RELEASE_LIFECYCLE" | "RUNBOOK" | "%future added value";
import { FragmentRefs } from "relay-runtime";
export type PipelineListItem_pipeline$data = {
  readonly annotations: ReadonlyArray<{
    readonly key: string;
  }>;
  readonly createdBy: string;
  readonly id: string;
  readonly metadata: {
    readonly createdAt: any;
  };
  readonly release: {
    readonly semanticVersion: string;
  } | null | undefined;
  readonly status: PipelineNodeStatus;
  readonly timestamps: {
    readonly completedAt: any | null | undefined;
    readonly startedAt: any | null | undefined;
  };
  readonly type: PipelineType;
  readonly " $fragmentSpreads": FragmentRefs<"PipelineAnnotationsFragment_pipeline" | "PipelineStageIcons_stages">;
  readonly " $fragmentType": "PipelineListItem_pipeline";
};
export type PipelineListItem_pipeline$key = {
  readonly " $data"?: PipelineListItem_pipeline$data;
  readonly " $fragmentSpreads": FragmentRefs<"PipelineListItem_pipeline">;
};

const node: ReaderFragment = {
  "argumentDefinitions": [],
  "kind": "Fragment",
  "metadata": null,
  "name": "PipelineListItem_pipeline",
  "selections": [
    {
      "alias": null,
      "args": null,
      "kind": "ScalarField",
      "name": "id",
      "storageKey": null
    },
    {
      "alias": null,
      "args": null,
      "kind": "ScalarField",
      "name": "createdBy",
      "storageKey": null
    },
    {
      "alias": null,
      "args": null,
      "kind": "ScalarField",
      "name": "status",
      "storageKey": null
    },
    {
      "alias": null,
      "args": null,
      "kind": "ScalarField",
      "name": "type",
      "storageKey": null
    },
    {
      "alias": null,
      "args": null,
      "concreteType": "ResourceMetadata",
      "kind": "LinkedField",
      "name": "metadata",
      "plural": false,
      "selections": [
        {
          "alias": null,
          "args": null,
          "kind": "ScalarField",
          "name": "createdAt",
          "storageKey": null
        }
      ],
      "storageKey": null
    },
    {
      "alias": null,
      "args": null,
      "concreteType": "Release",
      "kind": "LinkedField",
      "name": "release",
      "plural": false,
      "selections": [
        {
          "alias": null,
          "args": null,
          "kind": "ScalarField",
          "name": "semanticVersion",
          "storageKey": null
        }
      ],
      "storageKey": null
    },
    {
      "alias": null,
      "args": null,
      "concreteType": "PipelineAnnotation",
      "kind": "LinkedField",
      "name": "annotations",
      "plural": true,
      "selections": [
        {
          "alias": null,
          "args": null,
          "kind": "ScalarField",
          "name": "key",
          "storageKey": null
        }
      ],
      "storageKey": null
    },
    {
      "alias": null,
      "args": null,
      "concreteType": "PipelineTimestamps",
      "kind": "LinkedField",
      "name": "timestamps",
      "plural": false,
      "selections": [
        {
          "alias": null,
          "args": null,
          "kind": "ScalarField",
          "name": "startedAt",
          "storageKey": null
        },
        {
          "alias": null,
          "args": null,
          "kind": "ScalarField",
          "name": "completedAt",
          "storageKey": null
        }
      ],
      "storageKey": null
    },
    {
      "args": null,
      "kind": "FragmentSpread",
      "name": "PipelineStageIcons_stages"
    },
    {
      "args": null,
      "kind": "FragmentSpread",
      "name": "PipelineAnnotationsFragment_pipeline"
    }
  ],
  "type": "Pipeline",
  "abstractKey": null
};

(node as any).hash = "5d6848b64229a56022fa27696a107dfe";

export default node;
