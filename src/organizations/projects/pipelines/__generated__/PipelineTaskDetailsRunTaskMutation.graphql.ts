/**
 * @generated SignedSource<<39aa68a94f9f9f7387ce5fe41df86ddb>>
 * @lightSyntaxTransform
 * @nogrep
 */

/* tslint:disable */
/* eslint-disable */
// @ts-nocheck

import { ConcreteRequest, Mutation } from 'relay-runtime';
export type ProblemType = "BAD_REQUEST" | "CONFLICT" | "FORBIDDEN" | "NOT_FOUND" | "%future added value";
export type RunPipelineTaskInput = {
  clientMutationId?: string | null | undefined;
  pipelineId: string;
  taskPath: string;
};
export type PipelineTaskDetailsRunTaskMutation$variables = {
  input: RunPipelineTaskInput;
};
export type PipelineTaskDetailsRunTaskMutation$data = {
  readonly runPipelineTask: {
    readonly problems: ReadonlyArray<{
      readonly field: ReadonlyArray<string> | null | undefined;
      readonly message: string;
      readonly type: ProblemType;
    }>;
  };
};
export type PipelineTaskDetailsRunTaskMutation = {
  response: PipelineTaskDetailsRunTaskMutation$data;
  variables: PipelineTaskDetailsRunTaskMutation$variables;
};

const node: ConcreteRequest = (function(){
var v0 = [
  {
    "defaultValue": null,
    "kind": "LocalArgument",
    "name": "input"
  }
],
v1 = [
  {
    "alias": null,
    "args": [
      {
        "kind": "Variable",
        "name": "input",
        "variableName": "input"
      }
    ],
    "concreteType": "PipelineMutationPayload",
    "kind": "LinkedField",
    "name": "runPipelineTask",
    "plural": false,
    "selections": [
      {
        "alias": null,
        "args": null,
        "concreteType": "Problem",
        "kind": "LinkedField",
        "name": "problems",
        "plural": true,
        "selections": [
          {
            "alias": null,
            "args": null,
            "kind": "ScalarField",
            "name": "message",
            "storageKey": null
          },
          {
            "alias": null,
            "args": null,
            "kind": "ScalarField",
            "name": "field",
            "storageKey": null
          },
          {
            "alias": null,
            "args": null,
            "kind": "ScalarField",
            "name": "type",
            "storageKey": null
          }
        ],
        "storageKey": null
      }
    ],
    "storageKey": null
  }
];
return {
  "fragment": {
    "argumentDefinitions": (v0/*: any*/),
    "kind": "Fragment",
    "metadata": null,
    "name": "PipelineTaskDetailsRunTaskMutation",
    "selections": (v1/*: any*/),
    "type": "Mutation",
    "abstractKey": null
  },
  "kind": "Request",
  "operation": {
    "argumentDefinitions": (v0/*: any*/),
    "kind": "Operation",
    "name": "PipelineTaskDetailsRunTaskMutation",
    "selections": (v1/*: any*/)
  },
  "params": {
    "cacheID": "b70729642f4f07567e437a0c6add077c",
    "id": null,
    "metadata": {},
    "name": "PipelineTaskDetailsRunTaskMutation",
    "operationKind": "mutation",
    "text": "mutation PipelineTaskDetailsRunTaskMutation(\n  $input: RunPipelineTaskInput!\n) {\n  runPipelineTask(input: $input) {\n    problems {\n      message\n      field\n      type\n    }\n  }\n}\n"
  }
};
})();

(node as any).hash = "b6beeb191255a6c1f6819adaabc5a112";

export default node;
