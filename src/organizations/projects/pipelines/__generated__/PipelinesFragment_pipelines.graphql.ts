/**
 * @generated SignedSource<<7aee03d9f4f973f96c834655c7a16986>>
 * @lightSyntaxTransform
 * @nogrep
 */

/* tslint:disable */
/* eslint-disable */
// @ts-nocheck

import { Fragment, ReaderFragment } from 'relay-runtime';
import { FragmentRefs } from "relay-runtime";
export type PipelinesFragment_pipelines$data = {
  readonly metadata: {
    readonly prn: string;
  };
  readonly " $fragmentSpreads": FragmentRefs<"PipelineListFragment_project">;
  readonly " $fragmentType": "PipelinesFragment_pipelines";
};
export type PipelinesFragment_pipelines$key = {
  readonly " $data"?: PipelinesFragment_pipelines$data;
  readonly " $fragmentSpreads": FragmentRefs<"PipelinesFragment_pipelines">;
};

const node: ReaderFragment = {
  "argumentDefinitions": [],
  "kind": "Fragment",
  "metadata": null,
  "name": "PipelinesFragment_pipelines",
  "selections": [
    {
      "alias": null,
      "args": null,
      "concreteType": "ResourceMetadata",
      "kind": "LinkedField",
      "name": "metadata",
      "plural": false,
      "selections": [
        {
          "alias": null,
          "args": null,
          "kind": "ScalarField",
          "name": "prn",
          "storageKey": null
        }
      ],
      "storageKey": null
    },
    {
      "args": null,
      "kind": "FragmentSpread",
      "name": "PipelineListFragment_project"
    }
  ],
  "type": "Project",
  "abstractKey": null
};

(node as any).hash = "401179b52feda5b5ed544b3aa54de895";

export default node;
