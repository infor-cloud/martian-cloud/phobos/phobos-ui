/**
 * @generated SignedSource<<015b630152f339a4b7218d015580add8>>
 * @lightSyntaxTransform
 * @nogrep
 */

/* tslint:disable */
/* eslint-disable */
// @ts-nocheck

import { ConcreteRequest, Mutation } from 'relay-runtime';
export type ProblemType = "BAD_REQUEST" | "CONFLICT" | "FORBIDDEN" | "NOT_FOUND" | "%future added value";
export type CancelPipelineInput = {
  clientMutationId?: string | null | undefined;
  force?: boolean | null | undefined;
  id: string;
  metadata?: ResourceMetadataInput | null | undefined;
};
export type ResourceMetadataInput = {
  version: string;
};
export type PipelineDetailsIndexCancelPipelineMutation$variables = {
  input: CancelPipelineInput;
};
export type PipelineDetailsIndexCancelPipelineMutation$data = {
  readonly cancelPipeline: {
    readonly problems: ReadonlyArray<{
      readonly field: ReadonlyArray<string> | null | undefined;
      readonly message: string;
      readonly type: ProblemType;
    }>;
  };
};
export type PipelineDetailsIndexCancelPipelineMutation = {
  response: PipelineDetailsIndexCancelPipelineMutation$data;
  variables: PipelineDetailsIndexCancelPipelineMutation$variables;
};

const node: ConcreteRequest = (function(){
var v0 = [
  {
    "defaultValue": null,
    "kind": "LocalArgument",
    "name": "input"
  }
],
v1 = [
  {
    "alias": null,
    "args": [
      {
        "kind": "Variable",
        "name": "input",
        "variableName": "input"
      }
    ],
    "concreteType": "PipelineMutationPayload",
    "kind": "LinkedField",
    "name": "cancelPipeline",
    "plural": false,
    "selections": [
      {
        "alias": null,
        "args": null,
        "concreteType": "Problem",
        "kind": "LinkedField",
        "name": "problems",
        "plural": true,
        "selections": [
          {
            "alias": null,
            "args": null,
            "kind": "ScalarField",
            "name": "message",
            "storageKey": null
          },
          {
            "alias": null,
            "args": null,
            "kind": "ScalarField",
            "name": "field",
            "storageKey": null
          },
          {
            "alias": null,
            "args": null,
            "kind": "ScalarField",
            "name": "type",
            "storageKey": null
          }
        ],
        "storageKey": null
      }
    ],
    "storageKey": null
  }
];
return {
  "fragment": {
    "argumentDefinitions": (v0/*: any*/),
    "kind": "Fragment",
    "metadata": null,
    "name": "PipelineDetailsIndexCancelPipelineMutation",
    "selections": (v1/*: any*/),
    "type": "Mutation",
    "abstractKey": null
  },
  "kind": "Request",
  "operation": {
    "argumentDefinitions": (v0/*: any*/),
    "kind": "Operation",
    "name": "PipelineDetailsIndexCancelPipelineMutation",
    "selections": (v1/*: any*/)
  },
  "params": {
    "cacheID": "bd45aa91847ed99d9b19fa1ce55dae63",
    "id": null,
    "metadata": {},
    "name": "PipelineDetailsIndexCancelPipelineMutation",
    "operationKind": "mutation",
    "text": "mutation PipelineDetailsIndexCancelPipelineMutation(\n  $input: CancelPipelineInput!\n) {\n  cancelPipeline(input: $input) {\n    problems {\n      message\n      field\n      type\n    }\n  }\n}\n"
  }
};
})();

(node as any).hash = "da69ba1d116087353d8552eb017106b2";

export default node;
