/**
 * @generated SignedSource<<4d729f83505c9623a26162fdd95899a4>>
 * @lightSyntaxTransform
 * @nogrep
 */

/* tslint:disable */
/* eslint-disable */
// @ts-nocheck

import { Fragment, ReaderFragment } from 'relay-runtime';
import { FragmentRefs } from "relay-runtime";
export type ForceCancelPipelineAlertFragment_pipeline$data = {
  readonly forceCancelAvailableAt: any | null | undefined;
  readonly " $fragmentSpreads": FragmentRefs<"ForceCancelPipelineButtonFragment_pipeline">;
  readonly " $fragmentType": "ForceCancelPipelineAlertFragment_pipeline";
};
export type ForceCancelPipelineAlertFragment_pipeline$key = {
  readonly " $data"?: ForceCancelPipelineAlertFragment_pipeline$data;
  readonly " $fragmentSpreads": FragmentRefs<"ForceCancelPipelineAlertFragment_pipeline">;
};

const node: ReaderFragment = {
  "argumentDefinitions": [],
  "kind": "Fragment",
  "metadata": null,
  "name": "ForceCancelPipelineAlertFragment_pipeline",
  "selections": [
    {
      "alias": null,
      "args": null,
      "kind": "ScalarField",
      "name": "forceCancelAvailableAt",
      "storageKey": null
    },
    {
      "args": null,
      "kind": "FragmentSpread",
      "name": "ForceCancelPipelineButtonFragment_pipeline"
    }
  ],
  "type": "Pipeline",
  "abstractKey": null
};

(node as any).hash = "38dacf6a6ba944ada2545ca6084fc53f";

export default node;
