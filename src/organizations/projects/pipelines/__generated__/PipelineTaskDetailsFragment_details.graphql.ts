/**
 * @generated SignedSource<<1cecc647d855d3ef8abb23fc60d5dd56>>
 * @lightSyntaxTransform
 * @nogrep
 */

/* tslint:disable */
/* eslint-disable */
// @ts-nocheck

import { Fragment, ReaderFragment } from 'relay-runtime';
export type PipelineApprovalStatus = "APPROVED" | "NOT_REQUIRED" | "PENDING" | "%future added value";
export type PipelineNodeStatus = "APPROVAL_PENDING" | "CANCELED" | "CANCELING" | "CREATED" | "DEFERRED" | "FAILED" | "PENDING" | "READY" | "RUNNING" | "SKIPPED" | "SUCCEEDED" | "WAITING" | "%future added value";
import { FragmentRefs } from "relay-runtime";
export type PipelineTaskDetailsFragment_details$data = {
  readonly id: string;
  readonly stages: ReadonlyArray<{
    readonly tasks: ReadonlyArray<{
      readonly agentTags: ReadonlyArray<string>;
      readonly approvalStatus: PipelineApprovalStatus;
      readonly attemptCount: number;
      readonly cronSchedule: {
        readonly expression: string;
        readonly timezone: string;
      } | null | undefined;
      readonly currentJob: {
        readonly id: string;
      } | null | undefined;
      readonly errors: ReadonlyArray<string>;
      readonly interval: number | null | undefined;
      readonly jobs: {
        readonly totalCount: number;
      };
      readonly lastAttemptFinishedAt: any | null | undefined;
      readonly maxAttempts: number;
      readonly name: string;
      readonly path: string;
      readonly scheduledStartTime: any | null | undefined;
      readonly status: PipelineNodeStatus;
      readonly when: string;
      readonly " $fragmentSpreads": FragmentRefs<"PipelineActionOutputsFragment_outputs" | "PipelineApprovalActionsFragment_approvableNode" | "PipelineApprovalsFragment_approvals">;
    }>;
  }>;
  readonly status: PipelineNodeStatus;
  readonly " $fragmentType": "PipelineTaskDetailsFragment_details";
};
export type PipelineTaskDetailsFragment_details$key = {
  readonly " $data"?: PipelineTaskDetailsFragment_details$data;
  readonly " $fragmentSpreads": FragmentRefs<"PipelineTaskDetailsFragment_details">;
};

const node: ReaderFragment = (function(){
var v0 = {
  "alias": null,
  "args": null,
  "kind": "ScalarField",
  "name": "id",
  "storageKey": null
},
v1 = {
  "alias": null,
  "args": null,
  "kind": "ScalarField",
  "name": "status",
  "storageKey": null
};
return {
  "argumentDefinitions": [],
  "kind": "Fragment",
  "metadata": null,
  "name": "PipelineTaskDetailsFragment_details",
  "selections": [
    (v0/*: any*/),
    (v1/*: any*/),
    {
      "alias": null,
      "args": null,
      "concreteType": "PipelineStage",
      "kind": "LinkedField",
      "name": "stages",
      "plural": true,
      "selections": [
        {
          "alias": null,
          "args": null,
          "concreteType": "PipelineTask",
          "kind": "LinkedField",
          "name": "tasks",
          "plural": true,
          "selections": [
            {
              "args": null,
              "kind": "FragmentSpread",
              "name": "PipelineActionOutputsFragment_outputs"
            },
            {
              "args": null,
              "kind": "FragmentSpread",
              "name": "PipelineApprovalsFragment_approvals"
            },
            {
              "args": null,
              "kind": "FragmentSpread",
              "name": "PipelineApprovalActionsFragment_approvableNode"
            },
            {
              "alias": null,
              "args": null,
              "kind": "ScalarField",
              "name": "path",
              "storageKey": null
            },
            {
              "alias": null,
              "args": null,
              "kind": "ScalarField",
              "name": "name",
              "storageKey": null
            },
            (v1/*: any*/),
            {
              "alias": null,
              "args": null,
              "kind": "ScalarField",
              "name": "when",
              "storageKey": null
            },
            {
              "alias": null,
              "args": null,
              "kind": "ScalarField",
              "name": "agentTags",
              "storageKey": null
            },
            {
              "alias": null,
              "args": null,
              "kind": "ScalarField",
              "name": "scheduledStartTime",
              "storageKey": null
            },
            {
              "alias": null,
              "args": null,
              "concreteType": "PipelineCronSchedule",
              "kind": "LinkedField",
              "name": "cronSchedule",
              "plural": false,
              "selections": [
                {
                  "alias": null,
                  "args": null,
                  "kind": "ScalarField",
                  "name": "expression",
                  "storageKey": null
                },
                {
                  "alias": null,
                  "args": null,
                  "kind": "ScalarField",
                  "name": "timezone",
                  "storageKey": null
                }
              ],
              "storageKey": null
            },
            {
              "alias": null,
              "args": null,
              "kind": "ScalarField",
              "name": "maxAttempts",
              "storageKey": null
            },
            {
              "alias": null,
              "args": null,
              "kind": "ScalarField",
              "name": "attemptCount",
              "storageKey": null
            },
            {
              "alias": null,
              "args": null,
              "kind": "ScalarField",
              "name": "interval",
              "storageKey": null
            },
            {
              "alias": null,
              "args": null,
              "kind": "ScalarField",
              "name": "lastAttemptFinishedAt",
              "storageKey": null
            },
            {
              "alias": null,
              "args": null,
              "concreteType": "Job",
              "kind": "LinkedField",
              "name": "currentJob",
              "plural": false,
              "selections": [
                (v0/*: any*/)
              ],
              "storageKey": null
            },
            {
              "alias": null,
              "args": [
                {
                  "kind": "Literal",
                  "name": "first",
                  "value": 0
                }
              ],
              "concreteType": "JobConnection",
              "kind": "LinkedField",
              "name": "jobs",
              "plural": false,
              "selections": [
                {
                  "alias": null,
                  "args": null,
                  "kind": "ScalarField",
                  "name": "totalCount",
                  "storageKey": null
                }
              ],
              "storageKey": "jobs(first:0)"
            },
            {
              "alias": null,
              "args": null,
              "kind": "ScalarField",
              "name": "approvalStatus",
              "storageKey": null
            },
            {
              "alias": null,
              "args": null,
              "kind": "ScalarField",
              "name": "errors",
              "storageKey": null
            }
          ],
          "storageKey": null
        }
      ],
      "storageKey": null
    }
  ],
  "type": "Pipeline",
  "abstractKey": null
};
})();

(node as any).hash = "b7c73e9fbee290f2d2a4709caf3b6158";

export default node;
