/**
 * @generated SignedSource<<5597943c1c114ea61f43f2b1d6fb1e45>>
 * @lightSyntaxTransform
 * @nogrep
 */

/* tslint:disable */
/* eslint-disable */
// @ts-nocheck

import { Fragment, ReaderFragment } from 'relay-runtime';
import { FragmentRefs } from "relay-runtime";
export type PipelineListFragment_project$data = {
  readonly id: string;
  readonly " $fragmentType": "PipelineListFragment_project";
};
export type PipelineListFragment_project$key = {
  readonly " $data"?: PipelineListFragment_project$data;
  readonly " $fragmentSpreads": FragmentRefs<"PipelineListFragment_project">;
};

const node: ReaderFragment = {
  "argumentDefinitions": [],
  "kind": "Fragment",
  "metadata": null,
  "name": "PipelineListFragment_project",
  "selections": [
    {
      "alias": null,
      "args": null,
      "kind": "ScalarField",
      "name": "id",
      "storageKey": null
    }
  ],
  "type": "Project",
  "abstractKey": null
};

(node as any).hash = "fa470782fd8151bbc79aea77737fe1ac";

export default node;
