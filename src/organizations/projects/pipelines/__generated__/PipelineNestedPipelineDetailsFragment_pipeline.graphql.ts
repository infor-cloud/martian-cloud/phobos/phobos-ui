/**
 * @generated SignedSource<<58e5f0225554070808172e6413da29cb>>
 * @lightSyntaxTransform
 * @nogrep
 */

/* tslint:disable */
/* eslint-disable */
// @ts-nocheck

import { Fragment, ReaderFragment } from 'relay-runtime';
export type PipelineNodeStatus = "APPROVAL_PENDING" | "CANCELED" | "CANCELING" | "CREATED" | "DEFERRED" | "FAILED" | "PENDING" | "READY" | "RUNNING" | "SKIPPED" | "SUCCEEDED" | "WAITING" | "%future added value";
import { FragmentRefs } from "relay-runtime";
export type PipelineNestedPipelineDetailsFragment_pipeline$data = {
  readonly id: string;
  readonly status: PipelineNodeStatus;
  readonly when: string;
  readonly " $fragmentSpreads": FragmentRefs<"PipelineStagesFragment_stages">;
  readonly " $fragmentType": "PipelineNestedPipelineDetailsFragment_pipeline";
};
export type PipelineNestedPipelineDetailsFragment_pipeline$key = {
  readonly " $data"?: PipelineNestedPipelineDetailsFragment_pipeline$data;
  readonly " $fragmentSpreads": FragmentRefs<"PipelineNestedPipelineDetailsFragment_pipeline">;
};

const node: ReaderFragment = {
  "argumentDefinitions": [],
  "kind": "Fragment",
  "metadata": null,
  "name": "PipelineNestedPipelineDetailsFragment_pipeline",
  "selections": [
    {
      "alias": null,
      "args": null,
      "kind": "ScalarField",
      "name": "id",
      "storageKey": null
    },
    {
      "alias": null,
      "args": null,
      "kind": "ScalarField",
      "name": "when",
      "storageKey": null
    },
    {
      "alias": null,
      "args": null,
      "kind": "ScalarField",
      "name": "status",
      "storageKey": null
    },
    {
      "args": null,
      "kind": "FragmentSpread",
      "name": "PipelineStagesFragment_stages"
    }
  ],
  "type": "Pipeline",
  "abstractKey": null
};

(node as any).hash = "3ed9706342bb922aebfd588b3d0583df";

export default node;
