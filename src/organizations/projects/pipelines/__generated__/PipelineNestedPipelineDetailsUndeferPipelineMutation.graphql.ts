/**
 * @generated SignedSource<<b2f0d6b8e9f0d2c55453043675932b2b>>
 * @lightSyntaxTransform
 * @nogrep
 */

/* tslint:disable */
/* eslint-disable */
// @ts-nocheck

import { ConcreteRequest, Mutation } from 'relay-runtime';
export type PipelineNodeType = "ACTION" | "PIPELINE" | "STAGE" | "TASK" | "%future added value";
export type ProblemType = "BAD_REQUEST" | "CONFLICT" | "FORBIDDEN" | "NOT_FOUND" | "%future added value";
export type UndeferPipelineNodeInput = {
  clientMutationId?: string | null | undefined;
  nodePath: string;
  nodeType: PipelineNodeType;
  pipelineId: string;
};
export type PipelineNestedPipelineDetailsUndeferPipelineMutation$variables = {
  input: UndeferPipelineNodeInput;
};
export type PipelineNestedPipelineDetailsUndeferPipelineMutation$data = {
  readonly undeferPipelineNode: {
    readonly problems: ReadonlyArray<{
      readonly field: ReadonlyArray<string> | null | undefined;
      readonly message: string;
      readonly type: ProblemType;
    }>;
  };
};
export type PipelineNestedPipelineDetailsUndeferPipelineMutation = {
  response: PipelineNestedPipelineDetailsUndeferPipelineMutation$data;
  variables: PipelineNestedPipelineDetailsUndeferPipelineMutation$variables;
};

const node: ConcreteRequest = (function(){
var v0 = [
  {
    "defaultValue": null,
    "kind": "LocalArgument",
    "name": "input"
  }
],
v1 = [
  {
    "alias": null,
    "args": [
      {
        "kind": "Variable",
        "name": "input",
        "variableName": "input"
      }
    ],
    "concreteType": "PipelineMutationPayload",
    "kind": "LinkedField",
    "name": "undeferPipelineNode",
    "plural": false,
    "selections": [
      {
        "alias": null,
        "args": null,
        "concreteType": "Problem",
        "kind": "LinkedField",
        "name": "problems",
        "plural": true,
        "selections": [
          {
            "alias": null,
            "args": null,
            "kind": "ScalarField",
            "name": "message",
            "storageKey": null
          },
          {
            "alias": null,
            "args": null,
            "kind": "ScalarField",
            "name": "field",
            "storageKey": null
          },
          {
            "alias": null,
            "args": null,
            "kind": "ScalarField",
            "name": "type",
            "storageKey": null
          }
        ],
        "storageKey": null
      }
    ],
    "storageKey": null
  }
];
return {
  "fragment": {
    "argumentDefinitions": (v0/*: any*/),
    "kind": "Fragment",
    "metadata": null,
    "name": "PipelineNestedPipelineDetailsUndeferPipelineMutation",
    "selections": (v1/*: any*/),
    "type": "Mutation",
    "abstractKey": null
  },
  "kind": "Request",
  "operation": {
    "argumentDefinitions": (v0/*: any*/),
    "kind": "Operation",
    "name": "PipelineNestedPipelineDetailsUndeferPipelineMutation",
    "selections": (v1/*: any*/)
  },
  "params": {
    "cacheID": "d744643e8e87f0e8fdab77e24b212dd0",
    "id": null,
    "metadata": {},
    "name": "PipelineNestedPipelineDetailsUndeferPipelineMutation",
    "operationKind": "mutation",
    "text": "mutation PipelineNestedPipelineDetailsUndeferPipelineMutation(\n  $input: UndeferPipelineNodeInput!\n) {\n  undeferPipelineNode(input: $input) {\n    problems {\n      message\n      field\n      type\n    }\n  }\n}\n"
  }
};
})();

(node as any).hash = "8b4d8da51f4262f6b9ce057f02dfec96";

export default node;
