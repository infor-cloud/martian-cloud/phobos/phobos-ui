/**
 * @generated SignedSource<<e26ca681fd9ad2e82fec68523b757aea>>
 * @lightSyntaxTransform
 * @nogrep
 */

/* tslint:disable */
/* eslint-disable */
// @ts-nocheck

import { Fragment, ReaderFragment } from 'relay-runtime';
import { FragmentRefs } from "relay-runtime";
export type PipelineActionOutputsFragment_outputs$data = {
  readonly actions: ReadonlyArray<{
    readonly name: string;
    readonly outputs: ReadonlyArray<{
      readonly name: string;
      readonly " $fragmentSpreads": FragmentRefs<"PipelineActionOutputListItemFragment_output">;
    }>;
  }>;
  readonly " $fragmentType": "PipelineActionOutputsFragment_outputs";
};
export type PipelineActionOutputsFragment_outputs$key = {
  readonly " $data"?: PipelineActionOutputsFragment_outputs$data;
  readonly " $fragmentSpreads": FragmentRefs<"PipelineActionOutputsFragment_outputs">;
};

const node: ReaderFragment = (function(){
var v0 = {
  "alias": null,
  "args": null,
  "kind": "ScalarField",
  "name": "name",
  "storageKey": null
};
return {
  "argumentDefinitions": [],
  "kind": "Fragment",
  "metadata": null,
  "name": "PipelineActionOutputsFragment_outputs",
  "selections": [
    {
      "alias": null,
      "args": null,
      "concreteType": "PipelineAction",
      "kind": "LinkedField",
      "name": "actions",
      "plural": true,
      "selections": [
        (v0/*: any*/),
        {
          "alias": null,
          "args": null,
          "concreteType": "PipelineActionOutput",
          "kind": "LinkedField",
          "name": "outputs",
          "plural": true,
          "selections": [
            (v0/*: any*/),
            {
              "args": null,
              "kind": "FragmentSpread",
              "name": "PipelineActionOutputListItemFragment_output"
            }
          ],
          "storageKey": null
        }
      ],
      "storageKey": null
    }
  ],
  "type": "PipelineTask",
  "abstractKey": null
};
})();

(node as any).hash = "c3b94ed9add90c3fba6740f2f36078d5";

export default node;
