/**
 * @generated SignedSource<<1cd13560452599a61583cf94d163e6bb>>
 * @lightSyntaxTransform
 * @nogrep
 */

/* tslint:disable */
/* eslint-disable */
// @ts-nocheck

import { ConcreteRequest, GraphQLSubscription } from 'relay-runtime';
import { FragmentRefs } from "relay-runtime";
export type PipelineEventsSubscriptionInput = {
  lastSeenVersion?: string | null | undefined;
  pipelineId: string;
};
export type PipelineNestedPipelineDetailsSubscription$variables = {
  input: PipelineEventsSubscriptionInput;
};
export type PipelineNestedPipelineDetailsSubscription$data = {
  readonly pipelineEvents: {
    readonly action: string;
    readonly pipeline: {
      readonly id: string;
      readonly " $fragmentSpreads": FragmentRefs<"PipelineNestedPipelineDetailsFragment_pipeline">;
    };
  };
};
export type PipelineNestedPipelineDetailsSubscription = {
  response: PipelineNestedPipelineDetailsSubscription$data;
  variables: PipelineNestedPipelineDetailsSubscription$variables;
};

const node: ConcreteRequest = (function(){
var v0 = [
  {
    "defaultValue": null,
    "kind": "LocalArgument",
    "name": "input"
  }
],
v1 = [
  {
    "kind": "Variable",
    "name": "input",
    "variableName": "input"
  }
],
v2 = {
  "alias": null,
  "args": null,
  "kind": "ScalarField",
  "name": "action",
  "storageKey": null
},
v3 = {
  "alias": null,
  "args": null,
  "kind": "ScalarField",
  "name": "id",
  "storageKey": null
},
v4 = {
  "alias": null,
  "args": null,
  "kind": "ScalarField",
  "name": "when",
  "storageKey": null
},
v5 = {
  "alias": null,
  "args": null,
  "kind": "ScalarField",
  "name": "status",
  "storageKey": null
},
v6 = {
  "alias": null,
  "args": null,
  "kind": "ScalarField",
  "name": "path",
  "storageKey": null
},
v7 = {
  "alias": null,
  "args": null,
  "kind": "ScalarField",
  "name": "name",
  "storageKey": null
},
v8 = [
  (v6/*: any*/),
  (v7/*: any*/),
  (v5/*: any*/),
  (v4/*: any*/),
  {
    "alias": null,
    "args": null,
    "kind": "ScalarField",
    "name": "dependencies",
    "storageKey": null
  }
];
return {
  "fragment": {
    "argumentDefinitions": (v0/*: any*/),
    "kind": "Fragment",
    "metadata": null,
    "name": "PipelineNestedPipelineDetailsSubscription",
    "selections": [
      {
        "alias": null,
        "args": (v1/*: any*/),
        "concreteType": "PipelineEvent",
        "kind": "LinkedField",
        "name": "pipelineEvents",
        "plural": false,
        "selections": [
          (v2/*: any*/),
          {
            "alias": null,
            "args": null,
            "concreteType": "Pipeline",
            "kind": "LinkedField",
            "name": "pipeline",
            "plural": false,
            "selections": [
              (v3/*: any*/),
              {
                "args": null,
                "kind": "FragmentSpread",
                "name": "PipelineNestedPipelineDetailsFragment_pipeline"
              }
            ],
            "storageKey": null
          }
        ],
        "storageKey": null
      }
    ],
    "type": "Subscription",
    "abstractKey": null
  },
  "kind": "Request",
  "operation": {
    "argumentDefinitions": (v0/*: any*/),
    "kind": "Operation",
    "name": "PipelineNestedPipelineDetailsSubscription",
    "selections": [
      {
        "alias": null,
        "args": (v1/*: any*/),
        "concreteType": "PipelineEvent",
        "kind": "LinkedField",
        "name": "pipelineEvents",
        "plural": false,
        "selections": [
          (v2/*: any*/),
          {
            "alias": null,
            "args": null,
            "concreteType": "Pipeline",
            "kind": "LinkedField",
            "name": "pipeline",
            "plural": false,
            "selections": [
              (v3/*: any*/),
              (v4/*: any*/),
              (v5/*: any*/),
              {
                "alias": null,
                "args": null,
                "concreteType": "PipelineStage",
                "kind": "LinkedField",
                "name": "stages",
                "plural": true,
                "selections": [
                  (v6/*: any*/),
                  (v7/*: any*/),
                  (v5/*: any*/),
                  {
                    "alias": null,
                    "args": null,
                    "concreteType": "PipelineTask",
                    "kind": "LinkedField",
                    "name": "tasks",
                    "plural": true,
                    "selections": (v8/*: any*/),
                    "storageKey": null
                  },
                  {
                    "alias": null,
                    "args": null,
                    "concreteType": "NestedPipeline",
                    "kind": "LinkedField",
                    "name": "nestedPipelines",
                    "plural": true,
                    "selections": (v8/*: any*/),
                    "storageKey": null
                  }
                ],
                "storageKey": null
              }
            ],
            "storageKey": null
          }
        ],
        "storageKey": null
      }
    ]
  },
  "params": {
    "cacheID": "c6e59c5a7e68f8840fc365652df4b5db",
    "id": null,
    "metadata": {},
    "name": "PipelineNestedPipelineDetailsSubscription",
    "operationKind": "subscription",
    "text": "subscription PipelineNestedPipelineDetailsSubscription(\n  $input: PipelineEventsSubscriptionInput!\n) {\n  pipelineEvents(input: $input) {\n    action\n    pipeline {\n      id\n      ...PipelineNestedPipelineDetailsFragment_pipeline\n    }\n  }\n}\n\nfragment PipelineNestedPipelineDetailsFragment_pipeline on Pipeline {\n  id\n  when\n  status\n  ...PipelineStagesFragment_stages\n}\n\nfragment PipelineStagesFragment_stages on Pipeline {\n  stages {\n    path\n    name\n    status\n    tasks {\n      path\n      name\n      status\n      when\n      dependencies\n    }\n    nestedPipelines {\n      path\n      name\n      status\n      when\n      dependencies\n    }\n  }\n}\n"
  }
};
})();

(node as any).hash = "aa058d3b797f6ccab497bcde6c419258";

export default node;
