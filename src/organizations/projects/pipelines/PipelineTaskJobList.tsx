import { Box, Button, Paper, Table, TableBody, TableCell, TableContainer, TableHead, TableRow, Typography, useTheme } from '@mui/material';
import graphql from 'babel-plugin-relay/macro';
import { useLazyLoadQuery, usePaginationFragment } from 'react-relay/hooks';
import PipelineTaskJobListItem from './PipelineTaskJobListItem';
import { PipelineTaskJobListFragment_jobs$key } from './__generated__/PipelineTaskJobListFragment_jobs.graphql';
import { PipelineTaskJobListPaginationQuery } from './__generated__/PipelineTaskJobListPaginationQuery.graphql';
import { PipelineTaskJobListQuery } from './__generated__/PipelineTaskJobListQuery.graphql';

const query = graphql`
    query PipelineTaskJobListQuery($id: String!, $first: Int!, $after: String, $taskPath: String) {
        node(id: $id) {
            ... on Pipeline {
                id
                ...PipelineTaskJobListFragment_jobs
            }
        }
    }`;

interface Props {
    pipelineId: string
    taskPath: string
    onJobSelected: (id: string) => void
}

function PipelineTaskJobList({ pipelineId, taskPath, onJobSelected }: Props) {
    const theme = useTheme();

    const queryData = useLazyLoadQuery<PipelineTaskJobListQuery>(query, { id: pipelineId, taskPath, first: 10 }, { fetchPolicy: 'store-and-network' });

    const { data, loadNext, hasNext } = usePaginationFragment<PipelineTaskJobListPaginationQuery, PipelineTaskJobListFragment_jobs$key>(
        graphql`
        fragment PipelineTaskJobListFragment_jobs on Pipeline
        @refetchable(queryName: "PipelineTaskJobListPaginationQuery") {
                jobs(
                    first: $first
                    after: $after
                    sort: CREATED_AT_DESC
                    taskPath: $taskPath
                ) @connection(key: "PipelineTaskJobList_jobs") {
                    totalCount
                    edges {
                        node {
                            id
                            ...PipelineTaskJobListItemFragment
                        }
                    }
                }
            }
        `, queryData.node
    );

    return (
        <Box>
            {(!data?.jobs?.edges || data?.jobs?.edges?.length === 0) ? <Paper sx={{ p: 2, m: 2 }}>
                <Typography>This task does not have any jobs.</Typography>
            </Paper>
                :
                <Box>
                    <Paper sx={{ borderBottomLeftRadius: 0, borderBottomRightRadius: 0, border: `1px solid ${theme.palette.divider}` }}>
                        <Box padding={2}>
                            <Typography variant="subtitle1">
                                {data?.jobs.totalCount} job{data?.jobs.totalCount !== 1 && 's'}
                            </Typography>
                        </Box>
                    </Paper>
                    <TableContainer sx={{
                        borderLeft: `1px solid ${theme.palette.divider}`,
                        borderRight: `1px solid ${theme.palette.divider}`,
                        borderBottom: `1px solid ${theme.palette.divider}`,
                        borderBottomLeftRadius: 4,
                        borderBottomRightRadius: 4,
                    }}>
                        <Table
                            sx={{ minWidth: 650, tableLayout: 'fixed' }}
                            aria-label="agent jobs"
                        >
                            <TableHead>
                                <TableRow>
                                    <TableCell>Status</TableCell>
                                    <TableCell>ID</TableCell>
                                    <TableCell>Tags</TableCell>
                                    <TableCell>Agent</TableCell>
                                    <TableCell>Duration</TableCell>
                                    <TableCell>Created</TableCell>
                                </TableRow>
                            </TableHead>
                            <TableBody>
                                {data.jobs?.edges?.map((edge: any) => (
                                    <PipelineTaskJobListItem
                                        key={edge.node.id}
                                        fragmentRef={edge.node}
                                        onJobSelected={onJobSelected}
                                    />
                                ))}
                            </TableBody>
                        </Table>
                    </TableContainer>
                    {hasNext && <Box marginTop={2} display="flex" alignItems="center" justifyContent="center">
                        <Button color="inherit" onClick={() => loadNext(10)}>
                            Load More
                        </Button>
                    </Box>}
                </Box>}
        </Box>
    );
}

export default PipelineTaskJobList
