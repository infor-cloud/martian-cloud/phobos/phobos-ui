import { default as ArrowDropDownIcon } from '@mui/icons-material/ArrowDropDown';
import { Alert, Box, Button, ButtonGroup, Link as MLink, Menu, MenuItem, Paper, Tab, Tabs, Typography } from "@mui/material";
import graphql from 'babel-plugin-relay/macro';
import { Moment } from "moment";
import { enqueueSnackbar } from 'notistack';
import React, { useMemo, useState } from "react";
import { default as CalendarIcon } from '@mui/icons-material/CalendarMonthOutlined';
import { useFragment, useMutation, useSubscription } from "react-relay/hooks";
import { useNavigate, useOutletContext, useParams, useSearchParams } from "react-router-dom";
import { GraphQLSubscriptionConfig } from 'relay-runtime';
import ConfirmationDialog from '../../../common/ConfirmationDialog';
import { PipelineIcon } from '../../../common/Icons';
import TabContent from '../../../common/TabContent';
import Timestamp from '../../../common/Timestamp';
import Link from '../../../routes/Link';
import PipelineNestedPipelinesDialog from "./PipelineNestedPipelinesDialog";
import PipelineScheduleNodeDialog, { CronSchedule } from "./PipelineScheduleNodeDialog";
import PipelineStages from "./PipelineStages";
import PipelineStatusType from "./PipelineStatusType";
import { PipelineNestedPipelineDetailsCancelPipelineMutation } from "./__generated__/PipelineNestedPipelineDetailsCancelPipelineMutation.graphql";
import { PipelineNestedPipelineDetailsCancelScheduledNestedPipelineMutation } from "./__generated__/PipelineNestedPipelineDetailsCancelScheduledNestedPipelineMutation.graphql";
import { PipelineNestedPipelineDetailsFragment_details$key } from './__generated__/PipelineNestedPipelineDetailsFragment_details.graphql';
import { PipelineNestedPipelineDetailsFragment_pipeline$key } from './__generated__/PipelineNestedPipelineDetailsFragment_pipeline.graphql';
import { PipelineNestedPipelineDetailsRetryNestedPipelineMutation } from "./__generated__/PipelineNestedPipelineDetailsRetryNestedPipelineMutation.graphql";
import { PipelineNestedPipelineDetailsRunPipelineMutation } from "./__generated__/PipelineNestedPipelineDetailsRunPipelineMutation.graphql";
import { PipelineNestedPipelineDetailsScheduleNestedPipelineMutation } from "./__generated__/PipelineNestedPipelineDetailsScheduleNestedPipelineMutation.graphql";
import { PipelineNestedPipelineDetailsSubscription } from './__generated__/PipelineNestedPipelineDetailsSubscription.graphql';
import { PipelineNestedPipelineDetailsDeferPipelineMutation } from "./__generated__/PipelineNestedPipelineDetailsDeferPipelineMutation.graphql";
import { PipelineNestedPipelineDetailsUndeferPipelineMutation } from "./__generated__/PipelineNestedPipelineDetailsUndeferPipelineMutation.graphql";
import PipelineDeferPipelineNodeDialog from './PipelineDeferPipelineNodeDialog';
import { StyledCode } from '../../../common/StyledCode';
import { MutationError } from '../../../common/error';

const pipelineSubscription = graphql`subscription PipelineNestedPipelineDetailsSubscription($input: PipelineEventsSubscriptionInput!) {
    pipelineEvents(input: $input) {
      action
      pipeline {
        id
        ...PipelineNestedPipelineDetailsFragment_pipeline
      }
    }
  }`;

function PipelineNestedPipelineDetails() {
    const [searchParams, setSearchParams] = useSearchParams();
    const { pipelineId, nodePath } = useParams() as { nodePath: string, pipelineId: string };
    const navigate = useNavigate();
    const tab = searchParams.get('tab') || 'stages';
    const [menuAnchorEl, setMenuAnchorEl] = useState<Element | null>(null);
    const [showCancelPipelineConfirmation, setShowCancelPipelineConfirmation] = useState(false);
    const [showRunPipelineConfirmation, setShowRunPipelineConfirmation] = useState(false);
    const [showRetryPipelineConfirmation, setShowRetryPipelineConfirmation] = useState(false);
    const [NestedPipelinesDialogOpen, setNestedPipelinesDialogOpen] = useState(false);
    const [showScheduleNestedPipelineDialog, setShowScheduleNestedPipelineDialog] = useState(false);
    const [scheduleNestedPipelineError, setScheduleNestedPipelineError] = useState<MutationError | null>(null);
    const [showDeferPipelineDialog, setShowDeferPipelineDialog] = useState(false);
    const [showUndeferPipelineConfirmation, setShowUndeferPipelineConfirmation] = useState(false);

    const context = useOutletContext<PipelineNestedPipelineDetailsFragment_details$key>();

    const parentPipeline = useFragment<PipelineNestedPipelineDetailsFragment_details$key>(
        graphql`
            fragment PipelineNestedPipelineDetailsFragment_details on Pipeline
            {
                id
                stages {
                    nestedPipelines {
                        path
                        name
                        status
                        scheduledStartTime
                        cronSchedule {
                            expression
                            timezone
                        }
                        latestPipeline {
                            id
                            ...PipelineNestedPipelineDetailsFragment_pipeline
                        }
                        pipelines(first: 0) {
                            totalCount
                        }
                        errors
                    }
                }
            }
        `, context);

    const nestedPipelineNode = useMemo(() => {
        return parentPipeline.stages.flatMap(stage => stage.nestedPipelines).find(nested => nested.path === nodePath);
    }, [parentPipeline, nodePath]);

    const nestedPipeline = useFragment<PipelineNestedPipelineDetailsFragment_pipeline$key>(
        graphql`
            fragment PipelineNestedPipelineDetailsFragment_pipeline on Pipeline
            {
                id
                when
                status
                ...PipelineStagesFragment_stages
            }
        `, nestedPipelineNode?.latestPipeline);

    const pipelineSubscriptionConfig = useMemo<GraphQLSubscriptionConfig<PipelineNestedPipelineDetailsSubscription>>(() => ({
        variables: { input: { pipelineId: nestedPipelineNode?.latestPipeline.id || "" } },
        subscription: pipelineSubscription,
        onCompleted: () => console.log("Subscription completed"),
        onError: () => console.warn("Subscription error"),
    }), [nestedPipelineNode?.latestPipeline.id]);

    useSubscription<PipelineNestedPipelineDetailsSubscription>(pipelineSubscriptionConfig);

    const [commitCancelPipeline, commitCancelPipelineInFlight] = useMutation<PipelineNestedPipelineDetailsCancelPipelineMutation>(graphql`
        mutation PipelineNestedPipelineDetailsCancelPipelineMutation($input: CancelPipelineInput!) {
            cancelPipeline(input: $input) {
                problems {
                    message
                    field
                    type
                }
            }
        }
    `);

    const [commitRunPipeline, commitRunPipelineInFlight] = useMutation<PipelineNestedPipelineDetailsRunPipelineMutation>(graphql`
        mutation PipelineNestedPipelineDetailsRunPipelineMutation($input: RunPipelineInput!) {
            runPipeline(input: $input) {
                problems {
                    message
                    field
                    type
                }
            }
        }
    `);

    const [commitRetryPipeline, commitRetryPipelineInFlight] = useMutation<PipelineNestedPipelineDetailsRetryNestedPipelineMutation>(graphql`
        mutation PipelineNestedPipelineDetailsRetryNestedPipelineMutation($input: RetryNestedPipelineInput!) {
            retryNestedPipeline(input: $input) {
                problems {
                    message
                    field
                    type
                }
            }
        }
    `);

    const [commitScheduleNestedPipeline, commitScheduleNestedPipelineInFlight] = useMutation<PipelineNestedPipelineDetailsScheduleNestedPipelineMutation>(graphql`
        mutation PipelineNestedPipelineDetailsScheduleNestedPipelineMutation($input: SchedulePipelineNodeInput!) {
            schedulePipelineNode(input: $input) {
                problems {
                    message
                    field
                    type
                }
            }
        }
    `);

    const [commitCancelScheduledNestedPipeline, commitCancelScheduledNestedPipelineInFlight] = useMutation<PipelineNestedPipelineDetailsCancelScheduledNestedPipelineMutation>(graphql`
        mutation PipelineNestedPipelineDetailsCancelScheduledNestedPipelineMutation($input: CancelPipelineNodeScheduleInput!) {
            cancelPipelineNodeSchedule(input: $input) {
                problems {
                    message
                    field
                    type
                }
            }
        }
    `);

    const [commitDeferPipeline, commitDeferPipelineInFlight] = useMutation<PipelineNestedPipelineDetailsDeferPipelineMutation>(graphql`
        mutation PipelineNestedPipelineDetailsDeferPipelineMutation($input: DeferPipelineNodeInput!) {
            deferPipelineNode(input: $input) {
                problems {
                    message
                    field
                    type
                }
            }
        }
    `);

    const [commitUndeferPipeline, commitUndeferPipelineInFlight] = useMutation<PipelineNestedPipelineDetailsUndeferPipelineMutation>(graphql`
        mutation PipelineNestedPipelineDetailsUndeferPipelineMutation($input: UndeferPipelineNodeInput!) {
            undeferPipelineNode(input: $input) {
                problems {
                    message
                    field
                    type
                }
            }
        }
    `);

    const handleMutationError = (error: Error, completedCallback?: () => void) => {
        if (completedCallback) {
            completedCallback();
        }
        enqueueSnackbar(`Unexpected error: ${error.message}`, { variant: 'error' });
    }

    const handleMutationProblems = (problems: any, completedCallback?: () => void) => {
        if (completedCallback) {
            completedCallback();
        }
        if (problems && problems.length > 0) {
            enqueueSnackbar(problems.map((problem: any) => problem.message).join('; '), { variant: 'warning' });
        }
    }

    const onCancelPipeline = (pipelineId: string) => {
        commitCancelPipeline({
            variables: {
                input: {
                    id: pipelineId,
                },
            },
            onCompleted: data => handleMutationProblems(data.cancelPipeline?.problems, () => setShowCancelPipelineConfirmation(false)),
            onError: error => handleMutationError(error, () => setShowCancelPipelineConfirmation(false))
        });
    }

    const onRunPipeline = (pipelineId: string) => {
        commitRunPipeline({
            variables: {
                input: {
                    id: pipelineId,
                },
            },
            onCompleted: data => handleMutationProblems(data.runPipeline?.problems, () => setShowRunPipelineConfirmation(false)),
            onError: error => handleMutationError(error, () => setShowRunPipelineConfirmation(false))
        });
    }

    const onRetryPipeline = (parentPipelineId: string, parentNestedPipelineNodePath: string) => {
        commitRetryPipeline({
            variables: {
                input: {
                    parentPipelineId: parentPipelineId,
                    parentNestedPipelineNodePath: parentNestedPipelineNodePath,
                },
            },
            onCompleted: data => handleMutationProblems(data.retryNestedPipeline?.problems, () => setShowRetryPipelineConfirmation(false)),
            onError: error => handleMutationError(error, () => setShowRetryPipelineConfirmation(false))
        });
    }

    const onScheduleNestedPipeline = (nodePath: string, scheduledTime: Moment | null, cronSchedule: CronSchedule | null) => {
        commitScheduleNestedPipeline({
            variables: {
                input: {
                    pipelineId: pipelineId,
                    nodePath: nodePath,
                    nodeType: 'PIPELINE',
                    scheduledStartTime: scheduledTime?.utc().toISOString(),
                    cronSchedule: cronSchedule
                },
            },
            onCompleted: data => {
                if (data.schedulePipelineNode?.problems.length) {
                    setScheduleNestedPipelineError({
                        severity: 'warning',
                        message: data.schedulePipelineNode.problems.map((problem: any) => problem.message).join(', ')
                    });
                } else {
                    setScheduleNestedPipelineError(null);
                    setShowScheduleNestedPipelineDialog(false);
                }
            },
            onError: error => handleMutationError(error, () => setShowScheduleNestedPipelineDialog(false))
        });
    }

    const onCancelScheduledNestedPipeline = (nodePath: string) => {
        commitCancelScheduledNestedPipeline({
            variables: {
                input: {
                    pipelineId: pipelineId,
                    nodePath: nodePath,
                    nodeType: 'PIPELINE',
                },
            },
            onCompleted: data => handleMutationProblems(data.cancelPipelineNodeSchedule?.problems, () => setShowScheduleNestedPipelineDialog(false)),
            onError: error => handleMutationError(error, () => setShowScheduleNestedPipelineDialog(false))
        });
    }

    const onDeferPipeline = (reason: string) => {
        commitDeferPipeline({
            variables: {
                input: {
                    pipelineId: pipelineId,
                    nodePath: nestedPipelineNode?.path as string,
                    nodeType: 'PIPELINE',
                    reason: reason,
                },
            },
            onCompleted: data => handleMutationProblems(data.deferPipelineNode?.problems, () => setShowDeferPipelineDialog(false)),
            onError: error => handleMutationError(error, () => setShowDeferPipelineDialog(false))
        });
    }

    const onUndeferPipeline = (nodePath: string) => {
        commitUndeferPipeline({
            variables: {
                input: {
                    pipelineId: pipelineId,
                    nodePath: nodePath,
                    nodeType: 'PIPELINE',
                },
            },
            onCompleted: data => handleMutationProblems(data.undeferPipelineNode?.problems, () => setShowUndeferPipelineConfirmation(false)),
            onError: error => handleMutationError(error, () => setShowUndeferPipelineConfirmation(false))
        });
    }

    const onPipelineSelected = (pipelineId: string) => {
        navigate(`../../${pipelineId}`);
        setNestedPipelinesDialogOpen(false);
    }

    const onTabChange = (event: React.SyntheticEvent, newValue: string) => {
        searchParams.set('tab', newValue);
        setSearchParams(searchParams, { replace: true });
    };

    const actions = useMemo(() => [
        {
            label: 'Cancel Pipeline',
            condition: nestedPipeline?.status === 'RUNNING',
            handler: () => setShowCancelPipelineConfirmation(true)
        },
        {
            label: 'Restore Pipeline',
            condition: nestedPipeline?.status === 'DEFERRED',
            handler: () => setShowUndeferPipelineConfirmation(true)
        },
        {
            label: 'Set Schedule',
            condition: ['CREATED', 'APPROVAL_PENDING'].includes(nestedPipeline?.status ?? ''),
            handler: () => setShowScheduleNestedPipelineDialog(true)
        },
        {
            label: 'Retry Pipeline',
            condition: ['SUCCEEDED', 'FAILED', 'CANCELED'].includes(nestedPipeline?.status ?? ''),
            handler: () => setShowRetryPipelineConfirmation(true)
        },
        {
            label: 'Run Pipeline',
            condition: nestedPipeline?.status === 'READY' && nestedPipeline.when === 'manual',
            handler: () => setShowRunPipelineConfirmation(true)
        },
        {
            label: 'Update Schedule',
            condition: nestedPipeline?.status === 'WAITING',
            handler: () => setShowScheduleNestedPipelineDialog(true)
        },
        {
            label: 'Defer Nested Pipeline',
            condition: nestedPipeline?.status === 'WAITING' || nestedPipeline?.status === 'READY',
            handler: () => setShowDeferPipelineDialog(true)
        },
        {
            label: 'Schedule Nested Pipeline',
            condition: nestedPipeline?.status === 'READY',
            handler: () => setShowScheduleNestedPipelineDialog(true)
        }
    ], [nestedPipeline]);

    const mainButton = useMemo(() => actions.find(action => action.condition), [actions]);
    const menuItems = useMemo(() => actions.filter(action => action !== mainButton && action.condition), [actions, mainButton]);

    return nestedPipelineNode && nestedPipeline ? (
        <Box>
            <Box display="flex" alignItems="center" justifyContent="space-between" marginBottom={2}>
                <Box display="flex" alignItems="center">
                    <PipelineIcon />
                    <Typography ml={1} variant="h6">
                        Nested Pipeline
                    </Typography>
                </Box>
                {mainButton && menuItems.length === 0 && (
                    <Button variant="outlined" color="primary" onClick={mainButton.handler}>
                        {mainButton.label}
                    </Button>
                )}
                {mainButton && menuItems.length > 0 && <>
                    <ButtonGroup variant="outlined" color="primary">
                        <Button variant="outlined" color="primary" onClick={mainButton.handler}>
                            {mainButton.label}
                        </Button>
                        <Button
                            color="primary"
                            size="small"
                            aria-label="more options menu"
                            aria-haspopup="menu"
                            onClick={(event) => setMenuAnchorEl(event.currentTarget)}
                        >
                            <ArrowDropDownIcon fontSize="small" />
                        </Button>
                    </ButtonGroup>
                    <Menu
                        id="pipeline-more-options-menu"
                        anchorEl={menuAnchorEl}
                        open={Boolean(menuAnchorEl)}
                        onClose={() => setMenuAnchorEl(null)}
                        anchorOrigin={{
                            vertical: 'bottom',
                            horizontal: 'right',
                        }}
                        transformOrigin={{
                            vertical: 'top',
                            horizontal: 'right',
                        }}
                    >
                        {menuItems.map((item, index) => (
                            <MenuItem
                                key={index}
                                onClick={() => {
                                    item.handler();
                                    setMenuAnchorEl(null);
                                }}
                            >
                                {item.label}
                            </MenuItem>
                        ))}
                    </Menu>
                </>}
            </Box>
            <Paper variant="outlined" sx={{ padding: 2, display: 'flex', alignItems: 'center', marginBottom: 2 }}>
                {PipelineStatusType[nestedPipeline.status].icon}
                <Typography marginLeft={1} variant="body2" component="div">
                    Nested Pipeline
                    <Link to={`../../${nestedPipeline.id}`} variant="body2" sx={{ margin: '0 4px', fontWeight: 600 }}>
                        {nestedPipelineNode.name}
                    </Link>
                    {PipelineStatusType[nestedPipeline.status].tooltip}
                </Typography>
            </Paper>
            {(nestedPipelineNode.scheduledStartTime || nestedPipelineNode.cronSchedule) &&
                ['CREATED', 'APPROVAL_PENDING', 'WAITING'].includes(nestedPipelineNode.status) &&
                <Paper variant="outlined" sx={{ padding: 2, display: 'flex', alignItems: 'center', marginBottom: 2 }}>
                    <CalendarIcon />
                    {nestedPipelineNode.scheduledStartTime && <Box ml={1}>
                        <Typography variant="body2">
                            Scheduled to run <Timestamp format="absolute" component="span" timestamp={nestedPipelineNode.scheduledStartTime} />
                        </Typography>
                        {nestedPipelineNode.cronSchedule && <Typography component="div" variant="caption" color="textSecondary" mt={0.5}>
                            Scheduled start time was calculated using the cron expression
                            {' '}
                            <StyledCode>{nestedPipelineNode.cronSchedule.expression}</StyledCode>
                            {' '}
                            in the <strong>{nestedPipelineNode.cronSchedule.timezone}</strong> timezone
                        </Typography>}
                    </Box>}
                    {!nestedPipelineNode.scheduledStartTime && nestedPipelineNode.cronSchedule && <Typography component="div" marginLeft={1} variant="body2">
                        This nested pipeline will be scheduled based on the cron expression
                        {' '}
                        <StyledCode>{nestedPipelineNode.cronSchedule.expression}</StyledCode>
                        {' '}
                        in the <strong>{nestedPipelineNode.cronSchedule.timezone}</strong> timezone
                    </Typography>}
                </Paper>}
            <Paper variant="outlined" sx={{ padding: 2, display: 'flex', alignItems: 'center', marginBottom: 2 }}>
                <Typography variant="body2" component="div">
                    This nested pipeline has
                    <MLink component="button" underline="hover" onClick={() => setNestedPipelinesDialogOpen(true)} variant="body2" sx={{ marginLeft: '4px', fontWeight: 600 }}>
                        {nestedPipelineNode.pipelines.totalCount} pipeline run{nestedPipelineNode.pipelines.totalCount === 1 ? '' : 's'}
                    </MLink>
                </Typography>
            </Paper>
            {nestedPipelineNode.errors.length > 0 && <Alert severity='error' variant='outlined' sx={{ mb: 2 }}>
                This nested pipeline has {nestedPipelineNode.errors.length} error{nestedPipelineNode.errors.length > 1 ? 's' : ''}
                <ul>{nestedPipelineNode.errors.map((err, index) => (<Typography component="li" variant="body2" key={index}>{err}</Typography>))}</ul>
            </Alert>}
            {NestedPipelinesDialogOpen && <PipelineNestedPipelinesDialog
                pipelineId={pipelineId}
                nestedPipelinePath={nestedPipelineNode.path as string}
                onPipelineSelected={onPipelineSelected}
                onClose={() => setNestedPipelinesDialogOpen(false)}
            />}
            <Box sx={{ display: "flex", border: 1, borderColor: 'divider', borderTopLeftRadius: 4, borderTopRightRadius: 4, mb: 2 }}>
                <Tabs value={tab} onChange={onTabChange}>
                    <Tab label="Stages" value="stages" />
                </Tabs>
            </Box>
            <TabContent>
                {tab === 'stages' && <PipelineStages
                    fragmentRef={nestedPipeline}
                    onTaskSelected={(path: string) => navigate(`../../${nestedPipeline.id}/task/${path}`)}
                    onNestedPipelineSelected={(path: string) => navigate(`../../${nestedPipeline.id}/nested_pipeline/${path}`)}
                />}
            </TabContent>
            {showCancelPipelineConfirmation && <ConfirmationDialog
                title="Cancel Pipeline"
                message={<React.Fragment>Are you sure you want to cancel this pipeline?</React.Fragment>}
                confirmButtonLabel="Cancel"
                opInProgress={commitCancelPipelineInFlight}
                onConfirm={() => onCancelPipeline(nestedPipeline.id)}
                onClose={() => setShowCancelPipelineConfirmation(false)}
            />}
            {showRunPipelineConfirmation && <ConfirmationDialog
                title="Run Pipeline"
                message={<React.Fragment>Are you sure you want to run this pipeline?</React.Fragment>}
                confirmButtonLabel="Run"
                opInProgress={commitRunPipelineInFlight}
                onConfirm={() => onRunPipeline(nestedPipeline.id)}
                onClose={() => setShowRunPipelineConfirmation(false)}
            />}
            {showRetryPipelineConfirmation && <ConfirmationDialog
                title="Retry Pipeline"
                message={<React.Fragment>Are you sure you want to retry this pipeline?</React.Fragment>}
                confirmButtonLabel="Retry"
                opInProgress={commitRetryPipelineInFlight}
                onConfirm={() => onRetryPipeline(pipelineId, nestedPipelineNode.path)}
                onClose={() => setShowRetryPipelineConfirmation(false)}
            />}
            {showScheduleNestedPipelineDialog && <PipelineScheduleNodeDialog
                scheduledStartTime={nestedPipelineNode.scheduledStartTime}
                cronSchedule={nestedPipelineNode.cronSchedule}
                nodeType="pipeline"
                commitInFlight={commitScheduleNestedPipelineInFlight || commitCancelScheduledNestedPipelineInFlight}
                error={scheduleNestedPipelineError}
                onClose={() => setShowScheduleNestedPipelineDialog(false)}
                onSetSchedule={(scheduledTime: Moment | null, cronSchedule: CronSchedule | null) => onScheduleNestedPipeline(nestedPipelineNode.path, scheduledTime, cronSchedule)}
                onClearSchedule={() => onCancelScheduledNestedPipeline(nestedPipelineNode.path)}
            />}
            {showDeferPipelineDialog && <PipelineDeferPipelineNodeDialog
                title="Defer Nested Pipeline"
                message={<React.Fragment>Are you sure you want to defer this pipeline?</React.Fragment>}
                onClose={() => setShowDeferPipelineDialog(false)}
                onDefer={onDeferPipeline}
                inProgress={commitDeferPipelineInFlight}
            />}
            {showUndeferPipelineConfirmation && <ConfirmationDialog
                title="Restore Pipeline"
                message={<React.Fragment>Are you sure you want to restore this deferred pipeline?</React.Fragment>}
                confirmButtonLabel="Restore"
                opInProgress={commitUndeferPipelineInFlight}
                onConfirm={() => onUndeferPipeline(nestedPipelineNode.path)}
                onClose={() => setShowUndeferPipelineConfirmation(false)}
            />}
        </Box>
    ) : <Box display="flex" justifyContent="center" pt={4}>
        <Typography color="textSecondary">Nested pipeline with path {nodePath} not found</Typography>
    </Box>;
}

export default PipelineNestedPipelineDetails;
