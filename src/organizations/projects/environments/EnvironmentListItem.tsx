import { Avatar, ListItemButton, ListItemText, Typography, useTheme } from '@mui/material';
import purple from '@mui/material/colors/purple';
import graphql from 'babel-plugin-relay/macro';
import moment from 'moment';
import { useFragment } from 'react-relay/hooks';
import { Link as RouterLink } from 'react-router-dom';
import { EnvironmentListItemFragment_environment$key } from './__generated__/EnvironmentListItemFragment_environment.graphql';

interface Props {
    fragmentRef: EnvironmentListItemFragment_environment$key
}

function EnvironmentListItem({ fragmentRef }: Props) {
    const theme = useTheme();

    const data = useFragment<EnvironmentListItemFragment_environment$key>(graphql`
        fragment EnvironmentListItemFragment_environment on Environment {
            metadata {
                updatedAt
            }
            id
            name
            description
        }
    `, fragmentRef);

    return (
        <ListItemButton
            dense
            component={RouterLink}
            to={data.id}
            sx={{
                paddingY: data.description ? 0 : 1.5,
                borderBottom: `1px solid ${theme.palette.divider}`,
                borderLeft: `1px solid ${theme.palette.divider}`,
                borderRight: `1px solid ${theme.palette.divider}`,
                '&:last-child': {
                    borderBottomLeftRadius: 4,
                    borderBottomRightRadius: 4
                }
            }}>
            <Avatar variant="rounded" sx={{ width: 32, height: 32, bgcolor: purple[300], marginRight: 2 }}>
                {data.name[0].toUpperCase()}
            </Avatar>
            <ListItemText primary={<Typography>{data.name}</Typography>} secondary={data.description} />
            <Typography variant="body2" color="textSecondary">
                {moment(data.metadata.updatedAt as moment.MomentInput).fromNow()}
            </Typography>
        </ListItemButton>
    );
}

export default EnvironmentListItem
