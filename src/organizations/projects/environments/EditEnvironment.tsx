import { useState } from "react";
import { Box, Button, Divider, Typography } from "@mui/material";
import LoadingButton from "@mui/lab/LoadingButton";
import graphql from "babel-plugin-relay/macro";
import { useFragment, useLazyLoadQuery, useMutation } from "react-relay/hooks";
import {
  Link as RouterLink,
  useNavigate,
  useOutletContext,
  useParams,
} from "react-router-dom";
import { MutationError } from "../../../common/error";
import ProjectBreadcrumbs from "../ProjectBreadcrumbs";
import EnvironmentForm, { FormData } from "./EnvironmentForm";
import { EditEnvironmentFragment_project$key } from "./__generated__/EditEnvironmentFragment_project.graphql";
import { EditEnvironmentMutation } from "./__generated__/EditEnvironmentMutation.graphql";
import { EditEnvironmentQuery } from "./__generated__/EditEnvironmentQuery.graphql";

function EditEnvironment() {
  const context = useOutletContext<EditEnvironmentFragment_project$key>();
  const environmentId = useParams().environmentId as string;
  const navigate = useNavigate();

  const project = useFragment<EditEnvironmentFragment_project$key>(
    graphql`
      fragment EditEnvironmentFragment_project on Project {
        metadata {
          prn
        }
      }
    `,
    context
  );

  const queryData = useLazyLoadQuery<EditEnvironmentQuery>(
    graphql`
      query EditEnvironmentQuery($id: String!) {
        node(id: $id) {
          ... on Environment {
            metadata {
              createdAt
            }
            id
            name
            description
            createdBy
          }
        }
      }
    `,
    { id: environmentId }
  );

  const [commit, isInFlight] = useMutation<EditEnvironmentMutation>(graphql`
    mutation EditEnvironmentMutation($input: UpdateEnvironmentInput!) {
      updateEnvironment(input: $input) {
        environment {
          id
          name
          description
          createdBy
        }
        problems {
          message
          field
          type
        }
      }
    }
  `);

  const environment = queryData.node as any;

  const [error, setError] = useState<MutationError>();
  const [formData, setFormData] = useState<FormData>({
    name: environment.name,
    description: environment.description,
  });

  const onUpdate = () => {
    if (formData) {
      commit({
        variables: {
          input: {
            id: environment.id,
            description: formData.description,
          },
        },
        onCompleted: (data) => {
          if (data.updateEnvironment.problems.length) {
            setError({
              severity: "warning",
              message: data.updateEnvironment.problems
                .map((problem: any) => problem.message)
                .join("; "),
            });
          } else if (!data.updateEnvironment.environment) {
            setError({
              severity: "error",
              message: "Unexpected error occurred",
            });
          } else {
            navigate(`..`);
          }
        },
        onError: (error) => {
          setError({
            severity: "error",
            message: `Unexpected error occurred: ${error.message}`,
          });
        },
      });
    }
  };

  return (
    <Box>
      <ProjectBreadcrumbs
        prn={project.metadata.prn}
        childRoutes={[
          { title: "environments", path: "environments" },
          { title: formData.name, path: environmentId },
          { title: "edit", path: "edit" },
        ]}
      />
      <Typography variant="h5">Edit Environment</Typography>
      <EnvironmentForm
        editMode
        data={formData}
        onChange={(data: FormData) => setFormData(data)}
        error={error}
      />
      <Divider sx={{ opacity: 0.6 }} />
      <Box marginTop={2}>
        <LoadingButton
          loading={isInFlight}
          variant="outlined"
          color="primary"
          sx={{ marginRight: 2 }}
          onClick={onUpdate}
        >
          Update Environment
        </LoadingButton>
        <Button component={RouterLink} color="inherit" to={-1 as any}>
          Cancel
        </Button>
      </Box>
    </Box>
  );
}

export default EditEnvironment;
