import { Box, Button, List, Paper, Typography, useTheme } from "@mui/material";
import graphql from "babel-plugin-relay/macro";
import InfiniteScroll from "react-infinite-scroll-component";
import { Link as RouterLink } from "react-router-dom";
import {
  ConnectionHandler,
  useFragment,
  useLazyLoadQuery,
  usePaginationFragment,
} from "react-relay/hooks";
import { DESCRIPTION } from "./Environments";
import ListSkeleton from "../../../skeletons/ListSkeleton";
import EnvironmentListItem from "./EnvironmentListItem";
import { EnvironmentListFragment_project$key } from "./__generated__/EnvironmentListFragment_project.graphql";
import { EnvironmentListPaginationQuery } from "./__generated__/EnvironmentListPaginationQuery.graphql";
import { EnvironmentListFragment_environments$key } from "./__generated__/EnvironmentListFragment_environments.graphql";
import { EnvironmentListQuery } from "./__generated__/EnvironmentListQuery.graphql";

const INITIAL_ITEM_COUNT = 100;

const NewButton =
    <Button
        sx={{ minWidth: 200 }}
        component={RouterLink}
        variant="outlined"
        to="new"
    >
        New Environment
    </Button>;

const query = graphql`
  query EnvironmentListQuery(
    $projectId: String!
    $first: Int
    $last: Int
    $after: String
    $before: String
  ) {
    ...EnvironmentListFragment_environments
  }
`;

interface Props {
  fragmentRef: EnvironmentListFragment_project$key;
}

export function GetConnections(projectId: string): [string] {
  const connectionId = ConnectionHandler.getConnectionID(
    projectId,
    "EnvironmentList_environments"
  );
  return [connectionId];
}

function EnvironmentList({ fragmentRef }: Props) {
  const theme = useTheme();

    const project = useFragment<EnvironmentListFragment_project$key>(
        graphql`
      fragment EnvironmentListFragment_project on Project {
        id
        description
      }
    `,
        fragmentRef
    );

    const queryData = useLazyLoadQuery<EnvironmentListQuery>(
        query,
        { projectId: project.id, first: INITIAL_ITEM_COUNT },
        { fetchPolicy: "store-and-network" }
    );

    const { data, loadNext, hasNext } = usePaginationFragment<
        EnvironmentListPaginationQuery,
        EnvironmentListFragment_environments$key
    >(
        graphql`
            fragment EnvironmentListFragment_environments on Query
            @refetchable(queryName: "EnvironmentListPaginationQuery") {
                node(id: $projectId) {
                  ... on Project {
                    environments(
                      after: $after
                      before: $before
                      first: $first
                      last: $last
                    ) @connection(key: "EnvironmentList_environments") {
                      totalCount
                      edges {
                        node {
                          id
                          ...EnvironmentListItemFragment_environment
                        }
                      }
                    }
                  }
                }
            }
        `, queryData
    );

    return (
        <Box>
            {data.node?.environments?.edges?.length !== 0 ? (
                <Box>
                    <Box
                        sx={{
                            display: "flex",
                            flexDirection: "row",
                            justifyContent: "space-between",
                            marginBottom: 2,
                            [theme.breakpoints.down("md")]: {
                                flexDirection: "column",
                                alignItems: "flex-start",
                                "& > *": { marginBottom: 2 },
                            },
                        }}
                    >
                        <Box>
                            <Typography variant="h5" gutterBottom>
                                Environments
                            </Typography>
                            <Typography variant="body2">{DESCRIPTION}</Typography>
                        </Box>
                        <Box>
                            {NewButton}
                        </Box>
                    </Box>
                    <Paper
                        sx={{
                            borderBottomLeftRadius: 0,
                            borderBottomRightRadius: 0,
                            border: `1px solid ${theme.palette.divider}`,
                        }}
                    >
                        <Box
                            padding={2}
                            display="flex"
                            alignItems="center"
                            justifyContent="space-between"
                        >
                            <Typography variant="subtitle1">
                                {data.node?.environments?.totalCount} environment
                                {data.node?.environments?.totalCount === 1 ? "" : "s"}
                            </Typography>
                        </Box>
                    </Paper>
                    <InfiniteScroll
                        dataLength={data.node?.environments?.edges?.length ?? 0}
                        next={() => loadNext(20)}
                        hasMore={hasNext}
                        loader={<ListSkeleton rowCount={3} />}
                    >
                        <List disablePadding>
                            {data.node?.environments?.edges?.map((edge: any) => (
                                <EnvironmentListItem
                                    key={edge.node.id}
                                    fragmentRef={edge.node}
                                />
                            ))}
                        </List>
                    </InfiniteScroll>
                </Box>
            ) : (
                <Box sx={{ marginTop: 4 }} display="flex" justifyContent="center">
                    <Box
                        padding={4}
                        display="flex"
                        flexDirection="column"
                        justifyContent="center"
                        alignItems="center"
                        sx={{ maxWidth: 600 }}
                    >
                        <Typography variant="h6">Get started with environments</Typography>
                        <Typography
                            color="textSecondary"
                            align="center"
                            sx={{ marginBottom: 2 }}
                        >
                            {DESCRIPTION}
                        </Typography>
                        {NewButton}
                    </Box>
                </Box>
            )}
        </Box>
    );
}

export default EnvironmentList;
