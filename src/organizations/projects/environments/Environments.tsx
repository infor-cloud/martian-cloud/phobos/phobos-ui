import { Box } from '@mui/material';
import graphql from 'babel-plugin-relay/macro';
import { useFragment } from "react-relay/hooks";
import { useOutletContext } from 'react-router-dom';
import ProjectBreadcrumbs from "../ProjectBreadcrumbs";
import EnvironmentList from './EnvironmentList';
import { EnvironmentsFragment_project$key } from './__generated__/EnvironmentsFragment_project.graphql';

export const DESCRIPTION = 'Environments represent the location of where an application is deployed.';

function Environments() {
    const context = useOutletContext<EnvironmentsFragment_project$key>();

    const data = useFragment<EnvironmentsFragment_project$key>(graphql`
        fragment EnvironmentsFragment_project on Project
        {
            metadata {
              prn
            }
            ...EnvironmentListFragment_project
        }
    `, context);

    return (
        <Box>
            <ProjectBreadcrumbs
                prn={data.metadata.prn}
                childRoutes={[
                    { title: "environments", path: 'environments' }
                ]}
            />
            <Box>
                <EnvironmentList fragmentRef={data} />
            </Box>
        </Box>
    );
}

export default Environments
