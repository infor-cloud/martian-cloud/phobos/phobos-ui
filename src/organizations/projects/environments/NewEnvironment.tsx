import { useState } from "react";
import { Box, Button, Divider, Typography } from "@mui/material";
import LoadingButton from "@mui/lab/LoadingButton";
import graphql from "babel-plugin-relay/macro";
import { useFragment, useMutation } from "react-relay/hooks";
import {
  Link as RouterLink,
  useNavigate,
  useOutletContext,
} from "react-router-dom";
import { MutationError } from "../../../common/error";
import ProjectBreadcrumbs from "../ProjectBreadcrumbs";
import { GetConnections } from "./EnvironmentList";
import EnvironmentForm, { FormData } from "./EnvironmentForm";
import { NewEnvironmentFragment_project$key } from "./__generated__/NewEnvironmentFragment_project.graphql";
import { NewEnvironmentMutation } from "./__generated__/NewEnvironmentMutation.graphql";

function NewEnvironment() {
  const navigate = useNavigate();
  const context = useOutletContext<NewEnvironmentFragment_project$key>();

  const project = useFragment<NewEnvironmentFragment_project$key>(
    graphql`
      fragment NewEnvironmentFragment_project on Project {
        id
        metadata {
          prn
        }
      }
    `,
    context
  );

  const [commit, isInFlight] = useMutation<NewEnvironmentMutation>(graphql`
    mutation NewEnvironmentMutation(
      $input: CreateEnvironmentInput!
      $connections: [ID!]!
    ) {
      createEnvironment(input: $input) {
        # Use @prependNode to add the node to the connection
        environment
          @prependNode(
            connections: $connections
            edgeTypeName: "EnvironmentEdge"
          ) {
          id
          name
          description
          createdBy
        }
        problems {
          message
          field
          type
        }
      }
    }
  `);

  const [error, setError] = useState<MutationError>();
  const [formData, setFormData] = useState<FormData>({
    name: "",
    description: "",
  });

  const onSave = () => {
    commit({
      variables: {
        input: {
          name: formData.name,
          description: formData.description,
          projectId: project.id,
        },
        connections: GetConnections(project.id),
      },
      onCompleted: (data) => {
        if (data.createEnvironment.problems.length) {
          setError({
            severity: "warning",
            message: data.createEnvironment.problems
              .map((problem: any) => problem.message)
              .join("; "),
          });
        } else if (!data.createEnvironment.environment) {
          setError({
            severity: "error",
            message: "Unexpected error occurred",
          });
        } else {
          navigate(`../${data.createEnvironment.environment.id}`);
        }
      },
      onError: (error) => {
        setError({
          severity: "error",
          message: `Unexpected error occurred: ${error.message}`,
        });
      },
    });
  };

  return (
    <Box>
      <ProjectBreadcrumbs
        prn={project.metadata.prn}
        childRoutes={[
          { title: "environments", path: "environments" },
          { title: "new", path: "new" },
        ]}
      />
      <Typography variant="h5">New Environment</Typography>
      <EnvironmentForm
        data={formData}
        onChange={(data: FormData) => setFormData(data)}
        error={error}
      />
      <Divider sx={{ opacity: 0.6 }} />
      <Box marginTop={2}>
        <LoadingButton
          loading={isInFlight}
          variant="outlined"
          color="primary"
          sx={{ marginRight: 2 }}
          onClick={onSave}
        >
          Create Environment
        </LoadingButton>
        <Button component={RouterLink} color="inherit" to={-1 as any}>
          Cancel
        </Button>
      </Box>
    </Box>
  );
}

export default NewEnvironment;
