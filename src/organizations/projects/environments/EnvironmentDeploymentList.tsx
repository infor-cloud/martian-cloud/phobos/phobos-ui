import { usePaginationFragment } from "react-relay/hooks";
import graphql from "babel-plugin-relay/macro";
import DeploymentList from "../../../deployments/DeploymentList";
import { EnvironmentDeploymentListFragment_pipelines$key } from "./__generated__/EnvironmentDeploymentListFragment_pipelines.graphql";
import { EnvironmentDeploymentListFragmentPaginationQuery } from "./__generated__/EnvironmentDeploymentListFragmentPaginationQuery.graphql";

interface Props {
  fragmentRef: EnvironmentDeploymentListFragment_pipelines$key;
}

function EnvironmentDeploymentList({ fragmentRef }: Props) {
  const { data, loadNext, hasNext } = usePaginationFragment<
    EnvironmentDeploymentListFragmentPaginationQuery,
    EnvironmentDeploymentListFragment_pipelines$key
  >(
    graphql`
      fragment EnvironmentDeploymentListFragment_pipelines on Environment
      @refetchable(
        queryName: "EnvironmentDeploymentListFragmentPaginationQuery"
      ) {
        pipelines(first: $first, after: $after, sort: CREATED_AT_DESC, pipelineTypes: [DEPLOYMENT])
          @connection(key: "EnvironmentDeploymentListFragment_pipelines") {
          edges {
            node {
              id
            }
          }
          ...DeploymentListFragment_deployments
        }
      }
    `,
    fragmentRef
  );

  return (
    <DeploymentList
      hasNext={hasNext}
      loadNext={loadNext}
      fragmentRef={data.pipelines}
    />
  );
}

export default EnvironmentDeploymentList;
