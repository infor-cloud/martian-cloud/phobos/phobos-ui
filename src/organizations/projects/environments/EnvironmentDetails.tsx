import React, { useState } from "react";
import { default as ArrowDropDownIcon } from "@mui/icons-material/ArrowDropDown";
import { LoadingButton } from "@mui/lab";
import {
    Avatar,
    Box,
    Button,
    ButtonGroup,
    Menu,
    MenuItem,
    Stack,
    Tab,
    Tabs,
    Typography,
} from "@mui/material";
import {
    Dialog,
    DialogActions,
    DialogContent,
    DialogTitle,
} from "@mui/material";
import purple from "@mui/material/colors/purple";
import graphql from "babel-plugin-relay/macro";
import { useSnackbar } from "notistack";
import { useFragment, useLazyLoadQuery, useMutation } from "react-relay/hooks";
import {
    useNavigate,
    useOutletContext,
    useParams,
    useSearchParams,
} from "react-router-dom";
import ProjectBreadcrumbs from "../ProjectBreadcrumbs";
import PRNButton from "../../../common/PRNButton";
import { GetConnections } from "./EnvironmentList";
import EnvironmentDeploymentList from "./EnvironmentDeploymentList";
import { EnvironmentDetailsFragment_project$key } from "./__generated__/EnvironmentDetailsFragment_project.graphql";
import { EnvironmentDetailsDeleteMutation } from "./__generated__/EnvironmentDetailsDeleteMutation.graphql";
import { EnvironmentDetailsQuery } from "./__generated__/EnvironmentDetailsQuery.graphql";

interface ConfirmationDialogProps {
    environmentName: string;
    deleteInProgress: boolean;
    keepMounted: boolean;
    open: boolean;
    onClose: (confirm?: boolean) => void;
}

function DeleteConfirmationDialog(props: ConfirmationDialogProps) {
    const { environmentName, deleteInProgress, onClose, open, ...other } = props;
    return (
        <Dialog maxWidth="xs" open={open} {...other}>
            <DialogTitle>Delete Environment</DialogTitle>
            <DialogContent dividers>
                Are you sure you want to delete environment{" "}
                <strong>{environmentName}</strong>?
            </DialogContent>
            <DialogActions>
                <Button color="inherit" onClick={() => onClose()}>
                    Cancel
                </Button>
                <LoadingButton
                    color="error"
                    loading={deleteInProgress}
                    onClick={() => onClose(true)}
                >
                    Delete
                </LoadingButton>
            </DialogActions>
        </Dialog>
    );
}

function EnvironmentDetails() {
    const environmentId = useParams().environmentId as string;
    const { enqueueSnackbar } = useSnackbar();
    const context =
        useOutletContext<EnvironmentDetailsFragment_project$key>();
    const navigate = useNavigate();
    const [menuAnchorEl, setMenuAnchorEl] = useState<Element | null>(null);
    const [showDeleteConfirmationDialog, setShowDeleteConfirmationDialog] =
        useState<boolean>(false);
    const [searchParams, setSearchParams] = useSearchParams();
    const onTabChange = (event: React.SyntheticEvent, newValue: string) => {
        setSearchParams({ tab: newValue }, { replace: true });
    };
    const tab = searchParams.get("tab") || "deployments";

    const project = useFragment<EnvironmentDetailsFragment_project$key>(
        graphql`
        fragment EnvironmentDetailsFragment_project on Project {
            id
            metadata {
                prn
            }
        }
    `,
        context
    );

    const data = useLazyLoadQuery<EnvironmentDetailsQuery>(
        graphql`
        query EnvironmentDetailsQuery(
            $id: String!
            $first: Int!
            $after: String
        ) {
            node(id: $id) {
                ... on Environment {
                    metadata {
                      createdAt
                      prn
                    }
                    id
                    name
                    description
                    createdBy
                    ...EnvironmentDeploymentListFragment_pipelines
                }
            }
        }
    `,
        { id: environmentId, first: 20 }
    );

    const [commit, commitInFlight] = useMutation<EnvironmentDetailsDeleteMutation>(graphql`
            mutation EnvironmentDetailsDeleteMutation(
                $input: DeleteEnvironmentInput!
                $connections: [ID!]!
        ) {
            deleteEnvironment(input: $input) {
                environment {
                    id @deleteEdge(connections: $connections)
                }
                problems {
                    message
                    field
                    type
                }
            }
        }
    `);

    const onDeleteConfirmationDialogClosed = (confirm?: boolean) => {
        if (data.node && data.node.id && confirm) {
            commit({
                variables: {
                    input: {
                        id: data.node.id,
                    },
                    connections: GetConnections(project.id),
                },
                onCompleted: (data) => {
                    setShowDeleteConfirmationDialog(false);

                    if (data.deleteEnvironment.problems.length) {
                        enqueueSnackbar(
                            data.deleteEnvironment.problems
                                .map((problem: any) => problem.message)
                                .join("; "),
                            { variant: "warning" }
                        );
                    } else {
                        navigate(`..`);
                    }
                },
                onError: (error) => {
                    setShowDeleteConfirmationDialog(false);
                    enqueueSnackbar(`Unexpected error occurred: ${error.message}`, {
                        variant: "error",
                    });
                },
            });
        } else {
            setShowDeleteConfirmationDialog(false);
        }
    };

    const onOpenMenu = (event: React.MouseEvent<HTMLButtonElement>) => {
        setMenuAnchorEl(event.currentTarget);
    };

    const onMenuClose = () => {
        setMenuAnchorEl(null);
    };

    const onMenuAction = (actionCallback: () => void) => {
        setMenuAnchorEl(null);
        actionCallback();
    };

    const environment = data.node as any;

    if (data.node) {
        return (
            <Box>
                <ProjectBreadcrumbs
                    prn={project.metadata.prn}
                    childRoutes={[
                        { title: "environments", path: "environments" },
                        { title: environment.name, path: environment.id },
                    ]}
                />
                <Box display="flex" justifyContent="space-between">
                    <Box display="flex" alignItems="center">
                        <Avatar
                            variant="rounded"
                            sx={{
                                width: 32,
                                height: 32,
                                marginRight: 1,
                                bgcolor: purple[300],
                            }}
                        >
                            {environment.name[0].toUpperCase()}
                        </Avatar>
                        <Box marginLeft={1}>
                            <Box display="flex" alignItems="center">
                                <Typography variant="h5" sx={{ marginRight: 1 }}>
                                    {environment.name}
                                </Typography>
                            </Box>
                            <Typography color="textSecondary">
                                {environment.description}
                            </Typography>
                        </Box>
                    </Box>
                    <Box>
                        <Stack direction="row" spacing={1}>
                            <PRNButton prn={environment.metadata.prn} />
                            <ButtonGroup variant="outlined" color="primary">
                                <Button onClick={() => navigate("edit")}>Edit</Button>
                                <Button
                                    color="primary"
                                    size="small"
                                    aria-label="more options menu"
                                    aria-haspopup="menu"
                                    onClick={onOpenMenu}
                                >
                                    <ArrowDropDownIcon fontSize="small" />
                                </Button>
                            </ButtonGroup>
                        </Stack>
                        <Menu
                            id="environment-more-options-menu"
                            anchorEl={menuAnchorEl}
                            open={Boolean(menuAnchorEl)}
                            onClose={onMenuClose}
                            anchorOrigin={{
                                vertical: "bottom",
                                horizontal: "right",
                            }}
                            transformOrigin={{
                                vertical: "top",
                                horizontal: "right",
                            }}
                        >
                            <MenuItem
                                onClick={() =>
                                    onMenuAction(() => setShowDeleteConfirmationDialog(true))
                                }
                            >
                                Delete Environment
                            </MenuItem>
                        </Menu>
                    </Box>
                </Box>
                <Box sx={{ mt: 4, mb: 6 }}>
                    <Box sx={{ border: 1, borderColor: "divider", mb: 2 }}>
                        <Tabs value={tab} onChange={onTabChange}>
                            <Tab label="Deployments" value="deployments" />
                        </Tabs>
                    </Box>
                    <Box>
                        {tab === "deployments" && (
                            <Box>
                                <EnvironmentDeploymentList fragmentRef={environment} />
                            </Box>
                        )}
                    </Box>
                </Box>
                <DeleteConfirmationDialog
                    environmentName={environment.name}
                    keepMounted
                    deleteInProgress={commitInFlight}
                    open={showDeleteConfirmationDialog}
                    onClose={onDeleteConfirmationDialogClosed}
                />
            </Box>
        );
    } else {
        return (
            <Box display="flex" justifyContent="center" marginTop={4}>
                <Typography color="textSecondary">
                    Environment with ID {environmentId} not found
                </Typography>
            </Box>
        );
    }
}

export default EnvironmentDetails;
