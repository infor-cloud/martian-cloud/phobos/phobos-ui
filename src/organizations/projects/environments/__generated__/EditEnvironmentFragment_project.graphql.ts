/**
 * @generated SignedSource<<7e2724089757db9eba4907ac17e1d49b>>
 * @lightSyntaxTransform
 * @nogrep
 */

/* tslint:disable */
/* eslint-disable */
// @ts-nocheck

import { Fragment, ReaderFragment } from 'relay-runtime';
import { FragmentRefs } from "relay-runtime";
export type EditEnvironmentFragment_project$data = {
  readonly metadata: {
    readonly prn: string;
  };
  readonly " $fragmentType": "EditEnvironmentFragment_project";
};
export type EditEnvironmentFragment_project$key = {
  readonly " $data"?: EditEnvironmentFragment_project$data;
  readonly " $fragmentSpreads": FragmentRefs<"EditEnvironmentFragment_project">;
};

const node: ReaderFragment = {
  "argumentDefinitions": [],
  "kind": "Fragment",
  "metadata": null,
  "name": "EditEnvironmentFragment_project",
  "selections": [
    {
      "alias": null,
      "args": null,
      "concreteType": "ResourceMetadata",
      "kind": "LinkedField",
      "name": "metadata",
      "plural": false,
      "selections": [
        {
          "alias": null,
          "args": null,
          "kind": "ScalarField",
          "name": "prn",
          "storageKey": null
        }
      ],
      "storageKey": null
    }
  ],
  "type": "Project",
  "abstractKey": null
};

(node as any).hash = "314ede19fa27ec7710e15e6b725284f6";

export default node;
