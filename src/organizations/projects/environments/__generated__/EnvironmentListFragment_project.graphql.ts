/**
 * @generated SignedSource<<be8207b62395b002f06403649d3564de>>
 * @lightSyntaxTransform
 * @nogrep
 */

/* tslint:disable */
/* eslint-disable */
// @ts-nocheck

import { Fragment, ReaderFragment } from 'relay-runtime';
import { FragmentRefs } from "relay-runtime";
export type EnvironmentListFragment_project$data = {
  readonly description: string;
  readonly id: string;
  readonly " $fragmentType": "EnvironmentListFragment_project";
};
export type EnvironmentListFragment_project$key = {
  readonly " $data"?: EnvironmentListFragment_project$data;
  readonly " $fragmentSpreads": FragmentRefs<"EnvironmentListFragment_project">;
};

const node: ReaderFragment = {
  "argumentDefinitions": [],
  "kind": "Fragment",
  "metadata": null,
  "name": "EnvironmentListFragment_project",
  "selections": [
    {
      "alias": null,
      "args": null,
      "kind": "ScalarField",
      "name": "id",
      "storageKey": null
    },
    {
      "alias": null,
      "args": null,
      "kind": "ScalarField",
      "name": "description",
      "storageKey": null
    }
  ],
  "type": "Project",
  "abstractKey": null
};

(node as any).hash = "b607ead606c5c4a62b0f4c34ce2c38d2";

export default node;
