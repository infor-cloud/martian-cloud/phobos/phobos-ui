/**
 * @generated SignedSource<<3c31dd5024ae7176692e2b05a908bb82>>
 * @lightSyntaxTransform
 * @nogrep
 */

/* tslint:disable */
/* eslint-disable */
// @ts-nocheck

import { Fragment, ReaderFragment } from 'relay-runtime';
import { FragmentRefs } from "relay-runtime";
export type EnvironmentsFragment_project$data = {
  readonly metadata: {
    readonly prn: string;
  };
  readonly " $fragmentSpreads": FragmentRefs<"EnvironmentListFragment_project">;
  readonly " $fragmentType": "EnvironmentsFragment_project";
};
export type EnvironmentsFragment_project$key = {
  readonly " $data"?: EnvironmentsFragment_project$data;
  readonly " $fragmentSpreads": FragmentRefs<"EnvironmentsFragment_project">;
};

const node: ReaderFragment = {
  "argumentDefinitions": [],
  "kind": "Fragment",
  "metadata": null,
  "name": "EnvironmentsFragment_project",
  "selections": [
    {
      "alias": null,
      "args": null,
      "concreteType": "ResourceMetadata",
      "kind": "LinkedField",
      "name": "metadata",
      "plural": false,
      "selections": [
        {
          "alias": null,
          "args": null,
          "kind": "ScalarField",
          "name": "prn",
          "storageKey": null
        }
      ],
      "storageKey": null
    },
    {
      "args": null,
      "kind": "FragmentSpread",
      "name": "EnvironmentListFragment_project"
    }
  ],
  "type": "Project",
  "abstractKey": null
};

(node as any).hash = "5f0e98133d2bbfb7cc5a16763f37dc0b";

export default node;
