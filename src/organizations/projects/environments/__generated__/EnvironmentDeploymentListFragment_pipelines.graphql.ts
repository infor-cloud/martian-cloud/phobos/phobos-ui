/**
 * @generated SignedSource<<1f267b8350328e6bb458e429466014b2>>
 * @lightSyntaxTransform
 * @nogrep
 */

/* tslint:disable */
/* eslint-disable */
// @ts-nocheck

import { ReaderFragment, RefetchableFragment } from 'relay-runtime';
import { FragmentRefs } from "relay-runtime";
export type EnvironmentDeploymentListFragment_pipelines$data = {
  readonly id: string;
  readonly pipelines: {
    readonly edges: ReadonlyArray<{
      readonly node: {
        readonly id: string;
      } | null | undefined;
    } | null | undefined> | null | undefined;
    readonly " $fragmentSpreads": FragmentRefs<"DeploymentListFragment_deployments">;
  };
  readonly " $fragmentType": "EnvironmentDeploymentListFragment_pipelines";
};
export type EnvironmentDeploymentListFragment_pipelines$key = {
  readonly " $data"?: EnvironmentDeploymentListFragment_pipelines$data;
  readonly " $fragmentSpreads": FragmentRefs<"EnvironmentDeploymentListFragment_pipelines">;
};

const node: ReaderFragment = (function(){
var v0 = [
  "pipelines"
],
v1 = {
  "alias": null,
  "args": null,
  "kind": "ScalarField",
  "name": "id",
  "storageKey": null
};
return {
  "argumentDefinitions": [
    {
      "kind": "RootArgument",
      "name": "after"
    },
    {
      "kind": "RootArgument",
      "name": "first"
    }
  ],
  "kind": "Fragment",
  "metadata": {
    "connection": [
      {
        "count": "first",
        "cursor": "after",
        "direction": "forward",
        "path": (v0/*: any*/)
      }
    ],
    "refetch": {
      "connection": {
        "forward": {
          "count": "first",
          "cursor": "after"
        },
        "backward": null,
        "path": (v0/*: any*/)
      },
      "fragmentPathInResult": [
        "node"
      ],
      "operation": require('./EnvironmentDeploymentListFragmentPaginationQuery.graphql'),
      "identifierInfo": {
        "identifierField": "id",
        "identifierQueryVariableName": "id"
      }
    }
  },
  "name": "EnvironmentDeploymentListFragment_pipelines",
  "selections": [
    {
      "alias": "pipelines",
      "args": [
        {
          "kind": "Literal",
          "name": "pipelineTypes",
          "value": [
            "DEPLOYMENT"
          ]
        },
        {
          "kind": "Literal",
          "name": "sort",
          "value": "CREATED_AT_DESC"
        }
      ],
      "concreteType": "PipelineConnection",
      "kind": "LinkedField",
      "name": "__EnvironmentDeploymentListFragment_pipelines_connection",
      "plural": false,
      "selections": [
        {
          "alias": null,
          "args": null,
          "concreteType": "PipelineEdge",
          "kind": "LinkedField",
          "name": "edges",
          "plural": true,
          "selections": [
            {
              "alias": null,
              "args": null,
              "concreteType": "Pipeline",
              "kind": "LinkedField",
              "name": "node",
              "plural": false,
              "selections": [
                (v1/*: any*/),
                {
                  "alias": null,
                  "args": null,
                  "kind": "ScalarField",
                  "name": "__typename",
                  "storageKey": null
                }
              ],
              "storageKey": null
            },
            {
              "alias": null,
              "args": null,
              "kind": "ScalarField",
              "name": "cursor",
              "storageKey": null
            }
          ],
          "storageKey": null
        },
        {
          "args": null,
          "kind": "FragmentSpread",
          "name": "DeploymentListFragment_deployments"
        },
        {
          "alias": null,
          "args": null,
          "concreteType": "PageInfo",
          "kind": "LinkedField",
          "name": "pageInfo",
          "plural": false,
          "selections": [
            {
              "alias": null,
              "args": null,
              "kind": "ScalarField",
              "name": "endCursor",
              "storageKey": null
            },
            {
              "alias": null,
              "args": null,
              "kind": "ScalarField",
              "name": "hasNextPage",
              "storageKey": null
            }
          ],
          "storageKey": null
        }
      ],
      "storageKey": "__EnvironmentDeploymentListFragment_pipelines_connection(pipelineTypes:[\"DEPLOYMENT\"],sort:\"CREATED_AT_DESC\")"
    },
    (v1/*: any*/)
  ],
  "type": "Environment",
  "abstractKey": null
};
})();

(node as any).hash = "4ef9d6c5e68c2c8261498c5b569506c5";

export default node;
