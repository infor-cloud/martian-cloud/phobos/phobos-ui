import { Avatar, Link, ListItemButton, ListItemText, Typography } from "@mui/material";
import purple from '@mui/material/colors/purple';
import graphql from 'babel-plugin-relay/macro';
import moment from 'moment';
import { useNavigate } from 'react-router-dom';
import { useFragment } from "react-relay";

interface Props {
    projectKey: any
    last?: boolean
}

function ProjectListItem({ projectKey, last }: Props) {
    const navigate = useNavigate();

    const data = useFragment(graphql`
        fragment ProjectListItemFragment_project on Project {
            name
            description
            metadata {
                updatedAt
            }
        }
    `, projectKey);

    return (
        <ListItemButton
            dense
            onClick={() => navigate(`../projects/${data.name}`)}
            divider={!last}
            sx={{ paddingY: data.description ? 0 : 1.5 }}
        >
            <Avatar sx={{ width: 32, height: 32, mr: 2, bgcolor: purple[300] }} variant="rounded">{data.name[0].toUpperCase()}</Avatar>
            <ListItemText
                primary={<Link
                    underline="hover"
                    variant="body1"
                    color="textPrimary" sx={{ fontWeight: '500' }}
                >{data.name}</Link>}
                secondary={data.description} />
            <Typography variant="body2" color="textSecondary">
                {moment(data.metadata.updatedAt as moment.MomentInput).fromNow()}
            </Typography>
        </ListItemButton>
    );
}

export default ProjectListItem
