import { Box, Chip, ListItem, ListItemIcon, Tooltip, Typography } from '@mui/material';
import graphql from 'babel-plugin-relay/macro';
import React from 'react';
import { useFragment } from 'react-relay/hooks';
import Gravatar from '../../common/Gravatar';
import Timestamp from '../../common/Timestamp';
import Link from '../../routes/Link';
import { ProjectDetailsIndexPipelineListItemFragment_pipeline$key } from './__generated__/ProjectDetailsIndexPipelineListItemFragment_pipeline.graphql';
import PipelineAnnotations from './pipelines/PipelineAnnotations';
import PipelineStatusType from './pipelines/PipelineStatusType';

interface Props {
    fragmentRef: ProjectDetailsIndexPipelineListItemFragment_pipeline$key;
    last?: boolean;
}

function ProjectDetailsIndexPipelineListItem({ fragmentRef, last }: Props) {
    const pipeline = useFragment<ProjectDetailsIndexPipelineListItemFragment_pipeline$key>(
        graphql`
        fragment ProjectDetailsIndexPipelineListItemFragment_pipeline on Pipeline{
            id
            status
            type
            createdBy
            release {
                id
                semanticVersion
            }
            environmentName
            environment {
                id
            }
            timestamps {
                startedAt
                completedAt
            }
            annotations {
                key
            }
            ...PipelineAnnotationsFragment_pipeline
        }
        `, fragmentRef);

    const timestampFragment = (
        <React.Fragment>
            {pipeline.timestamps.completedAt ? 'completed' : 'started'} <Timestamp component="span" timestamp={pipeline.timestamps.completedAt || pipeline.timestamps.startedAt} />
        </React.Fragment>
    );

    return (
        <ListItem
            dense
            divider={!last}
        >
            <Tooltip title={PipelineStatusType[pipeline.status]?.label}>
                <ListItemIcon sx={{ minWidth: 48 }}>
                    {PipelineStatusType[pipeline.status]?.icon}
                </ListItemIcon>
            </Tooltip>
            <Box flex={1} display="flex" alignItems="center" justifyContent="space-between" pt={1} pb={1} overflow="hidden">
                <Box flex={1} overflow="hidden" mr={1}>
                    <Box display="flex" alignItems="center" mb={1}>
                        <Link
                            to={`-/pipelines/${pipeline.id}`}
                            component="span"
                            fontWeight={500}
                            variant="body2"
                            color="textPrimary"
                            mr={1}
                        >
                            {pipeline.id.substring(0, 8)}...
                        </Link>
                        {pipeline.release && <Chip
                            component={Link}
                            to={`-/releases/${pipeline.release.id}`}
                            variant="outlined"
                            size="small"
                            sx={{ fontWeight: 500, cursor: 'pointer', overflow: 'hidden' }}
                            label={`v${pipeline.release.semanticVersion}`}
                        />}
                    </Box>
                    <Typography variant="body2" color="textSecondary" mb={pipeline.annotations.length > 0 ? 1 : 0}>
                        {pipeline.type === 'RELEASE_LIFECYCLE' && <React.Fragment>
                            Release lifecycle pipeline {timestampFragment}
                        </React.Fragment>}
                        {pipeline.type === 'DEPLOYMENT' && <React.Fragment>
                            Deployment pipeline in {!pipeline.environment && pipeline.environmentName} {pipeline.environment && <Link color="textPrimary" to={`-/environments/${pipeline.environment?.id}`}>{pipeline.environmentName}</Link>} {timestampFragment}
                        </React.Fragment>}
                        {pipeline.type === 'RUNBOOK' && <React.Fragment>
                            Runbook pipeline {timestampFragment}
                        </React.Fragment>}
                        {pipeline.type === 'NESTED' && <React.Fragment>
                            Nested pipeline {timestampFragment}
                        </React.Fragment>}
                    </Typography>
                    <PipelineAnnotations fragmentRef={pipeline} />
                </Box>
                <Tooltip title={pipeline.createdBy}>
                    <Box>
                        <Gravatar width={20} height={20} email={pipeline.createdBy} />
                    </Box>
                </Tooltip>
            </Box>
        </ListItem>
    );
}

export default ProjectDetailsIndexPipelineListItem
