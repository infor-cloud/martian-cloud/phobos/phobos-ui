import { Stack, Breadcrumbs } from '@mui/material';
import Link from '../../routes/Link';

interface Route {
    title: string
    path: string
}

interface Props {
    childRoutes?: Route[]
    prn: string
}

function ProjectBreadcrumbs({ childRoutes, prn }: Props) {
    let modifiedChildRoutes: Route[] = [];

    const orgName = prn.split(':')[2].split('/')[0]
    const projectName = prn.split(':')[2].split('/')[1]

    const orgRoutes = [
        { title: orgName, path: '' },
        { title: "projects", path: `-/projects` },
        { title: projectName, path: `projects/${projectName}` },
    ]

    if (childRoutes) {
        const childRoutePaths = childRoutes.map(r => r.path)

        modifiedChildRoutes = childRoutes.map((route, i) => ({
            ...route,
            path: `projects/${projectName}/-/${childRoutePaths.slice(0, i + 1).join('/')}`
        }));
    }

    const routes = [...orgRoutes, ...modifiedChildRoutes];

    return (
        <Stack direction="row" spacing={2} marginBottom={2}>
            <Breadcrumbs aria-label="project breadcrumb">
                {routes.map(({ title, path }, i) => (
                    <Link
                        key={i}
                        color="inherit"
                        to={`/organizations/${orgName}/${path}`}>
                        {title}
                    </Link>
                ))}
            </Breadcrumbs>
        </Stack>
    );
}

export default ProjectBreadcrumbs
