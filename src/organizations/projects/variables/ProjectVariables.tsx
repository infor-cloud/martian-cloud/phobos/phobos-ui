import LoadingButton from '@mui/lab/LoadingButton';
import { CircularProgress, Dialog, DialogActions, DialogContent, DialogTitle, Tab, Tabs, useTheme } from '@mui/material';
import Box from '@mui/material/Box';
import Button from '@mui/material/Button';
import Typography from '@mui/material/Typography';
import Link from '../../../routes/Link';
import graphql from "babel-plugin-relay/macro";
import { useSnackbar } from 'notistack';
import React, { Suspense, useState } from 'react';
import { useFragment, useMutation } from 'react-relay/hooks';
import { useOutletContext, useSearchParams } from 'react-router-dom';
import { RecordSourceProxy } from 'relay-runtime';
import TabContent from '../../../common/TabContent';
import ProjectBreadcrumbs from '../ProjectBreadcrumbs';
import ProjectVariableDialog from './ProjectVariableDialog';
import ProjectVariableHistoryDialog from './ProjectVariableHistoryDialog';
import EditProjectVariables from './EditProjectVariables';
import ProjectVariableSet, { INITIAL_ITEM_COUNT } from './ProjectVariableSet';
import ProjectVariableSetAutocomplete, { ProjectVariableSetOption } from './ProjectVariableSetAutocomplete';
import { PipelineType } from './__generated__/ProjectVariableListFragment_variables.graphql';
import { ProjectVariablesFragment_project$key } from './__generated__/ProjectVariablesFragment_project.graphql';
import { PatchProjectVariablesInput, ProjectVariablesPatchVariablesMutation, ProjectVariablesPatchVariablesMutation$data } from './__generated__/ProjectVariablesPatchVariablesMutation.graphql';

interface ConfirmationDialogProps {
    variable: any
    deleteInProgress: boolean;
    onClose: (confirm?: boolean) => void
}

type TabType = PipelineType | 'EDITOR';

function DeleteConfirmationDialog(props: ConfirmationDialogProps) {
    const { variable, deleteInProgress, onClose, ...other } = props;
    return (
        <Dialog
            maxWidth="xs"
            open={!!variable}
            {...other}
        >
            <DialogTitle>Delete Variable</DialogTitle>
            <DialogContent dividers>
                Are you sure you want to delete the variable <strong>{variable?.key}</strong>?
            </DialogContent>
            <DialogActions>
                <Button color="inherit" onClick={() => onClose()}>
                    Cancel
                </Button>
                <LoadingButton color="error" loading={deleteInProgress} onClick={() => onClose(true)}>Delete</LoadingButton>
            </DialogActions>
        </Dialog>
    );
}

function ProjectVariables() {
    const { enqueueSnackbar } = useSnackbar();
    const theme = useTheme();
    const [searchParams, setSearchParams] = useSearchParams();
    const context = useOutletContext<ProjectVariablesFragment_project$key>()

    const data = useFragment<ProjectVariablesFragment_project$key>(
        graphql`
        fragment ProjectVariablesFragment_project on Project
        {
            metadata {
                prn
            }
            id
            latestVariableSet {
                revision
            }
            environmentNames
            ...ProjectVariableSetFragment_project
            ...EditProjectVariablesFragment_project
        }
    `, context);

    const [commitPatchVariables, commitInFlight] = useMutation<ProjectVariablesPatchVariablesMutation>(graphql`
        mutation ProjectVariablesPatchVariablesMutation($input: PatchProjectVariablesInput!, $first: Int!, $after: String, $pipelineType: PipelineType, $search: String, $environmentScopes: [String!]) {
                patchProjectVariables(input: $input) {
                    projectVariableSet {
                        metadata {
                            prn
                        }
                        id
                        revision
                        project {
                            latestVariableSet {
                                revision
                            }
                        }
                        ...ProjectVariableSetFragment_variableSet
                    }
                    problems {
                        message
                        field
                        type
                    }
                }
            }
    `);

    const [variableToEdit, setVariableToEdit] = useState<any>(null);
    const [variableToDelete, setVariableToDelete] = useState<any>(null);
    const [variableToShowHistory, setVariableToShowHistory] = useState<any>(null);
    const [tabType, setTabType] = useState<TabType>('DEPLOYMENT');
    const [search, setSearch] = useState<string>('');
    const [environmentFilter, setEnvironmentFilter] = useState<string[]>([]);

    const onOpenEditVariableDialog = (variable: any) => {
        setVariableToEdit(variable);
    };

    const onOpenNewVariableDialog = (pipelineType: TabType) => {
        setVariableToEdit({
            key: '',
            value: '',
            environmentScope: '*',
            pipelineType: pipelineType,
        });
    };

    const onVariableSetSelected = (value: ProjectVariableSetOption | null) => {
        if (value) {
            searchParams.set('revision', value.revision);
            setSearchParams(searchParams, { replace: false });
        }
    };

    const patchVariables = (input: PatchProjectVariablesInput, onCompleted: () => void) => {
        commitPatchVariables({
            variables: {
                input,
                first: INITIAL_ITEM_COUNT,
                pipelineType: tabType === 'EDITOR' ? undefined : tabType,
                search: search === '' ? undefined : search,
                environmentScopes: environmentFilter.length > 0 ? environmentFilter : undefined
            },
            onCompleted: data => {
                if (data.patchProjectVariables.problems.length) {
                    enqueueSnackbar(data.patchProjectVariables.problems.map(problem => problem.message).join('; '), { variant: 'warning' });
                } else {
                    if (data.patchProjectVariables?.projectVariableSet) {
                        searchParams.set('revision', data.patchProjectVariables.projectVariableSet.revision);
                        setSearchParams(searchParams, { replace: true });
                    }
                }
                onCompleted();
            },
            onError: error => {
                onCompleted();
                enqueueSnackbar(`Unexpected error occurred: ${error.message}`, { variant: 'error' });
            },
            updater: (store: RecordSourceProxy, payload: ProjectVariablesPatchVariablesMutation$data) => {
                // Use updater to add returned variable set to store to avoid refetching
                if (payload?.patchProjectVariables?.projectVariableSet?.id) {
                    const record = store.get(payload?.patchProjectVariables?.projectVariableSet.id);
                    if (record) {
                        store.getRoot().setLinkedRecord(record, 'node', { id: payload?.patchProjectVariables?.projectVariableSet?.metadata?.prn });
                    }
                }
            },
        });
    };

    const onCreateVariable = (variable: any, keepOpen: boolean) => {
        patchVariables({
            projectId: data.id,
            createVariables: [variable]
        }, () => keepOpen ? onOpenNewVariableDialog(tabType) : setVariableToEdit(null));
    };

    const onUpdateVariable = (variable: any, keepOpen: boolean) => {
        patchVariables({
            projectId: data.id,
            replaceVariables: [variable]
        }, () => keepOpen ? onOpenNewVariableDialog(tabType) : setVariableToEdit(null));
    };

    const onCloseDeleteVariableConfirmation = (confirm?: boolean) => {
        if (confirm) {
            patchVariables({
                projectId: data.id,
                removeVariableIds: [variableToDelete.id]
            }, () => setVariableToDelete(null));
        } else {
            setVariableToDelete(null);
        }
    };

    const onTabChange = (event: React.SyntheticEvent, newValue: TabType) => {
        setTabType(newValue);
        setEnvironmentFilter([]);
    };

    const selectedRevision = searchParams.get('revision') || data.latestVariableSet?.revision;

    return (
        <Box>
            <ProjectBreadcrumbs
                prn={data.metadata.prn}
                childRoutes={[{ title: "variables", path: 'variables' }]}
            />
            {data.latestVariableSet && selectedRevision && <React.Fragment>
                <Box sx={{
                    mb: 3,
                    display: 'flex',
                    justifyContent: 'space-between',
                    alignItems: 'center',
                    flexDirection: 'row',
                    [theme.breakpoints.down('md')]: {
                        flexDirection: 'column',
                        alignItems: 'flex-start',
                        '& > *:not(:last-child)': { mb: 2 },
                    }
                }}>
                    <Typography variant="h5">Project Variables</Typography>
                    <Box width={300}>
                        <ProjectVariableSetAutocomplete
                            projectId={data.id}
                            revision={selectedRevision}
                            onSelected={onVariableSetSelected}
                            disableClearable
                        />
                    </Box>
                </Box>
                <Box sx={{ display: "flex", border: 1, borderColor: 'divider', borderTopLeftRadius: 4, borderTopRightRadius: 4, mb: 1 }}>
                    <Tabs value={tabType} onChange={onTabChange}>
                        <Tab label="Deployment" value="DEPLOYMENT" />
                        <Tab label="Runbook" value="RUNBOOK" />
                        <Tab label="Editor" value="EDITOR" />
                    </Tabs>
                </Box>
                <TabContent>
                    <Suspense fallback={<Box
                        sx={{
                            position: 'absolute',
                            top: 0,
                            left: 0,
                            width: '100%',
                            height: '100vh',
                            display: 'flex',
                            alignItems: 'center',
                            justifyContent: 'center'
                        }}>
                        <CircularProgress />
                    </Box>}>
                        {tabType === 'EDITOR' &&
                            <EditProjectVariables
                                fragmentRef={data}
                                onCancel={() => setTabType('DEPLOYMENT')}
                                revision={selectedRevision}
                            />
                        }
                        {tabType !== 'EDITOR' &&
                            <ProjectVariableSet
                                fragmentRef={data}
                                revision={selectedRevision}
                                pipelineType={tabType}
                                search={search}
                                environmentFilter={environmentFilter}
                                onSearchChange={setSearch}
                                onEnvironmentFilterChange={setEnvironmentFilter}
                                onShowVariableHistory={(variable: any) => setVariableToShowHistory(variable)}
                                onNewVariable={onOpenNewVariableDialog}
                                onEditVariable={onOpenEditVariableDialog}
                                onDeleteVariable={(variable: any) => setVariableToDelete(variable)}
                            />
                        }
                    </Suspense>
                </TabContent>
            </React.Fragment>}
            {!data.latestVariableSet && <Box sx={{ marginTop: 4 }} display="flex" justifyContent="center">
                <Box padding={4} display="flex" flexDirection="column" justifyContent="center" alignItems="center" sx={{ maxWidth: 600 }}>
                    <Typography variant="h6">Get started with project variables</Typography>
                    <Typography color="textSecondary" align="center" sx={{ marginBottom: 2 }}>
                        Project variables are key-value pairs that can be used in your deployment and runbook pipelines. Select <strong>New Variable</strong>  to create your first variable for your project or use the{' '}
                        <Link
                            to={'new'}
                            color="secondary"
                        >editor
                        </Link>{' '}to create multiple variables at once.
                    </Typography>
                    <Button
                        sx={{ minWidth: 200 }}
                        onClick={() => onOpenNewVariableDialog('DEPLOYMENT')}
                        variant="outlined"
                    >
                        New Variable
                    </Button>
                </Box>
            </Box>}
            {variableToEdit && <ProjectVariableDialog
                variable={variableToEdit}
                saveInProgress={commitInFlight}
                environmentNames={data.environmentNames as string[]}
                onClose={() => setVariableToEdit(null)}
                onCreateVariable={onCreateVariable}
                onUpdateVariable={onUpdateVariable}
            />}
            {variableToDelete && <DeleteConfirmationDialog
                variable={variableToDelete}
                deleteInProgress={commitInFlight}
                onClose={onCloseDeleteVariableConfirmation}
            />}
            {variableToShowHistory && <ProjectVariableHistoryDialog
                variableId={variableToShowHistory.id}
                onClose={() => setVariableToShowHistory(null)}
            />}
        </Box>
    );
}

export default ProjectVariables;
