import { Box, Chip, Typography } from '@mui/material';
import Autocomplete from '@mui/material/Autocomplete';
import CircularProgress from '@mui/material/CircularProgress';
import TextField from '@mui/material/TextField';
import match from 'autosuggest-highlight/match';
import parse from 'autosuggest-highlight/parse';
import graphql from 'babel-plugin-relay/macro';
import throttle from 'lodash.throttle';
import React, { useEffect, useMemo, useState } from 'react';
import { fetchQuery, useRelayEnvironment } from 'react-relay/hooks';
import { ProjectVariableSetAutocompleteQuery } from './__generated__/ProjectVariableSetAutocompleteQuery.graphql';

export interface ProjectVariableSetOption {
    readonly id: string
    readonly revision: string
    readonly latest: boolean
}

interface Props {
    projectId: string
    revision: string | null
    disableClearable?: boolean
    onSelected: (value: ProjectVariableSetOption | null) => void
}

function ProjectVariableSetAutocomplete({ projectId, revision, disableClearable, onSelected }: Props) {
    const [options, setOptions] = useState<ReadonlyArray<ProjectVariableSetOption>>([]);
    const [loading, setLoading] = useState<boolean>(false);
    const [inputValue, setInputValue] = useState('');

    const environment = useRelayEnvironment();

    const fetch = React.useMemo(
        () =>
            throttle(
                (
                    request: { input: string },
                    callback: (results: readonly ProjectVariableSetOption[]) => void,
                ) => {
                    fetchQuery<ProjectVariableSetAutocompleteQuery>(
                        environment,
                        graphql`
                            query ProjectVariableSetAutocompleteQuery($id: String! $revisionSearch: String) {
                                node(id: $id) {
                                    ... on Project {
                                        variableSets(first: 50, revisionSearch: $revisionSearch, sort: CREATED_AT_DESC) {
                                            edges {
                                                node {
                                                    id
                                                    revision
                                                    latest
                                                }
                                            }
                                        }
                                    }
                                }
                            }
                        `,
                        { id: projectId, revisionSearch: request.input },
                        { fetchPolicy: 'network-only' }
                    ).toPromise().then(async response => {
                        const options = response?.node?.variableSets?.edges?.map(edge => edge?.node as ProjectVariableSetOption);
                        callback(options ?? []);
                    });
                },
                300,
            ),
        [environment, projectId]
    );

    useEffect(() => {
        let active = true;

        setLoading(true);

        fetch({ input: inputValue }, (results: readonly ProjectVariableSetOption[]) => {
            if (active) {
                // Check if selected revision is present in existing options
                if (revision !== null) {
                    let resultsCopy = [...results];
                    const option = options.find(option => option.revision === revision);
                    if (option) {
                        // Check if option is present in new options
                        const optionIndex = results.findIndex(option => option.revision === revision);
                        if (optionIndex !== -1) {
                            resultsCopy[optionIndex] = option;
                        } else {
                            resultsCopy = [option, ...resultsCopy];
                        }
                    }
                    setOptions(resultsCopy);
                } else {
                    setOptions(results);
                }
                setLoading(false);
            }
        });

        return () => {
            active = false;
        };
    }, [fetch, inputValue]);

    const value = useMemo(
        () => options.find(option => option.revision === revision) ?? null,
        [options, revision]
    );

    return (
        <Autocomplete
            fullWidth
            value={value}
            size="small"
            disableClearable={disableClearable ? value !== null : false}
            onChange={(event: React.SyntheticEvent, value: ProjectVariableSetOption | null) => onSelected(value)}
            onInputChange={(_, newInputValue: string) => setInputValue(newInputValue)}
            isOptionEqualToValue={(option: ProjectVariableSetOption, value: ProjectVariableSetOption) => option.id === value.id}
            getOptionLabel={(option: ProjectVariableSetOption) => option.revision}
            renderOption={(props: React.HTMLAttributes<HTMLLIElement>, option: ProjectVariableSetOption, { inputValue }) => {
                const matches = match(option.revision, inputValue);
                const parts = parse(option.revision, matches);
                return (
                    <Box component="li" {...props}>
                        <Box display="flex">
                            <Typography component="div">
                                {parts.map((part: any, index: number) => (
                                    <span
                                        key={index}
                                        style={{
                                            fontWeight: part.highlight ? 700 : 400,
                                        }}
                                    >
                                        {part.text}
                                    </span>
                                ))}
                            </Typography>
                            {option.latest && <Chip component="span" size="small" color="secondary" sx={{ ml: 1 }} label="latest" />}
                        </Box>
                    </Box>
                );
            }}
            options={options ?? []}
            loading={loading}
            renderInput={(params) => (
                <TextField
                    {...params}
                    label='Revision'
                    placeholder='Variable set revision'
                    InputProps={{
                        ...params.InputProps,
                        endAdornment: (
                            <React.Fragment>
                                {loading ? <CircularProgress color="inherit" size={20} /> : null}
                                {value?.latest && <Chip component="span" size="small" color="secondary" label="latest" />}
                                {params.InputProps.endAdornment}
                            </React.Fragment>
                        ),
                    }}
                />
            )}
        />
    );
}

export default ProjectVariableSetAutocomplete
