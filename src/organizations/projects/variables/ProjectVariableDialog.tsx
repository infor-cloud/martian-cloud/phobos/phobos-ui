import LoadingButton from '@mui/lab/LoadingButton';
import {
    Alert,
    Box,
    Button,
    Checkbox,
    Dialog,
    DialogActions,
    DialogContent,
    FormControlLabel,
    MenuItem,
    Select,
    SelectChangeEvent,
    Stack,
    TextField,
    Typography,
    useMediaQuery,
    useTheme
} from '@mui/material';
import { useEffect, useState } from 'react';
import { MutationError } from '../../../common/error';
import EnvironmentAutocomplete from '../../../environmentrules/EnvironmentAutocomplete';
import { PipelineType } from './__generated__/ProjectVariablesPatchVariablesMutation.graphql';

export interface Variable {
    readonly id?: string
    key: string
    value: any
    environmentScope: string | null | undefined
    pipelineType: PipelineType
}

interface Props {
    environmentNames: string[]
    variable: Variable
    saveInProgress: boolean
    onCreateVariable: (variable: any, keepOpen: boolean) => void
    onUpdateVariable: (variable: any, keepOpen: boolean) => void
    onClose: () => void
}

function ProjectVariableDialog({ environmentNames, variable, saveInProgress, onCreateVariable, onUpdateVariable, onClose }: Props) {
    const [environmentName, setEnvironmentName] = useState<string | null | undefined>(variable.environmentScope === '*' ? 'All Environments' : variable.environmentScope);
    const [keepOpen, setKeepOpen] = useState(false);
    const theme = useTheme();
    const fullScreen = useMediaQuery(theme.breakpoints.down('md'));

    const [error, setError] = useState<MutationError | null>();
    const [editedVariable, setEditedVariable] = useState<any>(variable);

    useEffect(() => {
        setEditedVariable({ ...variable });
        setError(null);
    }, [variable]);

    const saveVariable = () => {
        setError(null);

        const environmentScope = variable.pipelineType === 'DEPLOYMENT' ? environmentName === 'All Environments' ? '*' : environmentName : undefined;

        editedVariable.id ? onUpdateVariable({
            id: editedVariable.id,
            key: editedVariable.key,
            value: editedVariable.value,
            pipelineType: editedVariable.pipelineType,
            environmentScope
        }, keepOpen) : onCreateVariable({
            key: editedVariable.key,
            value: editedVariable.value,
            pipelineType: editedVariable.pipelineType,
            environmentScope
        }, keepOpen);
    };

    const onFieldChange = (event: React.ChangeEvent<HTMLInputElement | HTMLTextAreaElement>) => {
        setEditedVariable({ ...editedVariable, [event.target.name]: event.target.value });
    };

    const onPipelineTypeChange = (event: SelectChangeEvent<any>) => {
        const pipelineType = event.target.value;
        setEditedVariable({ ...editedVariable, pipelineType });
        setEnvironmentName(pipelineType === 'DEPLOYMENT' ? 'All Environments' : null);
    };

    return (
        <Dialog
            fullWidth
            maxWidth="md"
            fullScreen={fullScreen}
            open={!!variable}
        >
            <Box
                display="flex"
                justifyContent="space-between"
                sx={{ pt: 2, pl: 3, pr: 3 }}
            >
                <Box>
                    <Typography variant="h6">
                        {variable.id ? 'Edit' : 'Add'} Variable
                    </Typography>
                </Box>
            </Box>
            <DialogContent>
                {error && <Alert sx={{ marginBottom: 2 }} severity={error.severity}>
                    {error.message}
                </Alert>}
                <Box sx={{ mb: 2 }}>
                    <Typography variant="body2" gutterBottom color="textSecondary">Pipeline Type</Typography>
                    <Stack>
                        <Select
                            disabled={!!variable.id}
                            sx={{ width: 300 }}
                            size="small"
                            value={editedVariable.pipelineType}
                            onChange={onPipelineTypeChange}
                        >
                            <MenuItem value={'DEPLOYMENT'}>
                                Deployment
                            </MenuItem>
                            <MenuItem value={'RUNBOOK'}>
                                Runbook
                            </MenuItem>
                        </Select>
                    </Stack>
                </Box>
                {editedVariable.pipelineType === 'DEPLOYMENT' && <Box mb={2}>
                    <Typography variant="body2" gutterBottom color="textSecondary">Environment Scope</Typography>
                    <EnvironmentAutocomplete
                        defaultSelectedEnvironment={environmentName ?? undefined}
                        options={['All Environments', ...environmentNames]}
                        onSelected={(environmentName: string | null) => setEnvironmentName(environmentName)}
                    />
                </Box>}
                <Box mb={2}>
                    <Typography variant="body2" gutterBottom color="textSecondary">Key</Typography>
                    <TextField
                        value={editedVariable.key}
                        name="key"
                        size="small"
                        margin="none"
                        fullWidth
                        onChange={onFieldChange}
                        autoComplete="off"
                    />
                </Box>
                <Box mb={2}>
                    <Typography variant="body2" gutterBottom color="textSecondary">Value</Typography>
                    <TextField
                        value={editedVariable.value}
                        name="value"
                        size="small"
                        margin="none"
                        rows={6}
                        multiline
                        fullWidth
                        onChange={onFieldChange}
                        autoComplete="off"
                    />
                </Box>
            </DialogContent>
            <DialogActions sx={{ pl: 3, pr: 3, justifyContent: 'space-between' }}>
                <FormControlLabel
                    control={<Checkbox
                        color="secondary"
                        checked={keepOpen}
                        onChange={event => setKeepOpen(event.target.checked)}
                    />}
                    label="Keep open to add another"
                />
                <Stack direction="row" spacing={2}>
                    <Button onClick={onClose} color="inherit">
                        Cancel
                    </Button>
                    <LoadingButton
                        loading={saveInProgress}
                        onClick={saveVariable}
                        variant="contained">
                        Save
                    </LoadingButton>
                </Stack>
            </DialogActions>
        </Dialog>
    );
}

export default ProjectVariableDialog;
