/**
 * @generated SignedSource<<631532df41373fe8cf508ddc13d7e1de>>
 * @lightSyntaxTransform
 * @nogrep
 */

/* tslint:disable */
/* eslint-disable */
// @ts-nocheck

import { ConcreteRequest, Mutation } from 'relay-runtime';
import { FragmentRefs } from "relay-runtime";
export type PipelineType = "DEPLOYMENT" | "NESTED" | "RELEASE_LIFECYCLE" | "RUNBOOK" | "%future added value";
export type ProblemType = "BAD_REQUEST" | "CONFLICT" | "FORBIDDEN" | "NOT_FOUND" | "%future added value";
export type PatchProjectVariablesInput = {
  clientMutationId?: string | null | undefined;
  createVariables?: ReadonlyArray<CreateProjectVariableInput> | null | undefined;
  projectId: string;
  removeVariableIds?: ReadonlyArray<string> | null | undefined;
  replaceVariables?: ReadonlyArray<ReplaceProjectVariableInput> | null | undefined;
};
export type CreateProjectVariableInput = {
  environmentScope?: string | null | undefined;
  key: string;
  pipelineType: PipelineType;
  value: string;
};
export type ReplaceProjectVariableInput = {
  environmentScope?: string | null | undefined;
  id: string;
  key: string;
  pipelineType: PipelineType;
  value: string;
};
export type ProjectVariablesPatchVariablesMutation$variables = {
  after?: string | null | undefined;
  environmentScopes?: ReadonlyArray<string> | null | undefined;
  first: number;
  input: PatchProjectVariablesInput;
  pipelineType?: PipelineType | null | undefined;
  search?: string | null | undefined;
};
export type ProjectVariablesPatchVariablesMutation$data = {
  readonly patchProjectVariables: {
    readonly problems: ReadonlyArray<{
      readonly field: ReadonlyArray<string> | null | undefined;
      readonly message: string;
      readonly type: ProblemType;
    }>;
    readonly projectVariableSet: {
      readonly id: string;
      readonly metadata: {
        readonly prn: string;
      };
      readonly project: {
        readonly latestVariableSet: {
          readonly revision: string;
        } | null | undefined;
      };
      readonly revision: string;
      readonly " $fragmentSpreads": FragmentRefs<"ProjectVariableSetFragment_variableSet">;
    } | null | undefined;
  };
};
export type ProjectVariablesPatchVariablesMutation = {
  response: ProjectVariablesPatchVariablesMutation$data;
  variables: ProjectVariablesPatchVariablesMutation$variables;
};

const node: ConcreteRequest = (function(){
var v0 = {
  "defaultValue": null,
  "kind": "LocalArgument",
  "name": "after"
},
v1 = {
  "defaultValue": null,
  "kind": "LocalArgument",
  "name": "environmentScopes"
},
v2 = {
  "defaultValue": null,
  "kind": "LocalArgument",
  "name": "first"
},
v3 = {
  "defaultValue": null,
  "kind": "LocalArgument",
  "name": "input"
},
v4 = {
  "defaultValue": null,
  "kind": "LocalArgument",
  "name": "pipelineType"
},
v5 = {
  "defaultValue": null,
  "kind": "LocalArgument",
  "name": "search"
},
v6 = [
  {
    "kind": "Variable",
    "name": "input",
    "variableName": "input"
  }
],
v7 = {
  "alias": null,
  "args": null,
  "concreteType": "ResourceMetadata",
  "kind": "LinkedField",
  "name": "metadata",
  "plural": false,
  "selections": [
    {
      "alias": null,
      "args": null,
      "kind": "ScalarField",
      "name": "prn",
      "storageKey": null
    }
  ],
  "storageKey": null
},
v8 = {
  "alias": null,
  "args": null,
  "kind": "ScalarField",
  "name": "id",
  "storageKey": null
},
v9 = {
  "alias": null,
  "args": null,
  "kind": "ScalarField",
  "name": "revision",
  "storageKey": null
},
v10 = {
  "alias": null,
  "args": null,
  "concreteType": "Problem",
  "kind": "LinkedField",
  "name": "problems",
  "plural": true,
  "selections": [
    {
      "alias": null,
      "args": null,
      "kind": "ScalarField",
      "name": "message",
      "storageKey": null
    },
    {
      "alias": null,
      "args": null,
      "kind": "ScalarField",
      "name": "field",
      "storageKey": null
    },
    {
      "alias": null,
      "args": null,
      "kind": "ScalarField",
      "name": "type",
      "storageKey": null
    }
  ],
  "storageKey": null
},
v11 = [
  {
    "kind": "Variable",
    "name": "after",
    "variableName": "after"
  },
  {
    "kind": "Variable",
    "name": "environmentScopes",
    "variableName": "environmentScopes"
  },
  {
    "kind": "Variable",
    "name": "first",
    "variableName": "first"
  },
  {
    "kind": "Variable",
    "name": "pipelineType",
    "variableName": "pipelineType"
  },
  {
    "kind": "Variable",
    "name": "search",
    "variableName": "search"
  }
];
return {
  "fragment": {
    "argumentDefinitions": [
      (v0/*: any*/),
      (v1/*: any*/),
      (v2/*: any*/),
      (v3/*: any*/),
      (v4/*: any*/),
      (v5/*: any*/)
    ],
    "kind": "Fragment",
    "metadata": null,
    "name": "ProjectVariablesPatchVariablesMutation",
    "selections": [
      {
        "alias": null,
        "args": (v6/*: any*/),
        "concreteType": "PatchProjectVariablesPayload",
        "kind": "LinkedField",
        "name": "patchProjectVariables",
        "plural": false,
        "selections": [
          {
            "alias": null,
            "args": null,
            "concreteType": "ProjectVariableSet",
            "kind": "LinkedField",
            "name": "projectVariableSet",
            "plural": false,
            "selections": [
              (v7/*: any*/),
              (v8/*: any*/),
              (v9/*: any*/),
              {
                "alias": null,
                "args": null,
                "concreteType": "Project",
                "kind": "LinkedField",
                "name": "project",
                "plural": false,
                "selections": [
                  {
                    "alias": null,
                    "args": null,
                    "concreteType": "ProjectVariableSet",
                    "kind": "LinkedField",
                    "name": "latestVariableSet",
                    "plural": false,
                    "selections": [
                      (v9/*: any*/)
                    ],
                    "storageKey": null
                  }
                ],
                "storageKey": null
              },
              {
                "args": null,
                "kind": "FragmentSpread",
                "name": "ProjectVariableSetFragment_variableSet"
              }
            ],
            "storageKey": null
          },
          (v10/*: any*/)
        ],
        "storageKey": null
      }
    ],
    "type": "Mutation",
    "abstractKey": null
  },
  "kind": "Request",
  "operation": {
    "argumentDefinitions": [
      (v3/*: any*/),
      (v2/*: any*/),
      (v0/*: any*/),
      (v4/*: any*/),
      (v5/*: any*/),
      (v1/*: any*/)
    ],
    "kind": "Operation",
    "name": "ProjectVariablesPatchVariablesMutation",
    "selections": [
      {
        "alias": null,
        "args": (v6/*: any*/),
        "concreteType": "PatchProjectVariablesPayload",
        "kind": "LinkedField",
        "name": "patchProjectVariables",
        "plural": false,
        "selections": [
          {
            "alias": null,
            "args": null,
            "concreteType": "ProjectVariableSet",
            "kind": "LinkedField",
            "name": "projectVariableSet",
            "plural": false,
            "selections": [
              (v7/*: any*/),
              (v8/*: any*/),
              (v9/*: any*/),
              {
                "alias": null,
                "args": null,
                "concreteType": "Project",
                "kind": "LinkedField",
                "name": "project",
                "plural": false,
                "selections": [
                  {
                    "alias": null,
                    "args": null,
                    "concreteType": "ProjectVariableSet",
                    "kind": "LinkedField",
                    "name": "latestVariableSet",
                    "plural": false,
                    "selections": [
                      (v9/*: any*/),
                      (v8/*: any*/)
                    ],
                    "storageKey": null
                  },
                  (v8/*: any*/)
                ],
                "storageKey": null
              },
              {
                "alias": null,
                "args": null,
                "kind": "ScalarField",
                "name": "latest",
                "storageKey": null
              },
              {
                "alias": null,
                "args": (v11/*: any*/),
                "concreteType": "ProjectVariableConnection",
                "kind": "LinkedField",
                "name": "variables",
                "plural": false,
                "selections": [
                  {
                    "alias": null,
                    "args": null,
                    "kind": "ScalarField",
                    "name": "totalCount",
                    "storageKey": null
                  },
                  {
                    "alias": null,
                    "args": null,
                    "concreteType": "ProjectVariableEdge",
                    "kind": "LinkedField",
                    "name": "edges",
                    "plural": true,
                    "selections": [
                      {
                        "alias": null,
                        "args": null,
                        "concreteType": "ProjectVariable",
                        "kind": "LinkedField",
                        "name": "node",
                        "plural": false,
                        "selections": [
                          (v8/*: any*/),
                          {
                            "alias": null,
                            "args": null,
                            "kind": "ScalarField",
                            "name": "key",
                            "storageKey": null
                          },
                          {
                            "alias": null,
                            "args": null,
                            "kind": "ScalarField",
                            "name": "value",
                            "storageKey": null
                          },
                          {
                            "alias": null,
                            "args": null,
                            "kind": "ScalarField",
                            "name": "pipelineType",
                            "storageKey": null
                          },
                          {
                            "alias": null,
                            "args": null,
                            "kind": "ScalarField",
                            "name": "environmentScope",
                            "storageKey": null
                          },
                          {
                            "alias": null,
                            "args": null,
                            "kind": "ScalarField",
                            "name": "createdBy",
                            "storageKey": null
                          },
                          {
                            "alias": null,
                            "args": null,
                            "concreteType": "ResourceMetadata",
                            "kind": "LinkedField",
                            "name": "metadata",
                            "plural": false,
                            "selections": [
                              {
                                "alias": null,
                                "args": null,
                                "kind": "ScalarField",
                                "name": "createdAt",
                                "storageKey": null
                              }
                            ],
                            "storageKey": null
                          },
                          {
                            "alias": null,
                            "args": null,
                            "kind": "ScalarField",
                            "name": "__typename",
                            "storageKey": null
                          }
                        ],
                        "storageKey": null
                      },
                      {
                        "alias": null,
                        "args": null,
                        "kind": "ScalarField",
                        "name": "cursor",
                        "storageKey": null
                      }
                    ],
                    "storageKey": null
                  },
                  {
                    "alias": null,
                    "args": null,
                    "concreteType": "PageInfo",
                    "kind": "LinkedField",
                    "name": "pageInfo",
                    "plural": false,
                    "selections": [
                      {
                        "alias": null,
                        "args": null,
                        "kind": "ScalarField",
                        "name": "endCursor",
                        "storageKey": null
                      },
                      {
                        "alias": null,
                        "args": null,
                        "kind": "ScalarField",
                        "name": "hasNextPage",
                        "storageKey": null
                      }
                    ],
                    "storageKey": null
                  }
                ],
                "storageKey": null
              },
              {
                "alias": null,
                "args": (v11/*: any*/),
                "filters": [
                  "pipelineType",
                  "search",
                  "environmentScopes"
                ],
                "handle": "connection",
                "key": "ProjectVariableSet_variables",
                "kind": "LinkedHandle",
                "name": "variables"
              }
            ],
            "storageKey": null
          },
          (v10/*: any*/)
        ],
        "storageKey": null
      }
    ]
  },
  "params": {
    "cacheID": "e2f27279af05a8fde5fb2ef70df338e8",
    "id": null,
    "metadata": {},
    "name": "ProjectVariablesPatchVariablesMutation",
    "operationKind": "mutation",
    "text": "mutation ProjectVariablesPatchVariablesMutation(\n  $input: PatchProjectVariablesInput!\n  $first: Int!\n  $after: String\n  $pipelineType: PipelineType\n  $search: String\n  $environmentScopes: [String!]\n) {\n  patchProjectVariables(input: $input) {\n    projectVariableSet {\n      metadata {\n        prn\n      }\n      id\n      revision\n      project {\n        latestVariableSet {\n          revision\n          id\n        }\n        id\n      }\n      ...ProjectVariableSetFragment_variableSet\n    }\n    problems {\n      message\n      field\n      type\n    }\n  }\n}\n\nfragment ProjectVariableListItemFragment_variable on ProjectVariable {\n  id\n  key\n  value\n  pipelineType\n  environmentScope\n  createdBy\n  metadata {\n    createdAt\n  }\n}\n\nfragment ProjectVariableSetFragment_variableSet on ProjectVariableSet {\n  id\n  revision\n  latest\n  variables(first: $first, after: $after, pipelineType: $pipelineType, search: $search, environmentScopes: $environmentScopes) {\n    totalCount\n    edges {\n      node {\n        id\n        ...ProjectVariableListItemFragment_variable\n        __typename\n      }\n      cursor\n    }\n    pageInfo {\n      endCursor\n      hasNextPage\n    }\n  }\n}\n"
  }
};
})();

(node as any).hash = "65f02a3779dc35730188da6560e84330";

export default node;
