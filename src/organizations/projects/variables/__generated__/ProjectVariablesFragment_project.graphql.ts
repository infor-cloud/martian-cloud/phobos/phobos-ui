/**
 * @generated SignedSource<<d5e4875b77de88cb02d132d84a7a4a50>>
 * @lightSyntaxTransform
 * @nogrep
 */

/* tslint:disable */
/* eslint-disable */
// @ts-nocheck

import { Fragment, ReaderFragment } from 'relay-runtime';
import { FragmentRefs } from "relay-runtime";
export type ProjectVariablesFragment_project$data = {
  readonly environmentNames: ReadonlyArray<string>;
  readonly id: string;
  readonly latestVariableSet: {
    readonly revision: string;
  } | null | undefined;
  readonly metadata: {
    readonly prn: string;
  };
  readonly " $fragmentSpreads": FragmentRefs<"EditProjectVariablesFragment_project" | "ProjectVariableSetFragment_project">;
  readonly " $fragmentType": "ProjectVariablesFragment_project";
};
export type ProjectVariablesFragment_project$key = {
  readonly " $data"?: ProjectVariablesFragment_project$data;
  readonly " $fragmentSpreads": FragmentRefs<"ProjectVariablesFragment_project">;
};

const node: ReaderFragment = {
  "argumentDefinitions": [],
  "kind": "Fragment",
  "metadata": null,
  "name": "ProjectVariablesFragment_project",
  "selections": [
    {
      "alias": null,
      "args": null,
      "concreteType": "ResourceMetadata",
      "kind": "LinkedField",
      "name": "metadata",
      "plural": false,
      "selections": [
        {
          "alias": null,
          "args": null,
          "kind": "ScalarField",
          "name": "prn",
          "storageKey": null
        }
      ],
      "storageKey": null
    },
    {
      "alias": null,
      "args": null,
      "kind": "ScalarField",
      "name": "id",
      "storageKey": null
    },
    {
      "alias": null,
      "args": null,
      "concreteType": "ProjectVariableSet",
      "kind": "LinkedField",
      "name": "latestVariableSet",
      "plural": false,
      "selections": [
        {
          "alias": null,
          "args": null,
          "kind": "ScalarField",
          "name": "revision",
          "storageKey": null
        }
      ],
      "storageKey": null
    },
    {
      "alias": null,
      "args": null,
      "kind": "ScalarField",
      "name": "environmentNames",
      "storageKey": null
    },
    {
      "args": null,
      "kind": "FragmentSpread",
      "name": "ProjectVariableSetFragment_project"
    },
    {
      "args": null,
      "kind": "FragmentSpread",
      "name": "EditProjectVariablesFragment_project"
    }
  ],
  "type": "Project",
  "abstractKey": null
};

(node as any).hash = "2746c8f36f2e956349cb697b17e39bfc";

export default node;
