/**
 * @generated SignedSource<<a7b0076518201db884f1e618857ab252>>
 * @lightSyntaxTransform
 * @nogrep
 */

/* tslint:disable */
/* eslint-disable */
// @ts-nocheck

import { Fragment, ReaderFragment } from 'relay-runtime';
import { FragmentRefs } from "relay-runtime";
export type ProjectVariableSetFragment_project$data = {
  readonly environmentNames: ReadonlyArray<string>;
  readonly name: string;
  readonly organizationName: string;
  readonly " $fragmentType": "ProjectVariableSetFragment_project";
};
export type ProjectVariableSetFragment_project$key = {
  readonly " $data"?: ProjectVariableSetFragment_project$data;
  readonly " $fragmentSpreads": FragmentRefs<"ProjectVariableSetFragment_project">;
};

const node: ReaderFragment = {
  "argumentDefinitions": [],
  "kind": "Fragment",
  "metadata": null,
  "name": "ProjectVariableSetFragment_project",
  "selections": [
    {
      "alias": null,
      "args": null,
      "kind": "ScalarField",
      "name": "name",
      "storageKey": null
    },
    {
      "alias": null,
      "args": null,
      "kind": "ScalarField",
      "name": "organizationName",
      "storageKey": null
    },
    {
      "alias": null,
      "args": null,
      "kind": "ScalarField",
      "name": "environmentNames",
      "storageKey": null
    }
  ],
  "type": "Project",
  "abstractKey": null
};

(node as any).hash = "2cbd116c1fbe3714286c0ef9134fcf29";

export default node;
