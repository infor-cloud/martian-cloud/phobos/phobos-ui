/**
 * @generated SignedSource<<b793bdf32ab169b5ea9961cbc2884e0d>>
 * @lightSyntaxTransform
 * @nogrep
 */

/* tslint:disable */
/* eslint-disable */
// @ts-nocheck

import { Fragment, ReaderFragment } from 'relay-runtime';
import { FragmentRefs } from "relay-runtime";
export type ProjectVariableEditorDialogFragment_project$data = {
  readonly environmentNames: ReadonlyArray<string>;
  readonly name: string;
  readonly organizationName: string;
  readonly " $fragmentType": "ProjectVariableEditorDialogFragment_project";
};
export type ProjectVariableEditorDialogFragment_project$key = {
  readonly " $data"?: ProjectVariableEditorDialogFragment_project$data;
  readonly " $fragmentSpreads": FragmentRefs<"ProjectVariableEditorDialogFragment_project">;
};

const node: ReaderFragment = {
  "argumentDefinitions": [],
  "kind": "Fragment",
  "metadata": null,
  "name": "ProjectVariableEditorDialogFragment_project",
  "selections": [
    {
      "alias": null,
      "args": null,
      "kind": "ScalarField",
      "name": "name",
      "storageKey": null
    },
    {
      "alias": null,
      "args": null,
      "kind": "ScalarField",
      "name": "organizationName",
      "storageKey": null
    },
    {
      "alias": null,
      "args": null,
      "kind": "ScalarField",
      "name": "environmentNames",
      "storageKey": null
    }
  ],
  "type": "Project",
  "abstractKey": null
};

(node as any).hash = "949f879cccf05e663b9c8150ad0130b9";

export default node;
