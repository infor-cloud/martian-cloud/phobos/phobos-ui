/**
 * @generated SignedSource<<5aefde705c1b0acdaa47a5286ec00a80>>
 * @lightSyntaxTransform
 * @nogrep
 */

/* tslint:disable */
/* eslint-disable */
// @ts-nocheck

import { Fragment, ReaderFragment } from 'relay-runtime';
import { FragmentRefs } from "relay-runtime";
export type NewProjectVariablesFragment_project$data = {
  readonly id: string;
  readonly metadata: {
    readonly prn: string;
  };
  readonly " $fragmentType": "NewProjectVariablesFragment_project";
};
export type NewProjectVariablesFragment_project$key = {
  readonly " $data"?: NewProjectVariablesFragment_project$data;
  readonly " $fragmentSpreads": FragmentRefs<"NewProjectVariablesFragment_project">;
};

const node: ReaderFragment = {
  "argumentDefinitions": [],
  "kind": "Fragment",
  "metadata": null,
  "name": "NewProjectVariablesFragment_project",
  "selections": [
    {
      "alias": null,
      "args": null,
      "kind": "ScalarField",
      "name": "id",
      "storageKey": null
    },
    {
      "alias": null,
      "args": null,
      "concreteType": "ResourceMetadata",
      "kind": "LinkedField",
      "name": "metadata",
      "plural": false,
      "selections": [
        {
          "alias": null,
          "args": null,
          "kind": "ScalarField",
          "name": "prn",
          "storageKey": null
        }
      ],
      "storageKey": null
    }
  ],
  "type": "Project",
  "abstractKey": null
};

(node as any).hash = "9d87b74376cfe72fc63e0872d8c192c3";

export default node;
