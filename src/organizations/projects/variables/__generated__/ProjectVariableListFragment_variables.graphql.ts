/**
 * @generated SignedSource<<99a0ca4346e3d986b112c0b55b86b908>>
 * @lightSyntaxTransform
 * @nogrep
 */

/* tslint:disable */
/* eslint-disable */
// @ts-nocheck

import { Fragment, ReaderFragment } from 'relay-runtime';
export type PipelineType = "DEPLOYMENT" | "NESTED" | "RELEASE_LIFECYCLE" | "RUNBOOK" | "%future added value";
import { FragmentRefs } from "relay-runtime";
export type ProjectVariableListFragment_variables$data = {
  readonly variables: ReadonlyArray<{
    readonly id: string;
    readonly key: string;
    readonly pipelineType: PipelineType;
    readonly " $fragmentSpreads": FragmentRefs<"ProjectVariableListItemFragment_variable">;
  }>;
  readonly " $fragmentType": "ProjectVariableListFragment_variables";
};
export type ProjectVariableListFragment_variables$key = {
  readonly " $data"?: ProjectVariableListFragment_variables$data;
  readonly " $fragmentSpreads": FragmentRefs<"ProjectVariableListFragment_variables">;
};

const node: ReaderFragment = {
  "argumentDefinitions": [],
  "kind": "Fragment",
  "metadata": null,
  "name": "ProjectVariableListFragment_variables",
  "selections": [
    {
      "alias": null,
      "args": null,
      "concreteType": "ProjectVariable",
      "kind": "LinkedField",
      "name": "variables",
      "plural": true,
      "selections": [
        {
          "alias": null,
          "args": null,
          "kind": "ScalarField",
          "name": "id",
          "storageKey": null
        },
        {
          "alias": null,
          "args": null,
          "kind": "ScalarField",
          "name": "pipelineType",
          "storageKey": null
        },
        {
          "alias": null,
          "args": null,
          "kind": "ScalarField",
          "name": "key",
          "storageKey": null
        },
        {
          "args": null,
          "kind": "FragmentSpread",
          "name": "ProjectVariableListItemFragment_variable"
        }
      ],
      "storageKey": null
    }
  ],
  "type": "ProjectVariableSet",
  "abstractKey": null
};

(node as any).hash = "2a797cf235061154658c40d5806925e9";

export default node;
