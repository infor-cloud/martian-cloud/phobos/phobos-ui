/**
 * @generated SignedSource<<e7ec3c6ac522c2b80c0e859d1a2738bc>>
 * @lightSyntaxTransform
 * @nogrep
 */

/* tslint:disable */
/* eslint-disable */
// @ts-nocheck

import { Fragment, ReaderFragment } from 'relay-runtime';
import { FragmentRefs } from "relay-runtime";
export type EditProjectVariablesFragment_project$data = {
  readonly id: string;
  readonly latestVariableSet: {
    readonly revision: string;
  } | null | undefined;
  readonly name: string;
  readonly organizationName: string;
  readonly " $fragmentType": "EditProjectVariablesFragment_project";
};
export type EditProjectVariablesFragment_project$key = {
  readonly " $data"?: EditProjectVariablesFragment_project$data;
  readonly " $fragmentSpreads": FragmentRefs<"EditProjectVariablesFragment_project">;
};

const node: ReaderFragment = {
  "argumentDefinitions": [],
  "kind": "Fragment",
  "metadata": null,
  "name": "EditProjectVariablesFragment_project",
  "selections": [
    {
      "alias": null,
      "args": null,
      "kind": "ScalarField",
      "name": "id",
      "storageKey": null
    },
    {
      "alias": null,
      "args": null,
      "kind": "ScalarField",
      "name": "name",
      "storageKey": null
    },
    {
      "alias": null,
      "args": null,
      "concreteType": "ProjectVariableSet",
      "kind": "LinkedField",
      "name": "latestVariableSet",
      "plural": false,
      "selections": [
        {
          "alias": null,
          "args": null,
          "kind": "ScalarField",
          "name": "revision",
          "storageKey": null
        }
      ],
      "storageKey": null
    },
    {
      "alias": null,
      "args": null,
      "kind": "ScalarField",
      "name": "organizationName",
      "storageKey": null
    }
  ],
  "type": "Project",
  "abstractKey": null
};

(node as any).hash = "3ec0e163baa2e621e5bcdbae05a42a84";

export default node;
