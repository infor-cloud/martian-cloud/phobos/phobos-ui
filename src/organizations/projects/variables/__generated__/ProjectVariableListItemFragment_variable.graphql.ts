/**
 * @generated SignedSource<<c9893f8e35d777cd43beb7a97c833cc4>>
 * @lightSyntaxTransform
 * @nogrep
 */

/* tslint:disable */
/* eslint-disable */
// @ts-nocheck

import { Fragment, ReaderFragment } from 'relay-runtime';
export type PipelineType = "DEPLOYMENT" | "NESTED" | "RELEASE_LIFECYCLE" | "RUNBOOK" | "%future added value";
import { FragmentRefs } from "relay-runtime";
export type ProjectVariableListItemFragment_variable$data = {
  readonly createdBy: string;
  readonly environmentScope: string;
  readonly id: string;
  readonly key: string;
  readonly metadata: {
    readonly createdAt: any;
  };
  readonly pipelineType: PipelineType;
  readonly value: string;
  readonly " $fragmentType": "ProjectVariableListItemFragment_variable";
};
export type ProjectVariableListItemFragment_variable$key = {
  readonly " $data"?: ProjectVariableListItemFragment_variable$data;
  readonly " $fragmentSpreads": FragmentRefs<"ProjectVariableListItemFragment_variable">;
};

const node: ReaderFragment = {
  "argumentDefinitions": [],
  "kind": "Fragment",
  "metadata": null,
  "name": "ProjectVariableListItemFragment_variable",
  "selections": [
    {
      "alias": null,
      "args": null,
      "kind": "ScalarField",
      "name": "id",
      "storageKey": null
    },
    {
      "alias": null,
      "args": null,
      "kind": "ScalarField",
      "name": "key",
      "storageKey": null
    },
    {
      "alias": null,
      "args": null,
      "kind": "ScalarField",
      "name": "value",
      "storageKey": null
    },
    {
      "alias": null,
      "args": null,
      "kind": "ScalarField",
      "name": "pipelineType",
      "storageKey": null
    },
    {
      "alias": null,
      "args": null,
      "kind": "ScalarField",
      "name": "environmentScope",
      "storageKey": null
    },
    {
      "alias": null,
      "args": null,
      "kind": "ScalarField",
      "name": "createdBy",
      "storageKey": null
    },
    {
      "alias": null,
      "args": null,
      "concreteType": "ResourceMetadata",
      "kind": "LinkedField",
      "name": "metadata",
      "plural": false,
      "selections": [
        {
          "alias": null,
          "args": null,
          "kind": "ScalarField",
          "name": "createdAt",
          "storageKey": null
        }
      ],
      "storageKey": null
    }
  ],
  "type": "ProjectVariable",
  "abstractKey": null
};

(node as any).hash = "f290845ceee641353bea14d235d4ae0d";

export default node;
