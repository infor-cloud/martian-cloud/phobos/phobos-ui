import { useState } from 'react';
import { Link as RouterLink, useNavigate, useOutletContext } from 'react-router-dom';
import { Box, Button, Typography } from '@mui/material';
import { LoadingButton } from '@mui/lab';
import { MutationError } from '../../../common/error';
import { VariableError } from './utils/VariableError';
import graphql from 'babel-plugin-relay/macro';
import { useFragment } from 'react-relay/hooks';
import { useSnackbar } from 'notistack';
import ProjectBreadcrumbs from '../ProjectBreadcrumbs';
import ProjectVariablesSetEditor from './ProjectVariablesSetEditor';
import { VariableParser } from './utils/VariableParser';
import { setProjectVariables } from './mutations/setProjectVariables';
import { NewProjectVariablesFragment_project$key } from './__generated__/NewProjectVariablesFragment_project.graphql';
import { SetProjectVariablesInput } from './__generated__/ProjectVariablesSetVariablesMutation.graphql';

const parser = new VariableParser();

function NewProjectVariables() {
    const context = useOutletContext<NewProjectVariablesFragment_project$key>();
    const navigate = useNavigate();
    const { enqueueSnackbar } = useSnackbar();
    const { commitSetVariables, isInFlight } = setProjectVariables();
    const [errors, setErrors] = useState<MutationError[] | VariableError[]>([]);
    const [showErrors, setShowErrors] = useState<boolean>(false);
    const [editorData, setEditorData] = useState<string>('[]');

    const project = useFragment<NewProjectVariablesFragment_project$key>(
        graphql`
        fragment NewProjectVariablesFragment_project on Project
        {
            id
            metadata {
                prn
            }
        }
    `, context);

    const createVariables = (variablesData: SetProjectVariablesInput['variables']): void => {
        if (variablesData) {
            commitSetVariables({
                variables: {
                    input: {
                        projectId: project.id,
                        variables: variablesData
                    }
                },
                onCompleted: data => {
                    if (data.setProjectVariables.problems.length) {
                        setErrors(
                            data.setProjectVariables.problems.map(problem => ({
                                severity: 'error',
                                message: problem.message
                            }))
                        );
                        setShowErrors(true);
                    } else {
                        if (data.setProjectVariables.projectVariableSet) {
                            navigate(`../?revision=${data.setProjectVariables.projectVariableSet.revision}`);
                            enqueueSnackbar('Variables created successfully', { variant: 'success' });
                        }
                    }
                },
                onError: error => {
                    setErrors([
                        {
                            severity: 'error',
                            message: `Unexpected error occurred: ${error.message}`
                        }
                    ]);
                    setShowErrors(true);
                }
            })
        }
        else {
            setErrors([
                {
                    severity: 'error',
                    message: 'Variables data is null or undefined',
                },
            ]);
        }
    };

    const onCreate = () => {
        if (errors.length > 0) {
            setShowErrors(true);
        } else {
            try {
                const decodedVariables = parser.decode(editorData);
                createVariables(decodedVariables);
            } catch (error: any) {
                setErrors([{
                    severity: 'error',
                    message: `Invalid data: ${error.message}`
                }]);
            }
        }
    };

    const onChange = (value: string | undefined) => {
        if (showErrors) {
            setShowErrors(false);
            setErrors([]);
        }
        setEditorData(value || '')
    };

    const onValidation = (validationErrors: VariableError[]) => {
        setErrors(validationErrors.map((error: VariableError) => ({
            severity: error.severity,
            message: `Invalid data on line ${error.startLineNumber}: ${error.message}`
        })));
    };

    return (
        <Box>
            <ProjectBreadcrumbs
                prn={project.metadata.prn}
                childRoutes={[
                    { title: "variables", path: 'variables' },
                    { title: "new", path: 'new' }
                ]}
            />
            <Typography variant="h5">Create New Variable Set</Typography>
            <Box>
                <ProjectVariablesSetEditor
                    showErrors={showErrors}
                    editorData={editorData}
                    errors={errors}
                    onChange={onChange}
                    onValidation={onValidation}
                />
                <LoadingButton
                    loading={isInFlight}
                    disabled={editorData === ''}
                    variant="outlined"
                    color="primary"
                    sx={{ mr: 2 }}
                    onClick={onCreate}
                >
                    Create
                </LoadingButton>
                <Button component={RouterLink} color="inherit" to={-1 as any}>Cancel</Button>
            </Box>
        </Box>
    );
}

export default NewProjectVariables;
