import { VariableNode } from '../hooks/useProjectVariables';
import { SetProjectVariablesInput } from '../__generated__/ProjectVariablesSetVariablesMutation.graphql';

export class VariableParser {
    private parseValue(value: unknown): unknown {
        if (typeof value === 'string') {
            const trimmed = value.trim();
            try {
                return JSON.parse(trimmed);
            } catch {
                return trimmed;
            }
        }
        return value;
    }

    public encode(variableSet: VariableNode[]): string {
        const encodedVariables = variableSet.map((node: any) => {
            const { key, value, pipelineType, environmentScope } = node;
            const validatedValue = this.parseValue(value);

            return {
                key,
                value: validatedValue,
                pipelineType,
                ...(pipelineType === 'DEPLOYMENT' && environmentScope !== '*' && { environmentScope }),
            };
        });

        return JSON.stringify(encodedVariables, null, 4);
    }

    public decode(editorData: string): SetProjectVariablesInput['variables'] {
        const parsedData = JSON.parse(editorData);
        return parsedData.map((variable: any) => ({
            ...variable,
            value: typeof variable.value === "string"
                ? variable.value
                : JSON.stringify(variable.value),
            environmentScope:
                variable.pipelineType === "DEPLOYMENT"
                    ? variable.environmentScope?.trim() || "*"
                    : variable.environmentScope,
        }));
    }
}
