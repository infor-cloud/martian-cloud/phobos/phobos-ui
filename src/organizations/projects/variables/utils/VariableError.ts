export type VariableError =  {
    message: string;
    startLineNumber?: string;
    severity: 'warning' | 'error';
}
