import graphql from 'babel-plugin-relay/macro';
import { useMutation } from 'react-relay/hooks';
import { setProjectVariablesMutation } from './__generated__/setProjectVariablesMutation.graphql';

export const setProjectVariables = () => {
    const [commitSetVariables, isInFlight] = useMutation<setProjectVariablesMutation>(graphql`
        mutation setProjectVariablesMutation($input: SetProjectVariablesInput!) {
            setProjectVariables(input: $input) {
                projectVariableSet {
                    id
                    revision
                    project {
                        id
                        ...EditProjectVariablesFragment_project
                    }
                }
                problems {
                    message
                    field
                    type
                }
            }
        }
    `);
    return { commitSetVariables, isInFlight };
};
