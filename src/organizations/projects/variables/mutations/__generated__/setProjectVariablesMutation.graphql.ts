/**
 * @generated SignedSource<<abaabe42f1905529ccd9fcf6aff100ab>>
 * @lightSyntaxTransform
 * @nogrep
 */

/* tslint:disable */
/* eslint-disable */
// @ts-nocheck

import { ConcreteRequest, Mutation } from 'relay-runtime';
import { FragmentRefs } from "relay-runtime";
export type PipelineType = "DEPLOYMENT" | "NESTED" | "RELEASE_LIFECYCLE" | "RUNBOOK" | "%future added value";
export type ProblemType = "BAD_REQUEST" | "CONFLICT" | "FORBIDDEN" | "NOT_FOUND" | "%future added value";
export type SetProjectVariablesInput = {
  clientMutationId?: string | null | undefined;
  projectId: string;
  variables: ReadonlyArray<CreateProjectVariableInput>;
};
export type CreateProjectVariableInput = {
  environmentScope?: string | null | undefined;
  key: string;
  pipelineType: PipelineType;
  value: string;
};
export type setProjectVariablesMutation$variables = {
  input: SetProjectVariablesInput;
};
export type setProjectVariablesMutation$data = {
  readonly setProjectVariables: {
    readonly problems: ReadonlyArray<{
      readonly field: ReadonlyArray<string> | null | undefined;
      readonly message: string;
      readonly type: ProblemType;
    }>;
    readonly projectVariableSet: {
      readonly id: string;
      readonly project: {
        readonly id: string;
        readonly " $fragmentSpreads": FragmentRefs<"EditProjectVariablesFragment_project">;
      };
      readonly revision: string;
    } | null | undefined;
  };
};
export type setProjectVariablesMutation = {
  response: setProjectVariablesMutation$data;
  variables: setProjectVariablesMutation$variables;
};

const node: ConcreteRequest = (function(){
var v0 = [
  {
    "defaultValue": null,
    "kind": "LocalArgument",
    "name": "input"
  }
],
v1 = [
  {
    "kind": "Variable",
    "name": "input",
    "variableName": "input"
  }
],
v2 = {
  "alias": null,
  "args": null,
  "kind": "ScalarField",
  "name": "id",
  "storageKey": null
},
v3 = {
  "alias": null,
  "args": null,
  "kind": "ScalarField",
  "name": "revision",
  "storageKey": null
},
v4 = {
  "alias": null,
  "args": null,
  "concreteType": "Problem",
  "kind": "LinkedField",
  "name": "problems",
  "plural": true,
  "selections": [
    {
      "alias": null,
      "args": null,
      "kind": "ScalarField",
      "name": "message",
      "storageKey": null
    },
    {
      "alias": null,
      "args": null,
      "kind": "ScalarField",
      "name": "field",
      "storageKey": null
    },
    {
      "alias": null,
      "args": null,
      "kind": "ScalarField",
      "name": "type",
      "storageKey": null
    }
  ],
  "storageKey": null
};
return {
  "fragment": {
    "argumentDefinitions": (v0/*: any*/),
    "kind": "Fragment",
    "metadata": null,
    "name": "setProjectVariablesMutation",
    "selections": [
      {
        "alias": null,
        "args": (v1/*: any*/),
        "concreteType": "SetProjectVariablesPayload",
        "kind": "LinkedField",
        "name": "setProjectVariables",
        "plural": false,
        "selections": [
          {
            "alias": null,
            "args": null,
            "concreteType": "ProjectVariableSet",
            "kind": "LinkedField",
            "name": "projectVariableSet",
            "plural": false,
            "selections": [
              (v2/*: any*/),
              (v3/*: any*/),
              {
                "alias": null,
                "args": null,
                "concreteType": "Project",
                "kind": "LinkedField",
                "name": "project",
                "plural": false,
                "selections": [
                  (v2/*: any*/),
                  {
                    "args": null,
                    "kind": "FragmentSpread",
                    "name": "EditProjectVariablesFragment_project"
                  }
                ],
                "storageKey": null
              }
            ],
            "storageKey": null
          },
          (v4/*: any*/)
        ],
        "storageKey": null
      }
    ],
    "type": "Mutation",
    "abstractKey": null
  },
  "kind": "Request",
  "operation": {
    "argumentDefinitions": (v0/*: any*/),
    "kind": "Operation",
    "name": "setProjectVariablesMutation",
    "selections": [
      {
        "alias": null,
        "args": (v1/*: any*/),
        "concreteType": "SetProjectVariablesPayload",
        "kind": "LinkedField",
        "name": "setProjectVariables",
        "plural": false,
        "selections": [
          {
            "alias": null,
            "args": null,
            "concreteType": "ProjectVariableSet",
            "kind": "LinkedField",
            "name": "projectVariableSet",
            "plural": false,
            "selections": [
              (v2/*: any*/),
              (v3/*: any*/),
              {
                "alias": null,
                "args": null,
                "concreteType": "Project",
                "kind": "LinkedField",
                "name": "project",
                "plural": false,
                "selections": [
                  (v2/*: any*/),
                  {
                    "alias": null,
                    "args": null,
                    "kind": "ScalarField",
                    "name": "name",
                    "storageKey": null
                  },
                  {
                    "alias": null,
                    "args": null,
                    "concreteType": "ProjectVariableSet",
                    "kind": "LinkedField",
                    "name": "latestVariableSet",
                    "plural": false,
                    "selections": [
                      (v3/*: any*/),
                      (v2/*: any*/)
                    ],
                    "storageKey": null
                  },
                  {
                    "alias": null,
                    "args": null,
                    "kind": "ScalarField",
                    "name": "organizationName",
                    "storageKey": null
                  }
                ],
                "storageKey": null
              }
            ],
            "storageKey": null
          },
          (v4/*: any*/)
        ],
        "storageKey": null
      }
    ]
  },
  "params": {
    "cacheID": "5bfec221071d501babb1a011b79578d5",
    "id": null,
    "metadata": {},
    "name": "setProjectVariablesMutation",
    "operationKind": "mutation",
    "text": "mutation setProjectVariablesMutation(\n  $input: SetProjectVariablesInput!\n) {\n  setProjectVariables(input: $input) {\n    projectVariableSet {\n      id\n      revision\n      project {\n        id\n        ...EditProjectVariablesFragment_project\n      }\n    }\n    problems {\n      message\n      field\n      type\n    }\n  }\n}\n\nfragment EditProjectVariablesFragment_project on Project {\n  id\n  name\n  latestVariableSet {\n    revision\n    id\n  }\n  organizationName\n}\n"
  }
};
})();

(node as any).hash = "c5ef1964f53d52cf9038ef688e4aa391";

export default node;
