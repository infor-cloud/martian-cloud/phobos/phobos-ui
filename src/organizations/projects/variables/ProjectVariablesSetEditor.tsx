import React from "react";
import { Alert, Box, IconButton, Link, Typography } from "@mui/material";
import { Launch } from "@mui/icons-material";
import Editor from "@monaco-editor/react";
import CopyButton from "../../../common/CopyButton";
import { MutationError } from "../../../common/error";
import config from "../../../common/config";
import { VariableError } from "./utils/VariableError";
import { LoadingSpinner } from "./utils/LoadingSpinner";
import { editor } from "monaco-editor";

interface Props {
    showErrors: boolean;
    readOnly?: boolean;
    errors: MutationError[] | VariableError[];
    editorData: string;
    onChange: (value: string | undefined) => void;
    onValidation: (validationErrors: VariableError[]) => void;
}

const EDITOR_HEIGHT = 750;
const schema = {
    type: "array",
    items: {
        type: "object",
        properties: {
            key: {
                type: "string",
                maxLength: 64,
                pattern: "^(?=.{1,64}$)[a-z0-9](?:[a-z0-9\\-_]*[a-z0-9])?$"
            },
            value: {
                anyOf: [
                    { type: "object" },
                    { type: "array" },
                    { type: "number" },
                    { type: "boolean" },
                    { type: "string" },
                    { type: "null" }
                ]
            },
            pipelineType: { type: "string", enum: ["DEPLOYMENT", "RUNBOOK"] },
            environmentScope: { type: "string", optional: true }
        },
        required: ["key", "value", "pipelineType"],
        additionalProperties: false,
        if: {
            properties: {
                pipelineType: { const: "RUNBOOK" }
            }
        },
        then: {
            not: {
                required: ["environmentScope"]
            }
        }
    }
};

function ProjectVariablesSetEditor({ showErrors, readOnly, editorData, errors, onChange, onValidation }: Props) {

    const handleEditorValidation = (markers: editor.IMarker[]) => {
        const validationErrors: VariableError[] = markers.map((marker) => ({
            message: marker.message,
            startLineNumber: marker.startLineNumber.toString(),
            severity: marker.severity === 8 ? "error" : "warning"
        }));
        onValidation(validationErrors);
    };

    return (
        <Box>
            {showErrors && errors.map((error, index) => (
                <Alert key={index} sx={{ mt: 2, mb: 2 }} severity={error.severity || 'error'}>
                    {error.message}
                </Alert>
            ))}
            <Box display="flex" justifyContent="space-between">
                <Box display="flex">
                    <Typography sx={{ mt: 1 }} variant="body2">
                        For more information on project variables, see the documentation
                    </Typography>
                    <IconButton
                        LinkComponent={Link}
                        href={config.docsUrl}
                        target="_blank"
                        rel="noopener noreferrer"
                        disableRipple
                    >
                        <Launch fontSize="small" />
                    </IconButton>
                </Box>
                <Box>
                    {editorData && (
                        <CopyButton
                            data={editorData}
                            toolTip="Copy variable set"
                            opacity="100%"
                        />
                    )}
                </Box>
            </Box>
            <Box sx={{ mt: 2, mb: 4, height: EDITOR_HEIGHT, position: "relative" }}>
                <Box sx={{ position: "absolute", top: 0, left: 0, width: "100%", height: "100%" }}>
                    <Editor
                        options={{
                            fontSize: 12,
                            scrollBeyondLastLine: false,
                            minimap: { enabled: true },
                            readOnly: readOnly,
                            scrollbar: { vertical: "auto" },
                            wordWrap: "on"
                        }}
                        loading={<LoadingSpinner />}
                        height="100%"
                        onValidate={handleEditorValidation}
                        onMount={(editor, monaco) => {
                            const model = editor.getModel();
                            model?.pushEOL(monaco.editor.EndOfLineSequence.LF);

                            monaco.languages.json.jsonDefaults.setDiagnosticsOptions({
                                validate: true,
                                schemas: [
                                    {
                                        uri: "http://example.com/schema.json",
                                        fileMatch: ["*"],
                                        schema
                                    }
                                ]
                            });
                            editor.onDidContentSizeChange(() => {
                                editor.layout();
                            });
                        }}
                        theme="vs-dark"
                        defaultLanguage="json"
                        value={editorData}
                        onChange={onChange}
                    />
                </Box>
            </Box>
        </Box>
    );
}

export default ProjectVariablesSetEditor;
