import { useEffect, useMemo, useState } from 'react';
import { Box, Button } from '@mui/material';
import { useFragment } from 'react-relay/hooks';
import LoadingButton from '@mui/lab/LoadingButton';
import graphql from 'babel-plugin-relay/macro';
import { useSearchParams } from 'react-router-dom';
import { useSnackbar } from 'notistack';
import { MutationError } from '../../../common/error';
import { LoadingSpinner } from './utils/LoadingSpinner';
import { VariableError } from './utils/VariableError';
import ProjectVariablesSetEditor from './ProjectVariablesSetEditor';
import { setProjectVariables } from './mutations/setProjectVariables';
import { VariableParser } from './utils/VariableParser';
import { useProjectVariables } from './hooks/useProjectVariables';
import { EditProjectVariablesFragment_project$key } from './__generated__/EditProjectVariablesFragment_project.graphql';
import { SetProjectVariablesInput } from './__generated__/ProjectVariablesSetVariablesMutation.graphql';

interface Props {
    fragmentRef: EditProjectVariablesFragment_project$key;
    revision: string;
    onCancel: () => void;
}

const parser = new VariableParser();

function EditProjectVariables({ fragmentRef, revision, onCancel }: Props){
    const { commitSetVariables, isInFlight } = setProjectVariables();
    const [searchParams, setSearchParams] = useSearchParams();
    const { enqueueSnackbar } = useSnackbar();
    const [errors, setErrors] = useState<MutationError[] | VariableError[]>([]);
    const [showErrors, setShowErrors] = useState<boolean>(false);
    const [editorData, setEditorData] = useState<string>('');

    const project = useFragment<EditProjectVariablesFragment_project$key>(
        graphql`
        fragment EditProjectVariablesFragment_project on Project
        {
            id
            name
            latestVariableSet {
                revision
            }
            organizationName
        }
    `, fragmentRef);

    const variableSetPrn = useMemo(() => {
        return `prn:project_variable_set:${project.organizationName}/${project.name}/${revision}`
    }, [project.organizationName, project.name, revision]);

    const { variables, isLoading, error: fetchError } = useProjectVariables(variableSetPrn);

    useEffect(() => {
        try {
            if (!variables) {
                setErrors([
                    {
                        severity: 'error',
                        message: 'Variables data is null or undefined',
                    },
                ]);
                return;
            }
            const encodedData = parser.encode(variables);
            setEditorData(encodedData);
        } catch (error: any) {
            setErrors([
                {
                    severity: 'error',
                    message: `Unexpected error occurred: ${error.message}`,
                },
            ]);
        }
    }, [variables]);

    const updateVariables = (variablesData: SetProjectVariablesInput['variables']): void => {
        commitSetVariables({
            variables: {
                input: {
                    projectId: project.id,
                    variables: variablesData
                }
            },
            onCompleted: data => {
                if (data.setProjectVariables.problems.length) {
                    setErrors(
                        data.setProjectVariables.problems.map((problem: any) => ({
                            severity: 'error',
                            message: problem.message
                        }))
                    );
                    setShowErrors(true);
                } else {
                    if (data.setProjectVariables.projectVariableSet) {
                        searchParams.set('revision', data.setProjectVariables.projectVariableSet.revision);
                        setSearchParams(searchParams, { replace: true });
                        enqueueSnackbar('Variables updated successfully', { variant: 'success' });
                    }
                }
            },
            onError: error => {
                setErrors([
                    {
                        severity: 'error',
                        message: `Unexpected error occurred: ${error.message}`
                    }
                ]);
                setShowErrors(true);
            },
        });
    };

    const isLatestRevision = useMemo(() => {
        return project.latestVariableSet?.revision === revision;
    }, [project.latestVariableSet?.revision, revision]);

    const onUpdate = () => {
        if (errors.length > 0) {
            setShowErrors(true);
        } else {
            try {
                const decodedVariables = parser.decode(editorData);
                updateVariables(decodedVariables);
            } catch (error: any) {
                setErrors([{
                    severity: 'error',
                    message: `Invalid data: ${error.message}`
                }]);
            }
        }
    };

    const onChange = (value: string | undefined) => {
        if (showErrors) {
            setShowErrors(false);
            setErrors([]);
        }
        setEditorData(value || '')
    };

    const onValidation = (validationErrors: VariableError[]) => {
        setErrors(validationErrors.map((error: VariableError) => ({
            severity: error.severity,
            message: `Invalid data on line ${error.startLineNumber}: ${error.message}`
        })));
    };

    return (
        <Box>
            {isLoading && <LoadingSpinner />}
            {!isLoading &&
                <ProjectVariablesSetEditor
                    showErrors={showErrors}
                    readOnly={!isLatestRevision}
                    editorData={editorData}
                    errors={errors || fetchError}
                    onChange={onChange}
                    onValidation={onValidation}
                />
            }
            <LoadingButton
                loading={isInFlight || isLoading}
                disabled={!isLatestRevision || editorData === ""}
                variant="outlined"
                color="primary"
                sx={{ mr: 2 }}
                onClick={onUpdate}
            >
                Update
            </LoadingButton>
            <Button color="inherit" onClick={onCancel}>
                Cancel
            </Button>
        </Box>
    );
}

export default EditProjectVariables;
