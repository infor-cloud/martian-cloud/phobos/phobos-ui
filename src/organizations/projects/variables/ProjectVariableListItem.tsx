import DeleteIcon from '@mui/icons-material/CloseOutlined';
import EditIcon from '@mui/icons-material/EditOutlined';
import HistoryIcon from '@mui/icons-material/HistoryOutlined';
import { Stack } from '@mui/material';
import Button from '@mui/material/Button';
import TableCell from '@mui/material/TableCell';
import TableRow from '@mui/material/TableRow';
import graphql from 'babel-plugin-relay/macro';
import { useFragment } from 'react-relay/hooks';
import CopyButton from '../../../common/CopyButton';
import DataTableCell from '../../../common/DataTableCell';
import Timestamp from '../../../common/Timestamp';
import { ProjectVariableListItemFragment_variable$key } from './__generated__/ProjectVariableListItemFragment_variable.graphql';

interface Props {
    fragmentRef: ProjectVariableListItemFragment_variable$key;
    showValue: boolean;
    latest: boolean;
    onShowHistory: (variable: any) => void;
    onEdit: (variable: any) => void;
    onDelete: (variable: any) => void;
}

function ProjectVariableListItem({ onShowHistory, onEdit, onDelete, latest, fragmentRef, showValue }: Props) {
    const data = useFragment<ProjectVariableListItemFragment_variable$key>(
        graphql`
        fragment ProjectVariableListItemFragment_variable on ProjectVariable
        {
            id
            key
            value
            pipelineType
            environmentScope
            createdBy
            metadata {
                createdAt
            }
        }
      `, fragmentRef);

    return (
        <TableRow
            sx={{ '&:last-child td, &:last-child th': { border: 0 }, height: 64 }}
        >
            <DataTableCell sx={{ fontWeight: 'bold' }}>
                {data.key}
                <CopyButton
                    data={data.key}
                    toolTip="Copy key"
                />
            </DataTableCell>
            <DataTableCell sx={{ wordBreak: 'break-all' }}>
                {!showValue ? '********' : data.value}
                {(data.value !== '' || !showValue) && <CopyButton
                    data={data.value}
                    toolTip="Copy value"
                />}
            </DataTableCell>
            {data.environmentScope && <DataTableCell>
                {data.environmentScope}
            </DataTableCell>}
            <DataTableCell>
                <Timestamp whiteSpace="nowrap" variant="body2" timestamp={data.metadata.createdAt} />
            </DataTableCell>
            <TableCell>
                <Stack direction="row" spacing={1} justifyContent="flex-end">
                    {latest && <Button
                        onClick={() => onEdit(data)}
                        sx={{ minWidth: 40, padding: '2px' }}
                        size="small"
                        color="info"
                        variant="outlined">
                        <EditIcon />
                    </Button>}
                    <Button
                        onClick={() => onShowHistory(data)}
                        sx={{ minWidth: 40, padding: '2px' }}
                        size="small"
                        color="info"
                        variant="outlined">
                        <HistoryIcon />
                    </Button>
                    {latest && <Button
                        onClick={() => onDelete(data)}
                        sx={{ minWidth: 40, padding: '2px' }}
                        size="small"
                        color="info"
                        variant="outlined">
                        <DeleteIcon />
                    </Button>}
                </Stack>
            </TableCell>
        </TableRow>
    );
}

export default ProjectVariableListItem;
