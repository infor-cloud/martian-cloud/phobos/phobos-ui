import {
    Box,
    Button,
    CircularProgress,
    Dialog,
    DialogActions,
    DialogContent,
    DialogTitle,
    Link,
    Stack,
    Table,
    TableBody,
    TableCell,
    TableContainer,
    TableHead,
    TableRow,
    useMediaQuery,
    useTheme
} from '@mui/material';
import graphql from "babel-plugin-relay/macro";
import { Suspense } from 'react';
import { useLazyLoadQuery, usePaginationFragment } from 'react-relay/hooks';
import DataTableCell from '../../../common/DataTableCell';
import Timestamp from '../../../common/Timestamp';
import { ProjectVariableHistoryDialogFragment_variable$key } from './__generated__/ProjectVariableHistoryDialogFragment_variable.graphql';
import { ProjectVariableHistoryDialogQuery } from './__generated__/ProjectVariableHistoryDialogQuery.graphql';

const INITIAL_ITEM_COUNT = 5;

function ProjectVariableHistory({ variableId }: { variableId: string }) {
    const variableSet = useLazyLoadQuery<ProjectVariableHistoryDialogQuery>(graphql`
    query ProjectVariableHistoryDialogQuery($id: String!, $first: Int!, $after: String) {
        node(id: $id) {
            ... on ProjectVariable {
                metadata {
                    createdAt
                }
                id
                value
                createdBy
                ...ProjectVariableHistoryDialogFragment_variable
            }
        }
    }`, { id: variableId, first: INITIAL_ITEM_COUNT });

    const { data, loadNext, hasNext } = usePaginationFragment<ProjectVariableHistoryDialogQuery, ProjectVariableHistoryDialogFragment_variable$key>(
        graphql`
        fragment ProjectVariableHistoryDialogFragment_variable on ProjectVariable
        @refetchable(queryName: "ProjectVariableHistoryDialogPaginationQuery") {
            previousVersions(
                first: $first
                after: $after
                sort: CREATED_AT_DESC
                ) @connection(key: "ProjectVariableHistoryDialog_previousVersions") {
                    totalCount
                    edges {
                        node {
                            metadata {
                                createdAt
                            }
                            id
                            value
                            createdBy
                        }
                    }
                }
            }
        `, variableSet.node);

    return (
        <Box>
            <TableContainer>
                <Table>
                    <TableHead>
                        <TableRow>
                            <TableCell>Created</TableCell>
                            <TableCell>Creator</TableCell>
                            <TableCell>Value</TableCell>
                        </TableRow>
                    </TableHead>
                    <TableBody>
                        <TableRow>
                            <TableCell>
                                <Timestamp timestamp={variableSet.node?.metadata?.createdAt} />
                            </TableCell>
                            <TableCell>
                                {variableSet.node?.createdBy}
                            </TableCell>
                            <DataTableCell sx={{ wordBreak: 'break-all' }} >
                                {variableSet.node?.value}
                            </DataTableCell>
                        </TableRow>
                        {data?.previousVersions.edges?.map((edge) => <TableRow key={edge?.node?.id}>
                            <TableCell>
                                <Timestamp timestamp={edge?.node?.metadata.createdAt} />
                            </TableCell>
                            <TableCell>
                                {edge?.node?.createdBy}
                            </TableCell>
                            <DataTableCell sx={{ wordBreak: 'break-all' }} >
                                {edge?.node?.value}
                            </DataTableCell>
                        </TableRow>)}
                    </TableBody>
                </Table>
            </TableContainer>
            {hasNext && <Link
                mt={2}
                component="div"
                variant="body2"
                color="textSecondary"
                sx={{ cursor: 'pointer' }}
                underline="hover"
                onClick={() => loadNext(INITIAL_ITEM_COUNT)}
            >
                Show more
            </Link>}
        </Box>
    );
}

interface Props {
    variableId: string
    onClose: (keepOpen: boolean) => void
}

function ProjectVariableHistoryDialog({ variableId, onClose }: Props) {
    const theme = useTheme();
    const fullScreen = useMediaQuery(theme.breakpoints.down('md'));

    return (
        <Dialog
            fullWidth
            maxWidth="md"
            fullScreen={fullScreen}
            open
        >
            <DialogTitle>Variable History</DialogTitle>
            <DialogContent dividers sx={{ flex: 1, padding: 2, minHeight: 400, display: 'flex', flexDirection: 'column' }}>
                <Suspense fallback={<Box
                    sx={{
                        position: 'absolute',
                        top: 0,
                        left: 0,
                        width: '100%',
                        minHeight: '100%',
                        display: 'flex',
                        alignItems: 'center',
                        justifyContent: 'center'
                    }}>
                    <CircularProgress />
                </Box>}>
                    <ProjectVariableHistory variableId={variableId} />
                </Suspense>
            </DialogContent>
            <DialogActions sx={{ pl: 3, pr: 3, justifyContent: 'flex-end' }}>
                <Stack direction="row" spacing={2}>
                    <Button onClick={() => onClose(false)} color="inherit">
                        Close
                    </Button>
                </Stack>
            </DialogActions>
        </Dialog>
    );
}

export default ProjectVariableHistoryDialog;
