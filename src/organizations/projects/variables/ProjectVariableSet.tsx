import { Box, Button, Checkbox, FormControl, ListItemText, MenuItem, OutlinedInput, Paper, Select, SelectChangeEvent, Table, TableBody, TableCell, TableContainer, TableHead, TableRow, Typography } from '@mui/material';
import { useTheme } from '@mui/material/styles';
import graphql from 'babel-plugin-relay/macro';
import throttle from 'lodash.throttle';
import { useMemo, useState } from 'react';
import InfiniteScroll from 'react-infinite-scroll-component';
import { fetchQuery, useFragment, useLazyLoadQuery, usePaginationFragment, useRelayEnvironment } from 'react-relay/hooks';
import SearchInput from '../../../common/SearchInput';
import ListSkeleton from '../../../skeletons/ListSkeleton';
import ProjectVariableListItem from './ProjectVariableListItem';
import { PipelineType } from './__generated__/ProjectVariableListItemFragment_variable.graphql';
import { ProjectVariableSetFragment_project$key } from './__generated__/ProjectVariableSetFragment_project.graphql';
import { ProjectVariableSetFragment_variableSet$key } from './__generated__/ProjectVariableSetFragment_variableSet.graphql';
import { ProjectVariableSetQuery } from './__generated__/ProjectVariableSetQuery.graphql';

const query = graphql`
query ProjectVariableSetQuery($id: String!, $first: Int!, $after: String, $pipelineType: PipelineType, $search: String, $environmentScopes: [String!]) {
    node(id: $id) {
        ... on ProjectVariableSet {
            ...ProjectVariableSetFragment_variableSet
        }
    }
}`;

export const INITIAL_ITEM_COUNT = 100;

interface Props {
    fragmentRef: ProjectVariableSetFragment_project$key
    revision: string
    pipelineType: PipelineType,
    search: string
    environmentFilter: string[]
    onSearchChange: (search: string) => void
    onEnvironmentFilterChange: (environmentFilter: string[]) => void
    onShowVariableHistory: (variable: any) => void
    onNewVariable: (pipelineType: PipelineType) => void
    onEditVariable: (variable: any) => void
    onDeleteVariable: (variable: any) => void
}

function ProjectVariableSet({
    fragmentRef,
    revision,
    pipelineType,
    search,
    environmentFilter,
    onSearchChange,
    onEnvironmentFilterChange,
    onShowVariableHistory,
    onEditVariable,
    onNewVariable,
    onDeleteVariable
}: Props) {
    const theme = useTheme();
    const [isRefreshing, setIsRefreshing] = useState(false);
    const [showValues, setShowValues] = useState(false);

    const project = useFragment<ProjectVariableSetFragment_project$key>(
        graphql`
        fragment ProjectVariableSetFragment_project on Project
        {
            name
            organizationName
            environmentNames
        }
    `, fragmentRef);

    const variableSetPrn = `prn:project_variable_set:${project.organizationName}/${project.name}/${revision}`;

    const queryVariables = useMemo(() => ({
        id: variableSetPrn,
        first: INITIAL_ITEM_COUNT,
        pipelineType,
        search: search === '' ? undefined : search,
        environmentScopes: environmentFilter.length > 0 ? environmentFilter : undefined
    }), [project, revision, pipelineType, search, environmentFilter]);

    const queryResult = useLazyLoadQuery<ProjectVariableSetQuery>(
        query,
        queryVariables,
        { fetchPolicy: 'store-or-network' }
    );

    const { data: variableSet, loadNext, hasNext, refetch } = usePaginationFragment<ProjectVariableSetQuery, ProjectVariableSetFragment_variableSet$key>(
        graphql`
            fragment ProjectVariableSetFragment_variableSet on ProjectVariableSet
            @refetchable(queryName: "ProjectVariableSetVariablesPaginationQuery")
             {
                id
                revision
                latest
                variables(
                    first: $first
                    after: $after
                    pipelineType: $pipelineType
                    search: $search
                    environmentScopes: $environmentScopes
                ) @connection(key: "ProjectVariableSet_variables") {
                    totalCount
                    edges {
                        node {
                            id
                            ...ProjectVariableListItemFragment_variable
                        }
                    }
                }
            }
        `, queryResult.node);

    const environment = useRelayEnvironment();

    const fetch = useMemo(
        () =>
            throttle(
                (input: string) => {
                    setIsRefreshing(true);

                    fetchQuery(environment, query, { ...queryVariables, search: input })
                        .subscribe({
                            complete: () => {
                                setIsRefreshing(false);
                                onSearchChange(input);

                                // *After* the query has been fetched, we call
                                // refetch again to re-render with the updated data.
                                // At this point the data for the query should
                                // be cached, so we use the 'store-only'
                                // fetchPolicy to avoid suspending.
                                refetch({
                                    ...queryVariables,
                                    search: input
                                }, {
                                    fetchPolicy: 'store-only'
                                });
                            },
                            error: () => {
                                setIsRefreshing(false);
                            }
                        });
                },
                2000,
                { leading: false, trailing: true }
            ),
        [environment, refetch],
    );

    const onKeyDown = (event: React.KeyboardEvent<HTMLInputElement>) => {
        // Only handle enter key type
        if (event.key === 'Enter') {
            fetch.flush();
        }
    };

    const handleEnvironmentFilterChange = ({ target: { value } }: SelectChangeEvent<string[]>) => {
        let parsedValue = typeof value === 'string' ? value.split(',') : value;
        if (parsedValue.includes('')) {
            // Unselect all
            parsedValue = [];
        }

        const environmentScopes = parsedValue.length > 0 ? parsedValue : undefined;

        setIsRefreshing(true);

        fetchQuery(environment, query, { ...queryVariables, environmentScopes })
            .subscribe({
                complete: () => {
                    setIsRefreshing(false);
                    onEnvironmentFilterChange(parsedValue);

                    // *After* the query has been fetched, we call
                    // refetch again to re-render with the updated data.
                    // At this point the data for the query should
                    // be cached, so we use the 'store-only'
                    // fetchPolicy to avoid suspending.
                    refetch({
                        ...queryVariables,
                        environmentScopes,
                    }, {
                        fetchPolicy: 'store-only'
                    });
                },
                error: () => {
                    setIsRefreshing(false);
                }
            });
    };

    const environmentScopes = useMemo(() => ['*', ...project.environmentNames], [project]);

    if (variableSet) {
        const variables = variableSet.variables.edges?.map(edge => edge?.node) ?? [];

        return (
            <Box>
                <Paper variant="outlined">
                    <Paper sx={{ borderBottomLeftRadius: 0, borderBottomRightRadius: 0 }}>
                        <Box p={2} display="flex" alignItems="center" justifyContent="space-between">
                            <Typography variant="subtitle1">{variableSet.variables.totalCount} variable{variableSet.variables.totalCount === 1 ? '' : 's'}</Typography>
                            {variableSet.latest && <Button
                                    variant="outlined"
                                    color="secondary"
                                    onClick={() => onNewVariable(pipelineType)}
                                >
                                    New Variable
                                </Button>}
                        </Box>
                    </Paper>
                    <Box p={2}>
                        {(variables.length > 0 || search !== '' || environmentFilter.length > 0) && <Box sx={{
                            display: 'flex',
                            flexDirection: 'row',
                            '& > *:not(:last-child)': { mr: 2 },
                            [theme.breakpoints.down('md')]: {
                                flexDirection: 'column-reverse',
                                '& > *:not(:last-child)': { mr: 0 },
                                '& > *': { mb: `${theme.spacing(2)} !important` },
                            }
                        }}>
                            <SearchInput
                                placeholder="search for variable by key"
                                fullWidth
                                onChange={event => fetch(event.target.value.toLowerCase().trim())}
                                onKeyDown={onKeyDown}
                            />
                            {pipelineType === 'DEPLOYMENT' && <FormControl sx={{ minWidth: 300 }}>
                                <Select
                                    multiple
                                    displayEmpty
                                    value={environmentFilter}
                                    onChange={handleEnvironmentFilterChange}
                                    input={<OutlinedInput size="small" />}
                                    renderValue={(selected) => {
                                        if (selected.length === 0) {
                                            return <Typography color="textSecondary">Filter by environment scope</Typography>;
                                        }

                                        return selected.map(v => v === '*' ? 'All Environments' : v).join(', ');
                                    }}
                                >
                                    {environmentFilter.length > 0 && <MenuItem value="">
                                        <Typography color="textSecondary">
                                            Unselect all
                                        </Typography>
                                    </MenuItem>}
                                    {environmentScopes.map((name) => (
                                        <MenuItem key={name} value={name}>
                                            <Checkbox checked={environmentFilter.indexOf(name) > -1} />
                                            <ListItemText primary={name === '*' ? 'All Environments' : name} />
                                        </MenuItem>
                                    ))}
                                </Select>
                            </FormControl>}
                            <Button
                                color="info"
                                variant="outlined"
                                sx={{ whiteSpace: 'nowrap', minWidth: 114 }}
                                onClick={() => setShowValues(!showValues)}
                            >
                                {showValues ? 'Hide Values' : 'Show Values'}
                            </Button>
                        </Box>}
                        {variables.length === 0 && (search !== '' || environmentFilter.length > 0) && <Typography
                            sx={{
                                mt: 2,
                                padding: 4,
                            }}
                            align="center"
                            color="textSecondary"
                        >
                            No variables matching filters
                        </Typography>}
                        {variables.length === 0 && search === '' && environmentFilter.length === 0 && <Typography
                            sx={{
                                mt: 2,
                                padding: 4,
                            }}
                            align="center"
                            color="textSecondary"
                        >
                            This variable set doesn't have any {pipelineType === 'DEPLOYMENT' ? 'deployment' : 'runbook'} variables
                        </Typography>}
                        {variables.length > 0 && <InfiniteScroll
                            dataLength={variableSet.variables.edges?.length ?? 0}
                            next={() => loadNext(INITIAL_ITEM_COUNT)}
                            hasMore={hasNext}
                            loader={<ListSkeleton rowCount={3} />}
                        >
                            <TableContainer>
                                <Table sx={isRefreshing ? { opacity: 0.5 } : null}>
                                    <TableHead>
                                        <TableRow>
                                            <TableCell>
                                                <Typography color="textSecondary">Key</Typography>
                                            </TableCell>
                                            <TableCell>
                                                <Typography color="textSecondary">Value</Typography>
                                            </TableCell>
                                            {pipelineType === 'DEPLOYMENT' && <TableCell>
                                                <Typography color="textSecondary">Environment</Typography>
                                            </TableCell>}
                                            <TableCell>
                                                <Typography color="textSecondary">Created</Typography>
                                            </TableCell>
                                            <TableCell></TableCell>
                                        </TableRow>
                                    </TableHead>
                                    <TableBody>
                                        {variableSet.variables.edges?.map((edge) => edge?.node ? <ProjectVariableListItem
                                            key={edge.node.id}
                                            showValue={showValues}
                                            fragmentRef={edge.node}
                                            latest={variableSet.latest || false}
                                            onShowHistory={onShowVariableHistory}
                                            onEdit={onEditVariable}
                                            onDelete={onDeleteVariable}
                                        /> : null)}
                                    </TableBody>
                                </Table>
                            </TableContainer>
                        </InfiniteScroll>}
                    </Box>
                </Paper>
            </Box>
        );
    } else {
        return <Box display="flex" justifyContent="center" mt={4}>
            <Typography color="textSecondary">Project variable set with revision {revision} not found</Typography>
        </Box>;
    }
}

export default ProjectVariableSet;
