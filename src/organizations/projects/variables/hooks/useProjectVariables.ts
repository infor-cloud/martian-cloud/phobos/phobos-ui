import { useEffect, useState } from 'react';
import { fetchQuery, useRelayEnvironment } from 'react-relay/hooks';
import graphql from "babel-plugin-relay/macro";
import { MutationError } from '../../../../common/error';
import { useProjectVariablesQuery, useProjectVariablesQuery$data } from './__generated__/useProjectVariablesQuery.graphql';

export type VariableNode = NonNullable<
  NonNullable<
    useProjectVariablesQuery$data['node']
  >['variables']
    >['edges'];

const PAGE_COUNT = 100;

export function useProjectVariables(variableSetPrn: string) {
    const environment = useRelayEnvironment();
    const [variables, setVariables] = useState<VariableNode[]>([]);
    const [isLoading, setIsLoading] = useState(false);
    const [error, setError] = useState<MutationError>();

    useEffect(() => {
        const fetchVariables = async (after: string | null = null, allVariables: VariableNode[] = []) => {
            try {
                setIsLoading(true);
                const response = await fetchQuery<useProjectVariablesQuery>(
                    environment,
                    graphql`
                        query useProjectVariablesQuery($id: String!, $first: Int!, $after: String) {
                            node(id: $id) {
                                ... on ProjectVariableSet {
                                    variables(first: $first, after: $after) {
                                        edges {
                                            node {
                                                id
                                                key
                                                value
                                                pipelineType
                                                environmentScope
                                            }
                                        }
                                        pageInfo {
                                            hasNextPage
                                            endCursor
                                        }
                                    }
                                }
                            }
                        }
                    `,
                    {
                        id: variableSetPrn,
                        first: PAGE_COUNT,
                        after,
                    },
                    { fetchPolicy: 'network-only' }
                ).toPromise();

                const currentNodes = response?.node?.variables?.edges?.map((edge: any) => edge.node) ?? [];
                const nextNodes = [...allVariables, ...currentNodes];

                if (response?.node?.variables?.pageInfo?.hasNextPage) {
                    await fetchVariables(response.node.variables.pageInfo.endCursor, nextNodes);
                } else {
                    setVariables(nextNodes);
                    setError(undefined);
                }
            } catch (error) {
                setError(error as MutationError);
            } finally {
                setIsLoading(false);
            }
        };

        fetchVariables();
    }, [environment, variableSetPrn]);

    return { variables, isLoading, error };
}
