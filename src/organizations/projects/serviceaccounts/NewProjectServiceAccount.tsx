import { Box } from '@mui/material';
import { useOutletContext } from 'react-router-dom';
import { useFragment } from 'react-relay/hooks';
import graphql from 'babel-plugin-relay/macro';
import ProjectBreadcrumbs from '../ProjectBreadcrumbs';
import NewServiceAccount from '../../../serviceaccounts/NewServiceAccount';
import { GetConnections } from './ProjectServiceAccounts';
import { NewProjectServiceAccountFragment_project$key } from './__generated__/NewProjectServiceAccountFragment_project.graphql';

function NewProjectServiceAccount() {
    const context = useOutletContext<NewProjectServiceAccountFragment_project$key>();

    const project = useFragment<NewProjectServiceAccountFragment_project$key>(
        graphql`
            fragment NewProjectServiceAccountFragment_project on Project
            {
                id
                metadata {
                    prn
                }
            }
        `, context
    );

    return (
        <Box>
            <ProjectBreadcrumbs
                prn={project.metadata.prn}
                childRoutes={[
                    { title: "service accounts", path: 'service_accounts' },
                    { title: "new", path: 'new' },
                ]}
            />
            <NewServiceAccount
                projectId={project.id}
                getConnections={() => GetConnections(project.id)}
            />
        </Box>
    );
}

export default NewProjectServiceAccount;
