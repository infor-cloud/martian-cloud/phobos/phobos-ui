import React, { useMemo, useState } from 'react';
import { Box, Button, Typography, useTheme } from '@mui/material';
import throttle from 'lodash.throttle';
import SearchInput from '../../../common/SearchInput';
import graphql from 'babel-plugin-relay/macro';
import { ConnectionHandler, fetchQuery, useFragment, useLazyLoadQuery, usePaginationFragment, useRelayEnvironment } from "react-relay/hooks";
import { Link as RouterLink, useOutletContext } from 'react-router-dom';
import ProjectBreadcrumbs from "../ProjectBreadcrumbs";
import ServiceAccountList from '../../../serviceaccounts/ServiceAccountList';
import { ProjectServiceAccountsQuery } from './__generated__/ProjectServiceAccountsQuery.graphql';
import { ProjectServiceAccountsPaginationQuery } from './__generated__/ProjectServiceAccountsPaginationQuery.graphql';
import { ProjectServiceAccountsFragment_project$key } from './__generated__/ProjectServiceAccountsFragment_project.graphql';
import { ProjectServiceAccountsFragment_serviceAccounts$key } from './__generated__/ProjectServiceAccountsFragment_serviceAccounts.graphql';

const INITIAL_ITEM_COUNT = 100;

const DESCRIPTION = 'Service accounts provide access to the Phobos API using a token from an OpenID Connect-compatible identity provider.';

const NewButton =
    <Button
        sx={{ minWidth: 200 }}
        component={RouterLink}
        variant="outlined"
        to="new"
    >
        New Service Account
    </Button>;

const query = graphql`
    query ProjectServiceAccountsQuery($projectId: String!, $first: Int, $last: Int, $after: String, $before: String, $search: String) {
        ...ProjectServiceAccountsFragment_serviceAccounts
    }
`;

export function GetConnections(projectId: string): [string] {
    const connectionId = ConnectionHandler.getConnectionID(
        projectId,
        'ProjectServiceAccounts_serviceAccounts',
    );
    return [connectionId];
}

function ProjectServiceAccounts() {
    const theme = useTheme();
    const [search, setSearch] = useState<string | undefined>('');
    const [isRefreshing, setIsRefreshing] = useState(false);
    const context = useOutletContext<ProjectServiceAccountsFragment_project$key>();

    const project = useFragment<ProjectServiceAccountsFragment_project$key>(graphql`
        fragment ProjectServiceAccountsFragment_project on Project
        {
            id
            metadata {
                prn
            }
        }
    `, context);

const queryData = useLazyLoadQuery<ProjectServiceAccountsQuery>(query, { projectId: project.id, first: INITIAL_ITEM_COUNT }, { fetchPolicy: 'store-and-network' });

const { data, loadNext, hasNext, refetch } = usePaginationFragment<ProjectServiceAccountsPaginationQuery, ProjectServiceAccountsFragment_serviceAccounts$key>(
    graphql`
  fragment ProjectServiceAccountsFragment_serviceAccounts on Query
    @refetchable(queryName: "ProjectServiceAccountsPaginationQuery") {
        node (id: $projectId) {
            ... on Project {
            serviceAccounts(
                after: $after
                before: $before
                first: $first
                last: $last
                search: $search
            ) @connection(key: "ProjectServiceAccounts_serviceAccounts") {
                totalCount
                edges {
                    node {
                        id
                    }
                }
                ...ServiceAccountListFragment_serviceAccounts
            }

            }
        }
    }
`, queryData);

const environment = useRelayEnvironment();

const fetch = useMemo(
    () =>
        throttle(
            (input?: string) => {
                setIsRefreshing(true);

                const normalizedInput = input?.trim();

                fetchQuery(environment, query, { first: INITIAL_ITEM_COUNT, projectId: project.id, search: normalizedInput })
                    .subscribe({
                        complete: () => {
                            setIsRefreshing(false);
                            setSearch(input);

                            // *After* the query has been fetched, we call
                            // refetch again to re-render with the updated data.
                            // At this point the data for the query should
                            // be cached, so we use the 'store-only'
                            // fetchPolicy to avoid suspending.
                            refetch({
                                first: INITIAL_ITEM_COUNT,
                                search: normalizedInput
                            }, {
                                fetchPolicy: 'store-only'
                            });
                        },
                        error: () => {
                            setIsRefreshing(false);
                        }
                    });
            },
            2000,
            { leading: false, trailing: true }
        ),
    [environment, refetch, project.id]
);

const onSearchChange = (event: React.ChangeEvent<HTMLInputElement>) => {
    fetch(event.target.value.toLowerCase().trim());
};

const onKeyDown = (event: React.KeyboardEvent<HTMLInputElement>) => {
    // Only handle enter key type
    if (event.key === 'Enter') {
        fetch.flush();
    }
};

const edges = data.node?.serviceAccounts?.edges ?? [];

    return (
        <Box>
            <ProjectBreadcrumbs
                prn={project.metadata.prn}
                childRoutes={[
                    { title: "service accounts", path: 'service_accounts' }
                ]}
            />
            <Box>{(search !== '' || edges.length !== 0) && (data.node?.serviceAccounts) && <Box>
                <Box>
                    <Box sx={{
                        display: 'flex',
                        flexDirection: 'row',
                        justifyContent: 'space-between',
                        [theme.breakpoints.down('md')]: {
                            flexDirection: 'column',
                            alignItems: 'flex-start',
                            '& > *': { marginBottom: 2 },
                        }
                    }}>
                        <Box>
                            <Typography variant="h5" gutterBottom>Service Accounts</Typography>
                            <Typography variant="body2">
                                {DESCRIPTION}
                            </Typography>
                        </Box>
                        <Box>
                            {NewButton}
                        </Box>
                    </Box>
                    <SearchInput
                        sx={{ marginTop: 2, marginBottom: 2 }}
                        placeholder="search for service accounts"
                        fullWidth
                        onChange={onSearchChange}
                        onKeyDown={onKeyDown}
                    />
                </Box>
                <ServiceAccountList
                    showOrgChip
                    fragmentRef={data.node.serviceAccounts}
                    search={search}
                    isRefreshing={isRefreshing}
                    loadNext={loadNext}
                    hasNext={hasNext}
                />
            </Box>}
                {search === '' && edges.length === 0 && <Box sx={{ marginTop: 4 }} display="flex" justifyContent="center">
                    <Box padding={4} display="flex" flexDirection="column" justifyContent="center" alignItems="center" sx={{ maxWidth: 600 }}>
                        <Typography variant="h6">Get started with service accounts</Typography>
                        <Typography color="textSecondary" align="center" sx={{ marginBottom: 2 }}>
                            {DESCRIPTION}
                        </Typography>
                        {NewButton}
                    </Box>
                </Box>}
            </Box>
        </Box>
    );
}

export default ProjectServiceAccounts;
