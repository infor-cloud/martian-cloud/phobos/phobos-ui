/**
 * @generated SignedSource<<1590306f93680a22fa7f837de2808a80>>
 * @lightSyntaxTransform
 * @nogrep
 */

/* tslint:disable */
/* eslint-disable */
// @ts-nocheck

import { Fragment, ReaderFragment } from 'relay-runtime';
import { FragmentRefs } from "relay-runtime";
export type ProjectServiceAccountsFragment_project$data = {
  readonly id: string;
  readonly metadata: {
    readonly prn: string;
  };
  readonly " $fragmentType": "ProjectServiceAccountsFragment_project";
};
export type ProjectServiceAccountsFragment_project$key = {
  readonly " $data"?: ProjectServiceAccountsFragment_project$data;
  readonly " $fragmentSpreads": FragmentRefs<"ProjectServiceAccountsFragment_project">;
};

const node: ReaderFragment = {
  "argumentDefinitions": [],
  "kind": "Fragment",
  "metadata": null,
  "name": "ProjectServiceAccountsFragment_project",
  "selections": [
    {
      "alias": null,
      "args": null,
      "kind": "ScalarField",
      "name": "id",
      "storageKey": null
    },
    {
      "alias": null,
      "args": null,
      "concreteType": "ResourceMetadata",
      "kind": "LinkedField",
      "name": "metadata",
      "plural": false,
      "selections": [
        {
          "alias": null,
          "args": null,
          "kind": "ScalarField",
          "name": "prn",
          "storageKey": null
        }
      ],
      "storageKey": null
    }
  ],
  "type": "Project",
  "abstractKey": null
};

(node as any).hash = "5b10df7258b4cefe809a53854d9a4839";

export default node;
