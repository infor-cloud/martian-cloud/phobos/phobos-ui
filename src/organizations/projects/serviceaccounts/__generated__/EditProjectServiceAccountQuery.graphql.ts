/**
 * @generated SignedSource<<fb2ae3b517b0984deb42cb6241875c24>>
 * @lightSyntaxTransform
 * @nogrep
 */

/* tslint:disable */
/* eslint-disable */
// @ts-nocheck

import { ConcreteRequest, Query } from 'relay-runtime';
export type ScopeType = "GLOBAL" | "ORGANIZATION" | "PROJECT" | "%future added value";
export type EditProjectServiceAccountQuery$variables = {
  id: string;
};
export type EditProjectServiceAccountQuery$data = {
  readonly node: {
    readonly name?: string;
    readonly scope?: ScopeType;
  } | null | undefined;
};
export type EditProjectServiceAccountQuery = {
  response: EditProjectServiceAccountQuery$data;
  variables: EditProjectServiceAccountQuery$variables;
};

const node: ConcreteRequest = (function(){
var v0 = [
  {
    "defaultValue": null,
    "kind": "LocalArgument",
    "name": "id"
  }
],
v1 = [
  {
    "kind": "Variable",
    "name": "id",
    "variableName": "id"
  }
],
v2 = {
  "kind": "InlineFragment",
  "selections": [
    {
      "alias": null,
      "args": null,
      "kind": "ScalarField",
      "name": "name",
      "storageKey": null
    },
    {
      "alias": null,
      "args": null,
      "kind": "ScalarField",
      "name": "scope",
      "storageKey": null
    }
  ],
  "type": "ServiceAccount",
  "abstractKey": null
};
return {
  "fragment": {
    "argumentDefinitions": (v0/*: any*/),
    "kind": "Fragment",
    "metadata": null,
    "name": "EditProjectServiceAccountQuery",
    "selections": [
      {
        "alias": null,
        "args": (v1/*: any*/),
        "concreteType": null,
        "kind": "LinkedField",
        "name": "node",
        "plural": false,
        "selections": [
          (v2/*: any*/)
        ],
        "storageKey": null
      }
    ],
    "type": "Query",
    "abstractKey": null
  },
  "kind": "Request",
  "operation": {
    "argumentDefinitions": (v0/*: any*/),
    "kind": "Operation",
    "name": "EditProjectServiceAccountQuery",
    "selections": [
      {
        "alias": null,
        "args": (v1/*: any*/),
        "concreteType": null,
        "kind": "LinkedField",
        "name": "node",
        "plural": false,
        "selections": [
          {
            "alias": null,
            "args": null,
            "kind": "ScalarField",
            "name": "__typename",
            "storageKey": null
          },
          (v2/*: any*/),
          {
            "alias": null,
            "args": null,
            "kind": "ScalarField",
            "name": "id",
            "storageKey": null
          }
        ],
        "storageKey": null
      }
    ]
  },
  "params": {
    "cacheID": "169c4dc92b6ec4dde2965ac2b3af02a2",
    "id": null,
    "metadata": {},
    "name": "EditProjectServiceAccountQuery",
    "operationKind": "query",
    "text": "query EditProjectServiceAccountQuery(\n  $id: String!\n) {\n  node(id: $id) {\n    __typename\n    ... on ServiceAccount {\n      name\n      scope\n    }\n    id\n  }\n}\n"
  }
};
})();

(node as any).hash = "62706f7c7174d273a4ff2ddc8d4647ba";

export default node;
