import { Box, Typography } from '@mui/material';
import { useOutletContext, useParams } from 'react-router-dom';
import ProjectBreadcrumbs from '../ProjectBreadcrumbs';
import graphql from 'babel-plugin-relay/macro';
import { useFragment, useLazyLoadQuery } from "react-relay/hooks";
import EditServiceAccount from '../../../serviceaccounts/EditServiceAccount';
import { EditProjectServiceAccountFragment_project$key } from './__generated__/EditProjectServiceAccountFragment_project.graphql';
import { EditProjectServiceAccountQuery } from './__generated__/EditProjectServiceAccountQuery.graphql';

function EditProjectServiceAccount() {
    const serviceAccountId = useParams().serviceAccountId as string;
    const context = useOutletContext<EditProjectServiceAccountFragment_project$key>();

    const project = useFragment<EditProjectServiceAccountFragment_project$key>(
        graphql`
            fragment EditProjectServiceAccountFragment_project on Project
            {
                id
                metadata {
                    prn
                }
            }
        `, context
    );

    const queryData = useLazyLoadQuery<EditProjectServiceAccountQuery>(graphql`
        query EditProjectServiceAccountQuery($id: String!) {
            node(id: $id) {
                ... on ServiceAccount {
                    name
                    scope
                }
            }
        }
    `, { id: serviceAccountId });

    const serviceAccount = queryData.node as any;

    return queryData.node ? (
        <Box>
            <ProjectBreadcrumbs
                prn={project.metadata.prn}
                childRoutes={[
                    { title: "service accounts", path: 'service_accounts' },
                    { title: serviceAccount.name, path: serviceAccount.id },
                    { title: "edit", path: 'edit' },
                ]}
            />
            <EditServiceAccount />
        </Box>
    ) : <Box display="flex" justifyContent="center" marginTop={4}>
        <Typography color="textSecondary">Service account not found</Typography>
    </Box>;
}

export default EditProjectServiceAccount;
