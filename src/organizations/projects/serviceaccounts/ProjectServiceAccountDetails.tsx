import { Box, Typography } from '@mui/material';
import ProjectBreadcrumbs from '../ProjectBreadcrumbs';
import { useFragment, useLazyLoadQuery } from 'react-relay/hooks';
import { useOutletContext, useParams } from 'react-router-dom';
import graphql from 'babel-plugin-relay/macro';
import ServiceAccountDetails from '../../../serviceaccounts/ServiceAccountDetails';
import { GetConnections } from './ProjectServiceAccounts';
import { ProjectServiceAccountDetailsQuery } from './__generated__/ProjectServiceAccountDetailsQuery.graphql';
import { ProjectServiceAccountDetailsFragment_project$key } from './__generated__/ProjectServiceAccountDetailsFragment_project.graphql';

function ProjectServiceAccountDetails() {
    const context = useOutletContext<ProjectServiceAccountDetailsFragment_project$key>();
    const serviceAccountId = useParams().serviceAccountId as string;

    const project = useFragment<ProjectServiceAccountDetailsFragment_project$key>(
        graphql`
            fragment ProjectServiceAccountDetailsFragment_project on Project
            {
                id
                metadata {
                    prn
                }
            }
        `, context
    );

    const data = useLazyLoadQuery<ProjectServiceAccountDetailsQuery>(graphql`
        query ProjectServiceAccountDetailsQuery($id: String!) {
            node(id: $id) {
                ... on ServiceAccount {
                    id
                    name
                }
            }
        }
    `, { id: serviceAccountId }, { fetchPolicy: 'store-and-network' });

    if (data.node) {

        const serviceAccount = data.node as any;

        return (
            <Box>
                <ProjectBreadcrumbs
                    prn={project.metadata.prn}
                    childRoutes={[
                        { title: "service accounts", path: 'service_accounts' },
                        { title: serviceAccount.name, path: serviceAccount.id }
                    ]}
                />
                <ServiceAccountDetails getConnections={() => GetConnections(project.id)} />
            </Box>
        );
    }
    else {
        return <Box display="flex" justifyContent="center" marginTop={4}>
            <Typography color="textSecondary">Service account with ID {serviceAccountId} not found</Typography>
        </Box>;
    }
}

export default ProjectServiceAccountDetails;
