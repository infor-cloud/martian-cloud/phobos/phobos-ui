import Box from '@mui/material/Box';
import graphql from 'babel-plugin-relay/macro';
import { useFragment } from "react-relay/hooks";
import { useOutletContext } from 'react-router-dom';
import ProjectBreadcrumbs from '../ProjectBreadcrumbs';
import { GetConnections } from './ProjectApprovalRules';
import NewApprovalRule from '../../../approvalrules/NewApprovalRule';
import { NewProjectApprovalRuleFragment_project$key } from './__generated__/NewProjectApprovalRuleFragment_project.graphql';

function NewProjectApprovalRule() {
    const context = useOutletContext<NewProjectApprovalRuleFragment_project$key>();

    const project = useFragment<NewProjectApprovalRuleFragment_project$key>(
        graphql`
            fragment NewProjectApprovalRuleFragment_project on Project
            {
                id
                metadata {
                    prn
                }
            }
        `, context
    );

    return (
        <Box>
            <ProjectBreadcrumbs
                prn={project.metadata.prn}
                childRoutes={[
                    { title: "approval rules", path: 'approval_rules' },
                    { title: "new", path: 'new' }
                ]}
            />
            <NewApprovalRule
                projectId={project.id}
                getConnections={() => GetConnections(project.id)}
            />
        </Box>
    );
}

export default NewProjectApprovalRule;
