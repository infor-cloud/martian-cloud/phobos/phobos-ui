import { Box, Button, Typography } from '@mui/material';
import graphql from 'babel-plugin-relay/macro';
import { ConnectionHandler, useFragment, useLazyLoadQuery, usePaginationFragment } from 'react-relay/hooks';
import { Link as RouterLink, useOutletContext } from 'react-router-dom';
import ProjectBreadcrumbs from '../ProjectBreadcrumbs';
import ApprovalRuleList from '../../../approvalrules/ApprovalRuleList';
import { ProjectApprovalRulesFragment_project$key } from './__generated__/ProjectApprovalRulesFragment_project.graphql';
import { ProjectApprovalRulesQuery } from './__generated__/ProjectApprovalRulesQuery.graphql';
import { ProjectApprovalRulesPaginationQuery } from './__generated__/ProjectApprovalRulesPaginationQuery.graphql';
import { ProjectApprovalRulesFragment_approvalRules$key } from './__generated__/ProjectApprovalRulesFragment_approvalRules.graphql';

const INITIAL_ITEM_COUNT = 100;

const DESCRIPTION = 'Approval rules specify the users, service accounts, and teams authorized to approve pipeline tasks.';

const query = graphql`
    query ProjectApprovalRulesQuery($projectId: String!, $first: Int!, $last: Int, $after: String, $before: String) {
        ...ProjectApprovalRulesFragment_approvalRules
    }`;

export function GetConnections(projectId: string): [string] {
    const connectionId = ConnectionHandler.getConnectionID(
        projectId,
        'ProjectApprovalRules_approvalRules'
    );
    return [connectionId];
}

function ProjectApprovalRules() {
    const context = useOutletContext<ProjectApprovalRulesFragment_project$key>();

    const project = useFragment<ProjectApprovalRulesFragment_project$key>(graphql`
        fragment ProjectApprovalRulesFragment_project on Project
        {
            id
            name
            metadata {
                prn
            }
        }
    `, context);

    const queryData = useLazyLoadQuery<ProjectApprovalRulesQuery>(query, { projectId: project.id, first: INITIAL_ITEM_COUNT },
        { fetchPolicy: 'store-and-network' });

    const { data, loadNext, hasNext } = usePaginationFragment<ProjectApprovalRulesPaginationQuery, ProjectApprovalRulesFragment_approvalRules$key>(
        graphql`
    fragment ProjectApprovalRulesFragment_approvalRules on Query
    @refetchable(queryName: "ProjectApprovalRulesPaginationQuery") {
        node(id: $projectId) {
            ... on Project {
                id
                approvalRules(
                    after: $after
                    before: $before
                    first: $first
                    last: $last
                ) @connection(key: "ProjectApprovalRules_approvalRules") {
                    totalCount
                    edges {
                        node {
                            id
                        }
                    }
                    ...ApprovalRuleListFragment_approvalRules
                }
            }
        }
    }`, queryData
    );

    return (
        <Box>
            <ProjectBreadcrumbs
                prn={project.metadata.prn}
                childRoutes={[
                    { title: "approval rules", path: 'approval_rules' }
                ]}
            />
            <Box>{data.node && data.node.approvalRules && data.node.approvalRules?.edges && data.node.approvalRules?.edges.length > 0 ? <ApprovalRuleList
                showOrgChip
                loadNext={loadNext}
                hasNext={hasNext}
                getConnections={() => GetConnections(project.id)}
                fragmentRef={data.node.approvalRules}
            />
                : <Box sx={{ mt: 4 }} display="flex" justifyContent="center">
                    <Box padding={4} display="flex" flexDirection="column" justifyContent="center" alignItems="center" sx={{ maxWidth: 600 }}>
                        <Typography variant="h6">Add an approval rule</Typography>
                        <Typography color="textSecondary" align="center" sx={{ mb: 2 }}>
                            {DESCRIPTION}
                        </Typography>
                        <Button component={RouterLink} variant="outlined" to="new">Add Approval Rule</Button>
                    </Box>
                </Box>}
            </Box>
        </Box>
    );
}

export default ProjectApprovalRules;
