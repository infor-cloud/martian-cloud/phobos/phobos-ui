/**
 * @generated SignedSource<<40b1085355b450b1e74e4a6110da0805>>
 * @lightSyntaxTransform
 * @nogrep
 */

/* tslint:disable */
/* eslint-disable */
// @ts-nocheck

import { Fragment, ReaderFragment } from 'relay-runtime';
import { FragmentRefs } from "relay-runtime";
export type NewProjectApprovalRuleFragment_project$data = {
  readonly id: string;
  readonly metadata: {
    readonly prn: string;
  };
  readonly " $fragmentType": "NewProjectApprovalRuleFragment_project";
};
export type NewProjectApprovalRuleFragment_project$key = {
  readonly " $data"?: NewProjectApprovalRuleFragment_project$data;
  readonly " $fragmentSpreads": FragmentRefs<"NewProjectApprovalRuleFragment_project">;
};

const node: ReaderFragment = {
  "argumentDefinitions": [],
  "kind": "Fragment",
  "metadata": null,
  "name": "NewProjectApprovalRuleFragment_project",
  "selections": [
    {
      "alias": null,
      "args": null,
      "kind": "ScalarField",
      "name": "id",
      "storageKey": null
    },
    {
      "alias": null,
      "args": null,
      "concreteType": "ResourceMetadata",
      "kind": "LinkedField",
      "name": "metadata",
      "plural": false,
      "selections": [
        {
          "alias": null,
          "args": null,
          "kind": "ScalarField",
          "name": "prn",
          "storageKey": null
        }
      ],
      "storageKey": null
    }
  ],
  "type": "Project",
  "abstractKey": null
};

(node as any).hash = "b97181d591fd19f99e4b166cfb7de9c4";

export default node;
