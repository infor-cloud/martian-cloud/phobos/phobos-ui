/**
 * @generated SignedSource<<7b3f936d9b28a65df33d39e6985b989b>>
 * @lightSyntaxTransform
 * @nogrep
 */

/* tslint:disable */
/* eslint-disable */
// @ts-nocheck

import { Fragment, ReaderFragment } from 'relay-runtime';
import { FragmentRefs } from "relay-runtime";
export type ProjectApprovalRulesFragment_project$data = {
  readonly id: string;
  readonly metadata: {
    readonly prn: string;
  };
  readonly name: string;
  readonly " $fragmentType": "ProjectApprovalRulesFragment_project";
};
export type ProjectApprovalRulesFragment_project$key = {
  readonly " $data"?: ProjectApprovalRulesFragment_project$data;
  readonly " $fragmentSpreads": FragmentRefs<"ProjectApprovalRulesFragment_project">;
};

const node: ReaderFragment = {
  "argumentDefinitions": [],
  "kind": "Fragment",
  "metadata": null,
  "name": "ProjectApprovalRulesFragment_project",
  "selections": [
    {
      "alias": null,
      "args": null,
      "kind": "ScalarField",
      "name": "id",
      "storageKey": null
    },
    {
      "alias": null,
      "args": null,
      "kind": "ScalarField",
      "name": "name",
      "storageKey": null
    },
    {
      "alias": null,
      "args": null,
      "concreteType": "ResourceMetadata",
      "kind": "LinkedField",
      "name": "metadata",
      "plural": false,
      "selections": [
        {
          "alias": null,
          "args": null,
          "kind": "ScalarField",
          "name": "prn",
          "storageKey": null
        }
      ],
      "storageKey": null
    }
  ],
  "type": "Project",
  "abstractKey": null
};

(node as any).hash = "204afcdcccdd799ca233babc78f0abb0";

export default node;
