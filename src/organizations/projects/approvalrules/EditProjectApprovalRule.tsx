import Box from '@mui/material/Box';
import { useOutletContext, useParams } from 'react-router-dom';
import graphql from 'babel-plugin-relay/macro';
import { useFragment } from 'react-relay/hooks';
import ProjectBreadcrumbs from '../ProjectBreadcrumbs';
import EditApprovalRule from '../../../approvalrules/EditApprovalRule';
import { EditProjectApprovalRuleFragment_project$key } from './__generated__/EditProjectApprovalRuleFragment_project.graphql';

function EditProjectApprovalRule() {
    const context = useOutletContext<EditProjectApprovalRuleFragment_project$key>();
    const approvalRuleId = useParams().approvalRuleId as string;

    const project = useFragment<EditProjectApprovalRuleFragment_project$key>(
        graphql`
            fragment EditProjectApprovalRuleFragment_project on Project
            {
                id
                metadata {
                    prn
                }
            }
        `, context
    );

    return (
        <Box>
            <ProjectBreadcrumbs
                prn={project.metadata.prn}
                childRoutes={[
                    { title: "approval rules", path: 'approval_rules' },
                    { title: "edit", path: `${approvalRuleId}/edit` },
                ]}
            />
            <EditApprovalRule projectId={project.id} />
        </Box>
    );
}

export default EditProjectApprovalRule;
