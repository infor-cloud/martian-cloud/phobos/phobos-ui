import { TableCell, TableRow } from '@mui/material';
import graphql from 'babel-plugin-relay/macro';
import { useFragment } from 'react-relay/hooks';
import { useOutletContext } from 'react-router-dom';
import DataTableCell from '../../../../common/DataTableCell';
import PipelineTemplateAutocomplete, { Option as PipelineTemplateOption } from './PipelineTemplateAutocomplete';
import PipelineTemplateVersionAutocomplete from './PipelineTemplateVersionAutocomplete';
import { DeploymentTemplate } from './ReleaseForm';
import { DeploymentTemplateListItem_project$key } from './__generated__/DeploymentTemplateListItem_project.graphql';

interface Props {
    template: DeploymentTemplate
    onChange: (template: DeploymentTemplate) => void
}

function DeploymentTemplateListItem({ template, onChange }: Props) {
    const context = useOutletContext<DeploymentTemplateListItem_project$key>();

    const data = useFragment<DeploymentTemplateListItem_project$key>(
        graphql`
            fragment DeploymentTemplateListItem_project on Project
            {
                id
            }
        `, context);

    const onPipelineTemplateVersionChange = (version: string | null) => {
        onChange({ ...template, version });
    };

    const onPipelineTemplateChange = (option: PipelineTemplateOption | null) => {
        onChange({ ...template, name: option?.name || null, version: option?.semanticVersion || null });
    };

    return (
        <TableRow
            sx={{ '&:last-child td, &:last-child th': { border: 0 }, height: 64 }}
        >
            <DataTableCell sx={{ fontWeight: 'bold', wordBreak: 'break-all' }}>
                {template.environmentName}
            </DataTableCell>
            <TableCell sx={{ wordBreak: 'break-all' }}>
                <PipelineTemplateAutocomplete
                    pipelineTemplateName={template.name}
                    projectId={data.id}
                    onSelected={onPipelineTemplateChange} />
            </TableCell>
            <TableCell>
                <PipelineTemplateVersionAutocomplete
                    projectId={data.id}
                    name={template.name}
                    version={template.version}
                    onSelected={onPipelineTemplateVersionChange} />
            </TableCell>
        </TableRow>
    );
}

export default DeploymentTemplateListItem
