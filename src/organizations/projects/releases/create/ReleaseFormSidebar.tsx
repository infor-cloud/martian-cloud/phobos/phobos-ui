import { Box, Link, Typography } from '@mui/material';

interface Props {
    showMoreInfo: boolean
}

function ReleaseFormSidebar({ showMoreInfo }: Props) {

    return (
        <Box p={3}>
            <Typography variant="h5" gutterBottom>Creating a Release</Typography>
            <Box pb={4}>
                <Typography>Before starting the process to create a release, ensure you have the following set up in Phobos:</Typography>
                <Box p={3} pb={0}>
                    <Typography>A release lifecycle within the{' '}
                        <Link
                            href='../../../../-/release_lifecycles'
                            target='_blank'
                            rel='noopener noreferrer'
                            underline='hover'
                        >parent organization{' '}
                        </Link>
                        or within this
                        <Link
                            href='../../-/release_lifecycles'
                            target='_blank'
                            rel='noopener noreferrer'
                            underline='hover'
                        > project.
                        </Link>
                    </Typography>
                </Box>
                <Box p={3} pb={0}>
                    <Typography>A pipeline template within this{' '}
                        <Link
                            href='../../-/pipeline_templates'
                            target='_blank'
                            rel='noopener noreferrer'
                            underline='hover'
                        >project.
                        </Link>
                    </Typography>
                </Box>
            </Box>
            <Box pb={4}>
                <Typography variant='h6' gutterBottom>Semantic Version</Typography>
                <Typography paragraph>Semantic versioning is used to tag the release in the repository. Learn more about{' '}
                    <Link
                        href='https://semver.org/'
                        target='_blank'
                        rel='noopener noreferrer'
                        underline='hover'
                    >semantic versioning</Link>.
                </Typography>
            </Box>
            <Box pb={4}>
                <Typography variant='h6' gutterBottom> Release Lifecycles</Typography>
                <Typography paragraph>Select a release lifecycle to where this release will deploy. The environments within the selected release lifecycle will be listed on the form.
                </Typography>
            </Box>
            {showMoreInfo && <>
                <Box pb={4}>
                    <Typography variant='h6' gutterBottom>Deployments</Typography>
                    <Typography paragraph>A deployment comprises a pipeline template that will run in each environment listed in the selected lifecycle.</Typography>
                    <Typography paragraph>Select a pipeline template for each environment. When a pipeline template is selected, a drop down menu will be generated so that you can choose a specific version of the pipeline template. By default, the latest version of the pipeline template version is selected.</Typography>
                </Box>
                <Box>
                    <Typography variant='h6' gutterBottom>Variables</Typography>
                    <Typography paragraph>You can create values to pass data directly to any variables that have been defined in your selected pipeline templates. Adding variables is optional and dependent on the specifications of your pipeline templates.</Typography>
                </Box>
            </>}
        </Box>
    );
}

export default ReleaseFormSidebar
