import DeleteIcon from '@mui/icons-material/Delete';
import { Box, IconButton, List, ListItem, ListItemText, Paper, Typography } from '@mui/material';
import { useMemo } from 'react';
import Gravatar from '../../../../common/Gravatar';
import StyledAvatar from '../../../../common/StyledAvatar';
import ParticipantAutocomplete, { BaseOption, Option, UserOption } from './ReleaseParticipantAutocomplete';

export interface UserParticipant {
    id: string
    type: 'user'
    email: string
    username: string
}

export interface TeamParticipant {
    id: string
    type: 'team'
    name: string
}

export type Participant = UserParticipant | TeamParticipant;

interface Props {
    participants: Participant[]
    onAdd: (participant: Participant) => void
    onRemove: (participant: Participant) => void
}

function ReleaseParticipantSelectionBox({ participants, onAdd, onRemove }: Props) {
    const onSelected = (value: Option | null) => {
        if (value) {
            switch (value.type) {
                case 'user': {
                    const user = value as UserOption;
                    const participant: UserParticipant = { id: user.id, type: 'user', email: user.email, username: user.username };
                    onAdd(participant);
                    break;
                }
                case 'team': {
                    const team = value as BaseOption;
                    const participant: TeamParticipant = { id: team.id, type: 'team', name: team.label };
                    onAdd(participant);
                    break;
                }
            }
        }
    };

    const selectedIds = useMemo(() => participants.reduce((accumulator: Set<string>, item: any) => {
        accumulator.add(item.id);
        return accumulator;
    }, new Set()), [participants]);

    return (
        <Paper variant="outlined" sx={{ padding: 2, backgroundColor: 'inherit' }}>
            <Box sx={{ mb: 2 }}>
                <ParticipantAutocomplete
                    onSelected={onSelected}
                    filterOptions={(options: Option[]) => options.filter(option => !selectedIds.has(option.id))}
                />
            </Box>
            <Typography color="textSecondary">
                {participants.length} participant{participants.length === 1 ? '' : 's'} selected
            </Typography>
            <List dense>
                {participants.map((participant: Participant) => (
                    <ListItem
                        disableGutters
                        secondaryAction={
                            <IconButton onClick={() => onRemove(participant)}>
                                <DeleteIcon />
                            </IconButton>}
                        key={participant.id}>
                        {participant.type === 'user' && <Gravatar sx={{ marginRight: 1 }} width={24} height={24} email={participant.email} />}
                        {participant.type === 'team' &&
                            <StyledAvatar sx={{ mr: 1 }}>{participant.name[0].toUpperCase()}</StyledAvatar>}
                        <ListItemText primary={participant.type === 'user' ? participant.username : participant.name} primaryTypographyProps={{ noWrap: true }} />
                    </ListItem>
                ))}
            </List>
        </Paper>
    );
}

export default ReleaseParticipantSelectionBox;
