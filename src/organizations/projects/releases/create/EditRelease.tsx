import LoadingButton from '@mui/lab/LoadingButton';
import { Box, Button, Divider, Typography } from '@mui/material';
import graphql from 'babel-plugin-relay/macro';
import { useState } from 'react';
import { useFragment, useLazyLoadQuery, useMutation } from 'react-relay/hooks';
import { Link as RouterLink, useNavigate, useOutletContext, useParams } from 'react-router-dom';
import { MutationError } from '../../../../common/error';
import ProjectBreadcrumbs from '../../ProjectBreadcrumbs';
import ReleaseForm, { FormData } from './ReleaseForm';
import { EditReleaseFragment_project$key } from './__generated__/EditReleaseFragment_project.graphql';
import { EditReleaseMutation } from './__generated__/EditReleaseMutation.graphql';
import { EditReleaseQuery } from './__generated__/EditReleaseQuery.graphql';
import moment from 'moment';

function EditRelease() {
    const navigate = useNavigate();
    const releaseId = useParams().releaseId as string;
    const context = useOutletContext<EditReleaseFragment_project$key>();

    const project = useFragment<EditReleaseFragment_project$key>(
        graphql`
            fragment EditReleaseFragment_project on Project
            {
                id
                organizationName
                metadata {
                    prn
                }
            }
        `, context);

    const queryData = useLazyLoadQuery<EditReleaseQuery>(graphql`
        query EditReleaseQuery($id: String!) {
            node(id: $id) {
                ... on Release {
                    id
                    semanticVersion
                    notes
                    dueDate
                }
            }
        }
    `, { id: releaseId });

    const release = queryData.node as any;

    const [error, setError] = useState<MutationError>()
    const [formData, setFormData] = useState<FormData>({
        semanticVersion: release.semanticVersion,
        notes: release.notes,
        lifecycleId: '',
        variables: [],
        variableSetRevision: null,
        deploymentTemplates: [],
        userParticipants: [],
        teamParticipants: [],
        dueDate: release.dueDate ? moment(release.dueDate) : null,
        validationErrors: false
    });

    const [commit, isInFlight] = useMutation<EditReleaseMutation>(graphql`
        mutation EditReleaseMutation($input: UpdateReleaseInput!) {
            updateRelease(input: $input) {
                release {
                    id
                    ...ReleaseCardItemFragment_release
                }
                problems {
                    message
                    field
                    type
                }
            }
        }
    `);

    const onSave = () => {
        commit({
            variables: {
                input: {
                    releaseId: releaseId,
                    notes: formData.notes,
                    dueDate: formData.dueDate
                }
            },
            onCompleted: (data) => {
                if (data.updateRelease.problems.length) {
                    setError({
                        severity: 'warning',
                        message: data.updateRelease.problems.map((problem: any) => problem.message).join('; ')
                    });
                } else if (!data.updateRelease.release) {
                    setError({
                        severity: 'error',
                        message: "Unexpected error occurred"
                    });
                }
                else {
                    navigate(`..`);
                }
            },
            onError: error => {
                setError({
                    severity: 'error',
                    message: `Unexpected error occurred: ${error.message}`
                });
            }
        });
    };

    const disabled = formData.validationErrors;

    return (
        <Box>
            <ProjectBreadcrumbs
                prn={project.metadata.prn}
                childRoutes={[
                    { title: "releases", path: 'releases' },
                    { title: release.semanticVersion, path: release.id },
                    { title: "edit", path: 'edit' }
                ]}
            />
            <Typography variant="h5" mb={2}>Edit Release</Typography>
            <ReleaseForm
                projectId={project.id}
                editMode
                error={error}
                data={formData}
                onChange={(data: FormData) => setFormData(data)}
            />
            <Divider />
            <Box mt={2}>
                <LoadingButton
                    sx={{ mr: 2 }}
                    loading={isInFlight}
                    disabled={disabled}
                    variant="outlined"
                    color="primary"
                    onClick={onSave}
                >
                    Update Release
                </LoadingButton>
                <Button component={RouterLink} color="inherit" to={-1 as any}>Cancel</Button>
            </Box>
        </Box>
    );
}

export default EditRelease;
