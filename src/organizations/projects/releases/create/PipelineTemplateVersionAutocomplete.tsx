import { Autocomplete, Box, Chip, CircularProgress, TextField, Typography } from '@mui/material';
import match from 'autosuggest-highlight/match';
import parse from 'autosuggest-highlight/parse';
import graphql from 'babel-plugin-relay/macro';
import throttle from 'lodash.throttle';
import React, { useEffect, useMemo, useState } from 'react';
import { fetchQuery, useRelayEnvironment } from 'react-relay/hooks';
import { PipelineTemplateVersionAutocompleteQuery } from '../__generated__/PipelineTemplateVersionAutocompleteQuery.graphql';

interface Option {
    readonly latest: boolean
    readonly semanticVersion: string
}

interface Props {
    projectId: string
    name: string | null
    version: string | null
    onSelected: (value: string | null) => void
}

function PipelineTemplateVersionAutocomplete({ projectId, name, version, onSelected }: Props) {
    const [options, setOptions] = useState<ReadonlyArray<Option>>([]);
    const [loading, setLoading] = useState<boolean>(false);
    const [inputValue, setInputValue] = useState('');

    const environment = useRelayEnvironment();

    const fetch = React.useMemo(
        () =>
            throttle(
                (
                    callback: (results: readonly Option[]) => void,
                ) => {
                    fetchQuery<PipelineTemplateVersionAutocompleteQuery>(
                        environment,
                        graphql`
                            query PipelineTemplateVersionAutocompleteQuery($id: String!, $name: String!) {
                                node(id: $id) {
                                    ... on Project {
                                        pipelineTemplates(first: 50, name: $name, versioned: true, sort: UPDATED_AT_DESC) {
                                            edges {
                                                node {
                                                    latest
                                                    semanticVersion
                                                }
                                            }
                                        }
                                    }
                                }
                            }
                        `,
                        { id: projectId, name: name as string },
                        { fetchPolicy: 'network-only' }
                    ).toPromise().then(async response => {
                        const options = response?.node?.pipelineTemplates?.edges?.map(edge => edge?.node as Option);
                        callback(options ?? []);
                    });
                },
                300,
            ),
        [environment, name, projectId],
    );

    useEffect(() => {
        let active = true;

        setLoading(true);

        if (name === null) {
            setOptions([]);
            setLoading(false);
            return;
        }

        fetch((results: readonly Option[]) => {
            if (active) {
                // Check if selected value is present in existing options
                if (version !== null) {
                    let resultsCopy = [...results];
                    const option = options.find(option => option.semanticVersion === version);
                    if (option) {
                        // Check if option is present in new options
                        const optionIndex = results.findIndex(option => option.semanticVersion === version);
                        if (optionIndex !== -1) {
                            resultsCopy[optionIndex] = option;
                        } else {
                            resultsCopy = [option, ...resultsCopy];
                        }
                    }
                    setOptions(resultsCopy);
                } else {
                    setOptions(results);
                }

                setLoading(false);
            }
        });

        return () => {
            active = false;
        };
    }, [fetch, inputValue]);

    const value = useMemo(
        () => options.find(option => option.semanticVersion === version) ?? null,
        [options, version]
    );

    return (
        <Autocomplete
            size="small"
            value={value}
            onChange={(event: React.SyntheticEvent, value: Option | null) => onSelected(value?.semanticVersion || null)}
            onInputChange={(_, newInputValue: string) => setInputValue(newInputValue)}
            isOptionEqualToValue={(option: Option, value: Option) => { return option.semanticVersion === value.semanticVersion }}
            getOptionLabel={(option: Option) => option.semanticVersion}
            renderOption={(props: React.HTMLAttributes<HTMLLIElement>, option: Option, { inputValue }) => {
                const matches = match(option.semanticVersion, inputValue);
                const parts = parse(option.semanticVersion, matches);
                return (
                    <Box component="li" {...props}>
                        <Box>
                            <Box display="flex">
                                {parts.map((part: any, index: number) =>
                                    <Typography
                                        key={index}
                                        style={{
                                            fontWeight: part.highlight ? 700 : 400,
                                        }}
                                    >
                                        {part.text}
                                    </Typography>
                                )}
                                {option.latest && <Chip component="span" size="small" color="secondary" sx={{ ml: 1 }} label="latest" />}
                            </Box>
                        </Box>
                    </Box>
                );
            }}
            options={options}
            loading={loading}
            renderInput={(params) => (
                <TextField
                    {...params}
                    placeholder='Version'
                    InputProps={{
                        ...params.InputProps,
                        endAdornment: (
                            <React.Fragment>
                                {loading ? <CircularProgress color="secondary" size={15} /> : null}
                                {params.InputProps.endAdornment}
                            </React.Fragment>
                        ),
                    }}
                />
            )}
        />
    );
}

export default PipelineTemplateVersionAutocomplete
