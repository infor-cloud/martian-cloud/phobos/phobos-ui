import LoadingButton from '@mui/lab/LoadingButton';
import { Box, Button, Divider } from '@mui/material';
import graphql from 'babel-plugin-relay/macro';
import { useState } from 'react';
import { useFragment, useLazyLoadQuery, useMutation } from 'react-relay/hooks';
import { Link as RouterLink, useNavigate, useOutletContext, useParams } from 'react-router-dom';
import { MutationError } from '../../../../common/error';
import ProjectBreadcrumbs from '../../ProjectBreadcrumbs';
import ReleaseDeploymentForm, { FormData } from './ReleaseDeploymentForm';
import { EditReleaseDeploymentFragment_project$key } from './__generated__/EditReleaseDeploymentFragment_project.graphql';
import { EditReleaseDeploymentMutation } from './__generated__/EditReleaseDeploymentMutation.graphql';
import { EditReleaseDeploymentQuery } from './__generated__/EditReleaseDeploymentQuery.graphql';

function EditReleaseDeployment() {
    const navigate = useNavigate();
    const releaseId = useParams().releaseId as string;
    const environmentName = useParams().environmentName as string;
    const context = useOutletContext<EditReleaseDeploymentFragment_project$key>();

    const project = useFragment<EditReleaseDeploymentFragment_project$key>(
        graphql`
            fragment EditReleaseDeploymentFragment_project on Project
            {
                id
                name
                organizationName
                metadata {
                    prn
                }
            }
        `, context);

    const queryData = useLazyLoadQuery<EditReleaseDeploymentQuery>(graphql`
        query EditReleaseDeploymentQuery($id: String!) {
            node(id: $id) {
                ... on Release {
                    id
                    semanticVersion
                    notes
                    pipeline {
                        stages {
                            nestedPipelines {
                                environmentName
                                latestPipeline {
                                    pipelineTemplate {
                                        id
                                        versioned
                                        name
                                        semanticVersion
                                    }
                                    variables {
                                        category
                                        key
                                        value
                                    }
                                }
                            }
                        }
                    }
                }
            }
        }
    `, { id: releaseId });

    const release = queryData.node as any;

    const deployment = release.pipeline.stages.
        flatMap((stage: any) => stage.nestedPipelines.
            flatMap((nestedPipeline: any) => nestedPipeline)).
        find((nestedPipeline: any) => nestedPipeline.environmentName === environmentName);

    const [error, setError] = useState<MutationError>()
    const [formData, setFormData] = useState<FormData>({
        variables: deployment.latestPipeline.variables.map((v: any) => ({
            ...v,
            environmentName: environmentName,
            id: v.key
        })),
        pipelineTemplateName: deployment.latestPipeline.pipelineTemplate.name,
        pipelineTemplateVersion: deployment.latestPipeline.pipelineTemplate.semanticVersion
    });

    const [commit, isInFlight] = useMutation<EditReleaseDeploymentMutation>(graphql`
        mutation EditReleaseDeploymentMutation($input: UpdateReleaseDeploymentPipelineInput!) {
            updateReleaseDeploymentPipeline(input: $input) {
                problems {
                    message
                    field
                    type
                }
            }
        }
    `);

    const onSave = () => {
        commit({
            variables: {
                input: {
                    releaseId: releaseId,
                    environmentName: environmentName,
                    variables: formData.variables,
                    pipelineTemplateId: `prn:pipeline_template:${project.organizationName}/${project.name}/${formData.pipelineTemplateName}/${formData.pipelineTemplateVersion}`
                }
            },
            onCompleted: (data) => {
                if (data.updateReleaseDeploymentPipeline.problems.length) {
                    setError({
                        severity: 'warning',
                        message: data.updateReleaseDeploymentPipeline.problems.map((problem: any) => problem.message).join('; ')
                    });
                } else {
                    navigate(`..`);
                }
            },
            onError: error => {
                setError({
                    severity: 'error',
                    message: `Unexpected error occurred: ${error.message}`
                });
            }
        });
    };

    return (
        <Box>
            <ProjectBreadcrumbs
                prn={project.metadata.prn}
                childRoutes={[
                    { title: "releases", path: 'releases' },
                    { title: release.semanticVersion, path: release.id },
                    { title: `edit ${environmentName} deployment`, path: `edit_deployment/${environmentName}` }
                ]}
            />
            <ReleaseDeploymentForm
                semanticVersion={release.semanticVersion}
                environmentName={environmentName}
                error={error}
                data={formData}
                projectId={project.id}
                onChange={(data: FormData) => setFormData(data)}
            />
            <Divider sx={{ opacity: 0.6 }} />
            <Box mt={4}>
                <LoadingButton
                    sx={{ mr: 2 }}
                    disabled={!formData.pipelineTemplateVersion}
                    loading={isInFlight}
                    variant="outlined"
                    color="primary"
                    onClick={onSave}
                >
                    Update
                </LoadingButton>
                <Button component={RouterLink} color="inherit" to={-1 as any}>Cancel</Button>
            </Box>
        </Box>
    );
}

export default EditReleaseDeployment;
