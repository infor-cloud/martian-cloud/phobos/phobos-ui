import { Autocomplete, Box, CircularProgress, TextField, Typography } from '@mui/material';
import match from 'autosuggest-highlight/match';
import parse from 'autosuggest-highlight/parse';
import graphql from 'babel-plugin-relay/macro';
import throttle from 'lodash.throttle';
import React, { useEffect, useMemo, useState } from 'react';
import { fetchQuery, useRelayEnvironment } from 'react-relay/hooks';
import { PipelineTemplateAutocompleteQuery } from './__generated__/PipelineTemplateAutocompleteQuery.graphql';

export interface Option {
    readonly name: string
    readonly semanticVersion: string
}

interface Props {
    pipelineTemplateName: string | null
    projectId: string
    onSelected: (option: Option | null) => void
}

function PipelineTemplateAutocomplete({ pipelineTemplateName, projectId, onSelected }: Props) {
    const [options, setOptions] = useState<ReadonlyArray<Option>>([]);
    const [loading, setLoading] = useState<boolean>(false);
    const [inputValue, setInputValue] = useState('');

    const environment = useRelayEnvironment();

    const fetch = React.useMemo(
        () =>
            throttle(
                (
                    request: { input: string },
                    callback: (results: readonly Option[]) => void,
                ) => {
                    fetchQuery<PipelineTemplateAutocompleteQuery>(
                        environment,
                        graphql`
                            query PipelineTemplateAutocompleteQuery($id: String!, $search: String!) {
                                node(id: $id) {
                                    ... on Project {
                                        pipelineTemplates(first: 50, versioned: true, latest: true, search: $search) {
                                            edges {
                                                node {
                                                    name
                                                    semanticVersion
                                                }
                                            }
                                        }
                                    }
                                }
                            }
                        `,
                        { id: projectId, search: request.input },
                        { fetchPolicy: 'network-only' }
                    ).toPromise().then(async response => {
                        const options = response?.node?.pipelineTemplates?.edges?.map(edge => edge?.node as Option);
                        callback(options || []);
                    });
                },
                300,
            ),
        [environment, projectId],
    );

    useEffect(() => {
        let active = true;

        setLoading(true);

        fetch({ input: inputValue }, (results: readonly Option[]) => {
            if (active) {
                // Check if selected value is present in existing options
                if (pipelineTemplateName !== null) {
                    let resultsCopy = [...results];
                    const option = options.find(option => option.name === pipelineTemplateName);
                    if (option) {
                        // Check if option is present in new options
                        const optionIndex = results.findIndex(option => option.name === pipelineTemplateName);
                        if (optionIndex !== -1) {
                            resultsCopy[optionIndex] = option;
                        } else {
                            resultsCopy = [option, ...resultsCopy];
                        }
                    }
                    setOptions(resultsCopy);
                } else {
                    setOptions(results);
                }

                setLoading(false);
            }
        });

        return () => {
            active = false;
        };
    }, [fetch, inputValue]);

    const value = useMemo(
        () => options.find(option => option.name === pipelineTemplateName) ?? null,
        [options, pipelineTemplateName]
    );

    return (
        <Autocomplete
            value={value}
            fullWidth
            size="small"
            onChange={(event: React.SyntheticEvent, value: Option | null) => onSelected(value)}
            onInputChange={(_, newInputValue: string) => setInputValue(newInputValue)}
            isOptionEqualToValue={(option: Option, value: Option) => option.name === value.name}
            getOptionLabel={(option: Option) => option.name}
            renderOption={(props: React.HTMLAttributes<HTMLLIElement>, option: Option, { inputValue }) => {
                const matches = match(option.name, inputValue);
                const parts = parse(option.name, matches);
                return (
                    <Box component="li" {...props}>
                        <Box>
                            <Typography>
                                {parts.map((part: any, index: number) => (
                                    <span
                                        key={index}
                                        style={{
                                            fontWeight: part.highlight ? 700 : 400,
                                        }}
                                    >
                                        {part.text}
                                    </span>
                                ))}
                            </Typography>
                        </Box>
                    </Box>
                );
            }}
            options={options}
            loading={loading}
            renderInput={(params) => (
                <TextField
                    {...params}
                    placeholder={pipelineTemplateName ?? 'Select a pipeline template'}
                    InputProps={{
                        ...params.InputProps,
                        endAdornment: (
                            <React.Fragment>
                                {loading ? <CircularProgress color="secondary" size={15} /> : null}
                                {params.InputProps.endAdornment}
                            </React.Fragment>
                        ),
                    }}
                />
            )}
        />
    );
}

export default PipelineTemplateAutocomplete
