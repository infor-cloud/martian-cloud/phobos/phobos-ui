import { Alert, Box, Button, CircularProgress, Divider, FormControlLabel, Paper, Stack, Switch, TextField, Typography } from '@mui/material';
import { AdapterMoment } from '@mui/x-date-pickers/AdapterMoment';
import { DateTimePicker } from '@mui/x-date-pickers/DateTimePicker';
import { LocalizationProvider } from '@mui/x-date-pickers/LocalizationProvider';
import moment, { Moment } from 'moment';
import { nanoid } from 'nanoid';
import { Suspense, useEffect, useMemo, useRef, useState } from 'react';
import { Link as RouterLink } from 'react-router-dom';
import { StyledCode } from '../../../../common/StyledCode';
import { MutationError } from '../../../../common/error';
import VariableDialog, { Variable } from '../../../../organizations/projects/pipelines/create/VariableDialog';
import VariableList from '../../../../organizations/projects/pipelines/create/VariableList';
import ProjectVariableSetAutocomplete, { ProjectVariableSetOption } from '../../variables/ProjectVariableSetAutocomplete';
import DeploymentTemplateList from './DeploymentTemplateList';
import ReleaseLifecycleAutocomplete, { ReleaseLifecycleOption } from './ReleaseLifecycleAutocomplete';
import ParticipantSelectionBox, { Participant, TeamParticipant, UserParticipant } from './ReleaseParticipantSelectionBox';

export interface DeploymentTemplate {
    readonly environmentName: string
    name: string | null
    version: string | null
}

export interface FormData {
    semanticVersion: string
    notes: string
    lifecycleId: string
    variables: Variable[]
    variableSetRevision: string | null
    deploymentTemplates: DeploymentTemplate[]
    userParticipants: UserParticipant[]
    teamParticipants: TeamParticipant[]
    dueDate: Moment | null
    validationErrors: boolean
}

interface Props {
    data: FormData
    projectId: string
    onChange: (data: FormData) => void
    error?: MutationError
    editMode?: boolean
    currentLatestVersion?: string
}


function ReleaseForm({ data, projectId, onChange, error, editMode, currentLatestVersion }: Props) {
    const [variableToAdd, setVariableToAdd] = useState<Variable | null>(null);
    const [environNames, setEnvironNames] = useState<string[]>([]);
    const [showDueDatePicker, setShowDueDatePicker] = useState(data.dueDate !== null);
    const deploymentTemplatesRef = useRef<null | HTMLDivElement>(null)

    const onLifecycleChange = (lifecycle: ReleaseLifecycleOption | null) => {
        if (lifecycle) {
            setEnvironNames(lifecycle.environmentNames)

            const templateRows: DeploymentTemplate[] = lifecycle.environmentNames.map((environ: string) => ({
                environmentName: environ,
                name: null,
                version: null
            }));
            onChange({ ...data, lifecycleId: lifecycle.id, deploymentTemplates: templateRows });
        } else {
            onChange({ ...data, lifecycleId: '', deploymentTemplates: [] })
            setEnvironNames([])
        }
    };

    const onNewVariable = () => {
        setVariableToAdd({
            id: '',
            key: '',
            value: '',
            category: 'HCL',
            environmentName: null
        });
    };

    const updateVariable = (variable: Variable | null, keepOpen: boolean) => {
        if (!variable) {
            setVariableToAdd(null);
            return;
        }

        const updatedVariables = [...data.variables];

        if (variable.id === '') {
            updatedVariables.push({ ...variable, id: nanoid() });
        } else {
            const index = updatedVariables.findIndex((v) => v.id === variable.id);
            updatedVariables[index] = { ...variable, id: variable.id }
        }
        onChange({ ...data, variables: updatedVariables });

        if (keepOpen) {
            setVariableToAdd({
                id: '',
                key: '',
                value: '',
                category: 'HCL',
                environmentName: null
            });
        } else {
            setVariableToAdd(null);
        }
    };

    const onEditVariable = (variable: Variable) => {
        setVariableToAdd(variable)
    };

    const deleteVariable = (variable: Variable) => {
        const index = data.variables.findIndex((v) => ((v.id === variable.id)));

        if (index !== -1) {
            const updatedVariables = [...data.variables];
            updatedVariables.splice(index, 1);
            onChange({ ...data, variables: updatedVariables })
        }
    };

    const onAddParticipant = (participant: Participant) => {
        if (participant.type === 'user') {
            onChange({ ...data, userParticipants: [...data.userParticipants, participant] });
        } else {
            onChange({ ...data, teamParticipants: [...data.teamParticipants, participant] });
        }
    };

    const onRemoveParticipant = (participant: Participant) => {
        if (participant.type === 'user') {
            onChange({ ...data, userParticipants: data.userParticipants.filter((user) => user.id !== participant.id) });
        } else {
            onChange({ ...data, teamParticipants: data.teamParticipants.filter((team) => team.id !== participant.id) });
        }
    };

    const onDueDateSwitchChange = (_: React.SyntheticEvent, checked: boolean) => {
        if (!checked) {
            // Reset the due date data when the switch is toggled to false.
            onDueDateChange(null);
        }
        setShowDueDatePicker(checked);
    };

    const onVariableSetRevisionSelected = (value: ProjectVariableSetOption | null) => {
        onChange({ ...data, variableSetRevision: value?.revision || null })
    };

    const onDueDateChange = (date: Moment | null) => {
        onChange({ ...data, dueDate: date });
    };

    const formattedDueDate = useMemo(() => {
        if (!data.dueDate) return '';
        const zoneName = moment.tz(moment.tz.guess()).zoneName();
        return `${data.dueDate.format('MMMM Do YYYY, h:mm:ss A')} ${zoneName}`;
    }, [data]);

    useEffect(() => {
        let validationErrors = false;
        if (editMode) {
            validationErrors = data.dueDate === null && showDueDatePicker;
        } else {
            validationErrors = data.semanticVersion === '' ||
                data.lifecycleId === '' ||
                (data.dueDate === null && showDueDatePicker) ||
                (data.deploymentTemplates.some(t => !t.version));
        }

        if (data.validationErrors !== validationErrors) {
            onChange({ ...data, validationErrors: validationErrors });
        }

    }, [data, showDueDatePicker, editMode]);

    useEffect(() => {
        if (data.deploymentTemplates.length > 0) {
            deploymentTemplatesRef.current?.scrollIntoView({ behavior: "smooth" })
        }
    }, [data.deploymentTemplates.length]);

    const currentTime = moment();

    return (
        <Box>
            {error && <Alert sx={{ mt: 2, mb: 4 }} severity={error.severity}>
                {error.message}
            </Alert>}
            <Box sx={{ mb: 4 }}>
                <Typography variant="subtitle1" gutterBottom>Details</Typography>
                <Divider sx={{ opacity: 0.6 }} />
                <Box mt={2} display="flex" alignItems="center">
                    <TextField
                        disabled={editMode}
                        sx={{ maxWidth: 250 }}
                        size="small"
                        autoComplete="off"
                        margin="none"
                        label="Semantic Version"
                        value={data.semanticVersion}
                        onChange={event => onChange({ ...data, semanticVersion: event.target.value })}
                    />
                </Box>
                {currentLatestVersion && <Typography mt={1} component="div" variant="caption" color="textSecondary">The current latest release version is {' '}
                    <StyledCode>{currentLatestVersion}</StyledCode>
                </Typography>}
            </Box>
            <Box sx={{ mb: 4 }}>
                <Typography variant="subtitle1" gutterBottom>Does this release have a due date?</Typography>
                <Divider sx={{ opacity: 0.6, mb: 2 }} />
                <FormControlLabel
                    control={<Switch checked={showDueDatePicker} onChange={onDueDateSwitchChange} />}
                    label={showDueDatePicker ? 'Yes' : 'No'}
                />
                {showDueDatePicker && <Box sx={{ mt: 2 }}>
                    <LocalizationProvider dateAdapter={AdapterMoment}>
                        <DateTimePicker
                            sx={{ maxWidth: 250 }}
                            slotProps={{
                                textField: { size: 'small' },
                                actionBar: { actions: ['clear', 'accept'] },
                            }}
                            disablePast={!editMode}
                            value={data.dueDate}
                            onChange={onDueDateChange}
                            closeOnSelect={false}
                        />
                    </LocalizationProvider>
                    <Typography mt={1} component="div" variant="caption" color="textSecondary">
                        {data.dueDate ? `This release ${data.dueDate.isBefore(currentTime) ? 'was' : 'will be'} due on ${formattedDueDate} which ${data.dueDate.isBefore(currentTime) ? 'was' : 'is'} approximately ${data.dueDate.fromNow()}` : 'Select the date when this release will be due'}
                    </Typography>
                </Box>}
            </Box>
            {!editMode && <Box sx={{ mb: 4 }}>
                <Typography variant="subtitle1" gutterBottom>Participants</Typography>
                <Divider sx={{ opacity: 0.6, mb: 2 }} />
                <ParticipantSelectionBox
                    participants={[
                        ...data.userParticipants,
                        ...data.teamParticipants
                    ]}
                    onAdd={onAddParticipant}
                    onRemove={onRemoveParticipant}
                />
            </Box>}
            <Box sx={{ mb: 4 }}>
                <Typography variant="subtitle1" gutterBottom>Release Notes</Typography>
                <Divider sx={{ opacity: 0.6 }} />
                <Box mt={2}>
                    <TextField
                        size="small"
                        minRows={5}
                        multiline
                        margin='none'
                        fullWidth
                        label="Release Notes"
                        value={data.notes}
                        onChange={event => onChange({ ...data, notes: event.target.value })}
                    />
                </Box>
            </Box>
            {!editMode && <Box sx={{ mb: 4 }}>
                <Typography variant="subtitle1" gutterBottom>Release Lifecycle</Typography>
                <Divider sx={{ opacity: 0.6 }} />
                <Box mt={2}>
                    <ReleaseLifecycleAutocomplete
                        projectId={projectId}
                        onSelected={onLifecycleChange} />
                </Box>
            </Box>}
            <Suspense fallback={<Box
                sx={{
                    width: '100%',
                    height: `calc(100vh - 64px)`,
                    display: 'flex',
                    alignItems: 'center',
                    justifyContent: 'center'
                }}
            >
                <CircularProgress />
            </Box>}>
                {data.lifecycleId !== '' && <Box>
                    <Box mb={4} ref={deploymentTemplatesRef}>
                        <Typography variant="subtitle1" gutterBottom>Deployments</Typography>
                        <Divider sx={{ opacity: 0.6 }} />
                        <DeploymentTemplateList
                            onChange={(templates) => onChange({ ...data, deploymentTemplates: templates })}
                            deploymentTemplates={data.deploymentTemplates}
                        />
                    </Box>
                    <Box mb={4}>
                        <Typography variant="subtitle1" gutterBottom>Variable Set Revision</Typography>
                        <Divider sx={{ opacity: 0.6, mb: 2 }} />
                        <Box display="flex" alignItems="center">
                            <ProjectVariableSetAutocomplete
                                projectId={projectId}
                                revision={data.variableSetRevision}
                                onSelected={onVariableSetRevisionSelected}
                            />
                            {data.variableSetRevision && <Button
                                sx={{ ml: 1, minWidth: 120 }}
                                component={RouterLink}
                                color="inherit"
                                target='_blank'
                                rel='noopener noreferrer'
                                to={`../../-/variables?revision=${data.variableSetRevision}`}>
                                View Revision
                            </Button>}
                        </Box>
                    </Box>
                    <Box>
                        <Typography variant="subtitle1" gutterBottom>Release Variables</Typography>
                        <Divider sx={{ opacity: 0.6 }} />
                        <Stack sx={{ mt: 2, mb: 2 }}>
                            {data.variables.length === 0 ?
                                <Paper
                                    sx={{ display: 'flex', justifyContent: 'center', mb: 6 }}
                                    variant="outlined"
                                >
                                    <Box p={4} display="flex" flexDirection="column" justifyContent="center" alignItems="center">
                                        {<Typography color="textSecondary" align="center" sx={{ mb: 2 }}>
                                            Add variables which will be automatically set when executing pipelines
                                        </Typography>}
                                        <Button variant="outlined" color="primary" onClick={onNewVariable}>Add Variable</Button>
                                    </Box>
                                </Paper>
                                : <Box>
                                    <Paper>
                                        <Box p={2} display="flex" alignItems="center" justifyContent="space-between">
                                            <Typography
                                                variant="subtitle1">
                                                {data.variables.length} variable{data.variables.length === 1 ? '' : 's'}
                                            </Typography>
                                            <Button
                                                size="small"
                                                variant="outlined"
                                                color="secondary"
                                                onClick={onNewVariable}>
                                                Add Variable
                                            </Button>
                                        </Box>
                                    </Paper>
                                    <VariableList
                                        showEnvironment
                                        variables={data.variables}
                                        onDeleteVariable={deleteVariable}
                                        onEditVariable={onEditVariable}
                                    />
                                </Box>
                            }
                        </Stack>
                    </Box>
                </Box>}
            </Suspense>
            {variableToAdd && <VariableDialog
                showEnvironment
                environmentNames={['All Environments', ...environNames]}
                variable={variableToAdd}
                onClose={updateVariable}
            />}
        </Box>
    );
}

export default ReleaseForm
