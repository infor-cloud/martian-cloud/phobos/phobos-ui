import LoadingButton from '@mui/lab/LoadingButton';
import { Box, Button, Divider, Grid, Typography } from '@mui/material';
import graphql from 'babel-plugin-relay/macro';
import { useContext, useState } from 'react';
import { useFragment, useLazyLoadQuery, useMutation } from 'react-relay/hooks';
import { Link as RouterLink, useNavigate, useOutletContext } from 'react-router-dom';
import { UserContext } from '../../../../UserContext';
import { MutationError } from '../../../../common/error';
import ProjectBreadcrumbs from '../../ProjectBreadcrumbs';
import ReleaseForm, { DeploymentTemplate, FormData } from './ReleaseForm';
import ReleaseFormSidebar from './ReleaseFormSidebar';
import { Participant } from './ReleaseParticipantSelectionBox';
import { NewReleaseFragment_releases$key } from './__generated__/NewReleaseFragment_releases.graphql';
import { NewReleaseMutation } from './__generated__/NewReleaseMutation.graphql';
import { NewReleaseQuery } from './__generated__/NewReleaseQuery.graphql';

function NewRelease() {
    const navigate = useNavigate();
    const context = useOutletContext<NewReleaseFragment_releases$key>();
    const user = useContext(UserContext);

    const project = useFragment<NewReleaseFragment_releases$key>(
        graphql`
            fragment NewReleaseFragment_releases on Project
            {
                id
                name
                organizationName
                metadata {
                    prn
                }
            }
        `, context);

    const latestReleaseQuery = useLazyLoadQuery<NewReleaseQuery>(graphql`
        query NewReleaseQuery($projectId: String!) {
            node(id: $projectId){
                ...on Project {
                    releases(first: 1, latest: true) {
                        edges {
                            node {
                                semanticVersion
                            }
                        }
                    }
                }
            }
        }`, { projectId: project.id }, { fetchPolicy: 'store-and-network' });

    const [error, setError] = useState<MutationError>()
    const [formData, setFormData] = useState<FormData>({
        semanticVersion: '',
        notes: '',
        lifecycleId: '',
        variables: [],
        variableSetRevision: null,
        deploymentTemplates: [],
        userParticipants: [{ id: user.id, type: 'user', email: user.email, username: user.username }],
        teamParticipants: [],
        dueDate: null,
        validationErrors: false
    });

    const [commit, isInFlight] = useMutation<NewReleaseMutation>(graphql`
        mutation NewReleaseMutation($input: CreateReleaseInput!) {
            createRelease(input: $input) {
                release {
                    id
                    ...ReleaseCardItemFragment_release
                }
                problems {
                    message
                    field
                    type
                }
            }
        }
    `);

    const onSave = () => {
        commit({
            variables: {
                input: {
                    projectId: project.id,
                    semanticVersion: formData.semanticVersion,
                    lifecycleId: formData.lifecycleId,
                    deploymentTemplates: formData.deploymentTemplates.map((template: DeploymentTemplate) => ({
                        environmentName: template.environmentName,
                        pipelineTemplateId: `prn:pipeline_template:${project.organizationName}/${project.name}/${template.name}/${template.version}`
                    })),
                    notes: formData.notes,
                    variables: formData.variables,
                    variableSetRevision: formData.variableSetRevision,
                    dueDate: formData.dueDate,
                    userParticipants: formData.userParticipants.map((user: Participant) => user.id),
                    teamParticipants: formData.teamParticipants.map((team: Participant) => team.id)
                }
            },
            onCompleted: (data) => {
                if (data.createRelease.problems.length) {
                    setError({
                        severity: 'warning',
                        message: data.createRelease.problems.map((problem: any) => problem.message).join('; ')
                    });
                } else if (!data.createRelease.release) {
                    setError({
                        severity: 'error',
                        message: "Unexpected error occurred"
                    });
                }
                else {
                    navigate(`../${data.createRelease.release.id}`);
                }
            },
            onError: error => {
                setError({
                    severity: 'error',
                    message: `Unexpected error occurred: ${error.message}`
                });
            }
        });
    };

    const disabled = formData.validationErrors;

    return (
        <Grid container spacing={4}>
            <Grid item xs={12} md={8} sx={{ order: 0 }}>
                <Box>
                    <ProjectBreadcrumbs
                        prn={project.metadata.prn}
                        childRoutes={[
                            { title: "releases", path: 'releases' },
                            { title: "new", path: 'new' }
                        ]}
                    />
                    <Typography variant="h5" mb={2}>New Release</Typography>
                    <ReleaseForm
                        projectId={project.id}
                        error={error}
                        data={formData}
                        onChange={(data: FormData) => setFormData(data)}
                        currentLatestVersion={latestReleaseQuery.node?.releases?.edges?.[0]?.node?.semanticVersion}
                    />
                    <Divider />
                    <Box mt={2}>
                        <LoadingButton
                            sx={{ mr: 2 }}
                            loading={isInFlight}
                            disabled={disabled}
                            variant="outlined"
                            color="primary"
                            onClick={onSave}
                        >
                            Create Release
                        </LoadingButton>
                        <Button component={RouterLink} color="inherit" to={-1 as any}>Cancel</Button>
                    </Box>
                </Box>
            </Grid>
            <Grid item xs={12} md={4} sx={{ order: 1 }}>
                <ReleaseFormSidebar showMoreInfo={!!formData.lifecycleId} />
            </Grid>
        </Grid>
    );
}

export default NewRelease;
