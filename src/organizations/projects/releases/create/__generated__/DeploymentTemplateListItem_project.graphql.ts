/**
 * @generated SignedSource<<e81917b60dfcec655e029d947891bbf1>>
 * @lightSyntaxTransform
 * @nogrep
 */

/* tslint:disable */
/* eslint-disable */
// @ts-nocheck

import { Fragment, ReaderFragment } from 'relay-runtime';
import { FragmentRefs } from "relay-runtime";
export type DeploymentTemplateListItem_project$data = {
  readonly id: string;
  readonly " $fragmentType": "DeploymentTemplateListItem_project";
};
export type DeploymentTemplateListItem_project$key = {
  readonly " $data"?: DeploymentTemplateListItem_project$data;
  readonly " $fragmentSpreads": FragmentRefs<"DeploymentTemplateListItem_project">;
};

const node: ReaderFragment = {
  "argumentDefinitions": [],
  "kind": "Fragment",
  "metadata": null,
  "name": "DeploymentTemplateListItem_project",
  "selections": [
    {
      "alias": null,
      "args": null,
      "kind": "ScalarField",
      "name": "id",
      "storageKey": null
    }
  ],
  "type": "Project",
  "abstractKey": null
};

(node as any).hash = "6e55ded29c793da2553eea5e9d814468";

export default node;
