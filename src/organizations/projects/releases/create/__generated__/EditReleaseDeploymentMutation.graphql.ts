/**
 * @generated SignedSource<<3a760b3032c8689f264beae0883ed378>>
 * @lightSyntaxTransform
 * @nogrep
 */

/* tslint:disable */
/* eslint-disable */
// @ts-nocheck

import { ConcreteRequest, Mutation } from 'relay-runtime';
export type ProblemType = "BAD_REQUEST" | "CONFLICT" | "FORBIDDEN" | "NOT_FOUND" | "%future added value";
export type VariableCategory = "ENVIRONMENT" | "HCL" | "%future added value";
export type UpdateReleaseDeploymentPipelineInput = {
  clientMutationId?: string | null | undefined;
  environmentName: string;
  pipelineTemplateId?: string | null | undefined;
  releaseId: string;
  variables?: ReadonlyArray<PipelineVariableInput> | null | undefined;
};
export type PipelineVariableInput = {
  category: VariableCategory;
  key: string;
  value: string;
};
export type EditReleaseDeploymentMutation$variables = {
  input: UpdateReleaseDeploymentPipelineInput;
};
export type EditReleaseDeploymentMutation$data = {
  readonly updateReleaseDeploymentPipeline: {
    readonly problems: ReadonlyArray<{
      readonly field: ReadonlyArray<string> | null | undefined;
      readonly message: string;
      readonly type: ProblemType;
    }>;
  };
};
export type EditReleaseDeploymentMutation = {
  response: EditReleaseDeploymentMutation$data;
  variables: EditReleaseDeploymentMutation$variables;
};

const node: ConcreteRequest = (function(){
var v0 = [
  {
    "defaultValue": null,
    "kind": "LocalArgument",
    "name": "input"
  }
],
v1 = [
  {
    "alias": null,
    "args": [
      {
        "kind": "Variable",
        "name": "input",
        "variableName": "input"
      }
    ],
    "concreteType": "UpdateReleaseDeploymentPipelinePayload",
    "kind": "LinkedField",
    "name": "updateReleaseDeploymentPipeline",
    "plural": false,
    "selections": [
      {
        "alias": null,
        "args": null,
        "concreteType": "Problem",
        "kind": "LinkedField",
        "name": "problems",
        "plural": true,
        "selections": [
          {
            "alias": null,
            "args": null,
            "kind": "ScalarField",
            "name": "message",
            "storageKey": null
          },
          {
            "alias": null,
            "args": null,
            "kind": "ScalarField",
            "name": "field",
            "storageKey": null
          },
          {
            "alias": null,
            "args": null,
            "kind": "ScalarField",
            "name": "type",
            "storageKey": null
          }
        ],
        "storageKey": null
      }
    ],
    "storageKey": null
  }
];
return {
  "fragment": {
    "argumentDefinitions": (v0/*: any*/),
    "kind": "Fragment",
    "metadata": null,
    "name": "EditReleaseDeploymentMutation",
    "selections": (v1/*: any*/),
    "type": "Mutation",
    "abstractKey": null
  },
  "kind": "Request",
  "operation": {
    "argumentDefinitions": (v0/*: any*/),
    "kind": "Operation",
    "name": "EditReleaseDeploymentMutation",
    "selections": (v1/*: any*/)
  },
  "params": {
    "cacheID": "d69221109e66a9073323906a78bd673d",
    "id": null,
    "metadata": {},
    "name": "EditReleaseDeploymentMutation",
    "operationKind": "mutation",
    "text": "mutation EditReleaseDeploymentMutation(\n  $input: UpdateReleaseDeploymentPipelineInput!\n) {\n  updateReleaseDeploymentPipeline(input: $input) {\n    problems {\n      message\n      field\n      type\n    }\n  }\n}\n"
  }
};
})();

(node as any).hash = "081809826f348af2004fc8aa89dc2ad6";

export default node;
