/**
 * @generated SignedSource<<c1c2797f035cbb28f13578b913eb020e>>
 * @lightSyntaxTransform
 * @nogrep
 */

/* tslint:disable */
/* eslint-disable */
// @ts-nocheck

import { ConcreteRequest, Query } from 'relay-runtime';
export type VariableCategory = "ENVIRONMENT" | "HCL" | "%future added value";
export type EditReleaseDeploymentQuery$variables = {
  id: string;
};
export type EditReleaseDeploymentQuery$data = {
  readonly node: {
    readonly id?: string;
    readonly notes?: string;
    readonly pipeline?: {
      readonly stages: ReadonlyArray<{
        readonly nestedPipelines: ReadonlyArray<{
          readonly environmentName: string | null | undefined;
          readonly latestPipeline: {
            readonly pipelineTemplate: {
              readonly id: string;
              readonly name: string | null | undefined;
              readonly semanticVersion: string | null | undefined;
              readonly versioned: boolean;
            };
            readonly variables: ReadonlyArray<{
              readonly category: VariableCategory;
              readonly key: string;
              readonly value: string;
            }>;
          };
        }>;
      }>;
    };
    readonly semanticVersion?: string;
  } | null | undefined;
};
export type EditReleaseDeploymentQuery = {
  response: EditReleaseDeploymentQuery$data;
  variables: EditReleaseDeploymentQuery$variables;
};

const node: ConcreteRequest = (function(){
var v0 = [
  {
    "defaultValue": null,
    "kind": "LocalArgument",
    "name": "id"
  }
],
v1 = [
  {
    "kind": "Variable",
    "name": "id",
    "variableName": "id"
  }
],
v2 = {
  "alias": null,
  "args": null,
  "kind": "ScalarField",
  "name": "id",
  "storageKey": null
},
v3 = {
  "alias": null,
  "args": null,
  "kind": "ScalarField",
  "name": "semanticVersion",
  "storageKey": null
},
v4 = {
  "alias": null,
  "args": null,
  "kind": "ScalarField",
  "name": "notes",
  "storageKey": null
},
v5 = {
  "alias": null,
  "args": null,
  "kind": "ScalarField",
  "name": "environmentName",
  "storageKey": null
},
v6 = {
  "alias": null,
  "args": null,
  "concreteType": "PipelineTemplate",
  "kind": "LinkedField",
  "name": "pipelineTemplate",
  "plural": false,
  "selections": [
    (v2/*: any*/),
    {
      "alias": null,
      "args": null,
      "kind": "ScalarField",
      "name": "versioned",
      "storageKey": null
    },
    {
      "alias": null,
      "args": null,
      "kind": "ScalarField",
      "name": "name",
      "storageKey": null
    },
    (v3/*: any*/)
  ],
  "storageKey": null
},
v7 = {
  "alias": null,
  "args": null,
  "concreteType": "PipelineVariable",
  "kind": "LinkedField",
  "name": "variables",
  "plural": true,
  "selections": [
    {
      "alias": null,
      "args": null,
      "kind": "ScalarField",
      "name": "category",
      "storageKey": null
    },
    {
      "alias": null,
      "args": null,
      "kind": "ScalarField",
      "name": "key",
      "storageKey": null
    },
    {
      "alias": null,
      "args": null,
      "kind": "ScalarField",
      "name": "value",
      "storageKey": null
    }
  ],
  "storageKey": null
};
return {
  "fragment": {
    "argumentDefinitions": (v0/*: any*/),
    "kind": "Fragment",
    "metadata": null,
    "name": "EditReleaseDeploymentQuery",
    "selections": [
      {
        "alias": null,
        "args": (v1/*: any*/),
        "concreteType": null,
        "kind": "LinkedField",
        "name": "node",
        "plural": false,
        "selections": [
          {
            "kind": "InlineFragment",
            "selections": [
              (v2/*: any*/),
              (v3/*: any*/),
              (v4/*: any*/),
              {
                "alias": null,
                "args": null,
                "concreteType": "Pipeline",
                "kind": "LinkedField",
                "name": "pipeline",
                "plural": false,
                "selections": [
                  {
                    "alias": null,
                    "args": null,
                    "concreteType": "PipelineStage",
                    "kind": "LinkedField",
                    "name": "stages",
                    "plural": true,
                    "selections": [
                      {
                        "alias": null,
                        "args": null,
                        "concreteType": "NestedPipeline",
                        "kind": "LinkedField",
                        "name": "nestedPipelines",
                        "plural": true,
                        "selections": [
                          (v5/*: any*/),
                          {
                            "alias": null,
                            "args": null,
                            "concreteType": "Pipeline",
                            "kind": "LinkedField",
                            "name": "latestPipeline",
                            "plural": false,
                            "selections": [
                              (v6/*: any*/),
                              (v7/*: any*/)
                            ],
                            "storageKey": null
                          }
                        ],
                        "storageKey": null
                      }
                    ],
                    "storageKey": null
                  }
                ],
                "storageKey": null
              }
            ],
            "type": "Release",
            "abstractKey": null
          }
        ],
        "storageKey": null
      }
    ],
    "type": "Query",
    "abstractKey": null
  },
  "kind": "Request",
  "operation": {
    "argumentDefinitions": (v0/*: any*/),
    "kind": "Operation",
    "name": "EditReleaseDeploymentQuery",
    "selections": [
      {
        "alias": null,
        "args": (v1/*: any*/),
        "concreteType": null,
        "kind": "LinkedField",
        "name": "node",
        "plural": false,
        "selections": [
          {
            "alias": null,
            "args": null,
            "kind": "ScalarField",
            "name": "__typename",
            "storageKey": null
          },
          (v2/*: any*/),
          {
            "kind": "InlineFragment",
            "selections": [
              (v3/*: any*/),
              (v4/*: any*/),
              {
                "alias": null,
                "args": null,
                "concreteType": "Pipeline",
                "kind": "LinkedField",
                "name": "pipeline",
                "plural": false,
                "selections": [
                  {
                    "alias": null,
                    "args": null,
                    "concreteType": "PipelineStage",
                    "kind": "LinkedField",
                    "name": "stages",
                    "plural": true,
                    "selections": [
                      {
                        "alias": null,
                        "args": null,
                        "concreteType": "NestedPipeline",
                        "kind": "LinkedField",
                        "name": "nestedPipelines",
                        "plural": true,
                        "selections": [
                          (v5/*: any*/),
                          {
                            "alias": null,
                            "args": null,
                            "concreteType": "Pipeline",
                            "kind": "LinkedField",
                            "name": "latestPipeline",
                            "plural": false,
                            "selections": [
                              (v6/*: any*/),
                              (v7/*: any*/),
                              (v2/*: any*/)
                            ],
                            "storageKey": null
                          }
                        ],
                        "storageKey": null
                      }
                    ],
                    "storageKey": null
                  },
                  (v2/*: any*/)
                ],
                "storageKey": null
              }
            ],
            "type": "Release",
            "abstractKey": null
          }
        ],
        "storageKey": null
      }
    ]
  },
  "params": {
    "cacheID": "3939d5868c670002f2d20527a6cdf12c",
    "id": null,
    "metadata": {},
    "name": "EditReleaseDeploymentQuery",
    "operationKind": "query",
    "text": "query EditReleaseDeploymentQuery(\n  $id: String!\n) {\n  node(id: $id) {\n    __typename\n    ... on Release {\n      id\n      semanticVersion\n      notes\n      pipeline {\n        stages {\n          nestedPipelines {\n            environmentName\n            latestPipeline {\n              pipelineTemplate {\n                id\n                versioned\n                name\n                semanticVersion\n              }\n              variables {\n                category\n                key\n                value\n              }\n              id\n            }\n          }\n        }\n        id\n      }\n    }\n    id\n  }\n}\n"
  }
};
})();

(node as any).hash = "5c430fe97612432e0f67c8744da8ed98";

export default node;
