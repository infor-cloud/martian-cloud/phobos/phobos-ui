import { Box, Chip, Typography, useTheme } from '@mui/material';
import Autocomplete from '@mui/material/Autocomplete';
import CircularProgress from '@mui/material/CircularProgress';
import TextField from '@mui/material/TextField';
import match from 'autosuggest-highlight/match';
import parse from 'autosuggest-highlight/parse';
import graphql from 'babel-plugin-relay/macro';
import throttle from 'lodash.throttle';
import React, { useEffect, useState } from 'react';
import { fetchQuery, useRelayEnvironment } from 'react-relay/hooks';
import { ReleaseLifecycleAutocompleteQuery } from './__generated__/ReleaseLifecycleAutocompleteQuery.graphql';

export interface ReleaseLifecycleOption {
    readonly id: string
    readonly name: string
    readonly scope: string
    readonly environmentNames: string[]
}

interface Props {
    projectId: string
    onSelected: (value: ReleaseLifecycleOption | null) => void
}

function ReleaseLifecycleAutocomplete({ projectId, onSelected }: Props) {
    const [options, setOptions] = useState<ReadonlyArray<ReleaseLifecycleOption> | null>(null);
    const [loading, setLoading] = useState<boolean>(false);
    const [inputValue, setInputValue] = useState('');
    const theme = useTheme();

    const environment = useRelayEnvironment();

    const fetch = React.useMemo(
        () =>
            throttle(
                (
                    request: { input: string },
                    callback: (results?: readonly ReleaseLifecycleOption[]) => void,
                ) => {
                    fetchQuery<ReleaseLifecycleAutocompleteQuery>(
                        environment,
                        graphql`
                            query ReleaseLifecycleAutocompleteQuery($id: String!, $search: String) {
                                node(id: $id) {
                                    ... on Project {
                                        releaseLifecycles(first: 50, search: $search, scopes: [PROJECT, ORGANIZATION]) {
                                            edges {
                                                node {
                                                    id
                                                    name
                                                    scope
                                                    environmentNames
                                                }
                                            }
                                        }
                                    }
                                }
                            }
                        `,
                        { id: projectId, search: request.input },
                        { fetchPolicy: 'network-only' }
                    ).toPromise().then(async response => {
                        const options = response?.node?.releaseLifecycles?.edges?.map(edge => edge?.node as ReleaseLifecycleOption);
                        callback(options);
                    });
                },
                300,
            ),
        [environment, projectId]
    );

    useEffect(() => {
        let active = true;

        setLoading(true);

        fetch( { input: inputValue }, (results?: readonly ReleaseLifecycleOption[]) => {
            if (active) {
                setOptions(results ?? []);
                setLoading(false);
            }
        });

        return () => {
            active = false;
        };
    }, [fetch, inputValue]);

    return (
        <Autocomplete
            fullWidth
            size="small"
            onChange={(event: React.SyntheticEvent, value: ReleaseLifecycleOption | null) => onSelected(value)}
            onInputChange={(_, newInputValue: string) => setInputValue(newInputValue)}
            isOptionEqualToValue={(option: ReleaseLifecycleOption, value: ReleaseLifecycleOption) => option.id === value.id}
            getOptionLabel={(option: ReleaseLifecycleOption) => option.name}
            renderOption={(props: React.HTMLAttributes<HTMLLIElement>, option: ReleaseLifecycleOption, { inputValue }) => {
                const matches = match(option.name, inputValue);
                const parts = parse(option.name, matches);
                return (
                    <Box component="li" {...props}>
                        <Box>
                            <Typography component="div">
                                {parts.map((part: any, index: number) => (
                                    <span
                                        key={index}
                                        style={{
                                            fontWeight: part.highlight ? 700 : 400,
                                        }}
                                    >
                                        {part.text}
                                    </span>
                                ))}
                                {option.scope === 'ORGANIZATION' && <Chip
                                    sx={{ ml: 1, color: theme.palette.text.secondary }}
                                    size="xs"
                                    label='inherited from org'
                                />}
                            </Typography>
                        </Box>
                    </Box>
                );
            }}
            options={options ?? []}
            loading={loading}
            renderInput={(params) => (
                <TextField
                    {...params}
                    placeholder='Select a release lifecycle'
                    InputProps={{
                        ...params.InputProps,
                        endAdornment: (
                            <React.Fragment>
                                {loading ? <CircularProgress color="inherit" size={20} /> : null}
                                {params.InputProps.endAdornment}
                            </React.Fragment>
                        ),
                    }}
                />
            )}
        />
    );
}

export default ReleaseLifecycleAutocomplete
