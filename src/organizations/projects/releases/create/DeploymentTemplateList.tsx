import { Box, Checkbox, FormControlLabel, Table, TableBody, TableCell, TableContainer, TableHead, TableRow, Typography } from '@mui/material';
import { useState } from 'react';
import DeploymentTemplateListItem from './DeploymentTemplateListItem';
import { DeploymentTemplate } from './ReleaseForm';

interface Props {
    deploymentTemplates: DeploymentTemplate[]
    onChange: (templates: DeploymentTemplate[]) => void
}

function DeploymentTemplateList({ deploymentTemplates, onChange }: Props) {
    const [individualEnvSelection, setIndividualEnvSelection] = useState<boolean>(false);

    const updateTemplate = (template: DeploymentTemplate) => {
        const index = deploymentTemplates.findIndex((item: DeploymentTemplate) =>
            item.environmentName === template.environmentName);

        if (index !== -1) {
            const updatedTemplates = [...deploymentTemplates];
            updatedTemplates[index] = { ...template };
            onChange(updatedTemplates);
        }
    };

    const updateAllTemplates = (template: DeploymentTemplate) => {
        // Update all templates based on the provided template
        const updatedTemplates = deploymentTemplates.map((item) => ({ ...template, environmentName: item.environmentName }));
        onChange(updatedTemplates);
    };

    const onIndividualEnvSelectionChange = (event: React.ChangeEvent<HTMLInputElement>) => {
        const selectIndividualEnv = event.target.checked;
        if (!selectIndividualEnv) {
            // Reset all templates
            const updatedTemplates = deploymentTemplates.map((item) => ({ name: null, version: null, environmentName: item.environmentName }));
            onChange(updatedTemplates);
        }
        setIndividualEnvSelection(selectIndividualEnv);
    };

    return (
        <Box>
            <TableContainer>
                <Table sx={{ tableLayout: 'fixed' }}>
                    <TableHead>
                        <TableRow>
                            <TableCell>
                                <Typography color="textSecondary">Environment</Typography>
                            </TableCell>
                            <TableCell>
                                <Typography color="textSecondary">Pipeline Template</Typography>
                            </TableCell>
                            <TableCell>
                                <Typography color="textSecondary">Version</Typography>
                            </TableCell>
                        </TableRow>
                    </TableHead>
                    <TableBody>
                        {individualEnvSelection && deploymentTemplates.map((template: DeploymentTemplate) => <DeploymentTemplateListItem
                            key={template.environmentName}
                            template={template}
                            onChange={updateTemplate}
                        />)}
                        {!individualEnvSelection && <DeploymentTemplateListItem
                            template={{ environmentName: 'All Environments', name: deploymentTemplates[0].name, version: deploymentTemplates[0].version }}
                            onChange={updateAllTemplates}
                        />}
                    </TableBody>
                </Table>
            </TableContainer>
            <FormControlLabel
                control={<Checkbox
                    color="secondary"
                    checked={individualEnvSelection}
                    onChange={onIndividualEnvSelectionChange}
                />}
                label={<Typography variant="body2" color="textSecondary">Set deployment template per environment</Typography>}
            />
        </Box>
    );
}

export default DeploymentTemplateList
