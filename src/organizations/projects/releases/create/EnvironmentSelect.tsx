import { Box, MenuItem, Select, Stack, Typography } from '@mui/material';

interface Props {
    data: string
    environmentNames: string[]
    disabled?: boolean
    onChange: (selectedEnvironment: string) => void
}

function EnvironmentSelect({ environmentNames, data, disabled, onChange }: Props) {

    return (
        <Box sx={{ mb: 2 }}>
            <Typography variant="subtitle1" gutterBottom>Environment</Typography>
            <Stack>
                <Select
                    disabled={disabled}
                    sx={{ width: 200, mb: 1 }}
                    size="small"
                    labelId="Environment"
                    id="environment-select"
                    value={data}
                    onChange={event => onChange(event.target.value)}
                >
                    {environmentNames.map((name: string) =>
                        <MenuItem
                            key={name}
                            value={name}>
                            {name}
                        </MenuItem>)}
                </Select>
                <Typography variant="caption">Choose one of the environments listed in the selected lifecycle.
                    If none of the environments from the lifecycle are chosen, the variable will be scoped to All Environments.
                </Typography>
            </Stack>
        </Box>
    );
}

export default EnvironmentSelect
