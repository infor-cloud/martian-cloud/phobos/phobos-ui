import { Alert, Box, Button, Divider, Paper, Stack, Typography } from '@mui/material';
import { nanoid } from 'nanoid';
import { useState } from 'react';
import { MutationError } from '../../../../common/error';
import PipelineTemplateAutocomplete, { Option as PipelineTemplateOption } from './PipelineTemplateAutocomplete';
import PipelineTemplateVersionAutocomplete from './PipelineTemplateVersionAutocomplete';
import VariableDialog, { Variable } from '../../../../organizations/projects/pipelines/create/VariableDialog';
import VariableList from '../../../../organizations/projects/pipelines/create/VariableList';

export interface FormData {
    variables: Variable[]
    pipelineTemplateName: string | null
    pipelineTemplateVersion: string | null
}

interface Props {
    data: FormData
    environmentName: string
    semanticVersion: string
    projectId: string
    onChange: (data: FormData) => void
    error?: MutationError
}

function ReleaseDeploymentForm({ data, projectId, environmentName, semanticVersion, onChange, error }: Props) {
    const [variableToAdd, setVariableToAdd] = useState<Variable | null>(null);

    const onNewVariable = () => {
        setVariableToAdd({
            id: '',
            key: '',
            value: '',
            category: 'HCL',
            environmentName: environmentName
        });
    };

    const updateVariable = (variable: Variable | null) => {
        if (!variable) {
            setVariableToAdd(null);
            return;
        }

        const updatedVariables = [...data.variables];

        if (variable.id === '') {
            updatedVariables.push({ ...variable, id: nanoid() });
        } else {
            const index = updatedVariables.findIndex((v) => v.id === variable.id);
            updatedVariables[index] = { ...variable, id: variable.id }
        }
        onChange({ ...data, variables: updatedVariables });
        setVariableToAdd(null);
    };

    const onEditVariable = (variable: Variable) => {
        setVariableToAdd(variable)
    };

    const deleteVariable = (variable: Variable) => {
        const index = data.variables.findIndex((v) => ((v.id === variable.id)));

        if (index !== -1) {
            const updatedVariables = [...data.variables];
            updatedVariables.splice(index, 1);
            onChange({ ...data, variables: updatedVariables })
        }
    };

    const onPipelineTemplateVersionChange = (version: string | null) => {
        onChange({ ...data, pipelineTemplateVersion: version });
    };

    const onPipelineTemplateNameChange = (option: PipelineTemplateOption | null) => {
        onChange({ ...data, pipelineTemplateName: option?.name || null, pipelineTemplateVersion: option?.semanticVersion || null });
    };

    return (
        <Box>

            {error && <Alert sx={{ mt: 2 }} severity={error.severity}>
                {error.message}
            </Alert>}
            <Typography variant="h5" gutterBottom>Edit Release Deployment</Typography>
            <Typography variant="subtitle1" color="textSecondary" marginBottom={3}>
                Updating a release deployment will create a new deployment pipeline with the updated pipeline template and variables.
            </Typography>
            <Box marginBottom={3}>
                <Typography fontWeight={500} variant="subtitle1" gutterBottom>Release Version</Typography>
                <Divider sx={{ mb: 1, opacity: 0.6 }} />
                <Typography>{semanticVersion}</Typography>
            </Box>
            <Box marginBottom={3}>
                <Typography fontWeight={500} variant="subtitle1" gutterBottom>Environment</Typography>
                <Divider sx={{ mb: 1, opacity: 0.6 }} />
                <Typography>{environmentName}</Typography>
            </Box>
            <Box marginBottom={3}>
                <Typography fontWeight={500} variant="subtitle1" gutterBottom>Pipeline Template</Typography>
                <Divider sx={{ mb: 1, opacity: 0.6 }} />
                <Box display="flex">
                    <PipelineTemplateAutocomplete
                        pipelineTemplateName={data.pipelineTemplateName}
                        projectId={projectId}
                        onSelected={onPipelineTemplateNameChange}
                    />
                    <Box width={200} marginLeft={2}>
                        <PipelineTemplateVersionAutocomplete
                            projectId={projectId}
                            name={data.pipelineTemplateName}
                            version={data.pipelineTemplateVersion}
                            onSelected={onPipelineTemplateVersionChange}
                        />
                    </Box>
                </Box>
            </Box>
            <Box marginBottom={3}>
                <Typography fontWeight={500} variant="subtitle1" gutterBottom>Variables</Typography>
                <Divider sx={{ opacity: 0.6 }} />
                <Stack sx={{ mt: 2, mb: 2 }}>
                    {data.variables.length === 0 ?
                        <Paper
                            sx={{ display: 'flex', justifyContent: 'center', mb: 6 }}
                            variant="outlined"
                        >
                            <Box p={4} display="flex" flexDirection="column" justifyContent="center" alignItems="center">
                                {<Typography color="textSecondary" align="center" sx={{ mb: 2 }}>
                                    Add variables which will be automatically set when executing pipelines
                                </Typography>}
                                <Button variant="outlined" color="primary" onClick={onNewVariable}>Add Variable</Button>
                            </Box>
                        </Paper>
                        : <Box>
                            <Paper>
                                <Box p={2} display="flex" alignItems="center" justifyContent="space-between">
                                    <Typography
                                        variant="subtitle1">
                                        {data.variables.length} variable{data.variables.length === 1 ? '' : 's'}
                                    </Typography>
                                    <Button
                                        size="small"
                                        variant="outlined"
                                        color="secondary"
                                        onClick={onNewVariable}>
                                        Add Variable
                                    </Button>
                                </Box>
                            </Paper>
                            <VariableList
                                variables={data.variables}
                                onDeleteVariable={deleteVariable}
                                onEditVariable={onEditVariable}
                            />
                        </Box>
                    }
                </Stack>
            </Box>
            {variableToAdd && <VariableDialog
                disableEnvironmentSelection
                environmentNames={[environmentName]}
                variable={variableToAdd}
                onClose={updateVariable}
            />}
        </Box>
    );
}

export default ReleaseDeploymentForm
