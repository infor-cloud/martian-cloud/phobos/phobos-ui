import graphql from 'babel-plugin-relay/macro';
import { enqueueSnackbar } from 'notistack';
import { useState } from 'react';
import { useFragment, useMutation } from 'react-relay/hooks';
import { RecordSourceProxy } from 'relay-runtime';
import CommentDetails from '../../comments/CommentDetails';
import { ReleaseCommentDetailsFragment_comment$key } from './__generated__/ReleaseCommentDetailsFragment_comment.graphql';
import { ReleaseCommentDetails_DeleteMutation } from './__generated__/ReleaseCommentDetails_DeleteMutation.graphql';
import { ReleaseCommentDetails_EditMutation } from './__generated__/ReleaseCommentDetails_EditMutation.graphql';

interface Props {
    fragmentRef: ReleaseCommentDetailsFragment_comment$key
    showCollapse?: boolean
    onToggleReplyInput?: () => void
}

function ReleaseCommentDetails({ fragmentRef, onToggleReplyInput, showCollapse }: Props) {
    const [showEditForm, setShowEditForm] = useState<boolean>(false);
    const [showDeleteDialog, setShowDeleteDialog] = useState<boolean>(false);

    const data = useFragment<ReleaseCommentDetailsFragment_comment$key>(
        graphql`
            fragment ReleaseCommentDetailsFragment_comment on Comment {
                id
                thread {
                    id
                }
                ...CommentDetailsFragment_comment
            }
        `, fragmentRef
    );

    const [commitEdit, editIsInFlight] = useMutation<ReleaseCommentDetails_EditMutation>(graphql`
        mutation ReleaseCommentDetails_EditMutation($input: UpdateReleaseCommentInput!) {
            updateReleaseComment(input: $input) {
                comment {
                    id
                    thread {
                       ...ReleaseThreadListItemFragment_thread
                    }
                }
                problems {
                    message
                    field
                    type
                }
            }
        }
    `);

    const onEdit = (text: string) => {
        commitEdit({
            variables: {
                input: {
                    id: data.id,
                    text: text
                }
            },
            onCompleted: (data) => {
                if (data.updateReleaseComment.problems.length) {
                    enqueueSnackbar(data.updateReleaseComment.problems.map((problem: { message: any; }) =>
                        problem.message).join(', '), { variant: 'warning' });
                } else if (!data.updateReleaseComment.comment) {
                    enqueueSnackbar("Unexpected error occurred", { variant: 'error' });
                } else {
                    setShowEditForm(false);
                }
            },
            onError: (error) => {
                enqueueSnackbar(error.message, { variant: 'error' });
            }
        });
    };

    const [commitDelete, deleteIsInFlight] = useMutation<ReleaseCommentDetails_DeleteMutation>(graphql`
        mutation ReleaseCommentDetails_DeleteMutation($input: DeleteReleaseCommentInput!) {
            deleteReleaseComment(input: $input) {
                problems {
                    message
                    field
                    type
                }
            }
        }
    `);

    const onDelete = (commentId: string) => {
        commitDelete({
            variables: {
                input: {
                    id: commentId
                }
            },
            onCompleted: (data) => {
                if (data.deleteReleaseComment.problems.length) {
                    enqueueSnackbar(data.deleteReleaseComment.problems.map((problem: { message: any; }) => problem.message).join('; '), { variant: 'warning' });
                } else {
                    setShowDeleteDialog(false);
                }
            },
            updater: (store: RecordSourceProxy) => {
                const record = store.get(data.thread.id);
                if (record == null) {
                    return;
                }

                const commentRecords = record.getLinkedRecords("comments");
                if (commentRecords == null) {
                    return;
                }

                if (commentRecords.length === 1 && commentRecords[0].getValue("id") === commentId) {
                    // Delete record if it's the only comment in the thread
                    store.delete(record.getDataID())
                } else {
                    // Delete comment from the thread
                    const comments = commentRecords.filter((comment) => comment.getValue("id") !== commentId);
                    record.setLinkedRecords(comments, "comments");
                }
            },
            onError: (error) => {
                enqueueSnackbar(`Unexpected error occurred: ${error.message}`, { variant: 'error' });
            }
        });
    };

    return (
        <CommentDetails
            title='Delete comment'
            message='Are you sure you want to delete this comment?'
            showDeleteDialog={showDeleteDialog}
            opInProgress={editIsInFlight || deleteIsInFlight}
            showEditForm={showEditForm}
            showCollapse={showCollapse}
            fragmentRef={data}
            onEdit={onEdit}
            onToggleReplyInput={onToggleReplyInput}
            onOpenDeleteDialog={() => setShowDeleteDialog(true)}
            onCloseDeleteDialog={() => setShowDeleteDialog(false)}
            onShowEditForm={(show: boolean) => setShowEditForm(show)}
            onDelete={(commentToDelete: string) => onDelete(commentToDelete)}
        />
    );
}

export default ReleaseCommentDetails
