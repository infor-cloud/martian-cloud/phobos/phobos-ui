import { Box, Collapse, Divider, List, ListItem, Paper, TextField } from '@mui/material';
import graphql from 'babel-plugin-relay/macro';
import { enqueueSnackbar } from 'notistack';
import { useEffect, useState } from 'react';
import { useFragment, useMutation } from 'react-relay/hooks';
import NewReply from '../../comments/NewReply';
import ReleaseCommentDetails from './ReleaseCommentDetails';
import { ReleaseThreadListItemFragment_thread$key } from './__generated__/ReleaseThreadListItemFragment_thread.graphql';
import { ReleaseThreadListItem_CreateMutation } from './__generated__/ReleaseThreadListItem_CreateMutation.graphql';

interface Props {
    fragmentRef: ReleaseThreadListItemFragment_thread$key
}

function ReleaseThreadListItem({ fragmentRef }: Props) {
    const [showNewReply, setShowNewReply] = useState<boolean>(false);

    const data = useFragment<ReleaseThreadListItemFragment_thread$key>(
        graphql`
            fragment ReleaseThreadListItemFragment_thread on Thread {
                id
                release {
                    id
                }
                comments {
                    id
                    ...ReleaseCommentDetailsFragment_comment
                }
            }
        `, fragmentRef
    );

    const [showCollapse, setShowCollapse] = useState<boolean>(data.comments.length > 1);

    useEffect(() => {
        setShowCollapse(data.comments.length > 1);
    }, [data.comments.length]);

    const [commitNew, newIsInFlight] = useMutation<ReleaseThreadListItem_CreateMutation>(
        graphql`
        mutation ReleaseThreadListItem_CreateMutation($input: CreateReleaseCommentInput!) {
            createReleaseComment(input: $input) {
                comment {
                    id
                    thread {
                        ...ReleaseThreadListItemFragment_thread
                    }
                }
                problems {
                    message
                    field
                    type
                }
            }
        }
    `);

    const onNewComment = (text: string) => {
        commitNew({
            variables: {
                input: {
                    releaseId: data.release?.id ?? '',
                    text,
                    threadId: data.id
                },
            },
            onCompleted: (data: any) => {
                if (data.createReleaseComment.problems.length) {
                    enqueueSnackbar(data.createReleaseComment.problems.map((problem: any) => problem.message).join('; '), { variant: 'warning' });
                } else if (!data.createReleaseComment.comment) {
                    enqueueSnackbar("Unexpected error occurred", { variant: 'error' });
                } else {
                    setShowNewReply(false);
                }
            },
            onError: (error: any) => {
                enqueueSnackbar(error.message, { variant: 'error' });
            }
        });
    };

    return (
        <ListItem
            disableGutters
            sx={{ mb: 1, pt: 0 }}
        >
            <Paper sx={{ width: '100%', p: 2 }}>
                <ReleaseCommentDetails
                    showCollapse={showCollapse}
                    fragmentRef={data.comments[0]}
                    onToggleReplyInput={() => setShowCollapse(!showCollapse)}
                />
                {data.comments.length === 1 && <Collapse
                    in={showCollapse}
                    timeout="auto"
                    unmountOnExit
                >
                    <Box mt={2}>
                        <NewReply
                            opInFlight={newIsInFlight}
                            onSave={onNewComment}
                            onCancel={() => setShowCollapse(false)}
                        />
                    </Box>
                </Collapse>}
                {data.comments.length > 1 && <Collapse
                    in={showCollapse}
                    timeout="auto"
                    unmountOnExit
                >
                    <Box sx={{ width: "100%", mt: 1 }}>
                        <Divider sx={{ mb: 2, ml: -2, mr: -2, opacity: 0.6 }} />
                        <List dense disablePadding>
                            {data.comments.slice(1).map((reply: any) => (
                                <ListItem disableGutters key={reply.id}>
                                    <ReleaseCommentDetails fragmentRef={reply} />
                                </ListItem>
                            ))}
                        </List>
                        <Box>
                            {!showNewReply && <TextField
                                sx={{ width: "100%", mt: 2 }}
                                size="small"
                                placeholder='Reply to this conversation...'
                                onClick={() => setShowNewReply(true)}
                            />}
                            <Collapse
                                sx={{ mt: 2, width: "100%" }}
                                in={showNewReply}
                                timeout="auto"
                                unmountOnExit
                            >
                                <NewReply
                                    opInFlight={newIsInFlight}
                                    onSave={onNewComment}
                                    onCancel={() => setShowNewReply(false)}
                                />
                            </Collapse>
                        </Box>
                    </Box>
                </Collapse>}
            </Paper>
        </ListItem>
    );
}

export default ReleaseThreadListItem
