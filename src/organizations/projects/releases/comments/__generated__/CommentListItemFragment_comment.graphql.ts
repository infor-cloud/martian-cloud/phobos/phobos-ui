/**
 * @generated SignedSource<<7c05011a3f17e6eb80eb27b464145efd>>
 * @lightSyntaxTransform
 * @nogrep
 */

/* tslint:disable */
/* eslint-disable */
// @ts-nocheck

import { Fragment, ReaderFragment } from 'relay-runtime';
import { FragmentRefs } from "relay-runtime";
export type CommentListItemFragment_comment$data = {
  readonly creator: {
    readonly __typename: "ServiceAccount";
    readonly name: string;
  } | {
    readonly __typename: "User";
    readonly email: string;
    readonly username: string;
  } | {
    // This will never be '%other', but we need some
    // value in case none of the concrete values match.
    readonly __typename: "%other";
  } | null | undefined;
  readonly id: string;
  readonly metadata: {
    readonly createdAt: any;
  };
  readonly release: {
    readonly id: string;
  } | null | undefined;
  readonly replies: ReadonlyArray<{
    readonly id: string;
    readonly " $fragmentSpreads": FragmentRefs<"ReplyListItemFragment_reply">;
  }>;
  readonly " $fragmentSpreads": FragmentRefs<"CommentDetailsFragment_comment" | "NewReplyFragment_comment">;
  readonly " $fragmentType": "CommentListItemFragment_comment";
};
export type CommentListItemFragment_comment$key = {
  readonly " $data"?: CommentListItemFragment_comment$data;
  readonly " $fragmentSpreads": FragmentRefs<"CommentListItemFragment_comment">;
};

const node: ReaderFragment = (function(){
var v0 = {
  "alias": null,
  "args": null,
  "kind": "ScalarField",
  "name": "id",
  "storageKey": null
};
return {
  "argumentDefinitions": [],
  "kind": "Fragment",
  "metadata": null,
  "name": "CommentListItemFragment_comment",
  "selections": [
    (v0/*: any*/),
    {
      "alias": null,
      "args": null,
      "concreteType": "Release",
      "kind": "LinkedField",
      "name": "release",
      "plural": false,
      "selections": [
        (v0/*: any*/)
      ],
      "storageKey": null
    },
    {
      "alias": null,
      "args": null,
      "concreteType": "ResourceMetadata",
      "kind": "LinkedField",
      "name": "metadata",
      "plural": false,
      "selections": [
        {
          "alias": null,
          "args": null,
          "kind": "ScalarField",
          "name": "createdAt",
          "storageKey": null
        }
      ],
      "storageKey": null
    },
    {
      "alias": null,
      "args": null,
      "concreteType": null,
      "kind": "LinkedField",
      "name": "creator",
      "plural": false,
      "selections": [
        {
          "alias": null,
          "args": null,
          "kind": "ScalarField",
          "name": "__typename",
          "storageKey": null
        },
        {
          "kind": "InlineFragment",
          "selections": [
            {
              "alias": null,
              "args": null,
              "kind": "ScalarField",
              "name": "email",
              "storageKey": null
            },
            {
              "alias": null,
              "args": null,
              "kind": "ScalarField",
              "name": "username",
              "storageKey": null
            }
          ],
          "type": "User",
          "abstractKey": null
        },
        {
          "kind": "InlineFragment",
          "selections": [
            {
              "alias": null,
              "args": null,
              "kind": "ScalarField",
              "name": "name",
              "storageKey": null
            }
          ],
          "type": "ServiceAccount",
          "abstractKey": null
        }
      ],
      "storageKey": null
    },
    {
      "alias": null,
      "args": null,
      "concreteType": "Comment",
      "kind": "LinkedField",
      "name": "replies",
      "plural": true,
      "selections": [
        (v0/*: any*/),
        {
          "args": null,
          "kind": "FragmentSpread",
          "name": "ReplyListItemFragment_reply"
        }
      ],
      "storageKey": null
    },
    {
      "args": null,
      "kind": "FragmentSpread",
      "name": "CommentDetailsFragment_comment"
    },
    {
      "args": null,
      "kind": "FragmentSpread",
      "name": "NewReplyFragment_comment"
    }
  ],
  "type": "Comment",
  "abstractKey": null
};
})();

(node as any).hash = "986192bb1269e2a7704e746aae495cd5";

export default node;
