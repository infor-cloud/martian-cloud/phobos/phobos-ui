/**
 * @generated SignedSource<<15f2f0dd2a2e49cc0762a432ca5d40bf>>
 * @lightSyntaxTransform
 * @nogrep
 */

/* tslint:disable */
/* eslint-disable */
// @ts-nocheck

import { Fragment, ReaderFragment } from 'relay-runtime';
import { FragmentRefs } from "relay-runtime";
export type ReleaseThreadListFragment_release$data = {
  readonly id: string;
  readonly " $fragmentType": "ReleaseThreadListFragment_release";
};
export type ReleaseThreadListFragment_release$key = {
  readonly " $data"?: ReleaseThreadListFragment_release$data;
  readonly " $fragmentSpreads": FragmentRefs<"ReleaseThreadListFragment_release">;
};

const node: ReaderFragment = {
  "argumentDefinitions": [],
  "kind": "Fragment",
  "metadata": null,
  "name": "ReleaseThreadListFragment_release",
  "selections": [
    {
      "alias": null,
      "args": null,
      "kind": "ScalarField",
      "name": "id",
      "storageKey": null
    }
  ],
  "type": "Release",
  "abstractKey": null
};

(node as any).hash = "ba6a523f236f44ea2880bd915f4637c0";

export default node;
