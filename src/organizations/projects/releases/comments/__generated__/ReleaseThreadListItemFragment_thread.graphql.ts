/**
 * @generated SignedSource<<29c20ecb57eadeb5b0983e3c843921d7>>
 * @lightSyntaxTransform
 * @nogrep
 */

/* tslint:disable */
/* eslint-disable */
// @ts-nocheck

import { Fragment, ReaderFragment } from 'relay-runtime';
import { FragmentRefs } from "relay-runtime";
export type ReleaseThreadListItemFragment_thread$data = {
  readonly comments: ReadonlyArray<{
    readonly id: string;
    readonly " $fragmentSpreads": FragmentRefs<"ReleaseCommentDetailsFragment_comment">;
  }>;
  readonly id: string;
  readonly release: {
    readonly id: string;
  } | null | undefined;
  readonly " $fragmentType": "ReleaseThreadListItemFragment_thread";
};
export type ReleaseThreadListItemFragment_thread$key = {
  readonly " $data"?: ReleaseThreadListItemFragment_thread$data;
  readonly " $fragmentSpreads": FragmentRefs<"ReleaseThreadListItemFragment_thread">;
};

const node: ReaderFragment = (function(){
var v0 = {
  "alias": null,
  "args": null,
  "kind": "ScalarField",
  "name": "id",
  "storageKey": null
};
return {
  "argumentDefinitions": [],
  "kind": "Fragment",
  "metadata": null,
  "name": "ReleaseThreadListItemFragment_thread",
  "selections": [
    (v0/*: any*/),
    {
      "alias": null,
      "args": null,
      "concreteType": "Release",
      "kind": "LinkedField",
      "name": "release",
      "plural": false,
      "selections": [
        (v0/*: any*/)
      ],
      "storageKey": null
    },
    {
      "alias": null,
      "args": null,
      "concreteType": "Comment",
      "kind": "LinkedField",
      "name": "comments",
      "plural": true,
      "selections": [
        (v0/*: any*/),
        {
          "args": null,
          "kind": "FragmentSpread",
          "name": "ReleaseCommentDetailsFragment_comment"
        }
      ],
      "storageKey": null
    }
  ],
  "type": "Thread",
  "abstractKey": null
};
})();

(node as any).hash = "b1824a587746827d7e54f6fa501dbde2";

export default node;
