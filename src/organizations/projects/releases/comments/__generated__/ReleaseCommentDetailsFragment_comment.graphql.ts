/**
 * @generated SignedSource<<5485800a4b25de7866b0362c1cf93fb7>>
 * @lightSyntaxTransform
 * @nogrep
 */

/* tslint:disable */
/* eslint-disable */
// @ts-nocheck

import { Fragment, ReaderFragment } from 'relay-runtime';
import { FragmentRefs } from "relay-runtime";
export type ReleaseCommentDetailsFragment_comment$data = {
  readonly id: string;
  readonly thread: {
    readonly id: string;
  };
  readonly " $fragmentSpreads": FragmentRefs<"CommentDetailsFragment_comment">;
  readonly " $fragmentType": "ReleaseCommentDetailsFragment_comment";
};
export type ReleaseCommentDetailsFragment_comment$key = {
  readonly " $data"?: ReleaseCommentDetailsFragment_comment$data;
  readonly " $fragmentSpreads": FragmentRefs<"ReleaseCommentDetailsFragment_comment">;
};

const node: ReaderFragment = (function(){
var v0 = {
  "alias": null,
  "args": null,
  "kind": "ScalarField",
  "name": "id",
  "storageKey": null
};
return {
  "argumentDefinitions": [],
  "kind": "Fragment",
  "metadata": null,
  "name": "ReleaseCommentDetailsFragment_comment",
  "selections": [
    (v0/*: any*/),
    {
      "alias": null,
      "args": null,
      "concreteType": "Thread",
      "kind": "LinkedField",
      "name": "thread",
      "plural": false,
      "selections": [
        (v0/*: any*/)
      ],
      "storageKey": null
    },
    {
      "args": null,
      "kind": "FragmentSpread",
      "name": "CommentDetailsFragment_comment"
    }
  ],
  "type": "Comment",
  "abstractKey": null
};
})();

(node as any).hash = "d4b7b133b88f664ad55fda199edf5a38";

export default node;
