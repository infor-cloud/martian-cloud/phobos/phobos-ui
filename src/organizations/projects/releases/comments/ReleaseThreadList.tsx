import { Box, List } from '@mui/material';
import graphql from 'babel-plugin-relay/macro';
import { enqueueSnackbar } from 'notistack';
import { useMemo, useState } from 'react';
import InfiniteScroll from 'react-infinite-scroll-component';
import { ConnectionHandler, useFragment, useLazyLoadQuery, useMutation, usePaginationFragment, useSubscription } from 'react-relay/hooks';
import { GraphQLSubscriptionConfig, RecordSourceProxy } from 'relay-runtime';
import ListSkeleton from '../../../../skeletons/ListSkeleton';
import CommentOptionsBar, { SortType } from '../../comments/CommentOptionsBar';
import NewComment, { addThreadToConnection } from '../../comments/NewComment';
import ReleaseThreadListItem from './ReleaseThreadListItem';
import { ReleaseThreadListFragmentPaginationQuery } from './__generated__/ReleaseThreadListFragmentPaginationQuery.graphql';
import { ReleaseThreadListFragment_comments$key } from './__generated__/ReleaseThreadListFragment_comments.graphql';
import { ReleaseThreadListFragment_release$key } from './__generated__/ReleaseThreadListFragment_release.graphql';
import { ReleaseThreadListQuery } from './__generated__/ReleaseThreadListQuery.graphql';
import { ReleaseThreadListSubscription, ReleaseThreadListSubscription$data } from './__generated__/ReleaseThreadListSubscription.graphql';
import { ReleaseThreadList_CreateMutation } from './__generated__/ReleaseThreadList_CreateMutation.graphql';

const query = graphql`
    query ReleaseThreadListQuery($id: String!, $first: Int!, $after: String, $sort: ThreadSort!) {
        node(id: $id) {
            ... on Release {
                ...ReleaseThreadListFragment_comments
            }
        }
    }`;

const releaseCommentSubscription = graphql`
    subscription ReleaseThreadListSubscription($input: CommentEventsSubscriptionInput!) {
        commentEvents(input: $input) {
            action
            comment {
                id
                thread {
                    id
                    ...ReleaseThreadListItemFragment_thread
                }
            }
        }
    }`;

interface Props {
    fragmentRef: ReleaseThreadListFragment_release$key
}

function ReleaseThreadList({ fragmentRef }: Props) {
    const [sortOption, setSortOption] = useState<SortType>('CREATED_AT_ASC');
    const [tab, setTab] = useState<'write' | 'preview'>('write');
    const [formData, setFormData] = useState<string>('');

    const release = useFragment<ReleaseThreadListFragment_release$key>(graphql`
        fragment ReleaseThreadListFragment_release on Release
        {
            id
        }
    `, fragmentRef);

    const queryData = useLazyLoadQuery<ReleaseThreadListQuery>(query,
        { id: release.id, first: 20, sort: sortOption }, { fetchPolicy: 'store-and-network' });

    const { data, loadNext, hasNext } =
        usePaginationFragment<ReleaseThreadListFragmentPaginationQuery, ReleaseThreadListFragment_comments$key>(
            graphql`
            fragment ReleaseThreadListFragment_comments on Release
            @refetchable(queryName: "ReleaseThreadListFragmentPaginationQuery") {
                id
                threads(
                    first: $first
                    after: $after
                    sort: $sort
                    ) @connection(key: "ReleaseThreadListFragment_threads") {
                    edges {
                        node {
                            id
                            ...ReleaseThreadListItemFragment_thread
                        }
                    }
                }
            }
        `, queryData.node
        );

    const releaseCommentSubscriptionConfig = useMemo<GraphQLSubscriptionConfig<ReleaseThreadListSubscription>>(() => ({
        subscription: releaseCommentSubscription,
        variables: { input: { releaseId: release.id } },
        onCompleted: () => console.log('Subscription completed'),
        onError: () => console.warn('Subscription error'),
        updater: (store: RecordSourceProxy, payload: ReleaseThreadListSubscription$data) => {
            const record = store.get(payload.commentEvents.comment.thread.id);
            if (record == null) {
                return;
            }
            const connectionId = ConnectionHandler.getConnectionID(
                release.id,
                'ReleaseThreadListFragment_threads',
                { sort: sortOption }
            );
            addThreadToConnection(store, record, record.getDataID(), sortOption, connectionId);
        }
    }), [release.id, sortOption]);

    useSubscription<ReleaseThreadListSubscription>(releaseCommentSubscriptionConfig);

    const [commitNew, newIsInFlight] = useMutation<ReleaseThreadList_CreateMutation>(
        graphql`
        mutation ReleaseThreadList_CreateMutation($input: CreateReleaseCommentInput!) {
            createReleaseComment(input: $input) {
                comment {
                    id
                    thread {
                       ...ReleaseThreadListItemFragment_thread
                    }
                }
                problems {
                    message
                    field
                    type
                }
            }
        }
    `);

    const onSave = (text: string) => {
        commitNew({
            variables: {
                input: {
                    releaseId: release.id,
                    text
                },
            },
            onCompleted: (data) => {
                if (data.createReleaseComment.problems.length) {
                    enqueueSnackbar(data.createReleaseComment.problems.map((problem: any) => problem.message).join('; '), { variant: 'warning' });
                } else if (!data.createReleaseComment.comment) {
                    enqueueSnackbar("Unexpected error occurred", { variant: 'error' });
                } else {
                    setFormData('');
                    setTab('write');
                }
            },
            onError: (error) => {
                enqueueSnackbar(error.message, { variant: 'error' });
            }
        });
    };

    const comments = data?.threads?.edges ?? [];

    return (
        <Box>
            <Box sx={{ minWidth: 120 }}>
                {comments.length > 1 &&
                    <CommentOptionsBar
                        sortOption={sortOption}
                        onChange={() =>
                            setSortOption(sortOption === 'CREATED_AT_ASC' ? 'CREATED_AT_DESC' : 'CREATED_AT_ASC')}
                    />
                }
            </Box>
            <Box>
                {(sortOption === 'CREATED_AT_DESC' && comments.length !== 1) &&
                    <NewComment
                        tab={tab}
                        formData={formData}
                        isInFlight={newIsInFlight}
                        onSave={onSave}
                        onTabChange={(event: React.SyntheticEvent, newValue: 'write' | 'preview') => setTab(newValue)}
                        onChange={(data: string) => setFormData(data)}
                    />}
                <InfiniteScroll
                    dataLength={(comments.length) ?? 0}
                    next={() => loadNext(20)}
                    hasMore={hasNext}
                    loader={<ListSkeleton rowCount={3} />}
                >
                    <List disablePadding sx={{ mb: 2 }}>
                        {comments.map((edge) => edge?.node ? (
                            <ReleaseThreadListItem
                                key={edge.node.id}
                                fragmentRef={edge.node}
                            />
                        ) : null)}
                    </List>
                </InfiniteScroll>
                {(sortOption === 'CREATED_AT_ASC' || comments.length === 1) &&
                    <NewComment
                        tab={tab}
                        formData={formData}
                        isInFlight={newIsInFlight}
                        onSave={onSave}
                        onTabChange={(event: React.SyntheticEvent, newValue: 'write' | 'preview') => setTab(newValue)}
                        onChange={(data: string) => setFormData(data)}
                    />}
            </Box>
        </Box>
    );
}

export default ReleaseThreadList
