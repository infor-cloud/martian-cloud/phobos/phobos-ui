import { Button, Divider, Link as MuiLink, Stack, StepButton, StepLabel, Toolbar, Tooltip, Typography, styled } from '@mui/material';
import Box from '@mui/material/Box';
import MuiDrawer, { DrawerProps } from '@mui/material/Drawer';
import Step from '@mui/material/Step';
import Stepper from '@mui/material/Stepper';
import graphql from 'babel-plugin-relay/macro';
import moment from 'moment';
import { useMemo, useState } from 'react';
import { useFragment } from 'react-relay/hooks';
import { Link as LinkRouter, useSearchParams } from 'react-router-dom';
import Gravatar from '../../../common/Gravatar';
import StyledAvatar from '../../../common/StyledAvatar';
import Link from '../../../routes/Link';
import PipelineStatusType from '../pipelines/PipelineStatusType';
import ParticipantDialog from './ParticipantDialog';
import { ReleaseDetailsSidebarFragment_details$key } from './__generated__/ReleaseDetailsSidebarFragment_details.graphql';
import Timestamp from '../../../common/Timestamp';
import StackedContainer from '../../../common/StackedContainer';
import CopyButton from '../../../common/CopyButton';

const FieldLabel = styled(
    Typography
)(({ theme }) => ({
    marginBottom: theme.spacing(1)
}));

interface Props {
    fragmentRef: ReleaseDetailsSidebarFragment_details$key
    selectedNodePath?: string
    open: boolean
    temporary: boolean
    onClose: () => void
}

export const SidebarWidth = 400;

const Drawer = styled(MuiDrawer)<DrawerProps>(() => ({
    flexShrink: 0,
    overflowX: 'hidden',
    [`& .MuiDrawer-paper`]: {
        overflowX: 'hidden',
        width: SidebarWidth,
        boxSizing: 'border-box'
    },
    width: SidebarWidth,
}));

function ReleaseDetailsSidebar({ fragmentRef, open, temporary, onClose }: Props) {
    const [searchParams] = useSearchParams();
    const [showParticipantDialog, setShowParticipantDialog] = useState<boolean>(false);

    const selectedStage = searchParams.get('stage');

    const data = useFragment<ReleaseDetailsSidebarFragment_details$key>(
        graphql`
            fragment ReleaseDetailsSidebarFragment_details on Release
                {
                    metadata {
                        createdAt
                    }
                    id
                    dueDate
                    semanticVersion
                    lifecyclePrn
                    participants {
                        __typename
                        ... on User {
                            id
                            email
                            username
                        }
                        ... on Team {
                            id
                            name
                        }
                    }
                    createdBy
                    pipeline {
                        id
                        stages {
                            name
                            path
                            status
                        }
                    }
                    ...ParticipantDialogFragment_participants
                }
        `, fragmentRef);

    const participants = useMemo(() => data.participants.map((p => {
        if (p.__typename === 'User') {
            return { id: p.id, label: p.email, tooltip: p.email, type: 'user' }
        } else if (p.__typename === 'Team') {
            return { id: p.id, label: p.name[0].toUpperCase(), tooltip: p.name, type: 'team' }
        }
    })), [data.participants]);

    return (
        <Drawer
            variant={temporary ? 'temporary' : 'permanent'}
            open={open}
            hideBackdrop={false}
            anchor='right'
            onClose={onClose}
        >
            <Toolbar />
            <Box padding={2}>
                <Box marginBottom={2}>
                    <Box display="flex" alignItems="center" marginBottom={1}>
                        <Link to={`../${data.id}`} variant="h6" mr={2} color="textPrimary" underline="none">Release</Link>
                    </Box>
                    <Box display="flex" alignItems="center" marginBottom={1}>
                        <Tooltip title={data.metadata.createdAt as string}>
                            <Typography variant="caption" sx={{ marginRight: 1 }}>
                                Created {moment(data.metadata.createdAt as moment.MomentInput).fromNow()} by
                            </Typography>
                        </Tooltip>
                        <Tooltip title={data.createdBy}>
                            <Box>
                                <Gravatar width={20} height={20} email={data.createdBy} />
                            </Box>
                        </Tooltip>
                    </Box>
                </Box>
                <Divider sx={{ mt: 2, mb: 2 }} />
                <Box>
                    <FieldLabel>Lifecycle</FieldLabel>
                    <Stack direction="row" spacing={0.5} alignItems="center">
                        <Typography color="textSecondary" sx={{ maxWidth: 325, wordWrap: 'break-word', overflowWrap: 'break-word' }}>{data.lifecyclePrn}</Typography>
                        <CopyButton data={data.lifecyclePrn} toolTip="Copy Lifecycle PRN" />
                    </Stack>
                </Box>
                <Divider sx={{ mt: 2, mb: 2 }} />
                <Box>
                    <FieldLabel>Due Date</FieldLabel>
                    <Typography color="textSecondary">
                        {data.dueDate ? <Timestamp component="span" format="absolute" timestamp={data.dueDate} /> : 'None'}
                    </Typography>
                </Box>
                <Divider sx={{ mt: 2, mb: 2 }} />
                <Box>
                    <FieldLabel>Stages</FieldLabel>
                    <Box sx={{ width: '100%' }}>
                        <Stepper activeStep={1} alternativeLabel={false} orientation="vertical" nonLinear>
                            {data.pipeline.stages.map((stage) => (
                                <Step key={stage.name} active={selectedStage === stage.name}>
                                    <StepButton component={LinkRouter} color="primary" icon={PipelineStatusType[stage.status].icon} to={{ search: `?tab=stages&stage=${stage.name}` }}>
                                        <StepLabel>{stage.name}</StepLabel>
                                    </StepButton>
                                </Step>
                            ))}
                        </Stepper>
                    </Box>
                </Box>
                <Divider sx={{ mt: 2, mb: 2 }} />
                <Box>
                    {data.participants.length > 0 && <Box sx={{ display: "flex", justifyContent: "space-between", alignItems: 'center' }}>
                        <FieldLabel mb={0}>{data.participants.length} Participant{data.participants.length === 1 ? '' : 's'}</FieldLabel>
                        <Button
                            size="small"
                            onClick={() => setShowParticipantDialog(true)}
                            color="info"
                        >
                            Edit
                        </Button>
                    </Box>}
                    {data.participants.length === 0 && <Box>
                        <FieldLabel>Participants</FieldLabel>
                        <MuiLink
                            underline="hover"
                            sx={{ cursor: 'pointer' }}
                            color="secondary"
                            onClick={() => setShowParticipantDialog(true)}
                        >
                            Add Participants
                        </MuiLink>
                    </Box>}
                    <StackedContainer>
                        {participants.map((participant: any) => (
                            <Tooltip key={participant.id} title={participant.tooltip}>
                                <Box>
                                    {participant.type === 'user'
                                        ? <Gravatar width={24} height={24} email={participant.label} />
                                        : <StyledAvatar>{participant.label}</StyledAvatar>}
                                </Box>
                            </Tooltip>
                        ))}
                    </StackedContainer>
                </Box>
            </Box>
            {showParticipantDialog && <ParticipantDialog
                fragmentRef={data}
                onClose={() => setShowParticipantDialog(false)}
            />}
        </Drawer>
    );
}

export default ReleaseDetailsSidebar;
