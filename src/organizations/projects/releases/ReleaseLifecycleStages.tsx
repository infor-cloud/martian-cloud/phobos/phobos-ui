import Box from '@mui/material/Box';
import graphql from 'babel-plugin-relay/macro';
import { useFragment } from 'react-relay/hooks';
import { useSearchParams } from 'react-router-dom';
import LifecycleStage from './ReleaseLifecycleStage';
import { ReleaseLifecycleStagesFragment_stages$key } from './__generated__/ReleaseLifecycleStagesFragment_stages.graphql';

interface Props {
    fragmentRef: ReleaseLifecycleStagesFragment_stages$key;
}

function ReleaseLifecycleStages({ fragmentRef }: Props) {
    const [searchParams, setSearchParams] = useSearchParams();
    const selectedStage = searchParams.get('stage');

    const data = useFragment<ReleaseLifecycleStagesFragment_stages$key>(
        graphql`
            fragment ReleaseLifecycleStagesFragment_stages on Pipeline
            {
                id
                stages {
                    name
                    ...ReleaseLifecycleStageFragment_stage
                }
            }
  `, fragmentRef);

    const onSelectedStageChange = (stageName: string, selected: boolean) => {
        if (selected) {
            searchParams.set('stage', stageName);
        } else {
            searchParams.delete('stage');
        }
        setSearchParams(searchParams, { replace: true });
    };

    return (
        <Box>
            {data.stages.map(stage => (<Box key={stage.name} marginBottom={2}>
                <LifecycleStage
                    fragmentRef={stage}
                    lifecyclePipelineId={data.id}
                    selected={selectedStage === stage.name}
                    onChange={(selected) => onSelectedStageChange(stage.name, selected)}
                />
            </Box>))}
        </Box>
    );
}

export default ReleaseLifecycleStages;
