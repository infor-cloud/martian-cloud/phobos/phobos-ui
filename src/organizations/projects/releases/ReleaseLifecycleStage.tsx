import ExpandMoreIcon from '@mui/icons-material/ExpandMore';
import { Accordion, AccordionDetails, AccordionSummary, Chip, Table, TableBody, TableCell, TableContainer, TableHead, TableRow, Tooltip, Typography } from '@mui/material';
import Box from '@mui/material/Box';
import ListItemIcon from '@mui/material/ListItemIcon';
import graphql from 'babel-plugin-relay/macro';
import React, { useMemo } from 'react';
import { useFragment } from 'react-relay/hooks';
import PipelineStatusType from '../pipelines/PipelineStatusType';
import ReleaseDeploymentListItem from './ReleaseDeploymentListItem';
import ReleaseTaskListItem from './ReleaseTaskListItem';
import { ReleaseLifecycleStageFragment_stage$key } from './__generated__/ReleaseLifecycleStageFragment_stage.graphql';

interface Props {
    fragmentRef: ReleaseLifecycleStageFragment_stage$key
    lifecyclePipelineId: string
    selected: boolean
    onChange: (selected: boolean) => void
}

function ReleaseLifecycleStage({ fragmentRef, lifecyclePipelineId, selected, onChange }: Props) {
    const data = useFragment<ReleaseLifecycleStageFragment_stage$key>(
        graphql`
        fragment ReleaseLifecycleStageFragment_stage on PipelineStage
        {
            path
            name
            status
            tasks {
                path
                ...ReleaseTaskListItemFragment_task
            }
            nestedPipelines {
                path
                name
                status
                environmentName
                latestPipeline {
                    ...ReleaseDeploymentListItemFragment_pipeline
                    id
                }
            }
        }
    `, fragmentRef);

    const stageType = useMemo(() => {
        if (data.name.endsWith('.pre')) {
            return 'pre';
        }
        if (data.name.endsWith('.post')) {
            return 'post';
        }
        return 'deployment';
    }, [data.name]);

    return (
        <Accordion expanded={selected} onChange={(_, expanded) => onChange(expanded)}>
            <AccordionSummary
                expandIcon={<ExpandMoreIcon />}
            >
                <Box display="flex" alignItems="center" flex={1}>
                    <Tooltip title={`stage ${PipelineStatusType[data.status].tooltip}`}>
                        <ListItemIcon sx={{ minWidth: 40 }}>
                            {PipelineStatusType[data.status].icon}
                        </ListItemIcon>
                    </Tooltip>
                    <Box flex={1}>
                        <Typography component="div" variant="body1" fontWeight="500">{data.name}</Typography>
                        {!selected && <React.Fragment>
                            {data.tasks.length > 0 && <Typography variant="body2" color="textSecondary">
                                {data.tasks.length} task{data.tasks.length === 1 ? '' : 's'}
                            </Typography>}
                            {data.nestedPipelines.length > 0 && <Typography variant="body2" color="textSecondary">
                                {data.nestedPipelines.length} deployment{data.nestedPipelines.length === 1 ? '' : 's'}
                            </Typography>}
                        </React.Fragment>}
                    </Box>
                    {stageType !== 'deployment' && <Chip sx={{ mr: 2 }} size="small" label={`${stageType} condition`} />}
                </Box>
            </AccordionSummary>
            <AccordionDetails>
                {stageType !== 'deployment' ? (
                    <React.Fragment>
                        <Typography variant="subtitle1" color="textSecondary">{data.tasks.length} task{data.tasks.length === 1 ? '' : 's'}</Typography>
                        <TableContainer>
                            <Table
                                aria-label="release tasks"
                            >
                                <TableHead>
                                    <TableRow>
                                        <TableCell>Status</TableCell>
                                        <TableCell>Name</TableCell>
                                        <TableCell>Approval Rules</TableCell>
                                        <TableCell>Approvals</TableCell>
                                        <TableCell></TableCell>
                                    </TableRow>
                                </TableHead>
                                <TableBody>
                                    {data.tasks.map((task) => (
                                        <ReleaseTaskListItem key={task.path} fragmentRef={task} pipelineId={lifecyclePipelineId} />
                                    ))}
                                </TableBody>
                            </Table>
                        </TableContainer>
                    </React.Fragment>
                ) : (
                    <React.Fragment>
                        <Typography variant="subtitle1" color="textSecondary">{data.nestedPipelines.length} deployment{data.nestedPipelines.length === 1 ? '' : 's'}</Typography>
                        <TableContainer>
                            <Table
                                aria-label="release deployments"
                            >
                                <TableHead>
                                    <TableRow>
                                        <TableCell>Status</TableCell>
                                        <TableCell>Environment</TableCell>
                                        <TableCell>Pipeline Template</TableCell>
                                        <TableCell>Stages</TableCell>
                                        <TableCell>Approvals</TableCell>
                                        <TableCell></TableCell>
                                    </TableRow>
                                </TableHead>
                                <TableBody>
                                    {data.nestedPipelines.map((pipeline: any) => (
                                        <ReleaseDeploymentListItem key={pipeline.path} fragmentRef={pipeline.latestPipeline} />
                                    ))}
                                </TableBody>
                            </Table>
                        </TableContainer>
                    </React.Fragment>
                )}
            </AccordionDetails>
        </Accordion>
    );
}

export default ReleaseLifecycleStage;
