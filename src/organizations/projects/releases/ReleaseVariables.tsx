import { Box, Paper, TextField, Typography, useTheme } from "@mui/material";
import Table from '@mui/material/Table';
import TableBody from '@mui/material/TableBody';
import TableCell from '@mui/material/TableCell';
import TableContainer from '@mui/material/TableContainer';
import TableHead from '@mui/material/TableHead';
import TableRow from '@mui/material/TableRow';
import { darken } from '@mui/material/styles';
import graphql from 'babel-plugin-relay/macro';
import React, { useState } from "react";
import { useFragment } from "react-relay/hooks";
import DataTableCell from "../../../common/DataTableCell";
import { ReleaseVariablesFragment$key } from "./__generated__/ReleaseVariablesFragment.graphql";

interface Props {
    fragmentRef: ReleaseVariablesFragment$key
}

const variableSearchFilter = (search: string) => (variable: any) => {
    return variable.key.toLowerCase().includes(search) || variable.value.toLowerCase().includes(search);
};

function ReleaseVariables({ fragmentRef }: Props) {
    const theme = useTheme();
    const [search, setSearch] = useState('');

    const release = useFragment<ReleaseVariablesFragment$key>(
        graphql`
            fragment ReleaseVariablesFragment on Release
            {
                id
                pipeline {
                    stages {
                        nestedPipelines {
                            environmentName
                            latestPipeline {
                                variables {
                                    key
                                    value
                                    category
                                }
                            }
                        }
                    }
                }
            }
        `, fragmentRef);

    const onSearchChange = (event: React.ChangeEvent<HTMLInputElement>) => {
        setSearch(event.target.value.toLowerCase());
    };

    const variables = release.pipeline.stages.flatMap(stage => stage.nestedPipelines.flatMap(pipeline => {
        return pipeline.latestPipeline.variables.filter(v => v.category === 'HCL')
            .flatMap(v => ({ ...v, environmentName: pipeline.environmentName }));
    }));

    const filteredVariables = search ? variables.filter(variableSearchFilter(search)) : variables;

    return (
        <Box>
            {variables.length > 0 && <TextField
                size="small"
                margin='none'
                placeholder="search for variables"
                sx={{ marginBottom: 1 }}
                InputProps={{
                    sx: { background: darken(theme.palette.background.default, 0.5) }
                }}
                fullWidth
                onChange={onSearchChange}
                autoComplete="off"
            />}
            <Box>
                {(filteredVariables.length === 0 && search !== '') && <Typography sx={{ padding: 2, marginTop: 4 }} align="center" color="textSecondary">
                    No variables matching search <strong>{search}</strong>
                </Typography>}
                {(filteredVariables.length === 0 && search === '') && <Paper variant="outlined" sx={{ marginTop: 2, display: 'flex', justifyContent: 'center' }}>
                    <Box padding={4} display="flex" flexDirection="column" justifyContent="center" alignItems="center">
                        <Typography color="textSecondary" align="center">
                            This release does not have any variables
                        </Typography>
                    </Box>
                </Paper>}
                {filteredVariables.length > 0 && <TableContainer>
                    <Table sx={{ tableLayout: 'fixed' }}>
                        <TableHead>
                            <TableRow>
                                <TableCell>
                                    <Typography color="textSecondary">Key</Typography>
                                </TableCell>
                                <TableCell>
                                    <Typography color="textSecondary">Value</Typography>
                                </TableCell>
                                <TableCell>
                                    <Typography color="textSecondary">Environment</Typography>
                                </TableCell>
                            </TableRow>
                        </TableHead>
                        <TableBody>
                            {filteredVariables.map((v: any) => (
                                <TableRow
                                    key={`${v.environmentName}:${v.key}`}
                                    sx={{ '&:last-child td, &:last-child th': { border: 0 } }}
                                >
                                    <DataTableCell sx={{ wordBreak: 'break-all' }}>
                                        {v.key}
                                    </DataTableCell>
                                    <DataTableCell sx={{ wordBreak: 'break-all' }} mask={false}>
                                        {v.value}
                                    </DataTableCell>
                                    <DataTableCell sx={{ wordBreak: 'break-all' }}>
                                        {v.environmentName}
                                    </DataTableCell>
                                </TableRow>
                            ))}
                        </TableBody>
                    </Table>
                </TableContainer>}
            </Box>
        </Box>
    );
}

export default ReleaseVariables;
