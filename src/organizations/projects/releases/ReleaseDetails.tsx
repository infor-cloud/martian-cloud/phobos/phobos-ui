import CopyIcon from '@mui/icons-material/ContentCopy';
import DoubleArrowIcon from '@mui/icons-material/DoubleArrow';
import ArrowDropDownIcon from '@mui/icons-material/ArrowDropDown';
import { Alert, AlertTitle, Box, Button, ButtonGroup, Chip, IconButton, Paper, Menu, MenuItem, Stack, Tab, Tabs, Typography, useMediaQuery, useTheme } from '@mui/material';
import graphql from 'babel-plugin-relay/macro';
import React, { useMemo, useState } from 'react';
import { useFragment, useMutation, useSubscription } from 'react-relay/hooks';
import { Link as RouterLink, useNavigate, useSearchParams } from 'react-router-dom';
import { Prism as SyntaxHighlighter } from 'react-syntax-highlighter';
import { atomDark as prismTheme } from 'react-syntax-highlighter/dist/esm/styles/prism';
import { ConnectionHandler, ConnectionInterface, GraphQLSubscriptionConfig, RecordSourceProxy } from 'relay-runtime';
import remarkGfm from 'remark-gfm';
import { PipelineTemplateIcon } from '../../../common/Icons';
import MuiMarkdown from '../../../common/Markdown';
import TabContent from '../../../common/TabContent';
import ProjectBreadcrumbs from '../ProjectBreadcrumbs';
import ReleaseDetailsSidebar, { SidebarWidth } from './ReleaseDetailsSidebar';
import ReleaseDueDateWarningBanner from './ReleaseDueDateWarningBanner';
import LifecycleStages from './ReleaseLifecycleStages';
import ReleaseVariables from './ReleaseVariables';
import PRNButton from '../../../common/PRNButton';
import ConfirmationDialog from '../../../common/ConfirmationDialog';
import ReleaseActivityEventList from './activity/ReleaseActivityEventList';
import CommentList from './comments/ReleaseThreadList';
import PipelineStatusType from '../pipelines/PipelineStatusType';
import { useSnackbar } from 'notistack';
import { GetConnections } from './ReleaseList';
import { ReleaseDetailsCommentSubscription } from './__generated__/ReleaseDetailsCommentSubscription.graphql';
import { ReleaseDetailsLiveActivitySubscription, ReleaseDetailsLiveActivitySubscription$data } from './__generated__/ReleaseDetailsLiveActivitySubscription.graphql';
import { ReleaseDetailsCancelMutation } from './__generated__/ReleaseDetailsCancelMutation.graphql';
import { ReleaseDetailsDeleteMutation } from './__generated__/ReleaseDetailsDeleteMutation.graphql';
import { ReleaseDetailsSubscription } from './__generated__/ReleaseDetailsSubscription.graphql';
import { ReleaseDetailsFragment_release$key } from './__generated__/ReleaseDetailsFragment_release.graphql';

const cancelableStatuses = [
    "CREATED",
    "APPROVAL_PENDING",
    "WAITING",
    "READY",
    "PENDING",
    "RUNNING"
];

const pipelineSubscription = graphql`subscription ReleaseDetailsSubscription($input: PipelineEventsSubscriptionInput!) {
    pipelineEvents(input: $input) {
        action
        pipeline {
            id
            status
            ...ReleaseLifecycleStagesFragment_stages
        }
    }
}`;

const releaseDetailsCommentSubscription = graphql`
    subscription ReleaseDetailsCommentSubscription($input: CommentEventsSubscriptionInput!) {
        commentEvents(input: $input) {
            action
            comment {
                id
                thread {
                    id
                    ...ReleaseThreadListItemFragment_thread
                }
            }
        }
    }
`;

const releaseDetailsLiveActivitySubscription = graphql`
    subscription ReleaseDetailsLiveActivitySubscription($input: ActivityEventsSubscriptionInput!) {
        liveActivityEvents(input: $input) {
            id
            target {
                __typename
                ... on Release {
                    id
                }
                ... on Pipeline {
                    releaseType: release {
                        id
                    }
                }
                ... on Thread {
                    release {
                        id
                    }
                }
            }
            ...ActivityEventReleaseTargetFragment_event
            ...ActivityEventPipelineTargetFragment_event
        }
    }
`;

interface Props {
    fragmentRef: ReleaseDetailsFragment_release$key
}

function ReleaseDetails({ fragmentRef }: Props) {
    const [searchParams, setSearchParams] = useSearchParams();
    const tab = searchParams.get('tab') || 'stages';
    const theme = useTheme();
    const navigate = useNavigate();
    const mobile = useMediaQuery(theme.breakpoints.down('md'));
    const [sidebarOpen, setSidebarOpen] = useState(false);
    const [menuAnchorEl, setMenuAnchorEl] = useState<Element | null>(null);
    const [showDeleteConfirmationDialog, setShowDeleteConfirmationDialog] = useState(false);
    const [showCancelConfirmationDialog, setShowCancelConfirmationDialog] = useState(false);
    const { enqueueSnackbar } = useSnackbar();

    const data = useFragment<ReleaseDetailsFragment_release$key>(graphql`
        fragment ReleaseDetailsFragment_release on Release {
            ...ReleaseDetailsSidebarFragment_details
            ...ReleaseVariablesFragment
            ...ReleaseThreadListFragment_release
            ...ReleaseActivityEventListFragment_activityEvents
            id
            notes
            completed
            semanticVersion
            preRelease
            latest
            dueDate
            metadata {
                prn
            }
            project {
                id
                metadata {
                    prn
                }
            }
            pipeline {
                ...ReleaseLifecycleStagesFragment_stages
                id
                status
                metadata {
                    version
                }
                pipelineTemplate {
                    id
                    name
                    versioned
                    semanticVersion
                    hclData
                }
            }
        }`, fragmentRef);

    const pipelineSubscriptionConfig = useMemo<GraphQLSubscriptionConfig<ReleaseDetailsSubscription>>(() => ({
        variables: { input: { pipelineId: data.pipeline.id, lastSeenVersion: data.pipeline.metadata.version } },
        subscription: pipelineSubscription,
        onCompleted: () => console.log("Subscription completed"),
        onError: (error) => console.warn(`Subscription error: ${error.message}`)
    }), [data.pipeline]);

    useSubscription<ReleaseDetailsSubscription>(pipelineSubscriptionConfig);

    const releaseDetailsCommentSubscriptionConfig = useMemo<GraphQLSubscriptionConfig<ReleaseDetailsCommentSubscription>>(() => ({
        subscription: releaseDetailsCommentSubscription,
        variables: { input: { releaseId: data.id } },
        onCompleted: () => console.log('Subscription completed'),
        onError: (error) => console.warn(`Subscription error: ${error.message}`)
    }), [data.id]);

    useSubscription<ReleaseDetailsCommentSubscription>(releaseDetailsCommentSubscriptionConfig);

    const releaseDetailsLiveActivitySubscriptionConfig = useMemo<GraphQLSubscriptionConfig<ReleaseDetailsLiveActivitySubscription>>(() => ({
        variables: { input: { projectId: data.project.id } },
        subscription: releaseDetailsLiveActivitySubscription,
        onCompleted: () => console.log("Subscription completed"),
        onError: (error) => console.warn(`Subscription error: ${error.message}`),
        updater: (store: RecordSourceProxy, payload: ReleaseDetailsLiveActivitySubscription$data) => {
            const target = payload.liveActivityEvents.target as any;

            if ((target.__typename === 'Thread' && target.release?.id === data.id) ||
                (target.__typename === 'Pipeline' && target.releaseType?.id === data.id) ||
                (target.__typename === 'Release' && target?.id === data.id)) {
                const record = store.get(payload.liveActivityEvents.id);
                if (record) {
                    const { NODE, EDGES } = ConnectionInterface.get()
                    const connectionId = ConnectionHandler.getConnectionID(
                        data.id,
                        'ReleaseActivityEventList_activityEvents',
                        { sort: 'CREATED_AT_DESC' }
                    );
                    const connectionRecord = store.get(connectionId);
                    if (connectionRecord) {
                        const nodeAlreadyExistsInConnection = connectionRecord
                            .getLinkedRecords(EDGES)
                            ?.some(
                                edge => edge?.getLinkedRecord(NODE)?.getDataID() === record.getDataID(),
                            );
                        if (!nodeAlreadyExistsInConnection) {
                            const edge = ConnectionHandler.createEdge(
                                store,
                                connectionRecord,
                                record,
                                'ActivityEventEdge'
                            );
                            if (edge) {
                                ConnectionHandler.insertEdgeBefore(connectionRecord, edge);
                            } else {
                                console.warn('Failed to create edge');
                            }
                        }
                    } else {
                        console.warn('Connection record not found');
                    }
                }
            }
        }
    }), [data.project]);

    useSubscription<ReleaseDetailsLiveActivitySubscription>(releaseDetailsLiveActivitySubscriptionConfig);

    const [commitDelete, deleteInFlight] = useMutation<ReleaseDetailsDeleteMutation>(graphql`
        mutation ReleaseDetailsDeleteMutation(
            $input: DeleteReleaseInput!, $connections: [ID!]!) {
                deleteRelease(input: $input) {
                    release {
                        id @deleteEdge(connections: $connections)
                    }
                    problems {
                        message
                        field
                        type
                    }
                }
            }
        `);

    const onDeleteConfirmationDialogClosed = (confirm?: boolean) => {
        if (confirm) {
            commitDelete({
                variables: {
                    input: {
                        id: data.id
                    },
                    connections: GetConnections(data.project.id)
                },
                onCompleted: (data) => {
                    setShowDeleteConfirmationDialog(false);

                    if (data.deleteRelease.problems.length) {
                        enqueueSnackbar(
                            data.deleteRelease.problems
                                .map((problem: any) => problem.message)
                                .join("; "),
                            { variant: "warning" }
                        );
                    } else {
                        navigate(`..`);
                    }
                },
                onError: (error) => {
                    setShowDeleteConfirmationDialog(false);
                    enqueueSnackbar(`Unexpected error occurred: ${error.message}`, {
                        variant: "error",
                    });
                },
            });
        } else {
            setShowDeleteConfirmationDialog(false);
        }
    };

    const [cancelCommit, cancelInFlight] = useMutation<ReleaseDetailsCancelMutation>(graphql`
        mutation ReleaseDetailsCancelMutation($input: CancelPipelineInput!) {
            cancelPipeline(input: $input) {
                pipeline {
                    id
                }
                problems {
                    message
                    field
                    type
                }
            }
        }
    `);

    const onCancelConfirmationDialogClosed = (confirm?: boolean) => {
        if (confirm) {
            cancelCommit({
                variables: {
                    input: {
                        id: data.pipeline.id
                    }
                },
                onCompleted: (data) => {
                    setShowCancelConfirmationDialog(false);

                    if (data.cancelPipeline.problems.length) {
                        enqueueSnackbar(
                            data.cancelPipeline.problems
                                .map((problem: any) => problem.message)
                                .join("; "),
                            { variant: "warning" }
                        );
                    } else {
                        enqueueSnackbar("Release canceled", { variant: "success" });
                    }
                },
                onError: (error) => {
                    enqueueSnackbar(`Unexpected error occurred: ${error.message}`, {
                        variant: "error",
                    });
                }
            });
        } else {
            setShowCancelConfirmationDialog(false);
        }
    }

    const onTabChange = (event: React.SyntheticEvent, newValue: string) => {
        setSearchParams({ tab: newValue }, { replace: true });
    };

    const onToggleSidebar = () => {
        setSidebarOpen(prev => !prev);
    };

    const onMenuOpen = (event: React.MouseEvent<HTMLButtonElement>) => {
        setMenuAnchorEl(event.currentTarget);
    };

    const onMenuClose = () => {
        setMenuAnchorEl(null);
    };

    const onMenuAction = (actionCallback: () => void) => {
        setMenuAnchorEl(null);
        actionCallback();
    };

    return (
        <Box>
            <ReleaseDetailsSidebar
                fragmentRef={data}
                open={sidebarOpen}
                temporary={mobile}
                onClose={onToggleSidebar}
            />
            <Box paddingRight={!mobile ? `${SidebarWidth}px` : 0}>
                {mobile && <Box display="flex" justifyContent="flex-end">
                    <IconButton onClick={onToggleSidebar}><DoubleArrowIcon sx={{ transform: 'rotate(180deg)' }} /></IconButton>
                </Box>}
                <ProjectBreadcrumbs
                    prn={data.project.metadata.prn}
                    childRoutes={[
                        { title: "releases", path: 'releases' },
                        { title: `${data.id.substring(0, 8)}...`, path: data.id }
                    ]}
                />
                <Box display="flex" justifyContent="space-between" alignItems="center" mb={2}>
                    <Box display="flex" alignItems="center">
                        {PipelineStatusType[data.pipeline.status]?.icon}
                        <Typography ml={1} variant="h5" fontWeight={500}>Release v{data.semanticVersion}</Typography>
                        {data.preRelease && <Chip
                            sx={{ ml: 2 }}
                            size="small"
                            label="pre-release"
                            variant="outlined"
                            color="warning"
                        />}
                        {data.latest && <Chip
                            sx={{ ml: 2 }}
                            size="small"
                            label="latest"
                            variant="filled"
                            color="secondary"
                        />}
                    </Box>
                    <Stack direction="row" spacing={1}>
                        <PRNButton prn={data.metadata.prn} />
                        <ButtonGroup variant="outlined" color="primary">
                            <Button variant="outlined" onClick={() => navigate('edit')}>Edit</Button>
                            <Button
                                color="primary"
                                size="small"
                                aria-label="more options menu"
                                aria-haspopup="menu"
                                onClick={onMenuOpen}
                            >
                                <ArrowDropDownIcon fontSize="small" />
                            </Button>
                        </ButtonGroup>
                    </Stack>
                    <Menu
                        id="release-more-options-menu"
                        anchorEl={menuAnchorEl}
                        open={Boolean(menuAnchorEl)}
                        onClose={onMenuClose}
                        anchorOrigin={{
                            vertical: "bottom",
                            horizontal: "right",
                        }}
                        transformOrigin={{
                            vertical: "top",
                            horizontal: "right",
                        }}
                    >
                        <MenuItem
                            onClick={() =>
                                onMenuAction(() => setShowDeleteConfirmationDialog(true))
                            }
                        >
                            Delete Release
                        </MenuItem>
                        {cancelableStatuses.includes(data.pipeline.status) &&
                            <MenuItem
                                onClick={() =>
                                    onMenuAction(() => setShowCancelConfirmationDialog(true))
                                }
                            >
                                Cancel Release
                            </MenuItem>}
                    </Menu>
                </Box>
                {!data.completed && <ReleaseDueDateWarningBanner dueDate={data.dueDate} sx={{ mb: 2 }} />}
                <Box>
                    <Box sx={{ display: "flex", border: 1, borderColor: 'divider', borderTopLeftRadius: 4, borderTopRightRadius: 4, mb: 2 }}>
                        <Tabs value={tab} onChange={onTabChange}>
                            <Tab label="Stages" value="stages" />
                            <Tab label="Activity" value="activity" />
                            <Tab label="Notes" value="notes" />
                            <Tab label="Variables" value="variables" />
                            <Tab label="Template" value="template" />
                            <Tab label="Comments" value="comments" />
                        </Tabs>
                    </Box>
                    <TabContent>
                        {tab === 'stages' && <LifecycleStages fragmentRef={data.pipeline} />}
                        {tab === 'activity' && <ReleaseActivityEventList fragmentRef={data} />}
                        {tab === 'notes' && <Box>
                            {data.notes !== '' && <Paper variant="outlined" sx={{ padding: 2 }}>
                                <MuiMarkdown
                                    children={data.notes || ''}
                                    remarkPlugins={[remarkGfm]}
                                />
                            </Paper>}
                            {data.notes === '' && <Paper variant="outlined" sx={{ display: 'flex', justifyContent: 'center' }}>
                                <Box padding={4} display="flex" flexDirection="column" justifyContent="center" alignItems="center">
                                    <Typography color="textSecondary" align="center">
                                        This release does not have any notes
                                    </Typography>
                                </Box>
                            </Paper>}
                        </Box>}
                        {tab === 'variables' && <ReleaseVariables fragmentRef={data} />}
                        {tab === 'template' && <Box>
                            {data.pipeline && <Box mt={2} position="relative">
                                <Box sx={{ position: 'absolute', top: 0, right: 0 }} display="flex" alignItems="center">
                                    <IconButton component={RouterLink} sx={{ padding: 2 }} to={`../../-/pipeline_templates/${data.pipeline.pipelineTemplate.id}`}>
                                        <PipelineTemplateIcon sx={{ width: 16, height: 16 }} />
                                    </IconButton>
                                    <IconButton sx={{ padding: 2 }} onClick={() => navigator.clipboard.writeText(data.pipeline.pipelineTemplate.hclData)}>
                                        <CopyIcon sx={{ width: 16, height: 16 }} />
                                    </IconButton>
                                </Box>
                                <SyntaxHighlighter wrapLines customStyle={{ fontSize: 13, minHeight: '60px' }} language="hcl" style={prismTheme} children={data.pipeline.pipelineTemplate.hclData} />
                            </Box>}
                        </Box>}
                        {tab === 'comments' && <CommentList fragmentRef={data} />}
                    </TabContent>
                </Box>
            </Box>
            {showDeleteConfirmationDialog && <ConfirmationDialog
                title="Delete Release"
                message={<>
                    <Alert sx={{ mb: 2 }} severity="warning">
                        <AlertTitle>Warning</AlertTitle>
                        Deleting a release is an <strong><ins>irreversible</ins></strong> operation. All data associated with this release will be deleted and <strong><ins>cannot be recovered</ins></strong>.
                    </Alert>
                </>}
                confirmButtonLabel="Delete"
                opInProgress={deleteInFlight}
                onConfirm={() => onDeleteConfirmationDialogClosed(true)}
                onClose={() => onDeleteConfirmationDialogClosed()}
            />}
            {showCancelConfirmationDialog && <ConfirmationDialog
                title="Cancel Release"
                message={<>
                    <Alert sx={{ mb: 2 }} severity="warning">
                        <AlertTitle>Warning</AlertTitle>
                        This will cancel the current release lifecycle pipeline. Select <strong><ins>Cancel</ins></strong> to confirm. Select <strong><ins>No</ins></strong> to close this dialog without making any changes.
                    </Alert>
                </>}
                confirmButtonLabel="Cancel"
                opInProgress={cancelInFlight}
                onConfirm={() => onCancelConfirmationDialogClosed(true)}
                onClose={() => onCancelConfirmationDialogClosed()}
            />}
        </Box>
    );
}

export default ReleaseDetails;
