/**
 * @generated SignedSource<<843b7e526893344582931db784f72b8b>>
 * @lightSyntaxTransform
 * @nogrep
 */

/* tslint:disable */
/* eslint-disable */
// @ts-nocheck

import { Fragment, ReaderFragment } from 'relay-runtime';
export type PipelineApprovalStatus = "APPROVED" | "NOT_REQUIRED" | "PENDING" | "%future added value";
export type PipelineNodeStatus = "APPROVAL_PENDING" | "CANCELED" | "CANCELING" | "CREATED" | "DEFERRED" | "FAILED" | "PENDING" | "READY" | "RUNNING" | "SKIPPED" | "SUCCEEDED" | "WAITING" | "%future added value";
import { FragmentRefs } from "relay-runtime";
export type ReleaseDeploymentListItemFragment_pipeline$data = {
  readonly approvalRules: ReadonlyArray<{
    readonly approvalsRequired: number;
  }>;
  readonly approvalStatus: PipelineApprovalStatus;
  readonly approvals: ReadonlyArray<{
    readonly approver: {
      readonly __typename: "ServiceAccount";
      readonly name: string;
    } | {
      readonly __typename: "User";
      readonly email: string;
    } | {
      // This will never be '%other', but we need some
      // value in case none of the concrete values match.
      readonly __typename: "%other";
    } | null | undefined;
    readonly id: string;
  }>;
  readonly cronSchedule: {
    readonly expression: string;
    readonly timezone: string;
  } | null | undefined;
  readonly environmentName: string | null | undefined;
  readonly id: string;
  readonly metadata: {
    readonly version: string;
  };
  readonly parentPipeline: {
    readonly id: string;
  } | null | undefined;
  readonly parentPipelineNodePath: string | null | undefined;
  readonly pipelineTemplate: {
    readonly id: string;
    readonly name: string | null | undefined;
    readonly semanticVersion: string | null | undefined;
  };
  readonly scheduledStartTime: any | null | undefined;
  readonly status: PipelineNodeStatus;
  readonly timestamps: {
    readonly completedAt: any | null | undefined;
    readonly startedAt: any | null | undefined;
  };
  readonly when: string;
  readonly " $fragmentSpreads": FragmentRefs<"PipelineStageIcons_stages">;
  readonly " $fragmentType": "ReleaseDeploymentListItemFragment_pipeline";
};
export type ReleaseDeploymentListItemFragment_pipeline$key = {
  readonly " $data"?: ReleaseDeploymentListItemFragment_pipeline$data;
  readonly " $fragmentSpreads": FragmentRefs<"ReleaseDeploymentListItemFragment_pipeline">;
};

const node: ReaderFragment = (function(){
var v0 = {
  "alias": null,
  "args": null,
  "kind": "ScalarField",
  "name": "id",
  "storageKey": null
},
v1 = {
  "alias": null,
  "args": null,
  "kind": "ScalarField",
  "name": "name",
  "storageKey": null
};
return {
  "argumentDefinitions": [],
  "kind": "Fragment",
  "metadata": null,
  "name": "ReleaseDeploymentListItemFragment_pipeline",
  "selections": [
    {
      "alias": null,
      "args": null,
      "concreteType": "ResourceMetadata",
      "kind": "LinkedField",
      "name": "metadata",
      "plural": false,
      "selections": [
        {
          "alias": null,
          "args": null,
          "kind": "ScalarField",
          "name": "version",
          "storageKey": null
        }
      ],
      "storageKey": null
    },
    (v0/*: any*/),
    {
      "alias": null,
      "args": null,
      "kind": "ScalarField",
      "name": "status",
      "storageKey": null
    },
    {
      "alias": null,
      "args": null,
      "kind": "ScalarField",
      "name": "when",
      "storageKey": null
    },
    {
      "alias": null,
      "args": null,
      "kind": "ScalarField",
      "name": "scheduledStartTime",
      "storageKey": null
    },
    {
      "alias": null,
      "args": null,
      "concreteType": "PipelineCronSchedule",
      "kind": "LinkedField",
      "name": "cronSchedule",
      "plural": false,
      "selections": [
        {
          "alias": null,
          "args": null,
          "kind": "ScalarField",
          "name": "expression",
          "storageKey": null
        },
        {
          "alias": null,
          "args": null,
          "kind": "ScalarField",
          "name": "timezone",
          "storageKey": null
        }
      ],
      "storageKey": null
    },
    {
      "alias": null,
      "args": null,
      "kind": "ScalarField",
      "name": "parentPipelineNodePath",
      "storageKey": null
    },
    {
      "alias": null,
      "args": null,
      "concreteType": "PipelineApproval",
      "kind": "LinkedField",
      "name": "approvals",
      "plural": true,
      "selections": [
        (v0/*: any*/),
        {
          "alias": null,
          "args": null,
          "concreteType": null,
          "kind": "LinkedField",
          "name": "approver",
          "plural": false,
          "selections": [
            {
              "alias": null,
              "args": null,
              "kind": "ScalarField",
              "name": "__typename",
              "storageKey": null
            },
            {
              "kind": "InlineFragment",
              "selections": [
                {
                  "alias": null,
                  "args": null,
                  "kind": "ScalarField",
                  "name": "email",
                  "storageKey": null
                }
              ],
              "type": "User",
              "abstractKey": null
            },
            {
              "kind": "InlineFragment",
              "selections": [
                (v1/*: any*/)
              ],
              "type": "ServiceAccount",
              "abstractKey": null
            }
          ],
          "storageKey": null
        }
      ],
      "storageKey": null
    },
    {
      "alias": null,
      "args": null,
      "kind": "ScalarField",
      "name": "approvalStatus",
      "storageKey": null
    },
    {
      "alias": null,
      "args": null,
      "concreteType": "ApprovalRule",
      "kind": "LinkedField",
      "name": "approvalRules",
      "plural": true,
      "selections": [
        {
          "alias": null,
          "args": null,
          "kind": "ScalarField",
          "name": "approvalsRequired",
          "storageKey": null
        }
      ],
      "storageKey": null
    },
    {
      "alias": null,
      "args": null,
      "concreteType": "PipelineTemplate",
      "kind": "LinkedField",
      "name": "pipelineTemplate",
      "plural": false,
      "selections": [
        (v0/*: any*/),
        (v1/*: any*/),
        {
          "alias": null,
          "args": null,
          "kind": "ScalarField",
          "name": "semanticVersion",
          "storageKey": null
        }
      ],
      "storageKey": null
    },
    {
      "alias": null,
      "args": null,
      "concreteType": "Pipeline",
      "kind": "LinkedField",
      "name": "parentPipeline",
      "plural": false,
      "selections": [
        (v0/*: any*/)
      ],
      "storageKey": null
    },
    {
      "alias": null,
      "args": null,
      "concreteType": "PipelineTimestamps",
      "kind": "LinkedField",
      "name": "timestamps",
      "plural": false,
      "selections": [
        {
          "alias": null,
          "args": null,
          "kind": "ScalarField",
          "name": "startedAt",
          "storageKey": null
        },
        {
          "alias": null,
          "args": null,
          "kind": "ScalarField",
          "name": "completedAt",
          "storageKey": null
        }
      ],
      "storageKey": null
    },
    {
      "alias": null,
      "args": null,
      "kind": "ScalarField",
      "name": "environmentName",
      "storageKey": null
    },
    {
      "args": null,
      "kind": "FragmentSpread",
      "name": "PipelineStageIcons_stages"
    }
  ],
  "type": "Pipeline",
  "abstractKey": null
};
})();

(node as any).hash = "592bb270fd7f64c4c7da1079f7d55272";

export default node;
