/**
 * @generated SignedSource<<51be1d185d870e691480bfa0bfc54e7f>>
 * @lightSyntaxTransform
 * @nogrep
 */

/* tslint:disable */
/* eslint-disable */
// @ts-nocheck

import { ConcreteRequest, GraphQLSubscription } from 'relay-runtime';
import { FragmentRefs } from "relay-runtime";
export type PipelineEventsSubscriptionInput = {
  lastSeenVersion?: string | null | undefined;
  pipelineId: string;
};
export type ReleaseDeploymentListItemSubscription$variables = {
  input: PipelineEventsSubscriptionInput;
};
export type ReleaseDeploymentListItemSubscription$data = {
  readonly pipelineEvents: {
    readonly action: string;
    readonly pipeline: {
      readonly id: string;
      readonly " $fragmentSpreads": FragmentRefs<"ReleaseDeploymentListItemFragment_pipeline">;
    };
  };
};
export type ReleaseDeploymentListItemSubscription = {
  response: ReleaseDeploymentListItemSubscription$data;
  variables: ReleaseDeploymentListItemSubscription$variables;
};

const node: ConcreteRequest = (function(){
var v0 = [
  {
    "defaultValue": null,
    "kind": "LocalArgument",
    "name": "input"
  }
],
v1 = [
  {
    "kind": "Variable",
    "name": "input",
    "variableName": "input"
  }
],
v2 = {
  "alias": null,
  "args": null,
  "kind": "ScalarField",
  "name": "action",
  "storageKey": null
},
v3 = {
  "alias": null,
  "args": null,
  "kind": "ScalarField",
  "name": "id",
  "storageKey": null
},
v4 = {
  "alias": null,
  "args": null,
  "kind": "ScalarField",
  "name": "status",
  "storageKey": null
},
v5 = {
  "alias": null,
  "args": null,
  "kind": "ScalarField",
  "name": "name",
  "storageKey": null
},
v6 = [
  (v3/*: any*/)
];
return {
  "fragment": {
    "argumentDefinitions": (v0/*: any*/),
    "kind": "Fragment",
    "metadata": null,
    "name": "ReleaseDeploymentListItemSubscription",
    "selections": [
      {
        "alias": null,
        "args": (v1/*: any*/),
        "concreteType": "PipelineEvent",
        "kind": "LinkedField",
        "name": "pipelineEvents",
        "plural": false,
        "selections": [
          (v2/*: any*/),
          {
            "alias": null,
            "args": null,
            "concreteType": "Pipeline",
            "kind": "LinkedField",
            "name": "pipeline",
            "plural": false,
            "selections": [
              {
                "args": null,
                "kind": "FragmentSpread",
                "name": "ReleaseDeploymentListItemFragment_pipeline"
              },
              (v3/*: any*/)
            ],
            "storageKey": null
          }
        ],
        "storageKey": null
      }
    ],
    "type": "Subscription",
    "abstractKey": null
  },
  "kind": "Request",
  "operation": {
    "argumentDefinitions": (v0/*: any*/),
    "kind": "Operation",
    "name": "ReleaseDeploymentListItemSubscription",
    "selections": [
      {
        "alias": null,
        "args": (v1/*: any*/),
        "concreteType": "PipelineEvent",
        "kind": "LinkedField",
        "name": "pipelineEvents",
        "plural": false,
        "selections": [
          (v2/*: any*/),
          {
            "alias": null,
            "args": null,
            "concreteType": "Pipeline",
            "kind": "LinkedField",
            "name": "pipeline",
            "plural": false,
            "selections": [
              {
                "alias": null,
                "args": null,
                "concreteType": "ResourceMetadata",
                "kind": "LinkedField",
                "name": "metadata",
                "plural": false,
                "selections": [
                  {
                    "alias": null,
                    "args": null,
                    "kind": "ScalarField",
                    "name": "version",
                    "storageKey": null
                  }
                ],
                "storageKey": null
              },
              (v3/*: any*/),
              (v4/*: any*/),
              {
                "alias": null,
                "args": null,
                "kind": "ScalarField",
                "name": "when",
                "storageKey": null
              },
              {
                "alias": null,
                "args": null,
                "kind": "ScalarField",
                "name": "scheduledStartTime",
                "storageKey": null
              },
              {
                "alias": null,
                "args": null,
                "concreteType": "PipelineCronSchedule",
                "kind": "LinkedField",
                "name": "cronSchedule",
                "plural": false,
                "selections": [
                  {
                    "alias": null,
                    "args": null,
                    "kind": "ScalarField",
                    "name": "expression",
                    "storageKey": null
                  },
                  {
                    "alias": null,
                    "args": null,
                    "kind": "ScalarField",
                    "name": "timezone",
                    "storageKey": null
                  }
                ],
                "storageKey": null
              },
              {
                "alias": null,
                "args": null,
                "kind": "ScalarField",
                "name": "parentPipelineNodePath",
                "storageKey": null
              },
              {
                "alias": null,
                "args": null,
                "concreteType": "PipelineApproval",
                "kind": "LinkedField",
                "name": "approvals",
                "plural": true,
                "selections": [
                  (v3/*: any*/),
                  {
                    "alias": null,
                    "args": null,
                    "concreteType": null,
                    "kind": "LinkedField",
                    "name": "approver",
                    "plural": false,
                    "selections": [
                      {
                        "alias": null,
                        "args": null,
                        "kind": "ScalarField",
                        "name": "__typename",
                        "storageKey": null
                      },
                      {
                        "kind": "InlineFragment",
                        "selections": [
                          {
                            "alias": null,
                            "args": null,
                            "kind": "ScalarField",
                            "name": "email",
                            "storageKey": null
                          }
                        ],
                        "type": "User",
                        "abstractKey": null
                      },
                      {
                        "kind": "InlineFragment",
                        "selections": [
                          (v5/*: any*/)
                        ],
                        "type": "ServiceAccount",
                        "abstractKey": null
                      },
                      {
                        "kind": "InlineFragment",
                        "selections": (v6/*: any*/),
                        "type": "Node",
                        "abstractKey": "__isNode"
                      }
                    ],
                    "storageKey": null
                  }
                ],
                "storageKey": null
              },
              {
                "alias": null,
                "args": null,
                "kind": "ScalarField",
                "name": "approvalStatus",
                "storageKey": null
              },
              {
                "alias": null,
                "args": null,
                "concreteType": "ApprovalRule",
                "kind": "LinkedField",
                "name": "approvalRules",
                "plural": true,
                "selections": [
                  {
                    "alias": null,
                    "args": null,
                    "kind": "ScalarField",
                    "name": "approvalsRequired",
                    "storageKey": null
                  },
                  (v3/*: any*/)
                ],
                "storageKey": null
              },
              {
                "alias": null,
                "args": null,
                "concreteType": "PipelineTemplate",
                "kind": "LinkedField",
                "name": "pipelineTemplate",
                "plural": false,
                "selections": [
                  (v3/*: any*/),
                  (v5/*: any*/),
                  {
                    "alias": null,
                    "args": null,
                    "kind": "ScalarField",
                    "name": "semanticVersion",
                    "storageKey": null
                  }
                ],
                "storageKey": null
              },
              {
                "alias": null,
                "args": null,
                "concreteType": "Pipeline",
                "kind": "LinkedField",
                "name": "parentPipeline",
                "plural": false,
                "selections": (v6/*: any*/),
                "storageKey": null
              },
              {
                "alias": null,
                "args": null,
                "concreteType": "PipelineTimestamps",
                "kind": "LinkedField",
                "name": "timestamps",
                "plural": false,
                "selections": [
                  {
                    "alias": null,
                    "args": null,
                    "kind": "ScalarField",
                    "name": "startedAt",
                    "storageKey": null
                  },
                  {
                    "alias": null,
                    "args": null,
                    "kind": "ScalarField",
                    "name": "completedAt",
                    "storageKey": null
                  }
                ],
                "storageKey": null
              },
              {
                "alias": null,
                "args": null,
                "kind": "ScalarField",
                "name": "environmentName",
                "storageKey": null
              },
              {
                "alias": null,
                "args": null,
                "concreteType": "PipelineStage",
                "kind": "LinkedField",
                "name": "stages",
                "plural": true,
                "selections": [
                  {
                    "alias": null,
                    "args": null,
                    "kind": "ScalarField",
                    "name": "path",
                    "storageKey": null
                  },
                  (v5/*: any*/),
                  (v4/*: any*/)
                ],
                "storageKey": null
              }
            ],
            "storageKey": null
          }
        ],
        "storageKey": null
      }
    ]
  },
  "params": {
    "cacheID": "6d2d692fee0b357a03a39ffcdc182e30",
    "id": null,
    "metadata": {},
    "name": "ReleaseDeploymentListItemSubscription",
    "operationKind": "subscription",
    "text": "subscription ReleaseDeploymentListItemSubscription(\n  $input: PipelineEventsSubscriptionInput!\n) {\n  pipelineEvents(input: $input) {\n    action\n    pipeline {\n      ...ReleaseDeploymentListItemFragment_pipeline\n      id\n    }\n  }\n}\n\nfragment PipelineStageIcons_stages on Pipeline {\n  stages {\n    path\n    name\n    status\n  }\n}\n\nfragment ReleaseDeploymentListItemFragment_pipeline on Pipeline {\n  metadata {\n    version\n  }\n  id\n  status\n  when\n  scheduledStartTime\n  cronSchedule {\n    expression\n    timezone\n  }\n  parentPipelineNodePath\n  approvals {\n    id\n    approver {\n      __typename\n      ... on User {\n        email\n      }\n      ... on ServiceAccount {\n        name\n      }\n      ... on Node {\n        __isNode: __typename\n        id\n      }\n    }\n  }\n  approvalStatus\n  approvalRules {\n    approvalsRequired\n    id\n  }\n  pipelineTemplate {\n    id\n    name\n    semanticVersion\n  }\n  parentPipeline {\n    id\n  }\n  timestamps {\n    startedAt\n    completedAt\n  }\n  environmentName\n  ...PipelineStageIcons_stages\n}\n"
  }
};
})();

(node as any).hash = "a01193e355fef22ff6097a62ed56dd0c";

export default node;
