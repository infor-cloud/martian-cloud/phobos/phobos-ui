/**
 * @generated SignedSource<<bdd5498e1975ef1534f5cc4af8125420>>
 * @lightSyntaxTransform
 * @nogrep
 */

/* tslint:disable */
/* eslint-disable */
// @ts-nocheck

import { ConcreteRequest, Mutation } from 'relay-runtime';
export type ProblemType = "BAD_REQUEST" | "CONFLICT" | "FORBIDDEN" | "NOT_FOUND" | "%future added value";
export type RetryNestedPipelineInput = {
  clientMutationId?: string | null | undefined;
  parentNestedPipelineNodePath: string;
  parentPipelineId: string;
};
export type ReleaseDeploymentListItemRetryNestedPipelineMutation$variables = {
  input: RetryNestedPipelineInput;
};
export type ReleaseDeploymentListItemRetryNestedPipelineMutation$data = {
  readonly retryNestedPipeline: {
    readonly problems: ReadonlyArray<{
      readonly field: ReadonlyArray<string> | null | undefined;
      readonly message: string;
      readonly type: ProblemType;
    }>;
  };
};
export type ReleaseDeploymentListItemRetryNestedPipelineMutation = {
  response: ReleaseDeploymentListItemRetryNestedPipelineMutation$data;
  variables: ReleaseDeploymentListItemRetryNestedPipelineMutation$variables;
};

const node: ConcreteRequest = (function(){
var v0 = [
  {
    "defaultValue": null,
    "kind": "LocalArgument",
    "name": "input"
  }
],
v1 = [
  {
    "alias": null,
    "args": [
      {
        "kind": "Variable",
        "name": "input",
        "variableName": "input"
      }
    ],
    "concreteType": "PipelineMutationPayload",
    "kind": "LinkedField",
    "name": "retryNestedPipeline",
    "plural": false,
    "selections": [
      {
        "alias": null,
        "args": null,
        "concreteType": "Problem",
        "kind": "LinkedField",
        "name": "problems",
        "plural": true,
        "selections": [
          {
            "alias": null,
            "args": null,
            "kind": "ScalarField",
            "name": "message",
            "storageKey": null
          },
          {
            "alias": null,
            "args": null,
            "kind": "ScalarField",
            "name": "field",
            "storageKey": null
          },
          {
            "alias": null,
            "args": null,
            "kind": "ScalarField",
            "name": "type",
            "storageKey": null
          }
        ],
        "storageKey": null
      }
    ],
    "storageKey": null
  }
];
return {
  "fragment": {
    "argumentDefinitions": (v0/*: any*/),
    "kind": "Fragment",
    "metadata": null,
    "name": "ReleaseDeploymentListItemRetryNestedPipelineMutation",
    "selections": (v1/*: any*/),
    "type": "Mutation",
    "abstractKey": null
  },
  "kind": "Request",
  "operation": {
    "argumentDefinitions": (v0/*: any*/),
    "kind": "Operation",
    "name": "ReleaseDeploymentListItemRetryNestedPipelineMutation",
    "selections": (v1/*: any*/)
  },
  "params": {
    "cacheID": "c55607deb960d1fda2a40d115e782c6e",
    "id": null,
    "metadata": {},
    "name": "ReleaseDeploymentListItemRetryNestedPipelineMutation",
    "operationKind": "mutation",
    "text": "mutation ReleaseDeploymentListItemRetryNestedPipelineMutation(\n  $input: RetryNestedPipelineInput!\n) {\n  retryNestedPipeline(input: $input) {\n    problems {\n      message\n      field\n      type\n    }\n  }\n}\n"
  }
};
})();

(node as any).hash = "efd5f8cc753e8102033aa225024f29c9";

export default node;
