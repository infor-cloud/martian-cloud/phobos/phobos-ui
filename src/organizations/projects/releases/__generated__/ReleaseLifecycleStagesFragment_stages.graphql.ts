/**
 * @generated SignedSource<<78ab6546c160fabf40335fda5de2b107>>
 * @lightSyntaxTransform
 * @nogrep
 */

/* tslint:disable */
/* eslint-disable */
// @ts-nocheck

import { Fragment, ReaderFragment } from 'relay-runtime';
import { FragmentRefs } from "relay-runtime";
export type ReleaseLifecycleStagesFragment_stages$data = {
  readonly id: string;
  readonly stages: ReadonlyArray<{
    readonly name: string;
    readonly " $fragmentSpreads": FragmentRefs<"ReleaseLifecycleStageFragment_stage">;
  }>;
  readonly " $fragmentType": "ReleaseLifecycleStagesFragment_stages";
};
export type ReleaseLifecycleStagesFragment_stages$key = {
  readonly " $data"?: ReleaseLifecycleStagesFragment_stages$data;
  readonly " $fragmentSpreads": FragmentRefs<"ReleaseLifecycleStagesFragment_stages">;
};

const node: ReaderFragment = {
  "argumentDefinitions": [],
  "kind": "Fragment",
  "metadata": null,
  "name": "ReleaseLifecycleStagesFragment_stages",
  "selections": [
    {
      "alias": null,
      "args": null,
      "kind": "ScalarField",
      "name": "id",
      "storageKey": null
    },
    {
      "alias": null,
      "args": null,
      "concreteType": "PipelineStage",
      "kind": "LinkedField",
      "name": "stages",
      "plural": true,
      "selections": [
        {
          "alias": null,
          "args": null,
          "kind": "ScalarField",
          "name": "name",
          "storageKey": null
        },
        {
          "args": null,
          "kind": "FragmentSpread",
          "name": "ReleaseLifecycleStageFragment_stage"
        }
      ],
      "storageKey": null
    }
  ],
  "type": "Pipeline",
  "abstractKey": null
};

(node as any).hash = "8e511fdbe8624674d45dff447796c17f";

export default node;
