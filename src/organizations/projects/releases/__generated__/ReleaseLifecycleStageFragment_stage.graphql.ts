/**
 * @generated SignedSource<<ae198dc60cc5c814b0b5359a6ab01f44>>
 * @lightSyntaxTransform
 * @nogrep
 */

/* tslint:disable */
/* eslint-disable */
// @ts-nocheck

import { Fragment, ReaderFragment } from 'relay-runtime';
export type PipelineNodeStatus = "APPROVAL_PENDING" | "CANCELED" | "CANCELING" | "CREATED" | "DEFERRED" | "FAILED" | "PENDING" | "READY" | "RUNNING" | "SKIPPED" | "SUCCEEDED" | "WAITING" | "%future added value";
import { FragmentRefs } from "relay-runtime";
export type ReleaseLifecycleStageFragment_stage$data = {
  readonly name: string;
  readonly nestedPipelines: ReadonlyArray<{
    readonly environmentName: string | null | undefined;
    readonly latestPipeline: {
      readonly id: string;
      readonly " $fragmentSpreads": FragmentRefs<"ReleaseDeploymentListItemFragment_pipeline">;
    };
    readonly name: string;
    readonly path: string;
    readonly status: PipelineNodeStatus;
  }>;
  readonly path: string;
  readonly status: PipelineNodeStatus;
  readonly tasks: ReadonlyArray<{
    readonly path: string;
    readonly " $fragmentSpreads": FragmentRefs<"ReleaseTaskListItemFragment_task">;
  }>;
  readonly " $fragmentType": "ReleaseLifecycleStageFragment_stage";
};
export type ReleaseLifecycleStageFragment_stage$key = {
  readonly " $data"?: ReleaseLifecycleStageFragment_stage$data;
  readonly " $fragmentSpreads": FragmentRefs<"ReleaseLifecycleStageFragment_stage">;
};

const node: ReaderFragment = (function(){
var v0 = {
  "alias": null,
  "args": null,
  "kind": "ScalarField",
  "name": "path",
  "storageKey": null
},
v1 = {
  "alias": null,
  "args": null,
  "kind": "ScalarField",
  "name": "name",
  "storageKey": null
},
v2 = {
  "alias": null,
  "args": null,
  "kind": "ScalarField",
  "name": "status",
  "storageKey": null
};
return {
  "argumentDefinitions": [],
  "kind": "Fragment",
  "metadata": null,
  "name": "ReleaseLifecycleStageFragment_stage",
  "selections": [
    (v0/*: any*/),
    (v1/*: any*/),
    (v2/*: any*/),
    {
      "alias": null,
      "args": null,
      "concreteType": "PipelineTask",
      "kind": "LinkedField",
      "name": "tasks",
      "plural": true,
      "selections": [
        (v0/*: any*/),
        {
          "args": null,
          "kind": "FragmentSpread",
          "name": "ReleaseTaskListItemFragment_task"
        }
      ],
      "storageKey": null
    },
    {
      "alias": null,
      "args": null,
      "concreteType": "NestedPipeline",
      "kind": "LinkedField",
      "name": "nestedPipelines",
      "plural": true,
      "selections": [
        (v0/*: any*/),
        (v1/*: any*/),
        (v2/*: any*/),
        {
          "alias": null,
          "args": null,
          "kind": "ScalarField",
          "name": "environmentName",
          "storageKey": null
        },
        {
          "alias": null,
          "args": null,
          "concreteType": "Pipeline",
          "kind": "LinkedField",
          "name": "latestPipeline",
          "plural": false,
          "selections": [
            {
              "args": null,
              "kind": "FragmentSpread",
              "name": "ReleaseDeploymentListItemFragment_pipeline"
            },
            {
              "alias": null,
              "args": null,
              "kind": "ScalarField",
              "name": "id",
              "storageKey": null
            }
          ],
          "storageKey": null
        }
      ],
      "storageKey": null
    }
  ],
  "type": "PipelineStage",
  "abstractKey": null
};
})();

(node as any).hash = "7b23d9a7e19ec4352d4869a897c3ff68";

export default node;
