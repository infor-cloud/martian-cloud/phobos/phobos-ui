/**
 * @generated SignedSource<<da777c0766d3a5c12ea9f4eecb939f4b>>
 * @lightSyntaxTransform
 * @nogrep
 */

/* tslint:disable */
/* eslint-disable */
// @ts-nocheck

import { ConcreteRequest, Mutation } from 'relay-runtime';
export type ProblemType = "BAD_REQUEST" | "CONFLICT" | "FORBIDDEN" | "NOT_FOUND" | "%future added value";
export type RetryPipelineTaskInput = {
  clientMutationId?: string | null | undefined;
  pipelineId: string;
  taskPath: string;
};
export type ReleaseTaskListItemRetryTaskMutation$variables = {
  input: RetryPipelineTaskInput;
};
export type ReleaseTaskListItemRetryTaskMutation$data = {
  readonly retryPipelineTask: {
    readonly problems: ReadonlyArray<{
      readonly field: ReadonlyArray<string> | null | undefined;
      readonly message: string;
      readonly type: ProblemType;
    }>;
  };
};
export type ReleaseTaskListItemRetryTaskMutation = {
  response: ReleaseTaskListItemRetryTaskMutation$data;
  variables: ReleaseTaskListItemRetryTaskMutation$variables;
};

const node: ConcreteRequest = (function(){
var v0 = [
  {
    "defaultValue": null,
    "kind": "LocalArgument",
    "name": "input"
  }
],
v1 = [
  {
    "alias": null,
    "args": [
      {
        "kind": "Variable",
        "name": "input",
        "variableName": "input"
      }
    ],
    "concreteType": "PipelineMutationPayload",
    "kind": "LinkedField",
    "name": "retryPipelineTask",
    "plural": false,
    "selections": [
      {
        "alias": null,
        "args": null,
        "concreteType": "Problem",
        "kind": "LinkedField",
        "name": "problems",
        "plural": true,
        "selections": [
          {
            "alias": null,
            "args": null,
            "kind": "ScalarField",
            "name": "message",
            "storageKey": null
          },
          {
            "alias": null,
            "args": null,
            "kind": "ScalarField",
            "name": "field",
            "storageKey": null
          },
          {
            "alias": null,
            "args": null,
            "kind": "ScalarField",
            "name": "type",
            "storageKey": null
          }
        ],
        "storageKey": null
      }
    ],
    "storageKey": null
  }
];
return {
  "fragment": {
    "argumentDefinitions": (v0/*: any*/),
    "kind": "Fragment",
    "metadata": null,
    "name": "ReleaseTaskListItemRetryTaskMutation",
    "selections": (v1/*: any*/),
    "type": "Mutation",
    "abstractKey": null
  },
  "kind": "Request",
  "operation": {
    "argumentDefinitions": (v0/*: any*/),
    "kind": "Operation",
    "name": "ReleaseTaskListItemRetryTaskMutation",
    "selections": (v1/*: any*/)
  },
  "params": {
    "cacheID": "7aa649eec609a177648442b6a04929c3",
    "id": null,
    "metadata": {},
    "name": "ReleaseTaskListItemRetryTaskMutation",
    "operationKind": "mutation",
    "text": "mutation ReleaseTaskListItemRetryTaskMutation(\n  $input: RetryPipelineTaskInput!\n) {\n  retryPipelineTask(input: $input) {\n    problems {\n      message\n      field\n      type\n    }\n  }\n}\n"
  }
};
})();

(node as any).hash = "016a4ec1156b5ffa42fb40c4023f8ca9";

export default node;
