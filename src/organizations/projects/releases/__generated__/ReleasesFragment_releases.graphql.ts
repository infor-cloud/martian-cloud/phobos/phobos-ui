/**
 * @generated SignedSource<<a52a5fbb253855be3207b90173281925>>
 * @lightSyntaxTransform
 * @nogrep
 */

/* tslint:disable */
/* eslint-disable */
// @ts-nocheck

import { Fragment, ReaderFragment } from 'relay-runtime';
import { FragmentRefs } from "relay-runtime";
export type ReleasesFragment_releases$data = {
  readonly metadata: {
    readonly prn: string;
  };
  readonly " $fragmentSpreads": FragmentRefs<"ReleaseListFragment_project">;
  readonly " $fragmentType": "ReleasesFragment_releases";
};
export type ReleasesFragment_releases$key = {
  readonly " $data"?: ReleasesFragment_releases$data;
  readonly " $fragmentSpreads": FragmentRefs<"ReleasesFragment_releases">;
};

const node: ReaderFragment = {
  "argumentDefinitions": [],
  "kind": "Fragment",
  "metadata": null,
  "name": "ReleasesFragment_releases",
  "selections": [
    {
      "alias": null,
      "args": null,
      "concreteType": "ResourceMetadata",
      "kind": "LinkedField",
      "name": "metadata",
      "plural": false,
      "selections": [
        {
          "alias": null,
          "args": null,
          "kind": "ScalarField",
          "name": "prn",
          "storageKey": null
        }
      ],
      "storageKey": null
    },
    {
      "args": null,
      "kind": "FragmentSpread",
      "name": "ReleaseListFragment_project"
    }
  ],
  "type": "Project",
  "abstractKey": null
};

(node as any).hash = "fe47e45d4f2236d0525f3880b6eee8f9";

export default node;
