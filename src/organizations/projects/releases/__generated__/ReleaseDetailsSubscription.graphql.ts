/**
 * @generated SignedSource<<5c843c199e595d5a8fbface7ca7b6bb6>>
 * @lightSyntaxTransform
 * @nogrep
 */

/* tslint:disable */
/* eslint-disable */
// @ts-nocheck

import { ConcreteRequest, GraphQLSubscription } from 'relay-runtime';
import { FragmentRefs } from "relay-runtime";
export type PipelineNodeStatus = "APPROVAL_PENDING" | "CANCELED" | "CANCELING" | "CREATED" | "DEFERRED" | "FAILED" | "PENDING" | "READY" | "RUNNING" | "SKIPPED" | "SUCCEEDED" | "WAITING" | "%future added value";
export type PipelineEventsSubscriptionInput = {
  lastSeenVersion?: string | null | undefined;
  pipelineId: string;
};
export type ReleaseDetailsSubscription$variables = {
  input: PipelineEventsSubscriptionInput;
};
export type ReleaseDetailsSubscription$data = {
  readonly pipelineEvents: {
    readonly action: string;
    readonly pipeline: {
      readonly id: string;
      readonly status: PipelineNodeStatus;
      readonly " $fragmentSpreads": FragmentRefs<"ReleaseLifecycleStagesFragment_stages">;
    };
  };
};
export type ReleaseDetailsSubscription = {
  response: ReleaseDetailsSubscription$data;
  variables: ReleaseDetailsSubscription$variables;
};

const node: ConcreteRequest = (function(){
var v0 = [
  {
    "defaultValue": null,
    "kind": "LocalArgument",
    "name": "input"
  }
],
v1 = [
  {
    "kind": "Variable",
    "name": "input",
    "variableName": "input"
  }
],
v2 = {
  "alias": null,
  "args": null,
  "kind": "ScalarField",
  "name": "action",
  "storageKey": null
},
v3 = {
  "alias": null,
  "args": null,
  "kind": "ScalarField",
  "name": "id",
  "storageKey": null
},
v4 = {
  "alias": null,
  "args": null,
  "kind": "ScalarField",
  "name": "status",
  "storageKey": null
},
v5 = {
  "alias": null,
  "args": null,
  "kind": "ScalarField",
  "name": "name",
  "storageKey": null
},
v6 = {
  "alias": null,
  "args": null,
  "kind": "ScalarField",
  "name": "path",
  "storageKey": null
},
v7 = {
  "alias": null,
  "args": null,
  "kind": "ScalarField",
  "name": "when",
  "storageKey": null
},
v8 = [
  (v3/*: any*/)
],
v9 = {
  "alias": null,
  "args": null,
  "concreteType": "PipelineApproval",
  "kind": "LinkedField",
  "name": "approvals",
  "plural": true,
  "selections": [
    (v3/*: any*/),
    {
      "alias": null,
      "args": null,
      "concreteType": null,
      "kind": "LinkedField",
      "name": "approver",
      "plural": false,
      "selections": [
        {
          "alias": null,
          "args": null,
          "kind": "ScalarField",
          "name": "__typename",
          "storageKey": null
        },
        {
          "kind": "InlineFragment",
          "selections": [
            {
              "alias": null,
              "args": null,
              "kind": "ScalarField",
              "name": "email",
              "storageKey": null
            }
          ],
          "type": "User",
          "abstractKey": null
        },
        {
          "kind": "InlineFragment",
          "selections": [
            (v5/*: any*/)
          ],
          "type": "ServiceAccount",
          "abstractKey": null
        },
        {
          "kind": "InlineFragment",
          "selections": (v8/*: any*/),
          "type": "Node",
          "abstractKey": "__isNode"
        }
      ],
      "storageKey": null
    }
  ],
  "storageKey": null
},
v10 = {
  "alias": null,
  "args": null,
  "kind": "ScalarField",
  "name": "approvalStatus",
  "storageKey": null
},
v11 = {
  "alias": null,
  "args": null,
  "kind": "ScalarField",
  "name": "approvalsRequired",
  "storageKey": null
},
v12 = {
  "alias": null,
  "args": null,
  "kind": "ScalarField",
  "name": "environmentName",
  "storageKey": null
};
return {
  "fragment": {
    "argumentDefinitions": (v0/*: any*/),
    "kind": "Fragment",
    "metadata": null,
    "name": "ReleaseDetailsSubscription",
    "selections": [
      {
        "alias": null,
        "args": (v1/*: any*/),
        "concreteType": "PipelineEvent",
        "kind": "LinkedField",
        "name": "pipelineEvents",
        "plural": false,
        "selections": [
          (v2/*: any*/),
          {
            "alias": null,
            "args": null,
            "concreteType": "Pipeline",
            "kind": "LinkedField",
            "name": "pipeline",
            "plural": false,
            "selections": [
              (v3/*: any*/),
              (v4/*: any*/),
              {
                "args": null,
                "kind": "FragmentSpread",
                "name": "ReleaseLifecycleStagesFragment_stages"
              }
            ],
            "storageKey": null
          }
        ],
        "storageKey": null
      }
    ],
    "type": "Subscription",
    "abstractKey": null
  },
  "kind": "Request",
  "operation": {
    "argumentDefinitions": (v0/*: any*/),
    "kind": "Operation",
    "name": "ReleaseDetailsSubscription",
    "selections": [
      {
        "alias": null,
        "args": (v1/*: any*/),
        "concreteType": "PipelineEvent",
        "kind": "LinkedField",
        "name": "pipelineEvents",
        "plural": false,
        "selections": [
          (v2/*: any*/),
          {
            "alias": null,
            "args": null,
            "concreteType": "Pipeline",
            "kind": "LinkedField",
            "name": "pipeline",
            "plural": false,
            "selections": [
              (v3/*: any*/),
              (v4/*: any*/),
              {
                "alias": null,
                "args": null,
                "concreteType": "PipelineStage",
                "kind": "LinkedField",
                "name": "stages",
                "plural": true,
                "selections": [
                  (v5/*: any*/),
                  (v6/*: any*/),
                  (v4/*: any*/),
                  {
                    "alias": null,
                    "args": null,
                    "concreteType": "PipelineTask",
                    "kind": "LinkedField",
                    "name": "tasks",
                    "plural": true,
                    "selections": [
                      (v6/*: any*/),
                      (v5/*: any*/),
                      (v4/*: any*/),
                      (v7/*: any*/),
                      {
                        "alias": null,
                        "args": null,
                        "concreteType": "Job",
                        "kind": "LinkedField",
                        "name": "currentJob",
                        "plural": false,
                        "selections": (v8/*: any*/),
                        "storageKey": null
                      },
                      (v9/*: any*/),
                      (v10/*: any*/),
                      {
                        "alias": null,
                        "args": null,
                        "concreteType": "ApprovalRule",
                        "kind": "LinkedField",
                        "name": "approvalRules",
                        "plural": true,
                        "selections": [
                          (v3/*: any*/),
                          (v5/*: any*/),
                          (v11/*: any*/)
                        ],
                        "storageKey": null
                      }
                    ],
                    "storageKey": null
                  },
                  {
                    "alias": null,
                    "args": null,
                    "concreteType": "NestedPipeline",
                    "kind": "LinkedField",
                    "name": "nestedPipelines",
                    "plural": true,
                    "selections": [
                      (v6/*: any*/),
                      (v5/*: any*/),
                      (v4/*: any*/),
                      (v12/*: any*/),
                      {
                        "alias": null,
                        "args": null,
                        "concreteType": "Pipeline",
                        "kind": "LinkedField",
                        "name": "latestPipeline",
                        "plural": false,
                        "selections": [
                          {
                            "alias": null,
                            "args": null,
                            "concreteType": "ResourceMetadata",
                            "kind": "LinkedField",
                            "name": "metadata",
                            "plural": false,
                            "selections": [
                              {
                                "alias": null,
                                "args": null,
                                "kind": "ScalarField",
                                "name": "version",
                                "storageKey": null
                              }
                            ],
                            "storageKey": null
                          },
                          (v3/*: any*/),
                          (v4/*: any*/),
                          (v7/*: any*/),
                          {
                            "alias": null,
                            "args": null,
                            "kind": "ScalarField",
                            "name": "scheduledStartTime",
                            "storageKey": null
                          },
                          {
                            "alias": null,
                            "args": null,
                            "concreteType": "PipelineCronSchedule",
                            "kind": "LinkedField",
                            "name": "cronSchedule",
                            "plural": false,
                            "selections": [
                              {
                                "alias": null,
                                "args": null,
                                "kind": "ScalarField",
                                "name": "expression",
                                "storageKey": null
                              },
                              {
                                "alias": null,
                                "args": null,
                                "kind": "ScalarField",
                                "name": "timezone",
                                "storageKey": null
                              }
                            ],
                            "storageKey": null
                          },
                          {
                            "alias": null,
                            "args": null,
                            "kind": "ScalarField",
                            "name": "parentPipelineNodePath",
                            "storageKey": null
                          },
                          (v9/*: any*/),
                          (v10/*: any*/),
                          {
                            "alias": null,
                            "args": null,
                            "concreteType": "ApprovalRule",
                            "kind": "LinkedField",
                            "name": "approvalRules",
                            "plural": true,
                            "selections": [
                              (v11/*: any*/),
                              (v3/*: any*/)
                            ],
                            "storageKey": null
                          },
                          {
                            "alias": null,
                            "args": null,
                            "concreteType": "PipelineTemplate",
                            "kind": "LinkedField",
                            "name": "pipelineTemplate",
                            "plural": false,
                            "selections": [
                              (v3/*: any*/),
                              (v5/*: any*/),
                              {
                                "alias": null,
                                "args": null,
                                "kind": "ScalarField",
                                "name": "semanticVersion",
                                "storageKey": null
                              }
                            ],
                            "storageKey": null
                          },
                          {
                            "alias": null,
                            "args": null,
                            "concreteType": "Pipeline",
                            "kind": "LinkedField",
                            "name": "parentPipeline",
                            "plural": false,
                            "selections": (v8/*: any*/),
                            "storageKey": null
                          },
                          {
                            "alias": null,
                            "args": null,
                            "concreteType": "PipelineTimestamps",
                            "kind": "LinkedField",
                            "name": "timestamps",
                            "plural": false,
                            "selections": [
                              {
                                "alias": null,
                                "args": null,
                                "kind": "ScalarField",
                                "name": "startedAt",
                                "storageKey": null
                              },
                              {
                                "alias": null,
                                "args": null,
                                "kind": "ScalarField",
                                "name": "completedAt",
                                "storageKey": null
                              }
                            ],
                            "storageKey": null
                          },
                          (v12/*: any*/),
                          {
                            "alias": null,
                            "args": null,
                            "concreteType": "PipelineStage",
                            "kind": "LinkedField",
                            "name": "stages",
                            "plural": true,
                            "selections": [
                              (v6/*: any*/),
                              (v5/*: any*/),
                              (v4/*: any*/)
                            ],
                            "storageKey": null
                          }
                        ],
                        "storageKey": null
                      }
                    ],
                    "storageKey": null
                  }
                ],
                "storageKey": null
              }
            ],
            "storageKey": null
          }
        ],
        "storageKey": null
      }
    ]
  },
  "params": {
    "cacheID": "e8c5017b17a35685e4c3a6ef546f2acc",
    "id": null,
    "metadata": {},
    "name": "ReleaseDetailsSubscription",
    "operationKind": "subscription",
    "text": "subscription ReleaseDetailsSubscription(\n  $input: PipelineEventsSubscriptionInput!\n) {\n  pipelineEvents(input: $input) {\n    action\n    pipeline {\n      id\n      status\n      ...ReleaseLifecycleStagesFragment_stages\n    }\n  }\n}\n\nfragment PipelineStageIcons_stages on Pipeline {\n  stages {\n    path\n    name\n    status\n  }\n}\n\nfragment ReleaseDeploymentListItemFragment_pipeline on Pipeline {\n  metadata {\n    version\n  }\n  id\n  status\n  when\n  scheduledStartTime\n  cronSchedule {\n    expression\n    timezone\n  }\n  parentPipelineNodePath\n  approvals {\n    id\n    approver {\n      __typename\n      ... on User {\n        email\n      }\n      ... on ServiceAccount {\n        name\n      }\n      ... on Node {\n        __isNode: __typename\n        id\n      }\n    }\n  }\n  approvalStatus\n  approvalRules {\n    approvalsRequired\n    id\n  }\n  pipelineTemplate {\n    id\n    name\n    semanticVersion\n  }\n  parentPipeline {\n    id\n  }\n  timestamps {\n    startedAt\n    completedAt\n  }\n  environmentName\n  ...PipelineStageIcons_stages\n}\n\nfragment ReleaseLifecycleStageFragment_stage on PipelineStage {\n  path\n  name\n  status\n  tasks {\n    path\n    ...ReleaseTaskListItemFragment_task\n  }\n  nestedPipelines {\n    path\n    name\n    status\n    environmentName\n    latestPipeline {\n      ...ReleaseDeploymentListItemFragment_pipeline\n      id\n    }\n  }\n}\n\nfragment ReleaseLifecycleStagesFragment_stages on Pipeline {\n  id\n  stages {\n    name\n    ...ReleaseLifecycleStageFragment_stage\n  }\n}\n\nfragment ReleaseTaskListItemFragment_task on PipelineTask {\n  path\n  name\n  status\n  when\n  currentJob {\n    id\n  }\n  approvals {\n    id\n    approver {\n      __typename\n      ... on User {\n        email\n      }\n      ... on ServiceAccount {\n        name\n      }\n      ... on Node {\n        __isNode: __typename\n        id\n      }\n    }\n  }\n  approvalStatus\n  approvalRules {\n    id\n    name\n    approvalsRequired\n  }\n}\n"
  }
};
})();

(node as any).hash = "9dde7330856033d49c280a1f53d0919a";

export default node;
