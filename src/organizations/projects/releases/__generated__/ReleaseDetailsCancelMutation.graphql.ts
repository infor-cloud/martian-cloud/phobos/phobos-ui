/**
 * @generated SignedSource<<522c333b7a8158f59091cf384e04700b>>
 * @lightSyntaxTransform
 * @nogrep
 */

/* tslint:disable */
/* eslint-disable */
// @ts-nocheck

import { ConcreteRequest, Mutation } from 'relay-runtime';
export type ProblemType = "BAD_REQUEST" | "CONFLICT" | "FORBIDDEN" | "NOT_FOUND" | "%future added value";
export type CancelPipelineInput = {
  clientMutationId?: string | null | undefined;
  force?: boolean | null | undefined;
  id: string;
  metadata?: ResourceMetadataInput | null | undefined;
};
export type ResourceMetadataInput = {
  version: string;
};
export type ReleaseDetailsCancelMutation$variables = {
  input: CancelPipelineInput;
};
export type ReleaseDetailsCancelMutation$data = {
  readonly cancelPipeline: {
    readonly pipeline: {
      readonly id: string;
    } | null | undefined;
    readonly problems: ReadonlyArray<{
      readonly field: ReadonlyArray<string> | null | undefined;
      readonly message: string;
      readonly type: ProblemType;
    }>;
  };
};
export type ReleaseDetailsCancelMutation = {
  response: ReleaseDetailsCancelMutation$data;
  variables: ReleaseDetailsCancelMutation$variables;
};

const node: ConcreteRequest = (function(){
var v0 = [
  {
    "defaultValue": null,
    "kind": "LocalArgument",
    "name": "input"
  }
],
v1 = [
  {
    "alias": null,
    "args": [
      {
        "kind": "Variable",
        "name": "input",
        "variableName": "input"
      }
    ],
    "concreteType": "PipelineMutationPayload",
    "kind": "LinkedField",
    "name": "cancelPipeline",
    "plural": false,
    "selections": [
      {
        "alias": null,
        "args": null,
        "concreteType": "Pipeline",
        "kind": "LinkedField",
        "name": "pipeline",
        "plural": false,
        "selections": [
          {
            "alias": null,
            "args": null,
            "kind": "ScalarField",
            "name": "id",
            "storageKey": null
          }
        ],
        "storageKey": null
      },
      {
        "alias": null,
        "args": null,
        "concreteType": "Problem",
        "kind": "LinkedField",
        "name": "problems",
        "plural": true,
        "selections": [
          {
            "alias": null,
            "args": null,
            "kind": "ScalarField",
            "name": "message",
            "storageKey": null
          },
          {
            "alias": null,
            "args": null,
            "kind": "ScalarField",
            "name": "field",
            "storageKey": null
          },
          {
            "alias": null,
            "args": null,
            "kind": "ScalarField",
            "name": "type",
            "storageKey": null
          }
        ],
        "storageKey": null
      }
    ],
    "storageKey": null
  }
];
return {
  "fragment": {
    "argumentDefinitions": (v0/*: any*/),
    "kind": "Fragment",
    "metadata": null,
    "name": "ReleaseDetailsCancelMutation",
    "selections": (v1/*: any*/),
    "type": "Mutation",
    "abstractKey": null
  },
  "kind": "Request",
  "operation": {
    "argumentDefinitions": (v0/*: any*/),
    "kind": "Operation",
    "name": "ReleaseDetailsCancelMutation",
    "selections": (v1/*: any*/)
  },
  "params": {
    "cacheID": "11cba5d2b6cf1f6f6e2f07f11f38a24c",
    "id": null,
    "metadata": {},
    "name": "ReleaseDetailsCancelMutation",
    "operationKind": "mutation",
    "text": "mutation ReleaseDetailsCancelMutation(\n  $input: CancelPipelineInput!\n) {\n  cancelPipeline(input: $input) {\n    pipeline {\n      id\n    }\n    problems {\n      message\n      field\n      type\n    }\n  }\n}\n"
  }
};
})();

(node as any).hash = "8a53fc0d1b247b94e2f081e313ee8f4d";

export default node;
