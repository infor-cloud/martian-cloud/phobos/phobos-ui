/**
 * @generated SignedSource<<76b830712fd94c1361a59fccd5babdc8>>
 * @lightSyntaxTransform
 * @nogrep
 */

/* tslint:disable */
/* eslint-disable */
// @ts-nocheck

import { Fragment, ReaderFragment } from 'relay-runtime';
export type PipelineNodeStatus = "APPROVAL_PENDING" | "CANCELED" | "CANCELING" | "CREATED" | "DEFERRED" | "FAILED" | "PENDING" | "READY" | "RUNNING" | "SKIPPED" | "SUCCEEDED" | "WAITING" | "%future added value";
import { FragmentRefs } from "relay-runtime";
export type ReleaseDetailsSidebarFragment_details$data = {
  readonly createdBy: string;
  readonly dueDate: any | null | undefined;
  readonly id: string;
  readonly lifecyclePrn: string;
  readonly metadata: {
    readonly createdAt: any;
  };
  readonly participants: ReadonlyArray<{
    readonly __typename: "Team";
    readonly id: string;
    readonly name: string;
  } | {
    readonly __typename: "User";
    readonly email: string;
    readonly id: string;
    readonly username: string;
  } | {
    // This will never be '%other', but we need some
    // value in case none of the concrete values match.
    readonly __typename: "%other";
  }>;
  readonly pipeline: {
    readonly id: string;
    readonly stages: ReadonlyArray<{
      readonly name: string;
      readonly path: string;
      readonly status: PipelineNodeStatus;
    }>;
  };
  readonly semanticVersion: string;
  readonly " $fragmentSpreads": FragmentRefs<"ParticipantDialogFragment_participants">;
  readonly " $fragmentType": "ReleaseDetailsSidebarFragment_details";
};
export type ReleaseDetailsSidebarFragment_details$key = {
  readonly " $data"?: ReleaseDetailsSidebarFragment_details$data;
  readonly " $fragmentSpreads": FragmentRefs<"ReleaseDetailsSidebarFragment_details">;
};

const node: ReaderFragment = (function(){
var v0 = {
  "alias": null,
  "args": null,
  "kind": "ScalarField",
  "name": "id",
  "storageKey": null
},
v1 = {
  "alias": null,
  "args": null,
  "kind": "ScalarField",
  "name": "name",
  "storageKey": null
};
return {
  "argumentDefinitions": [],
  "kind": "Fragment",
  "metadata": null,
  "name": "ReleaseDetailsSidebarFragment_details",
  "selections": [
    {
      "alias": null,
      "args": null,
      "concreteType": "ResourceMetadata",
      "kind": "LinkedField",
      "name": "metadata",
      "plural": false,
      "selections": [
        {
          "alias": null,
          "args": null,
          "kind": "ScalarField",
          "name": "createdAt",
          "storageKey": null
        }
      ],
      "storageKey": null
    },
    (v0/*: any*/),
    {
      "alias": null,
      "args": null,
      "kind": "ScalarField",
      "name": "dueDate",
      "storageKey": null
    },
    {
      "alias": null,
      "args": null,
      "kind": "ScalarField",
      "name": "semanticVersion",
      "storageKey": null
    },
    {
      "alias": null,
      "args": null,
      "kind": "ScalarField",
      "name": "lifecyclePrn",
      "storageKey": null
    },
    {
      "alias": null,
      "args": null,
      "concreteType": null,
      "kind": "LinkedField",
      "name": "participants",
      "plural": true,
      "selections": [
        {
          "alias": null,
          "args": null,
          "kind": "ScalarField",
          "name": "__typename",
          "storageKey": null
        },
        {
          "kind": "InlineFragment",
          "selections": [
            (v0/*: any*/),
            {
              "alias": null,
              "args": null,
              "kind": "ScalarField",
              "name": "email",
              "storageKey": null
            },
            {
              "alias": null,
              "args": null,
              "kind": "ScalarField",
              "name": "username",
              "storageKey": null
            }
          ],
          "type": "User",
          "abstractKey": null
        },
        {
          "kind": "InlineFragment",
          "selections": [
            (v0/*: any*/),
            (v1/*: any*/)
          ],
          "type": "Team",
          "abstractKey": null
        }
      ],
      "storageKey": null
    },
    {
      "alias": null,
      "args": null,
      "kind": "ScalarField",
      "name": "createdBy",
      "storageKey": null
    },
    {
      "alias": null,
      "args": null,
      "concreteType": "Pipeline",
      "kind": "LinkedField",
      "name": "pipeline",
      "plural": false,
      "selections": [
        (v0/*: any*/),
        {
          "alias": null,
          "args": null,
          "concreteType": "PipelineStage",
          "kind": "LinkedField",
          "name": "stages",
          "plural": true,
          "selections": [
            (v1/*: any*/),
            {
              "alias": null,
              "args": null,
              "kind": "ScalarField",
              "name": "path",
              "storageKey": null
            },
            {
              "alias": null,
              "args": null,
              "kind": "ScalarField",
              "name": "status",
              "storageKey": null
            }
          ],
          "storageKey": null
        }
      ],
      "storageKey": null
    },
    {
      "args": null,
      "kind": "FragmentSpread",
      "name": "ParticipantDialogFragment_participants"
    }
  ],
  "type": "Release",
  "abstractKey": null
};
})();

(node as any).hash = "c6bb72a186fc561ac6a0e6f620e0ef24";

export default node;
