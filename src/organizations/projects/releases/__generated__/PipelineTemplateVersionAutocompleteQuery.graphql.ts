/**
 * @generated SignedSource<<5b7b1c98438cdc57a6600d333e4816b2>>
 * @lightSyntaxTransform
 * @nogrep
 */

/* tslint:disable */
/* eslint-disable */
// @ts-nocheck

import { ConcreteRequest, Query } from 'relay-runtime';
export type PipelineTemplateVersionAutocompleteQuery$variables = {
  id: string;
  name: string;
};
export type PipelineTemplateVersionAutocompleteQuery$data = {
  readonly node: {
    readonly pipelineTemplates?: {
      readonly edges: ReadonlyArray<{
        readonly node: {
          readonly id: string;
          readonly latest: boolean;
          readonly semanticVersion: string | null;
        } | null;
      } | null> | null;
    };
  } | null;
};
export type PipelineTemplateVersionAutocompleteQuery = {
  response: PipelineTemplateVersionAutocompleteQuery$data;
  variables: PipelineTemplateVersionAutocompleteQuery$variables;
};

const node: ConcreteRequest = (function(){
var v0 = [
  {
    "defaultValue": null,
    "kind": "LocalArgument",
    "name": "id"
  },
  {
    "defaultValue": null,
    "kind": "LocalArgument",
    "name": "name"
  }
],
v1 = [
  {
    "kind": "Variable",
    "name": "id",
    "variableName": "id"
  }
],
v2 = {
  "alias": null,
  "args": null,
  "kind": "ScalarField",
  "name": "id",
  "storageKey": null
},
v3 = {
  "kind": "InlineFragment",
  "selections": [
    {
      "alias": null,
      "args": [
        {
          "kind": "Literal",
          "name": "first",
          "value": 50
        },
        {
          "kind": "Variable",
          "name": "name",
          "variableName": "name"
        },
        {
          "kind": "Literal",
          "name": "versioned",
          "value": true
        }
      ],
      "concreteType": "PipelineTemplateConnection",
      "kind": "LinkedField",
      "name": "pipelineTemplates",
      "plural": false,
      "selections": [
        {
          "alias": null,
          "args": null,
          "concreteType": "PipelineTemplateEdge",
          "kind": "LinkedField",
          "name": "edges",
          "plural": true,
          "selections": [
            {
              "alias": null,
              "args": null,
              "concreteType": "PipelineTemplate",
              "kind": "LinkedField",
              "name": "node",
              "plural": false,
              "selections": [
                (v2/*: any*/),
                {
                  "alias": null,
                  "args": null,
                  "kind": "ScalarField",
                  "name": "latest",
                  "storageKey": null
                },
                {
                  "alias": null,
                  "args": null,
                  "kind": "ScalarField",
                  "name": "semanticVersion",
                  "storageKey": null
                }
              ],
              "storageKey": null
            }
          ],
          "storageKey": null
        }
      ],
      "storageKey": null
    }
  ],
  "type": "Project",
  "abstractKey": null
};
return {
  "fragment": {
    "argumentDefinitions": (v0/*: any*/),
    "kind": "Fragment",
    "metadata": null,
    "name": "PipelineTemplateVersionAutocompleteQuery",
    "selections": [
      {
        "alias": null,
        "args": (v1/*: any*/),
        "concreteType": null,
        "kind": "LinkedField",
        "name": "node",
        "plural": false,
        "selections": [
          (v3/*: any*/)
        ],
        "storageKey": null
      }
    ],
    "type": "Query",
    "abstractKey": null
  },
  "kind": "Request",
  "operation": {
    "argumentDefinitions": (v0/*: any*/),
    "kind": "Operation",
    "name": "PipelineTemplateVersionAutocompleteQuery",
    "selections": [
      {
        "alias": null,
        "args": (v1/*: any*/),
        "concreteType": null,
        "kind": "LinkedField",
        "name": "node",
        "plural": false,
        "selections": [
          {
            "alias": null,
            "args": null,
            "kind": "ScalarField",
            "name": "__typename",
            "storageKey": null
          },
          (v3/*: any*/),
          (v2/*: any*/)
        ],
        "storageKey": null
      }
    ]
  },
  "params": {
    "cacheID": "e9cb24284f82c13ff5c259d7f8d451e6",
    "id": null,
    "metadata": {},
    "name": "PipelineTemplateVersionAutocompleteQuery",
    "operationKind": "query",
    "text": "query PipelineTemplateVersionAutocompleteQuery(\n  $id: String!\n  $name: String!\n) {\n  node(id: $id) {\n    __typename\n    ... on Project {\n      pipelineTemplates(first: 50, name: $name, versioned: true) {\n        edges {\n          node {\n            id\n            latest\n            semanticVersion\n          }\n        }\n      }\n    }\n    id\n  }\n}\n"
  }
};
})();

(node as any).hash = "90babbda8496f9ee15956050201c3786";

export default node;
