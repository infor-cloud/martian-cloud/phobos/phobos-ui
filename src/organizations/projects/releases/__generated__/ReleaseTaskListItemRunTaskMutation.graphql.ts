/**
 * @generated SignedSource<<6c508489a0fdc5019fe4fc8645532652>>
 * @lightSyntaxTransform
 * @nogrep
 */

/* tslint:disable */
/* eslint-disable */
// @ts-nocheck

import { ConcreteRequest, Mutation } from 'relay-runtime';
export type ProblemType = "BAD_REQUEST" | "CONFLICT" | "FORBIDDEN" | "NOT_FOUND" | "%future added value";
export type RunPipelineTaskInput = {
  clientMutationId?: string | null | undefined;
  pipelineId: string;
  taskPath: string;
};
export type ReleaseTaskListItemRunTaskMutation$variables = {
  input: RunPipelineTaskInput;
};
export type ReleaseTaskListItemRunTaskMutation$data = {
  readonly runPipelineTask: {
    readonly problems: ReadonlyArray<{
      readonly field: ReadonlyArray<string> | null | undefined;
      readonly message: string;
      readonly type: ProblemType;
    }>;
  };
};
export type ReleaseTaskListItemRunTaskMutation = {
  response: ReleaseTaskListItemRunTaskMutation$data;
  variables: ReleaseTaskListItemRunTaskMutation$variables;
};

const node: ConcreteRequest = (function(){
var v0 = [
  {
    "defaultValue": null,
    "kind": "LocalArgument",
    "name": "input"
  }
],
v1 = [
  {
    "alias": null,
    "args": [
      {
        "kind": "Variable",
        "name": "input",
        "variableName": "input"
      }
    ],
    "concreteType": "PipelineMutationPayload",
    "kind": "LinkedField",
    "name": "runPipelineTask",
    "plural": false,
    "selections": [
      {
        "alias": null,
        "args": null,
        "concreteType": "Problem",
        "kind": "LinkedField",
        "name": "problems",
        "plural": true,
        "selections": [
          {
            "alias": null,
            "args": null,
            "kind": "ScalarField",
            "name": "message",
            "storageKey": null
          },
          {
            "alias": null,
            "args": null,
            "kind": "ScalarField",
            "name": "field",
            "storageKey": null
          },
          {
            "alias": null,
            "args": null,
            "kind": "ScalarField",
            "name": "type",
            "storageKey": null
          }
        ],
        "storageKey": null
      }
    ],
    "storageKey": null
  }
];
return {
  "fragment": {
    "argumentDefinitions": (v0/*: any*/),
    "kind": "Fragment",
    "metadata": null,
    "name": "ReleaseTaskListItemRunTaskMutation",
    "selections": (v1/*: any*/),
    "type": "Mutation",
    "abstractKey": null
  },
  "kind": "Request",
  "operation": {
    "argumentDefinitions": (v0/*: any*/),
    "kind": "Operation",
    "name": "ReleaseTaskListItemRunTaskMutation",
    "selections": (v1/*: any*/)
  },
  "params": {
    "cacheID": "0028c8e2d3df20b6c945a83fd191bd19",
    "id": null,
    "metadata": {},
    "name": "ReleaseTaskListItemRunTaskMutation",
    "operationKind": "mutation",
    "text": "mutation ReleaseTaskListItemRunTaskMutation(\n  $input: RunPipelineTaskInput!\n) {\n  runPipelineTask(input: $input) {\n    problems {\n      message\n      field\n      type\n    }\n  }\n}\n"
  }
};
})();

(node as any).hash = "31a24bbf27a36088b3726f9ff22b8ad6";

export default node;
