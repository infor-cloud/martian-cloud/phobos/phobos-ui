/**
 * @generated SignedSource<<77de016be4014371dfeaeb0e06970fb8>>
 * @lightSyntaxTransform
 * @nogrep
 */

/* tslint:disable */
/* eslint-disable */
// @ts-nocheck

import { ConcreteRequest, Query } from 'relay-runtime';
export type LifecycleAutocompleteQuery$variables = {
  name: string;
};
export type LifecycleAutocompleteQuery$data = {
  readonly organization: {
    readonly releaseLifecycles: {
      readonly edges: ReadonlyArray<{
        readonly node: {
          readonly id: string;
          readonly lifecycleTemplate: {
            readonly id: string;
          };
          readonly name: string;
        } | null;
      } | null> | null;
    };
  } | null;
};
export type LifecycleAutocompleteQuery = {
  response: LifecycleAutocompleteQuery$data;
  variables: LifecycleAutocompleteQuery$variables;
};

const node: ConcreteRequest = (function(){
var v0 = [
  {
    "defaultValue": null,
    "kind": "LocalArgument",
    "name": "name"
  }
],
v1 = [
  {
    "kind": "Variable",
    "name": "name",
    "variableName": "name"
  }
],
v2 = {
  "alias": null,
  "args": null,
  "kind": "ScalarField",
  "name": "id",
  "storageKey": null
},
v3 = {
  "alias": null,
  "args": [
    {
      "kind": "Literal",
      "name": "first",
      "value": 50
    }
  ],
  "concreteType": "ReleaseLifecycleConnection",
  "kind": "LinkedField",
  "name": "releaseLifecycles",
  "plural": false,
  "selections": [
    {
      "alias": null,
      "args": null,
      "concreteType": "ReleaseLifecycleEdge",
      "kind": "LinkedField",
      "name": "edges",
      "plural": true,
      "selections": [
        {
          "alias": null,
          "args": null,
          "concreteType": "ReleaseLifecycle",
          "kind": "LinkedField",
          "name": "node",
          "plural": false,
          "selections": [
            (v2/*: any*/),
            {
              "alias": null,
              "args": null,
              "kind": "ScalarField",
              "name": "name",
              "storageKey": null
            },
            {
              "alias": null,
              "args": null,
              "concreteType": "LifecycleTemplate",
              "kind": "LinkedField",
              "name": "lifecycleTemplate",
              "plural": false,
              "selections": [
                (v2/*: any*/)
              ],
              "storageKey": null
            }
          ],
          "storageKey": null
        }
      ],
      "storageKey": null
    }
  ],
  "storageKey": "releaseLifecycles(first:50)"
};
return {
  "fragment": {
    "argumentDefinitions": (v0/*: any*/),
    "kind": "Fragment",
    "metadata": null,
    "name": "LifecycleAutocompleteQuery",
    "selections": [
      {
        "alias": null,
        "args": (v1/*: any*/),
        "concreteType": "Organization",
        "kind": "LinkedField",
        "name": "organization",
        "plural": false,
        "selections": [
          (v3/*: any*/)
        ],
        "storageKey": null
      }
    ],
    "type": "Query",
    "abstractKey": null
  },
  "kind": "Request",
  "operation": {
    "argumentDefinitions": (v0/*: any*/),
    "kind": "Operation",
    "name": "LifecycleAutocompleteQuery",
    "selections": [
      {
        "alias": null,
        "args": (v1/*: any*/),
        "concreteType": "Organization",
        "kind": "LinkedField",
        "name": "organization",
        "plural": false,
        "selections": [
          (v3/*: any*/),
          (v2/*: any*/)
        ],
        "storageKey": null
      }
    ]
  },
  "params": {
    "cacheID": "2a897673726fb2899ce80faa8cfa2a64",
    "id": null,
    "metadata": {},
    "name": "LifecycleAutocompleteQuery",
    "operationKind": "query",
    "text": "query LifecycleAutocompleteQuery(\n  $name: String!\n) {\n  organization(name: $name) {\n    releaseLifecycles(first: 50) {\n      edges {\n        node {\n          id\n          name\n          lifecycleTemplate {\n            id\n          }\n        }\n      }\n    }\n    id\n  }\n}\n"
  }
};
})();

(node as any).hash = "fdac6d7bb1228c236a944a6c2ef69258";

export default node;
