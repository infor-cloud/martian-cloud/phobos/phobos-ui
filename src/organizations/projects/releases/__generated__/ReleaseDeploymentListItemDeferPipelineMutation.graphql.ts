/**
 * @generated SignedSource<<0f1b0b28818c2fc51c9dd456bfe2aa5d>>
 * @lightSyntaxTransform
 * @nogrep
 */

/* tslint:disable */
/* eslint-disable */
// @ts-nocheck

import { ConcreteRequest, Mutation } from 'relay-runtime';
export type PipelineNodeType = "ACTION" | "PIPELINE" | "STAGE" | "TASK" | "%future added value";
export type ProblemType = "BAD_REQUEST" | "CONFLICT" | "FORBIDDEN" | "NOT_FOUND" | "%future added value";
export type DeferPipelineNodeInput = {
  clientMutationId?: string | null | undefined;
  nodePath: string;
  nodeType: PipelineNodeType;
  pipelineId: string;
  reason: string;
};
export type ReleaseDeploymentListItemDeferPipelineMutation$variables = {
  input: DeferPipelineNodeInput;
};
export type ReleaseDeploymentListItemDeferPipelineMutation$data = {
  readonly deferPipelineNode: {
    readonly problems: ReadonlyArray<{
      readonly field: ReadonlyArray<string> | null | undefined;
      readonly message: string;
      readonly type: ProblemType;
    }>;
  };
};
export type ReleaseDeploymentListItemDeferPipelineMutation = {
  response: ReleaseDeploymentListItemDeferPipelineMutation$data;
  variables: ReleaseDeploymentListItemDeferPipelineMutation$variables;
};

const node: ConcreteRequest = (function(){
var v0 = [
  {
    "defaultValue": null,
    "kind": "LocalArgument",
    "name": "input"
  }
],
v1 = [
  {
    "alias": null,
    "args": [
      {
        "kind": "Variable",
        "name": "input",
        "variableName": "input"
      }
    ],
    "concreteType": "PipelineMutationPayload",
    "kind": "LinkedField",
    "name": "deferPipelineNode",
    "plural": false,
    "selections": [
      {
        "alias": null,
        "args": null,
        "concreteType": "Problem",
        "kind": "LinkedField",
        "name": "problems",
        "plural": true,
        "selections": [
          {
            "alias": null,
            "args": null,
            "kind": "ScalarField",
            "name": "message",
            "storageKey": null
          },
          {
            "alias": null,
            "args": null,
            "kind": "ScalarField",
            "name": "field",
            "storageKey": null
          },
          {
            "alias": null,
            "args": null,
            "kind": "ScalarField",
            "name": "type",
            "storageKey": null
          }
        ],
        "storageKey": null
      }
    ],
    "storageKey": null
  }
];
return {
  "fragment": {
    "argumentDefinitions": (v0/*: any*/),
    "kind": "Fragment",
    "metadata": null,
    "name": "ReleaseDeploymentListItemDeferPipelineMutation",
    "selections": (v1/*: any*/),
    "type": "Mutation",
    "abstractKey": null
  },
  "kind": "Request",
  "operation": {
    "argumentDefinitions": (v0/*: any*/),
    "kind": "Operation",
    "name": "ReleaseDeploymentListItemDeferPipelineMutation",
    "selections": (v1/*: any*/)
  },
  "params": {
    "cacheID": "c821239c4337ed0486f3bd94a123794b",
    "id": null,
    "metadata": {},
    "name": "ReleaseDeploymentListItemDeferPipelineMutation",
    "operationKind": "mutation",
    "text": "mutation ReleaseDeploymentListItemDeferPipelineMutation(\n  $input: DeferPipelineNodeInput!\n) {\n  deferPipelineNode(input: $input) {\n    problems {\n      message\n      field\n      type\n    }\n  }\n}\n"
  }
};
})();

(node as any).hash = "234cd78f3ccbf7f9b505b0041c721bbd";

export default node;
