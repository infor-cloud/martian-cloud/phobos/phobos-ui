/**
 * @generated SignedSource<<8f02b8181a7d90597ddf6f1a9fd18b68>>
 * @lightSyntaxTransform
 * @nogrep
 */

/* tslint:disable */
/* eslint-disable */
// @ts-nocheck

import { ConcreteRequest, Mutation } from 'relay-runtime';
export type ProblemType = "BAD_REQUEST" | "CONFLICT" | "FORBIDDEN" | "NOT_FOUND" | "%future added value";
export type RunPipelineInput = {
  clientMutationId?: string | null | undefined;
  id: string;
};
export type ReleaseDeploymentListItemRunPipelineMutation$variables = {
  input: RunPipelineInput;
};
export type ReleaseDeploymentListItemRunPipelineMutation$data = {
  readonly runPipeline: {
    readonly problems: ReadonlyArray<{
      readonly field: ReadonlyArray<string> | null | undefined;
      readonly message: string;
      readonly type: ProblemType;
    }>;
  };
};
export type ReleaseDeploymentListItemRunPipelineMutation = {
  response: ReleaseDeploymentListItemRunPipelineMutation$data;
  variables: ReleaseDeploymentListItemRunPipelineMutation$variables;
};

const node: ConcreteRequest = (function(){
var v0 = [
  {
    "defaultValue": null,
    "kind": "LocalArgument",
    "name": "input"
  }
],
v1 = [
  {
    "alias": null,
    "args": [
      {
        "kind": "Variable",
        "name": "input",
        "variableName": "input"
      }
    ],
    "concreteType": "PipelineMutationPayload",
    "kind": "LinkedField",
    "name": "runPipeline",
    "plural": false,
    "selections": [
      {
        "alias": null,
        "args": null,
        "concreteType": "Problem",
        "kind": "LinkedField",
        "name": "problems",
        "plural": true,
        "selections": [
          {
            "alias": null,
            "args": null,
            "kind": "ScalarField",
            "name": "message",
            "storageKey": null
          },
          {
            "alias": null,
            "args": null,
            "kind": "ScalarField",
            "name": "field",
            "storageKey": null
          },
          {
            "alias": null,
            "args": null,
            "kind": "ScalarField",
            "name": "type",
            "storageKey": null
          }
        ],
        "storageKey": null
      }
    ],
    "storageKey": null
  }
];
return {
  "fragment": {
    "argumentDefinitions": (v0/*: any*/),
    "kind": "Fragment",
    "metadata": null,
    "name": "ReleaseDeploymentListItemRunPipelineMutation",
    "selections": (v1/*: any*/),
    "type": "Mutation",
    "abstractKey": null
  },
  "kind": "Request",
  "operation": {
    "argumentDefinitions": (v0/*: any*/),
    "kind": "Operation",
    "name": "ReleaseDeploymentListItemRunPipelineMutation",
    "selections": (v1/*: any*/)
  },
  "params": {
    "cacheID": "78a97e845ab0f94a24c897628bea37b6",
    "id": null,
    "metadata": {},
    "name": "ReleaseDeploymentListItemRunPipelineMutation",
    "operationKind": "mutation",
    "text": "mutation ReleaseDeploymentListItemRunPipelineMutation(\n  $input: RunPipelineInput!\n) {\n  runPipeline(input: $input) {\n    problems {\n      message\n      field\n      type\n    }\n  }\n}\n"
  }
};
})();

(node as any).hash = "efacf7bdd05c7e26ed9dbca6bb6b9386";

export default node;
