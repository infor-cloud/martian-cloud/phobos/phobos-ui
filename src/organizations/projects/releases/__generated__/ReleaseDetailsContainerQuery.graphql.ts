/**
 * @generated SignedSource<<7de64cdfc292a26711421c104459c5e9>>
 * @lightSyntaxTransform
 * @nogrep
 */

/* tslint:disable */
/* eslint-disable */
// @ts-nocheck

import { ConcreteRequest, Query } from 'relay-runtime';
import { FragmentRefs } from "relay-runtime";
export type ReleaseDetailsContainerQuery$variables = {
  after?: string | null | undefined;
  first: number;
  id: string;
};
export type ReleaseDetailsContainerQuery$data = {
  readonly node: {
    readonly id?: string;
    readonly " $fragmentSpreads": FragmentRefs<"ReleaseDetailsFragment_release">;
  } | null | undefined;
};
export type ReleaseDetailsContainerQuery = {
  response: ReleaseDetailsContainerQuery$data;
  variables: ReleaseDetailsContainerQuery$variables;
};

const node: ConcreteRequest = (function(){
var v0 = {
  "defaultValue": null,
  "kind": "LocalArgument",
  "name": "after"
},
v1 = {
  "defaultValue": null,
  "kind": "LocalArgument",
  "name": "first"
},
v2 = {
  "defaultValue": null,
  "kind": "LocalArgument",
  "name": "id"
},
v3 = [
  {
    "kind": "Variable",
    "name": "id",
    "variableName": "id"
  }
],
v4 = {
  "alias": null,
  "args": null,
  "kind": "ScalarField",
  "name": "id",
  "storageKey": null
},
v5 = {
  "alias": null,
  "args": null,
  "kind": "ScalarField",
  "name": "__typename",
  "storageKey": null
},
v6 = {
  "alias": null,
  "args": null,
  "kind": "ScalarField",
  "name": "createdAt",
  "storageKey": null
},
v7 = {
  "alias": null,
  "args": null,
  "kind": "ScalarField",
  "name": "prn",
  "storageKey": null
},
v8 = {
  "alias": null,
  "args": null,
  "kind": "ScalarField",
  "name": "semanticVersion",
  "storageKey": null
},
v9 = {
  "alias": null,
  "args": null,
  "kind": "ScalarField",
  "name": "email",
  "storageKey": null
},
v10 = {
  "alias": null,
  "args": null,
  "kind": "ScalarField",
  "name": "username",
  "storageKey": null
},
v11 = {
  "alias": null,
  "args": null,
  "kind": "ScalarField",
  "name": "name",
  "storageKey": null
},
v12 = [
  (v4/*: any*/)
],
v13 = {
  "kind": "InlineFragment",
  "selections": (v12/*: any*/),
  "type": "Node",
  "abstractKey": "__isNode"
},
v14 = {
  "alias": null,
  "args": null,
  "kind": "ScalarField",
  "name": "path",
  "storageKey": null
},
v15 = {
  "alias": null,
  "args": null,
  "kind": "ScalarField",
  "name": "status",
  "storageKey": null
},
v16 = {
  "alias": null,
  "args": null,
  "kind": "ScalarField",
  "name": "environmentName",
  "storageKey": null
},
v17 = {
  "alias": null,
  "args": null,
  "concreteType": "ResourceMetadata",
  "kind": "LinkedField",
  "name": "metadata",
  "plural": false,
  "selections": [
    {
      "alias": null,
      "args": null,
      "kind": "ScalarField",
      "name": "version",
      "storageKey": null
    }
  ],
  "storageKey": null
},
v18 = {
  "alias": null,
  "args": null,
  "kind": "ScalarField",
  "name": "when",
  "storageKey": null
},
v19 = [
  (v11/*: any*/)
],
v20 = {
  "kind": "InlineFragment",
  "selections": (v19/*: any*/),
  "type": "ServiceAccount",
  "abstractKey": null
},
v21 = {
  "alias": null,
  "args": null,
  "concreteType": "PipelineApproval",
  "kind": "LinkedField",
  "name": "approvals",
  "plural": true,
  "selections": [
    (v4/*: any*/),
    {
      "alias": null,
      "args": null,
      "concreteType": null,
      "kind": "LinkedField",
      "name": "approver",
      "plural": false,
      "selections": [
        (v5/*: any*/),
        {
          "kind": "InlineFragment",
          "selections": [
            (v9/*: any*/)
          ],
          "type": "User",
          "abstractKey": null
        },
        (v20/*: any*/),
        (v13/*: any*/)
      ],
      "storageKey": null
    }
  ],
  "storageKey": null
},
v22 = {
  "alias": null,
  "args": null,
  "kind": "ScalarField",
  "name": "approvalStatus",
  "storageKey": null
},
v23 = {
  "alias": null,
  "args": null,
  "kind": "ScalarField",
  "name": "approvalsRequired",
  "storageKey": null
},
v24 = [
  {
    "kind": "Variable",
    "name": "after",
    "variableName": "after"
  },
  {
    "kind": "Variable",
    "name": "first",
    "variableName": "first"
  },
  {
    "kind": "Literal",
    "name": "sort",
    "value": "CREATED_AT_DESC"
  }
],
v25 = {
  "alias": null,
  "args": null,
  "concreteType": "ResourceMetadata",
  "kind": "LinkedField",
  "name": "metadata",
  "plural": false,
  "selections": [
    (v7/*: any*/)
  ],
  "storageKey": null
},
v26 = {
  "alias": null,
  "args": null,
  "kind": "ScalarField",
  "name": "type",
  "storageKey": null
},
v27 = {
  "alias": null,
  "args": null,
  "kind": "ScalarField",
  "name": "nodePath",
  "storageKey": null
},
v28 = {
  "alias": null,
  "args": null,
  "kind": "ScalarField",
  "name": "nodeType",
  "storageKey": null
},
v29 = [
  (v27/*: any*/),
  (v28/*: any*/)
];
return {
  "fragment": {
    "argumentDefinitions": [
      (v0/*: any*/),
      (v1/*: any*/),
      (v2/*: any*/)
    ],
    "kind": "Fragment",
    "metadata": null,
    "name": "ReleaseDetailsContainerQuery",
    "selections": [
      {
        "alias": null,
        "args": (v3/*: any*/),
        "concreteType": null,
        "kind": "LinkedField",
        "name": "node",
        "plural": false,
        "selections": [
          {
            "kind": "InlineFragment",
            "selections": [
              (v4/*: any*/),
              {
                "args": null,
                "kind": "FragmentSpread",
                "name": "ReleaseDetailsFragment_release"
              }
            ],
            "type": "Release",
            "abstractKey": null
          }
        ],
        "storageKey": null
      }
    ],
    "type": "Query",
    "abstractKey": null
  },
  "kind": "Request",
  "operation": {
    "argumentDefinitions": [
      (v2/*: any*/),
      (v1/*: any*/),
      (v0/*: any*/)
    ],
    "kind": "Operation",
    "name": "ReleaseDetailsContainerQuery",
    "selections": [
      {
        "alias": null,
        "args": (v3/*: any*/),
        "concreteType": null,
        "kind": "LinkedField",
        "name": "node",
        "plural": false,
        "selections": [
          (v5/*: any*/),
          (v4/*: any*/),
          {
            "kind": "InlineFragment",
            "selections": [
              {
                "alias": null,
                "args": null,
                "concreteType": "ResourceMetadata",
                "kind": "LinkedField",
                "name": "metadata",
                "plural": false,
                "selections": [
                  (v6/*: any*/),
                  (v7/*: any*/)
                ],
                "storageKey": null
              },
              {
                "alias": null,
                "args": null,
                "kind": "ScalarField",
                "name": "dueDate",
                "storageKey": null
              },
              (v8/*: any*/),
              {
                "alias": null,
                "args": null,
                "kind": "ScalarField",
                "name": "lifecyclePrn",
                "storageKey": null
              },
              {
                "alias": null,
                "args": null,
                "concreteType": null,
                "kind": "LinkedField",
                "name": "participants",
                "plural": true,
                "selections": [
                  (v5/*: any*/),
                  {
                    "kind": "InlineFragment",
                    "selections": [
                      (v4/*: any*/),
                      (v9/*: any*/),
                      (v10/*: any*/)
                    ],
                    "type": "User",
                    "abstractKey": null
                  },
                  {
                    "kind": "InlineFragment",
                    "selections": [
                      (v4/*: any*/),
                      (v11/*: any*/)
                    ],
                    "type": "Team",
                    "abstractKey": null
                  },
                  (v13/*: any*/)
                ],
                "storageKey": null
              },
              {
                "alias": null,
                "args": null,
                "kind": "ScalarField",
                "name": "createdBy",
                "storageKey": null
              },
              {
                "alias": null,
                "args": null,
                "concreteType": "Pipeline",
                "kind": "LinkedField",
                "name": "pipeline",
                "plural": false,
                "selections": [
                  (v4/*: any*/),
                  {
                    "alias": null,
                    "args": null,
                    "concreteType": "PipelineStage",
                    "kind": "LinkedField",
                    "name": "stages",
                    "plural": true,
                    "selections": [
                      (v11/*: any*/),
                      (v14/*: any*/),
                      (v15/*: any*/),
                      {
                        "alias": null,
                        "args": null,
                        "concreteType": "NestedPipeline",
                        "kind": "LinkedField",
                        "name": "nestedPipelines",
                        "plural": true,
                        "selections": [
                          (v16/*: any*/),
                          {
                            "alias": null,
                            "args": null,
                            "concreteType": "Pipeline",
                            "kind": "LinkedField",
                            "name": "latestPipeline",
                            "plural": false,
                            "selections": [
                              {
                                "alias": null,
                                "args": null,
                                "concreteType": "PipelineVariable",
                                "kind": "LinkedField",
                                "name": "variables",
                                "plural": true,
                                "selections": [
                                  {
                                    "alias": null,
                                    "args": null,
                                    "kind": "ScalarField",
                                    "name": "key",
                                    "storageKey": null
                                  },
                                  {
                                    "alias": null,
                                    "args": null,
                                    "kind": "ScalarField",
                                    "name": "value",
                                    "storageKey": null
                                  },
                                  {
                                    "alias": null,
                                    "args": null,
                                    "kind": "ScalarField",
                                    "name": "category",
                                    "storageKey": null
                                  }
                                ],
                                "storageKey": null
                              },
                              (v4/*: any*/),
                              (v17/*: any*/),
                              (v15/*: any*/),
                              (v18/*: any*/),
                              {
                                "alias": null,
                                "args": null,
                                "kind": "ScalarField",
                                "name": "scheduledStartTime",
                                "storageKey": null
                              },
                              {
                                "alias": null,
                                "args": null,
                                "concreteType": "PipelineCronSchedule",
                                "kind": "LinkedField",
                                "name": "cronSchedule",
                                "plural": false,
                                "selections": [
                                  {
                                    "alias": null,
                                    "args": null,
                                    "kind": "ScalarField",
                                    "name": "expression",
                                    "storageKey": null
                                  },
                                  {
                                    "alias": null,
                                    "args": null,
                                    "kind": "ScalarField",
                                    "name": "timezone",
                                    "storageKey": null
                                  }
                                ],
                                "storageKey": null
                              },
                              {
                                "alias": null,
                                "args": null,
                                "kind": "ScalarField",
                                "name": "parentPipelineNodePath",
                                "storageKey": null
                              },
                              (v21/*: any*/),
                              (v22/*: any*/),
                              {
                                "alias": null,
                                "args": null,
                                "concreteType": "ApprovalRule",
                                "kind": "LinkedField",
                                "name": "approvalRules",
                                "plural": true,
                                "selections": [
                                  (v23/*: any*/),
                                  (v4/*: any*/)
                                ],
                                "storageKey": null
                              },
                              {
                                "alias": null,
                                "args": null,
                                "concreteType": "PipelineTemplate",
                                "kind": "LinkedField",
                                "name": "pipelineTemplate",
                                "plural": false,
                                "selections": [
                                  (v4/*: any*/),
                                  (v11/*: any*/),
                                  (v8/*: any*/)
                                ],
                                "storageKey": null
                              },
                              {
                                "alias": null,
                                "args": null,
                                "concreteType": "Pipeline",
                                "kind": "LinkedField",
                                "name": "parentPipeline",
                                "plural": false,
                                "selections": (v12/*: any*/),
                                "storageKey": null
                              },
                              {
                                "alias": null,
                                "args": null,
                                "concreteType": "PipelineTimestamps",
                                "kind": "LinkedField",
                                "name": "timestamps",
                                "plural": false,
                                "selections": [
                                  {
                                    "alias": null,
                                    "args": null,
                                    "kind": "ScalarField",
                                    "name": "startedAt",
                                    "storageKey": null
                                  },
                                  {
                                    "alias": null,
                                    "args": null,
                                    "kind": "ScalarField",
                                    "name": "completedAt",
                                    "storageKey": null
                                  }
                                ],
                                "storageKey": null
                              },
                              (v16/*: any*/),
                              {
                                "alias": null,
                                "args": null,
                                "concreteType": "PipelineStage",
                                "kind": "LinkedField",
                                "name": "stages",
                                "plural": true,
                                "selections": [
                                  (v14/*: any*/),
                                  (v11/*: any*/),
                                  (v15/*: any*/)
                                ],
                                "storageKey": null
                              }
                            ],
                            "storageKey": null
                          },
                          (v14/*: any*/),
                          (v11/*: any*/),
                          (v15/*: any*/)
                        ],
                        "storageKey": null
                      },
                      {
                        "alias": null,
                        "args": null,
                        "concreteType": "PipelineTask",
                        "kind": "LinkedField",
                        "name": "tasks",
                        "plural": true,
                        "selections": [
                          (v14/*: any*/),
                          (v11/*: any*/),
                          (v15/*: any*/),
                          (v18/*: any*/),
                          {
                            "alias": null,
                            "args": null,
                            "concreteType": "Job",
                            "kind": "LinkedField",
                            "name": "currentJob",
                            "plural": false,
                            "selections": (v12/*: any*/),
                            "storageKey": null
                          },
                          (v21/*: any*/),
                          (v22/*: any*/),
                          {
                            "alias": null,
                            "args": null,
                            "concreteType": "ApprovalRule",
                            "kind": "LinkedField",
                            "name": "approvalRules",
                            "plural": true,
                            "selections": [
                              (v4/*: any*/),
                              (v11/*: any*/),
                              (v23/*: any*/)
                            ],
                            "storageKey": null
                          }
                        ],
                        "storageKey": null
                      }
                    ],
                    "storageKey": null
                  },
                  (v15/*: any*/),
                  (v17/*: any*/),
                  {
                    "alias": null,
                    "args": null,
                    "concreteType": "PipelineTemplate",
                    "kind": "LinkedField",
                    "name": "pipelineTemplate",
                    "plural": false,
                    "selections": [
                      (v4/*: any*/),
                      (v11/*: any*/),
                      {
                        "alias": null,
                        "args": null,
                        "kind": "ScalarField",
                        "name": "versioned",
                        "storageKey": null
                      },
                      (v8/*: any*/),
                      {
                        "alias": null,
                        "args": null,
                        "kind": "ScalarField",
                        "name": "hclData",
                        "storageKey": null
                      }
                    ],
                    "storageKey": null
                  }
                ],
                "storageKey": null
              },
              {
                "alias": null,
                "args": (v24/*: any*/),
                "concreteType": "ActivityEventConnection",
                "kind": "LinkedField",
                "name": "activityEvents",
                "plural": false,
                "selections": [
                  {
                    "alias": null,
                    "args": null,
                    "concreteType": "ActivityEventEdge",
                    "kind": "LinkedField",
                    "name": "edges",
                    "plural": true,
                    "selections": [
                      {
                        "alias": null,
                        "args": null,
                        "concreteType": "ActivityEvent",
                        "kind": "LinkedField",
                        "name": "node",
                        "plural": false,
                        "selections": [
                          (v4/*: any*/),
                          {
                            "alias": null,
                            "args": null,
                            "kind": "ScalarField",
                            "name": "action",
                            "storageKey": null
                          },
                          {
                            "alias": null,
                            "args": null,
                            "concreteType": null,
                            "kind": "LinkedField",
                            "name": "target",
                            "plural": false,
                            "selections": [
                              (v5/*: any*/),
                              (v4/*: any*/),
                              {
                                "kind": "InlineFragment",
                                "selections": [
                                  {
                                    "alias": null,
                                    "args": null,
                                    "concreteType": "Release",
                                    "kind": "LinkedField",
                                    "name": "release",
                                    "plural": false,
                                    "selections": (v12/*: any*/),
                                    "storageKey": null
                                  },
                                  {
                                    "alias": null,
                                    "args": null,
                                    "concreteType": "Comment",
                                    "kind": "LinkedField",
                                    "name": "comments",
                                    "plural": true,
                                    "selections": [
                                      (v4/*: any*/),
                                      {
                                        "alias": null,
                                        "args": null,
                                        "concreteType": "Thread",
                                        "kind": "LinkedField",
                                        "name": "thread",
                                        "plural": false,
                                        "selections": [
                                          (v4/*: any*/),
                                          {
                                            "alias": null,
                                            "args": null,
                                            "concreteType": "Comment",
                                            "kind": "LinkedField",
                                            "name": "comments",
                                            "plural": true,
                                            "selections": (v12/*: any*/),
                                            "storageKey": null
                                          }
                                        ],
                                        "storageKey": null
                                      },
                                      {
                                        "alias": null,
                                        "args": null,
                                        "concreteType": "ResourceMetadata",
                                        "kind": "LinkedField",
                                        "name": "metadata",
                                        "plural": false,
                                        "selections": [
                                          (v6/*: any*/),
                                          {
                                            "alias": null,
                                            "args": null,
                                            "kind": "ScalarField",
                                            "name": "updatedAt",
                                            "storageKey": null
                                          }
                                        ],
                                        "storageKey": null
                                      },
                                      {
                                        "alias": null,
                                        "args": null,
                                        "kind": "ScalarField",
                                        "name": "text",
                                        "storageKey": null
                                      },
                                      {
                                        "alias": null,
                                        "args": null,
                                        "concreteType": null,
                                        "kind": "LinkedField",
                                        "name": "creator",
                                        "plural": false,
                                        "selections": [
                                          (v5/*: any*/),
                                          {
                                            "kind": "InlineFragment",
                                            "selections": [
                                              (v9/*: any*/),
                                              (v10/*: any*/)
                                            ],
                                            "type": "User",
                                            "abstractKey": null
                                          },
                                          (v20/*: any*/),
                                          (v13/*: any*/)
                                        ],
                                        "storageKey": null
                                      }
                                    ],
                                    "storageKey": null
                                  }
                                ],
                                "type": "Thread",
                                "abstractKey": null
                              },
                              {
                                "kind": "InlineFragment",
                                "selections": [
                                  (v25/*: any*/),
                                  (v26/*: any*/),
                                  (v16/*: any*/)
                                ],
                                "type": "Pipeline",
                                "abstractKey": null
                              },
                              {
                                "kind": "InlineFragment",
                                "selections": [
                                  (v8/*: any*/),
                                  (v25/*: any*/)
                                ],
                                "type": "Release",
                                "abstractKey": null
                              }
                            ],
                            "storageKey": null
                          },
                          {
                            "alias": null,
                            "args": null,
                            "concreteType": "ResourceMetadata",
                            "kind": "LinkedField",
                            "name": "metadata",
                            "plural": false,
                            "selections": [
                              (v6/*: any*/)
                            ],
                            "storageKey": null
                          },
                          {
                            "alias": null,
                            "args": null,
                            "concreteType": null,
                            "kind": "LinkedField",
                            "name": "initiator",
                            "plural": false,
                            "selections": [
                              (v5/*: any*/),
                              {
                                "kind": "InlineFragment",
                                "selections": [
                                  (v10/*: any*/),
                                  (v9/*: any*/)
                                ],
                                "type": "User",
                                "abstractKey": null
                              },
                              (v20/*: any*/),
                              (v13/*: any*/)
                            ],
                            "storageKey": null
                          },
                          {
                            "alias": null,
                            "args": null,
                            "concreteType": null,
                            "kind": "LinkedField",
                            "name": "payload",
                            "plural": false,
                            "selections": [
                              (v5/*: any*/),
                              {
                                "kind": "InlineFragment",
                                "selections": [
                                  (v26/*: any*/)
                                ],
                                "type": "ActivityEventDeleteResourcePayload",
                                "abstractKey": null
                              },
                              {
                                "kind": "InlineFragment",
                                "selections": (v29/*: any*/),
                                "type": "ActivityEventUpdatePipelineNodePayload",
                                "abstractKey": null
                              },
                              {
                                "kind": "InlineFragment",
                                "selections": (v29/*: any*/),
                                "type": "ActivityEventApprovePipelineNodePayload",
                                "abstractKey": null
                              },
                              {
                                "kind": "InlineFragment",
                                "selections": (v29/*: any*/),
                                "type": "ActivityEventRevokeApprovalPipelineNodePayload",
                                "abstractKey": null
                              },
                              {
                                "kind": "InlineFragment",
                                "selections": (v29/*: any*/),
                                "type": "ActivityEventRetryPipelineNodePayload",
                                "abstractKey": null
                              },
                              {
                                "kind": "InlineFragment",
                                "selections": [
                                  (v27/*: any*/),
                                  (v28/*: any*/),
                                  {
                                    "alias": null,
                                    "args": null,
                                    "kind": "ScalarField",
                                    "name": "startTime",
                                    "storageKey": null
                                  }
                                ],
                                "type": "ActivityEventSchedulePipelineNodePayload",
                                "abstractKey": null
                              },
                              {
                                "kind": "InlineFragment",
                                "selections": (v29/*: any*/),
                                "type": "ActivityEventUnschedulePipelineNodePayload",
                                "abstractKey": null
                              },
                              {
                                "kind": "InlineFragment",
                                "selections": (v29/*: any*/),
                                "type": "ActivityEventStartPipelineNodePayload",
                                "abstractKey": null
                              },
                              {
                                "kind": "InlineFragment",
                                "selections": [
                                  (v27/*: any*/),
                                  (v28/*: any*/),
                                  {
                                    "alias": null,
                                    "args": null,
                                    "kind": "ScalarField",
                                    "name": "reason",
                                    "storageKey": null
                                  }
                                ],
                                "type": "ActivityEventDeferPipelineNodePayload",
                                "abstractKey": null
                              },
                              {
                                "kind": "InlineFragment",
                                "selections": (v29/*: any*/),
                                "type": "ActivityEventUndeferPipelineNodePayload",
                                "abstractKey": null
                              },
                              {
                                "kind": "InlineFragment",
                                "selections": [
                                  {
                                    "alias": null,
                                    "args": null,
                                    "kind": "ScalarField",
                                    "name": "changeType",
                                    "storageKey": null
                                  },
                                  (v26/*: any*/),
                                  {
                                    "alias": null,
                                    "args": null,
                                    "concreteType": null,
                                    "kind": "LinkedField",
                                    "name": "resource",
                                    "plural": false,
                                    "selections": [
                                      (v5/*: any*/),
                                      {
                                        "kind": "InlineFragment",
                                        "selections": (v19/*: any*/),
                                        "type": "Team",
                                        "abstractKey": null
                                      },
                                      {
                                        "kind": "InlineFragment",
                                        "selections": [
                                          (v10/*: any*/)
                                        ],
                                        "type": "User",
                                        "abstractKey": null
                                      },
                                      (v4/*: any*/)
                                    ],
                                    "storageKey": null
                                  }
                                ],
                                "type": "ActivityEventUpdateReleasePayload",
                                "abstractKey": null
                              }
                            ],
                            "storageKey": null
                          },
                          {
                            "alias": null,
                            "args": null,
                            "concreteType": "Project",
                            "kind": "LinkedField",
                            "name": "project",
                            "plural": false,
                            "selections": [
                              (v11/*: any*/),
                              {
                                "alias": null,
                                "args": null,
                                "kind": "ScalarField",
                                "name": "organizationName",
                                "storageKey": null
                              },
                              (v4/*: any*/)
                            ],
                            "storageKey": null
                          },
                          (v5/*: any*/)
                        ],
                        "storageKey": null
                      },
                      {
                        "alias": null,
                        "args": null,
                        "kind": "ScalarField",
                        "name": "cursor",
                        "storageKey": null
                      }
                    ],
                    "storageKey": null
                  },
                  {
                    "alias": null,
                    "args": null,
                    "concreteType": "PageInfo",
                    "kind": "LinkedField",
                    "name": "pageInfo",
                    "plural": false,
                    "selections": [
                      {
                        "alias": null,
                        "args": null,
                        "kind": "ScalarField",
                        "name": "endCursor",
                        "storageKey": null
                      },
                      {
                        "alias": null,
                        "args": null,
                        "kind": "ScalarField",
                        "name": "hasNextPage",
                        "storageKey": null
                      }
                    ],
                    "storageKey": null
                  }
                ],
                "storageKey": null
              },
              {
                "alias": null,
                "args": (v24/*: any*/),
                "filters": [
                  "sort"
                ],
                "handle": "connection",
                "key": "ReleaseActivityEventList_activityEvents",
                "kind": "LinkedHandle",
                "name": "activityEvents"
              },
              {
                "alias": null,
                "args": null,
                "kind": "ScalarField",
                "name": "notes",
                "storageKey": null
              },
              {
                "alias": null,
                "args": null,
                "kind": "ScalarField",
                "name": "completed",
                "storageKey": null
              },
              {
                "alias": null,
                "args": null,
                "kind": "ScalarField",
                "name": "preRelease",
                "storageKey": null
              },
              {
                "alias": null,
                "args": null,
                "kind": "ScalarField",
                "name": "latest",
                "storageKey": null
              },
              {
                "alias": null,
                "args": null,
                "concreteType": "Project",
                "kind": "LinkedField",
                "name": "project",
                "plural": false,
                "selections": [
                  (v4/*: any*/),
                  (v25/*: any*/)
                ],
                "storageKey": null
              }
            ],
            "type": "Release",
            "abstractKey": null
          }
        ],
        "storageKey": null
      }
    ]
  },
  "params": {
    "cacheID": "41a3ef576653193b969dbc436f51d70f",
    "id": null,
    "metadata": {},
    "name": "ReleaseDetailsContainerQuery",
    "operationKind": "query",
    "text": "query ReleaseDetailsContainerQuery(\n  $id: String!\n  $first: Int!\n  $after: String\n) {\n  node(id: $id) {\n    __typename\n    ... on Release {\n      id\n      ...ReleaseDetailsFragment_release\n    }\n    id\n  }\n}\n\nfragment ActivityEventListItemFragment_event on ActivityEvent {\n  metadata {\n    createdAt\n  }\n  id\n  initiator {\n    __typename\n    ... on User {\n      username\n      email\n    }\n    ... on ServiceAccount {\n      name\n    }\n    ... on Node {\n      __isNode: __typename\n      id\n    }\n  }\n}\n\nfragment ActivityEventPipelineTargetFragment_event on ActivityEvent {\n  action\n  target {\n    __typename\n    ... on Pipeline {\n      id\n      metadata {\n        prn\n      }\n      type\n      environmentName\n    }\n    id\n  }\n  payload {\n    __typename\n    ... on ActivityEventDeleteResourcePayload {\n      type\n    }\n    ... on ActivityEventUpdatePipelineNodePayload {\n      nodePath\n      nodeType\n    }\n    ... on ActivityEventApprovePipelineNodePayload {\n      nodePath\n      nodeType\n    }\n    ... on ActivityEventRevokeApprovalPipelineNodePayload {\n      nodePath\n      nodeType\n    }\n    ... on ActivityEventRetryPipelineNodePayload {\n      nodePath\n      nodeType\n    }\n    ... on ActivityEventSchedulePipelineNodePayload {\n      nodePath\n      nodeType\n      startTime\n    }\n    ... on ActivityEventUnschedulePipelineNodePayload {\n      nodePath\n      nodeType\n    }\n    ... on ActivityEventStartPipelineNodePayload {\n      nodePath\n      nodeType\n    }\n    ... on ActivityEventDeferPipelineNodePayload {\n      nodePath\n      nodeType\n      reason\n    }\n    ... on ActivityEventUndeferPipelineNodePayload {\n      nodePath\n      nodeType\n    }\n  }\n  ...ActivityEventListItemFragment_event\n}\n\nfragment ActivityEventReleaseTargetFragment_event on ActivityEvent {\n  project {\n    name\n    organizationName\n    id\n  }\n  target {\n    __typename\n    ... on Release {\n      id\n      semanticVersion\n      metadata {\n        prn\n      }\n    }\n    id\n  }\n  action\n  payload {\n    __typename\n    ... on ActivityEventUpdateReleasePayload {\n      changeType\n      type\n      resource {\n        __typename\n        ... on Team {\n          name\n        }\n        ... on User {\n          username\n        }\n        id\n      }\n    }\n    ... on ActivityEventDeleteResourcePayload {\n      type\n    }\n  }\n  ...ActivityEventListItemFragment_event\n}\n\nfragment CommentDetailsFragment_comment on Comment {\n  id\n  metadata {\n    createdAt\n    updatedAt\n  }\n  text\n  thread {\n    comments {\n      id\n    }\n    id\n  }\n  creator {\n    __typename\n    ... on User {\n      email\n      username\n    }\n    ... on ServiceAccount {\n      name\n    }\n    ... on Node {\n      __isNode: __typename\n      id\n    }\n  }\n  ...EditCommentFragment_comment\n}\n\nfragment EditCommentFragment_comment on Comment {\n  id\n  text\n}\n\nfragment ParticipantDialogFragment_participants on Release {\n  id\n  participants {\n    __typename\n    ... on User {\n      id\n      email\n      username\n    }\n    ... on Team {\n      id\n      name\n    }\n    ... on Node {\n      __isNode: __typename\n      id\n    }\n  }\n}\n\nfragment PipelineStageIcons_stages on Pipeline {\n  stages {\n    path\n    name\n    status\n  }\n}\n\nfragment ReleaseActivityEventListFragment_activityEvents on Release {\n  activityEvents(first: $first, after: $after, sort: CREATED_AT_DESC) {\n    edges {\n      node {\n        id\n        action\n        target {\n          __typename\n          id\n        }\n        ...ReleaseActivityEventThreadTarget_activityEvent\n        ...ActivityEventPipelineTargetFragment_event\n        ...ActivityEventReleaseTargetFragment_event\n        __typename\n      }\n      cursor\n    }\n    pageInfo {\n      endCursor\n      hasNextPage\n    }\n  }\n  id\n}\n\nfragment ReleaseActivityEventThreadTarget_activityEvent on ActivityEvent {\n  action\n  target {\n    __typename\n    ... on Thread {\n      ...ReleaseThreadListItemFragment_thread\n    }\n    id\n  }\n  ...ActivityEventListItemFragment_event\n}\n\nfragment ReleaseCommentDetailsFragment_comment on Comment {\n  id\n  thread {\n    id\n  }\n  ...CommentDetailsFragment_comment\n}\n\nfragment ReleaseDeploymentListItemFragment_pipeline on Pipeline {\n  metadata {\n    version\n  }\n  id\n  status\n  when\n  scheduledStartTime\n  cronSchedule {\n    expression\n    timezone\n  }\n  parentPipelineNodePath\n  approvals {\n    id\n    approver {\n      __typename\n      ... on User {\n        email\n      }\n      ... on ServiceAccount {\n        name\n      }\n      ... on Node {\n        __isNode: __typename\n        id\n      }\n    }\n  }\n  approvalStatus\n  approvalRules {\n    approvalsRequired\n    id\n  }\n  pipelineTemplate {\n    id\n    name\n    semanticVersion\n  }\n  parentPipeline {\n    id\n  }\n  timestamps {\n    startedAt\n    completedAt\n  }\n  environmentName\n  ...PipelineStageIcons_stages\n}\n\nfragment ReleaseDetailsFragment_release on Release {\n  ...ReleaseDetailsSidebarFragment_details\n  ...ReleaseVariablesFragment\n  ...ReleaseThreadListFragment_release\n  ...ReleaseActivityEventListFragment_activityEvents\n  id\n  notes\n  completed\n  semanticVersion\n  preRelease\n  latest\n  dueDate\n  metadata {\n    prn\n  }\n  project {\n    id\n    metadata {\n      prn\n    }\n  }\n  pipeline {\n    ...ReleaseLifecycleStagesFragment_stages\n    id\n    status\n    metadata {\n      version\n    }\n    pipelineTemplate {\n      id\n      name\n      versioned\n      semanticVersion\n      hclData\n    }\n  }\n}\n\nfragment ReleaseDetailsSidebarFragment_details on Release {\n  metadata {\n    createdAt\n  }\n  id\n  dueDate\n  semanticVersion\n  lifecyclePrn\n  participants {\n    __typename\n    ... on User {\n      id\n      email\n      username\n    }\n    ... on Team {\n      id\n      name\n    }\n    ... on Node {\n      __isNode: __typename\n      id\n    }\n  }\n  createdBy\n  pipeline {\n    id\n    stages {\n      name\n      path\n      status\n    }\n  }\n  ...ParticipantDialogFragment_participants\n}\n\nfragment ReleaseLifecycleStageFragment_stage on PipelineStage {\n  path\n  name\n  status\n  tasks {\n    path\n    ...ReleaseTaskListItemFragment_task\n  }\n  nestedPipelines {\n    path\n    name\n    status\n    environmentName\n    latestPipeline {\n      ...ReleaseDeploymentListItemFragment_pipeline\n      id\n    }\n  }\n}\n\nfragment ReleaseLifecycleStagesFragment_stages on Pipeline {\n  id\n  stages {\n    name\n    ...ReleaseLifecycleStageFragment_stage\n  }\n}\n\nfragment ReleaseTaskListItemFragment_task on PipelineTask {\n  path\n  name\n  status\n  when\n  currentJob {\n    id\n  }\n  approvals {\n    id\n    approver {\n      __typename\n      ... on User {\n        email\n      }\n      ... on ServiceAccount {\n        name\n      }\n      ... on Node {\n        __isNode: __typename\n        id\n      }\n    }\n  }\n  approvalStatus\n  approvalRules {\n    id\n    name\n    approvalsRequired\n  }\n}\n\nfragment ReleaseThreadListFragment_release on Release {\n  id\n}\n\nfragment ReleaseThreadListItemFragment_thread on Thread {\n  id\n  release {\n    id\n  }\n  comments {\n    id\n    ...ReleaseCommentDetailsFragment_comment\n  }\n}\n\nfragment ReleaseVariablesFragment on Release {\n  id\n  pipeline {\n    stages {\n      nestedPipelines {\n        environmentName\n        latestPipeline {\n          variables {\n            key\n            value\n            category\n          }\n          id\n        }\n      }\n    }\n    id\n  }\n}\n"
  }
};
})();

(node as any).hash = "d88d14e654102310f1369f5107358269";

export default node;
