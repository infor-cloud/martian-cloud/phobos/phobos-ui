/**
 * @generated SignedSource<<ccae987d862e54e23dbd52648538d37d>>
 * @lightSyntaxTransform
 * @nogrep
 */

/* tslint:disable */
/* eslint-disable */
// @ts-nocheck

import { ConcreteRequest, Mutation } from 'relay-runtime';
export type PipelineNodeType = "ACTION" | "PIPELINE" | "STAGE" | "TASK" | "%future added value";
export type ProblemType = "BAD_REQUEST" | "CONFLICT" | "FORBIDDEN" | "NOT_FOUND" | "%future added value";
export type CancelPipelineNodeScheduleInput = {
  clientMutationId?: string | null | undefined;
  nodePath: string;
  nodeType: PipelineNodeType;
  pipelineId: string;
};
export type ReleaseDeploymentListItemCancelScheduledDeploymentMutation$variables = {
  input: CancelPipelineNodeScheduleInput;
};
export type ReleaseDeploymentListItemCancelScheduledDeploymentMutation$data = {
  readonly cancelPipelineNodeSchedule: {
    readonly problems: ReadonlyArray<{
      readonly field: ReadonlyArray<string> | null | undefined;
      readonly message: string;
      readonly type: ProblemType;
    }>;
  };
};
export type ReleaseDeploymentListItemCancelScheduledDeploymentMutation = {
  response: ReleaseDeploymentListItemCancelScheduledDeploymentMutation$data;
  variables: ReleaseDeploymentListItemCancelScheduledDeploymentMutation$variables;
};

const node: ConcreteRequest = (function(){
var v0 = [
  {
    "defaultValue": null,
    "kind": "LocalArgument",
    "name": "input"
  }
],
v1 = [
  {
    "alias": null,
    "args": [
      {
        "kind": "Variable",
        "name": "input",
        "variableName": "input"
      }
    ],
    "concreteType": "PipelineMutationPayload",
    "kind": "LinkedField",
    "name": "cancelPipelineNodeSchedule",
    "plural": false,
    "selections": [
      {
        "alias": null,
        "args": null,
        "concreteType": "Problem",
        "kind": "LinkedField",
        "name": "problems",
        "plural": true,
        "selections": [
          {
            "alias": null,
            "args": null,
            "kind": "ScalarField",
            "name": "message",
            "storageKey": null
          },
          {
            "alias": null,
            "args": null,
            "kind": "ScalarField",
            "name": "field",
            "storageKey": null
          },
          {
            "alias": null,
            "args": null,
            "kind": "ScalarField",
            "name": "type",
            "storageKey": null
          }
        ],
        "storageKey": null
      }
    ],
    "storageKey": null
  }
];
return {
  "fragment": {
    "argumentDefinitions": (v0/*: any*/),
    "kind": "Fragment",
    "metadata": null,
    "name": "ReleaseDeploymentListItemCancelScheduledDeploymentMutation",
    "selections": (v1/*: any*/),
    "type": "Mutation",
    "abstractKey": null
  },
  "kind": "Request",
  "operation": {
    "argumentDefinitions": (v0/*: any*/),
    "kind": "Operation",
    "name": "ReleaseDeploymentListItemCancelScheduledDeploymentMutation",
    "selections": (v1/*: any*/)
  },
  "params": {
    "cacheID": "ef1269d403bf0c5c55f25d833e314733",
    "id": null,
    "metadata": {},
    "name": "ReleaseDeploymentListItemCancelScheduledDeploymentMutation",
    "operationKind": "mutation",
    "text": "mutation ReleaseDeploymentListItemCancelScheduledDeploymentMutation(\n  $input: CancelPipelineNodeScheduleInput!\n) {\n  cancelPipelineNodeSchedule(input: $input) {\n    problems {\n      message\n      field\n      type\n    }\n  }\n}\n"
  }
};
})();

(node as any).hash = "da21d7c018ca816a82f6b7441a71f583";

export default node;
