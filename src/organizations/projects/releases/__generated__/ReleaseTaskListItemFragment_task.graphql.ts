/**
 * @generated SignedSource<<a5caf736c1765c7a4d5afd0de894dccd>>
 * @lightSyntaxTransform
 * @nogrep
 */

/* tslint:disable */
/* eslint-disable */
// @ts-nocheck

import { Fragment, ReaderFragment } from 'relay-runtime';
export type PipelineApprovalStatus = "APPROVED" | "NOT_REQUIRED" | "PENDING" | "%future added value";
export type PipelineNodeStatus = "APPROVAL_PENDING" | "CANCELED" | "CANCELING" | "CREATED" | "DEFERRED" | "FAILED" | "PENDING" | "READY" | "RUNNING" | "SKIPPED" | "SUCCEEDED" | "WAITING" | "%future added value";
import { FragmentRefs } from "relay-runtime";
export type ReleaseTaskListItemFragment_task$data = {
  readonly approvalRules: ReadonlyArray<{
    readonly approvalsRequired: number;
    readonly id: string;
    readonly name: string;
  }>;
  readonly approvalStatus: PipelineApprovalStatus;
  readonly approvals: ReadonlyArray<{
    readonly approver: {
      readonly __typename: "ServiceAccount";
      readonly name: string;
    } | {
      readonly __typename: "User";
      readonly email: string;
    } | {
      // This will never be '%other', but we need some
      // value in case none of the concrete values match.
      readonly __typename: "%other";
    } | null | undefined;
    readonly id: string;
  }>;
  readonly currentJob: {
    readonly id: string;
  } | null | undefined;
  readonly name: string;
  readonly path: string;
  readonly status: PipelineNodeStatus;
  readonly when: string;
  readonly " $fragmentType": "ReleaseTaskListItemFragment_task";
};
export type ReleaseTaskListItemFragment_task$key = {
  readonly " $data"?: ReleaseTaskListItemFragment_task$data;
  readonly " $fragmentSpreads": FragmentRefs<"ReleaseTaskListItemFragment_task">;
};

const node: ReaderFragment = (function(){
var v0 = {
  "alias": null,
  "args": null,
  "kind": "ScalarField",
  "name": "name",
  "storageKey": null
},
v1 = {
  "alias": null,
  "args": null,
  "kind": "ScalarField",
  "name": "id",
  "storageKey": null
};
return {
  "argumentDefinitions": [],
  "kind": "Fragment",
  "metadata": null,
  "name": "ReleaseTaskListItemFragment_task",
  "selections": [
    {
      "alias": null,
      "args": null,
      "kind": "ScalarField",
      "name": "path",
      "storageKey": null
    },
    (v0/*: any*/),
    {
      "alias": null,
      "args": null,
      "kind": "ScalarField",
      "name": "status",
      "storageKey": null
    },
    {
      "alias": null,
      "args": null,
      "kind": "ScalarField",
      "name": "when",
      "storageKey": null
    },
    {
      "alias": null,
      "args": null,
      "concreteType": "Job",
      "kind": "LinkedField",
      "name": "currentJob",
      "plural": false,
      "selections": [
        (v1/*: any*/)
      ],
      "storageKey": null
    },
    {
      "alias": null,
      "args": null,
      "concreteType": "PipelineApproval",
      "kind": "LinkedField",
      "name": "approvals",
      "plural": true,
      "selections": [
        (v1/*: any*/),
        {
          "alias": null,
          "args": null,
          "concreteType": null,
          "kind": "LinkedField",
          "name": "approver",
          "plural": false,
          "selections": [
            {
              "alias": null,
              "args": null,
              "kind": "ScalarField",
              "name": "__typename",
              "storageKey": null
            },
            {
              "kind": "InlineFragment",
              "selections": [
                {
                  "alias": null,
                  "args": null,
                  "kind": "ScalarField",
                  "name": "email",
                  "storageKey": null
                }
              ],
              "type": "User",
              "abstractKey": null
            },
            {
              "kind": "InlineFragment",
              "selections": [
                (v0/*: any*/)
              ],
              "type": "ServiceAccount",
              "abstractKey": null
            }
          ],
          "storageKey": null
        }
      ],
      "storageKey": null
    },
    {
      "alias": null,
      "args": null,
      "kind": "ScalarField",
      "name": "approvalStatus",
      "storageKey": null
    },
    {
      "alias": null,
      "args": null,
      "concreteType": "ApprovalRule",
      "kind": "LinkedField",
      "name": "approvalRules",
      "plural": true,
      "selections": [
        (v1/*: any*/),
        (v0/*: any*/),
        {
          "alias": null,
          "args": null,
          "kind": "ScalarField",
          "name": "approvalsRequired",
          "storageKey": null
        }
      ],
      "storageKey": null
    }
  ],
  "type": "PipelineTask",
  "abstractKey": null
};
})();

(node as any).hash = "c9f19beb4d40b7514c2f483a0eab74b4";

export default node;
