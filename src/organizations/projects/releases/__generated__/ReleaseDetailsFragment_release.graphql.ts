/**
 * @generated SignedSource<<23f3d995817d94eaa4dc7135db05940f>>
 * @lightSyntaxTransform
 * @nogrep
 */

/* tslint:disable */
/* eslint-disable */
// @ts-nocheck

import { Fragment, ReaderFragment } from 'relay-runtime';
export type PipelineNodeStatus = "APPROVAL_PENDING" | "CANCELED" | "CANCELING" | "CREATED" | "DEFERRED" | "FAILED" | "PENDING" | "READY" | "RUNNING" | "SKIPPED" | "SUCCEEDED" | "WAITING" | "%future added value";
import { FragmentRefs } from "relay-runtime";
export type ReleaseDetailsFragment_release$data = {
  readonly completed: boolean;
  readonly dueDate: any | null | undefined;
  readonly id: string;
  readonly latest: boolean;
  readonly metadata: {
    readonly prn: string;
  };
  readonly notes: string;
  readonly pipeline: {
    readonly id: string;
    readonly metadata: {
      readonly version: string;
    };
    readonly pipelineTemplate: {
      readonly hclData: string;
      readonly id: string;
      readonly name: string | null | undefined;
      readonly semanticVersion: string | null | undefined;
      readonly versioned: boolean;
    };
    readonly status: PipelineNodeStatus;
    readonly " $fragmentSpreads": FragmentRefs<"ReleaseLifecycleStagesFragment_stages">;
  };
  readonly preRelease: boolean;
  readonly project: {
    readonly id: string;
    readonly metadata: {
      readonly prn: string;
    };
  };
  readonly semanticVersion: string;
  readonly " $fragmentSpreads": FragmentRefs<"ReleaseActivityEventListFragment_activityEvents" | "ReleaseDetailsSidebarFragment_details" | "ReleaseThreadListFragment_release" | "ReleaseVariablesFragment">;
  readonly " $fragmentType": "ReleaseDetailsFragment_release";
};
export type ReleaseDetailsFragment_release$key = {
  readonly " $data"?: ReleaseDetailsFragment_release$data;
  readonly " $fragmentSpreads": FragmentRefs<"ReleaseDetailsFragment_release">;
};

const node: ReaderFragment = (function(){
var v0 = {
  "alias": null,
  "args": null,
  "kind": "ScalarField",
  "name": "id",
  "storageKey": null
},
v1 = {
  "alias": null,
  "args": null,
  "kind": "ScalarField",
  "name": "semanticVersion",
  "storageKey": null
},
v2 = {
  "alias": null,
  "args": null,
  "concreteType": "ResourceMetadata",
  "kind": "LinkedField",
  "name": "metadata",
  "plural": false,
  "selections": [
    {
      "alias": null,
      "args": null,
      "kind": "ScalarField",
      "name": "prn",
      "storageKey": null
    }
  ],
  "storageKey": null
};
return {
  "argumentDefinitions": [],
  "kind": "Fragment",
  "metadata": null,
  "name": "ReleaseDetailsFragment_release",
  "selections": [
    {
      "args": null,
      "kind": "FragmentSpread",
      "name": "ReleaseDetailsSidebarFragment_details"
    },
    {
      "args": null,
      "kind": "FragmentSpread",
      "name": "ReleaseVariablesFragment"
    },
    {
      "args": null,
      "kind": "FragmentSpread",
      "name": "ReleaseThreadListFragment_release"
    },
    {
      "args": null,
      "kind": "FragmentSpread",
      "name": "ReleaseActivityEventListFragment_activityEvents"
    },
    (v0/*: any*/),
    {
      "alias": null,
      "args": null,
      "kind": "ScalarField",
      "name": "notes",
      "storageKey": null
    },
    {
      "alias": null,
      "args": null,
      "kind": "ScalarField",
      "name": "completed",
      "storageKey": null
    },
    (v1/*: any*/),
    {
      "alias": null,
      "args": null,
      "kind": "ScalarField",
      "name": "preRelease",
      "storageKey": null
    },
    {
      "alias": null,
      "args": null,
      "kind": "ScalarField",
      "name": "latest",
      "storageKey": null
    },
    {
      "alias": null,
      "args": null,
      "kind": "ScalarField",
      "name": "dueDate",
      "storageKey": null
    },
    (v2/*: any*/),
    {
      "alias": null,
      "args": null,
      "concreteType": "Project",
      "kind": "LinkedField",
      "name": "project",
      "plural": false,
      "selections": [
        (v0/*: any*/),
        (v2/*: any*/)
      ],
      "storageKey": null
    },
    {
      "alias": null,
      "args": null,
      "concreteType": "Pipeline",
      "kind": "LinkedField",
      "name": "pipeline",
      "plural": false,
      "selections": [
        {
          "args": null,
          "kind": "FragmentSpread",
          "name": "ReleaseLifecycleStagesFragment_stages"
        },
        (v0/*: any*/),
        {
          "alias": null,
          "args": null,
          "kind": "ScalarField",
          "name": "status",
          "storageKey": null
        },
        {
          "alias": null,
          "args": null,
          "concreteType": "ResourceMetadata",
          "kind": "LinkedField",
          "name": "metadata",
          "plural": false,
          "selections": [
            {
              "alias": null,
              "args": null,
              "kind": "ScalarField",
              "name": "version",
              "storageKey": null
            }
          ],
          "storageKey": null
        },
        {
          "alias": null,
          "args": null,
          "concreteType": "PipelineTemplate",
          "kind": "LinkedField",
          "name": "pipelineTemplate",
          "plural": false,
          "selections": [
            (v0/*: any*/),
            {
              "alias": null,
              "args": null,
              "kind": "ScalarField",
              "name": "name",
              "storageKey": null
            },
            {
              "alias": null,
              "args": null,
              "kind": "ScalarField",
              "name": "versioned",
              "storageKey": null
            },
            (v1/*: any*/),
            {
              "alias": null,
              "args": null,
              "kind": "ScalarField",
              "name": "hclData",
              "storageKey": null
            }
          ],
          "storageKey": null
        }
      ],
      "storageKey": null
    }
  ],
  "type": "Release",
  "abstractKey": null
};
})();

(node as any).hash = "8315745a013d926f43ac7821e6aec585";

export default node;
