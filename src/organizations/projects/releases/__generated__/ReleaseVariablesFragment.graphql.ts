/**
 * @generated SignedSource<<0498f76bcb02d3fb105cd8b32383aac2>>
 * @lightSyntaxTransform
 * @nogrep
 */

/* tslint:disable */
/* eslint-disable */
// @ts-nocheck

import { Fragment, ReaderFragment } from 'relay-runtime';
export type VariableCategory = "ENVIRONMENT" | "HCL" | "%future added value";
import { FragmentRefs } from "relay-runtime";
export type ReleaseVariablesFragment$data = {
  readonly id: string;
  readonly pipeline: {
    readonly stages: ReadonlyArray<{
      readonly nestedPipelines: ReadonlyArray<{
        readonly environmentName: string | null | undefined;
        readonly latestPipeline: {
          readonly variables: ReadonlyArray<{
            readonly category: VariableCategory;
            readonly key: string;
            readonly value: string;
          }>;
        };
      }>;
    }>;
  };
  readonly " $fragmentType": "ReleaseVariablesFragment";
};
export type ReleaseVariablesFragment$key = {
  readonly " $data"?: ReleaseVariablesFragment$data;
  readonly " $fragmentSpreads": FragmentRefs<"ReleaseVariablesFragment">;
};

const node: ReaderFragment = {
  "argumentDefinitions": [],
  "kind": "Fragment",
  "metadata": null,
  "name": "ReleaseVariablesFragment",
  "selections": [
    {
      "alias": null,
      "args": null,
      "kind": "ScalarField",
      "name": "id",
      "storageKey": null
    },
    {
      "alias": null,
      "args": null,
      "concreteType": "Pipeline",
      "kind": "LinkedField",
      "name": "pipeline",
      "plural": false,
      "selections": [
        {
          "alias": null,
          "args": null,
          "concreteType": "PipelineStage",
          "kind": "LinkedField",
          "name": "stages",
          "plural": true,
          "selections": [
            {
              "alias": null,
              "args": null,
              "concreteType": "NestedPipeline",
              "kind": "LinkedField",
              "name": "nestedPipelines",
              "plural": true,
              "selections": [
                {
                  "alias": null,
                  "args": null,
                  "kind": "ScalarField",
                  "name": "environmentName",
                  "storageKey": null
                },
                {
                  "alias": null,
                  "args": null,
                  "concreteType": "Pipeline",
                  "kind": "LinkedField",
                  "name": "latestPipeline",
                  "plural": false,
                  "selections": [
                    {
                      "alias": null,
                      "args": null,
                      "concreteType": "PipelineVariable",
                      "kind": "LinkedField",
                      "name": "variables",
                      "plural": true,
                      "selections": [
                        {
                          "alias": null,
                          "args": null,
                          "kind": "ScalarField",
                          "name": "key",
                          "storageKey": null
                        },
                        {
                          "alias": null,
                          "args": null,
                          "kind": "ScalarField",
                          "name": "value",
                          "storageKey": null
                        },
                        {
                          "alias": null,
                          "args": null,
                          "kind": "ScalarField",
                          "name": "category",
                          "storageKey": null
                        }
                      ],
                      "storageKey": null
                    }
                  ],
                  "storageKey": null
                }
              ],
              "storageKey": null
            }
          ],
          "storageKey": null
        }
      ],
      "storageKey": null
    }
  ],
  "type": "Release",
  "abstractKey": null
};

(node as any).hash = "baf153e220111bf31e7c7ef20d670675";

export default node;
