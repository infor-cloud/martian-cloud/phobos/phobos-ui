/**
 * @generated SignedSource<<14c511d41f00f2edf6e76b4b3a688fcd>>
 * @lightSyntaxTransform
 * @nogrep
 */

/* tslint:disable */
/* eslint-disable */
// @ts-nocheck

import { ConcreteRequest, GraphQLSubscription } from 'relay-runtime';
import { FragmentRefs } from "relay-runtime";
export type ActivityEventsSubscriptionInput = {
  organizationId?: string | null | undefined;
  projectId?: string | null | undefined;
};
export type ReleaseDetailsLiveActivitySubscription$variables = {
  input: ActivityEventsSubscriptionInput;
};
export type ReleaseDetailsLiveActivitySubscription$data = {
  readonly liveActivityEvents: {
    readonly id: string;
    readonly target: {
      readonly __typename: "Pipeline";
      readonly releaseType: {
        readonly id: string;
      } | null | undefined;
    } | {
      readonly __typename: "Release";
      readonly id: string;
    } | {
      readonly __typename: "Thread";
      readonly release: {
        readonly id: string;
      } | null | undefined;
    } | {
      // This will never be '%other', but we need some
      // value in case none of the concrete values match.
      readonly __typename: "%other";
    } | null | undefined;
    readonly " $fragmentSpreads": FragmentRefs<"ActivityEventPipelineTargetFragment_event" | "ActivityEventReleaseTargetFragment_event">;
  };
};
export type ReleaseDetailsLiveActivitySubscription = {
  response: ReleaseDetailsLiveActivitySubscription$data;
  variables: ReleaseDetailsLiveActivitySubscription$variables;
};

const node: ConcreteRequest = (function(){
var v0 = [
  {
    "defaultValue": null,
    "kind": "LocalArgument",
    "name": "input"
  }
],
v1 = [
  {
    "kind": "Variable",
    "name": "input",
    "variableName": "input"
  }
],
v2 = {
  "alias": null,
  "args": null,
  "kind": "ScalarField",
  "name": "id",
  "storageKey": null
},
v3 = {
  "alias": null,
  "args": null,
  "kind": "ScalarField",
  "name": "__typename",
  "storageKey": null
},
v4 = [
  (v2/*: any*/)
],
v5 = {
  "alias": "releaseType",
  "args": null,
  "concreteType": "Release",
  "kind": "LinkedField",
  "name": "release",
  "plural": false,
  "selections": (v4/*: any*/),
  "storageKey": null
},
v6 = {
  "kind": "InlineFragment",
  "selections": [
    {
      "alias": null,
      "args": null,
      "concreteType": "Release",
      "kind": "LinkedField",
      "name": "release",
      "plural": false,
      "selections": (v4/*: any*/),
      "storageKey": null
    }
  ],
  "type": "Thread",
  "abstractKey": null
},
v7 = {
  "alias": null,
  "args": null,
  "concreteType": "ResourceMetadata",
  "kind": "LinkedField",
  "name": "metadata",
  "plural": false,
  "selections": [
    {
      "alias": null,
      "args": null,
      "kind": "ScalarField",
      "name": "prn",
      "storageKey": null
    }
  ],
  "storageKey": null
},
v8 = {
  "alias": null,
  "args": null,
  "kind": "ScalarField",
  "name": "type",
  "storageKey": null
},
v9 = {
  "alias": null,
  "args": null,
  "kind": "ScalarField",
  "name": "name",
  "storageKey": null
},
v10 = [
  (v9/*: any*/)
],
v11 = {
  "alias": null,
  "args": null,
  "kind": "ScalarField",
  "name": "username",
  "storageKey": null
},
v12 = {
  "alias": null,
  "args": null,
  "kind": "ScalarField",
  "name": "nodePath",
  "storageKey": null
},
v13 = {
  "alias": null,
  "args": null,
  "kind": "ScalarField",
  "name": "nodeType",
  "storageKey": null
},
v14 = [
  (v12/*: any*/),
  (v13/*: any*/)
];
return {
  "fragment": {
    "argumentDefinitions": (v0/*: any*/),
    "kind": "Fragment",
    "metadata": null,
    "name": "ReleaseDetailsLiveActivitySubscription",
    "selections": [
      {
        "alias": null,
        "args": (v1/*: any*/),
        "concreteType": "ActivityEvent",
        "kind": "LinkedField",
        "name": "liveActivityEvents",
        "plural": false,
        "selections": [
          (v2/*: any*/),
          {
            "alias": null,
            "args": null,
            "concreteType": null,
            "kind": "LinkedField",
            "name": "target",
            "plural": false,
            "selections": [
              (v3/*: any*/),
              {
                "kind": "InlineFragment",
                "selections": (v4/*: any*/),
                "type": "Release",
                "abstractKey": null
              },
              {
                "kind": "InlineFragment",
                "selections": [
                  (v5/*: any*/)
                ],
                "type": "Pipeline",
                "abstractKey": null
              },
              (v6/*: any*/)
            ],
            "storageKey": null
          },
          {
            "args": null,
            "kind": "FragmentSpread",
            "name": "ActivityEventReleaseTargetFragment_event"
          },
          {
            "args": null,
            "kind": "FragmentSpread",
            "name": "ActivityEventPipelineTargetFragment_event"
          }
        ],
        "storageKey": null
      }
    ],
    "type": "Subscription",
    "abstractKey": null
  },
  "kind": "Request",
  "operation": {
    "argumentDefinitions": (v0/*: any*/),
    "kind": "Operation",
    "name": "ReleaseDetailsLiveActivitySubscription",
    "selections": [
      {
        "alias": null,
        "args": (v1/*: any*/),
        "concreteType": "ActivityEvent",
        "kind": "LinkedField",
        "name": "liveActivityEvents",
        "plural": false,
        "selections": [
          (v2/*: any*/),
          {
            "alias": null,
            "args": null,
            "concreteType": null,
            "kind": "LinkedField",
            "name": "target",
            "plural": false,
            "selections": [
              (v3/*: any*/),
              (v2/*: any*/),
              {
                "kind": "InlineFragment",
                "selections": [
                  {
                    "alias": null,
                    "args": null,
                    "kind": "ScalarField",
                    "name": "semanticVersion",
                    "storageKey": null
                  },
                  (v7/*: any*/)
                ],
                "type": "Release",
                "abstractKey": null
              },
              {
                "kind": "InlineFragment",
                "selections": [
                  (v5/*: any*/),
                  (v7/*: any*/),
                  (v8/*: any*/),
                  {
                    "alias": null,
                    "args": null,
                    "kind": "ScalarField",
                    "name": "environmentName",
                    "storageKey": null
                  }
                ],
                "type": "Pipeline",
                "abstractKey": null
              },
              (v6/*: any*/)
            ],
            "storageKey": null
          },
          {
            "alias": null,
            "args": null,
            "concreteType": "Project",
            "kind": "LinkedField",
            "name": "project",
            "plural": false,
            "selections": [
              (v9/*: any*/),
              {
                "alias": null,
                "args": null,
                "kind": "ScalarField",
                "name": "organizationName",
                "storageKey": null
              },
              (v2/*: any*/)
            ],
            "storageKey": null
          },
          {
            "alias": null,
            "args": null,
            "kind": "ScalarField",
            "name": "action",
            "storageKey": null
          },
          {
            "alias": null,
            "args": null,
            "concreteType": null,
            "kind": "LinkedField",
            "name": "payload",
            "plural": false,
            "selections": [
              (v3/*: any*/),
              {
                "kind": "InlineFragment",
                "selections": [
                  {
                    "alias": null,
                    "args": null,
                    "kind": "ScalarField",
                    "name": "changeType",
                    "storageKey": null
                  },
                  (v8/*: any*/),
                  {
                    "alias": null,
                    "args": null,
                    "concreteType": null,
                    "kind": "LinkedField",
                    "name": "resource",
                    "plural": false,
                    "selections": [
                      (v3/*: any*/),
                      {
                        "kind": "InlineFragment",
                        "selections": (v10/*: any*/),
                        "type": "Team",
                        "abstractKey": null
                      },
                      {
                        "kind": "InlineFragment",
                        "selections": [
                          (v11/*: any*/)
                        ],
                        "type": "User",
                        "abstractKey": null
                      },
                      (v2/*: any*/)
                    ],
                    "storageKey": null
                  }
                ],
                "type": "ActivityEventUpdateReleasePayload",
                "abstractKey": null
              },
              {
                "kind": "InlineFragment",
                "selections": [
                  (v8/*: any*/)
                ],
                "type": "ActivityEventDeleteResourcePayload",
                "abstractKey": null
              },
              {
                "kind": "InlineFragment",
                "selections": (v14/*: any*/),
                "type": "ActivityEventUpdatePipelineNodePayload",
                "abstractKey": null
              },
              {
                "kind": "InlineFragment",
                "selections": (v14/*: any*/),
                "type": "ActivityEventApprovePipelineNodePayload",
                "abstractKey": null
              },
              {
                "kind": "InlineFragment",
                "selections": (v14/*: any*/),
                "type": "ActivityEventRevokeApprovalPipelineNodePayload",
                "abstractKey": null
              },
              {
                "kind": "InlineFragment",
                "selections": (v14/*: any*/),
                "type": "ActivityEventRetryPipelineNodePayload",
                "abstractKey": null
              },
              {
                "kind": "InlineFragment",
                "selections": [
                  (v12/*: any*/),
                  (v13/*: any*/),
                  {
                    "alias": null,
                    "args": null,
                    "kind": "ScalarField",
                    "name": "startTime",
                    "storageKey": null
                  }
                ],
                "type": "ActivityEventSchedulePipelineNodePayload",
                "abstractKey": null
              },
              {
                "kind": "InlineFragment",
                "selections": (v14/*: any*/),
                "type": "ActivityEventUnschedulePipelineNodePayload",
                "abstractKey": null
              },
              {
                "kind": "InlineFragment",
                "selections": (v14/*: any*/),
                "type": "ActivityEventStartPipelineNodePayload",
                "abstractKey": null
              },
              {
                "kind": "InlineFragment",
                "selections": [
                  (v12/*: any*/),
                  (v13/*: any*/),
                  {
                    "alias": null,
                    "args": null,
                    "kind": "ScalarField",
                    "name": "reason",
                    "storageKey": null
                  }
                ],
                "type": "ActivityEventDeferPipelineNodePayload",
                "abstractKey": null
              },
              {
                "kind": "InlineFragment",
                "selections": (v14/*: any*/),
                "type": "ActivityEventUndeferPipelineNodePayload",
                "abstractKey": null
              }
            ],
            "storageKey": null
          },
          {
            "alias": null,
            "args": null,
            "concreteType": "ResourceMetadata",
            "kind": "LinkedField",
            "name": "metadata",
            "plural": false,
            "selections": [
              {
                "alias": null,
                "args": null,
                "kind": "ScalarField",
                "name": "createdAt",
                "storageKey": null
              }
            ],
            "storageKey": null
          },
          {
            "alias": null,
            "args": null,
            "concreteType": null,
            "kind": "LinkedField",
            "name": "initiator",
            "plural": false,
            "selections": [
              (v3/*: any*/),
              {
                "kind": "InlineFragment",
                "selections": [
                  (v11/*: any*/),
                  {
                    "alias": null,
                    "args": null,
                    "kind": "ScalarField",
                    "name": "email",
                    "storageKey": null
                  }
                ],
                "type": "User",
                "abstractKey": null
              },
              {
                "kind": "InlineFragment",
                "selections": (v10/*: any*/),
                "type": "ServiceAccount",
                "abstractKey": null
              },
              {
                "kind": "InlineFragment",
                "selections": (v4/*: any*/),
                "type": "Node",
                "abstractKey": "__isNode"
              }
            ],
            "storageKey": null
          }
        ],
        "storageKey": null
      }
    ]
  },
  "params": {
    "cacheID": "ad85296d7ced408764d4783bbaeb8a67",
    "id": null,
    "metadata": {},
    "name": "ReleaseDetailsLiveActivitySubscription",
    "operationKind": "subscription",
    "text": "subscription ReleaseDetailsLiveActivitySubscription(\n  $input: ActivityEventsSubscriptionInput!\n) {\n  liveActivityEvents(input: $input) {\n    id\n    target {\n      __typename\n      ... on Release {\n        id\n      }\n      ... on Pipeline {\n        releaseType: release {\n          id\n        }\n      }\n      ... on Thread {\n        release {\n          id\n        }\n      }\n      id\n    }\n    ...ActivityEventReleaseTargetFragment_event\n    ...ActivityEventPipelineTargetFragment_event\n  }\n}\n\nfragment ActivityEventListItemFragment_event on ActivityEvent {\n  metadata {\n    createdAt\n  }\n  id\n  initiator {\n    __typename\n    ... on User {\n      username\n      email\n    }\n    ... on ServiceAccount {\n      name\n    }\n    ... on Node {\n      __isNode: __typename\n      id\n    }\n  }\n}\n\nfragment ActivityEventPipelineTargetFragment_event on ActivityEvent {\n  action\n  target {\n    __typename\n    ... on Pipeline {\n      id\n      metadata {\n        prn\n      }\n      type\n      environmentName\n    }\n    id\n  }\n  payload {\n    __typename\n    ... on ActivityEventDeleteResourcePayload {\n      type\n    }\n    ... on ActivityEventUpdatePipelineNodePayload {\n      nodePath\n      nodeType\n    }\n    ... on ActivityEventApprovePipelineNodePayload {\n      nodePath\n      nodeType\n    }\n    ... on ActivityEventRevokeApprovalPipelineNodePayload {\n      nodePath\n      nodeType\n    }\n    ... on ActivityEventRetryPipelineNodePayload {\n      nodePath\n      nodeType\n    }\n    ... on ActivityEventSchedulePipelineNodePayload {\n      nodePath\n      nodeType\n      startTime\n    }\n    ... on ActivityEventUnschedulePipelineNodePayload {\n      nodePath\n      nodeType\n    }\n    ... on ActivityEventStartPipelineNodePayload {\n      nodePath\n      nodeType\n    }\n    ... on ActivityEventDeferPipelineNodePayload {\n      nodePath\n      nodeType\n      reason\n    }\n    ... on ActivityEventUndeferPipelineNodePayload {\n      nodePath\n      nodeType\n    }\n  }\n  ...ActivityEventListItemFragment_event\n}\n\nfragment ActivityEventReleaseTargetFragment_event on ActivityEvent {\n  project {\n    name\n    organizationName\n    id\n  }\n  target {\n    __typename\n    ... on Release {\n      id\n      semanticVersion\n      metadata {\n        prn\n      }\n    }\n    id\n  }\n  action\n  payload {\n    __typename\n    ... on ActivityEventUpdateReleasePayload {\n      changeType\n      type\n      resource {\n        __typename\n        ... on Team {\n          name\n        }\n        ... on User {\n          username\n        }\n        id\n      }\n    }\n    ... on ActivityEventDeleteResourcePayload {\n      type\n    }\n  }\n  ...ActivityEventListItemFragment_event\n}\n"
  }
};
})();

(node as any).hash = "846bcb56aa21065412ce50b2276e05fc";

export default node;
