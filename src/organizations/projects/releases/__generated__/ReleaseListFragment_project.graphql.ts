/**
 * @generated SignedSource<<165ce007970f1b3bae8cdb505d12cc87>>
 * @lightSyntaxTransform
 * @nogrep
 */

/* tslint:disable */
/* eslint-disable */
// @ts-nocheck

import { Fragment, ReaderFragment } from 'relay-runtime';
import { FragmentRefs } from "relay-runtime";
export type ReleaseListFragment_project$data = {
  readonly id: string;
  readonly " $fragmentType": "ReleaseListFragment_project";
};
export type ReleaseListFragment_project$key = {
  readonly " $data"?: ReleaseListFragment_project$data;
  readonly " $fragmentSpreads": FragmentRefs<"ReleaseListFragment_project">;
};

const node: ReaderFragment = {
  "argumentDefinitions": [],
  "kind": "Fragment",
  "metadata": null,
  "name": "ReleaseListFragment_project",
  "selections": [
    {
      "alias": null,
      "args": null,
      "kind": "ScalarField",
      "name": "id",
      "storageKey": null
    }
  ],
  "type": "Project",
  "abstractKey": null
};

(node as any).hash = "0c125e70cbdc7eae96f2f7208f78eec8";

export default node;
