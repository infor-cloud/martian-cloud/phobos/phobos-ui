import { Box, List, Paper, Typography } from '@mui/material';
import graphql from 'babel-plugin-relay/macro';
import InfiniteScroll from 'react-infinite-scroll-component';
import { usePaginationFragment } from 'react-relay/hooks';
import ActivityEventPipelineTarget from '../../../../activity/targets/ActivityEventPipelineTarget';
import ActivityEventReleaseTarget from '../../../../activity/targets/ActivityEventReleaseTarget';
import ListSkeleton from '../../../../skeletons/ListSkeleton';
import ReleaseActivityEventThreadTarget from './ReleaseActivityEventThreadTarget';
import { ReleaseActivityEventListFragment_activityEvents$key } from './__generated__/ReleaseActivityEventListFragment_activityEvents.graphql';
import { ReleaseActivityEventListPaginationQuery } from './__generated__/ReleaseActivityEventListPaginationQuery.graphql';

const TARGET_COMPONENT_MAP = {
    Thread: ReleaseActivityEventThreadTarget,
    Pipeline: ActivityEventPipelineTarget,
    Release: ActivityEventReleaseTarget
} as any;

interface Props {
    fragmentRef: ReleaseActivityEventListFragment_activityEvents$key
}

function ReleaseActivityEventList({ fragmentRef }: Props) {

    const { data, loadNext, hasNext } = usePaginationFragment<ReleaseActivityEventListPaginationQuery, ReleaseActivityEventListFragment_activityEvents$key>(
        graphql`
            fragment ReleaseActivityEventListFragment_activityEvents on Release
            @refetchable(queryName: "ReleaseActivityEventListPaginationQuery"){
                activityEvents(
                    first: $first
                    after: $after
                    sort: CREATED_AT_DESC
                ) @connection(key: "ReleaseActivityEventList_activityEvents") {
                    edges {
                        node {
                            id
                            action
                            target {
                                __typename
                            }
                            ...ReleaseActivityEventThreadTarget_activityEvent
                            ...ActivityEventPipelineTargetFragment_event
                            ...ActivityEventReleaseTargetFragment_event
                        }
                    }
                }
            }
        `, fragmentRef);

    const eventCount = data.activityEvents?.edges?.length || 0;

    return (
        <Box>
            {eventCount === 0 ?
                <Paper variant="outlined" sx={{ display: "flex", justifyContent: "center" }}>
                    <Box
                        sx={{ p: 4, display: "flex", flexDirection: "column", justifyContent: "center", alignItems: "center" }}>
                        <Typography color="textSecondary" align="center">
                            No activity events for this release
                        </Typography>
                    </Box>
                </Paper>
                :
                <InfiniteScroll
                    dataLength={eventCount}
                    next={() => loadNext(20)}
                    hasMore={hasNext}
                    loader={<ListSkeleton rowCount={3} />}
                >
                    <List sx={{ paddingTop: 0 }}>
                        {data.activityEvents?.edges?.map((edge: any) => {
                            const Target = TARGET_COMPONENT_MAP[edge.node.target?.__typename];
                            return Target ?
                                <Target
                                    key={edge.node.id}
                                    fragmentRef={edge.node}
                                    showReleaseLink={false}
                                /> : null
                        })}
                    </List>
                </InfiniteScroll>
            }
        </Box>
    );
}

export default ReleaseActivityEventList
