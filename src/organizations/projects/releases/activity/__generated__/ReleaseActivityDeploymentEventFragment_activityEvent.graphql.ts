/**
 * @generated SignedSource<<307a9e83e426712981a8025c592207ae>>
 * @lightSyntaxTransform
 * @nogrep
 */

/* tslint:disable */
/* eslint-disable */
// @ts-nocheck

import { Fragment, ReaderFragment } from 'relay-runtime';
export type ActivityEventAction = "ADD_PARTICIPANT" | "CANCEL" | "CREATE" | "CREATE_MEMBERSHIP" | "DELETE" | "REMOVE_MEMBERSHIP" | "REMOVE_PARTICIPANT" | "RETRY" | "START" | "UPDATE" | "%future added value";
import { FragmentRefs } from "relay-runtime";
export type ReleaseActivityDeploymentEventFragment_activityEvent$data = {
  readonly action: ActivityEventAction;
  readonly target: {
    readonly environment?: {
      readonly id: string;
      readonly name: string;
    };
    readonly pipeline?: {
      readonly id: string;
    };
    readonly project?: {
      readonly name: string;
      readonly organizationName: string;
    };
  } | null | undefined;
  readonly " $fragmentSpreads": FragmentRefs<"ActivityEventListItemFragment_event">;
  readonly " $fragmentType": "ReleaseActivityDeploymentEventFragment_activityEvent";
};
export type ReleaseActivityDeploymentEventFragment_activityEvent$key = {
  readonly " $data"?: ReleaseActivityDeploymentEventFragment_activityEvent$data;
  readonly " $fragmentSpreads": FragmentRefs<"ReleaseActivityDeploymentEventFragment_activityEvent">;
};

const node: ReaderFragment = (function(){
var v0 = {
  "alias": null,
  "args": null,
  "kind": "ScalarField",
  "name": "id",
  "storageKey": null
},
v1 = {
  "alias": null,
  "args": null,
  "kind": "ScalarField",
  "name": "name",
  "storageKey": null
};
return {
  "argumentDefinitions": [],
  "kind": "Fragment",
  "metadata": null,
  "name": "ReleaseActivityDeploymentEventFragment_activityEvent",
  "selections": [
    {
      "alias": null,
      "args": null,
      "kind": "ScalarField",
      "name": "action",
      "storageKey": null
    },
    {
      "alias": null,
      "args": null,
      "concreteType": null,
      "kind": "LinkedField",
      "name": "target",
      "plural": false,
      "selections": [
        {
          "kind": "InlineFragment",
          "selections": [
            {
              "alias": null,
              "args": null,
              "concreteType": "Pipeline",
              "kind": "LinkedField",
              "name": "pipeline",
              "plural": false,
              "selections": [
                (v0/*: any*/)
              ],
              "storageKey": null
            },
            {
              "alias": null,
              "args": null,
              "concreteType": "Project",
              "kind": "LinkedField",
              "name": "project",
              "plural": false,
              "selections": [
                (v1/*: any*/),
                {
                  "alias": null,
                  "args": null,
                  "kind": "ScalarField",
                  "name": "organizationName",
                  "storageKey": null
                }
              ],
              "storageKey": null
            },
            {
              "alias": null,
              "args": null,
              "concreteType": "Environment",
              "kind": "LinkedField",
              "name": "environment",
              "plural": false,
              "selections": [
                (v0/*: any*/),
                (v1/*: any*/)
              ],
              "storageKey": null
            }
          ],
          "type": "Deployment",
          "abstractKey": null
        }
      ],
      "storageKey": null
    },
    {
      "args": null,
      "kind": "FragmentSpread",
      "name": "ActivityEventListItemFragment_event"
    }
  ],
  "type": "ActivityEvent",
  "abstractKey": null
};
})();

(node as any).hash = "c6c6dfa49cc6231dbb3be6afdbe6b93b";

export default node;
