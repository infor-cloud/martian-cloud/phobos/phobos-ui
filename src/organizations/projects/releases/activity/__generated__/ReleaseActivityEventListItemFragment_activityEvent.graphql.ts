/**
 * @generated SignedSource<<fae8899642a33edd7538276a7bdc1b5a>>
 * @lightSyntaxTransform
 * @nogrep
 */

/* tslint:disable */
/* eslint-disable */
// @ts-nocheck

import { Fragment, ReaderFragment } from 'relay-runtime';
export type ActivityEventAction = "ADD_PARTICIPANT" | "CANCEL" | "CREATE" | "CREATE_MEMBERSHIP" | "DELETE" | "REMOVE_MEMBERSHIP" | "REMOVE_PARTICIPANT" | "RETRY" | "START" | "UPDATE" | "%future added value";
export type ActivityEventTargetType = "AGENT" | "APPROVAL_RULE" | "COMMENT" | "DEPLOYMENT" | "ENVIRONMENT" | "GLOBAL" | "LIFECYCLE_TEMPLATE" | "MEMBERSHIP" | "ORGANIZATION" | "PIPELINE" | "PIPELINE_TEMPLATE" | "PLUGIN" | "PLUGIN_VERSION" | "PROJECT" | "RELEASE" | "RELEASE_LIFECYCLE" | "ROLE" | "SERVICE_ACCOUNT" | "TEAM" | "USER" | "%future added value";
import { FragmentRefs } from "relay-runtime";
export type ReleaseActivityEventListItemFragment_activityEvent$data = {
  readonly action: ActivityEventAction;
  readonly initiator: {
    readonly __typename: "ServiceAccount";
    readonly name: string;
  } | {
    readonly __typename: "User";
    readonly email: string;
    readonly username: string;
  } | {
    // This will never be '%other', but we need some
    // value in case none of the concrete values match.
    readonly __typename: "%other";
  };
  readonly metadata: {
    readonly createdAt: any;
  };
  readonly payload: {
    readonly __typename: "ActivityEventUpdateReleasePayload";
    readonly changeType: string;
    readonly resource: {
      readonly name?: string;
      readonly username?: string;
    } | null | undefined;
    readonly type: string;
  } | {
    // This will never be '%other', but we need some
    // value in case none of the concrete values match.
    readonly __typename: "%other";
  } | null | undefined;
  readonly target: {
    readonly semanticVersion?: string;
  } | null | undefined;
  readonly targetType: ActivityEventTargetType;
  readonly " $fragmentSpreads": FragmentRefs<"ReleaseActivityCommentEventFragment_activityEvent" | "ReleaseActivityDeploymentEventFragment_activityEvent">;
  readonly " $fragmentType": "ReleaseActivityEventListItemFragment_activityEvent";
};
export type ReleaseActivityEventListItemFragment_activityEvent$key = {
  readonly " $data"?: ReleaseActivityEventListItemFragment_activityEvent$data;
  readonly " $fragmentSpreads": FragmentRefs<"ReleaseActivityEventListItemFragment_activityEvent">;
};

const node: ReaderFragment = (function(){
var v0 = {
  "alias": null,
  "args": null,
  "kind": "ScalarField",
  "name": "__typename",
  "storageKey": null
},
v1 = {
  "alias": null,
  "args": null,
  "kind": "ScalarField",
  "name": "username",
  "storageKey": null
},
v2 = [
  {
    "alias": null,
    "args": null,
    "kind": "ScalarField",
    "name": "name",
    "storageKey": null
  }
];
return {
  "argumentDefinitions": [],
  "kind": "Fragment",
  "metadata": null,
  "name": "ReleaseActivityEventListItemFragment_activityEvent",
  "selections": [
    {
      "alias": null,
      "args": null,
      "concreteType": null,
      "kind": "LinkedField",
      "name": "initiator",
      "plural": false,
      "selections": [
        (v0/*: any*/),
        {
          "kind": "InlineFragment",
          "selections": [
            (v1/*: any*/),
            {
              "alias": null,
              "args": null,
              "kind": "ScalarField",
              "name": "email",
              "storageKey": null
            }
          ],
          "type": "User",
          "abstractKey": null
        },
        {
          "kind": "InlineFragment",
          "selections": (v2/*: any*/),
          "type": "ServiceAccount",
          "abstractKey": null
        }
      ],
      "storageKey": null
    },
    {
      "alias": null,
      "args": null,
      "concreteType": "ResourceMetadata",
      "kind": "LinkedField",
      "name": "metadata",
      "plural": false,
      "selections": [
        {
          "alias": null,
          "args": null,
          "kind": "ScalarField",
          "name": "createdAt",
          "storageKey": null
        }
      ],
      "storageKey": null
    },
    {
      "alias": null,
      "args": null,
      "kind": "ScalarField",
      "name": "action",
      "storageKey": null
    },
    {
      "alias": null,
      "args": null,
      "kind": "ScalarField",
      "name": "targetType",
      "storageKey": null
    },
    {
      "alias": null,
      "args": null,
      "concreteType": null,
      "kind": "LinkedField",
      "name": "target",
      "plural": false,
      "selections": [
        {
          "kind": "InlineFragment",
          "selections": [
            {
              "alias": null,
              "args": null,
              "kind": "ScalarField",
              "name": "semanticVersion",
              "storageKey": null
            }
          ],
          "type": "Release",
          "abstractKey": null
        }
      ],
      "storageKey": null
    },
    {
      "alias": null,
      "args": null,
      "concreteType": null,
      "kind": "LinkedField",
      "name": "payload",
      "plural": false,
      "selections": [
        (v0/*: any*/),
        {
          "kind": "InlineFragment",
          "selections": [
            {
              "alias": null,
              "args": null,
              "kind": "ScalarField",
              "name": "changeType",
              "storageKey": null
            },
            {
              "alias": null,
              "args": null,
              "kind": "ScalarField",
              "name": "type",
              "storageKey": null
            },
            {
              "alias": null,
              "args": null,
              "concreteType": null,
              "kind": "LinkedField",
              "name": "resource",
              "plural": false,
              "selections": [
                {
                  "kind": "InlineFragment",
                  "selections": (v2/*: any*/),
                  "type": "Team",
                  "abstractKey": null
                },
                {
                  "kind": "InlineFragment",
                  "selections": [
                    (v1/*: any*/)
                  ],
                  "type": "User",
                  "abstractKey": null
                }
              ],
              "storageKey": null
            }
          ],
          "type": "ActivityEventUpdateReleasePayload",
          "abstractKey": null
        }
      ],
      "storageKey": null
    },
    {
      "args": null,
      "kind": "FragmentSpread",
      "name": "ReleaseActivityCommentEventFragment_activityEvent"
    },
    {
      "args": null,
      "kind": "FragmentSpread",
      "name": "ReleaseActivityDeploymentEventFragment_activityEvent"
    }
  ],
  "type": "ActivityEvent",
  "abstractKey": null
};
})();

(node as any).hash = "46d4e6ce3c1c893a59bf7a0554a2062e";

export default node;
