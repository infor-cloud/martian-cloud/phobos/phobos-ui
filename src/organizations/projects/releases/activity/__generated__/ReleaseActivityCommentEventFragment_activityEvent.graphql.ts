/**
 * @generated SignedSource<<4eef77e7ba3070857737fac7d2e85bff>>
 * @lightSyntaxTransform
 * @nogrep
 */

/* tslint:disable */
/* eslint-disable */
// @ts-nocheck

import { Fragment, ReaderFragment } from 'relay-runtime';
export type ActivityEventAction = "ADD_PARTICIPANT" | "CANCEL" | "CREATE" | "CREATE_MEMBERSHIP" | "DELETE" | "REMOVE_MEMBERSHIP" | "REMOVE_PARTICIPANT" | "RETRY" | "START" | "UPDATE" | "%future added value";
export type CommentType = "REPLY" | "THREAD" | "%future added value";
import { FragmentRefs } from "relay-runtime";
export type ReleaseActivityCommentEventFragment_activityEvent$data = {
  readonly action: ActivityEventAction;
  readonly target: {
    readonly text?: string;
    readonly type?: CommentType;
    readonly " $fragmentSpreads": FragmentRefs<"ReleaseCommentListItemFragment_comment">;
  } | null | undefined;
  readonly " $fragmentSpreads": FragmentRefs<"ActivityEventListItemFragment_event">;
  readonly " $fragmentType": "ReleaseActivityCommentEventFragment_activityEvent";
};
export type ReleaseActivityCommentEventFragment_activityEvent$key = {
  readonly " $data"?: ReleaseActivityCommentEventFragment_activityEvent$data;
  readonly " $fragmentSpreads": FragmentRefs<"ReleaseActivityCommentEventFragment_activityEvent">;
};

const node: ReaderFragment = {
  "argumentDefinitions": [],
  "kind": "Fragment",
  "metadata": null,
  "name": "ReleaseActivityCommentEventFragment_activityEvent",
  "selections": [
    {
      "alias": null,
      "args": null,
      "kind": "ScalarField",
      "name": "action",
      "storageKey": null
    },
    {
      "alias": null,
      "args": null,
      "concreteType": null,
      "kind": "LinkedField",
      "name": "target",
      "plural": false,
      "selections": [
        {
          "kind": "InlineFragment",
          "selections": [
            {
              "alias": null,
              "args": null,
              "kind": "ScalarField",
              "name": "type",
              "storageKey": null
            },
            {
              "alias": null,
              "args": null,
              "kind": "ScalarField",
              "name": "text",
              "storageKey": null
            },
            {
              "args": null,
              "kind": "FragmentSpread",
              "name": "ReleaseCommentListItemFragment_comment"
            }
          ],
          "type": "Comment",
          "abstractKey": null
        }
      ],
      "storageKey": null
    },
    {
      "args": null,
      "kind": "FragmentSpread",
      "name": "ActivityEventListItemFragment_event"
    }
  ],
  "type": "ActivityEvent",
  "abstractKey": null
};

(node as any).hash = "3336e23dc83d94d8ad7b751f1aadd2f5";

export default node;
