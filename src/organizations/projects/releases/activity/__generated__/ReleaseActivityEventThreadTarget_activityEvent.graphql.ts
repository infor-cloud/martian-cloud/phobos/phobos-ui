/**
 * @generated SignedSource<<0b210a1ce6b3fc7819df80c34689e15b>>
 * @lightSyntaxTransform
 * @nogrep
 */

/* tslint:disable */
/* eslint-disable */
// @ts-nocheck

import { Fragment, ReaderFragment } from 'relay-runtime';
export type ActivityEventAction = "ADD_PARTICIPANT" | "APPROVE" | "CANCEL" | "CREATE" | "CREATE_MEMBERSHIP" | "DEFER" | "DELETE" | "REMOVE_MEMBERSHIP" | "REMOVE_PARTICIPANT" | "RETRY" | "REVOKE_APPROVAL" | "SCHEDULE" | "SKIP" | "START" | "UNDEFER" | "UNSCHEDULE" | "UPDATE" | "%future added value";
import { FragmentRefs } from "relay-runtime";
export type ReleaseActivityEventThreadTarget_activityEvent$data = {
  readonly action: ActivityEventAction;
  readonly target: {
    readonly " $fragmentSpreads": FragmentRefs<"ReleaseThreadListItemFragment_thread">;
  } | null | undefined;
  readonly " $fragmentSpreads": FragmentRefs<"ActivityEventListItemFragment_event">;
  readonly " $fragmentType": "ReleaseActivityEventThreadTarget_activityEvent";
};
export type ReleaseActivityEventThreadTarget_activityEvent$key = {
  readonly " $data"?: ReleaseActivityEventThreadTarget_activityEvent$data;
  readonly " $fragmentSpreads": FragmentRefs<"ReleaseActivityEventThreadTarget_activityEvent">;
};

const node: ReaderFragment = {
  "argumentDefinitions": [],
  "kind": "Fragment",
  "metadata": null,
  "name": "ReleaseActivityEventThreadTarget_activityEvent",
  "selections": [
    {
      "alias": null,
      "args": null,
      "kind": "ScalarField",
      "name": "action",
      "storageKey": null
    },
    {
      "alias": null,
      "args": null,
      "concreteType": null,
      "kind": "LinkedField",
      "name": "target",
      "plural": false,
      "selections": [
        {
          "kind": "InlineFragment",
          "selections": [
            {
              "args": null,
              "kind": "FragmentSpread",
              "name": "ReleaseThreadListItemFragment_thread"
            }
          ],
          "type": "Thread",
          "abstractKey": null
        }
      ],
      "storageKey": null
    },
    {
      "args": null,
      "kind": "FragmentSpread",
      "name": "ActivityEventListItemFragment_event"
    }
  ],
  "type": "ActivityEvent",
  "abstractKey": null
};

(node as any).hash = "484d154e5587a04465b154389a7adecb";

export default node;
