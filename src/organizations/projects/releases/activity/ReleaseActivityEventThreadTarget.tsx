import List from '@mui/material/List';
import graphql from 'babel-plugin-relay/macro';
import { useFragment } from 'react-relay/hooks';
import ActivityEventListItem from '../../../../activity/ActivityEventListItem';
import { CommentIcon } from '../../../../common/Icons';
import ReleaseThreadListItem from '../comments/ReleaseThreadListItem';
import { ReleaseActivityEventThreadTarget_activityEvent$key } from './__generated__/ReleaseActivityEventThreadTarget_activityEvent.graphql';

interface Props {
    fragmentRef: ReleaseActivityEventThreadTarget_activityEvent$key
}

function ReleaseActivityEventThreadTarget({ fragmentRef }: Props) {

    const data = useFragment<ReleaseActivityEventThreadTarget_activityEvent$key>(
        graphql`
        fragment ReleaseActivityEventThreadTarget_activityEvent on ActivityEvent
        {
            action
            target {
                ... on Thread {
                    ...ReleaseThreadListItemFragment_thread
                }
            }
            ...ActivityEventListItemFragment_event

        }`, fragmentRef
    );

    const comment = data.target;
    return comment ? (
        <ActivityEventListItem
            fragmentRef={data}
            icon={<CommentIcon />}
            hideHeader
            secondary={<List
                sx={{ mb: -2, width: "100%" }}
            >
                <ReleaseThreadListItem
                    fragmentRef={comment}
                />
            </List>}
        />
    ) : null;
}

export default ReleaseActivityEventThreadTarget;
