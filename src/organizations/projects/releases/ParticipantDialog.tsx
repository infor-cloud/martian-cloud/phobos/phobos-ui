import CloseIcon from '@mui/icons-material/Close';
import { Alert, Dialog, DialogContent, DialogTitle, IconButton, LinearProgress, Typography } from '@mui/material';
import graphql from 'babel-plugin-relay/macro';
import { useMemo, useState } from 'react';
import { useFragment, useMutation } from 'react-relay/hooks';
import { MutationError } from '../../../common/error';
import { ParticipantDialogFragment_participants$key } from './__generated__/ParticipantDialogFragment_participants.graphql';
import { ParticipantDialog_AddParticipantMutation } from './__generated__/ParticipantDialog_AddParticipantMutation.graphql';
import { ParticipantDialog_RemoveParticipantMutation } from './__generated__/ParticipantDialog_RemoveParticipantMutation.graphql';
import ParticipantSelectionBox, { Participant, TeamParticipant, UserParticipant } from './create/ReleaseParticipantSelectionBox';

interface ParticipantDialogProps {
    fragmentRef: ParticipantDialogFragment_participants$key
    onClose: () => void
}

function ParticipantDialog({ onClose, fragmentRef, ...other }: ParticipantDialogProps) {
    const [error, setError] = useState<MutationError>()

    const data = useFragment<ParticipantDialogFragment_participants$key>(
        graphql`
            fragment ParticipantDialogFragment_participants on Release
                {
                    id
                    participants {
                        __typename
                        ... on User {
                            id
                            email
                            username
                        }
                        ... on Team {
                            id
                            name
                        }
                    }
                }
        `, fragmentRef);

    const [addParticipantCommit, addParticipantCommitInFlight] = useMutation<ParticipantDialog_AddParticipantMutation>(graphql`
        mutation ParticipantDialog_AddParticipantMutation($input: ReleaseParticipantInput!) {
            addParticipantToRelease(input: $input) {
                release {
                    id
                    ...ParticipantDialogFragment_participants
                }
                problems {
                    message
                    field
                    type
                }
            }
        }
    `);

    const [removeParticipantCommit, removeParticipantCommitInFlight] = useMutation<ParticipantDialog_RemoveParticipantMutation>(graphql`
        mutation ParticipantDialog_RemoveParticipantMutation($input: ReleaseParticipantInput!) {
            removeParticipantFromRelease(input: $input) {
                release {
                    ...ParticipantDialogFragment_participants
                }
                problems {
                    message
                    field
                    type
                }
            }
        }
    `);

    const onAddParticipant = (participant: Participant) => {
        addParticipantCommit({
            variables: {
                input: {
                    releaseId: data.id,
                    userId: participant.type === 'user' ? participant.id : undefined,
                    teamId: participant.type === 'team' ? participant.id : undefined
                },
            },
            onCompleted: (data) => {
                if (data.addParticipantToRelease.problems.length) {
                    setError({
                        severity: 'warning',
                        message: data.addParticipantToRelease.problems.map((problem: any) => problem.message).join('; ')
                    });
                } else if (!data.addParticipantToRelease.release) {
                    setError({
                        severity: 'error',
                        message: "Unexpected error occurred"
                    });
                }
            },
            onError: (error) => {
                setError({
                    severity: 'error',
                    message: `Unexpected error occurred: ${error.message}`
                });
            }
        });
    };

    const onRemoveParticipant = (participant: Participant) => {
        removeParticipantCommit({
            variables: {
                input: {
                    releaseId: data.id,
                    userId: participant.type === 'user' ? participant.id : undefined,
                    teamId: participant.type === 'team' ? participant.id : undefined
                }
            },
            onCompleted: (data) => {
                if (data.removeParticipantFromRelease.problems.length) {
                    setError({
                        severity: 'warning',
                        message: data.removeParticipantFromRelease.problems.map((problem: any) => problem.message).join('; ')
                    });
                } else if (!data.removeParticipantFromRelease.release) {
                    setError({
                        severity: 'error',
                        message: "Unexpected error occurred"
                    });
                }
            },
            onError: (error) => {
                setError({
                    severity: 'error',
                    message: `Unexpected error occurred: ${error.message}`
                });
            }
        });
    };

    const participants = useMemo<Participant[]>(() => data.participants.map((p => {
        if (p.__typename === 'User') {
            return { id: p.id, type: 'user', username: p.username, email: p.email } as UserParticipant;
        } else if (p.__typename === 'Team') {
            return { id: p.id, type: 'team', name: p.name } as TeamParticipant;
        } else {
            throw new Error(`Unexpected participant type: ${p.__typename}`);
        }
    })), [data.participants]);

    return (
        <Dialog
            fullWidth
            maxWidth="md"
            open
            onClose={onClose}
            {...other}
        >
            <DialogTitle component="div" sx={{ display: 'flex', justifyContent: 'space-between', alignItems: 'center' }}>
                <Typography variant="h6">
                    Edit Participants
                </Typography>
                <IconButton
                    color="inherit"
                    size="small"
                    onClick={() => onClose()}
                >
                    <CloseIcon />
                </IconButton>
            </DialogTitle>
            {(addParticipantCommitInFlight || removeParticipantCommitInFlight) && <LinearProgress />}
            <DialogContent dividers>
                {error && <Alert sx={{ mb: 2 }} severity={error.severity}>
                    {error.message}
                </Alert>}
                <ParticipantSelectionBox participants={participants} onAdd={onAddParticipant} onRemove={onRemoveParticipant} />
            </DialogContent>
        </Dialog>
    );
}

export default ParticipantDialog;
