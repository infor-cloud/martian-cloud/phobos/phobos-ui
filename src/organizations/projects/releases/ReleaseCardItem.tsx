import { Box, Card, CardContent, Chip, Divider, Stack, Tooltip, Typography, styled, useTheme } from '@mui/material';
import graphql from 'babel-plugin-relay/macro';
import { useMemo } from 'react';
import { useFragment } from 'react-relay/hooks';
import remarkGfm from 'remark-gfm';
import Gravatar from '../../../common/Gravatar';
import MuiMarkdown from '../../../common/Markdown';
import StyledAvatar from '../../../common/StyledAvatar';
import Timestamp from '../../../common/Timestamp';
import Link from '../../../routes/Link';
import StackedContainer from '../../../common/StackedContainer';
import ReleaseDueDateWarningBanner from './ReleaseDueDateWarningBanner';
import { ReleaseCardItemFragment_release$key } from './__generated__/ReleaseCardItemFragment_release.graphql';
import PipelineStatusType from '../pipelines/PipelineStatusType';
import CopyButton from '../../../common/CopyButton';

interface Props {
    fragmentRef: ReleaseCardItemFragment_release$key
}

const FieldLabel = styled(
    Typography
)(({ theme }) => ({
    marginBottom: theme.spacing(1)
}));

function ReleaseCardItem({ fragmentRef }: Props) {
    const theme = useTheme();

    const data = useFragment<ReleaseCardItemFragment_release$key>(graphql`
        fragment ReleaseCardItemFragment_release on Release {
            id
            semanticVersion
            notes
            dueDate
            completed
            preRelease
            latest
            lifecyclePrn
            participants {
                __typename
                ... on User {
                    id
                    email
                    username
                }
                ... on Team {
                    id
                    name
                }
            }
            pipeline {
                id
                status
            }
            metadata {
                createdAt
            }
            createdBy
        }
    `, fragmentRef);

    const participants = useMemo(() => data.participants.map((p => {
        if (p.__typename === 'User') {
            return { id: p.id, label: p.email, tooltip: p.email, type: 'user' }
        } else if (p.__typename === 'Team') {
            return { id: p.id, label: p.name[0].toUpperCase(), tooltip: p.name, type: 'team' }
        }
    })), [data.participants]);

    return (
        <Card variant="outlined" sx={{ mb: 2, p: 1 }}>
            {!data.completed && <ReleaseDueDateWarningBanner dueDate={data.dueDate} />}
            <Box padding={2}>
                <Box sx={{
                    [theme.breakpoints.up('md')]: {
                        display: 'flex',
                        justifyContent: 'space-between'
                    }
                }}>
                    <Box display="flex" alignItems="center">
                        {PipelineStatusType[data.pipeline.status]?.icon}
                        <Link ml={1} variant="h5" color="inherit" to={data.id}><strong>v{data.semanticVersion}</strong></Link>
                        {data.preRelease && <Chip
                            sx={{ ml: 2 }}
                            size="small"
                            label="pre-release"
                            variant="outlined"
                            color="warning"
                        />}
                        {data.latest && <Chip
                            sx={{ ml: 2 }}
                            size="small"
                            label="latest"
                            variant="filled"
                            color="secondary"
                        />}
                    </Box>
                    <Box
                        display="flex"
                        alignItems="center"
                        sx={{
                            mt: 1,
                            [theme.breakpoints.up('md')]: {
                                mt: 0
                            }
                        }}>
                        <Typography variant="body2" color="textSecondary" mr={1}>
                            created <Timestamp component="span" timestamp={data.metadata.createdAt} /> by
                        </Typography>
                        <Box>
                            <Tooltip title={data.createdBy}>
                                <Box>
                                    <Gravatar width={24} height={24} email={data.createdBy} />
                                </Box>
                            </Tooltip>
                        </Box>
                    </Box>
                </Box>
                {data.notes !== '' && <Box mt={2}>
                    <MuiMarkdown remarkPlugins={[remarkGfm]}>
                        {`${data.notes}`}
                    </MuiMarkdown>
                </Box>}
            </Box>
            <Divider sx={{ opacity: 0.6 }} />
            <CardContent>
                <Box mb={2}>
                    <FieldLabel>Lifecycle</FieldLabel>
                    <Stack direction="row" spacing={0.5} alignItems="center">
                        <Typography color="textSecondary">{data.lifecyclePrn}</Typography>
                        <CopyButton data={data.lifecyclePrn} toolTip='Copy Lifecycle PRN' />
                    </Stack>
                </Box>
                <Box mb={2}>
                    <FieldLabel>Due Date</FieldLabel>
                    <Typography color="textSecondary">
                        {data.dueDate ? <Timestamp component="span" format="absolute" timestamp={data.dueDate} /> : 'None'}
                    </Typography>
                </Box>
                <Box>
                    <FieldLabel>Participants</FieldLabel>{participants.length === 0 ?
                        <Typography color="textSecondary">None</Typography>
                        :
                        <StackedContainer>
                            {participants.map((participant: any) => (
                                <Tooltip key={participant.id} title={participant.tooltip}>
                                    <Box>
                                        {participant.type == 'user'
                                            ? <Gravatar width={24} height={24} email={participant.label} />
                                            : <StyledAvatar>{participant.label}</StyledAvatar>}
                                    </Box>
                                </Tooltip>
                            ))}
                        </StackedContainer>}
                </Box>
            </CardContent>
        </Card>
    );
}

export default ReleaseCardItem
