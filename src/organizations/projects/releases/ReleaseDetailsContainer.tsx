import { Box, Typography } from '@mui/material';
import { useLazyLoadQuery } from 'react-relay/hooks';
import graphql from 'babel-plugin-relay/macro';
import { useParams } from 'react-router-dom';
import ReleaseDetails from './ReleaseDetails';
import { ReleaseDetailsContainerQuery } from './__generated__/ReleaseDetailsContainerQuery.graphql';

function ReleaseDetailsContainer() {
    const releaseId = useParams().releaseId as string;

    const data = useLazyLoadQuery<ReleaseDetailsContainerQuery>(graphql`
        query ReleaseDetailsContainerQuery($id: String!, $first: Int!, $after: String) {
            node(id: $id) {
                ... on Release {
                    id
                    ...ReleaseDetailsFragment_release
                }
            }
        }
    `, { id: releaseId, first: 20 }, { fetchPolicy: 'store-and-network' });

    if (!data.node) {
        return (
            <Box display="flex" justifyContent="center" mt={4}>
                <Typography color="textSecondary">Release with ID {releaseId} not found</Typography>
            </Box>);
    } else {
        return <ReleaseDetails fragmentRef={data.node} />
    }
}

export default ReleaseDetailsContainer;
