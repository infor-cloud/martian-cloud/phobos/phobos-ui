import { Alert, SxProps, Theme } from "@mui/material";
import moment, { Moment } from "moment";
import { useMemo } from "react";

interface Props {
    dueDate: Moment | null;
    sx?: SxProps<Theme>;
}

function ReleaseDueDateWarningBanner({ dueDate, sx }: Props) {
    const releaseDueSoon = useMemo(() => {
        return dueDate ? (moment(dueDate as moment.MomentInput).isBefore(moment().add(1, 'hour')) &&
            moment(dueDate as moment.MomentInput).isAfter(moment())) : false;
    }, [dueDate]);
    const releasePastDue = useMemo(() => {
        return dueDate ? moment(dueDate as moment.MomentInput).isBefore(moment()) : false;
    }, [dueDate]);

    if (releaseDueSoon) {
        return (<Alert severity="info" variant="outlined" sx={sx}>This release is due within the next hour</Alert>);
    } else if (releasePastDue) {
        return (<Alert severity="warning" variant="outlined" sx={sx}>This release is past due</Alert>);
    }
    return null;
}

export default ReleaseDueDateWarningBanner;
