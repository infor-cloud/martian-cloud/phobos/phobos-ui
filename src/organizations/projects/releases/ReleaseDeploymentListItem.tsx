import { Done as ApproveIcon, CloseOutlined, EditOutlined, PlayArrowOutlined, ReplayOutlined, ScheduleOutlined } from '@mui/icons-material';
import { LoadingButton } from '@mui/lab';
import { Button, Stack, TableCell, TableRow, Tooltip, Typography } from '@mui/material';
import Box from '@mui/material/Box';
import graphql from 'babel-plugin-relay/macro';
import { Moment } from 'moment';
import { useSnackbar } from 'notistack';
import React, { useMemo, useState } from 'react';
import { useAuth } from 'react-oidc-context';
import { useFragment, useMutation, useSubscription } from 'react-relay/hooks';
import { useNavigate } from 'react-router-dom';
import { GraphQLSubscriptionConfig } from 'relay-runtime';
import ConfirmationDialog from '../../../common/ConfirmationDialog';
import Gravatar from '../../../common/Gravatar';
import StyledAvatar from '../../../common/StyledAvatar';
import { StyledCode } from '../../../common/StyledCode';
import Timestamp from '../../../common/Timestamp';
import Link from '../../../routes/Link';
import PipelineScheduleNodeDialog, { CronSchedule } from '../pipelines/PipelineScheduleNodeDialog';
import PipelineStageIcons from '../pipelines/PipelineStageIcons';
import PipelineStatusChip from '../pipelines/PipelineStatusChip';
import { ReleaseDeploymentListItemApproveDeploymentMutation } from './__generated__/ReleaseDeploymentListItemApproveDeploymentMutation.graphql';
import { ReleaseDeploymentListItemCancelPipelineMutation } from './__generated__/ReleaseDeploymentListItemCancelPipelineMutation.graphql';
import { ReleaseDeploymentListItemCancelScheduledDeploymentMutation } from './__generated__/ReleaseDeploymentListItemCancelScheduledDeploymentMutation.graphql';
import { ReleaseDeploymentListItemFragment_pipeline$key } from './__generated__/ReleaseDeploymentListItemFragment_pipeline.graphql';
import { ReleaseDeploymentListItemRetryNestedPipelineMutation } from './__generated__/ReleaseDeploymentListItemRetryNestedPipelineMutation.graphql';
import { ReleaseDeploymentListItemRunPipelineMutation } from './__generated__/ReleaseDeploymentListItemRunPipelineMutation.graphql';
import { ReleaseDeploymentListItemScheduleDeploymentMutation } from './__generated__/ReleaseDeploymentListItemScheduleDeploymentMutation.graphql';
import { ReleaseDeploymentListItemSubscription } from './__generated__/ReleaseDeploymentListItemSubscription.graphql';
import { ReleaseDeploymentListItemDeferPipelineMutation } from './__generated__/ReleaseDeploymentListItemDeferPipelineMutation.graphql';
import { ReleaseDeploymentListItemUndeferPipelineMutation } from './__generated__/ReleaseDeploymentListItemUndeferPipelineMutation.graphql';
import { MutationError } from '../../../common/error';
import { CalendarArrowRight, DeleteRestore } from 'mdi-material-ui';
import PipelineDeferPipelineNodeDialog from '../pipelines/PipelineDeferPipelineNodeDialog';

const pipelineSubscription = graphql`subscription ReleaseDeploymentListItemSubscription($input: PipelineEventsSubscriptionInput!) {
    pipelineEvents(input: $input) {
      action
      pipeline {
        ...ReleaseDeploymentListItemFragment_pipeline
        id
      }
    }
}`;

interface Props {
    fragmentRef: ReleaseDeploymentListItemFragment_pipeline$key;
}

function ReleaseDeploymentListItem({ fragmentRef }: Props) {
    const [showCancelDeploymentConfirmation, setShowCancelDeploymentConfirmation] = useState(false);
    const [showRunDeploymentConfirmation, setShowRunDeploymentConfirmation] = useState(false);
    const [showRetryDeploymentConfirmation, setShowRetryDeploymentConfirmation] = useState(false);
    const [showScheduleDeploymentDialog, setShowScheduleDeploymentDialog] = useState(false);
    const [scheduleDeploymentError, setScheduleDeploymentError] = useState<MutationError | null>(null);
    const [showDeferDeploymentDialog, setShowDeferDeploymentDialog] = useState(false);
    const [showUndeferDeploymentConfirmation, setShowUndeferDeploymentConfirmation] = useState(false);
    const { enqueueSnackbar } = useSnackbar();
    const { user } = useAuth();

    const navigate = useNavigate();
    const data = useFragment<ReleaseDeploymentListItemFragment_pipeline$key>(
        graphql`
        fragment ReleaseDeploymentListItemFragment_pipeline on Pipeline
        {
            metadata {
                version
            }
            id
            status
            when
            scheduledStartTime
            cronSchedule {
                expression
                timezone
            }
            parentPipelineNodePath
            approvals {
                id
                approver {
                    __typename
                    ...on User {
                        email
                    }
                    ...on ServiceAccount {
                        name
                    }
                }
            }
            approvalStatus
            approvalRules {
                approvalsRequired
            }
            pipelineTemplate {
                id
                name
                semanticVersion
            }
            parentPipeline {
                id
            }
            timestamps {
                startedAt
                completedAt
            }
            environmentName
            ...PipelineStageIcons_stages
        }
    `, fragmentRef);

    const pipelineSubscriptionConfig = useMemo<GraphQLSubscriptionConfig<ReleaseDeploymentListItemSubscription>>(() => ({
        variables: { input: { pipelineId: data.id as string, lastSeenVersion: data.metadata.version } },
        subscription: pipelineSubscription,
        onCompleted: () => console.log("Subscription completed"),
        onError: () => console.warn("Subscription error"),
    }), [data.id]);

    useSubscription<ReleaseDeploymentListItemSubscription>(pipelineSubscriptionConfig);

    const parentPipelineId = data.parentPipeline?.id as string;
    const parentPipelineNodePath = data.parentPipelineNodePath as string;

    const [commitRunDeployment, commitRunDeploymentInFlight] = useMutation<ReleaseDeploymentListItemRunPipelineMutation>(graphql`
        mutation ReleaseDeploymentListItemRunPipelineMutation($input: RunPipelineInput!) {
            runPipeline(input: $input) {
                problems {
                    message
                    field
                    type
                }
            }
        }
    `);

    const [commitCancelDeployment, commitCancelDeploymentInFlight] = useMutation<ReleaseDeploymentListItemCancelPipelineMutation>(graphql`
        mutation ReleaseDeploymentListItemCancelPipelineMutation($input: CancelPipelineInput!) {
            cancelPipeline(input: $input) {
                problems {
                    message
                    field
                    type
                }
            }
        }
    `);

    const [commitRetryDeployment, commitRetryDeploymentInFlight] = useMutation<ReleaseDeploymentListItemRetryNestedPipelineMutation>(graphql`
        mutation ReleaseDeploymentListItemRetryNestedPipelineMutation($input: RetryNestedPipelineInput!) {
            retryNestedPipeline(input: $input) {
                problems {
                    message
                    field
                    type
                }
            }
        }
    `);

    const [commitScheduleDeployment, commitScheduleNestedPipelineInFlight] = useMutation<ReleaseDeploymentListItemScheduleDeploymentMutation>(graphql`
        mutation ReleaseDeploymentListItemScheduleDeploymentMutation($input: SchedulePipelineNodeInput!) {
            schedulePipelineNode(input: $input) {
                problems {
                    message
                    field
                    type
                }
            }
        }
    `);

    const [commitCancelScheduledDeployment, commitCancelScheduledDeploymentInFlight] = useMutation<ReleaseDeploymentListItemCancelScheduledDeploymentMutation>(graphql`
        mutation ReleaseDeploymentListItemCancelScheduledDeploymentMutation($input: CancelPipelineNodeScheduleInput!) {
            cancelPipelineNodeSchedule(input: $input) {
                problems {
                    message
                    field
                    type
                }
            }
        }
    `);

    const [commitApproveDeployment, commitApproveDeploymentInFlight] = useMutation<ReleaseDeploymentListItemApproveDeploymentMutation>(graphql`
        mutation ReleaseDeploymentListItemApproveDeploymentMutation($input: ApprovePipelineInput!) {
            approvePipeline(input: $input) {
                pipeline {
                    ...ReleaseDeploymentListItemFragment_pipeline
                }
                problems {
                    message
                    field
                    type
                }
            }
        }
    `);

    const [commitDeferDeployment, commitDeferDeploymentInFlight] = useMutation<ReleaseDeploymentListItemDeferPipelineMutation>(graphql`
        mutation ReleaseDeploymentListItemDeferPipelineMutation($input: DeferPipelineNodeInput!) {
            deferPipelineNode(input: $input) {
                problems {
                    message
                    field
                    type
                }
            }
        }
    `);

    const [commitUndeferDeployment, commitUndeferDeploymentInFlight] = useMutation<ReleaseDeploymentListItemUndeferPipelineMutation>(graphql`
        mutation ReleaseDeploymentListItemUndeferPipelineMutation($input: UndeferPipelineNodeInput!) {
            undeferPipelineNode(input: $input) {
                problems {
                    message
                    field
                    type
                }
            }
        }
    `);

    const handleMutationError = (error: Error, completedCallback?: () => void) => {
        if (completedCallback) {
            completedCallback();
        }
        enqueueSnackbar(`Unexpected error: ${error.message}`, { variant: 'error' });
    }

    const handleMutationProblems = (problems: any, completedCallback?: () => void) => {
        if (completedCallback) {
            completedCallback();
        }
        if (problems && problems.length > 0) {
            enqueueSnackbar(problems.map((problem: any) => problem.message).join('; '), { variant: 'warning' });
        }
    }

    const onStartDeployment = () => {
        commitRunDeployment({
            variables: {
                input: {
                    id: data.id,
                },
            },
            onCompleted: data => handleMutationProblems(data.runPipeline?.problems, () => setShowRunDeploymentConfirmation(false)),
            onError: error => handleMutationError(error, () => setShowRunDeploymentConfirmation(false))
        });
    }

    const onCancelDeployment = () => {
        commitCancelDeployment({
            variables: {
                input: {
                    id: data.id
                },
            },
            onCompleted: data => handleMutationProblems(data.cancelPipeline?.problems, () => setShowCancelDeploymentConfirmation(false)),
            onError: error => handleMutationError(error, () => setShowCancelDeploymentConfirmation(false))
        });
    }

    const onRetryDeployment = () => {
        commitRetryDeployment({
            variables: {
                input: {
                    parentPipelineId: parentPipelineId,
                    parentNestedPipelineNodePath: parentPipelineNodePath,
                },
            },
            onCompleted: data => handleMutationProblems(data.retryNestedPipeline?.problems, () => setShowRetryDeploymentConfirmation(false)),
            onError: error => handleMutationError(error, () => setShowRetryDeploymentConfirmation(false))
        });
    }

    const onScheduleDeployment = (scheduledTime: Moment | null, cronSchedule: CronSchedule | null) => {
        commitScheduleDeployment({
            variables: {
                input: {
                    pipelineId: parentPipelineId,
                    nodePath: parentPipelineNodePath,
                    nodeType: 'PIPELINE',
                    scheduledStartTime: scheduledTime?.utc().toISOString(),
                    cronSchedule: cronSchedule
                },
            },
            onCompleted: data => {
                if (data.schedulePipelineNode?.problems.length) {
                    setScheduleDeploymentError({
                        severity: 'warning',
                        message: data.schedulePipelineNode.problems.map((problem: any) => problem.message).join(', ')
                    });
                } else {
                    setScheduleDeploymentError(null);
                    setShowScheduleDeploymentDialog(false);
                }
            },
            onError: error => handleMutationError(error, () => setShowScheduleDeploymentDialog(false))
        });
    }

    const onCancelScheduledDeployment = () => {
        commitCancelScheduledDeployment({
            variables: {
                input: {
                    pipelineId: parentPipelineId,
                    nodePath: parentPipelineNodePath,
                    nodeType: 'PIPELINE',
                },
            },
            onCompleted: data => handleMutationProblems(data.cancelPipelineNodeSchedule?.problems, () => setShowScheduleDeploymentDialog(false)),
            onError: error => handleMutationError(error, () => setShowScheduleDeploymentDialog(false))
        });
    }

    const onApproveDeployment = () => {
        commitApproveDeployment({
            variables: {
                input: {
                    pipelineId: data.id,
                },
            },
            onCompleted: data => handleMutationProblems(data.approvePipeline?.problems),
            onError: handleMutationError
        });
    }

    const onDeferDeployment = (reason: string) => {
        commitDeferDeployment({
            variables: {
                input: {
                    pipelineId: parentPipelineId,
                    nodePath: parentPipelineNodePath,
                    nodeType: 'PIPELINE',
                    reason: reason,
                },
            },
            onCompleted: data => handleMutationProblems(data.deferPipelineNode?.problems, () => setShowDeferDeploymentDialog(false)),
            onError: error => handleMutationError(error, () => setShowDeferDeploymentDialog(false))
        });
    }

    const onUndeferDeployment = () => {
        commitUndeferDeployment({
            variables: {
                input: {
                    pipelineId: parentPipelineId,
                    nodePath: parentPipelineNodePath,
                    nodeType: 'PIPELINE',
                },
            },
            onCompleted: data => handleMutationProblems(data.undeferPipelineNode?.problems, () => setShowUndeferDeploymentConfirmation(false)),
            onError: error => handleMutationError(error, () => setShowUndeferDeploymentConfirmation(false))
        });
    }

    const userApprovedDeployment = useMemo(() => {
        return data?.approvals.map(approval => approval.approver).find(approver => approver?.__typename === 'User' && approver.email === user?.profile.email) ? true : false;
    }, [data]);

    const actions = useMemo(() => [
        {
            label: 'Edit Deployment',
            condition: ['CREATED', 'READY', 'SUCCEEDED', 'FAILED', 'CANCELED', 'SKIPPED'].includes(data.status),
            handler: () => navigate(`edit_deployment/${data.environmentName}`),
            icon: <EditOutlined />
        },
        {
            label: 'Defer Deployment',
            condition: ['READY', 'WAITING'].includes(data.status),
            handler: () => setShowDeferDeploymentDialog(true),
            icon: <CalendarArrowRight />
        },
        {
            label: 'Restore Deployment',
            condition: data.status === 'DEFERRED',
            handler: () => setShowUndeferDeploymentConfirmation(true),
            icon: <DeleteRestore />
        },
        {
            label: 'Schedule Deployment',
            condition: ['CREATED', 'APPROVAL_PENDING', 'WAITING', 'READY'].includes(data.status),
            handler: () => setShowScheduleDeploymentDialog(true),
            icon: <ScheduleOutlined />
        },
        {
            label: 'Start Deployment',
            condition: data.status === 'READY' && data.when === 'manual',
            handler: () => setShowRunDeploymentConfirmation(true),
            icon: <PlayArrowOutlined />
        },
        {
            label: 'Cancel Deployment',
            condition: data.status === 'RUNNING',
            handler: () => setShowCancelDeploymentConfirmation(true),
            icon: <CloseOutlined />
        },
        {
            label: 'Retry Deployment',
            condition: ['SUCCEEDED', 'FAILED', 'CANCELED'].includes(data.status),
            handler: () => setShowRetryDeploymentConfirmation(true),
            icon: <ReplayOutlined />
        }
    ], [data]);

    return (
        <React.Fragment>
            <TableRow
                sx={{ '&:last-child td, &:last-child th': { border: 0 } }}
            >
                <TableCell>
                    <PipelineStatusChip to={`../../-/pipelines/${data.id}`} status={data.status} />
                    {data.scheduledStartTime && ['CREATED', 'APPROVAL_PENDING', 'WAITING'].includes(data.status) && <Typography
                        component="div"
                        mt={0.5}
                        variant="caption"
                        color="textSecondary"
                    >
                        scheduled to start <Timestamp component="span" timestamp={data.scheduledStartTime} />
                    </Typography>}
                    {data.cronSchedule && data.status === 'CREATED' && <Typography
                        component="div"
                        mt={0.5}
                        maxWidth={260}
                        variant="caption"
                        color="textSecondary"
                    >
                        will be scheduled using cron expression <StyledCode>{data.cronSchedule.expression}</StyledCode> in {data.cronSchedule.timezone} timezone
                    </Typography>}
                    {!data.timestamps.completedAt && <Box>
                        {data.timestamps.startedAt && <Typography
                            component="div"
                            mt={0.5}
                            variant="caption"
                            color="textSecondary"
                        >
                            started{' '}<Timestamp component="span" timestamp={data.timestamps.startedAt} />
                        </Typography>}
                    </Box>}
                    {data.timestamps.completedAt && <Typography
                        component="div"
                        mt={0.5}
                        variant="caption"
                        color="textSecondary"
                    >
                        finished{' '}<Timestamp component="span" timestamp={data.timestamps.completedAt} />
                    </Typography>}
                </TableCell>
                <TableCell>
                    {data.environmentName}
                </TableCell>
                <TableCell>
                    <Link color="inherit" to={`../../-/pipeline_templates/${data.pipelineTemplate.id}`}>
                        <Typography variant="body2" style={{ maxWidth: 200, wordWrap: 'break-word', overflowWrap: 'break-word' }}>
                            {data.pipelineTemplate.name}:{data.pipelineTemplate.semanticVersion}
                        </Typography>
                    </Link>
                </TableCell>
                <TableCell>
                    <PipelineStageIcons fragmentRef={data} />
                </TableCell>
                <TableCell>
                    {data.approvalRules.length === 0 && 'None Required'}
                    {data.approvalStatus === 'PENDING' && data?.approvals.length == 0 && 'None'}
                    {<Stack direction="row" marginLeft={1}>
                        {data.approvals.map(approval => (
                            <React.Fragment key={approval.id}>
                                {approval.approver?.__typename === 'User' && <Tooltip title={approval.approver?.email}>
                                    <Box><Gravatar width={20} height={20} email={approval.approver.email} /></Box>
                                </Tooltip>}
                                {approval.approver?.__typename === 'ServiceAccount' && <Tooltip title={approval.approver?.name}>
                                    <StyledAvatar>{approval.approver.name[0].toUpperCase()}</StyledAvatar>
                                </Tooltip>}
                            </React.Fragment>
                        ))}
                    </Stack>}
                </TableCell>
                <TableCell>
                    <Stack direction="row" spacing={1} justifyContent="flex-end">
                        {actions.map((action, index) => (
                            action.condition && (
                                <Tooltip key={index} title={action.label}>
                                    <Button sx={{ minWidth: 40, padding: '2px' }} color="info" size="small" variant="outlined" onClick={action.handler}>
                                        {action.icon}
                                    </Button>
                                </Tooltip>
                            )
                        ))}
                        {data.status === 'APPROVAL_PENDING' && !userApprovedDeployment &&
                            <Tooltip title='Approve'>
                                <LoadingButton
                                    sx={{ minWidth: 40, padding: '2px' }}
                                    variant="outlined"
                                    color="primary"
                                    loading={commitApproveDeploymentInFlight}
                                    onClick={onApproveDeployment}>
                                    <ApproveIcon />
                                </LoadingButton>
                            </Tooltip>
                        }
                    </Stack>
                </TableCell>
            </TableRow>
            {showCancelDeploymentConfirmation && <ConfirmationDialog
                title="Cancel Deployment"
                message={<React.Fragment>Are you sure you want to cancel the deployment in environment <strong>{data.environmentName}</strong>?</React.Fragment>}
                confirmButtonLabel="Yes"
                opInProgress={commitCancelDeploymentInFlight}
                onConfirm={onCancelDeployment}
                onClose={() => setShowCancelDeploymentConfirmation(false)}
            />}
            {showRunDeploymentConfirmation && <ConfirmationDialog
                title="Start Deployment"
                message={<React.Fragment>Are you sure you want to start the deployment to environment <strong>{data.environmentName}</strong>?</React.Fragment>}
                confirmButtonLabel="Yes"
                opInProgress={commitRunDeploymentInFlight}
                onConfirm={onStartDeployment}
                onClose={() => setShowRunDeploymentConfirmation(false)}
            />}
            {showRetryDeploymentConfirmation && <ConfirmationDialog
                title="Retry Deployment"
                message={<React.Fragment>Are you sure you want to retry the deployment to environment <strong>{data.environmentName}</strong>?</React.Fragment>}
                confirmButtonLabel="Yes"
                opInProgress={commitRetryDeploymentInFlight}
                onConfirm={onRetryDeployment}
                onClose={() => setShowRetryDeploymentConfirmation(false)}
            />}
            {showScheduleDeploymentDialog && <PipelineScheduleNodeDialog
                scheduledStartTime={data.scheduledStartTime}
                cronSchedule={data.cronSchedule}
                nodeType="deployment"
                commitInFlight={commitScheduleNestedPipelineInFlight || commitCancelScheduledDeploymentInFlight}
                error={scheduleDeploymentError}
                onClose={() => setShowScheduleDeploymentDialog(false)}
                onSetSchedule={onScheduleDeployment}
                onClearSchedule={onCancelScheduledDeployment}
            />}
            {showDeferDeploymentDialog && <PipelineDeferPipelineNodeDialog
                title="Defer Deployment"
                message={<React.Fragment>Are you sure you want to defer the deployment to environment <strong>{data.environmentName}</strong>?</React.Fragment>}
                onClose={() => setShowDeferDeploymentDialog(false)}
                onDefer={onDeferDeployment}
                inProgress={commitDeferDeploymentInFlight}
            />}
            {showUndeferDeploymentConfirmation && <ConfirmationDialog
                title="Restore Deployment"
                message={<React.Fragment>Are you sure you want to restore the deferred deployment to environment <strong>{data.environmentName}</strong>?</React.Fragment>}
                confirmButtonLabel="Yes"
                opInProgress={commitUndeferDeploymentInFlight}
                onConfirm={onUndeferDeployment}
                onClose={() => setShowUndeferDeploymentConfirmation(false)}
            />}
        </React.Fragment>
    );
}

export default ReleaseDeploymentListItem;
