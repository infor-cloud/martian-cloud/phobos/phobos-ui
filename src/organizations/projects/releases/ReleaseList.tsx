import { Box, Button, Typography, useTheme } from '@mui/material';
import graphql from 'babel-plugin-relay/macro';
import InfiniteScroll from 'react-infinite-scroll-component';
import { Link as RouterLink } from 'react-router-dom';
import { ConnectionHandler, useFragment, useLazyLoadQuery, usePaginationFragment } from 'react-relay/hooks';
import { DESCRIPTION } from './Releases';
import ListSkeleton from '../../../skeletons/ListSkeleton';
import ReleaseCardItem from './ReleaseCardItem';
import { ReleaseListPaginationQuery } from './__generated__/ReleaseListPaginationQuery.graphql';
import { ReleaseListFragment_releases$key } from './__generated__/ReleaseListFragment_releases.graphql';
import { ReleaseListQuery } from './__generated__/ReleaseListQuery.graphql';
import { ReleaseListFragment_project$key } from './__generated__/ReleaseListFragment_project.graphql';
const INITIAL_ITEM_COUNT = 50;
export function GetConnections(projectId: string): [string] {
    const connectionId = ConnectionHandler.getConnectionID(
        projectId,
        'ReleaseList_releases',
        { sort: 'CREATED_AT_DESC' }
    );
    return [connectionId];
}

const NewButton =
    <Button
        sx={{ minWidth: 200 }}
        component={RouterLink}
        variant="outlined"
        to="new"
    >
        New Release
    </Button>;

const query = graphql`
    query ReleaseListQuery($id: String!, $first: Int!, $after: String) {
        node(id: $id) {
            ... on Project {
                ...ReleaseListFragment_releases
            }
        }
    }`

interface Props {
    fragmentRef: ReleaseListFragment_project$key
}

function ReleaseList({ fragmentRef }: Props) {
    const theme = useTheme();

    const project = useFragment<ReleaseListFragment_project$key>(
        graphql`
        fragment ReleaseListFragment_project on Project
        {
            id
        }
    `, fragmentRef);

    const queryData = useLazyLoadQuery<ReleaseListQuery>(query, { first: INITIAL_ITEM_COUNT, id: project.id }, { fetchPolicy: "store-and-network" })

    const { data, loadNext, hasNext } = usePaginationFragment<ReleaseListPaginationQuery, ReleaseListFragment_releases$key>(
        graphql`
          fragment ReleaseListFragment_releases on Project
          @refetchable(queryName: "ReleaseListPaginationQuery") {
              releases(
                  after: $after
                  first: $first
                  sort: CREATED_AT_DESC
            ) @connection(key: "ReleaseList_releases") {
              totalCount
                edges {
                    node {
                        id
                          ...ReleaseCardItemFragment_release
                    }
                }
            }
          }
        `, queryData.node);

    const edges = data?.releases?.edges ?? [];

    return (
        <Box>
            {edges.length !== 0 ? <Box>
                <Box>
                    <Box sx={{
                        mb: 2,
                        display: 'flex',
                        flexDirection: 'row',
                        justifyContent: 'space-between',
                        [theme.breakpoints.down('md')]: {
                            flexDirection: 'column',
                            alignItems: 'flex-start',
                            '& > *': { mb: 2 },
                        }
                    }}>
                        <Box>
                            <Typography variant="h5" gutterBottom>Releases</Typography>
                            <Typography variant="body2">
                                {DESCRIPTION}
                            </Typography>
                        </Box>
                        <Box>
                            {NewButton}
                        </Box>
                    </Box>
                </Box>
                <InfiniteScroll
                    dataLength={edges.length ?? 0}
                    next={() => loadNext(20)}
                    hasMore={hasNext}
                    loader={<ListSkeleton rowCount={3} />}
                >
                    {edges.map((edge: any) => (
                        <ReleaseCardItem key={edge.node.id} fragmentRef={edge.node} />
                    ))}
                </InfiniteScroll>
            </Box> : <Box sx={{ mt: 4 }} display="flex" justifyContent="center">
                <Box p={4} display="flex" flexDirection="column" justifyContent="center" alignItems="center" sx={{ maxWidth: 600 }}>
                    <Typography variant="h6">Get started by creating a release</Typography>
                    <Typography color="textSecondary" align="center" sx={{ mb: 2 }}>
                        {DESCRIPTION}
                    </Typography>
                    {NewButton}
                </Box>
            </Box>}
        </Box>
    );
}

export default ReleaseList
