import { Box } from '@mui/material';
import graphql from 'babel-plugin-relay/macro';
import ProjectBreadcrumbs from '../ProjectBreadcrumbs';
import ReleaseList from './ReleaseList';
import { useOutletContext } from 'react-router-dom';
import { useFragment } from 'react-relay/hooks';
import { ReleasesFragment_releases$key } from './__generated__/ReleasesFragment_releases.graphql';

export const DESCRIPTION = 'Releases consist of a set of assets, a snapshot of the automation used for deployment, and the current state of the automation.';

function Releases() {
    const context = useOutletContext<ReleasesFragment_releases$key>();

    const data = useFragment<ReleasesFragment_releases$key>(
        graphql`
            fragment ReleasesFragment_releases on Project
            {
                metadata {
                    prn
                }
                ...ReleaseListFragment_project
            }
        `, context);

    return (
        <Box>
            <ProjectBreadcrumbs
                prn={data.metadata.prn}
                childRoutes={[
                    { title: "releases", path: 'releases' }
                ]}
            />
            <ReleaseList fragmentRef={data} />
        </Box>
    );
}

export default Releases
