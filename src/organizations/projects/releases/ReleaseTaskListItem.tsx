import { Box, Button, Stack, TableCell, TableRow, Tooltip } from '@mui/material';
import graphql from 'babel-plugin-relay/macro';
import React, { useMemo, useState } from 'react';
import { useFragment, useMutation } from 'react-relay/hooks';
import ConfirmationDialog from '../../../common/ConfirmationDialog';
import Gravatar from '../../../common/Gravatar';
import Link from '../../../routes/Link';
import PipelineStatusChip from '../pipelines/PipelineStatusChip';
import { ReleaseTaskListItemApproveTaskMutation } from './__generated__/ReleaseTaskListItemApproveTaskMutation.graphql';
import { ReleaseTaskListItemRevokeTaskApprovalMutation } from './__generated__/ReleaseTaskListItemRevokeTaskApprovalMutation.graphql';
import { ReleaseTaskListItemCancelJobMutation } from './__generated__/ReleaseTaskListItemCancelJobMutation.graphql';
import { ReleaseTaskListItemFragment_task$key } from './__generated__/ReleaseTaskListItemFragment_task.graphql';
import { ReleaseTaskListItemRunTaskMutation } from './__generated__/ReleaseTaskListItemRunTaskMutation.graphql';
import { ReleaseTaskListItemDeferTaskMutation } from './__generated__/ReleaseTaskListItemDeferTaskMutation.graphql';
import { ReleaseTaskListItemUndeferTaskMutation } from './__generated__/ReleaseTaskListItemUndeferTaskMutation.graphql';
import { ReleaseTaskListItemRetryTaskMutation } from './__generated__/ReleaseTaskListItemRetryTaskMutation.graphql';
import { useSnackbar } from 'notistack';
import { LoadingButton } from '@mui/lab';
import StyledAvatar from '../../../common/StyledAvatar';
import { useAuth } from 'react-oidc-context';
import PipelineDeferPipelineNodeDialog from '../pipelines/PipelineDeferPipelineNodeDialog';

interface Props {
    fragmentRef: ReleaseTaskListItemFragment_task$key;
    pipelineId: string;
}

function ReleaseTaskListItem({ fragmentRef, pipelineId }: Props) {
    const [showCancelJobConfirmation, setShowCancelJobConfirmation] = useState(false);
    const [showRunTaskConfirmation, setShowRunTaskConfirmation] = useState(false);
    const [showDeferTaskDialog, setShowDeferTaskDialog] = useState(false);
    const [showUndeferTaskConfirmation, setShowUndeferTaskConfirmation] = useState(false);
    const [showRetryTaskConfirmation, setShowRetryTaskConfirmation] = useState(false);
    const { enqueueSnackbar } = useSnackbar();
    const { user } = useAuth();

    const data = useFragment<ReleaseTaskListItemFragment_task$key>(
        graphql`
        fragment ReleaseTaskListItemFragment_task on PipelineTask
        {
            path
            name
            status
            when
            currentJob {
                id
            }
            approvals {
                id
                approver {
                    __typename
                    ...on User {
                        email
                    }
                    ...on ServiceAccount {
                        name
                    }
                }
            }
            approvalStatus
            approvalRules {
                id
                name
                approvalsRequired
            }
        }
    `, fragmentRef);

    const [commitApproveTask, commitApproveTaskInFlight] = useMutation<ReleaseTaskListItemApproveTaskMutation>(graphql`
        mutation ReleaseTaskListItemApproveTaskMutation($input: ApprovePipelineTaskInput!) {
            approvePipelineTask(input: $input) {
                pipeline {
                    ...PipelineDetailsFragment_pipeline
                }
                problems {
                    message
                    field
                    type
                }
            }
        }
    `);

    const [commitRevokeTaskApproval, commitRevokeTaskApprovalInFlight] = useMutation<ReleaseTaskListItemRevokeTaskApprovalMutation>(graphql`
        mutation ReleaseTaskListItemRevokeTaskApprovalMutation($input: RevokePipelineTaskApprovalInput!) {
            revokePipelineTaskApproval(input: $input) {
                pipeline {
                    ...PipelineDetailsFragment_pipeline
                }
                problems {
                    message
                    field
                    type
                }
            }
        }
    `);

    const [commitCancelJob, commitCancelJobInFlight] = useMutation<ReleaseTaskListItemCancelJobMutation>(graphql`
        mutation ReleaseTaskListItemCancelJobMutation($input: CancelJobInput!) {
            cancelJob(input: $input) {
                problems {
                    message
                    field
                    type
                }
            }
        }
    `);

    const [commitRunTask, commitRunTaskInFlight] = useMutation<ReleaseTaskListItemRunTaskMutation>(graphql`
        mutation ReleaseTaskListItemRunTaskMutation($input: RunPipelineTaskInput!) {
            runPipelineTask(input: $input) {
                problems {
                    message
                    field
                    type
                }
            }
        }
    `);

    const [commitDeferTask, commitDeferTaskInFlight] = useMutation<ReleaseTaskListItemDeferTaskMutation>(graphql`
        mutation ReleaseTaskListItemDeferTaskMutation($input: DeferPipelineNodeInput!) {
            deferPipelineNode(input: $input) {
                problems {
                    message
                    field
                    type
                }
            }
        }
    `);

    const [commitUndeferTask, commitUndeferTaskInFlight] = useMutation<ReleaseTaskListItemUndeferTaskMutation>(graphql`
        mutation ReleaseTaskListItemUndeferTaskMutation($input: UndeferPipelineNodeInput!) {
            undeferPipelineNode(input: $input) {
                problems {
                    message
                    field
                    type
                }
            }
        }
    `);

    const [commitRetryTask, commitRetryTaskInFlight] = useMutation<ReleaseTaskListItemRetryTaskMutation>(graphql`
        mutation ReleaseTaskListItemRetryTaskMutation($input: RetryPipelineTaskInput!) {
            retryPipelineTask(input: $input) {
                problems {
                    message
                    field
                    type
                }
            }
        }
    `);

    const handleMutationError = (error: Error, completedCallback?: () => void) => {
        if (completedCallback) {
            completedCallback();
        }
        enqueueSnackbar(`Unexpected error: ${error.message}`, { variant: 'error' });
    }

    const handleMutationProblems = (problems: any, completedCallback?: () => void) => {
        if (completedCallback) {
            completedCallback();
        }
        if (problems && problems.length > 0) {
            enqueueSnackbar(problems.map((problem: any) => problem.message).join('; '), { variant: 'warning' });
        }
    }

    const onApproveTask = () => {
        commitApproveTask({
            variables: {
                input: {
                    pipelineId: pipelineId,
                    taskPath: data.path,
                },
            },
            onCompleted: data => handleMutationProblems(data.approvePipelineTask?.problems),
            onError: handleMutationError
        });
    }

    const onRevokeTaskApproval = () => {
        commitRevokeTaskApproval({
            variables: {
                input: {
                    pipelineId: pipelineId,
                    taskPath: data.path,
                },
            },
            onCompleted: data => handleMutationProblems(data.revokePipelineTaskApproval?.problems),
            onError: handleMutationError
        });
    }

    const onCancelJob = () => {
        if (data.currentJob) {
            commitCancelJob({
                variables: {
                    input: {
                        id: data.currentJob.id
                    },
                },
                onCompleted: data => handleMutationProblems(data.cancelJob?.problems, () => setShowCancelJobConfirmation(false)),
                onError: error => handleMutationError(error, () => setShowCancelJobConfirmation(false))
            });
        }
    }

    const onRunTask = () => {
        commitRunTask({
            variables: {
                input: {
                    pipelineId: pipelineId,
                    taskPath: data.path,
                },
            },
            onCompleted: data => handleMutationProblems(data.runPipelineTask?.problems, () => setShowRunTaskConfirmation(false)),
            onError: error => handleMutationError(error, () => setShowRunTaskConfirmation(false))
        });
    }

    const onDeferTask = (reason: string) => {
        commitDeferTask({
            variables: {
                input: {
                    pipelineId: pipelineId,
                    nodePath: data.path,
                    nodeType: 'TASK',
                    reason: reason,
                },
            },
            onCompleted: data => handleMutationProblems(data.deferPipelineNode?.problems, () => setShowDeferTaskDialog(false)),
            onError: error => handleMutationError(error, () => setShowDeferTaskDialog(false))
        });
    }

    const onUndeferTask = () => {
        commitUndeferTask({
            variables: {
                input: {
                    pipelineId: pipelineId,
                    nodePath: data.path,
                    nodeType: 'TASK',
                },
            },
            onCompleted: data => handleMutationProblems(data.undeferPipelineNode?.problems, () => setShowUndeferTaskConfirmation(false)),
            onError: error => handleMutationError(error, () => setShowUndeferTaskConfirmation(false))
        });
    }

    const onRetryTask = () => {
        commitRetryTask({
            variables: {
                input: {
                    pipelineId: pipelineId,
                    taskPath: data.path,
                },
            },
            onCompleted: data => handleMutationProblems(data.retryPipelineTask?.problems, () => setShowRetryTaskConfirmation(false)),
            onError: error => handleMutationError(error, () => setShowRetryTaskConfirmation(false))
        });
    }

    const userApprovedTask = useMemo(() => {
        return data?.approvals.map(approval => approval.approver).find(approver => approver?.__typename === 'User' && approver.email === user?.profile.email) ? true : false;
    }, [data]);

    const actions = useMemo(() => [
        {
            label: 'Cancel',
            condition: data.status === 'RUNNING',
            handler: () => setShowCancelJobConfirmation(true),
        },
        {
            label: 'Start',
            condition: data.status === 'READY' && data.when === 'manual',
            handler: () => setShowRunTaskConfirmation(true),
        },
        {
            label: 'Defer',
            condition: ['READY', 'WAITING'].includes(data.status),
            handler: () => setShowDeferTaskDialog(true),
        },
        {
            label: 'Restore',
            condition: data.status === 'DEFERRED',
            handler: () => setShowUndeferTaskConfirmation(true),
        },
        {
            label: 'Retry',
            condition: ['SUCCEEDED', 'FAILED', 'CANCELED'].includes(data.status),
            handler: () => setShowRetryTaskConfirmation(true),
        }
    ], [data]);

    return (
        <React.Fragment>
            <TableRow
                key={data.path}
                sx={{ '&:last-child td, &:last-child th': { border: 0 } }}
            >
                <TableCell>
                    <PipelineStatusChip to={`../../-/pipelines/${pipelineId}/task/${data.path}`} status={data.status} />
                </TableCell>
                <TableCell>
                    <Link color="inherit" to={`../../-/pipelines/${pipelineId}/task/${data.path}`}>{data.name}</Link>
                </TableCell>
                <TableCell>
                    {data.approvalRules?.length > 0 ? `${data.approvalRules?.map(approvalRule => approvalRule.name).join(', ')}` : 'Not Required'}
                </TableCell>
                <TableCell>
                    <Stack direction="row" marginLeft={1}>
                        {data.approvals.map(approval => (
                            <React.Fragment key={approval.id}>
                                {approval.approver?.__typename === 'User' && <Tooltip title={approval.approver?.email}>
                                    <Box><Gravatar width={20} height={20} email={approval.approver.email} /></Box>
                                </Tooltip>}
                                {approval.approver?.__typename === 'ServiceAccount' && <Tooltip title={approval.approver?.name}>
                                    <StyledAvatar>{approval.approver.name[0].toUpperCase()}</StyledAvatar>
                                </Tooltip>}
                            </React.Fragment>
                        ))}
                        {data.approvals.length === 0 && 'None'}
                    </Stack>
                </TableCell>
                <TableCell>
                    <Stack direction="row" spacing={1} justifyContent="flex-end">
                        {actions.map((action, index) => (
                            action.condition && (
                                <Button key={index} color="info" size="small" variant="outlined" onClick={action.handler}>
                                    {action.label}
                                </Button>
                            )
                        ))}
                        {data.status === 'APPROVAL_PENDING' && !userApprovedTask && <LoadingButton size="small" loading={commitApproveTaskInFlight} variant="outlined" color="primary" onClick={onApproveTask}>Approve</LoadingButton>}
                        {data.status === 'APPROVAL_PENDING' && userApprovedTask && <LoadingButton size="small" loading={commitRevokeTaskApprovalInFlight} variant="outlined" color="primary" onClick={onRevokeTaskApproval}>Revoke Approval</LoadingButton>}
                    </Stack>
                </TableCell>
            </TableRow >
            {showRunTaskConfirmation && <ConfirmationDialog
                title="Run Task"
                message={<React.Fragment>Are you sure you want to run this task?</React.Fragment>}
                confirmButtonLabel="Run"
                opInProgress={commitRunTaskInFlight}
                onConfirm={onRunTask}
                onClose={() => setShowRunTaskConfirmation(false)}
            />}
            {showCancelJobConfirmation && <ConfirmationDialog
                title="Cancel Job"
                message={<React.Fragment>Are you sure you want to cancel this job?</React.Fragment>}
                confirmButtonLabel="Cancel"
                opInProgress={commitCancelJobInFlight}
                onConfirm={onCancelJob}
                onClose={() => setShowCancelJobConfirmation(false)}
            />}
            {showDeferTaskDialog && <PipelineDeferPipelineNodeDialog
                title="Defer Task"
                message={<React.Fragment>Are you sure you want to defer this task?</React.Fragment>}
                onClose={() => setShowDeferTaskDialog(false)}
                onDefer={onDeferTask}
                inProgress={commitDeferTaskInFlight}
            />}
            {showUndeferTaskConfirmation && <ConfirmationDialog
                title="Restore Task"
                message={<React.Fragment>Are you sure you want to restore this deferred task?</React.Fragment>}
                confirmButtonLabel="Restore"
                opInProgress={commitUndeferTaskInFlight}
                onConfirm={onUndeferTask}
                onClose={() => setShowUndeferTaskConfirmation(false)}
            />}
            {showRetryTaskConfirmation && <ConfirmationDialog
                title="Retry Task"
                message={<React.Fragment>Are you sure you want to retry this task?</React.Fragment>}
                confirmButtonLabel="Retry"
                opInProgress={commitRetryTaskInFlight}
                onConfirm={onRetryTask}
                onClose={() => setShowRetryTaskConfirmation(false)}
            />}
        </React.Fragment>
    );
}

export default ReleaseTaskListItem;
