import { Avatar, Box, CircularProgress, Divider, Paper, Stack, ToggleButton, Typography, useTheme } from '@mui/material';
import { purple } from '@mui/material/colors';
import graphql from 'babel-plugin-relay/macro';
import { Suspense, useContext } from 'react';
import { useFragment } from 'react-relay/hooks';
import { useOutletContext, useSearchParams } from 'react-router-dom';
import { UserContext } from '../../UserContext';
import HomeToDoList from '../../home/HomeToDoList';
import ProjectActivity from './ProjectActivity';
import ProjectBreadcrumbs from './ProjectBreadcrumbs';
import PRNButton from '../../common/PRNButton';
import ProjectDetailsIndexEnvironments from './ProjectDetailsIndexEnvironments';
import ProjectDetailsIndexPipelineList from './ProjectDetailsIndexPipelineList';
import { ProjectDetailsIndexFragment_project$key } from './__generated__/ProjectDetailsIndexFragment_project.graphql';

function ProjectDetailsIndex() {
    const theme = useTheme();
    const user = useContext(UserContext);
    const context = useOutletContext<ProjectDetailsIndexFragment_project$key>();
    const [searchParams, setSearchParams] = useSearchParams();

    // This filter will only show the current users activity when true
    const showMyActivity = searchParams.get('filter') === 'myActivity';

    const data = useFragment<ProjectDetailsIndexFragment_project$key>(graphql`
        fragment ProjectDetailsIndexFragment_project on Project
        {
            id
            name
            description
            metadata {
                prn
            }
            ...ProjectActivityFragment_project
        }
    `, context);

    return (
        <Box>
            <ProjectBreadcrumbs prn={data.metadata.prn} />
            <Box sx={{
                display: 'flex',
                flexDirection: 'row',
                justifyContent: 'space-between',
                [theme.breakpoints.down('sm')]: {
                    flexDirection: 'column',
                    alignItems: 'flex-start',
                    '& > *': { marginBottom: 2 },
                }
            }}>
                <Box display="flex" alignItems="center">
                    <Avatar sx={{ width: 40, height: 40, marginRight: 2, bgcolor: purple[300] }} variant="rounded">
                        {data.name[0].toUpperCase()}
                    </Avatar>
                    <Stack>
                        <Typography noWrap variant="h5" sx={{ maxWidth: 400, fontWeight: "bold" }}>{data.name}</Typography>
                        <Typography color="textSecondary" variant="subtitle2">
                            {`${data.description.slice(0, 50)}${data.description.length > 50 ? '...' : ''}`}
                        </Typography>
                    </Stack>
                </Box>
                <PRNButton prn={data.metadata.prn} />
            </Box>
            <Divider sx={{ mt: 2, mb: 2 }} />
            <Box sx={{
                maxWidth: 1400,
                margin: 'auto',
                display: 'flex',
                [theme.breakpoints.down('lg')]: {
                    flexDirection: 'column-reverse',
                    alignItems: 'flex-start',
                    '& > *': { mb: 4 }
                }
            }}>
                <Box flex={1}>
                    <Box display="flex" justifyContent="space-between" alignItems="center">
                        <Typography variant="h6" fontWeight={600} mb={1}>Activity</Typography>
                        <ToggleButton
                            onChange={() => setSearchParams(!showMyActivity ? { filter: 'myActivity' } : {}, { replace: true })}
                            color="secondary"
                            selected={showMyActivity}
                            size="small"
                            value="myActivity">
                            My Activity
                        </ToggleButton>
                    </Box>
                    <Suspense fallback={<Box
                        sx={{
                            width: '100%',
                            height: `calc(100vh - 64px)`,
                            display: 'flex',
                            alignItems: 'center',
                            justifyContent: 'center'
                        }}>
                        <CircularProgress />
                    </Box>}>
                        <ProjectActivity userId={showMyActivity ? user.id : undefined} fragmentRef={data} />
                    </Suspense>
                </Box>
                <Box sx={{
                    width: '100%',
                    [theme.breakpoints.up('lg')]: {
                        width: 400,
                        maxWidth: 400,
                        ml: 4
                    }
                }}>
                    <Paper sx={{ padding: 2, mb: 3 }}>
                        <HomeToDoList
                            projectId={data.id}
                            noResultsText="You do not have any To-Do items in this project. Any item that requires your attention will automatically show up here."
                        />
                    </Paper>
                    <Paper sx={{ padding: 2, mb: 3 }}>
                        <ProjectDetailsIndexEnvironments projectId={data.id} />
                    </Paper>
                    <Paper sx={{ padding: 2 }}>
                        <ProjectDetailsIndexPipelineList projectId={data.id} />
                    </Paper>
                </Box>
            </Box>
        </Box>
    );
}

export default ProjectDetailsIndex
