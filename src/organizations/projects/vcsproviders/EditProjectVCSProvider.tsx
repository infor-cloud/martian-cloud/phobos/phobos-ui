import { Box } from '@mui/material';
import { useOutletContext, useParams } from 'react-router-dom';
import ProjectBreadcrumbs from '../ProjectBreadcrumbs';
import graphql from 'babel-plugin-relay/macro';
import { useFragment, useLazyLoadQuery } from "react-relay/hooks";
import EditVCSProvider from '../../../vcsproviders/EditVCSProvider';
import { EditProjectVCSProviderFragment_project$key } from './__generated__/EditProjectVCSProviderFragment_project.graphql';
import { EditProjectVCSProviderQuery } from './__generated__/EditProjectVCSProviderQuery.graphql';

function EditProjectVCSProvider() {
    const vcsProviderId = useParams().vcsProviderId as string;
    const context = useOutletContext<EditProjectVCSProviderFragment_project$key>();

    const project = useFragment<EditProjectVCSProviderFragment_project$key>(graphql`
        fragment EditProjectVCSProviderFragment_project on Project
            {
                metadata {
                    prn
                }
            }
    `, context);

    const queryData = useLazyLoadQuery<EditProjectVCSProviderQuery>(graphql`
        query EditProjectVCSProviderQuery($id: String!) {
            node(id: $id) {
                ... on VCSProvider {
                    name
                    ...EditVCSProviderFragment_vcsProvider
                }
            }
        }
    `, { id: vcsProviderId });

    const vcsProvider = queryData.node as any;

    return queryData.node ? (
        <Box>
            <ProjectBreadcrumbs
                prn={project.metadata.prn}
                childRoutes={[
                    { title: "vcs providers", path: 'vcs_providers' },
                    { title: vcsProvider.name, path: vcsProviderId },
                    { title: "edit", path: 'edit' },
                ]}
            />
            <EditVCSProvider fragmentRef={queryData.node} />
        </Box>
    ) : <Box>VCS Provider Not found</Box>;
}

export default EditProjectVCSProvider;
