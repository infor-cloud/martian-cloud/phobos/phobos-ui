import { Box, Typography } from '@mui/material'
import ProjectBreadcrumbs from '../ProjectBreadcrumbs';
import { useFragment, useLazyLoadQuery } from 'react-relay/hooks';
import { useOutletContext, useParams } from 'react-router-dom';
import graphql from 'babel-plugin-relay/macro';
import VCSProviderDetails from '../../../vcsproviders/VCSProviderDetails';
import { ProjectVCSProviderDetailsQuery } from './__generated__/ProjectVCSProviderDetailsQuery.graphql';
import { ProjectVCSProviderDetailsFragment_project$key } from './__generated__/ProjectVCSProviderDetailsFragment_project.graphql';

function ProjectVCSProviderDetails() {
    const context = useOutletContext<ProjectVCSProviderDetailsFragment_project$key>();
    const vcsProviderId = useParams().vcsProviderId as string;

    const project = useFragment<ProjectVCSProviderDetailsFragment_project$key>(
        graphql`
            fragment ProjectVCSProviderDetailsFragment_project on Project
            {
                metadata {
                    prn
                }
            }
        `, context
    );

    const data = useLazyLoadQuery<ProjectVCSProviderDetailsQuery>(graphql`
        query ProjectVCSProviderDetailsQuery($id: String!) {
            node(id: $id) {
                ... on VCSProvider {
                    id
                    name
                    ...VCSProviderDetailsFragment_vcsProvider
                }
            }
        }
    `, { id: vcsProviderId }, { fetchPolicy: 'store-and-network' });

    if (data.node) {

        const vcsProvider = data.node as any;

        return (
            <Box>
                <ProjectBreadcrumbs
                    prn={project.metadata.prn}
                    childRoutes={[
                        { title: "vcs providers", path: 'vcs_providers' },
                        { title: vcsProvider.name, path: vcsProvider.id }
                    ]}
                />
                <VCSProviderDetails fragmentRef={data.node} />
            </Box>
        );
    }
    else {
        return <Box display="flex" justifyContent="center" marginTop={4}>
            <Typography color="textSecondary">VCS Provider with ID {vcsProviderId} not found</Typography>
        </Box>;
    }
}

export default ProjectVCSProviderDetails;
