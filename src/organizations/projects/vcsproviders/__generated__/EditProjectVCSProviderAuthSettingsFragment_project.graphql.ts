/**
 * @generated SignedSource<<4790b0d30c99601a27a0aaca8f0f872f>>
 * @lightSyntaxTransform
 * @nogrep
 */

/* tslint:disable */
/* eslint-disable */
// @ts-nocheck

import { Fragment, ReaderFragment } from 'relay-runtime';
import { FragmentRefs } from "relay-runtime";
export type EditProjectVCSProviderAuthSettingsFragment_project$data = {
  readonly metadata: {
    readonly prn: string;
  };
  readonly name: string;
  readonly " $fragmentType": "EditProjectVCSProviderAuthSettingsFragment_project";
};
export type EditProjectVCSProviderAuthSettingsFragment_project$key = {
  readonly " $data"?: EditProjectVCSProviderAuthSettingsFragment_project$data;
  readonly " $fragmentSpreads": FragmentRefs<"EditProjectVCSProviderAuthSettingsFragment_project">;
};

const node: ReaderFragment = {
  "argumentDefinitions": [],
  "kind": "Fragment",
  "metadata": null,
  "name": "EditProjectVCSProviderAuthSettingsFragment_project",
  "selections": [
    {
      "alias": null,
      "args": null,
      "kind": "ScalarField",
      "name": "name",
      "storageKey": null
    },
    {
      "alias": null,
      "args": null,
      "concreteType": "ResourceMetadata",
      "kind": "LinkedField",
      "name": "metadata",
      "plural": false,
      "selections": [
        {
          "alias": null,
          "args": null,
          "kind": "ScalarField",
          "name": "prn",
          "storageKey": null
        }
      ],
      "storageKey": null
    }
  ],
  "type": "Project",
  "abstractKey": null
};

(node as any).hash = "b8cdc7338a2524e31efc80cfac907169";

export default node;
