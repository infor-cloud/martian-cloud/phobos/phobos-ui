/**
 * @generated SignedSource<<ef63c7564853c9d3a5a7de07fd34f8df>>
 * @lightSyntaxTransform
 * @nogrep
 */

/* tslint:disable */
/* eslint-disable */
// @ts-nocheck

import { ConcreteRequest, Query } from 'relay-runtime';
import { FragmentRefs } from "relay-runtime";
export type EditProjectVCSProviderQuery$variables = {
  id: string;
};
export type EditProjectVCSProviderQuery$data = {
  readonly node: {
    readonly name?: string;
    readonly " $fragmentSpreads": FragmentRefs<"EditVCSProviderFragment_vcsProvider">;
  } | null | undefined;
};
export type EditProjectVCSProviderQuery = {
  response: EditProjectVCSProviderQuery$data;
  variables: EditProjectVCSProviderQuery$variables;
};

const node: ConcreteRequest = (function(){
var v0 = [
  {
    "defaultValue": null,
    "kind": "LocalArgument",
    "name": "id"
  }
],
v1 = [
  {
    "kind": "Variable",
    "name": "id",
    "variableName": "id"
  }
],
v2 = {
  "alias": null,
  "args": null,
  "kind": "ScalarField",
  "name": "name",
  "storageKey": null
};
return {
  "fragment": {
    "argumentDefinitions": (v0/*: any*/),
    "kind": "Fragment",
    "metadata": null,
    "name": "EditProjectVCSProviderQuery",
    "selections": [
      {
        "alias": null,
        "args": (v1/*: any*/),
        "concreteType": null,
        "kind": "LinkedField",
        "name": "node",
        "plural": false,
        "selections": [
          {
            "kind": "InlineFragment",
            "selections": [
              (v2/*: any*/),
              {
                "args": null,
                "kind": "FragmentSpread",
                "name": "EditVCSProviderFragment_vcsProvider"
              }
            ],
            "type": "VCSProvider",
            "abstractKey": null
          }
        ],
        "storageKey": null
      }
    ],
    "type": "Query",
    "abstractKey": null
  },
  "kind": "Request",
  "operation": {
    "argumentDefinitions": (v0/*: any*/),
    "kind": "Operation",
    "name": "EditProjectVCSProviderQuery",
    "selections": [
      {
        "alias": null,
        "args": (v1/*: any*/),
        "concreteType": null,
        "kind": "LinkedField",
        "name": "node",
        "plural": false,
        "selections": [
          {
            "alias": null,
            "args": null,
            "kind": "ScalarField",
            "name": "__typename",
            "storageKey": null
          },
          {
            "alias": null,
            "args": null,
            "kind": "ScalarField",
            "name": "id",
            "storageKey": null
          },
          {
            "kind": "InlineFragment",
            "selections": [
              (v2/*: any*/),
              {
                "alias": null,
                "args": null,
                "kind": "ScalarField",
                "name": "description",
                "storageKey": null
              },
              {
                "alias": null,
                "args": null,
                "kind": "ScalarField",
                "name": "url",
                "storageKey": null
              },
              {
                "alias": null,
                "args": null,
                "kind": "ScalarField",
                "name": "type",
                "storageKey": null
              }
            ],
            "type": "VCSProvider",
            "abstractKey": null
          }
        ],
        "storageKey": null
      }
    ]
  },
  "params": {
    "cacheID": "b3a4388f496dae37abd58cc0393fdbf9",
    "id": null,
    "metadata": {},
    "name": "EditProjectVCSProviderQuery",
    "operationKind": "query",
    "text": "query EditProjectVCSProviderQuery(\n  $id: String!\n) {\n  node(id: $id) {\n    __typename\n    ... on VCSProvider {\n      name\n      ...EditVCSProviderFragment_vcsProvider\n    }\n    id\n  }\n}\n\nfragment EditVCSProviderFragment_vcsProvider on VCSProvider {\n  id\n  name\n  description\n  url\n  type\n}\n"
  }
};
})();

(node as any).hash = "d51f80414346667e6ca2bea51c4f1f0f";

export default node;
