/**
 * @generated SignedSource<<99cbe12adc95d083a0fea2b4a72df593>>
 * @lightSyntaxTransform
 * @nogrep
 */

/* tslint:disable */
/* eslint-disable */
// @ts-nocheck

import { Fragment, ReaderFragment } from 'relay-runtime';
import { FragmentRefs } from "relay-runtime";
export type ProjectVCSProviderDetailsFragment_project$data = {
  readonly metadata: {
    readonly prn: string;
  };
  readonly " $fragmentType": "ProjectVCSProviderDetailsFragment_project";
};
export type ProjectVCSProviderDetailsFragment_project$key = {
  readonly " $data"?: ProjectVCSProviderDetailsFragment_project$data;
  readonly " $fragmentSpreads": FragmentRefs<"ProjectVCSProviderDetailsFragment_project">;
};

const node: ReaderFragment = {
  "argumentDefinitions": [],
  "kind": "Fragment",
  "metadata": null,
  "name": "ProjectVCSProviderDetailsFragment_project",
  "selections": [
    {
      "alias": null,
      "args": null,
      "concreteType": "ResourceMetadata",
      "kind": "LinkedField",
      "name": "metadata",
      "plural": false,
      "selections": [
        {
          "alias": null,
          "args": null,
          "kind": "ScalarField",
          "name": "prn",
          "storageKey": null
        }
      ],
      "storageKey": null
    }
  ],
  "type": "Project",
  "abstractKey": null
};

(node as any).hash = "6ad33a59675e23831c427b26156bbc90";

export default node;
