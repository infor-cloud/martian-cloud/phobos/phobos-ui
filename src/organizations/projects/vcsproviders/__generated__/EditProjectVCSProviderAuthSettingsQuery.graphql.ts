/**
 * @generated SignedSource<<f8e77384ce345a83f9b52908ba918816>>
 * @lightSyntaxTransform
 * @nogrep
 */

/* tslint:disable */
/* eslint-disable */
// @ts-nocheck

import { ConcreteRequest, Query } from 'relay-runtime';
import { FragmentRefs } from "relay-runtime";
export type VCSProviderAuthType = "ACCESS_TOKEN" | "OAUTH" | "%future added value";
export type VCSProviderType = "GITHUB" | "GITLAB" | "%future added value";
export type EditProjectVCSProviderAuthSettingsQuery$variables = {
  id: string;
};
export type EditProjectVCSProviderAuthSettingsQuery$data = {
  readonly node: {
    readonly authType?: VCSProviderAuthType;
    readonly id?: string;
    readonly name?: string;
    readonly type?: VCSProviderType;
    readonly " $fragmentSpreads": FragmentRefs<"EditVCSProviderAuthSettingsFragment_vcsProvider">;
  } | null | undefined;
};
export type EditProjectVCSProviderAuthSettingsQuery = {
  response: EditProjectVCSProviderAuthSettingsQuery$data;
  variables: EditProjectVCSProviderAuthSettingsQuery$variables;
};

const node: ConcreteRequest = (function(){
var v0 = [
  {
    "defaultValue": null,
    "kind": "LocalArgument",
    "name": "id"
  }
],
v1 = [
  {
    "kind": "Variable",
    "name": "id",
    "variableName": "id"
  }
],
v2 = {
  "alias": null,
  "args": null,
  "kind": "ScalarField",
  "name": "id",
  "storageKey": null
},
v3 = {
  "alias": null,
  "args": null,
  "kind": "ScalarField",
  "name": "name",
  "storageKey": null
},
v4 = {
  "alias": null,
  "args": null,
  "kind": "ScalarField",
  "name": "type",
  "storageKey": null
},
v5 = {
  "alias": null,
  "args": null,
  "kind": "ScalarField",
  "name": "authType",
  "storageKey": null
};
return {
  "fragment": {
    "argumentDefinitions": (v0/*: any*/),
    "kind": "Fragment",
    "metadata": null,
    "name": "EditProjectVCSProviderAuthSettingsQuery",
    "selections": [
      {
        "alias": null,
        "args": (v1/*: any*/),
        "concreteType": null,
        "kind": "LinkedField",
        "name": "node",
        "plural": false,
        "selections": [
          {
            "kind": "InlineFragment",
            "selections": [
              (v2/*: any*/),
              (v3/*: any*/),
              (v4/*: any*/),
              (v5/*: any*/),
              {
                "args": null,
                "kind": "FragmentSpread",
                "name": "EditVCSProviderAuthSettingsFragment_vcsProvider"
              }
            ],
            "type": "VCSProvider",
            "abstractKey": null
          }
        ],
        "storageKey": null
      }
    ],
    "type": "Query",
    "abstractKey": null
  },
  "kind": "Request",
  "operation": {
    "argumentDefinitions": (v0/*: any*/),
    "kind": "Operation",
    "name": "EditProjectVCSProviderAuthSettingsQuery",
    "selections": [
      {
        "alias": null,
        "args": (v1/*: any*/),
        "concreteType": null,
        "kind": "LinkedField",
        "name": "node",
        "plural": false,
        "selections": [
          {
            "alias": null,
            "args": null,
            "kind": "ScalarField",
            "name": "__typename",
            "storageKey": null
          },
          (v2/*: any*/),
          {
            "kind": "InlineFragment",
            "selections": [
              (v3/*: any*/),
              (v4/*: any*/),
              (v5/*: any*/),
              {
                "alias": null,
                "args": null,
                "kind": "ScalarField",
                "name": "extraOAuthScopes",
                "storageKey": null
              }
            ],
            "type": "VCSProvider",
            "abstractKey": null
          }
        ],
        "storageKey": null
      }
    ]
  },
  "params": {
    "cacheID": "aa162037bb742c232436374bb2e49ca6",
    "id": null,
    "metadata": {},
    "name": "EditProjectVCSProviderAuthSettingsQuery",
    "operationKind": "query",
    "text": "query EditProjectVCSProviderAuthSettingsQuery(\n  $id: String!\n) {\n  node(id: $id) {\n    __typename\n    ... on VCSProvider {\n      id\n      name\n      type\n      authType\n      ...EditVCSProviderAuthSettingsFragment_vcsProvider\n    }\n    id\n  }\n}\n\nfragment EditVCSProviderAuthSettingsFragment_vcsProvider on VCSProvider {\n  id\n  name\n  type\n  authType\n  extraOAuthScopes\n  ...VCSProviderAuthSettingsForm_vcsProvider\n}\n\nfragment VCSProviderAuthSettingsForm_vcsProvider on VCSProvider {\n  type\n  authType\n}\n"
  }
};
})();

(node as any).hash = "4d49d941fbf922888a3ea634c9184737";

export default node;
