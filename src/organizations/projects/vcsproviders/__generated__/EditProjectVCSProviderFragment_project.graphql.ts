/**
 * @generated SignedSource<<2f60bd514e9859601713503dc26ad1e4>>
 * @lightSyntaxTransform
 * @nogrep
 */

/* tslint:disable */
/* eslint-disable */
// @ts-nocheck

import { Fragment, ReaderFragment } from 'relay-runtime';
import { FragmentRefs } from "relay-runtime";
export type EditProjectVCSProviderFragment_project$data = {
  readonly metadata: {
    readonly prn: string;
  };
  readonly " $fragmentType": "EditProjectVCSProviderFragment_project";
};
export type EditProjectVCSProviderFragment_project$key = {
  readonly " $data"?: EditProjectVCSProviderFragment_project$data;
  readonly " $fragmentSpreads": FragmentRefs<"EditProjectVCSProviderFragment_project">;
};

const node: ReaderFragment = {
  "argumentDefinitions": [],
  "kind": "Fragment",
  "metadata": null,
  "name": "EditProjectVCSProviderFragment_project",
  "selections": [
    {
      "alias": null,
      "args": null,
      "concreteType": "ResourceMetadata",
      "kind": "LinkedField",
      "name": "metadata",
      "plural": false,
      "selections": [
        {
          "alias": null,
          "args": null,
          "kind": "ScalarField",
          "name": "prn",
          "storageKey": null
        }
      ],
      "storageKey": null
    }
  ],
  "type": "Project",
  "abstractKey": null
};

(node as any).hash = "037f4873d8722e70f7295022d67e2fca";

export default node;
