import { Box } from "@mui/material";
import graphql from 'babel-plugin-relay/macro';
import { useParams, useOutletContext } from 'react-router-dom';
import { useFragment, useLazyLoadQuery } from "react-relay/hooks";
import ProjectBreadcrumbs from '../ProjectBreadcrumbs';
import EditVCSProviderAuthSettings from '../../../vcsproviders/EditVCSProviderAuthSettings';
import { EditProjectVCSProviderAuthSettingsFragment_project$key } from './__generated__/EditProjectVCSProviderAuthSettingsFragment_project.graphql';
import { EditProjectVCSProviderAuthSettingsQuery } from './__generated__/EditProjectVCSProviderAuthSettingsQuery.graphql';

function EditProjectVCSProviderAuthSettings() {
    const vcsProviderId = useParams().vcsProviderId as string;
    const context = useOutletContext<EditProjectVCSProviderAuthSettingsFragment_project$key>();

    const project = useFragment<EditProjectVCSProviderAuthSettingsFragment_project$key>(
        graphql`
        fragment EditProjectVCSProviderAuthSettingsFragment_project on Project
        {
            name
            metadata {
                prn
            }
        }
        `, context
    );

    const queryData = useLazyLoadQuery<EditProjectVCSProviderAuthSettingsQuery>(graphql`
        query EditProjectVCSProviderAuthSettingsQuery($id: String!) {
            node(id: $id) {
                ... on VCSProvider {
                    id
                    name
                    type
                    authType
                    ...EditVCSProviderAuthSettingsFragment_vcsProvider
                }
            }
        }
    `, { id: vcsProviderId });

    const vcsProvider = queryData.node as any;

    return (
        <Box>
            <ProjectBreadcrumbs
                prn={project.metadata.prn}
                childRoutes={[
                    { title: "vcs providers", path: 'vcs_providers' },
                    { title: vcsProvider.name, path: vcsProvider.id },
                    { title: "edit auth settings", path: 'edit_auth_settings' },
                ]}
            />
            <EditVCSProviderAuthSettings fragmentRef={vcsProvider} />
        </Box>
    );
}

export default EditProjectVCSProviderAuthSettings;
