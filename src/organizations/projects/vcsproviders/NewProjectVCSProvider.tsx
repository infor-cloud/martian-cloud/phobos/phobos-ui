import { Box } from '@mui/material'
import { useOutletContext } from 'react-router-dom';
import { useFragment } from 'react-relay/hooks';
import graphql from 'babel-plugin-relay/macro';
import ProjectBreadcrumbs from '../ProjectBreadcrumbs';
import NewVCSProvider from '../../../vcsproviders/NewVCSProvider';
import { GetConnections } from './ProjectVCSProviders';
import { NewProjectVCSProviderFragment_project$key } from './__generated__/NewProjectVCSProviderFragment_project.graphql';

function NewProjectVCSProvider() {
    const context = useOutletContext<NewProjectVCSProviderFragment_project$key>();

    const project = useFragment<NewProjectVCSProviderFragment_project$key>(
        graphql`
        fragment NewProjectVCSProviderFragment_project on Project
            {
                id
                name
                metadata {
                    prn
                }
            }
        `, context
    );

    return (
        <Box>
            <ProjectBreadcrumbs
                prn={project.metadata.prn}
                childRoutes={[
                    { title: "vcs providers", path: 'vcs_providers' },
                    { title: "new", path: 'new' },
                ]}
            />
            <NewVCSProvider scope='PROJECT' id={project.id} getConnections={GetConnections} />
        </Box>
    );
}

export default NewProjectVCSProvider;
