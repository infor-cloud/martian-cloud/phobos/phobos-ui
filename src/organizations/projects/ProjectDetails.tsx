import { Box, CircularProgress, Typography } from '@mui/material';
import graphql from 'babel-plugin-relay/macro';
import { Suspense, useEffect, useMemo } from 'react';
import { PreloadedQuery, useFragment, usePreloadedQuery, useQueryLoader } from 'react-relay/hooks';
import { Outlet, matchPath, useLocation, useParams } from 'react-router-dom';
import ProjectDetailsDrawer from './ProjectDetailsDrawer';
import { ProjectDetailsFragment_project$key } from './__generated__/ProjectDetailsFragment_project.graphql';
import { ProjectDetailsQuery } from './__generated__/ProjectDetailsQuery.graphql';

const query = graphql`
    query ProjectDetailsQuery($prn: String!) {
        node(id: $prn) {
            ... on Project {
                ...ProjectDetailsFragment_project
            }
        }
    }
`;

function ProjectDetailsEntryPoint() {
    const { orgName, projectName } = useParams() as { orgName: string, projectName: string };

    const [queryRef, loadQuery] = useQueryLoader<ProjectDetailsQuery>(query);

    useEffect(() => {
        loadQuery({ prn: `prn:project:${orgName}/${projectName}` }, { fetchPolicy: 'store-and-network' });
    }, [loadQuery])

    return queryRef != null ? <ProjectDetailsContainer queryRef={queryRef} projectName={projectName} /> : null;
}

interface ProjectDetailsContainerProps {
    queryRef: PreloadedQuery<ProjectDetailsQuery>
    projectName: string
}

function ProjectDetailsContainer({ queryRef, projectName }: ProjectDetailsContainerProps) {
    const queryData = usePreloadedQuery<ProjectDetailsQuery>(query, queryRef)

    return queryData.node ? (
        <ProjectDetails fragmentRef={queryData.node} />
    ) : (
        <Box display="flex" justifyContent="center" pt={4}>
            <Typography color="textSecondary">Project {projectName} not found</Typography>
        </Box>
    );
}

interface ProjectDetailsProps {
    fragmentRef: ProjectDetailsFragment_project$key
}

function ProjectDetails({ fragmentRef }: ProjectDetailsProps) {
    const { pathname } = useLocation();

    const data = useFragment<ProjectDetailsFragment_project$key>(
        graphql`
          fragment ProjectDetailsFragment_project on Project
          {
            name
            ...ProjectDetailsIndexFragment_project
            ...ProjectSettingsFragment_project
            ...ReleasesFragment_releases
            ...NewReleaseFragment_releases
            ...ProjectReleaseLifecyclesFragment_project
            ...ProjectReleaseLifecycleDetailsFragment_project
            ...NewProjectReleaseLifecycleFragment_project
            ...EditProjectReleaseLifecycleFragment_project
            ...ProjectServiceAccountsFragment_project
            ...NewProjectServiceAccountFragment_project
            ...ProjectServiceAccountDetailsFragment_project
            ...EditProjectServiceAccountFragment_project
            ...ProjectApprovalRulesFragment_project
            ...NewProjectApprovalRuleFragment_project
            ...EditProjectApprovalRuleFragment_project
            ...EditReleaseFragment_project
            ...EditReleaseDeploymentFragment_project
            ...ProjectMembershipsFragment_project
            ...NewProjectMembershipFragment_memberships
            ...PipelinesFragment_pipelines
            ...CreatePipelineFragment_project
            ...PipelineDetailsFragment_details
            ...PipelineTemplatesFragment_pipelines
            ...PipelineTemplateDetailsFragment_pipelines
            ...NewPipelineTemplateFragment_pipelines
            ...NewPipelineTemplateVersionFragment_project
            ...DeploymentTemplateListItem_project
            ...ProjectActivityFragment_project
            ...EnvironmentsFragment_project
            ...NewEnvironmentFragment_project
            ...EnvironmentDetailsFragment_project
            ...EditEnvironmentFragment_project
            ...ProjectVCSProvidersFragment_project
            ...NewProjectVCSProviderFragment_project
            ...ProjectVCSProviderDetailsFragment_project
            ...EditProjectVCSProviderFragment_project
            ...EditProjectVCSProviderAuthSettingsFragment_project
            ...NewProjectEnvironmentRuleFragment_project
            ...EditProjectEnvironmentRuleFragment_project
            ...ProjectInsightsFragment_project
            ...ProjectVariablesFragment_project
            ...EditProjectVariablesFragment_project
            ...NewProjectVariablesFragment_project
          }
    `, fragmentRef);

    const showExpandedPage = useMemo(() => matchPath('organizations/:orgName/projects/:projectName/-/releases/:releaseId', pathname) ||
        matchPath('organizations/:orgName/projects/:projectName/-/pipelines/:pipelineId/task/:taskPath', pathname) ||
        matchPath('organizations/:orgName/projects/:projectName/-/pipelines/:pipelineId/nested_pipeline/:pipelinePath', pathname), [pathname]);

    const hideDrawer = useMemo(() => matchPath('organizations/:orgName/projects/:projectName/-/pipelines/:pipelineId/task/:taskPath', pathname) ||
        matchPath('organizations/:orgName/projects/:projectName/-/pipelines/:pipelineId/nested_pipeline/:pipelinePath', pathname), [pathname]);

    return (
        <Box display="flex">
            {!hideDrawer && <ProjectDetailsDrawer name={data.name} />}
            <Box component="main" sx={{ flexGrow: 1 }}>
                <Suspense fallback={<Box
                    sx={{
                        width: '100%',
                        height: `calc(100vh - 64px)`,
                        display: 'flex',
                        alignItems: 'center',
                        justifyContent: 'center'
                    }}>
                    <CircularProgress />
                </Box>}>
                    <Box maxWidth={showExpandedPage ? 1400 : 1200} margin="auto" padding={2}>
                        <Outlet context={data} />
                    </Box>
                </Suspense>
            </Box>
        </Box>
    );
}

export default ProjectDetailsEntryPoint
