import { Box, Typography } from '@mui/material';

const DESCRIPTION = 'Coming soon'

function ReleaseTemplates () {
    return (
        <Box>
            <Typography variant="h5" gutterBottom>Release Templates</Typography>
            <Typography variant="body2">
                {DESCRIPTION}
            </Typography>
        </Box>
    );
}

export default ReleaseTemplates
