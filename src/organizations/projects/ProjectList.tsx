import { useEffect, useMemo, useState } from 'react';
import { Box, Button, List, Typography } from '@mui/material';
import throttle from 'lodash.throttle';
import { Link as RouterLink, useParams } from 'react-router-dom';
import InfiniteScroll from 'react-infinite-scroll-component';
import ListSkeleton from '../../skeletons/ListSkeleton';
import graphql from 'babel-plugin-relay/macro';
import { ConnectionHandler, fetchQuery, useLazyLoadQuery, usePaginationFragment, useRelayEnvironment } from 'react-relay/hooks';
import SearchInput from '../../common/SearchInput';
import ProjectListItem from './ProjectListItem';
import { ProjectListQuery } from './__generated__/ProjectListQuery.graphql';
import { ProjectListPaginationQuery } from './__generated__/ProjectListPaginationQuery.graphql';
import { ProjectListFragment_projects$key } from './__generated__/ProjectListFragment_projects.graphql';

export function GetConnections(orgId: string) {
    const connectionId = ConnectionHandler.getConnectionID(
        orgId,
        "ProjectList_projects",
        { sort: 'UPDATED_AT_DESC' }
    );
    return [connectionId]
}

const INITIAL_ITEM_COUNT = 100;
const DESCRIPTION = 'Projects contain all the resources used to deploy a specific application or product, such as releases, release templates, and pipelines.'

const query = graphql`
    query ProjectListQuery($orgId: String!, $first: Int!, $after: String, $search: String) {
        ...ProjectListFragment_projects
    }
`

function ProjectList() {
    const orgName = useParams().orgName as string;
    const orgPrn = `prn:organization:${orgName}`;
    const [search, setSearch] = useState<string | undefined>('');
    const [isRefreshing, setIsRefreshing] = useState(false);

    const queryData = useLazyLoadQuery<ProjectListQuery>(query, { orgId: orgPrn, first: INITIAL_ITEM_COUNT }, { fetchPolicy: 'store-and-network' });

    const { data, loadNext, hasNext, refetch } = usePaginationFragment<ProjectListPaginationQuery, ProjectListFragment_projects$key>(
        graphql`
        fragment ProjectListFragment_projects on Query
        @refetchable(queryName: "ProjectListPaginationQuery") {
            node(id: $orgId) {
                ...on Organization {
                    projects(
                        first: $first
                        after: $after
                        search: $search
                        sort: UPDATED_AT_DESC
                        ) @connection(key: "ProjectList_projects") {
                            totalCount
                            edges {
                                node {
                                    id
                                    name
                                    ...ProjectListItemFragment_project
                                }
                            }
                        }
                    }
                }
            }
        `, queryData);

    const environment = useRelayEnvironment();

    const fetch = useMemo(
        () =>
            throttle(
                (input?: string) => {
                    setIsRefreshing(true);

                    const normalizedInput = input?.trim();

                    fetchQuery(environment, query, { orgId: orgPrn, first: INITIAL_ITEM_COUNT, search: normalizedInput })
                        .subscribe({
                            complete: () => {
                                setIsRefreshing(false);
                                setSearch(input);

                                // *After* the query has been fetched, we call
                                // refetch again to re-render with the updated data.
                                // At this point the data for the query should
                                // be cached, so we use the 'store-only'
                                // fetchPolicy to avoid suspending.
                                refetch({ orgId: orgPrn, first: INITIAL_ITEM_COUNT, search: normalizedInput }, { fetchPolicy: 'store-only' });
                            },
                            error: () => {
                                setIsRefreshing(false);
                            }
                        }
                        );
                },
                2000,
                { leading: false, trailing: true }
            ),
        [environment, refetch, orgName],
    );

    useEffect(() => {
        return () => {
            fetch.cancel()
        }
    }, [fetch]);

    const onSearchChange = (event: React.ChangeEvent<HTMLInputElement>) => {
        fetch(event.target.value.toLowerCase().trim());
    };

    const onKeyDown = (event: React.KeyboardEvent<HTMLInputElement>) => {
        // Only handle enter key type
        if (event.key === 'Enter') {
            fetch.flush();
        }
    };

    const edges = data?.node?.projects?.edges ?? [];
    const edgeCount = (edges.length ?? 0) - 1;

    return (
        <Box>
            {(search !== '' || edges.length !== 0) &&
                <Box>
                    <Box>
                        <Typography variant="h5" gutterBottom>Projects</Typography>
                        <Typography variant="body2">
                            {DESCRIPTION}
                        </Typography>
                    </Box>
                    <SearchInput
                        sx={{ mt: 2, mb: 2 }}
                        placeholder="search for projects"
                        fullWidth
                        onChange={onSearchChange}
                        onKeyDown={onKeyDown}
                    />
                    {(edges.length === 0 && search !== '') && <Typography
                        sx={{ p: 4 }}
                        align="center"
                        color="textSecondary"
                    >
                        No projects matching search <strong>{search}</strong>
                    </Typography>}
                    <InfiniteScroll
                        dataLength={edges.length ?? 0}
                        next={() => loadNext(20)}
                        hasMore={hasNext}
                        loader={<ListSkeleton rowCount={3} />}
                    >
                        <List sx={isRefreshing ? { opacity: 0.5 } : null} >
                            {edges.map((edge: any, index: number) => <ProjectListItem key={edge.node.id}
                                projectKey={edge.node} last={index === edgeCount}
                            />)}
                        </List>
                    </InfiniteScroll>
                </Box>}
            {(search === '' && data?.node?.projects?.totalCount === 0) &&
                <Box sx={{ mt: 4 }}
                    display="flex"
                    justifyContent="center"
                >
                    <Box
                        sx={{ maxWidth: 600, p: 4 }}
                        display="flex"
                        flexDirection="column"
                        alignItems="center"
                    >
                        <Typography variant="h6" align="center">Get started by creating a project in this organization</Typography>
                        <Typography sx={{ mb: 2 }} color="textSecondary" align="center">
                            {DESCRIPTION}
                        </Typography>
                        <Button component={RouterLink} variant="outlined" to="new">New Project</Button>
                    </Box>
                </Box>}
        </Box>
    );
}

export default ProjectList
