import { Alert, Box, Grid, Tab, Tabs, Typography, useTheme } from '@mui/material';
import Checkbox from '@mui/material/Checkbox';
import FormControl from '@mui/material/FormControl';
import ListItemText from '@mui/material/ListItemText';
import MenuItem from '@mui/material/MenuItem';
import OutlinedInput from '@mui/material/OutlinedInput';
import Select, { SelectChangeEvent } from '@mui/material/Select';
import graphql from 'babel-plugin-relay/macro';
import { useMemo, useState } from 'react';
import { useFragment } from 'react-relay/hooks';
import { useOutletContext, useSearchParams } from 'react-router-dom';
import TabContent from '../../common/TabContent';
import { MAX_SERIES } from '../../insights/InsightsChart';
import InsightsChartApprovalRequestCompleted from '../../insights/InsightsChartApprovalRequestCompleted';
import InsightsChartApprovalRequestCreated from '../../insights/InsightsChartApprovalRequestCreated';
import InsightsChartApprovalRequestWaitTime from '../../insights/InsightsChartApprovalRequestWaitTime';
import InsightsChartDeploymentFailureRate from '../../insights/InsightsChartDeploymentFailureRate';
import InsightsChartDeploymentFrequency from '../../insights/InsightsChartDeploymentFrequency';
import InsightsChartDeploymentLeadTime from '../../insights/InsightsChartDeploymentLeadTime';
import InsightsChartDeploymentRecoveryTime from '../../insights/InsightsChartDeploymentRecoveryTime';
import InsightsChartDeploymentUpdates from '../../insights/InsightsChartDeploymentUpdates';
import InsightsChartPipelineDuration from '../../insights/InsightsChartPipelineDuration';
import InsightsChartPipelinesCompleted from '../../insights/InsightsChartPipelinesCompleted';
import InsightsTimeRangeSelector from '../../insights/InsightsTimeRangeSelector';
import OrganizationBreadcrumbs from '../OrganizationBreadcrumbs';
import ProjectFilter from './ProjectFilter';
import { OrganizationInsightsFragment_organization$key } from './__generated__/OrganizationInsightsFragment_organization.graphql';

function OrganizationInsights() {
    const theme = useTheme();
    const context = useOutletContext<OrganizationInsightsFragment_organization$key>();
    const [searchParams, setSearchParams] = useSearchParams();
    const timeRange = searchParams.get('range') || '30d';
    const tab = searchParams.get('tab') || 'deployments';
    const [environments, setEnvironments] = useState<string[]>([]);
    const [projects, setProjects] = useState<string[]>([]);

    const org = useFragment<OrganizationInsightsFragment_organization$key>(
        graphql`
            fragment OrganizationInsightsFragment_organization on Organization
            {
                id
                name
                environmentNames
            }
        `, context
    );

    const onTimeRangeChange = (value: string) => {
        searchParams.set('range', value);
        setSearchParams(searchParams, { replace: true });
    };

    const onEnvironmentsChange = ({ target: { value } }: SelectChangeEvent<string[]>) => {
        const parsedValue = typeof value === 'string' ? value.split(',') : value;
        if (parsedValue.includes('')) {
            // Select all
            setEnvironments(environments.length === org.environmentNames.length ? [] : org.environmentNames.map((name) => name));
        } else {
            setEnvironments(parsedValue);
        }
    };

    const onTabChange = (event: React.SyntheticEvent, newValue: string) => {
        searchParams.set('tab', newValue);
        setSearchParams(searchParams, { replace: true });
    };

    const filters = useMemo(() => {
        if (projects.length) {
            return projects.flatMap(projectName => (environments.length > 0 ? environments : [undefined]).map(environmentName => ({
                projectId: `prn:project:${org.name}/${projectName}`,
                environmentName,
                label: projectName + (environmentName ? ` - ${environmentName}` : '')
            })));
        } else if (environments.length) {
            return environments.map(environmentName => ({ environmentName, label: environmentName }));
        } else {
            return [{ label: '' }];
        }
    }, [projects, environments]);

    const showLegend = useMemo(() => environments.length > 0 || projects.length > 0, [environments, projects]);

    return <Box>
        <OrganizationBreadcrumbs
            orgName={org.name}
            childRoutes={[
                { title: "insights", path: 'insights' }
            ]}
        />
        <Typography variant="h5" mb={2}>Organization Insights</Typography>
        <Box sx={{
            marginBottom: 2,
            display: 'flex',
            flexDirection: 'row',
            justifyContent: 'space-between',
            [theme.breakpoints.down('md')]: {
                flexDirection: 'column',
                alignItems: 'flex-start',
                '& > *:not(:last-child)': { marginBottom: 2 },
            }
        }}>
            <InsightsTimeRangeSelector value={timeRange} onChange={onTimeRangeChange} />
            <Box sx={{
                display: 'flex',
                flexDirection: 'row',
                justifyContent: 'space-between',
                ml: 2,
                '& > *': { width: 250 },
                '& > *:not(:last-child)': { mr: 2 },
                [theme.breakpoints.down('md')]: {
                    ml: 0,
                    width: '100%',
                    flexDirection: 'column',
                    alignItems: 'flex-start',
                    '& > *': { width: '100%' },
                    '& > *:not(:last-child)': { mb: 2, mr: 0 },
                }
            }}>
                <FormControl>
                    <ProjectFilter orgId={org.id} selected={projects} onChange={setProjects} />
                </FormControl>
                <FormControl>
                    <Select
                        multiple
                        displayEmpty
                        value={environments}
                        onChange={onEnvironmentsChange}
                        input={<OutlinedInput size="small" />}
                        renderValue={(selected) => {
                            if (selected.length === 0) {
                                return <Typography color="textSecondary">Filter by environment</Typography>;
                            }

                            return selected.join(', ');
                        }}
                    >
                        {org.environmentNames.length > 0 && <MenuItem value="">
                            <Typography color="textSecondary">
                                {environments.length === org.environmentNames.length ? 'Unselect all ' : 'Select all'} environments
                            </Typography>
                        </MenuItem>}
                        {org.environmentNames.map((name) => (
                            <MenuItem key={name} value={name}>
                                <Checkbox checked={environments.indexOf(name) > -1} />
                                <ListItemText primary={name} />
                            </MenuItem>
                        ))}
                        {org.environmentNames.length === 0 && <MenuItem disabled>No Environments</MenuItem>}
                    </Select>
                </FormControl>
            </Box>
        </Box>
        {(filters.length * 2) > MAX_SERIES && <Alert severity="warning" sx={{ marginBottom: 2 }}>
            <Typography variant="body2">
                Some data is not being displayed because there are too many filters selected. Please reduce the number of filters to see all the data.
            </Typography>
        </Alert>}
        <Box sx={{ display: "flex", border: 1, borderColor: 'divider', borderTopLeftRadius: 4, borderTopRightRadius: 4, mb: 1 }}>
            <Tabs value={tab} onChange={onTabChange}>
                <Tab label="Deployments" value="deployments" />
                <Tab label="Pipelines" value="pipelines" />
                <Tab label="Approvals" value="approvals" />
            </Tabs>
        </Box>
        <TabContent>
            {tab === 'deployments' && <Grid container spacing={2}>
                <Grid item xs={12} md={6}>
                    <InsightsChartDeploymentFrequency
                        timeRange={timeRange}
                        organizationId={org.id}
                        filters={filters}
                        showLegend={showLegend}
                    />
                </Grid>
                <Grid item xs={12} md={6}>
                    <InsightsChartDeploymentLeadTime
                        timeRange={timeRange}
                        organizationId={org.id}
                        filters={filters}
                        showLegend={showLegend}
                    />
                </Grid>
                <Grid item xs={12} md={6}>
                    <InsightsChartDeploymentFailureRate
                        timeRange={timeRange}
                        organizationId={org.id}
                        filters={filters}
                        showLegend={showLegend}
                    />
                </Grid>
                <Grid item xs={12} md={6}>
                    <InsightsChartDeploymentRecoveryTime
                        timeRange={timeRange}
                        organizationId={org.id}
                        filters={filters}
                        showLegend={showLegend}
                    />
                </Grid>
                <Grid item xs={12}>
                    <InsightsChartDeploymentUpdates
                        timeRange={timeRange}
                        organizationId={org.id}
                        filters={filters}
                        showLegend={showLegend}
                    />
                </Grid>
            </Grid>}
            {tab === 'pipelines' && <Grid container spacing={2}>
                <Grid item xs={12}>
                    <InsightsChartPipelinesCompleted
                        timeRange={timeRange}
                        organizationId={org.id}
                        filters={filters}
                        showLegend={showLegend}
                    />
                </Grid>
                <Grid item xs={12}>
                    <InsightsChartPipelineDuration
                        timeRange={timeRange}
                        organizationId={org.id}
                        filters={filters}
                        showLegend={showLegend}
                    />
                </Grid>
            </Grid>}
            {tab === 'approvals' && <Grid container spacing={2}>
                <Grid item xs={12} md={6}>
                    <InsightsChartApprovalRequestCreated
                        timeRange={timeRange}
                        organizationId={org.id}
                        filters={filters}
                        showLegend={showLegend}
                    />
                </Grid>
                <Grid item xs={12} md={6}>
                    <InsightsChartApprovalRequestCompleted
                        timeRange={timeRange}
                        organizationId={org.id}
                        filters={filters}
                        showLegend={showLegend}
                    />
                </Grid>
                <Grid item xs={12}>
                    <InsightsChartApprovalRequestWaitTime
                        timeRange={timeRange}
                        organizationId={org.id}
                        filters={filters}
                        showLegend={showLegend}
                    />
                </Grid>
            </Grid>}
        </TabContent>
    </Box >;
}

export default OrganizationInsights;
