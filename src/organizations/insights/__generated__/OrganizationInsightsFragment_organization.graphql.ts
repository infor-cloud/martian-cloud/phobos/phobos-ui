/**
 * @generated SignedSource<<46b3f867cf4ed478e5052d4debe69c03>>
 * @lightSyntaxTransform
 * @nogrep
 */

/* tslint:disable */
/* eslint-disable */
// @ts-nocheck

import { Fragment, ReaderFragment } from 'relay-runtime';
import { FragmentRefs } from "relay-runtime";
export type OrganizationInsightsFragment_organization$data = {
  readonly environmentNames: ReadonlyArray<string>;
  readonly id: string;
  readonly name: string;
  readonly " $fragmentType": "OrganizationInsightsFragment_organization";
};
export type OrganizationInsightsFragment_organization$key = {
  readonly " $data"?: OrganizationInsightsFragment_organization$data;
  readonly " $fragmentSpreads": FragmentRefs<"OrganizationInsightsFragment_organization">;
};

const node: ReaderFragment = {
  "argumentDefinitions": [],
  "kind": "Fragment",
  "metadata": null,
  "name": "OrganizationInsightsFragment_organization",
  "selections": [
    {
      "alias": null,
      "args": null,
      "kind": "ScalarField",
      "name": "id",
      "storageKey": null
    },
    {
      "alias": null,
      "args": null,
      "kind": "ScalarField",
      "name": "name",
      "storageKey": null
    },
    {
      "alias": null,
      "args": null,
      "kind": "ScalarField",
      "name": "environmentNames",
      "storageKey": null
    }
  ],
  "type": "Organization",
  "abstractKey": null
};

(node as any).hash = "b6953131563e459212c2d667fa726535";

export default node;
