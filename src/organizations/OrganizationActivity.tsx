import { Box, Typography } from '@mui/material';
import graphql from 'babel-plugin-relay/macro';
import { useFragment, useLazyLoadQuery, usePaginationFragment } from "react-relay/hooks";
import { useOutletContext } from 'react-router-dom';
import OrganizationBreadcrumbs from './OrganizationBreadcrumbs';
import { OrganizationActivityQuery } from './__generated__/OrganizationActivityQuery.graphql';
import { OrganizationActivityFragment_activity$key } from './__generated__/OrganizationActivityFragment_activity.graphql';
import { OrganizationActivityPaginationQuery } from './__generated__/OrganizationActivityPaginationQuery.graphql';
import { OrganizationActivityFragment_organization$key } from './__generated__/OrganizationActivityFragment_organization.graphql';
import ActivityEventList from '../activity/ActivityEventList';
import InfiniteScroll from 'react-infinite-scroll-component';
import ListSkeleton from '../skeletons/ListSkeleton';

function OrganizationActivity() {
    const context = useOutletContext<OrganizationActivityFragment_organization$key>();

    const organization = useFragment<OrganizationActivityFragment_organization$key>(graphql`
        fragment OrganizationActivityFragment_organization on Organization
        {
            id
            name
        }
    `, context);

    const queryData = useLazyLoadQuery<OrganizationActivityQuery>(graphql`
        query OrganizationActivityQuery($organizationId: String, $first: Int, $last: Int, $after: String, $before: String) {
            ...OrganizationActivityFragment_activity
        }
    `, { first: 20, organizationId: organization.id }, { fetchPolicy: 'store-and-network' });

    const { data, loadNext, hasNext } = usePaginationFragment<OrganizationActivityPaginationQuery, OrganizationActivityFragment_activity$key>(
        graphql`
            fragment OrganizationActivityFragment_activity on Query
            @refetchable(queryName: "OrganizationActivityPaginationQuery") {
                activityEvents(
                    organizationId: $organizationId,
                    first: $first,
                    last: $last,
                    after: $after,
                    before: $before,
                    sort: CREATED_AT_DESC
                    ) @connection(key: "OrganizationActivityFragment_activityEvents") {
                    edges {
                        node {
                            id
                        }
                    }
                    ...ActivityEventListFragment_connection
                }
            }
        `,
        queryData);

    const eventCount = data.activityEvents?.edges?.length || 0;

    return (
        <Box>
            <OrganizationBreadcrumbs
                orgName={organization.name}
                childRoutes={[
                    { title: "activity", path: 'activity' }
                ]}
            />
            {eventCount > 0 &&
                <InfiniteScroll
                    dataLength={data.activityEvents?.edges?.length ?? 0}
                    next={() => loadNext(20)}
                    hasMore={hasNext}
                    loader={<ListSkeleton rowCount={3} />}
                >
                    <ActivityEventList
                        fragmentRef={data.activityEvents}
                        showOrgLink={false}
                        showProjectLink={true}
                    />
                </InfiniteScroll>
            }
            {eventCount === 0 && <Box sx={{ mt: 8 }} display="flex" justifyContent="center">
                <Typography variant="h6" color="textSecondary">
                    This organization does not have any activity events
                </Typography>
            </Box>}
        </Box>
    );
}

export default OrganizationActivity;
