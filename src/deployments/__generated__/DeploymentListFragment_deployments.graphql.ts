/**
 * @generated SignedSource<<333c63d0dae4b17e8c5b11b939e3fc2c>>
 * @lightSyntaxTransform
 * @nogrep
 */

/* tslint:disable */
/* eslint-disable */
// @ts-nocheck

import { Fragment, ReaderFragment } from 'relay-runtime';
import { FragmentRefs } from "relay-runtime";
export type DeploymentListFragment_deployments$data = {
  readonly edges: ReadonlyArray<{
    readonly node: {
      readonly id: string;
      readonly " $fragmentSpreads": FragmentRefs<"DeploymentListItemFragment_deployment">;
    } | null | undefined;
  } | null | undefined> | null | undefined;
  readonly " $fragmentType": "DeploymentListFragment_deployments";
};
export type DeploymentListFragment_deployments$key = {
  readonly " $data"?: DeploymentListFragment_deployments$data;
  readonly " $fragmentSpreads": FragmentRefs<"DeploymentListFragment_deployments">;
};

const node: ReaderFragment = {
  "argumentDefinitions": [],
  "kind": "Fragment",
  "metadata": null,
  "name": "DeploymentListFragment_deployments",
  "selections": [
    {
      "alias": null,
      "args": null,
      "concreteType": "PipelineEdge",
      "kind": "LinkedField",
      "name": "edges",
      "plural": true,
      "selections": [
        {
          "alias": null,
          "args": null,
          "concreteType": "Pipeline",
          "kind": "LinkedField",
          "name": "node",
          "plural": false,
          "selections": [
            {
              "alias": null,
              "args": null,
              "kind": "ScalarField",
              "name": "id",
              "storageKey": null
            },
            {
              "args": null,
              "kind": "FragmentSpread",
              "name": "DeploymentListItemFragment_deployment"
            }
          ],
          "storageKey": null
        }
      ],
      "storageKey": null
    }
  ],
  "type": "PipelineConnection",
  "abstractKey": null
};

(node as any).hash = "f00336fd3ffe08be689e69e2974c5406";

export default node;
