/**
 * @generated SignedSource<<ba0b49dd4122dec73b6b4953a205f071>>
 * @lightSyntaxTransform
 * @nogrep
 */

/* tslint:disable */
/* eslint-disable */
// @ts-nocheck

import { Fragment, ReaderFragment } from 'relay-runtime';
export type PipelineNodeStatus = "APPROVAL_PENDING" | "CANCELED" | "CANCELING" | "CREATED" | "DEFERRED" | "FAILED" | "PENDING" | "READY" | "RUNNING" | "SKIPPED" | "SUCCEEDED" | "WAITING" | "%future added value";
import { FragmentRefs } from "relay-runtime";
export type DeploymentListItemFragment_deployment$data = {
  readonly annotations: ReadonlyArray<{
    readonly key: string;
  }>;
  readonly environmentName: string | null | undefined;
  readonly id: string;
  readonly metadata: {
    readonly prn: string;
  };
  readonly pipelineTemplate: {
    readonly id: string;
    readonly name: string | null | undefined;
    readonly semanticVersion: string | null | undefined;
  };
  readonly project: {
    readonly name: string;
    readonly organizationName: string;
  };
  readonly release: {
    readonly id: string;
    readonly semanticVersion: string;
  } | null | undefined;
  readonly status: PipelineNodeStatus;
  readonly timestamps: {
    readonly completedAt: any | null | undefined;
    readonly startedAt: any | null | undefined;
  };
  readonly " $fragmentSpreads": FragmentRefs<"PipelineAnnotationsFragment_pipeline" | "PipelineStageIcons_stages">;
  readonly " $fragmentType": "DeploymentListItemFragment_deployment";
};
export type DeploymentListItemFragment_deployment$key = {
  readonly " $data"?: DeploymentListItemFragment_deployment$data;
  readonly " $fragmentSpreads": FragmentRefs<"DeploymentListItemFragment_deployment">;
};

const node: ReaderFragment = (function(){
var v0 = {
  "alias": null,
  "args": null,
  "kind": "ScalarField",
  "name": "id",
  "storageKey": null
},
v1 = {
  "alias": null,
  "args": null,
  "kind": "ScalarField",
  "name": "semanticVersion",
  "storageKey": null
},
v2 = {
  "alias": null,
  "args": null,
  "kind": "ScalarField",
  "name": "name",
  "storageKey": null
};
return {
  "argumentDefinitions": [],
  "kind": "Fragment",
  "metadata": null,
  "name": "DeploymentListItemFragment_deployment",
  "selections": [
    (v0/*: any*/),
    {
      "alias": null,
      "args": null,
      "concreteType": "ResourceMetadata",
      "kind": "LinkedField",
      "name": "metadata",
      "plural": false,
      "selections": [
        {
          "alias": null,
          "args": null,
          "kind": "ScalarField",
          "name": "prn",
          "storageKey": null
        }
      ],
      "storageKey": null
    },
    {
      "alias": null,
      "args": null,
      "kind": "ScalarField",
      "name": "environmentName",
      "storageKey": null
    },
    {
      "alias": null,
      "args": null,
      "concreteType": "Release",
      "kind": "LinkedField",
      "name": "release",
      "plural": false,
      "selections": [
        (v0/*: any*/),
        (v1/*: any*/)
      ],
      "storageKey": null
    },
    {
      "alias": null,
      "args": null,
      "kind": "ScalarField",
      "name": "status",
      "storageKey": null
    },
    {
      "alias": null,
      "args": null,
      "concreteType": "PipelineTemplate",
      "kind": "LinkedField",
      "name": "pipelineTemplate",
      "plural": false,
      "selections": [
        (v0/*: any*/),
        (v2/*: any*/),
        (v1/*: any*/)
      ],
      "storageKey": null
    },
    {
      "alias": null,
      "args": null,
      "concreteType": "Project",
      "kind": "LinkedField",
      "name": "project",
      "plural": false,
      "selections": [
        (v2/*: any*/),
        {
          "alias": null,
          "args": null,
          "kind": "ScalarField",
          "name": "organizationName",
          "storageKey": null
        }
      ],
      "storageKey": null
    },
    {
      "alias": null,
      "args": null,
      "concreteType": "PipelineTimestamps",
      "kind": "LinkedField",
      "name": "timestamps",
      "plural": false,
      "selections": [
        {
          "alias": null,
          "args": null,
          "kind": "ScalarField",
          "name": "startedAt",
          "storageKey": null
        },
        {
          "alias": null,
          "args": null,
          "kind": "ScalarField",
          "name": "completedAt",
          "storageKey": null
        }
      ],
      "storageKey": null
    },
    {
      "alias": null,
      "args": null,
      "concreteType": "PipelineAnnotation",
      "kind": "LinkedField",
      "name": "annotations",
      "plural": true,
      "selections": [
        {
          "alias": null,
          "args": null,
          "kind": "ScalarField",
          "name": "key",
          "storageKey": null
        }
      ],
      "storageKey": null
    },
    {
      "args": null,
      "kind": "FragmentSpread",
      "name": "PipelineAnnotationsFragment_pipeline"
    },
    {
      "args": null,
      "kind": "FragmentSpread",
      "name": "PipelineStageIcons_stages"
    }
  ],
  "type": "Pipeline",
  "abstractKey": null
};
})();

(node as any).hash = "054b042ca58b8476d3eb993e06050eb4";

export default node;
