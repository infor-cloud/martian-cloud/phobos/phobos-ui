import {
  Box, Paper, Table,
  TableBody,
  TableCell,
  TableContainer,
  TableHead,
  TableRow, Typography
} from "@mui/material";
import graphql from "babel-plugin-relay/macro";
import InfiniteScroll from "react-infinite-scroll-component";
import { LoadMoreFn, useFragment } from "react-relay/hooks";
import { EnvironmentDeploymentListFragmentPaginationQuery } from "../organizations/projects/environments/__generated__/EnvironmentDeploymentListFragmentPaginationQuery.graphql";
import ListSkeleton from "../skeletons/ListSkeleton";
import DeploymentListItem from "./DeploymentListItem";
import { DeploymentListFragment_deployments$key } from "./__generated__/DeploymentListFragment_deployments.graphql";

interface Props {
  hasNext: boolean;
  loadNext: LoadMoreFn<EnvironmentDeploymentListFragmentPaginationQuery>;
  fragmentRef: DeploymentListFragment_deployments$key;
}

function DeploymentList({ fragmentRef, hasNext, loadNext }: Props) {
  const data = useFragment<DeploymentListFragment_deployments$key>(
    graphql`
      fragment DeploymentListFragment_deployments on PipelineConnection {
        edges {
          node {
            id
            ...DeploymentListItemFragment_deployment
          }
        }
      }
    `,
    fragmentRef
  );

  return data?.edges && data?.edges.length > 0 ? (
    <InfiniteScroll
      dataLength={data?.edges?.length ?? 0}
      next={() => loadNext(20)}
      hasMore={hasNext}
      loader={<ListSkeleton rowCount={3} />}
    >
      <TableContainer>
        <Table sx={{ tableLayout: "fixed" }} aria-label="deployments">
          <TableHead>
            <TableRow>
              <TableCell>Status</TableCell>
              <TableCell>Annotations</TableCell>
              <TableCell>Release</TableCell>
              <TableCell>Environment</TableCell>
              <TableCell>Pipeline Template</TableCell>
              <TableCell>Stages</TableCell>
            </TableRow>
          </TableHead>
          <TableBody>
            {data?.edges?.map((deployment: any) => (
              <DeploymentListItem
                key={deployment.node.id}
                fragmentRef={deployment.node}
              />
            ))}
          </TableBody>
        </Table>
      </TableContainer>
    </InfiniteScroll>
  ) : (
    <Paper
      variant="outlined"
      sx={{ display: "flex", justifyContent: "center" }}
    >
      <Box sx={{ p: 4 }}>
        <Typography variant="h6" color="textSecondary" align="center">
          No deployments have been created.
        </Typography>
      </Box>
    </Paper>
  );
}

export default DeploymentList;
