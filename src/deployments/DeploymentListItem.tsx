import { Box, TableCell, TableRow, Typography } from "@mui/material";
import graphql from "babel-plugin-relay/macro";
import { useFragment } from "react-relay/hooks";
import Timestamp from "../common/Timestamp";
import PipelineAnnotations from "../organizations/projects/pipelines/PipelineAnnotations";
import PipelineStageIcons from "../organizations/projects/pipelines/PipelineStageIcons";
import PipelineStatusChip from "../organizations/projects/pipelines/PipelineStatusChip";
import Link from "../routes/Link";
import { DeploymentListItemFragment_deployment$key } from "./__generated__/DeploymentListItemFragment_deployment.graphql";

interface Props {
  fragmentRef: DeploymentListItemFragment_deployment$key;
}

function DeploymentListItem({ fragmentRef }: Props) {
  const data = useFragment<DeploymentListItemFragment_deployment$key>(
    graphql`
      fragment DeploymentListItemFragment_deployment on Pipeline {
        id
        metadata {
          prn
        }
        environmentName
        release {
          id
          semanticVersion
        }
        status
        pipelineTemplate {
          id
          name
          semanticVersion
        }
        project {
          name
          organizationName
        }
        timestamps {
          startedAt
          completedAt
        }
        annotations {
          key
      }
        ...PipelineAnnotationsFragment_pipeline
        ...PipelineStageIcons_stages
      }
    `,
    fragmentRef
  );

  const buildRoute = (type: string, id: string) => {
    return `/organizations/${data.project.organizationName}/projects/${data.project.name}/-/${type}/${id}`;
  };

    return (
        <TableRow sx={{ "&:last-child td, &:last-child th": { border: 0 } }}>
            <TableCell>
                <PipelineStatusChip
                    to={buildRoute("pipelines", data.id)}
                    status={data.status}
                />
                {(data.timestamps.completedAt || data.timestamps.startedAt) && (
                    <Box marginTop={"4px"}>
                        {!data.timestamps.completedAt && (
                            <Box>
                                {data.timestamps.startedAt && (
                                    <Typography variant="caption" color="textSecondary">
                                        started{" "}
                                        <Timestamp component="span" timestamp={data.timestamps.startedAt} />
                                    </Typography>
                                )}
                            </Box>
                        )}
                        {data.timestamps.completedAt && (
                            <Typography variant="caption" color="textSecondary">
                                finished{" "}
                                <Timestamp component="span" timestamp={data.timestamps.completedAt} />
                            </Typography>
                        )}
                    </Box>
                )}
            </TableCell>
            <TableCell>
                {data.annotations.length === 0 && '--'}
                <PipelineAnnotations fragmentRef={data} />
            </TableCell>
            <TableCell>
                {data.release && <Link color="inherit" to={buildRoute("releases", data.release.id)}>
                    {data.release.semanticVersion}
                </Link>}
            </TableCell>
            <TableCell>{data.environmentName}</TableCell>
            <TableCell
                sx={{ wordWrap: "break-word", overflowWrap: "break-word" }}>
                <Link
                    color="inherit"
                    to={buildRoute(
                        "pipeline_templates",
                        data.pipelineTemplate.id
                    )}
                >
                    {data.pipelineTemplate.name}:
                    {data.pipelineTemplate.semanticVersion}
                </Link>
            </TableCell>
            <TableCell>
                <PipelineStageIcons fragmentRef={data} />
            </TableCell>
        </TableRow>
    );
}

export default DeploymentListItem;
