/**
 * @generated SignedSource<<d44c9da9608a3d34aa7aecf965ae6f10>>
 * @lightSyntaxTransform
 * @nogrep
 */

/* tslint:disable */
/* eslint-disable */
// @ts-nocheck

import { Fragment, ReaderFragment } from 'relay-runtime';
import { FragmentRefs } from "relay-runtime";
export type AgentListItemFragment_agent$data = {
  readonly createdBy: string;
  readonly disabled: boolean;
  readonly id: string;
  readonly metadata: {
    readonly createdAt: any;
  };
  readonly name: string;
  readonly tags: ReadonlyArray<string>;
  readonly " $fragmentType": "AgentListItemFragment_agent";
};
export type AgentListItemFragment_agent$key = {
  readonly " $data"?: AgentListItemFragment_agent$data;
  readonly " $fragmentSpreads": FragmentRefs<"AgentListItemFragment_agent">;
};

const node: ReaderFragment = {
  "argumentDefinitions": [],
  "kind": "Fragment",
  "metadata": null,
  "name": "AgentListItemFragment_agent",
  "selections": [
    {
      "alias": null,
      "args": null,
      "concreteType": "ResourceMetadata",
      "kind": "LinkedField",
      "name": "metadata",
      "plural": false,
      "selections": [
        {
          "alias": null,
          "args": null,
          "kind": "ScalarField",
          "name": "createdAt",
          "storageKey": null
        }
      ],
      "storageKey": null
    },
    {
      "alias": null,
      "args": null,
      "kind": "ScalarField",
      "name": "tags",
      "storageKey": null
    },
    {
      "alias": null,
      "args": null,
      "kind": "ScalarField",
      "name": "id",
      "storageKey": null
    },
    {
      "alias": null,
      "args": null,
      "kind": "ScalarField",
      "name": "name",
      "storageKey": null
    },
    {
      "alias": null,
      "args": null,
      "kind": "ScalarField",
      "name": "disabled",
      "storageKey": null
    },
    {
      "alias": null,
      "args": null,
      "kind": "ScalarField",
      "name": "createdBy",
      "storageKey": null
    }
  ],
  "type": "Agent",
  "abstractKey": null
};

(node as any).hash = "56db5ec1340b9e7435c4e3b554f74568";

export default node;
