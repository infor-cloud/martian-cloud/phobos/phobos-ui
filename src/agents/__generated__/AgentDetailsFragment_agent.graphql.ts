/**
 * @generated SignedSource<<c21fadacec0c3bbbe092e8fae508384a>>
 * @lightSyntaxTransform
 * @nogrep
 */

/* tslint:disable */
/* eslint-disable */
// @ts-nocheck

import { Fragment, ReaderFragment } from 'relay-runtime';
export type AgentType = "ORGANIZATION" | "SHARED" | "%future added value";
import { FragmentRefs } from "relay-runtime";
export type AgentDetailsFragment_agent$data = {
  readonly assignedServiceAccounts: {
    readonly totalCount: number;
  };
  readonly createdBy: string;
  readonly description: string;
  readonly disabled: boolean;
  readonly id: string;
  readonly metadata: {
    readonly createdAt: any;
    readonly prn: string;
  };
  readonly name: string;
  readonly runUntaggedJobs: boolean;
  readonly sessions: {
    readonly edges: ReadonlyArray<{
      readonly node: {
        readonly active: boolean;
        readonly lastContacted: any;
      } | null | undefined;
    } | null | undefined> | null | undefined;
  };
  readonly tags: ReadonlyArray<string>;
  readonly type: AgentType;
  readonly " $fragmentType": "AgentDetailsFragment_agent";
};
export type AgentDetailsFragment_agent$key = {
  readonly " $data"?: AgentDetailsFragment_agent$data;
  readonly " $fragmentSpreads": FragmentRefs<"AgentDetailsFragment_agent">;
};

const node: ReaderFragment = {
  "argumentDefinitions": [],
  "kind": "Fragment",
  "metadata": null,
  "name": "AgentDetailsFragment_agent",
  "selections": [
    {
      "alias": null,
      "args": null,
      "kind": "ScalarField",
      "name": "id",
      "storageKey": null
    },
    {
      "alias": null,
      "args": null,
      "kind": "ScalarField",
      "name": "name",
      "storageKey": null
    },
    {
      "alias": null,
      "args": null,
      "kind": "ScalarField",
      "name": "type",
      "storageKey": null
    },
    {
      "alias": null,
      "args": null,
      "kind": "ScalarField",
      "name": "disabled",
      "storageKey": null
    },
    {
      "alias": null,
      "args": null,
      "kind": "ScalarField",
      "name": "description",
      "storageKey": null
    },
    {
      "alias": null,
      "args": null,
      "kind": "ScalarField",
      "name": "createdBy",
      "storageKey": null
    },
    {
      "alias": null,
      "args": null,
      "kind": "ScalarField",
      "name": "tags",
      "storageKey": null
    },
    {
      "alias": null,
      "args": null,
      "kind": "ScalarField",
      "name": "runUntaggedJobs",
      "storageKey": null
    },
    {
      "alias": null,
      "args": null,
      "concreteType": "ResourceMetadata",
      "kind": "LinkedField",
      "name": "metadata",
      "plural": false,
      "selections": [
        {
          "alias": null,
          "args": null,
          "kind": "ScalarField",
          "name": "createdAt",
          "storageKey": null
        },
        {
          "alias": null,
          "args": null,
          "kind": "ScalarField",
          "name": "prn",
          "storageKey": null
        }
      ],
      "storageKey": null
    },
    {
      "alias": null,
      "args": [
        {
          "kind": "Literal",
          "name": "first",
          "value": 0
        }
      ],
      "concreteType": "ServiceAccountConnection",
      "kind": "LinkedField",
      "name": "assignedServiceAccounts",
      "plural": false,
      "selections": [
        {
          "alias": null,
          "args": null,
          "kind": "ScalarField",
          "name": "totalCount",
          "storageKey": null
        }
      ],
      "storageKey": "assignedServiceAccounts(first:0)"
    },
    {
      "alias": null,
      "args": [
        {
          "kind": "Literal",
          "name": "first",
          "value": 1
        },
        {
          "kind": "Literal",
          "name": "sort",
          "value": "LAST_CONTACTED_AT_DESC"
        }
      ],
      "concreteType": "AgentSessionConnection",
      "kind": "LinkedField",
      "name": "sessions",
      "plural": false,
      "selections": [
        {
          "alias": null,
          "args": null,
          "concreteType": "AgentSessionEdge",
          "kind": "LinkedField",
          "name": "edges",
          "plural": true,
          "selections": [
            {
              "alias": null,
              "args": null,
              "concreteType": "AgentSession",
              "kind": "LinkedField",
              "name": "node",
              "plural": false,
              "selections": [
                {
                  "alias": null,
                  "args": null,
                  "kind": "ScalarField",
                  "name": "active",
                  "storageKey": null
                },
                {
                  "alias": null,
                  "args": null,
                  "kind": "ScalarField",
                  "name": "lastContacted",
                  "storageKey": null
                }
              ],
              "storageKey": null
            }
          ],
          "storageKey": null
        }
      ],
      "storageKey": "sessions(first:1,sort:\"LAST_CONTACTED_AT_DESC\")"
    }
  ],
  "type": "Agent",
  "abstractKey": null
};

(node as any).hash = "3d257eaa7779f7d6478e8ceeb60aadd5";

export default node;
