/**
 * @generated SignedSource<<fe5650c35e29d331c5379be9dd67bb68>>
 * @lightSyntaxTransform
 * @nogrep
 */

/* tslint:disable */
/* eslint-disable */
// @ts-nocheck

import { ConcreteRequest, GraphQLSubscription } from 'relay-runtime';
import { FragmentRefs } from "relay-runtime";
export type AgentType = "ORGANIZATION" | "SHARED" | "%future added value";
export type AgentSessionEventSubscriptionInput = {
  agentId?: string | null | undefined;
  agentType?: AgentType | null | undefined;
  organizationId?: string | null | undefined;
};
export type AgentDetailsSessionEventsSubscription$variables = {
  input: AgentSessionEventSubscriptionInput;
};
export type AgentDetailsSessionEventsSubscription$data = {
  readonly agentSessionEvents: {
    readonly action: string;
    readonly agentSession: {
      readonly id: string;
      readonly " $fragmentSpreads": FragmentRefs<"AgentSessionListItemFragment">;
    };
  };
};
export type AgentDetailsSessionEventsSubscription = {
  response: AgentDetailsSessionEventsSubscription$data;
  variables: AgentDetailsSessionEventsSubscription$variables;
};

const node: ConcreteRequest = (function(){
var v0 = [
  {
    "defaultValue": null,
    "kind": "LocalArgument",
    "name": "input"
  }
],
v1 = [
  {
    "kind": "Variable",
    "name": "input",
    "variableName": "input"
  }
],
v2 = {
  "alias": null,
  "args": null,
  "kind": "ScalarField",
  "name": "action",
  "storageKey": null
},
v3 = {
  "alias": null,
  "args": null,
  "kind": "ScalarField",
  "name": "id",
  "storageKey": null
};
return {
  "fragment": {
    "argumentDefinitions": (v0/*: any*/),
    "kind": "Fragment",
    "metadata": null,
    "name": "AgentDetailsSessionEventsSubscription",
    "selections": [
      {
        "alias": null,
        "args": (v1/*: any*/),
        "concreteType": "AgentSessionEvent",
        "kind": "LinkedField",
        "name": "agentSessionEvents",
        "plural": false,
        "selections": [
          (v2/*: any*/),
          {
            "alias": null,
            "args": null,
            "concreteType": "AgentSession",
            "kind": "LinkedField",
            "name": "agentSession",
            "plural": false,
            "selections": [
              (v3/*: any*/),
              {
                "args": null,
                "kind": "FragmentSpread",
                "name": "AgentSessionListItemFragment"
              }
            ],
            "storageKey": null
          }
        ],
        "storageKey": null
      }
    ],
    "type": "Subscription",
    "abstractKey": null
  },
  "kind": "Request",
  "operation": {
    "argumentDefinitions": (v0/*: any*/),
    "kind": "Operation",
    "name": "AgentDetailsSessionEventsSubscription",
    "selections": [
      {
        "alias": null,
        "args": (v1/*: any*/),
        "concreteType": "AgentSessionEvent",
        "kind": "LinkedField",
        "name": "agentSessionEvents",
        "plural": false,
        "selections": [
          (v2/*: any*/),
          {
            "alias": null,
            "args": null,
            "concreteType": "AgentSession",
            "kind": "LinkedField",
            "name": "agentSession",
            "plural": false,
            "selections": [
              (v3/*: any*/),
              {
                "alias": null,
                "args": null,
                "kind": "ScalarField",
                "name": "lastContacted",
                "storageKey": null
              },
              {
                "alias": null,
                "args": null,
                "kind": "ScalarField",
                "name": "active",
                "storageKey": null
              },
              {
                "alias": null,
                "args": null,
                "kind": "ScalarField",
                "name": "internal",
                "storageKey": null
              },
              {
                "alias": null,
                "args": null,
                "kind": "ScalarField",
                "name": "errorCount",
                "storageKey": null
              },
              {
                "alias": null,
                "args": null,
                "concreteType": "ResourceMetadata",
                "kind": "LinkedField",
                "name": "metadata",
                "plural": false,
                "selections": [
                  {
                    "alias": null,
                    "args": null,
                    "kind": "ScalarField",
                    "name": "prn",
                    "storageKey": null
                  },
                  {
                    "alias": null,
                    "args": null,
                    "kind": "ScalarField",
                    "name": "updatedAt",
                    "storageKey": null
                  }
                ],
                "storageKey": null
              }
            ],
            "storageKey": null
          }
        ],
        "storageKey": null
      }
    ]
  },
  "params": {
    "cacheID": "5659fc08d2d5d1892d28f60f2223e1aa",
    "id": null,
    "metadata": {},
    "name": "AgentDetailsSessionEventsSubscription",
    "operationKind": "subscription",
    "text": "subscription AgentDetailsSessionEventsSubscription(\n  $input: AgentSessionEventSubscriptionInput!\n) {\n  agentSessionEvents(input: $input) {\n    action\n    agentSession {\n      id\n      ...AgentSessionListItemFragment\n    }\n  }\n}\n\nfragment AgentSessionListItemFragment on AgentSession {\n  id\n  lastContacted\n  active\n  internal\n  errorCount\n  metadata {\n    prn\n    updatedAt\n  }\n}\n"
  }
};
})();

(node as any).hash = "5a32a84f06c91822ba1ededf3b145d25";

export default node;
