/**
 * @generated SignedSource<<1cbcc66ff4119affcdd1c235e2334fd5>>
 * @lightSyntaxTransform
 * @nogrep
 */

/* tslint:disable */
/* eslint-disable */
// @ts-nocheck

import { ConcreteRequest, Mutation } from 'relay-runtime';
import { FragmentRefs } from "relay-runtime";
export type ProblemType = "BAD_REQUEST" | "CONFLICT" | "FORBIDDEN" | "NOT_FOUND" | "%future added value";
export type AssignServiceAccountToAgentInput = {
  agentId: string;
  clientMutationId?: string | null | undefined;
  serviceAccountId: string;
};
export type AssignedServiceAccountListAssignMutation$variables = {
  connections: ReadonlyArray<string>;
  input: AssignServiceAccountToAgentInput;
};
export type AssignedServiceAccountListAssignMutation$data = {
  readonly assignServiceAccountToAgent: {
    readonly agent: {
      readonly assignedServiceAccounts: {
        readonly totalCount: number;
      };
    } | null | undefined;
    readonly problems: ReadonlyArray<{
      readonly field: ReadonlyArray<string> | null | undefined;
      readonly message: string;
      readonly type: ProblemType;
    }>;
    readonly serviceAccount: {
      readonly id: string;
      readonly " $fragmentSpreads": FragmentRefs<"AssignedServiceAccountListItemFragment_assignedServiceAccount">;
    } | null | undefined;
  };
};
export type AssignedServiceAccountListAssignMutation = {
  response: AssignedServiceAccountListAssignMutation$data;
  variables: AssignedServiceAccountListAssignMutation$variables;
};

const node: ConcreteRequest = (function(){
var v0 = {
  "defaultValue": null,
  "kind": "LocalArgument",
  "name": "connections"
},
v1 = {
  "defaultValue": null,
  "kind": "LocalArgument",
  "name": "input"
},
v2 = [
  {
    "kind": "Variable",
    "name": "input",
    "variableName": "input"
  }
],
v3 = {
  "alias": null,
  "args": [
    {
      "kind": "Literal",
      "name": "first",
      "value": 0
    }
  ],
  "concreteType": "ServiceAccountConnection",
  "kind": "LinkedField",
  "name": "assignedServiceAccounts",
  "plural": false,
  "selections": [
    {
      "alias": null,
      "args": null,
      "kind": "ScalarField",
      "name": "totalCount",
      "storageKey": null
    }
  ],
  "storageKey": "assignedServiceAccounts(first:0)"
},
v4 = {
  "alias": null,
  "args": null,
  "kind": "ScalarField",
  "name": "id",
  "storageKey": null
},
v5 = {
  "alias": null,
  "args": null,
  "concreteType": "Problem",
  "kind": "LinkedField",
  "name": "problems",
  "plural": true,
  "selections": [
    {
      "alias": null,
      "args": null,
      "kind": "ScalarField",
      "name": "message",
      "storageKey": null
    },
    {
      "alias": null,
      "args": null,
      "kind": "ScalarField",
      "name": "field",
      "storageKey": null
    },
    {
      "alias": null,
      "args": null,
      "kind": "ScalarField",
      "name": "type",
      "storageKey": null
    }
  ],
  "storageKey": null
};
return {
  "fragment": {
    "argumentDefinitions": [
      (v0/*: any*/),
      (v1/*: any*/)
    ],
    "kind": "Fragment",
    "metadata": null,
    "name": "AssignedServiceAccountListAssignMutation",
    "selections": [
      {
        "alias": null,
        "args": (v2/*: any*/),
        "concreteType": "AssignServiceAccountToAgentPayload",
        "kind": "LinkedField",
        "name": "assignServiceAccountToAgent",
        "plural": false,
        "selections": [
          {
            "alias": null,
            "args": null,
            "concreteType": "Agent",
            "kind": "LinkedField",
            "name": "agent",
            "plural": false,
            "selections": [
              (v3/*: any*/)
            ],
            "storageKey": null
          },
          {
            "alias": null,
            "args": null,
            "concreteType": "ServiceAccount",
            "kind": "LinkedField",
            "name": "serviceAccount",
            "plural": false,
            "selections": [
              (v4/*: any*/),
              {
                "args": null,
                "kind": "FragmentSpread",
                "name": "AssignedServiceAccountListItemFragment_assignedServiceAccount"
              }
            ],
            "storageKey": null
          },
          (v5/*: any*/)
        ],
        "storageKey": null
      }
    ],
    "type": "Mutation",
    "abstractKey": null
  },
  "kind": "Request",
  "operation": {
    "argumentDefinitions": [
      (v1/*: any*/),
      (v0/*: any*/)
    ],
    "kind": "Operation",
    "name": "AssignedServiceAccountListAssignMutation",
    "selections": [
      {
        "alias": null,
        "args": (v2/*: any*/),
        "concreteType": "AssignServiceAccountToAgentPayload",
        "kind": "LinkedField",
        "name": "assignServiceAccountToAgent",
        "plural": false,
        "selections": [
          {
            "alias": null,
            "args": null,
            "concreteType": "Agent",
            "kind": "LinkedField",
            "name": "agent",
            "plural": false,
            "selections": [
              (v3/*: any*/),
              (v4/*: any*/)
            ],
            "storageKey": null
          },
          {
            "alias": null,
            "args": null,
            "concreteType": "ServiceAccount",
            "kind": "LinkedField",
            "name": "serviceAccount",
            "plural": false,
            "selections": [
              (v4/*: any*/),
              {
                "alias": null,
                "args": null,
                "kind": "ScalarField",
                "name": "name",
                "storageKey": null
              },
              {
                "alias": null,
                "args": null,
                "kind": "ScalarField",
                "name": "scope",
                "storageKey": null
              },
              {
                "alias": null,
                "args": null,
                "kind": "ScalarField",
                "name": "description",
                "storageKey": null
              },
              {
                "alias": null,
                "args": null,
                "concreteType": "ResourceMetadata",
                "kind": "LinkedField",
                "name": "metadata",
                "plural": false,
                "selections": [
                  {
                    "alias": null,
                    "args": null,
                    "kind": "ScalarField",
                    "name": "prn",
                    "storageKey": null
                  },
                  {
                    "alias": null,
                    "args": null,
                    "kind": "ScalarField",
                    "name": "updatedAt",
                    "storageKey": null
                  }
                ],
                "storageKey": null
              }
            ],
            "storageKey": null
          },
          {
            "alias": null,
            "args": null,
            "filters": null,
            "handle": "prependNode",
            "key": "",
            "kind": "LinkedHandle",
            "name": "serviceAccount",
            "handleArgs": [
              {
                "kind": "Variable",
                "name": "connections",
                "variableName": "connections"
              },
              {
                "kind": "Literal",
                "name": "edgeTypeName",
                "value": "ServiceAccountEdgeType"
              }
            ]
          },
          (v5/*: any*/)
        ],
        "storageKey": null
      }
    ]
  },
  "params": {
    "cacheID": "6a75ce097adbe7d3697088458979ad66",
    "id": null,
    "metadata": {},
    "name": "AssignedServiceAccountListAssignMutation",
    "operationKind": "mutation",
    "text": "mutation AssignedServiceAccountListAssignMutation(\n  $input: AssignServiceAccountToAgentInput!\n) {\n  assignServiceAccountToAgent(input: $input) {\n    agent {\n      assignedServiceAccounts(first: 0) {\n        totalCount\n      }\n      id\n    }\n    serviceAccount {\n      id\n      ...AssignedServiceAccountListItemFragment_assignedServiceAccount\n    }\n    problems {\n      message\n      field\n      type\n    }\n  }\n}\n\nfragment AssignedServiceAccountListItemFragment_assignedServiceAccount on ServiceAccount {\n  id\n  name\n  scope\n  description\n  metadata {\n    prn\n    updatedAt\n  }\n}\n"
  }
};
})();

(node as any).hash = "3fb685acca2d8dd692c07488de51627a";

export default node;
