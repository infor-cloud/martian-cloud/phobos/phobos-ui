/**
 * @generated SignedSource<<ec27bcb5c182fc12966add65ffd6fac4>>
 * @lightSyntaxTransform
 * @nogrep
 */

/* tslint:disable */
/* eslint-disable */
// @ts-nocheck

import { Fragment, ReaderFragment } from 'relay-runtime';
import { FragmentRefs } from "relay-runtime";
export type AgentSessionListItemFragment$data = {
  readonly active: boolean;
  readonly errorCount: number;
  readonly id: string;
  readonly internal: boolean;
  readonly lastContacted: any;
  readonly metadata: {
    readonly prn: string;
    readonly updatedAt: any;
  };
  readonly " $fragmentType": "AgentSessionListItemFragment";
};
export type AgentSessionListItemFragment$key = {
  readonly " $data"?: AgentSessionListItemFragment$data;
  readonly " $fragmentSpreads": FragmentRefs<"AgentSessionListItemFragment">;
};

const node: ReaderFragment = {
  "argumentDefinitions": [],
  "kind": "Fragment",
  "metadata": null,
  "name": "AgentSessionListItemFragment",
  "selections": [
    {
      "alias": null,
      "args": null,
      "kind": "ScalarField",
      "name": "id",
      "storageKey": null
    },
    {
      "alias": null,
      "args": null,
      "kind": "ScalarField",
      "name": "lastContacted",
      "storageKey": null
    },
    {
      "alias": null,
      "args": null,
      "kind": "ScalarField",
      "name": "active",
      "storageKey": null
    },
    {
      "alias": null,
      "args": null,
      "kind": "ScalarField",
      "name": "internal",
      "storageKey": null
    },
    {
      "alias": null,
      "args": null,
      "kind": "ScalarField",
      "name": "errorCount",
      "storageKey": null
    },
    {
      "alias": null,
      "args": null,
      "concreteType": "ResourceMetadata",
      "kind": "LinkedField",
      "name": "metadata",
      "plural": false,
      "selections": [
        {
          "alias": null,
          "args": null,
          "kind": "ScalarField",
          "name": "prn",
          "storageKey": null
        },
        {
          "alias": null,
          "args": null,
          "kind": "ScalarField",
          "name": "updatedAt",
          "storageKey": null
        }
      ],
      "storageKey": null
    }
  ],
  "type": "AgentSession",
  "abstractKey": null
};

(node as any).hash = "031e5d9f91f008a0d71c3cb85d147712";

export default node;
