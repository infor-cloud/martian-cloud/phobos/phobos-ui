/**
 * @generated SignedSource<<1905b304af96088bbafdb1f946589524>>
 * @lightSyntaxTransform
 * @nogrep
 */

/* tslint:disable */
/* eslint-disable */
// @ts-nocheck

import { Fragment, ReaderFragment } from 'relay-runtime';
export type ScopeType = "GLOBAL" | "ORGANIZATION" | "PROJECT" | "%future added value";
import { FragmentRefs } from "relay-runtime";
export type AssignedServiceAccountListItemFragment_assignedServiceAccount$data = {
  readonly description: string;
  readonly id: string;
  readonly metadata: {
    readonly prn: string;
    readonly updatedAt: any;
  };
  readonly name: string;
  readonly scope: ScopeType;
  readonly " $fragmentType": "AssignedServiceAccountListItemFragment_assignedServiceAccount";
};
export type AssignedServiceAccountListItemFragment_assignedServiceAccount$key = {
  readonly " $data"?: AssignedServiceAccountListItemFragment_assignedServiceAccount$data;
  readonly " $fragmentSpreads": FragmentRefs<"AssignedServiceAccountListItemFragment_assignedServiceAccount">;
};

const node: ReaderFragment = {
  "argumentDefinitions": [],
  "kind": "Fragment",
  "metadata": null,
  "name": "AssignedServiceAccountListItemFragment_assignedServiceAccount",
  "selections": [
    {
      "alias": null,
      "args": null,
      "kind": "ScalarField",
      "name": "id",
      "storageKey": null
    },
    {
      "alias": null,
      "args": null,
      "kind": "ScalarField",
      "name": "name",
      "storageKey": null
    },
    {
      "alias": null,
      "args": null,
      "kind": "ScalarField",
      "name": "scope",
      "storageKey": null
    },
    {
      "alias": null,
      "args": null,
      "kind": "ScalarField",
      "name": "description",
      "storageKey": null
    },
    {
      "alias": null,
      "args": null,
      "concreteType": "ResourceMetadata",
      "kind": "LinkedField",
      "name": "metadata",
      "plural": false,
      "selections": [
        {
          "alias": null,
          "args": null,
          "kind": "ScalarField",
          "name": "prn",
          "storageKey": null
        },
        {
          "alias": null,
          "args": null,
          "kind": "ScalarField",
          "name": "updatedAt",
          "storageKey": null
        }
      ],
      "storageKey": null
    }
  ],
  "type": "ServiceAccount",
  "abstractKey": null
};

(node as any).hash = "3318cc4de18e88ae21ebe5bada3065b5";

export default node;
