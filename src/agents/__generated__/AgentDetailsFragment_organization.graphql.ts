/**
 * @generated SignedSource<<f9a0310a0d20020397af892399f9e615>>
 * @lightSyntaxTransform
 * @nogrep
 */

/* tslint:disable */
/* eslint-disable */
// @ts-nocheck

import { Fragment, ReaderFragment } from 'relay-runtime';
import { FragmentRefs } from "relay-runtime";
export type AgentDetailsFragment_organization$data = {
  readonly id: string;
  readonly name: string;
  readonly " $fragmentType": "AgentDetailsFragment_organization";
};
export type AgentDetailsFragment_organization$key = {
  readonly " $data"?: AgentDetailsFragment_organization$data;
  readonly " $fragmentSpreads": FragmentRefs<"AgentDetailsFragment_organization">;
};

const node: ReaderFragment = {
  "argumentDefinitions": [],
  "kind": "Fragment",
  "metadata": null,
  "name": "AgentDetailsFragment_organization",
  "selections": [
    {
      "alias": null,
      "args": null,
      "kind": "ScalarField",
      "name": "id",
      "storageKey": null
    },
    {
      "alias": null,
      "args": null,
      "kind": "ScalarField",
      "name": "name",
      "storageKey": null
    }
  ],
  "type": "Organization",
  "abstractKey": null
};

(node as any).hash = "4e4498052b966d5a2086cd949c38486f";

export default node;
