/**
 * @generated SignedSource<<a0ce72d2e89e51b40117472f2509d74e>>
 * @lightSyntaxTransform
 * @nogrep
 */

/* tslint:disable */
/* eslint-disable */
// @ts-nocheck

import { ConcreteRequest, Query } from 'relay-runtime';
export type SharedServiceAccountAutocompleteQuery$variables = {
  search: string;
};
export type SharedServiceAccountAutocompleteQuery$data = {
  readonly serviceAccounts: {
    readonly edges: ReadonlyArray<{
      readonly node: {
        readonly description: string;
        readonly id: string;
        readonly name: string;
      } | null | undefined;
    } | null | undefined> | null | undefined;
  };
};
export type SharedServiceAccountAutocompleteQuery = {
  response: SharedServiceAccountAutocompleteQuery$data;
  variables: SharedServiceAccountAutocompleteQuery$variables;
};

const node: ConcreteRequest = (function(){
var v0 = [
  {
    "defaultValue": null,
    "kind": "LocalArgument",
    "name": "search"
  }
],
v1 = [
  {
    "alias": null,
    "args": [
      {
        "kind": "Literal",
        "name": "first",
        "value": 50
      },
      {
        "kind": "Literal",
        "name": "scopes",
        "value": [
          "GLOBAL"
        ]
      },
      {
        "kind": "Variable",
        "name": "search",
        "variableName": "search"
      }
    ],
    "concreteType": "ServiceAccountConnection",
    "kind": "LinkedField",
    "name": "serviceAccounts",
    "plural": false,
    "selections": [
      {
        "alias": null,
        "args": null,
        "concreteType": "ServiceAccountEdge",
        "kind": "LinkedField",
        "name": "edges",
        "plural": true,
        "selections": [
          {
            "alias": null,
            "args": null,
            "concreteType": "ServiceAccount",
            "kind": "LinkedField",
            "name": "node",
            "plural": false,
            "selections": [
              {
                "alias": null,
                "args": null,
                "kind": "ScalarField",
                "name": "id",
                "storageKey": null
              },
              {
                "alias": null,
                "args": null,
                "kind": "ScalarField",
                "name": "name",
                "storageKey": null
              },
              {
                "alias": null,
                "args": null,
                "kind": "ScalarField",
                "name": "description",
                "storageKey": null
              }
            ],
            "storageKey": null
          }
        ],
        "storageKey": null
      }
    ],
    "storageKey": null
  }
];
return {
  "fragment": {
    "argumentDefinitions": (v0/*: any*/),
    "kind": "Fragment",
    "metadata": null,
    "name": "SharedServiceAccountAutocompleteQuery",
    "selections": (v1/*: any*/),
    "type": "Query",
    "abstractKey": null
  },
  "kind": "Request",
  "operation": {
    "argumentDefinitions": (v0/*: any*/),
    "kind": "Operation",
    "name": "SharedServiceAccountAutocompleteQuery",
    "selections": (v1/*: any*/)
  },
  "params": {
    "cacheID": "ab1e154f1f5549e081da791183399843",
    "id": null,
    "metadata": {},
    "name": "SharedServiceAccountAutocompleteQuery",
    "operationKind": "query",
    "text": "query SharedServiceAccountAutocompleteQuery(\n  $search: String!\n) {\n  serviceAccounts(first: 50, search: $search, scopes: [GLOBAL]) {\n    edges {\n      node {\n        id\n        name\n        description\n      }\n    }\n  }\n}\n"
  }
};
})();

(node as any).hash = "f7cc52ae98217b7f1245e5c7b21e03b2";

export default node;
