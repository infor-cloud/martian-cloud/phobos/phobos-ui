/**
 * @generated SignedSource<<4e302f68ee35309a883fd3de9a636896>>
 * @lightSyntaxTransform
 * @nogrep
 */

/* tslint:disable */
/* eslint-disable */
// @ts-nocheck

import { Fragment, ReaderFragment } from 'relay-runtime';
import { FragmentRefs } from "relay-runtime";
export type AgentListFragment_agents$data = {
  readonly edges: ReadonlyArray<{
    readonly node: {
      readonly id: string;
      readonly " $fragmentSpreads": FragmentRefs<"AgentListItemFragment_agent">;
    } | null | undefined;
  } | null | undefined> | null | undefined;
  readonly " $fragmentType": "AgentListFragment_agents";
};
export type AgentListFragment_agents$key = {
  readonly " $data"?: AgentListFragment_agents$data;
  readonly " $fragmentSpreads": FragmentRefs<"AgentListFragment_agents">;
};

const node: ReaderFragment = {
  "argumentDefinitions": [],
  "kind": "Fragment",
  "metadata": null,
  "name": "AgentListFragment_agents",
  "selections": [
    {
      "alias": null,
      "args": null,
      "concreteType": "AgentEdge",
      "kind": "LinkedField",
      "name": "edges",
      "plural": true,
      "selections": [
        {
          "alias": null,
          "args": null,
          "concreteType": "Agent",
          "kind": "LinkedField",
          "name": "node",
          "plural": false,
          "selections": [
            {
              "alias": null,
              "args": null,
              "kind": "ScalarField",
              "name": "id",
              "storageKey": null
            },
            {
              "args": null,
              "kind": "FragmentSpread",
              "name": "AgentListItemFragment_agent"
            }
          ],
          "storageKey": null
        }
      ],
      "storageKey": null
    }
  ],
  "type": "AgentConnection",
  "abstractKey": null
};

(node as any).hash = "5dc9c1d9ae80eca33c14cc717519b275";

export default node;
