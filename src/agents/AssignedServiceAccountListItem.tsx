import { Avatar, Button, ListItem, ListItemText, Typography, useTheme } from '@mui/material';
import purple from '@mui/material/colors/purple';
import graphql from 'babel-plugin-relay/macro';
import moment from 'moment';
import Link from '../../src/routes/Link';
import DeleteIcon from '@mui/icons-material/CloseOutlined';
import { useFragment } from 'react-relay/hooks';
import { AssignedServiceAccountListItemFragment_assignedServiceAccount$key } from './__generated__/AssignedServiceAccountListItemFragment_assignedServiceAccount.graphql';

interface Props {
    fragmentRef: AssignedServiceAccountListItemFragment_assignedServiceAccount$key
    onDelete: (id: string, name: string) => void
}

function AssignedServiceAccountListItem({ fragmentRef, onDelete }: Props) {
    const theme = useTheme();

    const data = useFragment(graphql`
        fragment AssignedServiceAccountListItemFragment_assignedServiceAccount on ServiceAccount {
            id
            name
            scope
            description
            metadata {
                prn
                updatedAt
                prn
            }
        }
    `, fragmentRef);

    const route = (data.scope === 'ORGANIZATION')
        ? `/organizations/${data.metadata.prn.split(":")[2].split("/")[0]}/-/service_accounts/${data.id}`
        : `/admin/service_accounts/${data.id}`;

    return (
        <ListItem
            dense
            sx={{
                paddingY: data.description ? 0 : 1.5,
                borderBottom: `1px solid ${theme.palette.divider}`,
                borderLeft: `1px solid ${theme.palette.divider}`,
                borderRight: `1px solid ${theme.palette.divider}`,
                '&:last-child': {
                    borderBottomLeftRadius: 4,
                    borderBottomRightRadius: 4
                }
            }}>
            <Avatar variant="rounded" sx={{ width: 32, height: 32, bgcolor: purple[300], marginRight: 2 }}>
                {data.name[0].toUpperCase()}
            </Avatar>
            <ListItemText
                primary={
                    <Link
                        to={route}
                        color='inherit'
                        sx={{ fontSize: '16px', textDecoration: 'none' }}
                    >
                        {data.name}
                    </Link>}
                secondary={data.description} />
            <Typography variant="body2" color="textSecondary">
                {moment(data.metadata.updatedAt as moment.MomentInput).fromNow()}
            </Typography>
            <Button
                onClick={() => onDelete(data.id, data.name)}
                sx={{ ml: 2, minWidth: 40, padding: '2px' }}
                size="small"
                color="info"
                variant="outlined">
                <DeleteIcon />
            </Button>
        </ListItem>
    );
}

export default AssignedServiceAccountListItem
