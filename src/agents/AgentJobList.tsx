import { Box, Paper, Table, TableBody, TableCell, TableContainer, TableHead, TableRow, Typography, useTheme } from '@mui/material';
import graphql from 'babel-plugin-relay/macro';
import { useMemo } from 'react';
import InfiniteScroll from 'react-infinite-scroll-component';
import { useLazyLoadQuery, usePaginationFragment, useSubscription } from 'react-relay/hooks';
import { useParams } from 'react-router-dom';
import { GraphQLSubscriptionConfig } from "relay-runtime";
import ListSkeleton from '../skeletons/ListSkeleton';
import AgentJobListItem from './AgentJobListItem';
import { AgentJobListEventsSubscription } from './__generated__/AgentJobListEventsSubscription.graphql';
import { AgentJobListFragment_jobs$key } from './__generated__/AgentJobListFragment_jobs.graphql';
import { AgentJobListPaginationQuery } from './__generated__/AgentJobListPaginationQuery.graphql';
import { AgentJobListQuery } from './__generated__/AgentJobListQuery.graphql';

const jobEventsSubscription = graphql`subscription AgentJobListEventsSubscription($input: JobSubscriptionInput!) {
    jobEvents(input: $input) {
      action
      job {
        id
        ...AgentJobListItemFragment
      }
    }
  }`;

const query = graphql`
    query AgentJobListQuery($id: String!, $first: Int!, $after: String) {
        node(id: $id) {
            ... on Agent {
                id
                ...AgentJobListFragment_jobs
            }
        }
    }`

function AgentJobList() {
    const theme = useTheme();
    const agentId = useParams().agentId as string;

    const queryData = useLazyLoadQuery<AgentJobListQuery>(query, { id: agentId, first: 20 }, { fetchPolicy: 'store-and-network' });

    const { data, loadNext, hasNext } = usePaginationFragment<AgentJobListPaginationQuery, AgentJobListFragment_jobs$key>(
        graphql`
        fragment AgentJobListFragment_jobs on Agent
        @refetchable(queryName: "AgentJobListPaginationQuery") {
                type
                jobs(
                    first: $first
                    after: $after
                    sort: CREATED_AT_DESC
                ) @connection(key: "AgentJobList_jobs") {
                    totalCount
                    edges {
                        node {
                            id
                            ...AgentJobListItemFragment
                        }
                    }
                }
            }
        `, queryData.node
    );

    const jobSubscriptionConfig = useMemo<GraphQLSubscriptionConfig<AgentJobListEventsSubscription>>(() => ({
        variables: { input: { agentId } },
        subscription: jobEventsSubscription,
        onCompleted: () => console.log("Subscription completed"),
        onError: () => console.warn("Subscription error")
    }), [agentId]);

    useSubscription<AgentJobListEventsSubscription>(jobSubscriptionConfig);

    return (
        <Box sx={{ border: 1, borderTop: 0, borderBottomLeftRadius: 4, borderBottomRightRadius: 4, borderColor: 'divider' }}>
            <Box sx={{
                display: 'flex',
                flexDirection: 'row',
                justifyContent: 'space-between',
                alignItems: 'flex-start',
                p: 2,
                pb: 0,
                [theme.breakpoints.down('lg')]: {
                    flexDirection: 'column',
                    alignItems: 'flex-start',
                    '& > *': { mb: 2 }
                }
            }}>
                <Typography color="textSecondary">The following jobs have been claimed by this agent</Typography>
            </Box>
            {(!data?.jobs?.edges || data?.jobs?.edges?.length === 0) ? <Paper sx={{ p: 2, m: 2 }}>
                <Typography>This agent does not have any jobs.</Typography>
            </Paper>
                :
                <Box sx={{ p: 2 }}>
                    <Paper sx={{ borderBottomLeftRadius: 0, borderBottomRightRadius: 0, border: `1px solid ${theme.palette.divider}` }}>
                        <Box padding={2}>
                            <Typography variant="subtitle1">
                                {data?.jobs.totalCount} job{data?.jobs.totalCount !== 1 && 's'}
                            </Typography>
                        </Box>
                    </Paper>
                    <InfiniteScroll
                        dataLength={(data?.jobs?.edges && data?.jobs?.edges.length) ?? 0}
                        next={() => loadNext(20)}
                        hasMore={hasNext}
                        loader={<ListSkeleton rowCount={3} />}
                    >
                        <TableContainer sx={{
                            borderLeft: `1px solid ${theme.palette.divider}`,
                            borderRight: `1px solid ${theme.palette.divider}`,
                            borderBottom: `1px solid ${theme.palette.divider}`,
                            borderBottomLeftRadius: 4,
                            borderBottomRightRadius: 4,
                        }}>
                            <Table
                                sx={{ minWidth: 650, tableLayout: 'fixed' }}
                                aria-label="agent jobs"
                            >
                                <TableHead>
                                    <TableRow>
                                        <TableCell>Status</TableCell>
                                        <TableCell>ID</TableCell>
                                        <TableCell>Project</TableCell>
                                        <TableCell>Org</TableCell>
                                        <TableCell>Tags</TableCell>
                                        <TableCell>Duration</TableCell>
                                        <TableCell>Created</TableCell>
                                    </TableRow>
                                </TableHead>
                                <TableBody>
                                    {data.jobs?.edges?.map((edge: any) => (
                                        <AgentJobListItem
                                            key={edge.node.id}
                                            fragmentRef={edge.node}
                                        />
                                    ))}
                                </TableBody>
                            </Table>
                        </TableContainer>
                    </InfiniteScroll>
                </Box>}
        </Box>
    );
}

export default AgentJobList
