import { Box, Button, List, Paper, Typography, useTheme } from '@mui/material';
import graphql from 'babel-plugin-relay/macro';
import { useState } from 'react';
import InfiniteScroll from 'react-infinite-scroll-component';
import { ConnectionHandler, useLazyLoadQuery, useMutation, usePaginationFragment } from 'react-relay/hooks';
import { useParams } from 'react-router-dom';
import { MutationError } from '../common/error';
import { ServiceAccountOption } from '../members/ServiceAccountAutocomplete';
import ListSkeleton from '../skeletons/ListSkeleton';
import AssignServiceAccountDialog from './AssignServiceAccountDialog';
import AssignedServiceAccountListItem from './AssignedServiceAccountListItem';
import UnassignServiceAccountDialog from './UnassignServiceAccountDialog';
import { AssignedServiceAccountListAssignMutation } from './__generated__/AssignedServiceAccountListAssignMutation.graphql';
import { AssignedServiceAccountListFragment_assignedServiceAccounts$key } from './__generated__/AssignedServiceAccountListFragment_assignedServiceAccounts.graphql';
import { AssignedServiceAccountListPaginationQuery } from './__generated__/AssignedServiceAccountListPaginationQuery.graphql';
import { AssignedServiceAccountListQuery } from './__generated__/AssignedServiceAccountListQuery.graphql';
import { AssignedServiceAccountListUnassignMutation } from './__generated__/AssignedServiceAccountListUnassignMutation.graphql';

function GetConnections(agentId: string): [string] {
    const connectionId = ConnectionHandler.getConnectionID(
        agentId,
        "AssignedServiceAccountList_assignedServiceAccounts"
    );
    return [connectionId];
}

const query = graphql`
    query AssignedServiceAccountListQuery($id: String!, $first: Int!, $after: String) {
        node(id: $id) {
            ... on Agent {
                id
                ...AssignedServiceAccountListFragment_assignedServiceAccounts
            }
        }
    }`

function AssignedServiceAccountList() {
    const [showAssignServiceAccountDialog, setShowAssignServiceAccountDialog] = useState(false);
    const [showUnassignServiceAccountDialog, setShowUnassignServiceAccountDialog] = useState(false);
    const [error, setError] = useState<MutationError | null>(null);
    const [selectedServiceAccount, setSelectedServiceAccount] = useState<{ id: string, name: string }>({ id: '', name: '' });
    const theme = useTheme();
    const agentId = useParams().agentId as string;

    const queryData = useLazyLoadQuery<AssignedServiceAccountListQuery>(query, { id: agentId, first: 100 }, { fetchPolicy: 'store-and-network' });

    const { data, loadNext, hasNext } = usePaginationFragment<AssignedServiceAccountListPaginationQuery, AssignedServiceAccountListFragment_assignedServiceAccounts$key>(
        graphql`
        fragment AssignedServiceAccountListFragment_assignedServiceAccounts on Agent
        @refetchable(queryName: "AssignedServiceAccountListPaginationQuery") {
                type
                organization {
                    id
                }
                assignedServiceAccounts(
                    first: $first
                    after: $after
                ) @connection(key: "AssignedServiceAccountList_assignedServiceAccounts") {
                    totalCount
                    edges {
                        node {
                            id
                            ...AssignedServiceAccountListItemFragment_assignedServiceAccount
                        }
                    }
                }
            }
        `, queryData.node
    );

    const [commitAssign, assignCommitInFlight] = useMutation<AssignedServiceAccountListAssignMutation>(graphql`
        mutation AssignedServiceAccountListAssignMutation($input: AssignServiceAccountToAgentInput!, $connections: [ID!]!) {
            assignServiceAccountToAgent(input: $input) {
                agent {
                    assignedServiceAccounts (first: 0) {
                        totalCount
                    }
                }
                serviceAccount @prependNode(connections: $connections, edgeTypeName: "ServiceAccountEdgeType") {
                    id
                    ...AssignedServiceAccountListItemFragment_assignedServiceAccount
                }
                problems {
                    message
                    field
                    type
                }
            }
        }
    `);

    const onServiceAccountChange = (serviceAccount: ServiceAccountOption | null) => {
        serviceAccount ? setSelectedServiceAccount({ ...selectedServiceAccount, id: serviceAccount.id }) : setSelectedServiceAccount({ id: '', name: '' });
        setError(null)
    };

    const resetAssignDialog = () => {
        setShowAssignServiceAccountDialog(false)
        setSelectedServiceAccount({ id: '', name: '' })
        setError(null)
    };

    const resetUnassignDialog = () => {
        setShowUnassignServiceAccountDialog(false)
        setSelectedServiceAccount({ id: '', name: '' })
        setError(null)
    };

    const onDelete = (id: string, name: string) => {
        setSelectedServiceAccount({ id, name })
        setShowUnassignServiceAccountDialog(true)
    }

    const assignServiceAccount = (confirm?: boolean) => {
        if (confirm && selectedServiceAccount.id) {
            commitAssign({
                variables: {
                    input: {
                        agentId: agentId,
                        serviceAccountId: selectedServiceAccount.id
                    },
                    connections: GetConnections(agentId)
                },
                updater: (store, data) => {
                    if (data.assignServiceAccountToAgent.serviceAccount) {
                        const agentRecord = store.get(agentId);
                        if (agentRecord) {
                            const connectionRecord = ConnectionHandler.getConnection(
                                agentRecord,
                                'AssignedServiceAccountList_assignedServiceAccounts',
                            );
                            if (connectionRecord) {
                                const totalCount = connectionRecord.getValue('totalCount') as number;
                                connectionRecord.setValue(totalCount + 1, 'totalCount');
                            }
                        }
                    }
                },
                onCompleted: data => {
                    if (data.assignServiceAccountToAgent.problems.length) {
                        setError({
                            severity: 'warning',
                            message: data.assignServiceAccountToAgent.problems.map(problem => problem.message).join('; ')
                        });
                    } else {
                        resetAssignDialog()
                    }
                },
                onError: error => {
                    setError({
                        severity: 'error',
                        message: `Unexpected Error Occurred: ${error.message}`
                    });
                }
            })
        } else {
            resetAssignDialog()
        }
    };

    const [commitUnassign, unAssignCommitInFlight] = useMutation<AssignedServiceAccountListUnassignMutation>(graphql`
        mutation AssignedServiceAccountListUnassignMutation($input: AssignServiceAccountToAgentInput!, $connections: [ID!]!) {
            unassignServiceAccountFromAgent(input: $input) {
                agent {
                    assignedServiceAccounts (first: 0) {
                        totalCount
                    }
                }
                serviceAccount {
                    id @deleteEdge(connections: $connections)
                }
                problems {
                    message
                    field
                    type
                }
            }
        }
    `);

    const unassignServiceAccount = (confirm?: boolean) => {
        if (confirm && selectedServiceAccount.id) {
            commitUnassign({
                variables: {
                    input: {
                        agentId: agentId,
                        serviceAccountId: selectedServiceAccount.id
                    },
                    connections: GetConnections(agentId)
                },
                updater: (store, data) => {
                    if (data.unassignServiceAccountFromAgent.serviceAccount) {
                        const agentRecord = store.get(agentId);
                        if (agentRecord) {
                            const connectionRecord = ConnectionHandler.getConnection(
                                agentRecord,
                                'AssignedServiceAccountList_assignedServiceAccounts',
                            );
                            if (connectionRecord) {
                                const totalCount = connectionRecord.getValue('totalCount') as number;
                                connectionRecord.setValue(totalCount - 1, 'totalCount');
                            }
                        }
                    }
                },
                onCompleted: data => {
                    if (data.unassignServiceAccountFromAgent.problems.length) {
                        setError({
                            severity: 'warning',
                            message: data.unassignServiceAccountFromAgent.problems.map(problem => problem.message).join('; ')
                        });
                    }
                    else {
                        resetUnassignDialog()
                    }
                },
                onError: error => {
                    setError({
                        severity: 'error',
                        message: `Unexpected Error Occurred: ${error.message}`
                    });
                }
            })
        } else {
            resetUnassignDialog()
        }
    };

    return (
        <Box sx={{ border: 1, borderTop: 0, borderBottomLeftRadius: 4, borderBottomRightRadius: 4, borderColor: 'divider' }}>
            <Box sx={{
                display: 'flex',
                flexDirection: 'row',
                justifyContent: 'space-between',
                alignItems: 'flex-start',
                p: 2,
                pb: 0,
                [theme.breakpoints.down('lg')]: {
                    flexDirection: 'column',
                    alignItems: 'flex-start',
                    '& > *': { mb: 2 }
                }
            }}>
                <Typography color="textSecondary">Service accounts are used to authenticate with the Phobos API, which allows an agent to interact with Phobos.</Typography>
                <Button
                    sx={{ width: 225 }}
                    size="small"
                    color="secondary"
                    variant="outlined"
                    onClick={() => setShowAssignServiceAccountDialog(true)}
                >
                    Assign Service Account
                </Button>
            </Box>
            {(!data?.assignedServiceAccounts?.edges || data?.assignedServiceAccounts?.edges?.length === 0) ? <Paper sx={{ p: 2, m: 2 }}>
                <Typography>No service accounts are assigned to this agent.</Typography>
            </Paper>
                :
                <Box sx={{ p: 2 }}>
                    <Paper sx={{ borderBottomLeftRadius: 0, borderBottomRightRadius: 0, border: `1px solid ${theme.palette.divider}` }}>
                        <Box padding={2}>
                            <Typography variant="subtitle1">
                                {data?.assignedServiceAccounts.totalCount} assigned service account{data?.assignedServiceAccounts.totalCount !== 1 && 's'}
                            </Typography>
                        </Box>
                    </Paper>
                    <InfiniteScroll
                        dataLength={(data?.assignedServiceAccounts?.edges && data?.assignedServiceAccounts?.edges.length) ?? 0}
                        next={() => loadNext(20)}
                        hasMore={hasNext}
                        loader={<ListSkeleton rowCount={3} />}
                    >
                        <List disablePadding>{data?.assignedServiceAccounts?.edges?.map((edge: any) => <AssignedServiceAccountListItem
                            key={edge.node.id}
                            fragmentRef={edge.node}
                            onDelete={onDelete}
                        />)}
                        </List>
                    </InfiniteScroll>
                </Box>}
            {showAssignServiceAccountDialog && <AssignServiceAccountDialog
                error={error}
                type={data?.type}
                onClose={assignServiceAccount}
                serviceAccountId={selectedServiceAccount.id}
                orgId={data?.organization?.id ?? ''}
                assignCommitInFlight={assignCommitInFlight}
                onServiceAccountChange={onServiceAccountChange}
            />}
            {showUnassignServiceAccountDialog && <UnassignServiceAccountDialog
                error={error}
                onClose={unassignServiceAccount}
                name={selectedServiceAccount.name}
                unAssignCommitInFlight={unAssignCommitInFlight}
            />}
        </Box>
    );
}

export default AssignedServiceAccountList;
