import { Box, Button, Table, TableBody, TableCell, TableContainer, TableHead, TableRow, Typography, useTheme } from '@mui/material';
import graphql from 'babel-plugin-relay/macro';
import InfiniteScroll from 'react-infinite-scroll-component';
import { LoadMoreFn, useFragment } from 'react-relay/hooks';
import { Link as RouterLink } from 'react-router-dom';
import { AdminAreaAgentsPaginationQuery } from '../administrator/agents/__generated__/AdminAreaAgentsPaginationQuery.graphql';
import { OrganizationPaginationQuery } from '../nav/__generated__/OrganizationPaginationQuery.graphql';
import ListSkeleton from '../skeletons/ListSkeleton';
import AgentListItem from './AgentListItem';
import { AgentListFragment_agents$key } from './__generated__/AgentListFragment_agents.graphql';

const DESCRIPTION = 'Agents are responsible for claiming and launching pipeline jobs.';

interface Props {
    fragmentRef: AgentListFragment_agents$key
    loadNext: LoadMoreFn<OrganizationPaginationQuery | AdminAreaAgentsPaginationQuery>
    hasNext: boolean
}

function AgentList({ fragmentRef, loadNext, hasNext }: Props) {
    const theme = useTheme();

    const data = useFragment<AgentListFragment_agents$key>(graphql`
        fragment AgentListFragment_agents on AgentConnection {
            edges {
                node {
                    id
                    ...AgentListItemFragment_agent
                }
            }
        }
    `, fragmentRef);

    return (
        <Box>
            {data?.edges?.length !== 0 ? <Box>
                <Box>
                    <Box sx={{
                        display: 'flex',
                        flexDirection: 'row',
                        justifyContent: 'space-between',
                        mb: 2,
                        [theme.breakpoints.down('md')]: {
                            flexDirection: 'column',
                            alignItems: 'flex-start',
                            '& > *': { mb: 2 },
                        }
                    }}>
                        <Box>
                            <Typography variant="h5" gutterBottom>Agents</Typography>
                            <Typography variant="body2">
                                {DESCRIPTION}
                            </Typography>
                        </Box>
                        <Box>
                            <Button
                                sx={{ minWidth: 125 }}
                                component={RouterLink}
                                variant="outlined"
                                to="new"
                            >
                                New Agent
                            </Button>
                        </Box>
                    </Box>
                </Box>
                <InfiniteScroll
                    dataLength={data?.edges?.length ?? 0}
                    next={() => loadNext(20)}
                    hasMore={hasNext}
                    loader={<ListSkeleton rowCount={3} />}
                >
                    <TableContainer>
                        <Table
                            sx={{ minWidth: 650, tableLayout: 'fixed' }}
                            aria-label="agents"
                        >
                            <TableHead>
                                <TableRow>
                                    <TableCell>Name</TableCell>
                                    <TableCell>Tags</TableCell>
                                    <TableCell>Created By</TableCell>
                                </TableRow>
                            </TableHead>
                            <TableBody>
                                {data?.edges?.map((edge: any) => (
                                    <AgentListItem key={edge.node.id} fragmentRef={edge.node} />
                                ))}
                            </TableBody>
                        </Table>
                    </TableContainer>
                </InfiniteScroll>
            </Box> : <Box sx={{ mt: 4 }} display="flex" justifyContent="center">
                <Box p={4} display="flex" flexDirection="column" justifyContent="center" alignItems="center" sx={{ maxWidth: 600 }}>
                    <Typography variant="h6">Get started with agents</Typography>
                    <Typography sx={{ mb: 2 }} color="textSecondary" align="center" >
                        {DESCRIPTION}
                    </Typography>
                    <Button component={RouterLink} variant="outlined" to="new">New Agent</Button>
                </Box>
            </Box>}
        </Box>
    );
}

export default AgentList
