import { Alert, Button, Dialog, DialogActions, DialogContent, DialogTitle } from '@mui/material';
import LoadingButton from '@mui/lab/LoadingButton';
import { MutationError } from '../common/error';
import ServiceAccountAutocomplete, { ServiceAccountOption } from '../members/ServiceAccountAutocomplete';
import SharedServiceAccountAutocomplete from './SharedServiceAccountAutocomplete';
import { AgentType } from './__generated__/AssignedServiceAccountListFragment_assignedServiceAccounts.graphql';

interface Props {
    error?: MutationError | null
    type: AgentType | undefined
    serviceAccountId: string
    orgId: string
    assignCommitInFlight: boolean
    onClose: (confirm?: boolean) => void
    onServiceAccountChange: (serviceAccount: ServiceAccountOption | null) => void
}

function AssignServiceAccountDialog({ onClose, assignCommitInFlight, error, serviceAccountId, orgId, onServiceAccountChange, type }: Props) {

    return (
        <Dialog
            fullWidth
            maxWidth="sm"
            open>
            <DialogTitle>
                Assign Service Account
            </DialogTitle>
            <DialogContent dividers>
                {error && <Alert sx={{ mb: 2 }} severity={error.severity}>
                    {error.message}
                </Alert>}
                {type === 'SHARED' ?
                    <SharedServiceAccountAutocomplete onSelected={onServiceAccountChange} />
                    : <ServiceAccountAutocomplete
                        orgId={orgId}
                        onSelected={onServiceAccountChange} />}
            </DialogContent>
            <DialogActions>
                <Button
                    size="small"
                    variant="outlined"
                    onClick={() => onClose()}
                    color="inherit"
                >
                    Cancel</Button>
                <LoadingButton
                    disabled={!serviceAccountId || !!error}
                    loading={assignCommitInFlight}
                    size="small"
                    variant="contained"
                    color="primary"
                    sx={{ ml: 2 }}
                    onClick={() => onClose(true)}
                >
                    Assign
                </LoadingButton>
            </DialogActions>
        </Dialog>
    );
}

export default AssignServiceAccountDialog;
