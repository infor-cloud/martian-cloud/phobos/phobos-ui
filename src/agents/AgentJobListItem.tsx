import { Box, Chip, Stack, TableCell, TableRow, Tooltip, Typography } from '@mui/material';
import graphql from 'babel-plugin-relay/macro';
import humanizeDuration from 'humanize-duration';
import moment from 'moment';
import { useFragment } from 'react-relay/hooks';
import Link from '../../src/routes/Link';
import JobStatusChip from '../organizations/projects/pipelines/JobStatusChip';
import { AgentJobListItemFragment$key } from './__generated__/AgentJobListItemFragment.graphql';

interface Props {
    fragmentRef: AgentJobListItemFragment$key
}

function AgentJobListItem({ fragmentRef }: Props) {
    const data = useFragment(graphql`
        fragment AgentJobListItemFragment on Job {
            id
            status
            type
            timestamps {
                queuedAt
                pendingAt
                runningAt
                finishedAt
            }
            project {
                name
                organizationName
            }
            data {
                ... on JobTaskData {
                    pipelineId
                    taskPath
                }
            }
            tags
            metadata {
                prn
                createdAt
                updatedAt
            }
        }
    `, fragmentRef);

    const timestamps = data.timestamps;
    const duration = timestamps?.finishedAt ?
        moment.duration(moment(timestamps.finishedAt as moment.MomentInput).diff(moment(timestamps.runningAt as moment.MomentInput))) : null;

    const taskPath = data.data?.taskPath ? encodeURIComponent(data.data.taskPath) : '';
    const linkToTask = `/organizations/${data.project.organizationName}/projects/${data.project.name}/-/pipelines/${data.data?.pipelineId}/task/${taskPath}`;

    return (
        <TableRow sx={{ '&:last-child td, &:last-child th': { border: 0 } }}>
            <TableCell>
                <JobStatusChip status={data.status} to={linkToTask} />
            </TableCell>
            <TableCell>
                <Link color="inherit" to={linkToTask}>{data.id.substring(0, 8)}...</Link>
            </TableCell>
            <TableCell>
                <Link color="inherit" to={`/organizations/${data.project.organizationName}/projects/${data.project.name}`}>{data.project.name}</Link>
            </TableCell>
            <TableCell>
                <Link color="inherit" to={`/organizations/${data.project.organizationName}`}>{data.project.organizationName}</Link>
            </TableCell>
            <TableCell>
                {data.tags.length > 0 && <Stack direction="row" spacing={1}>
                    {data.tags.map(tag => <Chip key={tag} size="small" color="secondary" label={tag} />)}
                </Stack>}
                {data.tags.length === 0 && <Typography variant="body2" color="textSecondary">None</Typography>}
            </TableCell>
            <TableCell>
                {duration ? humanizeDuration(duration.asMilliseconds()) : '--'}
            </TableCell>
            <TableCell>
                <Tooltip title={data.metadata.createdAt}>
                    <Box>{moment(data.metadata.createdAt as moment.MomentInput).fromNow()}</Box>
                </Tooltip>
            </TableCell>
        </TableRow >
    );
}

export default AgentJobListItem;
