import { ArrowDropUp } from '@mui/icons-material';
import { default as ArrowDropDown, default as ArrowDropDownIcon } from '@mui/icons-material/ArrowDropDown';
import { LoadingButton } from '@mui/lab';
import { Alert, Box, Button, ButtonGroup, Chip, Collapse, Dialog, DialogActions, DialogContent, DialogTitle, Link, Menu, MenuItem, Stack, Tab, Tabs, Typography, useTheme } from "@mui/material";
import { green } from "@mui/material/colors";
import graphql from 'babel-plugin-relay/macro';
import moment from 'moment';
import { useSnackbar } from 'notistack';
import React, { useMemo, useState } from 'react';
import { useFragment, useMutation, useSubscription } from 'react-relay/hooks';
import { useNavigate, useSearchParams } from 'react-router-dom';
import { ConnectionHandler, ConnectionInterface, GraphQLSubscriptionConfig, RecordSourceProxy } from "relay-runtime";
import { GetConnections as GetAdminAreaConnections } from "../administrator/agents/AdminAreaAgents";
import { AgentIcon } from "../common/Icons";
import TabContent from "../common/TabContent";
import { GetConnections as GetOrganizationAgentConnections } from "../organizations/agents/OrganizationAgents";
import AgentChip from './AgentChip';
import AgentJobList from "./AgentJobList";
import PRNButton from '../common/PRNButton';
import AgentSessionList, { GetConnections as GetAgentSessionConnections } from "./AgentSessionList";
import AssignedServiceAccountList from "./AssignedServiceAccountList";
import { AgentDetailsDeleteMutation } from "./__generated__/AgentDetailsDeleteMutation.graphql";
import { AgentDetailsFragment_agent$key } from "./__generated__/AgentDetailsFragment_agent.graphql";
import { AgentDetailsSessionEventsSubscription, AgentDetailsSessionEventsSubscription$data } from "./__generated__/AgentDetailsSessionEventsSubscription.graphql";


const agentSessionEventsSubscription = graphql`subscription AgentDetailsSessionEventsSubscription($input: AgentSessionEventSubscriptionInput!) {
    agentSessionEvents(input: $input) {
      action
      agentSession {
        id
        ...AgentSessionListItemFragment
      }
    }
  }`;

interface ConfirmationDialogProps {
    agentName: string
    deleteInProgress: boolean;
    keepMounted: boolean;
    open: boolean;
    onClose: (confirm?: boolean) => void
}

function DeleteConfirmationDialog(props: ConfirmationDialogProps) {
    const { agentName, deleteInProgress, onClose, open, ...other } = props;

    return (
        <Dialog
            maxWidth="xs"
            open={open}
            {...other}
        >
            <DialogTitle>Delete Agent</DialogTitle>
            <DialogContent dividers>
                Are you sure you want to delete agent <strong>{agentName}</strong>?
            </DialogContent>
            <DialogActions>
                <Button color="inherit" onClick={() => onClose()}>
                    Cancel
                </Button>
                <LoadingButton color="error" loading={deleteInProgress} onClick={() => onClose(true)}>
                    Delete
                </LoadingButton>
            </DialogActions>
        </Dialog>
    );
}

interface Props {
    fragmentRef: AgentDetailsFragment_agent$key
    orgId?: string
}

function AgentDetails({ fragmentRef, orgId }: Props) {
    const [showMore, setShowMore] = useState(false);
    const [menuAnchorEl, setMenuAnchorEl] = useState<Element | null>(null);
    const [showDeleteConfirmationDialog, setShowDeleteConfirmationDialog] = useState<boolean>(false);
    const [searchParams, setSearchParams] = useSearchParams();
    const { enqueueSnackbar } = useSnackbar();
    const navigate = useNavigate();
    const tab = searchParams.get('tab') || 'details';
    const theme = useTheme();

    const agent = useFragment<AgentDetailsFragment_agent$key>(graphql`
        fragment AgentDetailsFragment_agent on Agent
        {
            id
            name
            type
            disabled
            description
            createdBy
            tags
            runUntaggedJobs
            metadata {
                createdAt
                prn
            }
            assignedServiceAccounts (first: 0) {
                totalCount
            }
            sessions(first: 1, sort: LAST_CONTACTED_AT_DESC) {
                edges {
                    node {
                        active
                        lastContacted
                    }
                }
            }
        }
    `, fragmentRef);

    const [commit, commitInFlight] = useMutation<AgentDetailsDeleteMutation>(graphql`
            mutation AgentDetailsDeleteMutation($input: DeleteAgentInput!, $connections: [ID!]!) {
                deleteAgent(input: $input) {
                    agent {
                        id @deleteEdge(connections: $connections)
                    }
                    problems {
                        message
                        field
                        type
                    }
                }
            }
        `);

    const agentSessionsSubscriptionConfig = useMemo<GraphQLSubscriptionConfig<AgentDetailsSessionEventsSubscription>>(() => ({
        variables: { input: { agentId: agent.id } },
        subscription: agentSessionEventsSubscription,
        onCompleted: () => console.log("Subscription completed"),
        onError: () => console.warn("Subscription error"),
        updater: (store: RecordSourceProxy, payload: AgentDetailsSessionEventsSubscription$data) => {
            const record = store.get(payload.agentSessionEvents.agentSession.id);
            if (record == null) {
                return;
            }
            GetAgentSessionConnections(agent.id).forEach(id => {
                const connectionRecord = store.get(id);
                if (connectionRecord) {
                    const { NODE, EDGES } = ConnectionInterface.get();

                    const recordId = record.getDataID();
                    // Check if edge already exists in connection
                    const nodeAlreadyExistsInConnection = connectionRecord
                        .getLinkedRecords(EDGES)
                        ?.some(
                            edge => edge?.getLinkedRecord(NODE)?.getDataID() === recordId,
                        );
                    if (!nodeAlreadyExistsInConnection) {
                        const totalCount = connectionRecord.getValue('totalCount') as number;
                        connectionRecord.setValue(totalCount + 1, 'totalCount');

                        // Create Edge
                        const edge = ConnectionHandler.createEdge(
                            store,
                            connectionRecord,
                            record,
                            'AgentSessionEdge'
                        );
                        if (edge) {
                            // Add edge to the beginning of the connection
                            ConnectionHandler.insertEdgeBefore(
                                connectionRecord,
                                edge,
                            );
                        }
                    }
                }
            });
        }
    }), [agent.id]);

    useSubscription<AgentDetailsSessionEventsSubscription>(agentSessionsSubscriptionConfig);

    const onDeleteConfirmationDialogClosed = (confirm?: boolean) => {
        if (confirm) {
            commit({
                variables: {
                    input: {
                        id: agent.id
                    },
                    connections: orgId ? GetOrganizationAgentConnections(orgId) : GetAdminAreaConnections()
                },
                onCompleted: data => {
                    setShowDeleteConfirmationDialog(false);

                    if (data.deleteAgent.problems.length) {
                        enqueueSnackbar(data.deleteAgent.problems.map((problem: any) => problem.message).join('; '), { variant: 'warning' });
                    } else {
                        navigate(`..`);
                    }
                },
                onError: error => {
                    setShowDeleteConfirmationDialog(false);
                    enqueueSnackbar(`Unexpected error occurred: ${error.message}`, { variant: 'error' });
                }
            });
        } else {
            setShowDeleteConfirmationDialog(false);
        }
    };

    const onTabChange = (event: React.SyntheticEvent, newValue: string) => {
        searchParams.set('tab', newValue);
        setSearchParams(searchParams, { replace: true });
    };

    const onOpenMenu = (event: React.MouseEvent<HTMLButtonElement>) => {
        setMenuAnchorEl(event.currentTarget);
    };

    const onMenuClose = () => {
        setMenuAnchorEl(null);
    };

    const onMenuAction = (actionCallback: () => void) => {
        setMenuAnchorEl(null);
        actionCallback();
    };

    const lastContacted = agent.sessions?.edges?.[0]?.node?.lastContacted;
    const active = agent.sessions?.edges?.[0]?.node?.active;
    const showBanner = !active && agent.assignedServiceAccounts.totalCount === 0;

    return (
        <Box>
            {showBanner &&
                <Alert sx={{ mb: 2, mt: 2 }} severity="warning" variant="outlined">No service accounts are assigned to this agent.
                </Alert>}
            <Box sx={{
                display: 'flex',
                flexDirection: 'row',
                justifyContent: 'space-between',
                [theme.breakpoints.down('sm')]: {
                    flexDirection: 'column',
                    alignItems: 'flex-start',
                    '& > *': { mb: 2 },
                }
            }}>
                <Box display="flex" alignItems="center" mb={2}>
                    <AgentIcon sx={{ color: active ? green[400] : null, mr: 2 }} />
                    <Box>
                        <Box display="flex" alignItems="center">
                            <Typography variant="h5" marginRight={1}>
                                {agent.name}
                            </Typography>
                            <AgentChip disabled={agent.disabled} />
                        </Box>
                        <Typography color="textSecondary">{agent.description}</Typography>
                        {lastContacted && <Typography variant="caption" color="textSecondary">
                            {`last seen ${moment(lastContacted as moment.MomentInput).fromNow()}`}
                        </Typography>}
                    </Box>
                </Box>
                <Box>
                    <Stack direction="row" spacing={1}>
                        <PRNButton prn={agent.metadata.prn} />
                        <ButtonGroup variant="outlined" color="primary">
                            <Button onClick={() => navigate('edit')}>Edit</Button>
                            <Button
                                color="primary"
                                size="small"
                                aria-label="more options menu"
                                aria-haspopup="menu"
                                onClick={onOpenMenu}
                            >
                                <ArrowDropDownIcon fontSize="small" />
                            </Button>
                        </ButtonGroup>
                    </Stack>
                    <Menu
                        id="agent-more-options-menu"
                        anchorEl={menuAnchorEl}
                        open={Boolean(menuAnchorEl)}
                        onClose={onMenuClose}
                        anchorOrigin={{
                            vertical: 'bottom',
                            horizontal: 'right',
                        }}
                        transformOrigin={{
                            vertical: 'top',
                            horizontal: 'right',
                        }}
                    >
                        <MenuItem onClick={() => onMenuAction(() => setShowDeleteConfirmationDialog(true))}>
                            Delete Agent
                        </MenuItem>
                    </Menu>
                </Box>
            </Box>
            <Box sx={{ display: "flex", border: 1, borderColor: 'divider', borderTopLeftRadius: 4, borderTopRightRadius: 4 }}>
                <Tabs value={tab} onChange={onTabChange}>
                    <Tab label="Details" value="details" />
                    <Tab label="Sessions" value="sessions" />
                    <Tab label="Jobs" value="jobs" />
                    <Tab label="Assigned Service Accounts" value="assignedServiceAccounts" />
                </Tabs>
            </Box>
            <TabContent>
                {tab === 'details' && <Box sx={{ border: 1, borderTop: 0, borderBottomLeftRadius: 4, borderBottomRightRadius: 4, borderColor: 'divider', padding: 2 }}>
                    <Box mb={2}>
                        <Typography mb={1}>Type</Typography>
                        <Typography color="textSecondary">{agent.type[0].toUpperCase() + agent.type.slice(1).toLowerCase()}</Typography>
                    </Box>
                    <Box mb={2}>
                        <Typography mb={1}>Tags</Typography>
                        {agent.tags.length > 0 && <Stack direction="row" spacing={1}>
                            {agent.tags.map(tag => <Chip key={tag} size="small" color="secondary" label={tag} />)}
                        </Stack>}
                        {agent.tags.length === 0 && <Typography color="textSecondary">No Tags</Typography>}
                    </Box>
                    <Box mb={2}>
                        <Typography mb={1}>Run Untagged Jobs</Typography>
                        <Typography color="textSecondary">{agent.runUntaggedJobs ? 'Yes' : 'No'}</Typography>
                    </Box>
                    <Box mt={4}>
                        <Link
                            sx={{ display: "flex", alignItems: "center" }}
                            component="button" variant="body1"
                            color="textSecondary"
                            underline="hover"
                            onClick={() => setShowMore(!showMore)}
                        >
                            More Details {showMore ? <ArrowDropUp /> : <ArrowDropDown />}
                        </Link>
                        <Collapse in={showMore} timeout="auto" unmountOnExit>
                            <Box mt={2}>
                                <Typography variant="body2">
                                    Created {moment(agent.metadata.createdAt as moment.MomentInput).fromNow()} by {agent.createdBy}
                                </Typography>
                            </Box>
                        </Collapse>
                    </Box>
                </Box>}
                {tab === 'sessions' && <AgentSessionList />}
                {tab === 'jobs' && <AgentJobList />}
                {tab === 'assignedServiceAccounts' && <AssignedServiceAccountList />}
            </TabContent>
            <DeleteConfirmationDialog
                agentName={agent.name}
                keepMounted
                deleteInProgress={commitInFlight}
                open={showDeleteConfirmationDialog}
                onClose={onDeleteConfirmationDialogClosed}
            />
        </Box>
    );
}

export default AgentDetails
