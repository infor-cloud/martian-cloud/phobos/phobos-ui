import { Box, Chip, Stack, TableCell, TableRow, Tooltip, Typography } from '@mui/material';
import graphql from 'babel-plugin-relay/macro';
import moment from 'moment';
import { useFragment } from 'react-relay/hooks';
import Gravatar from '../common/Gravatar';
import Link from '../routes/Link';
import AgentChip from './AgentChip';
import { AgentListItemFragment_agent$key } from './__generated__/AgentListItemFragment_agent.graphql';

interface Props {
    fragmentRef: AgentListItemFragment_agent$key
}

function AgentListItem({ fragmentRef }: Props) {

    const data = useFragment<AgentListItemFragment_agent$key>(graphql`
        fragment AgentListItemFragment_agent on Agent {
            metadata {
                createdAt
            }
            tags
            id
            name
            disabled
            createdBy
        }
    `, fragmentRef);

    return (
        <TableRow sx={{ '&:last-child td, &:last-child th': { border: 0 } }}>
            <TableCell>
                <Link color="inherit" to={data.id} sx={{ mr: 1 }}>{data.name}</Link>
                <AgentChip disabled={data.disabled} />
            </TableCell>
            <TableCell>
                {data.tags.length > 0 && <Stack direction="row" spacing={1}>
                    {data.tags.map(tag => <Chip key={tag} size="small" color="secondary" label={tag} />)}
                </Stack>}
                {data.tags.length === 0 && <Typography variant="body2" color="textSecondary">None</Typography>}
            </TableCell>
            <TableCell>
                <Tooltip title={data.createdBy}>
                    <Box display="flex" alignItems="center">
                        <Gravatar width={24} height={24} email={data.createdBy} />
                        <Box ml={1}>
                            {moment(data.metadata.createdAt as moment.MomentInput).fromNow()}
                        </Box>
                    </Box>
                </Tooltip>
            </TableCell>
        </TableRow >
    );
}

export default AgentListItem
