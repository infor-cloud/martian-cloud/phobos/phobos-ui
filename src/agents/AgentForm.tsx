import { Alert, Box, Button, Checkbox, Chip, Divider, FormControlLabel, Paper, Stack, Switch, TextField, Typography } from '@mui/material';
import { MutationError } from '../common/error';
import { useState } from 'react';

export interface FormData {
    name: string
    description: string
    disabled: boolean
    tags: string[]
    runUntaggedJobs: boolean
}

interface Props {
    data: FormData
    onChange: (data: FormData) => void
    editMode?: boolean
    error?: MutationError
}

function AgentForm({ data, onChange, editMode, error }: Props) {
    const [tagToAdd, setTagToAdd] = useState('');

    const onAddTag = () => {
        onChange({ ...data, tags: [...data.tags, tagToAdd] });
        setTagToAdd('');
    };

    const onRemoveTag = (tag: string) => {
        const index = data.tags.indexOf(tag);
        if (index !== -1) {
            const tagsCopy = [...data.tags];
            tagsCopy.splice(index, 1);
            onChange({ ...data, tags: tagsCopy });
        }
    };

    return (
        <Box>
            {error && <Alert sx={{ mt: 2 }} severity={error.severity}>
                {error.message}
            </Alert>}
            <Box marginBottom={3}>
                <Typography sx={{ mt: 2 }} variant="subtitle1" gutterBottom>Details</Typography>
                <Divider sx={{ mb: 2, opacity: 0.6 }} />
                <Box>
                    <TextField
                        disabled={editMode}
                        size="small"
                        fullWidth
                        label="Name"
                        value={data.name}
                        onChange={event => onChange({ ...data, name: event.target.value })}
                    />
                    <TextField
                        size="small"
                        margin='normal'
                        fullWidth
                        label="Description"
                        value={data.description}
                        onChange={event => onChange({ ...data, description: event.target.value })}
                    />
                </Box>
            </Box>
            <Box marginBottom={3}>
                <Typography sx={{ mt: 2 }} variant="subtitle1" gutterBottom>Tags</Typography>
                <Divider sx={{ mb: 2, opacity: 0.6 }} />
                <Paper sx={{ padding: 2, mb: 2 }} variant="outlined">
                    <Typography gutterBottom color="textSecondary">
                        Add tags to target specific jobs. An agent must have all tags specified for a job in order to claim it.
                    </Typography>
                    <Paper sx={{ padding: 2, display: 'flex', alignItems: 'center', mb: 2 }}>
                        <TextField
                            size="small"
                            margin="none"
                            sx={{ flex: 1, mr: 1 }}
                            fullWidth
                            value={tagToAdd}
                            placeholder="Enter tag to add"
                            variant="standard"
                            color="secondary"
                            onChange={event => setTagToAdd(event.target.value)}
                        />
                        <Button
                            onClick={onAddTag}
                            disabled={tagToAdd === ''}
                            variant="outlined"
                            color="secondary">
                            Add Tag
                        </Button>
                    </Paper>
                    <Stack direction="row" spacing={2}>
                        {data.tags.map(tag => <Chip key={tag} color="secondary" label={tag} onDelete={() => onRemoveTag(tag)} />)}
                    </Stack>
                </Paper>
                <FormControlLabel
                    control={<Checkbox
                        color="secondary"
                        checked={data.runUntaggedJobs}
                        onChange={event => onChange({ ...data, runUntaggedJobs: event.target.checked })}
                    />}
                    label="Run Untagged Jobs"
                />
            </Box>
            <Box sx={{ mb: 3 }}>
                <Typography sx={{ mt: 2 }} variant="subtitle1" gutterBottom>Status</Typography>
                <Divider sx={{ mb: 2, opacity: 0.6 }} />
                <Typography color="textSecondary">
                    An agent will not claim any jobs when it's disabled.
                </Typography>
                <FormControlLabel
                    control={<Switch
                        sx={{ m: 2 }}
                        checked={!data.disabled}
                        color="secondary"
                        onChange={event => onChange({ ...data, disabled: !event.target.checked })}
                    />}
                    label={data.disabled ? "Disabled" : "Enabled"}
                />
            </Box>
        </Box>
    );
}

export default AgentForm
