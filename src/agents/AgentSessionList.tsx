import { Box, List, Paper, Typography, useTheme } from '@mui/material';
import graphql from 'babel-plugin-relay/macro';
import { useState } from 'react';
import InfiniteScroll from 'react-infinite-scroll-component';
import { ConnectionHandler, useLazyLoadQuery, usePaginationFragment } from 'react-relay/hooks';
import { useParams } from 'react-router-dom';
import ListSkeleton from '../skeletons/ListSkeleton';
import AgentSessionErrorLogDialog from './AgentSessionErrorLogDialog';
import AgentSessionListItem from './AgentSessionListItem';
import { AgentSessionListFragment_sessions$key } from './__generated__/AgentSessionListFragment_sessions.graphql';
import { AgentSessionListPaginationQuery } from './__generated__/AgentSessionListPaginationQuery.graphql';
import { AgentSessionListQuery } from './__generated__/AgentSessionListQuery.graphql';

export function GetConnections(agentId: string): [string] {
    const connectionId = ConnectionHandler.getConnectionID(
        agentId,
        "AgentSessionList_sessions",
        { sort: "LAST_CONTACTED_AT_DESC" }
    );
    return [connectionId];
}

const query = graphql`
    query AgentSessionListQuery($id: String!, $first: Int!, $after: String) {
        node(id: $id) {
            ... on Agent {
                id
                ...AgentSessionListFragment_sessions
            }
        }
    }`

function AgentSessionList() {
    const theme = useTheme();
    const agentId = useParams().agentId as string;

    const [selectedSession, setSelectedSession] = useState(null);

    const queryData = useLazyLoadQuery<AgentSessionListQuery>(query, { id: agentId, first: 20 }, { fetchPolicy: 'store-and-network' });

    const { data, loadNext, hasNext } = usePaginationFragment<AgentSessionListPaginationQuery, AgentSessionListFragment_sessions$key>(
        graphql`
        fragment AgentSessionListFragment_sessions on Agent
        @refetchable(queryName: "AgentSessionListPaginationQuery") {
                type
                sessions(
                    first: $first
                    after: $after
                    sort: LAST_CONTACTED_AT_DESC
                ) @connection(key: "AgentSessionList_sessions") {
                    totalCount
                    edges {
                        node {
                            id
                            ...AgentSessionListItemFragment
                        }
                    }
                }
            }
        `, queryData.node
    );

    return (
        <Box sx={{ border: 1, borderTop: 0, borderBottomLeftRadius: 4, borderBottomRightRadius: 4, borderColor: 'divider' }}>
            <Box sx={{
                display: 'flex',
                flexDirection: 'row',
                justifyContent: 'space-between',
                alignItems: 'flex-start',
                p: 2,
                pb: 0,
                [theme.breakpoints.down('lg')]: {
                    flexDirection: 'column',
                    alignItems: 'flex-start',
                    '& > *': { mb: 2 }
                }
            }}>
                <Typography color="textSecondary">Agent sessions are individual connections from this agent</Typography>
            </Box>
            {(!data?.sessions?.edges || data?.sessions?.edges?.length === 0) ? (
                <Paper sx={{ p: 2, m: 2 }}>
                    <Typography>This agent does not have any sessions.</Typography>
                </Paper>
            )
                :
                (
                    <Box sx={{ p: 2 }}>
                        <Paper sx={{ borderBottomLeftRadius: 0, borderBottomRightRadius: 0, border: `1px solid ${theme.palette.divider}` }}>
                            <Box padding={2}>
                                <Typography variant="subtitle1">
                                    {data?.sessions.totalCount} session{data?.sessions.totalCount !== 1 && 's'}
                                </Typography>
                            </Box>
                        </Paper>
                        <InfiniteScroll
                            dataLength={(data?.sessions?.edges && data?.sessions?.edges.length) ?? 0}
                            next={() => loadNext(20)}
                            hasMore={hasNext}
                            loader={<ListSkeleton rowCount={3} />}
                        >
                            <List disablePadding>{data?.sessions?.edges?.map((edge: any) => <AgentSessionListItem
                                key={edge.node.id}
                                fragmentRef={edge.node}
                                onClick={() => setSelectedSession(edge.node.id)}
                            />)}
                            </List>
                        </InfiniteScroll>
                    </Box>
                )}
            {selectedSession && <AgentSessionErrorLogDialog sessionId={selectedSession} onClose={() => setSelectedSession(null)} />}
        </Box>
    );
}

export default AgentSessionList
