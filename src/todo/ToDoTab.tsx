import DoneAllIcon from '@mui/icons-material/DoneAll';
import { Box, Paper, Typography, useTheme } from '@mui/material';
import graphql from 'babel-plugin-relay/macro';
import React, { useEffect, useState } from 'react';
import InfiniteScroll from 'react-infinite-scroll-component';
import { ConnectionHandler, usePaginationFragment } from 'react-relay/hooks';
import ListSkeleton from '../skeletons/ListSkeleton';
import ToDoList from './ToDoList';
import { ToDoTabFragment_todoItems$key } from './__generated__/ToDoTabFragment_todoItems.graphql';
import { ToDoTabPaginationQuery } from './__generated__/ToDoTabPaginationQuery.graphql';

export const INITIAL_ITEM_COUNT = 20;

function GetConnections(userId: string, resolved: boolean): [string] {
    const connectionId = ConnectionHandler.getConnectionID(
        userId,
        'ToDoTab_todoItems',
        { resolved: resolved, sort: "UPDATED_AT_DESC" }
    );
    return [connectionId];
}

interface Props {
    fragmentRef: ToDoTabFragment_todoItems$key;
    resolved: boolean;
}

function ToDoTab({ fragmentRef, resolved }: Props) {
    const theme = useTheme();
    const [resolvedValue, setResolvedValue] = useState(resolved);

    const { data, loadNext, hasNext, refetch } = usePaginationFragment<ToDoTabPaginationQuery, ToDoTabFragment_todoItems$key>(
        graphql`
            fragment ToDoTabFragment_todoItems on User
            @refetchable(queryName: "ToDoTabPaginationQuery") {
                id
                todoItems(
                    first: $first
                    after: $after
                    resolved: $resolved
                    sort: UPDATED_AT_DESC
                ) @connection(key: "ToDoTab_todoItems") {
                    totalCount
                    edges {
                        node {
                            id
                        }
                    }
                    ...ToDoListFragment_connection
                }
            }
        `, fragmentRef,
    );

    const userId = data.id as string;

    useEffect(() => {
        if (resolved === resolvedValue) {
            return;
        }
        refetch({ resolved: resolved }, { fetchPolicy: 'store-and-network' });
        setResolvedValue(resolved);
    }, [resolved]);

    const itemsCount = data?.todoItems?.totalCount || 0;
    const text = resolvedValue ? "Completed items will show up here." : "When something requires your attention it'll show up here.";

    return resolved === resolvedValue ? (
        <React.Fragment>
            {itemsCount === 0 ? <Box sx={{ mt: 4 }} display="flex" justifyContent="center">
                <Box padding={4} display="flex" flexDirection="column" justifyContent="center" alignItems="center" sx={{ maxWidth: 600 }}>
                    <DoneAllIcon color="primary" sx={{ height: 150, width: 150 }} />
                    <Typography variant="h6">All set!</Typography>
                    <Typography color="textSecondary" align="center" sx={{ mb: 2 }}>{text}</Typography>
                </Box>
            </Box> : <Box>
                <Paper sx={{ borderBottomLeftRadius: 0, borderBottomRightRadius: 0, border: `1px solid ${theme.palette.divider}` }}>
                    <Box padding={2} display="flex" alignItems="center" justifyContent="space-between">
                        <Typography variant="subtitle1">
                            {itemsCount} item{itemsCount === 1 ? '' : 's'}
                        </Typography>
                    </Box>
                </Paper>
                <InfiniteScroll
                    dataLength={data?.todoItems?.edges?.length ?? 0}
                    next={() => loadNext(INITIAL_ITEM_COUNT)}
                    hasMore={hasNext}
                    loader={<ListSkeleton rowCount={3} />}
                >
                    <ToDoList fragmentRef={data.todoItems} userId={userId} getConnections={GetConnections} />
                </InfiniteScroll>
            </Box>}
        </React.Fragment>
    ) : null;
}

export default ToDoTab;
