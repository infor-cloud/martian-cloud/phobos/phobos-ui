import React from 'react';
import { useFragment } from 'react-relay';
import { ToDoItemPipelineApprovalTargetFragment_todoItem$key } from './__generated__/ToDoItemPipelineApprovalTargetFragment_todoItem.graphql';
import graphql from 'babel-plugin-relay/macro';
import ToDoItemListItem from '../ToDoListItem';
import { ApprovalIcon } from '../../common/Icons';

interface Props {
    fragmentRef: ToDoItemPipelineApprovalTargetFragment_todoItem$key;
    userId: string;
    getConnections: (userId: string, resolved: boolean) => [string];
}

function ToDoItemPipelineApprovalTarget({ fragmentRef, userId, getConnections }: Props) {
    const data = useFragment<ToDoItemPipelineApprovalTargetFragment_todoItem$key>(
        graphql`
            fragment ToDoItemPipelineApprovalTargetFragment_todoItem on ToDoItem {
                target {
                    ... on Pipeline {
                        id
                        project {
                            name
                            organizationName
                        }
                    }
                }
                ...ToDoListItemFragment_todoItem
            }
    `, fragmentRef);

    const pipeline = data.target as any;

    return (
        <ToDoItemListItem
            fragmentRef={data}
            organizationName={pipeline.project.organizationName}
            projectName={pipeline.project.name}
            userId={userId}
            to={`/organizations/${pipeline.project.organizationName}/projects/${pipeline.project.name}/-/pipelines/${pipeline.id}`}
            icon={<ApprovalIcon />}
            primary={<React.Fragment>Review requested for pipeline</React.Fragment>}
            getConnections={getConnections}
        />
    );
}

export default ToDoItemPipelineApprovalTarget;
