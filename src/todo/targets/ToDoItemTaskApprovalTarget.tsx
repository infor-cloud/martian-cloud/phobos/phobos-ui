import React from 'react';
import { useFragment } from 'react-relay';
import { ToDoItemTaskApprovalTargetFragment_todoItem$key } from './__generated__/ToDoItemTaskApprovalTargetFragment_todoItem.graphql';
import graphql from 'babel-plugin-relay/macro';
import ToDoItemListItem from '../ToDoListItem';
import { ApprovalIcon } from '../../common/Icons';

interface Props {
    fragmentRef: ToDoItemTaskApprovalTargetFragment_todoItem$key;
    userId: string;
    getConnections: (userId: string, resolved: boolean) => [string];
}

function ToDoItemTaskApprovalTarget({ fragmentRef, userId, getConnections }: Props) {
    const data = useFragment<ToDoItemTaskApprovalTargetFragment_todoItem$key>(
        graphql`
            fragment ToDoItemTaskApprovalTargetFragment_todoItem on ToDoItem {
                target {
                    ... on Pipeline {
                        id
                        project {
                            name
                            organizationName
                        }
                    }
                }
                payload {
                    ... on ToDoItemPipelineTaskApprovalPayload {
                        taskPath
                    }
                }
                ...ToDoListItemFragment_todoItem
            }
    `, fragmentRef);

    const pipeline = data.target as any;
    const taskName = data.payload?.taskPath?.split(".").pop();

    return (
        <ToDoItemListItem
            fragmentRef={data}
            organizationName={pipeline.project.organizationName}
            projectName={pipeline.project.name}
            userId={userId}
            to={`/organizations/${pipeline.project.organizationName}/projects/${pipeline.project.name}/-/pipelines/${pipeline.id}/task/${data.payload?.taskPath}`}
            icon={<ApprovalIcon />}
            primary={<React.Fragment>Review requested for <strong>{taskName}</strong></React.Fragment>}
            getConnections={getConnections}
        />
    );
}

export default ToDoItemTaskApprovalTarget;
