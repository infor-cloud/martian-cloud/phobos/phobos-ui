/**
 * @generated SignedSource<<a73c7b263dd2a3f7118358e080e71ae5>>
 * @lightSyntaxTransform
 * @nogrep
 */

/* tslint:disable */
/* eslint-disable */
// @ts-nocheck

import { Fragment, ReaderFragment } from 'relay-runtime';
import { FragmentRefs } from "relay-runtime";
export type ToDoItemTaskApprovalTargetFragment_todoItem$data = {
  readonly payload: {
    readonly taskPath?: string;
  } | null | undefined;
  readonly target: {
    readonly id?: string;
    readonly project?: {
      readonly name: string;
      readonly organizationName: string;
    };
  } | null | undefined;
  readonly " $fragmentSpreads": FragmentRefs<"ToDoListItemFragment_todoItem">;
  readonly " $fragmentType": "ToDoItemTaskApprovalTargetFragment_todoItem";
};
export type ToDoItemTaskApprovalTargetFragment_todoItem$key = {
  readonly " $data"?: ToDoItemTaskApprovalTargetFragment_todoItem$data;
  readonly " $fragmentSpreads": FragmentRefs<"ToDoItemTaskApprovalTargetFragment_todoItem">;
};

const node: ReaderFragment = {
  "argumentDefinitions": [],
  "kind": "Fragment",
  "metadata": null,
  "name": "ToDoItemTaskApprovalTargetFragment_todoItem",
  "selections": [
    {
      "alias": null,
      "args": null,
      "concreteType": null,
      "kind": "LinkedField",
      "name": "target",
      "plural": false,
      "selections": [
        {
          "kind": "InlineFragment",
          "selections": [
            {
              "alias": null,
              "args": null,
              "kind": "ScalarField",
              "name": "id",
              "storageKey": null
            },
            {
              "alias": null,
              "args": null,
              "concreteType": "Project",
              "kind": "LinkedField",
              "name": "project",
              "plural": false,
              "selections": [
                {
                  "alias": null,
                  "args": null,
                  "kind": "ScalarField",
                  "name": "name",
                  "storageKey": null
                },
                {
                  "alias": null,
                  "args": null,
                  "kind": "ScalarField",
                  "name": "organizationName",
                  "storageKey": null
                }
              ],
              "storageKey": null
            }
          ],
          "type": "Pipeline",
          "abstractKey": null
        }
      ],
      "storageKey": null
    },
    {
      "alias": null,
      "args": null,
      "concreteType": null,
      "kind": "LinkedField",
      "name": "payload",
      "plural": false,
      "selections": [
        {
          "kind": "InlineFragment",
          "selections": [
            {
              "alias": null,
              "args": null,
              "kind": "ScalarField",
              "name": "taskPath",
              "storageKey": null
            }
          ],
          "type": "ToDoItemPipelineTaskApprovalPayload",
          "abstractKey": null
        }
      ],
      "storageKey": null
    },
    {
      "args": null,
      "kind": "FragmentSpread",
      "name": "ToDoListItemFragment_todoItem"
    }
  ],
  "type": "ToDoItem",
  "abstractKey": null
};

(node as any).hash = "f68f5ad7784e82cba4a5d27740bb1131";

export default node;
