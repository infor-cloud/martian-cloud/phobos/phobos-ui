/**
 * @generated SignedSource<<e5bdfdd3d5b38714cef7e2129b1cb612>>
 * @lightSyntaxTransform
 * @nogrep
 */

/* tslint:disable */
/* eslint-disable */
// @ts-nocheck

import { Fragment, ReaderFragment } from 'relay-runtime';
import { FragmentRefs } from "relay-runtime";
export type ToDoItemPipelineApprovalTargetFragment_todoItem$data = {
  readonly target: {
    readonly id?: string;
    readonly project?: {
      readonly name: string;
      readonly organizationName: string;
    };
  } | null | undefined;
  readonly " $fragmentSpreads": FragmentRefs<"ToDoListItemFragment_todoItem">;
  readonly " $fragmentType": "ToDoItemPipelineApprovalTargetFragment_todoItem";
};
export type ToDoItemPipelineApprovalTargetFragment_todoItem$key = {
  readonly " $data"?: ToDoItemPipelineApprovalTargetFragment_todoItem$data;
  readonly " $fragmentSpreads": FragmentRefs<"ToDoItemPipelineApprovalTargetFragment_todoItem">;
};

const node: ReaderFragment = {
  "argumentDefinitions": [],
  "kind": "Fragment",
  "metadata": null,
  "name": "ToDoItemPipelineApprovalTargetFragment_todoItem",
  "selections": [
    {
      "alias": null,
      "args": null,
      "concreteType": null,
      "kind": "LinkedField",
      "name": "target",
      "plural": false,
      "selections": [
        {
          "kind": "InlineFragment",
          "selections": [
            {
              "alias": null,
              "args": null,
              "kind": "ScalarField",
              "name": "id",
              "storageKey": null
            },
            {
              "alias": null,
              "args": null,
              "concreteType": "Project",
              "kind": "LinkedField",
              "name": "project",
              "plural": false,
              "selections": [
                {
                  "alias": null,
                  "args": null,
                  "kind": "ScalarField",
                  "name": "name",
                  "storageKey": null
                },
                {
                  "alias": null,
                  "args": null,
                  "kind": "ScalarField",
                  "name": "organizationName",
                  "storageKey": null
                }
              ],
              "storageKey": null
            }
          ],
          "type": "Pipeline",
          "abstractKey": null
        }
      ],
      "storageKey": null
    },
    {
      "args": null,
      "kind": "FragmentSpread",
      "name": "ToDoListItemFragment_todoItem"
    }
  ],
  "type": "ToDoItem",
  "abstractKey": null
};

(node as any).hash = "10a28afbdafdc491ce6eb2e659d11582";

export default node;
