/**
 * @generated SignedSource<<94aca639a25e13e1d64982cfa3ad14e0>>
 * @lightSyntaxTransform
 * @nogrep
 */

/* tslint:disable */
/* eslint-disable */
// @ts-nocheck

import { Fragment, ReaderFragment } from 'relay-runtime';
export type ToDoItemTargetType = "PIPELINE_APPROVAL" | "TASK_APPROVAL" | "%future added value";
import { FragmentRefs } from "relay-runtime";
export type ToDoListFragment_connection$data = {
  readonly edges: ReadonlyArray<{
    readonly node: {
      readonly id: string;
      readonly resolved: boolean;
      readonly targetType: ToDoItemTargetType;
      readonly " $fragmentSpreads": FragmentRefs<"ToDoItemPipelineApprovalTargetFragment_todoItem" | "ToDoItemTaskApprovalTargetFragment_todoItem">;
    } | null | undefined;
  } | null | undefined> | null | undefined;
  readonly " $fragmentType": "ToDoListFragment_connection";
};
export type ToDoListFragment_connection$key = {
  readonly " $data"?: ToDoListFragment_connection$data;
  readonly " $fragmentSpreads": FragmentRefs<"ToDoListFragment_connection">;
};

const node: ReaderFragment = {
  "argumentDefinitions": [],
  "kind": "Fragment",
  "metadata": null,
  "name": "ToDoListFragment_connection",
  "selections": [
    {
      "alias": null,
      "args": null,
      "concreteType": "ToDoItemEdge",
      "kind": "LinkedField",
      "name": "edges",
      "plural": true,
      "selections": [
        {
          "alias": null,
          "args": null,
          "concreteType": "ToDoItem",
          "kind": "LinkedField",
          "name": "node",
          "plural": false,
          "selections": [
            {
              "alias": null,
              "args": null,
              "kind": "ScalarField",
              "name": "id",
              "storageKey": null
            },
            {
              "alias": null,
              "args": null,
              "kind": "ScalarField",
              "name": "resolved",
              "storageKey": null
            },
            {
              "alias": null,
              "args": null,
              "kind": "ScalarField",
              "name": "targetType",
              "storageKey": null
            },
            {
              "args": null,
              "kind": "FragmentSpread",
              "name": "ToDoItemPipelineApprovalTargetFragment_todoItem"
            },
            {
              "args": null,
              "kind": "FragmentSpread",
              "name": "ToDoItemTaskApprovalTargetFragment_todoItem"
            }
          ],
          "storageKey": null
        }
      ],
      "storageKey": null
    }
  ],
  "type": "ToDoItemConnection",
  "abstractKey": null
};

(node as any).hash = "0dafcc793dacf438b28e7d43296916b2";

export default node;
