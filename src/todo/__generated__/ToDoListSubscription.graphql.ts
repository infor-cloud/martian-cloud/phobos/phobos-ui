/**
 * @generated SignedSource<<65d5d2b11f884a2cde8a635eea69ecd8>>
 * @lightSyntaxTransform
 * @nogrep
 */

/* tslint:disable */
/* eslint-disable */
// @ts-nocheck

import { ConcreteRequest, GraphQLSubscription } from 'relay-runtime';
import { FragmentRefs } from "relay-runtime";
export type ToDoItemTargetType = "PIPELINE_APPROVAL" | "TASK_APPROVAL" | "%future added value";
export type ToDoListSubscription$variables = Record<PropertyKey, never>;
export type ToDoListSubscription$data = {
  readonly userToDoItemEvents: {
    readonly action: string;
    readonly todoItem: {
      readonly id: string;
      readonly resolved: boolean;
      readonly targetType: ToDoItemTargetType;
      readonly " $fragmentSpreads": FragmentRefs<"ToDoItemPipelineApprovalTargetFragment_todoItem" | "ToDoItemTaskApprovalTargetFragment_todoItem">;
    };
  };
};
export type ToDoListSubscription = {
  response: ToDoListSubscription$data;
  variables: ToDoListSubscription$variables;
};

const node: ConcreteRequest = (function(){
var v0 = {
  "alias": null,
  "args": null,
  "kind": "ScalarField",
  "name": "action",
  "storageKey": null
},
v1 = {
  "alias": null,
  "args": null,
  "kind": "ScalarField",
  "name": "id",
  "storageKey": null
},
v2 = {
  "alias": null,
  "args": null,
  "kind": "ScalarField",
  "name": "resolved",
  "storageKey": null
},
v3 = {
  "alias": null,
  "args": null,
  "kind": "ScalarField",
  "name": "targetType",
  "storageKey": null
},
v4 = {
  "alias": null,
  "args": null,
  "kind": "ScalarField",
  "name": "__typename",
  "storageKey": null
};
return {
  "fragment": {
    "argumentDefinitions": [],
    "kind": "Fragment",
    "metadata": null,
    "name": "ToDoListSubscription",
    "selections": [
      {
        "alias": null,
        "args": null,
        "concreteType": "ToDoItemEvent",
        "kind": "LinkedField",
        "name": "userToDoItemEvents",
        "plural": false,
        "selections": [
          (v0/*: any*/),
          {
            "alias": null,
            "args": null,
            "concreteType": "ToDoItem",
            "kind": "LinkedField",
            "name": "todoItem",
            "plural": false,
            "selections": [
              (v1/*: any*/),
              (v2/*: any*/),
              (v3/*: any*/),
              {
                "args": null,
                "kind": "FragmentSpread",
                "name": "ToDoItemPipelineApprovalTargetFragment_todoItem"
              },
              {
                "args": null,
                "kind": "FragmentSpread",
                "name": "ToDoItemTaskApprovalTargetFragment_todoItem"
              }
            ],
            "storageKey": null
          }
        ],
        "storageKey": null
      }
    ],
    "type": "Subscription",
    "abstractKey": null
  },
  "kind": "Request",
  "operation": {
    "argumentDefinitions": [],
    "kind": "Operation",
    "name": "ToDoListSubscription",
    "selections": [
      {
        "alias": null,
        "args": null,
        "concreteType": "ToDoItemEvent",
        "kind": "LinkedField",
        "name": "userToDoItemEvents",
        "plural": false,
        "selections": [
          (v0/*: any*/),
          {
            "alias": null,
            "args": null,
            "concreteType": "ToDoItem",
            "kind": "LinkedField",
            "name": "todoItem",
            "plural": false,
            "selections": [
              (v1/*: any*/),
              (v2/*: any*/),
              (v3/*: any*/),
              {
                "alias": null,
                "args": null,
                "concreteType": null,
                "kind": "LinkedField",
                "name": "target",
                "plural": false,
                "selections": [
                  (v4/*: any*/),
                  (v1/*: any*/),
                  {
                    "kind": "InlineFragment",
                    "selections": [
                      {
                        "alias": null,
                        "args": null,
                        "concreteType": "Project",
                        "kind": "LinkedField",
                        "name": "project",
                        "plural": false,
                        "selections": [
                          {
                            "alias": null,
                            "args": null,
                            "kind": "ScalarField",
                            "name": "name",
                            "storageKey": null
                          },
                          {
                            "alias": null,
                            "args": null,
                            "kind": "ScalarField",
                            "name": "organizationName",
                            "storageKey": null
                          },
                          (v1/*: any*/)
                        ],
                        "storageKey": null
                      }
                    ],
                    "type": "Pipeline",
                    "abstractKey": null
                  }
                ],
                "storageKey": null
              },
              {
                "alias": null,
                "args": null,
                "kind": "ScalarField",
                "name": "resolvable",
                "storageKey": null
              },
              {
                "alias": null,
                "args": null,
                "concreteType": "ResourceMetadata",
                "kind": "LinkedField",
                "name": "metadata",
                "plural": false,
                "selections": [
                  {
                    "alias": null,
                    "args": null,
                    "kind": "ScalarField",
                    "name": "createdAt",
                    "storageKey": null
                  }
                ],
                "storageKey": null
              },
              {
                "alias": null,
                "args": null,
                "concreteType": null,
                "kind": "LinkedField",
                "name": "payload",
                "plural": false,
                "selections": [
                  (v4/*: any*/),
                  {
                    "kind": "InlineFragment",
                    "selections": [
                      {
                        "alias": null,
                        "args": null,
                        "kind": "ScalarField",
                        "name": "taskPath",
                        "storageKey": null
                      }
                    ],
                    "type": "ToDoItemPipelineTaskApprovalPayload",
                    "abstractKey": null
                  }
                ],
                "storageKey": null
              }
            ],
            "storageKey": null
          }
        ],
        "storageKey": null
      }
    ]
  },
  "params": {
    "cacheID": "5a37fcb2151bfa02d7d4ed178273918d",
    "id": null,
    "metadata": {},
    "name": "ToDoListSubscription",
    "operationKind": "subscription",
    "text": "subscription ToDoListSubscription {\n  userToDoItemEvents {\n    action\n    todoItem {\n      id\n      resolved\n      targetType\n      ...ToDoItemPipelineApprovalTargetFragment_todoItem\n      ...ToDoItemTaskApprovalTargetFragment_todoItem\n    }\n  }\n}\n\nfragment ToDoItemPipelineApprovalTargetFragment_todoItem on ToDoItem {\n  target {\n    __typename\n    ... on Pipeline {\n      id\n      project {\n        name\n        organizationName\n        id\n      }\n    }\n    id\n  }\n  ...ToDoListItemFragment_todoItem\n}\n\nfragment ToDoItemTaskApprovalTargetFragment_todoItem on ToDoItem {\n  target {\n    __typename\n    ... on Pipeline {\n      id\n      project {\n        name\n        organizationName\n        id\n      }\n    }\n    id\n  }\n  payload {\n    __typename\n    ... on ToDoItemPipelineTaskApprovalPayload {\n      taskPath\n    }\n  }\n  ...ToDoListItemFragment_todoItem\n}\n\nfragment ToDoListItemFragment_todoItem on ToDoItem {\n  id\n  resolvable\n  resolved\n  metadata {\n    createdAt\n  }\n}\n"
  }
};
})();

(node as any).hash = "a39a9c241b29ce7e5f055173d7ecc990";

export default node;
