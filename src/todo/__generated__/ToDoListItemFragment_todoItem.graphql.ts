/**
 * @generated SignedSource<<417b257eb39511d34a16e2f5fde23b08>>
 * @lightSyntaxTransform
 * @nogrep
 */

/* tslint:disable */
/* eslint-disable */
// @ts-nocheck

import { Fragment, ReaderFragment } from 'relay-runtime';
import { FragmentRefs } from "relay-runtime";
export type ToDoListItemFragment_todoItem$data = {
  readonly id: string;
  readonly metadata: {
    readonly createdAt: any;
  };
  readonly resolvable: boolean;
  readonly resolved: boolean;
  readonly " $fragmentType": "ToDoListItemFragment_todoItem";
};
export type ToDoListItemFragment_todoItem$key = {
  readonly " $data"?: ToDoListItemFragment_todoItem$data;
  readonly " $fragmentSpreads": FragmentRefs<"ToDoListItemFragment_todoItem">;
};

const node: ReaderFragment = {
  "argumentDefinitions": [],
  "kind": "Fragment",
  "metadata": null,
  "name": "ToDoListItemFragment_todoItem",
  "selections": [
    {
      "alias": null,
      "args": null,
      "kind": "ScalarField",
      "name": "id",
      "storageKey": null
    },
    {
      "alias": null,
      "args": null,
      "kind": "ScalarField",
      "name": "resolvable",
      "storageKey": null
    },
    {
      "alias": null,
      "args": null,
      "kind": "ScalarField",
      "name": "resolved",
      "storageKey": null
    },
    {
      "alias": null,
      "args": null,
      "concreteType": "ResourceMetadata",
      "kind": "LinkedField",
      "name": "metadata",
      "plural": false,
      "selections": [
        {
          "alias": null,
          "args": null,
          "kind": "ScalarField",
          "name": "createdAt",
          "storageKey": null
        }
      ],
      "storageKey": null
    }
  ],
  "type": "ToDoItem",
  "abstractKey": null
};

(node as any).hash = "5cff7ac7ec8288372a1f13723a2a032c";

export default node;
