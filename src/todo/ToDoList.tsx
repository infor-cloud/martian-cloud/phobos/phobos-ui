import { List } from '@mui/material';
import graphql from 'babel-plugin-relay/macro';
import { useMemo } from 'react';
import { ConnectionHandler, useFragment, useSubscription } from 'react-relay/hooks';
import { ConnectionInterface, GraphQLSubscriptionConfig, RecordSourceProxy } from 'relay-runtime';
import { ToDoListFragment_connection$key } from './__generated__/ToDoListFragment_connection.graphql';
import { ToDoListSubscription, ToDoListSubscription$data } from './__generated__/ToDoListSubscription.graphql';
import ToDoItemPipelineApprovalTarget from './targets/ToDoItemPipelineApprovalTarget';
import ToDoItemTaskApprovalTarget from './targets/ToDoItemTaskApprovalTarget';

const TARGET_COMPONENT_MAP = {
    PIPELINE_APPROVAL: ToDoItemPipelineApprovalTarget,
    TASK_APPROVAL: ToDoItemTaskApprovalTarget,
} as any;

const todoListSubscription = graphql`
    subscription ToDoListSubscription {
        userToDoItemEvents {
            action
            todoItem {
                id
                resolved
                targetType
                ...ToDoItemPipelineApprovalTargetFragment_todoItem
                ...ToDoItemTaskApprovalTargetFragment_todoItem
            }
        }
    }`


interface Props {
    fragmentRef: ToDoListFragment_connection$key;
    userId: string;
    dense?: boolean;
    getConnections: (userId: string, resolved: boolean) => string[];
}

function ToDoList({ fragmentRef, userId, dense, getConnections }: Props) {
    const data = useFragment<ToDoListFragment_connection$key>(graphql`
        fragment ToDoListFragment_connection on ToDoItemConnection {
            edges {
                node {
                    id
                    resolved
                    targetType
                    ...ToDoItemPipelineApprovalTargetFragment_todoItem
                    ...ToDoItemTaskApprovalTargetFragment_todoItem
                }
            }
        }
    `, fragmentRef);

    const todoListSubscriptionConfig = useMemo<GraphQLSubscriptionConfig<ToDoListSubscription>>(() => ({
        variables: {},
        subscription: todoListSubscription,
        onCompleted: () => console.log('Subscription completed'),
        onError: () => console.warn('Subscription error'),
        updater: (store: RecordSourceProxy, payload: ToDoListSubscription$data) => {
            const record = store.get(payload.userToDoItemEvents.todoItem.id)
            if (record == null) {
                return;
            }
            // Create edges regardless of an update or insert.
            getConnections(userId, payload.userToDoItemEvents.todoItem.resolved).forEach(id => {
                const connectionRecord = store.get(id);
                if (connectionRecord) {
                    const { NODE, EDGES } = ConnectionInterface.get();
                    const recordId = record.getDataID();

                    // Check if edge already exists in connection
                    const nodeAlreadyExistsInConnection = connectionRecord
                        .getLinkedRecords(EDGES)
                        ?.some(
                            edge => edge?.getLinkedRecord(NODE)?.getDataID() === recordId
                        );
                    if (!nodeAlreadyExistsInConnection) {
                        const totalCount = connectionRecord.getValue('totalCount') as number;
                        connectionRecord.setValue(totalCount + 1, 'totalCount');

                        // Create Edge
                        const edge = ConnectionHandler.createEdge(
                            store,
                            connectionRecord,
                            record,
                            'ToDoItemEdge'
                        );
                        if (edge) {
                            // Add edge to the beginning of the connection
                            ConnectionHandler.insertEdgeBefore(
                                connectionRecord,
                                edge,
                            );
                        }
                    }
                }
            });

            // Remove edges from the current list only when they're updated (marked as resolved or vice-versa).
            if (payload.userToDoItemEvents.action === 'UPDATE') {
                getConnections(userId, !payload.userToDoItemEvents.todoItem.resolved).forEach(id => {
                    const connectionRecord = store.get(id);
                    if (connectionRecord) {
                        const { NODE, EDGES } = ConnectionInterface.get();
                        const recordId = record.getDataID();

                        // Check if node exists in connection
                        const nodeExistsInConnection = connectionRecord
                            .getLinkedRecords(EDGES)
                            ?.some(
                                edge => edge?.getLinkedRecord(NODE)?.getDataID() === recordId
                            );
                        if (nodeExistsInConnection) {
                            const totalCount = connectionRecord.getValue('totalCount') as number;
                            connectionRecord.setValue(totalCount - 1, 'totalCount');
                            // Remove node
                            ConnectionHandler.deleteNode(connectionRecord, recordId);
                        }
                    }
                });
            }
        }
    }), [userId]);

    useSubscription<ToDoListSubscription>(todoListSubscriptionConfig);

    return (
        <List sx={{ paddingTop: 0 }} dense={dense}>
            {data?.edges?.map((edge: any) => {
                const Target = TARGET_COMPONENT_MAP[edge.node.targetType];
                return Target ? <Target key={edge.node.id} fragmentRef={edge.node} userId={userId} getConnections={getConnections} /> : null;
            })}
        </List>
    );
}

export default ToDoList;
