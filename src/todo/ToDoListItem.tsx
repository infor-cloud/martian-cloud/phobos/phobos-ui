import DoneIcon from '@mui/icons-material/Done';
import PlaylistAddIcon from '@mui/icons-material/PlaylistAdd';
import LoadingButton from '@mui/lab/LoadingButton';
import { Avatar, Box, ListItemAvatar, ListItemButton, ListItemSecondaryAction, Tooltip, Typography, useTheme } from "@mui/material";
import graphql from "babel-plugin-relay/macro";
import { useSnackbar } from "notistack";
import { useFragment, useMutation } from "react-relay/hooks";
import { useNavigate } from "react-router-dom";
import Timestamp from "../common/Timestamp";
import { ToDoListItemFragment_todoItem$key } from './__generated__/ToDoListItemFragment_todoItem.graphql';
import { ToDoListItemUpdateToDoItemMutation } from './__generated__/ToDoListItemUpdateToDoItemMutation.graphql';

interface Props {
    fragmentRef: ToDoListItemFragment_todoItem$key;
    to: string;
    organizationName: string;
    projectName?: string;
    userId: string;
    icon: React.ReactNode;
    primary: React.ReactNode;
    getConnections: (userId: string, resolved: boolean) => [string];
}

function ToDoListItem({ fragmentRef, to, organizationName, projectName, userId, icon, primary, getConnections }: Props) {
    const theme = useTheme();
    const navigate = useNavigate();
    const { enqueueSnackbar } = useSnackbar();

    const data = useFragment<ToDoListItemFragment_todoItem$key>(
        graphql`
            fragment ToDoListItemFragment_todoItem on ToDoItem {
                id
                resolvable
                resolved
                metadata {
                    createdAt
                }
            }
        `, fragmentRef
    );

    const [commitUpdateToDoItem, updateInFlight] = useMutation<ToDoListItemUpdateToDoItemMutation>(graphql`
        mutation ToDoListItemUpdateToDoItemMutation($input: UpdateToDoItemInput!, $connections: [ID!]!) {
            updateToDoItem(input: $input) {
                todoItem {
                    id @deleteEdge(connections: $connections)
                    resolved
                }
                problems {
                    message
                    field
                    type
                }
            }
        }
    `);

    const onSave = (resolved: boolean) => {
        commitUpdateToDoItem({
            variables: {
                input: {
                    id: data.id,
                    resolved: resolved
                },
                connections: getConnections(userId, data.resolved)
            },
            onCompleted: data => {
                if (data.updateToDoItem.problems.length) {
                    enqueueSnackbar(data.updateToDoItem.problems.map(problem => problem.message).join('; '), { variant: 'warning' });
                } else {
                    enqueueSnackbar('To-Do item updated', { variant: 'success' });
                }
            },
            onError: error => {
                enqueueSnackbar(`Unexpected error occurred: ${error.message}`, { variant: 'error' });
            }
        });
    };

    const path = organizationName.concat(projectName ? ` / ${projectName}` : '');

    return (
        <ListItemButton
            onClick={() => navigate(to)}
            sx={{
                borderBottom: `1px solid ${theme.palette.divider}`,
                '&:last-child': {
                    borderBottomWidth: 0
                },
            }}
        >
            <ListItemAvatar sx={{ minWidth: 64 }}>
                <Avatar sx={{ backgroundColor: 'inherit', color: theme.palette.text.secondary, border: `1px ${theme.palette.divider} solid` }}>
                    {icon}
                </Avatar>
            </ListItemAvatar>
            <Box minWidth={0}>
                <Typography mb={0.5} noWrap component={"div"} variant="body2" color="textSecondary">
                    {path}
                </Typography>
                <Typography sx={{ wordWrap: 'break-word' }} variant="body1" color="text-primary">{primary}</Typography>
                <Timestamp variant="caption" color="textSecondary" timestamp={data.metadata.createdAt} />
            </Box>
            <ListItemSecondaryAction>
                {data.resolvable && !data.resolved && <Tooltip title="Mark as resolved">
                    <Box>
                        <LoadingButton
                            loading={updateInFlight}
                            onClick={() => onSave(true)}
                            sx={{ minWidth: 40, padding: '2px' }}
                            size="small"
                            color="info"
                            variant="outlined"
                        >
                            <DoneIcon />
                        </LoadingButton>
                    </Box>
                </Tooltip>}
                {data.resolvable && data.resolved && <Tooltip title="Add a to do">
                    <Box>
                        <LoadingButton
                            loading={updateInFlight}
                            onClick={() => onSave(false)}
                            sx={{ minWidth: 40, padding: '2px' }}
                            size="small"
                            color="info"
                            variant="outlined"
                        >
                            <PlaylistAddIcon />
                        </LoadingButton>
                    </Box>
                </Tooltip>}
            </ListItemSecondaryAction>
        </ListItemButton>
    );
}

export default ToDoListItem;
