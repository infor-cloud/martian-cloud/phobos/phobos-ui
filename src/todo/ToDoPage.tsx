import { Box, Tab, Tabs, Typography } from '@mui/material';
import graphql from 'babel-plugin-relay/macro';
import { useEffect } from 'react';
import { PreloadedQuery, usePreloadedQuery, useQueryLoader } from 'react-relay/hooks';
import { useSearchParams } from 'react-router-dom';
import TabContent from '../common/TabContent';
import ToDoTab, { INITIAL_ITEM_COUNT } from './ToDoTab';
import { ToDoPageQuery } from './__generated__/ToDoPageQuery.graphql';

const query = graphql`
    query ToDoPageQuery($first: Int, $after: String, $resolved: Boolean) {
        me {
            ... on User {
                ...ToDoTabFragment_todoItems
            }
        }
    }
`;

function ToDoPageEntryPoint() {
    const [queryRef, loadQuery] = useQueryLoader<ToDoPageQuery>(query);
    const searchParams = new URLSearchParams(document.location.search);
    const resolved = searchParams.get('tab') === 'resolved';

    useEffect(() => {
        loadQuery({ first: INITIAL_ITEM_COUNT, resolved: resolved }, { fetchPolicy: 'store-and-network' })
    }, [loadQuery])

    return queryRef != null ? <ToDoPage queryRef={queryRef} /> : null;
}

interface Props {
    queryRef: PreloadedQuery<ToDoPageQuery>;
}

function ToDoPage({ queryRef }: Props) {
    const [searchParams, setSearchParams] = useSearchParams();
    const tab = searchParams.get('tab') || 'todo';

    const queryData = usePreloadedQuery<ToDoPageQuery>(query, queryRef);

    const onTabChange = (_: React.SyntheticEvent, newValue: string) => {
        searchParams.set('tab', newValue);
        setSearchParams(searchParams, { replace: true });
    };

    return (
        <Box maxWidth={1200} margin="auto" padding={2} flex={1}>
            <Box>
                <Typography variant="h6" mb={1} fontWeight={600}>To-Do List</Typography>
                <Box sx={{ display: "flex", border: 1, borderColor: 'divider', borderTopLeftRadius: 4, borderTopRightRadius: 4, mb: 2 }}>
                    <Tabs value={tab} onChange={onTabChange}>
                        <Tab label="To Do" value="todo" />
                        <Tab label="Resolved" value="resolved" />
                    </Tabs>
                </Box>
                <TabContent>
                    <ToDoTab fragmentRef={queryData.me as any} resolved={tab === 'resolved'} />
                </TabContent>
            </Box>
        </Box>
    );
}

export default ToDoPageEntryPoint;
