import { Checkbox, ListItemText, MenuItem, SelectChangeEvent, Typography } from "@mui/material";
import graphql from 'babel-plugin-relay/macro';
import throttle from 'lodash.throttle';
import { useEffect, useMemo, useState } from "react";
import { fetchQuery, useRelayEnvironment } from "react-relay";
import SearchableSelect from "../../common/SearchableSelect";
import { OrgFilterQuery } from "./__generated__/OrgFilterQuery.graphql";

interface Props {
    selected: string[];
    onChange: (organizations: string[]) => void;
}

function OrgFilter({ selected, onChange }: Props) {
    const [searchText, setSearchText] = useState("");
    const [options, setOptions] = useState<ReadonlyArray<string>>([]);
    const [loading, setLoading] = useState<boolean>(false);

    const onOrganizationChange = ({ target: { value } }: SelectChangeEvent<string[]>) => {
        const parsedValue = typeof value === 'string' ? value.split(',') : value;
        onChange(parsedValue);
    };

    const environment = useRelayEnvironment();

    const fetch = useMemo(
        () =>
            throttle(
                (
                    request: { input: string },
                    callback: (results?: readonly string[]) => void,
                ) => {
                    fetchQuery<OrgFilterQuery>(
                        environment,
                        graphql`
                            query OrgFilterQuery($search: String!) {
                                organizations(first: 10, search: $search) {
                                    edges {
                                        node {
                                            name
                                        }
                                    }
                                }
                            }
                        `,
                        { search: request.input },
                        { fetchPolicy: 'network-only' }
                    ).toPromise().then(async response => {
                        const options = response?.organizations?.edges?.map(edge => edge?.node?.name as string);
                        callback(options);
                    });
                },
                300,
            ),
        [environment],
    );

    useEffect(() => {
        let active = true;

        setLoading(true);

        fetch({ input: searchText }, (results?: readonly string[]) => {
            if (active) {
                setOptions(results ?? []);
                setLoading(false);
            }
        });

        return () => {
            active = false;
        };
    }, [fetch, searchText]);

    return (
        <SearchableSelect
            loading={loading}
            displayEmpty
            multiple
            size="small"
            onChange={onOrganizationChange}
            value={selected}
            onSearchChange={setSearchText}
            renderValue={(selected: string[]) => {
                if (selected.length === 0) {
                    return <Typography color="textSecondary">Filter by organization</Typography>;
                }

                return selected.join(', ');
            }}
        >
            {options.map((option) => (
                <MenuItem key={option} value={option}>
                    <Checkbox checked={selected.indexOf(option) > -1} />
                    <ListItemText primary={option} />
                </MenuItem>
            ))}
            {options.length === 0 && searchText === '' && <MenuItem disabled>No Organizations</MenuItem>}
            {options.length === 0 && searchText !== '' && <MenuItem disabled>
                <Typography>
                    No organizations matching search
                </Typography>
            </MenuItem>}
        </SearchableSelect>
    );
}

export default OrgFilter;
