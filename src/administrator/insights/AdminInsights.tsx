import { Alert, Box, Grid, Tab, Tabs, Typography, useTheme } from '@mui/material';
import FormControl from '@mui/material/FormControl';
import { useMemo, useState } from 'react';
import { useSearchParams } from 'react-router-dom';
import TabContent from '../../common/TabContent';
import { MAX_SERIES } from '../../insights/InsightsChart';
import InsightsChartApprovalRequestCompleted from '../../insights/InsightsChartApprovalRequestCompleted';
import InsightsChartApprovalRequestCreated from '../../insights/InsightsChartApprovalRequestCreated';
import InsightsChartApprovalRequestWaitTime from '../../insights/InsightsChartApprovalRequestWaitTime';
import InsightsChartDeploymentFailureRate from '../../insights/InsightsChartDeploymentFailureRate';
import InsightsChartDeploymentFrequency from '../../insights/InsightsChartDeploymentFrequency';
import InsightsChartDeploymentLeadTime from '../../insights/InsightsChartDeploymentLeadTime';
import InsightsChartDeploymentRecoveryTime from '../../insights/InsightsChartDeploymentRecoveryTime';
import InsightsChartDeploymentUpdates from '../../insights/InsightsChartDeploymentUpdates';
import InsightsChartPipelineDuration from '../../insights/InsightsChartPipelineDuration';
import InsightsChartPipelinesCompleted from '../../insights/InsightsChartPipelinesCompleted';
import InsightsTimeRangeSelector from '../../insights/InsightsTimeRangeSelector';
import OrgFilter from './OrgFilter';

function AdminInsights() {
    const theme = useTheme();
    const [searchParams, setSearchParams] = useSearchParams();
    const timeRange = searchParams.get('range') || '30d';
    const tab = searchParams.get('tab') || 'deployments';
    const [organizations, setOrganizations] = useState<string[]>([]);

    const onTimeRangeChange = (value: string) => {
        searchParams.set('range', value);
        setSearchParams(searchParams, { replace: true });
    };

    const onTabChange = (event: React.SyntheticEvent, newValue: string) => {
        searchParams.set('tab', newValue);
        setSearchParams(searchParams, { replace: true });
    };

    const filters = useMemo(() => {
        if (organizations.length) {
            return organizations.map(orgName => ({
                organizationId: `prn:organization:${orgName}`,
                label: orgName,
            }));
        } else {
            return [{ label: '' }];
        }
    }, [organizations]);

    const showLegend = useMemo(() => organizations.length > 0, [organizations]);

    return <Box>
        <Typography variant="h5" mb={2}>Insights</Typography>
        <Box sx={{
            marginBottom: 2,
            display: 'flex',
            flexDirection: 'row',
            justifyContent: 'space-between',
            [theme.breakpoints.down('md')]: {
                flexDirection: 'column',
                alignItems: 'flex-start',
                '& > *:not(:last-child)': { marginBottom: 2 },
            }
        }}>
            <InsightsTimeRangeSelector value={timeRange} onChange={onTimeRangeChange} />
            <Box sx={{
                display: 'flex',
                flexDirection: 'row',
                justifyContent: 'space-between',
                ml: 2,
                '& > *': { width: 250 },
                '& > *:not(:last-child)': { mr: 2 },
                [theme.breakpoints.down('md')]: {
                    ml: 0,
                    width: '100%',
                    flexDirection: 'column',
                    alignItems: 'flex-start',
                    '& > *': { width: '100%' },
                    '& > *:not(:last-child)': { mb: 2, mr: 0 },
                }
            }}>
                <FormControl>
                    <OrgFilter selected={organizations} onChange={setOrganizations} />
                </FormControl>
            </Box>
        </Box>
        {(filters.length * 2) > MAX_SERIES && <Alert severity="warning" sx={{ marginBottom: 2 }}>
            <Typography variant="body2">
                Some data is not being displayed because there are too many filters selected. Please reduce the number of filters to see all the data.
            </Typography>
        </Alert>}
        <Box sx={{ display: "flex", border: 1, borderColor: 'divider', borderTopLeftRadius: 4, borderTopRightRadius: 4, mb: 1 }}>
            <Tabs value={tab} onChange={onTabChange}>
                <Tab label="Deployments" value="deployments" />
                <Tab label="Pipelines" value="pipelines" />
                <Tab label="Approvals" value="approvals" />
            </Tabs>
        </Box>
        <TabContent>
            {tab === 'deployments' && <Grid container spacing={2}>
                <Grid item xs={12} md={6}>
                    <InsightsChartDeploymentFrequency
                        timeRange={timeRange}
                        filters={filters}
                        showLegend={showLegend}
                    />
                </Grid>
                <Grid item xs={12} md={6}>
                    <InsightsChartDeploymentLeadTime
                        timeRange={timeRange}
                        filters={filters}
                        showLegend={showLegend}
                    />
                </Grid>
                <Grid item xs={12} md={6}>
                    <InsightsChartDeploymentFailureRate
                        timeRange={timeRange}
                        filters={filters}
                        showLegend={showLegend}
                    />
                </Grid>
                <Grid item xs={12} md={6}>
                    <InsightsChartDeploymentRecoveryTime
                        timeRange={timeRange}
                        filters={filters}
                        showLegend={showLegend}
                    />
                </Grid>
                <Grid item xs={12}>
                    <InsightsChartDeploymentUpdates
                        timeRange={timeRange}
                        filters={filters}
                        showLegend={showLegend}
                    />
                </Grid>
            </Grid>}
            {tab === 'pipelines' && <Grid container spacing={2}>
                <Grid item xs={12}>
                    <InsightsChartPipelinesCompleted
                        timeRange={timeRange}
                        filters={filters}
                        showLegend={showLegend}
                    />
                </Grid>
                <Grid item xs={12}>
                    <InsightsChartPipelineDuration
                        timeRange={timeRange}
                        filters={filters}
                        showLegend={showLegend}
                    />
                </Grid>
            </Grid>}
            {tab === 'approvals' && <Grid container spacing={2}>
                <Grid item xs={12} md={6}>
                    <InsightsChartApprovalRequestCreated
                        timeRange={timeRange}
                        filters={filters}
                        showLegend={showLegend}
                    />
                </Grid>
                <Grid item xs={12} md={6}>
                    <InsightsChartApprovalRequestCompleted
                        timeRange={timeRange}
                        filters={filters}
                        showLegend={showLegend}
                    />
                </Grid>
                <Grid item xs={12}>
                    <InsightsChartApprovalRequestWaitTime
                        timeRange={timeRange}
                        filters={filters}
                        showLegend={showLegend}
                    />
                </Grid>
            </Grid>}
        </TabContent>
    </Box >;
}

export default AdminInsights;
