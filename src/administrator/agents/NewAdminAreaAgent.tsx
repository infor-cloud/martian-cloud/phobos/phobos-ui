import { useState } from 'react';
import { Box, Button, Divider, Typography } from '@mui/material';
import LoadingButton from '@mui/lab/LoadingButton';
import graphql from 'babel-plugin-relay/macro';
import { useMutation } from 'react-relay/hooks';
import { Link as RouterLink, useNavigate } from 'react-router-dom';
import { MutationError } from '../../common/error';
import AdminAreaBreadcrumbs from '../AdminAreaBreadcrumbs';
import AgentForm, { FormData } from '../../agents/AgentForm';
import { GetConnections } from './AdminAreaAgents';
import { NewAdminAreaAgentMutation } from './__generated__/NewAdminAreaAgentMutation.graphql';

function NewAdminAreaAgent() {
    const navigate = useNavigate();

    const [commit, isInFlight] = useMutation<NewAdminAreaAgentMutation>(graphql`
        mutation NewAdminAreaAgentMutation($input: CreateAgentInput!, $connections: [ID!]!) {
            createAgent(input: $input) {
                # Use @prependNode to add the node to the connection
                agent  @prependNode(connections: $connections, edgeTypeName: "AgentEdge") {
                    id
                    ...AgentDetailsFragment_agent
                }
                problems {
                    message
                    field
                    type
                }
            }
        }
    `);

    const [error, setError] = useState<MutationError>()
    const [formData, setFormData] = useState<FormData>({
        name: '',
        description: '',
        disabled: false,
        tags: [],
        runUntaggedJobs: true
    });

    const onSave = () => {
        commit({
            variables: {
                input: {
                    name: formData.name,
                    description: formData.description,
                    type: 'SHARED',
                    disabled: formData.disabled,
                    runUntaggedJobs: formData.runUntaggedJobs,
                    tags: formData.tags
                },
                connections: GetConnections()
            },
            onCompleted: data => {
                if (data.createAgent.problems.length) {
                    setError({
                        severity: 'warning',
                        message: data.createAgent.problems.map((problem: any) => problem.message).join('; ')
                    });
                } else if (!data.createAgent.agent) {
                    setError({
                        severity: 'error',
                        message: "Unexpected error occurred"
                    });
                } else {
                    navigate(`../${data.createAgent.agent.id}`);
                }
            },
            onError: error => {
                setError({
                    severity: 'error',
                    message: `Unexpected error occurred: ${error.message}`
                });
            }
        });
    };

    return (
        <Box>
            <AdminAreaBreadcrumbs
                childRoutes={[
                    { title: "agents", path: 'agents' },
                    { title: "new", path: 'new' },
                ]}
            />
            <Typography variant="h5">New Agent</Typography>
            <AgentForm
                data={formData}
                onChange={(data: FormData) => setFormData(data)}
                error={error}
            />
            <Divider sx={{ opacity: 0.6 }}   />
            <Box mt={2}>
                <LoadingButton
                    disabled={!formData.name}
                    loading={isInFlight}
                    variant="outlined"
                    color="primary"
                    sx={{ mr: 2 }}
                    onClick={onSave}
                >
                    Create Agent
                </LoadingButton>
                <Button component={RouterLink} color="inherit" to={-1 as any}>Cancel</Button>
            </Box>
        </Box>
    );
}

export default NewAdminAreaAgent
