import Box from '@mui/material/Box';
import graphql from 'babel-plugin-relay/macro';
import { ConnectionHandler, useLazyLoadQuery, usePaginationFragment } from 'react-relay/hooks';
import AdminAreaBreadcrumbs from '../AdminAreaBreadcrumbs';
import AgentList from '../../agents/AgentList';
import { AdminAreaAgentsQuery } from './__generated__/AdminAreaAgentsQuery.graphql';
import { AdminAreaAgentsPaginationQuery } from './__generated__/AdminAreaAgentsPaginationQuery.graphql';
import { AdminAreaAgentsFragment_sharedAgents$key } from './__generated__/AdminAreaAgentsFragment_sharedAgents.graphql';

export const INITIAL_ITEM_COUNT = 100;

export function GetConnections(): [string] {
    const connectionId = ConnectionHandler.getConnectionID(
        'root',
        'AdminAreaAgents_sharedAgents'
    );
    return [connectionId];
}

const query = graphql`
    query AdminAreaAgentsQuery($first: Int!, $after: String) {
        ...AdminAreaAgentsFragment_sharedAgents
    }
`;

function AdminAreaAgents() {

    const queryData = useLazyLoadQuery<AdminAreaAgentsQuery>(query, { first: INITIAL_ITEM_COUNT });

    const { data, loadNext, hasNext } = usePaginationFragment<AdminAreaAgentsPaginationQuery, AdminAreaAgentsFragment_sharedAgents$key> (
        graphql`
            fragment AdminAreaAgentsFragment_sharedAgents on Query
            @refetchable(queryName: "AdminAreaAgentsPaginationQuery")
             {
                sharedAgents(
                    first: $first
                    after: $after
                ) @connection(key: "AdminAreaAgents_sharedAgents") {
                    edges {
                        node {
                            id
                        }
                    }
                    ...AgentListFragment_agents
                }
            }
        `, queryData);

    return (
        <Box>
            <AdminAreaBreadcrumbs
                childRoutes={[
                    { title: "agents", path: 'agents' }
                ]}
            />
            <Box>
                <AgentList
                    fragmentRef={data.sharedAgents}
                    hasNext={hasNext}
                    loadNext={loadNext}
                />
            </Box>
        </Box>
    );
}

export default AdminAreaAgents
