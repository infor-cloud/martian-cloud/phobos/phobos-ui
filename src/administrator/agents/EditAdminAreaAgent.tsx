import { useState } from 'react';
import { Box, Button, Divider, Typography } from '@mui/material';
import LoadingButton from '@mui/lab/LoadingButton';
import graphql from 'babel-plugin-relay/macro';
import { useLazyLoadQuery, useMutation } from 'react-relay/hooks';
import { Link as RouterLink, useNavigate, useParams } from 'react-router-dom';
import { MutationError } from '../../common/error';
import AdminAreaBreadcrumbs from '../AdminAreaBreadcrumbs';
import AgentForm, { FormData } from '../../agents/AgentForm';
import { EditAdminAreaAgentQuery } from './__generated__/EditAdminAreaAgentQuery.graphql';
import { EditAdminAreaAgentMutation } from './__generated__/EditAdminAreaAgentMutation.graphql';

function EditAdminAreaAgent() {
    const agentId = useParams().agentId as string;
    const navigate = useNavigate();

    const queryData = useLazyLoadQuery<EditAdminAreaAgentQuery>(graphql`
        query EditAdminAreaAgentQuery($id: String!) {
            node(id: $id) {
                ... on Agent {
                    id
                    name
                    description
                    disabled
                    tags
                    runUntaggedJobs
                }
            }
        }
    `, { id: agentId });

    const [commit, isInFlight] = useMutation<EditAdminAreaAgentMutation>(graphql`
        mutation EditAdminAreaAgentMutation($input: UpdateAgentInput!) {
            updateAgent(input: $input) {
                agent {
                    ...AgentDetailsFragment_agent
                }
                problems {
                    message
                    field
                    type
                }
            }
        }
    `);

    const agent = queryData.node as any;

    const [error, setError] = useState<MutationError>();
    const [formData, setFormData] = useState<FormData>({
        name: agent.name,
        description: agent.description,
        disabled: agent.disabled,
        tags: agent.tags,
        runUntaggedJobs: agent.runUntaggedJobs
    });

    const onUpdate = () => {
        if (formData) {
            commit({
                variables: {
                    input: {
                        id: agent.id,
                        description: formData.description,
                        disabled: formData.disabled,
                        runUntaggedJobs: formData.runUntaggedJobs,
                        tags: formData.tags
                    }
                },
                onCompleted: data => {
                    if (data.updateAgent.problems.length) {
                        setError({
                            severity: 'warning',
                            message: data.updateAgent.problems.map((problem: any) => problem.message).join('; ')
                        });
                    } else if (!data.updateAgent.agent) {
                        setError({
                            severity: 'error',
                            message: "Unexpected error occurred"
                        });
                    } else {
                        navigate(`../`);
                    }
                },
                onError: error => {
                    setError({
                        severity: 'error',
                        message: `Unexpected error occurred: ${error.message}`
                    });
                }
            });
        }
    };

    return (
        <Box>
            <AdminAreaBreadcrumbs
                childRoutes={[
                    { title: "agents", path: 'agents' },
                    { title: formData.name, path: agent.id },
                    { title: "edit", path: 'edit' },
                ]}
            />
            <Typography variant="h5">Edit Agent</Typography>
            <AgentForm
                editMode
                data={formData}
                onChange={(data: FormData) => setFormData(data)}
                error={error}
            />
            <Divider sx={{ opacity: 0.6 }}   />
            <Box mt={2}>
                <LoadingButton
                    loading={isInFlight}
                    variant="outlined"
                    color="primary"
                    sx={{ mr: 2 }}
                    onClick={onUpdate}>
                    Update Agent
                </LoadingButton>
                <Button component={RouterLink} color="inherit" to={-1 as any}>Cancel</Button>
            </Box>
        </Box>
    );
}

export default EditAdminAreaAgent
