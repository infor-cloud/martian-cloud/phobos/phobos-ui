import { Box, Typography } from '@mui/material';
import graphql from 'babel-plugin-relay/macro';
import { useLazyLoadQuery } from 'react-relay/hooks';
import { useParams } from 'react-router-dom';
import AgentDetails from '../../agents/AgentDetails';
import AdminAreaBreadcrumbs from '../AdminAreaBreadcrumbs';
import { AdminAreaAgentDetailsQuery } from './__generated__/AdminAreaAgentDetailsQuery.graphql';

function AdminAreaAgentDetails() {
    const agentId = useParams().agentId as string;

    const data = useLazyLoadQuery<AdminAreaAgentDetailsQuery>(graphql`
        query AdminAreaAgentDetailsQuery($id: String!) {
            node(id: $id) {
                ... on Agent {
                    id
                    name
                    ...AgentDetailsFragment_agent
                }
            }
        }`, { id: agentId });

    const agent = data.node as any;

    if (data.node) {
        return (
            <Box>
                <AdminAreaBreadcrumbs
                    childRoutes={[
                        { title: "agents", path: 'agents' },
                        { title: agent.name, path: agent.id }
                    ]}
                />
                <AgentDetails fragmentRef={data.node}  />
            </Box>
        );
    } else {
        return <Box display="flex" justifyContent="center" mt={4}>
            <Typography color="textSecondary">Agent with ID {agentId} not found</Typography>
        </Box>;
    }
}

export default AdminAreaAgentDetails
