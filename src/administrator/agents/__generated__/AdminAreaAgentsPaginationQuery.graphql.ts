/**
 * @generated SignedSource<<37d770ff08dae0d15d11eea6874f194f>>
 * @lightSyntaxTransform
 * @nogrep
 */

/* tslint:disable */
/* eslint-disable */
// @ts-nocheck

import { ConcreteRequest, Query } from 'relay-runtime';
import { FragmentRefs } from "relay-runtime";
export type AdminAreaAgentsPaginationQuery$variables = {
  after?: string | null | undefined;
  first?: number | null | undefined;
};
export type AdminAreaAgentsPaginationQuery$data = {
  readonly " $fragmentSpreads": FragmentRefs<"AdminAreaAgentsFragment_sharedAgents">;
};
export type AdminAreaAgentsPaginationQuery = {
  response: AdminAreaAgentsPaginationQuery$data;
  variables: AdminAreaAgentsPaginationQuery$variables;
};

const node: ConcreteRequest = (function(){
var v0 = [
  {
    "defaultValue": null,
    "kind": "LocalArgument",
    "name": "after"
  },
  {
    "defaultValue": null,
    "kind": "LocalArgument",
    "name": "first"
  }
],
v1 = [
  {
    "kind": "Variable",
    "name": "after",
    "variableName": "after"
  },
  {
    "kind": "Variable",
    "name": "first",
    "variableName": "first"
  }
];
return {
  "fragment": {
    "argumentDefinitions": (v0/*: any*/),
    "kind": "Fragment",
    "metadata": null,
    "name": "AdminAreaAgentsPaginationQuery",
    "selections": [
      {
        "args": null,
        "kind": "FragmentSpread",
        "name": "AdminAreaAgentsFragment_sharedAgents"
      }
    ],
    "type": "Query",
    "abstractKey": null
  },
  "kind": "Request",
  "operation": {
    "argumentDefinitions": (v0/*: any*/),
    "kind": "Operation",
    "name": "AdminAreaAgentsPaginationQuery",
    "selections": [
      {
        "alias": null,
        "args": (v1/*: any*/),
        "concreteType": "AgentConnection",
        "kind": "LinkedField",
        "name": "sharedAgents",
        "plural": false,
        "selections": [
          {
            "alias": null,
            "args": null,
            "concreteType": "AgentEdge",
            "kind": "LinkedField",
            "name": "edges",
            "plural": true,
            "selections": [
              {
                "alias": null,
                "args": null,
                "concreteType": "Agent",
                "kind": "LinkedField",
                "name": "node",
                "plural": false,
                "selections": [
                  {
                    "alias": null,
                    "args": null,
                    "kind": "ScalarField",
                    "name": "id",
                    "storageKey": null
                  },
                  {
                    "alias": null,
                    "args": null,
                    "kind": "ScalarField",
                    "name": "__typename",
                    "storageKey": null
                  },
                  {
                    "alias": null,
                    "args": null,
                    "concreteType": "ResourceMetadata",
                    "kind": "LinkedField",
                    "name": "metadata",
                    "plural": false,
                    "selections": [
                      {
                        "alias": null,
                        "args": null,
                        "kind": "ScalarField",
                        "name": "createdAt",
                        "storageKey": null
                      }
                    ],
                    "storageKey": null
                  },
                  {
                    "alias": null,
                    "args": null,
                    "kind": "ScalarField",
                    "name": "tags",
                    "storageKey": null
                  },
                  {
                    "alias": null,
                    "args": null,
                    "kind": "ScalarField",
                    "name": "name",
                    "storageKey": null
                  },
                  {
                    "alias": null,
                    "args": null,
                    "kind": "ScalarField",
                    "name": "disabled",
                    "storageKey": null
                  },
                  {
                    "alias": null,
                    "args": null,
                    "kind": "ScalarField",
                    "name": "createdBy",
                    "storageKey": null
                  }
                ],
                "storageKey": null
              },
              {
                "alias": null,
                "args": null,
                "kind": "ScalarField",
                "name": "cursor",
                "storageKey": null
              }
            ],
            "storageKey": null
          },
          {
            "alias": null,
            "args": null,
            "concreteType": "PageInfo",
            "kind": "LinkedField",
            "name": "pageInfo",
            "plural": false,
            "selections": [
              {
                "alias": null,
                "args": null,
                "kind": "ScalarField",
                "name": "endCursor",
                "storageKey": null
              },
              {
                "alias": null,
                "args": null,
                "kind": "ScalarField",
                "name": "hasNextPage",
                "storageKey": null
              }
            ],
            "storageKey": null
          }
        ],
        "storageKey": null
      },
      {
        "alias": null,
        "args": (v1/*: any*/),
        "filters": null,
        "handle": "connection",
        "key": "AdminAreaAgents_sharedAgents",
        "kind": "LinkedHandle",
        "name": "sharedAgents"
      }
    ]
  },
  "params": {
    "cacheID": "143e324701c2cf0779b785e3669f6c0e",
    "id": null,
    "metadata": {},
    "name": "AdminAreaAgentsPaginationQuery",
    "operationKind": "query",
    "text": "query AdminAreaAgentsPaginationQuery(\n  $after: String\n  $first: Int\n) {\n  ...AdminAreaAgentsFragment_sharedAgents\n}\n\nfragment AdminAreaAgentsFragment_sharedAgents on Query {\n  sharedAgents(first: $first, after: $after) {\n    edges {\n      node {\n        id\n        __typename\n      }\n      cursor\n    }\n    ...AgentListFragment_agents\n    pageInfo {\n      endCursor\n      hasNextPage\n    }\n  }\n}\n\nfragment AgentListFragment_agents on AgentConnection {\n  edges {\n    node {\n      id\n      ...AgentListItemFragment_agent\n    }\n  }\n}\n\nfragment AgentListItemFragment_agent on Agent {\n  metadata {\n    createdAt\n  }\n  tags\n  id\n  name\n  disabled\n  createdBy\n}\n"
  }
};
})();

(node as any).hash = "d37cc36d267c4ae0d0fcefd8ad06b2ed";

export default node;
