import { useState } from "react";
import graphql from "babel-plugin-relay/macro";
import { Box, Button, Divider, Typography } from "@mui/material";
import LoadingButton from '@mui/lab/LoadingButton';
import { Link as RouterLink, useNavigate } from "react-router-dom";
import { MutationError } from "../../common/error";
import AdminAreaOrganizationForm, { FormData } from "./AdminAreaOrganizationForm";
import { useMutation } from "react-relay/hooks";
import AdminBreadcrumbs from "../AdminAreaBreadcrumbs";
import { GetConnections } from "./AdminAreaOrganizationList";
import { GetMenuButtonConnections } from "../../organizations/OrganizationMenu";
import { AdminAreaNewOrganizationMutation } from "./__generated__/AdminAreaNewOrganizationMutation.graphql";

function AdminAreaNewOrganization() {
    const navigate = useNavigate();
    const [error, setError] = useState<MutationError>()
    const [formData, setFormData] = useState<FormData>({
        name: '',
        description: ''
    })

    const [commit, isInFlight] = useMutation<AdminAreaNewOrganizationMutation>(graphql`
        mutation AdminAreaNewOrganizationMutation($input: CreateOrganizationInput!, $connections: [ID!]!) {
            createOrganization(input: $input) {
                # Use @prependNode to add the node to the connection
                organization @prependNode(connections: $connections, edgeTypeName: "OrganizationEdge") {
                    name
                    description
                    ...AdminAreaOrganizationListItemFragment_organization
                }
                problems {
                    message
                    field
                    type
                }
            }
        }
    `);

    const onSave = () => {
        commit({
            variables: {
                input: {
                    name: formData.name,
                    description: formData.description
                },
                connections: GetConnections() && GetMenuButtonConnections()
            },
            onCompleted: data => {
                if (data.createOrganization.problems.length) {
                    setError({
                        severity: 'warning',
                        message: data.createOrganization.problems.map((problem: any) => problem.message).join(', ')
                    });
                } else if (!data.createOrganization.organization) {
                    setError({
                        severity: 'error',
                        message: 'Unexpected error occurred'
                    });
                } else {
                    navigate(`/organizations/${data.createOrganization.organization.name}`)
                }
            },
            onError: (error) => {
                setError({
                    severity: 'error',
                    message: `Unexpected error occurred: ${error.message}`
                })
            }
        });
    };

    return (
        <Box>
            <AdminBreadcrumbs
                childRoutes={[
                    { title: "organizations", path: 'organizations' },
                    { title: "new", path: 'new' }
                ]}
            />
            <Typography variant="h5">New Organization</Typography>
            <AdminAreaOrganizationForm
                data={formData}
                onChange={(data: FormData) => setFormData(data)}
                error={error}
            />
            <Divider sx={{ opacity: 0.6 }}   />
            <Box marginTop={2}>
                <LoadingButton
                    sx={{ mr: 2 }}
                    loading={isInFlight}
                    variant="outlined"
                    color="primary"
                    onClick={onSave}
                >Create Organization</LoadingButton>
                <Button component={RouterLink} color="inherit" to={-1 as any}>Cancel</Button>
            </Box>
        </Box>
    );
}

export default AdminAreaNewOrganization
