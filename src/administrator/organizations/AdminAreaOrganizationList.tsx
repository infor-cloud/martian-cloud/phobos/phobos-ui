import { Box, Button, List, Paper, Typography, useTheme } from "@mui/material";
import graphql from "babel-plugin-relay/macro";
import throttle from 'lodash.throttle';
import { useMemo, useState } from "react";
import InfiniteScroll from 'react-infinite-scroll-component';
import { ConnectionHandler, fetchQuery, useLazyLoadQuery, usePaginationFragment, useRelayEnvironment } from "react-relay/hooks";
import { Link as RouterLink } from "react-router-dom";
import SearchInput from "../../common/SearchInput";
import ListSkeleton from "../../skeletons/ListSkeleton";
import AdminBreadcrumbs from "../AdminAreaBreadcrumbs";
import AdminAreaOrganizationListItem from "./AdminAreaOrganizationListItem";
import { AdminAreaOrganizationListFragment_organizations$key } from "./__generated__/AdminAreaOrganizationListFragment_organizations.graphql";
import { AdminAreaOrganizationListQuery } from "./__generated__/AdminAreaOrganizationListQuery.graphql";
import { OrganizationPaginationQuery } from "./__generated__/OrganizationPaginationQuery.graphql";

export function GetConnections() {
    const connectionId = ConnectionHandler.getConnectionID(
        "root",
        "AdminAreaOrganizationList_organizations",
    );
    return [connectionId];
}

const query = graphql`
    query AdminAreaOrganizationListQuery($first: Int!, $after: String, $search: String) {
        ...AdminAreaOrganizationListFragment_organizations
    }`

const INITIAL_ITEM_COUNT = 100;
const DESCRIPTION = 'Organizations are the top-level isolation boundary in Phobos and contain many of the resources used in Phobos, such as projects, runner agents, and lifecycles.';

function AdminAreaOrganizationList() {
    const theme = useTheme();
    const [search, setSearch] = useState<string | undefined>('');
    const [isRefreshing, setIsRefreshing] = useState(false);

    const queryData = useLazyLoadQuery<AdminAreaOrganizationListQuery>(
        query,
        { first: INITIAL_ITEM_COUNT, search: '' },
        { fetchPolicy: 'store-and-network' }
    );

    const { data, loadNext, hasNext, refetch } = usePaginationFragment<OrganizationPaginationQuery, AdminAreaOrganizationListFragment_organizations$key>(
        graphql`
        fragment AdminAreaOrganizationListFragment_organizations on Query
        @refetchable(queryName: "OrganizationPaginationQuery") {
            organizations(
                first: $first
                after: $after
                search: $search
                sort: UPDATED_AT_DESC
            ) @connection(key: "AdminAreaOrganizationList_organizations") {
                totalCount
                edges {
                    node {
                        id
                        ...AdminAreaOrganizationListItemFragment_organization
                    }
                }
            }
        }
    `, queryData);

    const environment = useRelayEnvironment();

    const fetch = useMemo(
        () =>
            throttle(
                (input?: string) => {
                    setIsRefreshing(true);

                    fetchQuery(environment, query, { first: INITIAL_ITEM_COUNT, search: input })
                        .subscribe({
                            complete: () => {
                                setIsRefreshing(false);
                                setSearch(input);

                                // *After* the query has been fetched, we call
                                // refetch again to re-render with the updated data.
                                // At this point the data for the query should
                                // be cached, so we use the 'store-only'
                                // fetchPolicy to avoid suspending.
                                refetch({
                                    first: INITIAL_ITEM_COUNT,
                                    search: input
                                }, {
                                    fetchPolicy: 'store-only'
                                });
                            },
                            error: () => {
                                setIsRefreshing(false);
                            }
                        });
                },
                2000,
                { leading: false, trailing: true }
            ),
        [environment, refetch],
    );

    const onSearchChange = (event: React.ChangeEvent<HTMLInputElement>) => {
        fetch(event.target.value.toLowerCase().trim());
    };

    const onKeyDown = (event: React.KeyboardEvent<HTMLInputElement>) => {
        // Only handle enter key type
        if (event.key === 'Enter') {
            fetch.flush();
        }
    };

    return (
        <Box>
            <AdminBreadcrumbs
                childRoutes={[
                    { title: "organizations", path: 'organizations' }
                ]}
            />
            {(data.organizations?.edges && (search !== '' || data.organizations?.edges.length !== 0)) &&
                <Box>
                    <Box sx={{
                        display: 'flex',
                        flexDirection: 'row',
                        justifyContent: 'space-between',
                        [theme.breakpoints.down('md')]: {
                            flexDirection: 'column',
                            alignItems: 'flex-start',
                            '& > *': { marginBottom: 2 },
                        }
                    }}>
                        <Box>
                            <Typography variant="h5" gutterBottom>Organizations</Typography>
                            <Typography variant="body2">{DESCRIPTION}</Typography>
                        </Box>
                        <Box>
                            <Button
                                sx={{ minWidth: 200 }}
                                variant="outlined"
                                component={RouterLink}
                                to="new"
                            >
                                New Organization
                            </Button>
                        </Box>

                    </Box>
                    <SearchInput
                        sx={{ marginTop: 2, marginBottom: 2 }}
                        placeholder="search for organizations"
                        fullWidth
                        onChange={onSearchChange}
                        onKeyDown={onKeyDown}
                    />
                    <Paper sx={{ borderBottomLeftRadius: 0, borderBottomRightRadius: 0, border: `1px solid ${theme.palette.divider}` }}>
                        <Box padding={2} display="flex" alignItems="center" justifyContent="space-between">
                            <Typography variant="subtitle1">
                                {data.organizations?.edges.length} organization{data.organizations.totalCount === 1 ? '' : 's'}
                            </Typography>
                        </Box>
                    </Paper>
                    {(data.organizations?.edges?.length === 0) && search !== '' && <Typography
                        sx={{
                            padding: 4,
                            borderBottom: `1px solid ${theme.palette.divider}`,
                            borderLeft: `1px solid ${theme.palette.divider}`,
                            borderRight: `1px solid ${theme.palette.divider}`,
                            borderBottomLeftRadius: 4,
                            borderBottomRightRadius: 4
                        }}
                        align="center"
                        color="textSecondary"
                    >
                        No organizations matching search <strong>{search}</strong>
                    </Typography>}
                    <InfiniteScroll
                        dataLength={data.organizations?.edges.length ?? 0}
                        next={() => loadNext(20)}
                        hasMore={hasNext}
                        loader={<ListSkeleton rowCount={3} />}
                    >
                        <List sx={isRefreshing ? { opacity: 0.5 } : null} disablePadding>
                            {data.organizations?.edges?.map((edge: any) => <AdminAreaOrganizationListItem key={edge.node.id} fragmentRef={edge.node} />)}
                        </List>
                    </InfiniteScroll>
                </Box>}
            {(search === '' && (data.organizations?.edges && data.organizations.totalCount === 0)) && <Box sx={{ marginTop: 4 }} display="flex" justifyContent="center">
                <Box padding={4} display="flex" flexDirection="column" justifyContent="center" alignItems="center" sx={{ maxWidth: 600 }}>
                    <Typography variant="h6">Get started with organizations</Typography>
                    <Typography color="textSecondary" align="center" sx={{ marginBottom: 2 }}>
                        {DESCRIPTION}
                    </Typography>
                    <Button
                        component={RouterLink}
                        variant="outlined"
                        to='new'
                    >New Organization</Button>
                </Box>
            </Box>}
        </Box>
    );
}

export default AdminAreaOrganizationList
