import { Avatar, ListItemButton, ListItemText, Typography, useTheme } from "@mui/material";
import { purple } from "@mui/material/colors";
import { useFragment } from "react-relay";
import graphql from 'babel-plugin-relay/macro';
import moment from 'moment';
import { useNavigate } from "react-router";
import { AdminAreaOrganizationListItemFragment_organization$key } from "./__generated__/AdminAreaOrganizationListItemFragment_organization.graphql";

interface Props {
    fragmentRef: AdminAreaOrganizationListItemFragment_organization$key
}

function AdminAreaOrganizationListItem({ fragmentRef }: Props) {
    const theme = useTheme();
    const navigate = useNavigate();

    const data = useFragment<AdminAreaOrganizationListItemFragment_organization$key>(
        graphql`
            fragment AdminAreaOrganizationListItemFragment_organization on Organization {
                metadata {
                    updatedAt
                }
                name
                description
            }
        `, fragmentRef
    );

    return (
        <ListItemButton
            dense
            onClick={() => navigate(`/organizations/${data.name}`)}
            sx={{
                paddingY: data.description ? 0 : 1.5,
                borderBottom: `1px solid ${theme.palette.divider}`,
                borderLeft: `1px solid ${theme.palette.divider}`,
                borderRight: `1px solid ${theme.palette.divider}`,
                '&:last-child': {
                    borderBottomLeftRadius: 4,
                    borderBottomRightRadius: 4
                }
            }}>
            <Avatar variant="rounded" sx={{ width: 32, height: 32, bgcolor: purple[300], marginRight: 2 }}>
                {data.name[0].toUpperCase()}
            </Avatar>
            <ListItemText
                primary={<Typography>{data.name}</Typography>}
                secondary={data.description} />
            <Typography variant="body2" color="textSecondary">
                {moment(data.metadata.updatedAt as moment.MomentInput).fromNow()}
            </Typography>
        </ListItemButton>
    );
}

export default AdminAreaOrganizationListItem
