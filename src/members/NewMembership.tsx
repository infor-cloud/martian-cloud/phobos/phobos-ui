import React, { useState } from 'react';
import PanelButton from '../common/PanelButton';
import { MutationError } from '../common/error';
import LoadingButton from '@mui/lab/LoadingButton';
import { Link as RouterLink } from 'react-router-dom';
import { Alert, Box, Button, Divider, Stack, Typography } from '@mui/material';
import UserAutocomplete, { UserOption } from '../members/UserAutocomplete';
import RoleAutocomplete, { RoleOption } from '../common/RoleAutocomplete';
import TeamAutocomplete, { TeamOption } from '../members/TeamAutocomplete';
import ServiceAccountAutocomplete, { ServiceAccountOption } from '../members/ServiceAccountAutocomplete';

const MemberTypes = [
    { name: 'user', title: 'User', description: 'A user represents a human identity' },
    { name: 'team', title: 'Team', description: 'A team is a collection of users' },
    { name: 'serviceAccount', title: 'Service Account', description: 'A service account represents a machine identity' }
];

interface Props {
    orgId?: string
    projectId?: string
    error: MutationError | undefined
    onCreate: (member: string | undefined, role: string | undefined, memberType: string) => void
    loading: boolean
}

function NewMembership({ orgId, projectId, onCreate, loading, error }: Props) {
    const [memberType, setMemberType] = useState<string>('');
    const [role, setRole] = useState<string | undefined>();
    const [member, setMember] = useState<string | undefined>();

    const onTypeChange = (type: string) => {
        if (type !== memberType) {
            setMemberType(type);
            setMember(undefined);
        }
    };

    const onRoleChange = (role: RoleOption | null) => {
        setRole(role?.name);
    };

    const onUserChange = (user: UserOption | null) => {
        setMember(user?.username);
    };

    const onTeamChange = (team: TeamOption | null) => {
        setMember(team?.name);
    };

    const onServiceAccountChange = (serviceAccount: ServiceAccountOption | null) => {
        setMember(serviceAccount?.id);
    };

    return (
        <Box>
            <Typography variant="h5">Add Member</Typography>
            {error && <Alert sx={{ marginTop: 2 }} severity={error.severity}>
                <Typography>{error.message}</Typography>
            </Alert>}
            <Box marginTop={2} marginBottom={2}>
                <Typography variant="subtitle1" gutterBottom>Select a Member Type</Typography>
                <Divider sx={{ opacity: 0.6 }} />
                <Stack marginTop={2} direction="row" spacing={2}>
                    {MemberTypes.map(type => <PanelButton
                        key={type.name}
                        selected={type.name === memberType}
                        onClick={() => onTypeChange(type.name)}
                    >
                        <Typography variant="subtitle1">{type.title}</Typography>
                        <Typography variant="caption" align="center">
                            {type.description}
                        </Typography>
                    </PanelButton>)}
                </Stack>
            </Box>
            {!!memberType && <React.Fragment>
                <Typography variant="subtitle1" gutterBottom>Details</Typography>
                <Divider sx={{ opacity: 0.6 }} />
                <Box marginTop={2} marginBottom={2}>
                    {memberType === 'user' && <Box marginBottom={2}>
                        <Typography gutterBottom color="textSecondary">User</Typography>
                        <UserAutocomplete onSelected={onUserChange} />
                    </Box>}
                    {memberType === 'team' && <Box marginBottom={2}>
                        <Typography gutterBottom color="textSecondary">Team</Typography>
                        <TeamAutocomplete onSelected={onTeamChange} />
                    </Box>}
                    {memberType === 'serviceAccount' && <Box marginBottom={2}>
                        <Typography gutterBottom color="textSecondary">Service Account</Typography>
                        <ServiceAccountAutocomplete
                            orgId={orgId}
                            projectId={projectId}
                            onSelected={onServiceAccountChange} />
                    </Box>}
                    <Box marginBottom={2}>
                        <Typography gutterBottom color="textSecondary">Role</Typography>
                        <RoleAutocomplete onSelected={onRoleChange} />
                    </Box>
                </Box>
            </React.Fragment>}
            <Divider sx={{ opacity: 0.6 }} />
            <Box marginTop={2}>
                <LoadingButton
                    loading={loading}
                    disabled={!member || !role}
                    variant="outlined"
                    color="primary"
                    sx={{ marginRight: 2 }}
                    onClick={() => onCreate(member, role, memberType)}
                >
                    Add Member
                </LoadingButton>
                <Button component={RouterLink} color="inherit" to="..">Cancel</Button>
            </Box>
        </Box>
    );
}

export default NewMembership;
