/**
 * @generated SignedSource<<45108c84256a966a30eb902ef25aacef>>
 * @lightSyntaxTransform
 * @nogrep
 */

/* tslint:disable */
/* eslint-disable */
// @ts-nocheck

import { ConcreteRequest, Query } from 'relay-runtime';
export type ScopeType = "GLOBAL" | "ORGANIZATION" | "PROJECT" | "%future added value";
export type ServiceAccountAutocompleteQuery$variables = {
  organizationId?: string | null | undefined;
  projectId?: string | null | undefined;
  scopes?: ReadonlyArray<ScopeType> | null | undefined;
  search: string;
};
export type ServiceAccountAutocompleteQuery$data = {
  readonly serviceAccounts: {
    readonly edges: ReadonlyArray<{
      readonly node: {
        readonly description: string;
        readonly id: string;
        readonly name: string;
        readonly scope: ScopeType;
      } | null | undefined;
    } | null | undefined> | null | undefined;
  };
};
export type ServiceAccountAutocompleteQuery = {
  response: ServiceAccountAutocompleteQuery$data;
  variables: ServiceAccountAutocompleteQuery$variables;
};

const node: ConcreteRequest = (function(){
var v0 = [
  {
    "defaultValue": null,
    "kind": "LocalArgument",
    "name": "organizationId"
  },
  {
    "defaultValue": null,
    "kind": "LocalArgument",
    "name": "projectId"
  },
  {
    "defaultValue": null,
    "kind": "LocalArgument",
    "name": "scopes"
  },
  {
    "defaultValue": null,
    "kind": "LocalArgument",
    "name": "search"
  }
],
v1 = [
  {
    "alias": null,
    "args": [
      {
        "kind": "Literal",
        "name": "first",
        "value": 50
      },
      {
        "kind": "Variable",
        "name": "organizationId",
        "variableName": "organizationId"
      },
      {
        "kind": "Variable",
        "name": "projectId",
        "variableName": "projectId"
      },
      {
        "kind": "Variable",
        "name": "scopes",
        "variableName": "scopes"
      },
      {
        "kind": "Variable",
        "name": "search",
        "variableName": "search"
      }
    ],
    "concreteType": "ServiceAccountConnection",
    "kind": "LinkedField",
    "name": "serviceAccounts",
    "plural": false,
    "selections": [
      {
        "alias": null,
        "args": null,
        "concreteType": "ServiceAccountEdge",
        "kind": "LinkedField",
        "name": "edges",
        "plural": true,
        "selections": [
          {
            "alias": null,
            "args": null,
            "concreteType": "ServiceAccount",
            "kind": "LinkedField",
            "name": "node",
            "plural": false,
            "selections": [
              {
                "alias": null,
                "args": null,
                "kind": "ScalarField",
                "name": "id",
                "storageKey": null
              },
              {
                "alias": null,
                "args": null,
                "kind": "ScalarField",
                "name": "name",
                "storageKey": null
              },
              {
                "alias": null,
                "args": null,
                "kind": "ScalarField",
                "name": "scope",
                "storageKey": null
              },
              {
                "alias": null,
                "args": null,
                "kind": "ScalarField",
                "name": "description",
                "storageKey": null
              }
            ],
            "storageKey": null
          }
        ],
        "storageKey": null
      }
    ],
    "storageKey": null
  }
];
return {
  "fragment": {
    "argumentDefinitions": (v0/*: any*/),
    "kind": "Fragment",
    "metadata": null,
    "name": "ServiceAccountAutocompleteQuery",
    "selections": (v1/*: any*/),
    "type": "Query",
    "abstractKey": null
  },
  "kind": "Request",
  "operation": {
    "argumentDefinitions": (v0/*: any*/),
    "kind": "Operation",
    "name": "ServiceAccountAutocompleteQuery",
    "selections": (v1/*: any*/)
  },
  "params": {
    "cacheID": "7477850d854d26079f843a87c75a0a1b",
    "id": null,
    "metadata": {},
    "name": "ServiceAccountAutocompleteQuery",
    "operationKind": "query",
    "text": "query ServiceAccountAutocompleteQuery(\n  $organizationId: String\n  $projectId: String\n  $scopes: [ScopeType!]\n  $search: String!\n) {\n  serviceAccounts(first: 50, search: $search, organizationId: $organizationId, projectId: $projectId, scopes: $scopes) {\n    edges {\n      node {\n        id\n        name\n        scope\n        description\n      }\n    }\n  }\n}\n"
  }
};
})();

(node as any).hash = "02562e1d14c88398a2a125f3cf974593";

export default node;
