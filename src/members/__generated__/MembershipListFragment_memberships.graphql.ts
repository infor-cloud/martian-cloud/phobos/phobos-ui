/**
 * @generated SignedSource<<c92661991856fc590776a18d6b6dbbc8>>
 * @lightSyntaxTransform
 * @nogrep
 */

/* tslint:disable */
/* eslint-disable */
// @ts-nocheck

import { Fragment, ReaderFragment } from 'relay-runtime';
import { FragmentRefs } from "relay-runtime";
export type MembershipListFragment_memberships$data = {
  readonly edges: ReadonlyArray<{
    readonly node: {
      readonly id: string;
      readonly " $fragmentSpreads": FragmentRefs<"MembershipListItemFragment_membership">;
    } | null | undefined;
  } | null | undefined> | null | undefined;
  readonly " $fragmentType": "MembershipListFragment_memberships";
};
export type MembershipListFragment_memberships$key = {
  readonly " $data"?: MembershipListFragment_memberships$data;
  readonly " $fragmentSpreads": FragmentRefs<"MembershipListFragment_memberships">;
};

const node: ReaderFragment = {
  "argumentDefinitions": [],
  "kind": "Fragment",
  "metadata": null,
  "name": "MembershipListFragment_memberships",
  "selections": [
    {
      "alias": null,
      "args": null,
      "concreteType": "MembershipEdge",
      "kind": "LinkedField",
      "name": "edges",
      "plural": true,
      "selections": [
        {
          "alias": null,
          "args": null,
          "concreteType": "Membership",
          "kind": "LinkedField",
          "name": "node",
          "plural": false,
          "selections": [
            {
              "alias": null,
              "args": null,
              "kind": "ScalarField",
              "name": "id",
              "storageKey": null
            },
            {
              "args": null,
              "kind": "FragmentSpread",
              "name": "MembershipListItemFragment_membership"
            }
          ],
          "storageKey": null
        }
      ],
      "storageKey": null
    }
  ],
  "type": "MembershipConnection",
  "abstractKey": null
};

(node as any).hash = "3bda84bc3931566267332a33e0048550";

export default node;
