import { Box, Paper, Table, TableBody, TableCell, TableContainer, TableHead, TableRow, Typography, useTheme } from '@mui/material';
import { LoadMoreFn, useFragment } from 'react-relay/hooks';
import ListSkeleton from '../skeletons/ListSkeleton';
import InfiniteScroll from 'react-infinite-scroll-component';
import graphql from 'babel-plugin-relay/macro';
import MembershipListItem from './MembershipListItem';
import { MembershipListFragment_memberships$key } from './__generated__/MembershipListFragment_memberships.graphql';
import { OrganizationMembershipsPaginationQuery } from '../organizations/members/__generated__/OrganizationMembershipsPaginationQuery.graphql';
import { ProjectMembershipsPaginationQuery } from '../organizations/projects/members/__generated__/ProjectMembershipsPaginationQuery.graphql';

interface Props {
    fragmentRef: MembershipListFragment_memberships$key
    orgName: string
    hideSourceCol?: boolean
    loadNext: LoadMoreFn<OrganizationMembershipsPaginationQuery | ProjectMembershipsPaginationQuery>
    hasNext: boolean
    onDelete: (membership: any) => void
}

function MembershipList({ fragmentRef, hideSourceCol, loadNext, hasNext, onDelete, orgName }: Props) {
    const theme = useTheme();

    const data = useFragment<MembershipListFragment_memberships$key>(graphql`
        fragment MembershipListFragment_memberships on MembershipConnection {
            edges {
                node {
                    id
                    ...MembershipListItemFragment_membership
                }
            }
        }
    `, fragmentRef);

    const edges = data?.edges ?? [];

    if (edges.length > 0) {
        return (
            <Box>
                <Box>
                    <Paper sx={{ borderBottomLeftRadius: 0, borderBottomRightRadius: 0, border: `1px solid ${theme.palette.divider}` }}>
                        <Box padding={2} display="flex" alignItems="center" justifyContent="space-between">
                            <Typography variant="subtitle1">{edges.length} member{edges.length === 1 ? '' : 's'}</Typography>
                        </Box>
                    </Paper>
                    <InfiniteScroll
                        dataLength={edges.length ?? 0}
                        next={() => loadNext(20)}
                        hasMore={hasNext}
                        loader={<ListSkeleton rowCount={3} />}
                    >
                        <TableContainer>
                            <Table
                                sx={{
                                    minWidth: 650,
                                    borderCollapse: 'separate',
                                    borderSpacing: 0,
                                    'td, th': {
                                        borderBottom: `1px solid ${theme.palette.divider}`,
                                    },
                                    'td:first-of-type, th:first-of-type': {
                                        borderLeft: `1px solid ${theme.palette.divider}`
                                    },
                                    'td:last-of-type, th:last-of-type': {
                                        borderRight: `1px solid ${theme.palette.divider}`
                                    },
                                    'tr:last-of-type td:first-of-type': {
                                        borderBottomLeftRadius: 4,
                                    },
                                    'tr:last-of-type td:last-of-type': {
                                        borderBottomRightRadius: 4
                                    }
                                }}
                                aria-label="memberships">
                                <colgroup>
                                    <Box component="col" />
                                    <Box component="col" />
                                    <Box component="col" sx={{ width: "200px" }} />
                                    <Box component="col" />
                                    {!hideSourceCol && <Box component="col" />}
                                    <Box component="col" />
                                </colgroup>
                                <TableHead>
                                    <TableRow>
                                        <TableCell>Name</TableCell>
                                        <TableCell>Type</TableCell>
                                        <TableCell>Role</TableCell>
                                        <TableCell>Last Updated</TableCell>
                                        {!hideSourceCol && <TableCell>Source</TableCell>}
                                        <TableCell></TableCell>
                                    </TableRow>
                                </TableHead>
                                <TableBody>
                                    {edges.map((membership: any) => <MembershipListItem
                                        key={membership.node.id}
                                        orgName={orgName}
                                        fragmentRef={membership.node}
                                        onDelete={onDelete}
                                        hideSourceCol={hideSourceCol}
                                    />)}
                                </TableBody>
                            </Table>
                        </TableContainer>
                    </InfiniteScroll>
                </Box>
            </Box>
        );
    } else {
        return (
            <Box sx={{ display: "flex", justifyContent: "center", p: 4 }}>
                <Typography color="textSecondary" align="center">No members in this {hideSourceCol ? 'organization' : 'project'}.</Typography>
            </Box>
        );
    }
}

export default MembershipList
