import { Box, Chip, Typography, useTheme } from '@mui/material';
import Autocomplete from '@mui/material/Autocomplete';
import CircularProgress from '@mui/material/CircularProgress';
import TextField from '@mui/material/TextField';
import match from 'autosuggest-highlight/match';
import parse from 'autosuggest-highlight/parse';
import graphql from 'babel-plugin-relay/macro';
import throttle from 'lodash.throttle';
import { useParams } from 'react-router-dom';
import React, { useEffect, useState } from 'react';
import { fetchQuery, useRelayEnvironment } from 'react-relay/hooks';
import { ServiceAccountAutocompleteQuery } from './__generated__/ServiceAccountAutocompleteQuery.graphql';

export interface ServiceAccountOption {
    readonly id: string;
    readonly name: string;
    readonly scope: string;
    readonly description: string
}

interface Props {
    orgId?: string
    projectId?: string
    onSelected: (value: ServiceAccountOption | null) => void
}

function ServiceAccountAutocomplete({ orgId, projectId, onSelected }: Props) {
    const orgName = useParams().orgName as string;
    const [options, setOptions] = useState<ReadonlyArray<ServiceAccountOption> | null>(null);
    const [loading, setLoading] = useState<boolean>(false);
    const [inputValue, setInputValue] = useState('');
    const theme = useTheme();

    const environment = useRelayEnvironment();

    const fetch = React.useMemo(
        () =>
            throttle(
                (
                    request: { input: string },
                    callback: (results?: readonly ServiceAccountOption[]) => void,
                ) => {
                    fetchQuery<ServiceAccountAutocompleteQuery>(
                        environment,
                        graphql`
                            query ServiceAccountAutocompleteQuery($organizationId: String, $projectId: String, $scopes: [ScopeType!], $search: String!) {
                                serviceAccounts(first: 50, search: $search, organizationId: $organizationId, projectId: $projectId, scopes: $scopes) {
                                    edges {
                                        node {
                                            id
                                            name
                                            scope
                                            description
                                        }
                                    }
                                }
                            }
                        `,
                        {
                            search: request.input,
                            organizationId: orgId ?? null,
                            projectId: projectId ?? null,
                            scopes: orgId ? ['ORGANIZATION'] : ['ORGANIZATION', 'PROJECT'],
                        },
                        { fetchPolicy: 'network-only' }
                    ).toPromise().then(async response => {
                        const options = response?.serviceAccounts?.edges?.map(edge => edge?.node as ServiceAccountOption);
                        callback(options);
                    });
                },
                300,
            ),
        [environment, orgName],
    );

    useEffect(() => {
        let active = true;

        setLoading(true);

        fetch({ input: inputValue }, (results?: readonly ServiceAccountOption[]) => {
            if (active) {
                setOptions(results ?? []);
                setLoading(false);
            }
        });

        return () => {
            active = false;
        };
    }, [fetch, inputValue]);

    return (
        <Autocomplete
            fullWidth
            size="small"
            onChange={(event: React.SyntheticEvent, value: ServiceAccountOption | null) => onSelected(value)}
            onInputChange={(_, newInputValue: string) => setInputValue(newInputValue)}
            filterOptions={(x) => x}
            isOptionEqualToValue={(option: ServiceAccountOption, value: ServiceAccountOption) => option.id === value.id}
            getOptionLabel={(option: ServiceAccountOption) => option.name}
            renderOption={(props: React.HTMLAttributes<HTMLLIElement>, option: ServiceAccountOption, { inputValue }) => {
                const matches = match(option.name, inputValue);
                const parts = parse(option.name, matches);
                return (
                    <Box component="li" {...props}>
                        <Box sx={{ display: "flex", alignItems: "center" }}>
                            <Box>
                                <Typography>
                                    {parts.map((part: any, index: number) => (
                                        <span
                                            key={index}
                                            style={{
                                                fontWeight: part.highlight ? 700 : 400,
                                            }}
                                        >
                                            {part.text}
                                        </span>
                                    ))}
                                </Typography>
                                <Typography variant="caption">{option.description}</Typography>
                            </Box>
                            <Box>{projectId && option.scope === 'ORGANIZATION' && <Chip
                                sx={{ ml: 1, color: theme.palette.text.secondary }}
                                size="xs"
                                label='inherited from org'
                            />}
                            </Box>
                        </Box>
                    </Box>
                );
            }}
            options={options ?? []}
            loading={loading}
            renderInput={(params) => (
                <TextField
                    {...params}
                    placeholder='Select a service account'
                    InputProps={{
                        ...params.InputProps,
                        endAdornment: (
                            <React.Fragment>
                                {loading ? <CircularProgress color="inherit" size={20} /> : null}
                                {params.InputProps.endAdornment}
                            </React.Fragment>
                        ),
                    }}
                />
            )}
        />
    );
}

export default ServiceAccountAutocomplete;
