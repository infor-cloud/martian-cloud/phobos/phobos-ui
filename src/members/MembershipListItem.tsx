import React, { useState } from 'react';
import { Avatar, Box, Button, Stack, TableCell, TableRow } from '@mui/material';
import DeleteIcon from '@mui/icons-material/CloseOutlined';
import EditIcon from '@mui/icons-material/EditOutlined';
import moment from 'moment';
import purple from '@mui/material/colors/purple'
import { useSnackbar } from 'notistack';
import Gravatar from '../common/Gravatar';
import Link from '../routes/Link';
import graphql from 'babel-plugin-relay/macro';
import RoleAutocomplete, { RoleOption } from '../common/RoleAutocomplete';
import LoadingButton from '@mui/lab/LoadingButton';
import PRNButton from '../common/PRNButton';
import { useMutation, useFragment } from 'react-relay/hooks';
import { MembershipListItemFragment_membership$key } from './__generated__/MembershipListItemFragment_membership.graphql';
import { MembershipListItemUpdateMembershipMutation } from './__generated__/MembershipListItemUpdateMembershipMutation.graphql';

interface Props {
    orgName: string
    fragmentRef: MembershipListItemFragment_membership$key
    onDelete: (membership: any) => void
    hideSourceCol?: boolean
}

function MembershipListItem({ fragmentRef, onDelete, hideSourceCol, orgName }: Props) {
    const { enqueueSnackbar } = useSnackbar();

    const data = useFragment<MembershipListItemFragment_membership$key>(graphql`
        fragment MembershipListItemFragment_membership on Membership {
            id
            scope
            metadata {
                prn
                createdAt
                updatedAt
            }
            role {
                name
            }
            member {
                __typename
                ...on User {
                    id
                    username
                    email
                }
                __typename
                ...on Team {
                    id
                    name
                }
                __typename
                ...on ServiceAccount {
                    id
                    name
                }
            }
        }`, fragmentRef);

    const [commitUpdateMembership, updateInFlight] = useMutation<MembershipListItemUpdateMembershipMutation>(graphql`
        mutation MembershipListItemUpdateMembershipMutation($input: UpdateMembershipInput!) {
            updateMembership(input: $input) {
                membership {
                    ...MembershipListItemFragment_membership
                }
                problems {
                    message
                    field
                    type
                }
            }
        }
    `);

    const [editMode, setEditMode] = useState(false);
    const [role, setRole] = useState(data.role.name);

    const onSave = () => {
        commitUpdateMembership({
            variables: {
                input: {
                    id: data.id,
                    role
                },
            },
            onCompleted: data => {
                if (data.updateMembership.problems.length) {
                    enqueueSnackbar(data.updateMembership.problems.map(problem => problem.message).join('; '), { variant: 'warning' });
                } else {
                    setEditMode(false);
                }
            },
            onError: error => {
                enqueueSnackbar(`Unexpected error occurred: ${error.message}`, { variant: 'error' });
            }
        });
    };

    const memberType = data.member?.__typename;
    const isDirectMember = (hideSourceCol && data.scope == 'ORGANIZATION') || (!hideSourceCol && data.scope == 'PROJECT');

    return (
        <TableRow sx={{ height: '75px' }}>
            <TableCell sx={{ fontWeight: 'bold' }}>
                <Stack direction="row" alignItems="center" spacing={1}>
                    {memberType === 'User' && <React.Fragment>
                        <Gravatar width={24} height={24} sx={{ marginRight: 1 }} email={data.member?.email ?? ''} />
                        <Box>{data.member?.username}</Box>
                    </React.Fragment>}
                    {memberType === 'Team' && <React.Fragment>
                        <Avatar variant="rounded" sx={{ width: 24, height: 24, bgcolor: purple[300], fontSize: 14, marginRight: 1 }}>
                            {(data.member?.name ?? '')[0].toUpperCase()}
                        </Avatar>
                        <Box>
                            <Link color="inherit" to={`../../teams/${data.member?.name}`}>
                                {data.member?.name}
                            </Link>
                        </Box>
                    </React.Fragment>}
                    {memberType === 'ServiceAccount' && <React.Fragment>
                        <Avatar variant="rounded" sx={{ width: 24, height: 24, bgcolor: purple[300], fontSize: 14, marginRight: 1 }}>
                            {data.member?.name[0].toUpperCase()}
                        </Avatar>
                        <Box>
                            <Link color="inherit" to={`../../organizations/${orgName}/-/service_accounts/${data.member?.id}`}>
                                {data.member?.name}
                            </Link>
                        </Box>
                    </React.Fragment>}
                </Stack>
            </TableCell>
            <TableCell>
                {data.member?.__typename === 'ServiceAccount' ? 'Service Account' : data.member?.__typename}
            </TableCell>
            <TableCell>
                {editMode ? <RoleAutocomplete onSelected={(role: RoleOption | null) => role && setRole(role.name)} /> : <React.Fragment>{data.role.name}</React.Fragment>}
            </TableCell>
            <TableCell>
                {moment(data.metadata.updatedAt as moment.MomentInput).fromNow()}
            </TableCell>
            {!hideSourceCol && <TableCell>{isDirectMember ? 'Direct Member' : 'Organization'}</TableCell>}
            <TableCell>{isDirectMember &&
                <Stack direction="row" spacing={1} display="flex" justifyContent="center">
                    {editMode ? <React.Fragment>
                        <LoadingButton
                            loading={updateInFlight}
                            onClick={onSave}
                            sx={{ minWidth: 40, padding: '3px' }}
                            size="small"
                            color="secondary"
                            variant="outlined">
                            Save
                        </LoadingButton>
                        <Button
                            onClick={() => setEditMode(false)}
                            sx={{ minWidth: 40, padding: '3px' }}
                            size="small"
                            color="info"
                            variant="outlined">
                            Cancel
                        </Button>
                    </React.Fragment> : <React.Fragment>
                        <PRNButton prn={data.metadata.prn} />
                        <Button
                            onClick={() => setEditMode(true)}
                            sx={{ minWidth: 40, padding: '2px' }}
                            size="small"
                            color="info"
                            variant="outlined">
                            <EditIcon />
                        </Button>
                        <Button
                            onClick={() => onDelete(data)}
                            sx={{ minWidth: 40, padding: '2px' }}
                            size="small"
                            color="info"
                            variant="outlined">
                            <DeleteIcon />
                        </Button>
                    </React.Fragment>}
                </Stack>}
            </TableCell>
        </TableRow>
    );
}

export default MembershipListItem
