import { Box, List, Paper, Typography, useTheme } from '@mui/material';
import graphql from 'babel-plugin-relay/macro';
import InfiniteScroll from 'react-infinite-scroll-component';
import { LoadMoreFn, useFragment } from 'react-relay/hooks';
import ListSkeleton from '../skeletons/ListSkeleton';
import ReleaseLifecycleListItem from './ReleaseLifecycleListItem';
import { OrgReleaseLifecyclesPaginationQuery } from '../organizations/releaselifecycles/__generated__/OrgReleaseLifecyclesPaginationQuery.graphql';
import { ProjectReleaseLifecyclesPaginationQuery } from '../organizations/projects/releaselifecycles/__generated__/ProjectReleaseLifecyclesPaginationQuery.graphql';
import { ReleaseLifecycleListFragment_releaseLifecycles$key } from './__generated__/ReleaseLifecycleListFragment_releaseLifecycles.graphql';

interface Props {
    search: string | undefined
    isRefreshing: boolean
    loadNext: LoadMoreFn<OrgReleaseLifecyclesPaginationQuery | ProjectReleaseLifecyclesPaginationQuery>
    hasNext: boolean
    showOrgChip?: boolean
    fragmentRef: ReleaseLifecycleListFragment_releaseLifecycles$key
}

function ReleaseLifecycleList({ fragmentRef, isRefreshing, loadNext, hasNext, showOrgChip, search }: Props) {
    const theme = useTheme();

    const data = useFragment<ReleaseLifecycleListFragment_releaseLifecycles$key>(
        graphql`
            fragment ReleaseLifecycleListFragment_releaseLifecycles on ReleaseLifecycleConnection
            {
                totalCount
                edges {
                    node {
                        id
                        scope
                        ...ReleaseLifecycleListItemFragment_releaseLifecycle
                    }
                }
            }
    `, fragmentRef);

    return (
        <Box>
            <Paper sx={{ borderBottomLeftRadius: 0, borderBottomRightRadius: 0, border: `1px solid ${theme.palette.divider}` }}>
                <Box padding={2} display="flex" alignItems="center" justifyContent="space-between">
                    <Typography variant="subtitle1">
                        {data.totalCount} release lifecycle{data.totalCount === 1 ? '' : 's'}
                    </Typography>
                </Box>
            </Paper>
            {(data?.edges?.length === 0) && search !== '' && <Typography
                sx={{
                    padding: 4,
                    borderBottom: `1px solid ${theme.palette.divider}`,
                    borderLeft: `1px solid ${theme.palette.divider}`,
                    borderRight: `1px solid ${theme.palette.divider}`,
                    borderBottomLeftRadius: 4,
                    borderBottomRightRadius: 4
                }}
                align="center"
                color="textSecondary"
            >
                No release matching search <strong>{search}</strong>
            </Typography>}
            <InfiniteScroll
                dataLength={data?.edges?.length ?? 0}
                next={() => loadNext(20)}
                hasMore={hasNext}
                loader={<ListSkeleton rowCount={3} />}
            >
                <List sx={isRefreshing ? { opacity: 0.5 } : null} disablePadding>
                    {data?.edges?.map((edge: any) => <ReleaseLifecycleListItem
                        key={edge.node.id}
                        fragmentRef={edge.node}
                        showOrgChip={showOrgChip && edge.node.scope === 'ORGANIZATION'}
                    />)}
                </List>
            </InfiniteScroll>
        </Box>
    );
}

export default ReleaseLifecycleList
