import { useState } from 'react';
import { Box, Button, Typography } from '@mui/material';
import LoadingButton from '@mui/lab/LoadingButton';
import graphql from 'babel-plugin-relay/macro';
import { useLazyLoadQuery, useMutation } from 'react-relay/hooks';
import { Link as RouterLink, useNavigate, useParams } from 'react-router-dom';
import { MutationError } from '../common/error';
import ReleaseLifecycleForm, { FormData } from './ReleaseLifecycleForm';
import { EditReleaseLifecycleQuery } from './__generated__/EditReleaseLifecycleQuery.graphql';
import { EditReleaseLifecycle_LifecycleTemplateMutation } from './__generated__/EditReleaseLifecycle_LifecycleTemplateMutation.graphql';
import { EditReleaseLifecycle_ReleaseLifecycleMutation } from './__generated__/EditReleaseLifecycle_ReleaseLifecycleMutation.graphql';

interface Props {
    orgId?: string
    projectId?: string
}

function EditReleaseLifecycle({ orgId, projectId }: Props) {
    const releaseLifecycleId = useParams().releaseLifecycleId as string;
    const navigate = useNavigate();

    const queryData = useLazyLoadQuery<EditReleaseLifecycleQuery>(graphql`
        query EditReleaseLifecycleQuery($id: String!) {
            node(id: $id) {
                ... on ReleaseLifecycle {
                    metadata {
                        createdAt
                    }
                    id
                    name
                    scope
                    createdBy
                    lifecycleTemplate {
                        id
                        hclData
                    }
                }
            }
        }
    `, { id: releaseLifecycleId });

    const [commitLifecycleTemplate, isInFlight] = useMutation<EditReleaseLifecycle_LifecycleTemplateMutation>(graphql`
        mutation EditReleaseLifecycle_LifecycleTemplateMutation($input: CreateLifecycleTemplateInput!) {
            createLifecycleTemplate(input: $input) {
                lifecycleTemplate {
                    id
                    hclData
                }
                problems {
                    message
                    field
                    type
                }
            }
        }
    `);

    const [commitReleaseLifecycle] = useMutation<EditReleaseLifecycle_ReleaseLifecycleMutation>(graphql`
        mutation EditReleaseLifecycle_ReleaseLifecycleMutation($input: UpdateReleaseLifecycleInput!) {
            updateReleaseLifecycle(input: $input) {
                releaseLifecycle {
                    id
                    name
                    createdBy
                }
                problems {
                    message
                    field
                    type
                }
            }
        }
    `);

    const releaseLifecycle = queryData.node as any;

    const [error, setError] = useState<MutationError>();
    const [formData, setFormData] = useState<FormData>({
        name: releaseLifecycle.name,
        hclData: releaseLifecycle.lifecycleTemplate.hclData,
    });

    const onUpdate = () => {
        if (queryData.node?.lifecycleTemplate?.hclData !== formData.hclData) {

            commitLifecycleTemplate({
                variables: {
                    input: {
                        hclData: formData.hclData || '',
                        scope: orgId ? 'ORGANIZATION' : 'PROJECT',
                        organizationId: orgId,
                        projectId: projectId
                    }
                },
                onCompleted: data => {
                    if (data.createLifecycleTemplate.problems.length) {
                        setError({
                            severity: 'warning',
                            message: data.createLifecycleTemplate.problems.map((problem: any) => problem.message).join('; ')
                        });
                    } else if (!data.createLifecycleTemplate.lifecycleTemplate) {
                        setError({
                            severity: 'error',
                            message: "Unexpected error occurred"
                        });
                    } else {
                        commitReleaseLifecycle({
                            variables: {
                                input: {
                                    id: releaseLifecycleId,
                                    lifecycleTemplateId: data.createLifecycleTemplate.lifecycleTemplate.id,
                                }
                            },
                            onCompleted: data => {
                                if (data.updateReleaseLifecycle.problems.length) {
                                    setError({
                                        severity: 'warning',
                                        message: data.updateReleaseLifecycle.problems.map((problem: any) => problem.message).join('; ')
                                    });
                                } else if (!data.updateReleaseLifecycle.releaseLifecycle) {
                                    setError({
                                        severity: 'error',
                                        message: "Unexpected error occurred"
                                    });
                                } else {
                                    navigate(`../`);
                                }
                            }
                        });
                    }
                },
                onError: error => {
                    setError({
                        severity: 'error',
                        message: `Unexpected error occurred: ${error.message}`
                    });
                }
            });
        }
    };

    return (
        <Box>
            <Typography variant="h5">Edit Release Lifecycle</Typography>
            <ReleaseLifecycleForm
                editMode
                data={formData}
                onChange={(data: FormData) => setFormData(data)}
                error={error}
            />
            <Box mt={2}>
                <LoadingButton
                    disabled={!formData.hclData}
                    loading={isInFlight}
                    variant="outlined"
                    color="primary"
                    sx={{ mr: 2 }}
                    onClick={onUpdate}
                >
                    Update Release Lifecycle
                </LoadingButton>
                <Button component={RouterLink} color="inherit" to={-1 as any}>Cancel</Button>
            </Box>
        </Box>
    );
}

export default EditReleaseLifecycle
