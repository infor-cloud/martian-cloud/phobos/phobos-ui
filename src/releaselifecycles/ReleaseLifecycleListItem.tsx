import { Box, Chip, ListItemButton, ListItemText, Typography, useTheme } from '@mui/material';
import graphql from 'babel-plugin-relay/macro';
import moment from 'moment';
import { useFragment } from 'react-relay/hooks';
import { Link as RouterLink } from 'react-router-dom';
import { ReleaseLifecycleListItemFragment_releaseLifecycle$key } from './__generated__/ReleaseLifecycleListItemFragment_releaseLifecycle.graphql';

interface Props {
    showOrgChip?: boolean
    fragmentRef: ReleaseLifecycleListItemFragment_releaseLifecycle$key
}

function ReleaseLifecycleListItem({ fragmentRef, showOrgChip }: Props) {
    const theme = useTheme();

    const data = useFragment<ReleaseLifecycleListItemFragment_releaseLifecycle$key>(graphql`
        fragment ReleaseLifecycleListItemFragment_releaseLifecycle on ReleaseLifecycle {
            id
            name
            scope
            metadata {
                updatedAt
            }
            organizationName
            project {
                name
            }
        }
    `, fragmentRef);

const baseLink = `/organizations/${data.organizationName}`;
const route = data.scope === 'PROJECT' && data.project ? `${baseLink}/projects/${data.project.name}/-/release_lifecycles/${data.id}` : `${baseLink}/-/release_lifecycles/${data.id}`;

    return (
        <ListItemButton
            component={RouterLink}
            to={route}
            sx={{
                paddingY: 1,
                borderBottom: `1px solid ${theme.palette.divider}`,
                borderLeft: `1px solid ${theme.palette.divider}`,
                borderRight: `1px solid ${theme.palette.divider}`,
                '&:last-child': {
                    borderBottomLeftRadius: 4,
                    borderBottomRightRadius: 4
                }
            }}>
            <ListItemText
                primary={
                    <Box sx={{ display: "flex", alignItems: "center" }}>
                        <Typography fontWeight={500}>{data.name}</Typography>
                        {showOrgChip && <Chip
                            sx={{ ml: 1, color: theme.palette.text.secondary }}
                            size="xs"
                            label='inherited from org'
                        />}
                    </Box>
                }
            />
            <Typography variant="body2" color="textSecondary">
                {moment(data.metadata.updatedAt as moment.MomentInput).fromNow()}
            </Typography>
        </ListItemButton>
    );
}

export default ReleaseLifecycleListItem
