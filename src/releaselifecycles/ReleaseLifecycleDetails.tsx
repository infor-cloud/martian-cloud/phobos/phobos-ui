import React, { useState } from 'react';
import CopyIcon from '@mui/icons-material/ContentCopy';
import { Prism as SyntaxHighlighter } from 'react-syntax-highlighter';
import { atomDark as prismTheme } from 'react-syntax-highlighter/dist/esm/styles/prism';
import { default as ArrowDropDownIcon } from '@mui/icons-material/ArrowDropDown';
import { LoadingButton } from '@mui/lab';
import { Box, Button, ButtonGroup, Dialog, DialogActions, DialogContent, DialogTitle, IconButton, Menu, MenuItem, Stack, Tab, Tabs, Typography } from '@mui/material';
import graphql from 'babel-plugin-relay/macro';
import { useSnackbar } from 'notistack';
import { useLazyLoadQuery, useMutation } from 'react-relay/hooks';
import { useNavigate, useParams, useSearchParams } from 'react-router-dom';
import PRNButton from '../common/PRNButton';
import { ReleaseLifecycleDetailsQuery } from './__generated__/ReleaseLifecycleDetailsQuery.graphql';
import { ReleaseLifecycleDetailsDeleteMutation } from './__generated__/ReleaseLifecycleDetailsDeleteMutation.graphql';

interface ConfirmationDialogProps {
    releaseLifecycleName: string
    deleteInProgress: boolean;
    keepMounted: boolean;
    open: boolean;
    onClose: (confirm?: boolean) => void
}

function DeleteConfirmationDialog(props: ConfirmationDialogProps) {
    const { releaseLifecycleName, deleteInProgress, onClose, open, ...other } = props;
    return (
        <Dialog
            maxWidth="xs"
            open={open}
            {...other}
        >
            <DialogTitle>Delete Release Lifecycle</DialogTitle>
            <DialogContent dividers>
                Are you sure you want to delete release lifecycle <strong>{releaseLifecycleName}</strong>?
            </DialogContent>
            <DialogActions>
                <Button color="inherit" onClick={() => onClose()}>
                    Cancel
                </Button>
                <LoadingButton color="error" loading={deleteInProgress} onClick={() => onClose(true)}>
                    Delete
                </LoadingButton>
            </DialogActions>
        </Dialog>
    );
}

interface Props {
    getConnections: () => [string]
}

function ReleaseLifecycleDetails({ getConnections }: Props) {
    const releaseLifecycleId = useParams().releaseLifecycleId as string;
    const [searchParams, setSearchParams] = useSearchParams();
    const navigate = useNavigate();
    const { enqueueSnackbar } = useSnackbar();
    const [menuAnchorEl, setMenuAnchorEl] = useState<Element | null>(null);
    const [showDeleteConfirmationDialog, setShowDeleteConfirmationDialog] = useState<boolean>(false);

    const tab = searchParams.get('tab') || 'template';

    const data = useLazyLoadQuery<ReleaseLifecycleDetailsQuery>(graphql`
        query ReleaseLifecycleDetailsQuery($id: String!) {
            node(id: $id) {
                ... on ReleaseLifecycle {
                    id
                    name
                    metadata {
                        prn
                    }
                    lifecycleTemplate {
                        id
                        hclData
                    }
                }
            }
        }
    `, { id: releaseLifecycleId }, { fetchPolicy: 'store-and-network' });

    const [commit, commitInFlight] = useMutation<ReleaseLifecycleDetailsDeleteMutation>(graphql`
        mutation ReleaseLifecycleDetailsDeleteMutation($input: DeleteReleaseLifecycleInput!, $connections: [ID!]!) {
            deleteReleaseLifecycle(input: $input) {
                releaseLifecycle {
                    id @deleteEdge(connections: $connections)
                }
                problems {
                    message
                    field
                    type
                }
            }
        }
    `);

    const onTabChange = (event: React.SyntheticEvent, newValue: string) => {
        searchParams.set('tab', newValue);
        setSearchParams(searchParams, { replace: true });
    };

    const onDeleteConfirmationDialogClosed = (confirm?: boolean) => {
        if (confirm) {
            commit({
                variables: {
                    input: {
                        id: releaseLifecycleId
                    },
                    connections: getConnections(),
                },
                onCompleted: data => {
                    setShowDeleteConfirmationDialog(false);

                    if (data.deleteReleaseLifecycle.problems.length) {
                        enqueueSnackbar(data.deleteReleaseLifecycle.problems.map((problem: any) => problem.message).join('; '), { variant: 'warning' });
                    } else {
                        navigate(`..`);
                    }
                },
                onError: error => {
                    setShowDeleteConfirmationDialog(false);
                    enqueueSnackbar(`Unexpected error occurred: ${error.message}`, { variant: 'error' });
                }
            });
        } else {
            setShowDeleteConfirmationDialog(false);
        }
    };

    const onOpenMenu = (event: React.MouseEvent<HTMLButtonElement>) => {
        setMenuAnchorEl(event.currentTarget);
    };

    const onMenuClose = () => {
        setMenuAnchorEl(null);
    };

    const onMenuAction = (actionCallback: () => void) => {
        setMenuAnchorEl(null);
        actionCallback();
    };

    const releaseLifecycle = data.node as any;

    if (data.node) {
        return (
            <Box>
                <Box display="flex" justifyContent="space-between" mb={2}>
                    <Box display="flex" alignItems="center">
                        <Typography variant="h5" mr={1}>{releaseLifecycle.name}</Typography>
                    </Box>
                    <Box sx={{ display: "flex" }}>
                        <Stack direction="row" spacing={1}>
                            <PRNButton prn={releaseLifecycle.metadata.prn} />
                            <ButtonGroup variant="outlined" color="primary">
                                <Button onClick={() => navigate('edit')}>Edit</Button>
                                <Button
                                    color="primary"
                                    size="small"
                                    aria-label="more options menu"
                                    aria-haspopup="menu"
                                    onClick={onOpenMenu}
                                >
                                    <ArrowDropDownIcon fontSize="small" />
                                </Button>
                            </ButtonGroup>
                        </Stack>
                        <Menu
                            id="lifecycle-more-options-menu"
                            anchorEl={menuAnchorEl}
                            open={Boolean(menuAnchorEl)}
                            onClose={onMenuClose}
                            anchorOrigin={{
                                vertical: 'bottom',
                                horizontal: 'right',
                            }}
                            transformOrigin={{
                                vertical: 'top',
                                horizontal: 'right',
                            }}
                        >
                            <MenuItem onClick={() => onMenuAction(() => setShowDeleteConfirmationDialog(true))}>
                                Delete Release Lifecycle
                            </MenuItem>
                        </Menu>
                    </Box>
                </Box>
                <Box sx={{
                    display: "flex",
                    border: 1, borderTopLeftRadius: 4,
                    borderTopRightRadius: 4,
                    borderColor: 'divider'
                }}>
                    <Tabs value={tab} onChange={onTabChange}>
                        <Tab label="Template" value="template" />
                    </Tabs>
                </Box>
                <Box sx={{ border: 1, borderTop: 0, borderBottomLeftRadius: 4, borderBottomRightRadius: 4, borderColor: 'divider', padding: 2 }}>
                    {tab === 'template' &&
                        <Box position="relative">
                            <IconButton
                                sx={{
                                    padding: 2,
                                    position: 'absolute',
                                    top: 0,
                                    right: 0
                                }}
                                onClick={() => navigator.clipboard.writeText(releaseLifecycle.lifecycleTemplate.hclData)}>
                                <CopyIcon sx={{ width: 16, height: 16 }} />
                            </IconButton>
                            <SyntaxHighlighter
                                wrapLongLines
                                customStyle={{ fontSize: 13 }}
                                language="hcl"
                                style={prismTheme}
                                children={releaseLifecycle.lifecycleTemplate.hclData}
                            />
                        </Box>
                    }
                </Box>
                <DeleteConfirmationDialog
                    releaseLifecycleName={releaseLifecycle.name}
                    keepMounted
                    deleteInProgress={commitInFlight}
                    open={showDeleteConfirmationDialog}
                    onClose={onDeleteConfirmationDialogClosed}
                />
            </Box>
        );
    } else {
        return <Box display="flex" justifyContent="center" marginTop={4}>
            <Typography color="textSecondary">Release Lifecycle with ID {releaseLifecycleId} not found</Typography>
        </Box>;
    }
}

export default ReleaseLifecycleDetails
