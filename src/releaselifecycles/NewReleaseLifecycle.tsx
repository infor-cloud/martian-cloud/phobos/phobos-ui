import { useState } from 'react';
import { Box, Button, Typography } from '@mui/material';
import LoadingButton from '@mui/lab/LoadingButton';
import graphql from 'babel-plugin-relay/macro';
import { useMutation } from 'react-relay/hooks';
import { Link as RouterLink, useNavigate } from 'react-router-dom';
import { MutationError } from '../common/error';
import LifecycleForm, { FormData } from './ReleaseLifecycleForm';
import { NewReleaseLifecycle_LifecycleTemplateMutation } from './__generated__/NewReleaseLifecycle_LifecycleTemplateMutation.graphql';
import { NewReleaseLifecycle_ReleaseLifecycle_Mutation } from './__generated__/NewReleaseLifecycle_ReleaseLifecycle_Mutation.graphql';


interface Props {
    orgId?: string
    projectId?: string
    getConnections: (id: string) => [string]
}

function NewReleaseLifecycle({ orgId, projectId, getConnections }: Props) {
    const navigate = useNavigate();

    const [commitLifecycleTemplate, isInFlight] = useMutation<NewReleaseLifecycle_LifecycleTemplateMutation>(graphql`
        mutation NewReleaseLifecycle_LifecycleTemplateMutation($input: CreateLifecycleTemplateInput!) {
            createLifecycleTemplate(input: $input) {
                lifecycleTemplate {
                    id
                    hclData
                }
                problems {
                    message
                    field
                    type
                }
            }
        }
    `);

    const [commitReleaseLifecycle] = useMutation<NewReleaseLifecycle_ReleaseLifecycle_Mutation>(graphql`
        mutation NewReleaseLifecycle_ReleaseLifecycle_Mutation($input: CreateReleaseLifecycleInput!, $connections: [ID!]!) {
            createReleaseLifecycle(input: $input) {
                # Use @prependNode to add the node to the connection
                releaseLifecycle  @prependNode(connections: $connections, edgeTypeName: "Lifecycle_ReleaseLifecycleEdge")  {
                    id
                    name
                    createdBy
                    ...ReleaseLifecycleListItemFragment_releaseLifecycle
                }
                problems {
                    message
                    field
                    type
                }
            }
        }
    `);

    const [error, setError] = useState<MutationError>()
    const [formData, setFormData] = useState<FormData>({
        name: '',
        hclData: "stage \"dev\" {\n  deployment {\n    environment = \"dev\"\n  }\n}\n\nstage \"prod\" {\n  deployment {\n    environment = \"prod\"\n  }\n}\n",
    });

    const disabled = formData.name === '' || !formData.hclData;

    const onSave = () => {

        commitLifecycleTemplate({
            variables: {
                input: {
                    hclData: formData.hclData || '',
                    scope: orgId ? 'ORGANIZATION' : 'PROJECT',
                    organizationId: orgId,
                    projectId: projectId
                }
            },
            onCompleted: data => {
                if (data.createLifecycleTemplate.problems.length) {
                    setError({
                        severity: 'warning',
                        message: data.createLifecycleTemplate.problems.map((problem: any) => problem.message).join('; ')
                    });
                } else if (!data.createLifecycleTemplate.lifecycleTemplate) {
                    setError({
                        severity: 'error',
                        message: "Unexpected error occurred"
                    });
                } else {
                    commitReleaseLifecycle({
                        variables: {
                            input: {
                                lifecycleTemplateId: data.createLifecycleTemplate.lifecycleTemplate.id,
                                name: formData.name,
                                scope: orgId ? 'ORGANIZATION' : 'PROJECT',
                                organizationId: orgId,
                                projectId: projectId
                            },
                            connections: getConnections(orgId || projectId || '')
                        },
                        onCompleted: data => {
                            if (data.createReleaseLifecycle.problems.length) {
                                setError({
                                    severity: 'warning',
                                    message: data.createReleaseLifecycle.problems.map((problem: any) => problem.message).join('; ')
                                });
                            } else if (!data.createReleaseLifecycle.releaseLifecycle) {
                                setError({
                                    severity: 'error',
                                    message: "Unexpected error occurred"
                                });
                            } else {
                                navigate(`../${data.createReleaseLifecycle.releaseLifecycle.id}`);
                            }
                        }
                    })
                }
            },
            onError: error => {
                setError({
                    severity: 'error',
                    message: `Unexpected error occurred: ${error.message}`
                });
            }
        });
    };

    return (
        <Box>
            <Typography variant="h5">New Release Lifecycle</Typography>
            <LifecycleForm
                data={formData}
                onChange={(data: FormData) => setFormData(data)}
                error={error}
            />
            <Box mt={2}>
                <LoadingButton
                    loading={isInFlight}
                    disabled={disabled}
                    variant="outlined"
                    color="primary"
                    sx={{ mr: 2 }}
                    onClick={onSave}
                >
                    Create Release Lifecycle
                </LoadingButton>
                <Button component={RouterLink} color="inherit" to={-1 as any}>Cancel</Button>
            </Box>
        </Box>
    );
}

export default NewReleaseLifecycle;
