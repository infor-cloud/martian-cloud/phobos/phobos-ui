import Editor from "@monaco-editor/react";
import DeleteIcon from '@mui/icons-material/Delete';
import { Alert, Box, Button, Divider, IconButton, List, ListItem, ListItemText, Stack, TextField, Typography, useTheme } from '@mui/material';
import { useState } from 'react';
import PanelButton from '../common/PanelButton';
import { MutationError } from "../common/error";

const EDITOR_HEIGHT = 500;

export interface FormData {
    name: string
    hclData: string | undefined
}

interface Props {
    data: FormData
    onChange: (data: FormData) => void
    editMode?: boolean
    error?: MutationError
}

interface OptionTypes {
    name: string
    title: string
    description: string
}

const OPTION_TYPES = [
    { name: 'editor', title: 'Enter HCL data', description: 'Create a release lifecycle by entering HCL data into a code editor' },
    { name: 'upload', title: 'Upload HCL file', description: 'Create a release lifecycle by uploading an HCL file' }
];

function LifecycleForm({ data, onChange, editMode, error }: Props) {
    const theme = useTheme();
    const [fileName, setFileName] = useState<string | undefined>(undefined);
    const [selection, setSelection] = useState<string | undefined>(editMode ? 'editor' : undefined);

    const handleFileChange = (event: React.ChangeEvent<HTMLInputElement>) => {
        const file = event.target.files?.[0];

        if (file) {
            const reader = new FileReader();
            setFileName(file.name);

            reader.onload = (e) => {
                const fileContents: string = e.target?.result as string;
                onChange({ ...data, hclData: fileContents });
            };
            reader.readAsText(file);
        }
    };

    const onOptionSelected = (option: OptionTypes) => {
        setSelection(option.name);
        setFileName(undefined);
    };

    return (
        <Box>
            {error && <Alert sx={{ mt: 2, mb: 2 }} severity={error.severity}>
                {error.message}
            </Alert>}
            <Box sx={{ mt: 2, mb: 2 }}>
                <Typography sx={{ mt: 2 }} variant="subtitle1" gutterBottom>Details</Typography>
                <Divider sx={{ opacity: 0.6 }}  />
                <Box sx={{ mt: 2, mb: 2 }}>
                    <TextField
                        disabled={editMode}
                        size="small"
                        fullWidth
                        label="Name"
                        value={data.name}
                        onChange={event => onChange({ ...data, name: event.target.value })}
                    />
                </Box>
                <Typography variant="subtitle1" gutterBottom>Select option</Typography>
                <Divider sx={{ opacity: 0.6 }}  />
                <Stack sx={{ mt: 2, mb: 2 }} direction="row" spacing={2}>
                    {OPTION_TYPES.map(option => <PanelButton
                        key={option.name}
                        selected={selection === option.name}
                        onClick={() => onOptionSelected(option)}>
                        <Typography variant="subtitle1">{option.title}</Typography>
                        <Typography variant="caption" align="center">
                            {option.description}
                        </Typography>
                    </PanelButton>)}
                </Stack>
                {selection === 'editor' && <Box>
                    {!editMode && <Typography variant="caption">Phobos has provided a template for a release lifecycle that can be edited as needed. A valid release lifecycle must contain at least one environment.
                    </Typography>}
                    <Box sx={{ mt: 2, mb: 4, height: EDITOR_HEIGHT, position: 'relative' }}>
                        <Box sx={{ position: 'absolute', top: 0, left: 0, width: '100%', height: '100%' }} >
                            <Editor
                                options={{
                                    fontSize: 12,
                                    scrollBeyondLastLine: false,
                                    automaticLayout: true,
                                    scrollbar: {
                                        vertical: 'auto',
                                        horizontal: 'auto'
                                    },
                                    minimap: {
                                        enabled: false
                                    }
                                }}
                                onMount={(editor, monaco) => {
                                    const model = editor.getModel();
                                    // Enforce only Line Feed '\n' since Windows will add a carriage return as well.
                                    model?.pushEOL(monaco.editor.EndOfLineSequence.LF);
                                }}
                                theme="vs-dark"
                                defaultLanguage="hcl"
                                value={data.hclData}
                                onChange={value => onChange({ ...data, hclData: value })}
                            />
                        </Box>
                    </Box>
                </Box>}
                {selection === 'upload' && <Box mb={4}>
                    <Typography variant="subtitle1" gutterBottom>Upload HCL file</Typography>
                    <Divider sx={{ opacity: 0.6 }} />
                    <Box mt={2}>
                        {fileName ?
                            <List sx={{ border: `1px solid ${theme.palette.divider}`, maxWidth: 400 }}>
                                <ListItem
                                    secondaryAction={
                                        <IconButton onClick={() => {
                                            onChange({ ...data, hclData: undefined })
                                            setFileName(undefined);
                                        }}>
                                            <DeleteIcon />
                                        </IconButton>
                                    }>
                                    <ListItemText primary={fileName} />
                                </ListItem>
                            </List>
                            :
                            <Button
                                variant="outlined"
                                component="label"
                                color="secondary"
                                size="small"
                            >
                                Upload File
                                <input
                                    type="file"
                                    accept=".hcl"
                                    id="hcl"
                                    style={{ display: "none" }}
                                    onChange={handleFileChange}
                                />
                            </Button>
                        }
                    </Box>
                </Box>}
            </Box>
            <Divider sx={{ opacity: 0.6 }} />
        </Box>
    );
}

export default LifecycleForm
