/**
 * @generated SignedSource<<54b879179355d9fb98e615b617d8dae4>>
 * @lightSyntaxTransform
 * @nogrep
 */

/* tslint:disable */
/* eslint-disable */
// @ts-nocheck

import { ConcreteRequest, Query } from 'relay-runtime';
export type ScopeType = "GLOBAL" | "ORGANIZATION" | "PROJECT" | "%future added value";
export type EditReleaseLifecycleQuery$variables = {
  id: string;
};
export type EditReleaseLifecycleQuery$data = {
  readonly node: {
    readonly createdBy?: string;
    readonly id?: string;
    readonly lifecycleTemplate?: {
      readonly hclData: string;
      readonly id: string;
    };
    readonly metadata?: {
      readonly createdAt: any;
    };
    readonly name?: string;
    readonly scope?: ScopeType;
  } | null | undefined;
};
export type EditReleaseLifecycleQuery = {
  response: EditReleaseLifecycleQuery$data;
  variables: EditReleaseLifecycleQuery$variables;
};

const node: ConcreteRequest = (function(){
var v0 = [
  {
    "defaultValue": null,
    "kind": "LocalArgument",
    "name": "id"
  }
],
v1 = [
  {
    "kind": "Variable",
    "name": "id",
    "variableName": "id"
  }
],
v2 = {
  "alias": null,
  "args": null,
  "concreteType": "ResourceMetadata",
  "kind": "LinkedField",
  "name": "metadata",
  "plural": false,
  "selections": [
    {
      "alias": null,
      "args": null,
      "kind": "ScalarField",
      "name": "createdAt",
      "storageKey": null
    }
  ],
  "storageKey": null
},
v3 = {
  "alias": null,
  "args": null,
  "kind": "ScalarField",
  "name": "id",
  "storageKey": null
},
v4 = {
  "alias": null,
  "args": null,
  "kind": "ScalarField",
  "name": "name",
  "storageKey": null
},
v5 = {
  "alias": null,
  "args": null,
  "kind": "ScalarField",
  "name": "scope",
  "storageKey": null
},
v6 = {
  "alias": null,
  "args": null,
  "kind": "ScalarField",
  "name": "createdBy",
  "storageKey": null
},
v7 = {
  "alias": null,
  "args": null,
  "concreteType": "LifecycleTemplate",
  "kind": "LinkedField",
  "name": "lifecycleTemplate",
  "plural": false,
  "selections": [
    (v3/*: any*/),
    {
      "alias": null,
      "args": null,
      "kind": "ScalarField",
      "name": "hclData",
      "storageKey": null
    }
  ],
  "storageKey": null
};
return {
  "fragment": {
    "argumentDefinitions": (v0/*: any*/),
    "kind": "Fragment",
    "metadata": null,
    "name": "EditReleaseLifecycleQuery",
    "selections": [
      {
        "alias": null,
        "args": (v1/*: any*/),
        "concreteType": null,
        "kind": "LinkedField",
        "name": "node",
        "plural": false,
        "selections": [
          {
            "kind": "InlineFragment",
            "selections": [
              (v2/*: any*/),
              (v3/*: any*/),
              (v4/*: any*/),
              (v5/*: any*/),
              (v6/*: any*/),
              (v7/*: any*/)
            ],
            "type": "ReleaseLifecycle",
            "abstractKey": null
          }
        ],
        "storageKey": null
      }
    ],
    "type": "Query",
    "abstractKey": null
  },
  "kind": "Request",
  "operation": {
    "argumentDefinitions": (v0/*: any*/),
    "kind": "Operation",
    "name": "EditReleaseLifecycleQuery",
    "selections": [
      {
        "alias": null,
        "args": (v1/*: any*/),
        "concreteType": null,
        "kind": "LinkedField",
        "name": "node",
        "plural": false,
        "selections": [
          {
            "alias": null,
            "args": null,
            "kind": "ScalarField",
            "name": "__typename",
            "storageKey": null
          },
          (v3/*: any*/),
          {
            "kind": "InlineFragment",
            "selections": [
              (v2/*: any*/),
              (v4/*: any*/),
              (v5/*: any*/),
              (v6/*: any*/),
              (v7/*: any*/)
            ],
            "type": "ReleaseLifecycle",
            "abstractKey": null
          }
        ],
        "storageKey": null
      }
    ]
  },
  "params": {
    "cacheID": "3c3a30130d610372f3e8a0522a4a0195",
    "id": null,
    "metadata": {},
    "name": "EditReleaseLifecycleQuery",
    "operationKind": "query",
    "text": "query EditReleaseLifecycleQuery(\n  $id: String!\n) {\n  node(id: $id) {\n    __typename\n    ... on ReleaseLifecycle {\n      metadata {\n        createdAt\n      }\n      id\n      name\n      scope\n      createdBy\n      lifecycleTemplate {\n        id\n        hclData\n      }\n    }\n    id\n  }\n}\n"
  }
};
})();

(node as any).hash = "9266b0ec279a12c4de4864b1c540c6bf";

export default node;
