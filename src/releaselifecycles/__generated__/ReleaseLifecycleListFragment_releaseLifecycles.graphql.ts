/**
 * @generated SignedSource<<b268613a4c892dc6630f2b2c6f2e6c89>>
 * @lightSyntaxTransform
 * @nogrep
 */

/* tslint:disable */
/* eslint-disable */
// @ts-nocheck

import { Fragment, ReaderFragment } from 'relay-runtime';
export type ScopeType = "GLOBAL" | "ORGANIZATION" | "PROJECT" | "%future added value";
import { FragmentRefs } from "relay-runtime";
export type ReleaseLifecycleListFragment_releaseLifecycles$data = {
  readonly edges: ReadonlyArray<{
    readonly node: {
      readonly id: string;
      readonly scope: ScopeType;
      readonly " $fragmentSpreads": FragmentRefs<"ReleaseLifecycleListItemFragment_releaseLifecycle">;
    } | null | undefined;
  } | null | undefined> | null | undefined;
  readonly totalCount: number;
  readonly " $fragmentType": "ReleaseLifecycleListFragment_releaseLifecycles";
};
export type ReleaseLifecycleListFragment_releaseLifecycles$key = {
  readonly " $data"?: ReleaseLifecycleListFragment_releaseLifecycles$data;
  readonly " $fragmentSpreads": FragmentRefs<"ReleaseLifecycleListFragment_releaseLifecycles">;
};

const node: ReaderFragment = {
  "argumentDefinitions": [],
  "kind": "Fragment",
  "metadata": null,
  "name": "ReleaseLifecycleListFragment_releaseLifecycles",
  "selections": [
    {
      "alias": null,
      "args": null,
      "kind": "ScalarField",
      "name": "totalCount",
      "storageKey": null
    },
    {
      "alias": null,
      "args": null,
      "concreteType": "ReleaseLifecycleEdge",
      "kind": "LinkedField",
      "name": "edges",
      "plural": true,
      "selections": [
        {
          "alias": null,
          "args": null,
          "concreteType": "ReleaseLifecycle",
          "kind": "LinkedField",
          "name": "node",
          "plural": false,
          "selections": [
            {
              "alias": null,
              "args": null,
              "kind": "ScalarField",
              "name": "id",
              "storageKey": null
            },
            {
              "alias": null,
              "args": null,
              "kind": "ScalarField",
              "name": "scope",
              "storageKey": null
            },
            {
              "args": null,
              "kind": "FragmentSpread",
              "name": "ReleaseLifecycleListItemFragment_releaseLifecycle"
            }
          ],
          "storageKey": null
        }
      ],
      "storageKey": null
    }
  ],
  "type": "ReleaseLifecycleConnection",
  "abstractKey": null
};

(node as any).hash = "46a56e98b71ccbd9d8b1babd4f5141df";

export default node;
