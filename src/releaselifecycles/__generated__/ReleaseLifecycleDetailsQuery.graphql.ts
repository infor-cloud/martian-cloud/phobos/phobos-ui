/**
 * @generated SignedSource<<96ca75ed9b293553d904ff66bda7c8cb>>
 * @lightSyntaxTransform
 * @nogrep
 */

/* tslint:disable */
/* eslint-disable */
// @ts-nocheck

import { ConcreteRequest, Query } from 'relay-runtime';
export type ReleaseLifecycleDetailsQuery$variables = {
  id: string;
};
export type ReleaseLifecycleDetailsQuery$data = {
  readonly node: {
    readonly id?: string;
    readonly lifecycleTemplate?: {
      readonly hclData: string;
      readonly id: string;
    };
    readonly metadata?: {
      readonly prn: string;
    };
    readonly name?: string;
  } | null | undefined;
};
export type ReleaseLifecycleDetailsQuery = {
  response: ReleaseLifecycleDetailsQuery$data;
  variables: ReleaseLifecycleDetailsQuery$variables;
};

const node: ConcreteRequest = (function(){
var v0 = [
  {
    "defaultValue": null,
    "kind": "LocalArgument",
    "name": "id"
  }
],
v1 = [
  {
    "kind": "Variable",
    "name": "id",
    "variableName": "id"
  }
],
v2 = {
  "alias": null,
  "args": null,
  "kind": "ScalarField",
  "name": "id",
  "storageKey": null
},
v3 = {
  "alias": null,
  "args": null,
  "kind": "ScalarField",
  "name": "name",
  "storageKey": null
},
v4 = {
  "alias": null,
  "args": null,
  "concreteType": "ResourceMetadata",
  "kind": "LinkedField",
  "name": "metadata",
  "plural": false,
  "selections": [
    {
      "alias": null,
      "args": null,
      "kind": "ScalarField",
      "name": "prn",
      "storageKey": null
    }
  ],
  "storageKey": null
},
v5 = {
  "alias": null,
  "args": null,
  "concreteType": "LifecycleTemplate",
  "kind": "LinkedField",
  "name": "lifecycleTemplate",
  "plural": false,
  "selections": [
    (v2/*: any*/),
    {
      "alias": null,
      "args": null,
      "kind": "ScalarField",
      "name": "hclData",
      "storageKey": null
    }
  ],
  "storageKey": null
};
return {
  "fragment": {
    "argumentDefinitions": (v0/*: any*/),
    "kind": "Fragment",
    "metadata": null,
    "name": "ReleaseLifecycleDetailsQuery",
    "selections": [
      {
        "alias": null,
        "args": (v1/*: any*/),
        "concreteType": null,
        "kind": "LinkedField",
        "name": "node",
        "plural": false,
        "selections": [
          {
            "kind": "InlineFragment",
            "selections": [
              (v2/*: any*/),
              (v3/*: any*/),
              (v4/*: any*/),
              (v5/*: any*/)
            ],
            "type": "ReleaseLifecycle",
            "abstractKey": null
          }
        ],
        "storageKey": null
      }
    ],
    "type": "Query",
    "abstractKey": null
  },
  "kind": "Request",
  "operation": {
    "argumentDefinitions": (v0/*: any*/),
    "kind": "Operation",
    "name": "ReleaseLifecycleDetailsQuery",
    "selections": [
      {
        "alias": null,
        "args": (v1/*: any*/),
        "concreteType": null,
        "kind": "LinkedField",
        "name": "node",
        "plural": false,
        "selections": [
          {
            "alias": null,
            "args": null,
            "kind": "ScalarField",
            "name": "__typename",
            "storageKey": null
          },
          (v2/*: any*/),
          {
            "kind": "InlineFragment",
            "selections": [
              (v3/*: any*/),
              (v4/*: any*/),
              (v5/*: any*/)
            ],
            "type": "ReleaseLifecycle",
            "abstractKey": null
          }
        ],
        "storageKey": null
      }
    ]
  },
  "params": {
    "cacheID": "c8845eca34b11f8dfbe40920d74fb979",
    "id": null,
    "metadata": {},
    "name": "ReleaseLifecycleDetailsQuery",
    "operationKind": "query",
    "text": "query ReleaseLifecycleDetailsQuery(\n  $id: String!\n) {\n  node(id: $id) {\n    __typename\n    ... on ReleaseLifecycle {\n      id\n      name\n      metadata {\n        prn\n      }\n      lifecycleTemplate {\n        id\n        hclData\n      }\n    }\n    id\n  }\n}\n"
  }
};
})();

(node as any).hash = "e8419e4bb0aeccbf0860ecc46abe5e01";

export default node;
