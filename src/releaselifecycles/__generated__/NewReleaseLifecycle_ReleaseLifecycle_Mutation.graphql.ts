/**
 * @generated SignedSource<<26c57febb907bd88ffca28414cd53a64>>
 * @lightSyntaxTransform
 * @nogrep
 */

/* tslint:disable */
/* eslint-disable */
// @ts-nocheck

import { ConcreteRequest, Mutation } from 'relay-runtime';
import { FragmentRefs } from "relay-runtime";
export type ProblemType = "BAD_REQUEST" | "CONFLICT" | "FORBIDDEN" | "NOT_FOUND" | "%future added value";
export type ScopeType = "GLOBAL" | "ORGANIZATION" | "PROJECT" | "%future added value";
export type CreateReleaseLifecycleInput = {
  clientMutationId?: string | null | undefined;
  lifecycleTemplateId: string;
  name: string;
  organizationId?: string | null | undefined;
  projectId?: string | null | undefined;
  scope: ScopeType;
};
export type NewReleaseLifecycle_ReleaseLifecycle_Mutation$variables = {
  connections: ReadonlyArray<string>;
  input: CreateReleaseLifecycleInput;
};
export type NewReleaseLifecycle_ReleaseLifecycle_Mutation$data = {
  readonly createReleaseLifecycle: {
    readonly problems: ReadonlyArray<{
      readonly field: ReadonlyArray<string> | null | undefined;
      readonly message: string;
      readonly type: ProblemType;
    }>;
    readonly releaseLifecycle: {
      readonly createdBy: string;
      readonly id: string;
      readonly name: string;
      readonly " $fragmentSpreads": FragmentRefs<"ReleaseLifecycleListItemFragment_releaseLifecycle">;
    } | null | undefined;
  };
};
export type NewReleaseLifecycle_ReleaseLifecycle_Mutation = {
  response: NewReleaseLifecycle_ReleaseLifecycle_Mutation$data;
  variables: NewReleaseLifecycle_ReleaseLifecycle_Mutation$variables;
};

const node: ConcreteRequest = (function(){
var v0 = {
  "defaultValue": null,
  "kind": "LocalArgument",
  "name": "connections"
},
v1 = {
  "defaultValue": null,
  "kind": "LocalArgument",
  "name": "input"
},
v2 = [
  {
    "kind": "Variable",
    "name": "input",
    "variableName": "input"
  }
],
v3 = {
  "alias": null,
  "args": null,
  "kind": "ScalarField",
  "name": "id",
  "storageKey": null
},
v4 = {
  "alias": null,
  "args": null,
  "kind": "ScalarField",
  "name": "name",
  "storageKey": null
},
v5 = {
  "alias": null,
  "args": null,
  "kind": "ScalarField",
  "name": "createdBy",
  "storageKey": null
},
v6 = {
  "alias": null,
  "args": null,
  "concreteType": "Problem",
  "kind": "LinkedField",
  "name": "problems",
  "plural": true,
  "selections": [
    {
      "alias": null,
      "args": null,
      "kind": "ScalarField",
      "name": "message",
      "storageKey": null
    },
    {
      "alias": null,
      "args": null,
      "kind": "ScalarField",
      "name": "field",
      "storageKey": null
    },
    {
      "alias": null,
      "args": null,
      "kind": "ScalarField",
      "name": "type",
      "storageKey": null
    }
  ],
  "storageKey": null
};
return {
  "fragment": {
    "argumentDefinitions": [
      (v0/*: any*/),
      (v1/*: any*/)
    ],
    "kind": "Fragment",
    "metadata": null,
    "name": "NewReleaseLifecycle_ReleaseLifecycle_Mutation",
    "selections": [
      {
        "alias": null,
        "args": (v2/*: any*/),
        "concreteType": "CreateReleaseLifecyclePayload",
        "kind": "LinkedField",
        "name": "createReleaseLifecycle",
        "plural": false,
        "selections": [
          {
            "alias": null,
            "args": null,
            "concreteType": "ReleaseLifecycle",
            "kind": "LinkedField",
            "name": "releaseLifecycle",
            "plural": false,
            "selections": [
              (v3/*: any*/),
              (v4/*: any*/),
              (v5/*: any*/),
              {
                "args": null,
                "kind": "FragmentSpread",
                "name": "ReleaseLifecycleListItemFragment_releaseLifecycle"
              }
            ],
            "storageKey": null
          },
          (v6/*: any*/)
        ],
        "storageKey": null
      }
    ],
    "type": "Mutation",
    "abstractKey": null
  },
  "kind": "Request",
  "operation": {
    "argumentDefinitions": [
      (v1/*: any*/),
      (v0/*: any*/)
    ],
    "kind": "Operation",
    "name": "NewReleaseLifecycle_ReleaseLifecycle_Mutation",
    "selections": [
      {
        "alias": null,
        "args": (v2/*: any*/),
        "concreteType": "CreateReleaseLifecyclePayload",
        "kind": "LinkedField",
        "name": "createReleaseLifecycle",
        "plural": false,
        "selections": [
          {
            "alias": null,
            "args": null,
            "concreteType": "ReleaseLifecycle",
            "kind": "LinkedField",
            "name": "releaseLifecycle",
            "plural": false,
            "selections": [
              (v3/*: any*/),
              (v4/*: any*/),
              (v5/*: any*/),
              {
                "alias": null,
                "args": null,
                "kind": "ScalarField",
                "name": "scope",
                "storageKey": null
              },
              {
                "alias": null,
                "args": null,
                "concreteType": "ResourceMetadata",
                "kind": "LinkedField",
                "name": "metadata",
                "plural": false,
                "selections": [
                  {
                    "alias": null,
                    "args": null,
                    "kind": "ScalarField",
                    "name": "updatedAt",
                    "storageKey": null
                  }
                ],
                "storageKey": null
              },
              {
                "alias": null,
                "args": null,
                "kind": "ScalarField",
                "name": "organizationName",
                "storageKey": null
              },
              {
                "alias": null,
                "args": null,
                "concreteType": "Project",
                "kind": "LinkedField",
                "name": "project",
                "plural": false,
                "selections": [
                  (v4/*: any*/),
                  (v3/*: any*/)
                ],
                "storageKey": null
              }
            ],
            "storageKey": null
          },
          {
            "alias": null,
            "args": null,
            "filters": null,
            "handle": "prependNode",
            "key": "",
            "kind": "LinkedHandle",
            "name": "releaseLifecycle",
            "handleArgs": [
              {
                "kind": "Variable",
                "name": "connections",
                "variableName": "connections"
              },
              {
                "kind": "Literal",
                "name": "edgeTypeName",
                "value": "Lifecycle_ReleaseLifecycleEdge"
              }
            ]
          },
          (v6/*: any*/)
        ],
        "storageKey": null
      }
    ]
  },
  "params": {
    "cacheID": "c4c6a2a6e98d69194857ca6acbae6718",
    "id": null,
    "metadata": {},
    "name": "NewReleaseLifecycle_ReleaseLifecycle_Mutation",
    "operationKind": "mutation",
    "text": "mutation NewReleaseLifecycle_ReleaseLifecycle_Mutation(\n  $input: CreateReleaseLifecycleInput!\n) {\n  createReleaseLifecycle(input: $input) {\n    releaseLifecycle {\n      id\n      name\n      createdBy\n      ...ReleaseLifecycleListItemFragment_releaseLifecycle\n    }\n    problems {\n      message\n      field\n      type\n    }\n  }\n}\n\nfragment ReleaseLifecycleListItemFragment_releaseLifecycle on ReleaseLifecycle {\n  id\n  name\n  scope\n  metadata {\n    updatedAt\n  }\n  organizationName\n  project {\n    name\n    id\n  }\n}\n"
  }
};
})();

(node as any).hash = "c9da8c8e6d5fd823e2f077c8b685bbc7";

export default node;
