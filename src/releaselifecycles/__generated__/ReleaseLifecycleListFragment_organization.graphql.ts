/**
 * @generated SignedSource<<637203c75a2adbaf3cb9161901b47d3c>>
 * @lightSyntaxTransform
 * @nogrep
 */

/* tslint:disable */
/* eslint-disable */
// @ts-nocheck

import { Fragment, ReaderFragment } from 'relay-runtime';
import { FragmentRefs } from "relay-runtime";
export type ReleaseLifecycleListFragment_organization$data = {
  readonly description: string;
  readonly name: string;
  readonly " $fragmentType": "ReleaseLifecycleListFragment_organization";
};
export type ReleaseLifecycleListFragment_organization$key = {
  readonly " $data"?: ReleaseLifecycleListFragment_organization$data;
  readonly " $fragmentSpreads": FragmentRefs<"ReleaseLifecycleListFragment_organization">;
};

const node: ReaderFragment = {
  "argumentDefinitions": [],
  "kind": "Fragment",
  "metadata": null,
  "name": "ReleaseLifecycleListFragment_organization",
  "selections": [
    {
      "alias": null,
      "args": null,
      "kind": "ScalarField",
      "name": "name",
      "storageKey": null
    },
    {
      "alias": null,
      "args": null,
      "kind": "ScalarField",
      "name": "description",
      "storageKey": null
    }
  ],
  "type": "Organization",
  "abstractKey": null
};

(node as any).hash = "f7a0f49b0370faa9679be3142ea964ed";

export default node;
