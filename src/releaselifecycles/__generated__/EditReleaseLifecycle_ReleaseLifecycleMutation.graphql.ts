/**
 * @generated SignedSource<<b8d1567c8e161a00a23cb8d25ac49428>>
 * @lightSyntaxTransform
 * @nogrep
 */

/* tslint:disable */
/* eslint-disable */
// @ts-nocheck

import { ConcreteRequest, Mutation } from 'relay-runtime';
export type ProblemType = "BAD_REQUEST" | "CONFLICT" | "FORBIDDEN" | "NOT_FOUND" | "%future added value";
export type UpdateReleaseLifecycleInput = {
  clientMutationId?: string | null | undefined;
  id: string;
  lifecycleTemplateId: string;
  metadata?: ResourceMetadataInput | null | undefined;
};
export type ResourceMetadataInput = {
  version: string;
};
export type EditReleaseLifecycle_ReleaseLifecycleMutation$variables = {
  input: UpdateReleaseLifecycleInput;
};
export type EditReleaseLifecycle_ReleaseLifecycleMutation$data = {
  readonly updateReleaseLifecycle: {
    readonly problems: ReadonlyArray<{
      readonly field: ReadonlyArray<string> | null | undefined;
      readonly message: string;
      readonly type: ProblemType;
    }>;
    readonly releaseLifecycle: {
      readonly createdBy: string;
      readonly id: string;
      readonly name: string;
    } | null | undefined;
  };
};
export type EditReleaseLifecycle_ReleaseLifecycleMutation = {
  response: EditReleaseLifecycle_ReleaseLifecycleMutation$data;
  variables: EditReleaseLifecycle_ReleaseLifecycleMutation$variables;
};

const node: ConcreteRequest = (function(){
var v0 = [
  {
    "defaultValue": null,
    "kind": "LocalArgument",
    "name": "input"
  }
],
v1 = [
  {
    "alias": null,
    "args": [
      {
        "kind": "Variable",
        "name": "input",
        "variableName": "input"
      }
    ],
    "concreteType": "UpdateReleaseLifecyclePayload",
    "kind": "LinkedField",
    "name": "updateReleaseLifecycle",
    "plural": false,
    "selections": [
      {
        "alias": null,
        "args": null,
        "concreteType": "ReleaseLifecycle",
        "kind": "LinkedField",
        "name": "releaseLifecycle",
        "plural": false,
        "selections": [
          {
            "alias": null,
            "args": null,
            "kind": "ScalarField",
            "name": "id",
            "storageKey": null
          },
          {
            "alias": null,
            "args": null,
            "kind": "ScalarField",
            "name": "name",
            "storageKey": null
          },
          {
            "alias": null,
            "args": null,
            "kind": "ScalarField",
            "name": "createdBy",
            "storageKey": null
          }
        ],
        "storageKey": null
      },
      {
        "alias": null,
        "args": null,
        "concreteType": "Problem",
        "kind": "LinkedField",
        "name": "problems",
        "plural": true,
        "selections": [
          {
            "alias": null,
            "args": null,
            "kind": "ScalarField",
            "name": "message",
            "storageKey": null
          },
          {
            "alias": null,
            "args": null,
            "kind": "ScalarField",
            "name": "field",
            "storageKey": null
          },
          {
            "alias": null,
            "args": null,
            "kind": "ScalarField",
            "name": "type",
            "storageKey": null
          }
        ],
        "storageKey": null
      }
    ],
    "storageKey": null
  }
];
return {
  "fragment": {
    "argumentDefinitions": (v0/*: any*/),
    "kind": "Fragment",
    "metadata": null,
    "name": "EditReleaseLifecycle_ReleaseLifecycleMutation",
    "selections": (v1/*: any*/),
    "type": "Mutation",
    "abstractKey": null
  },
  "kind": "Request",
  "operation": {
    "argumentDefinitions": (v0/*: any*/),
    "kind": "Operation",
    "name": "EditReleaseLifecycle_ReleaseLifecycleMutation",
    "selections": (v1/*: any*/)
  },
  "params": {
    "cacheID": "cd201d6eb6412444fc2ab82a0fb3ecde",
    "id": null,
    "metadata": {},
    "name": "EditReleaseLifecycle_ReleaseLifecycleMutation",
    "operationKind": "mutation",
    "text": "mutation EditReleaseLifecycle_ReleaseLifecycleMutation(\n  $input: UpdateReleaseLifecycleInput!\n) {\n  updateReleaseLifecycle(input: $input) {\n    releaseLifecycle {\n      id\n      name\n      createdBy\n    }\n    problems {\n      message\n      field\n      type\n    }\n  }\n}\n"
  }
};
})();

(node as any).hash = "bd57198ea3de672edde41a7289f2bad2";

export default node;
