/**
 * @generated SignedSource<<920c96c9d8e833891dd76293c6ef67fe>>
 * @lightSyntaxTransform
 * @nogrep
 */

/* tslint:disable */
/* eslint-disable */
// @ts-nocheck

import { ConcreteRequest, Mutation } from 'relay-runtime';
export type ProblemType = "BAD_REQUEST" | "CONFLICT" | "FORBIDDEN" | "NOT_FOUND" | "%future added value";
export type ScopeType = "GLOBAL" | "ORGANIZATION" | "PROJECT" | "%future added value";
export type CreateLifecycleTemplateInput = {
  clientMutationId?: string | null | undefined;
  hclData: string;
  organizationId?: string | null | undefined;
  projectId?: string | null | undefined;
  scope: ScopeType;
};
export type NewReleaseLifecycle_LifecycleTemplateMutation$variables = {
  input: CreateLifecycleTemplateInput;
};
export type NewReleaseLifecycle_LifecycleTemplateMutation$data = {
  readonly createLifecycleTemplate: {
    readonly lifecycleTemplate: {
      readonly hclData: string;
      readonly id: string;
    } | null | undefined;
    readonly problems: ReadonlyArray<{
      readonly field: ReadonlyArray<string> | null | undefined;
      readonly message: string;
      readonly type: ProblemType;
    }>;
  };
};
export type NewReleaseLifecycle_LifecycleTemplateMutation = {
  response: NewReleaseLifecycle_LifecycleTemplateMutation$data;
  variables: NewReleaseLifecycle_LifecycleTemplateMutation$variables;
};

const node: ConcreteRequest = (function(){
var v0 = [
  {
    "defaultValue": null,
    "kind": "LocalArgument",
    "name": "input"
  }
],
v1 = [
  {
    "alias": null,
    "args": [
      {
        "kind": "Variable",
        "name": "input",
        "variableName": "input"
      }
    ],
    "concreteType": "CreateLifecycleTemplatePayload",
    "kind": "LinkedField",
    "name": "createLifecycleTemplate",
    "plural": false,
    "selections": [
      {
        "alias": null,
        "args": null,
        "concreteType": "LifecycleTemplate",
        "kind": "LinkedField",
        "name": "lifecycleTemplate",
        "plural": false,
        "selections": [
          {
            "alias": null,
            "args": null,
            "kind": "ScalarField",
            "name": "id",
            "storageKey": null
          },
          {
            "alias": null,
            "args": null,
            "kind": "ScalarField",
            "name": "hclData",
            "storageKey": null
          }
        ],
        "storageKey": null
      },
      {
        "alias": null,
        "args": null,
        "concreteType": "Problem",
        "kind": "LinkedField",
        "name": "problems",
        "plural": true,
        "selections": [
          {
            "alias": null,
            "args": null,
            "kind": "ScalarField",
            "name": "message",
            "storageKey": null
          },
          {
            "alias": null,
            "args": null,
            "kind": "ScalarField",
            "name": "field",
            "storageKey": null
          },
          {
            "alias": null,
            "args": null,
            "kind": "ScalarField",
            "name": "type",
            "storageKey": null
          }
        ],
        "storageKey": null
      }
    ],
    "storageKey": null
  }
];
return {
  "fragment": {
    "argumentDefinitions": (v0/*: any*/),
    "kind": "Fragment",
    "metadata": null,
    "name": "NewReleaseLifecycle_LifecycleTemplateMutation",
    "selections": (v1/*: any*/),
    "type": "Mutation",
    "abstractKey": null
  },
  "kind": "Request",
  "operation": {
    "argumentDefinitions": (v0/*: any*/),
    "kind": "Operation",
    "name": "NewReleaseLifecycle_LifecycleTemplateMutation",
    "selections": (v1/*: any*/)
  },
  "params": {
    "cacheID": "6418680600201e61c4d99929584d05b1",
    "id": null,
    "metadata": {},
    "name": "NewReleaseLifecycle_LifecycleTemplateMutation",
    "operationKind": "mutation",
    "text": "mutation NewReleaseLifecycle_LifecycleTemplateMutation(\n  $input: CreateLifecycleTemplateInput!\n) {\n  createLifecycleTemplate(input: $input) {\n    lifecycleTemplate {\n      id\n      hclData\n    }\n    problems {\n      message\n      field\n      type\n    }\n  }\n}\n"
  }
};
})();

(node as any).hash = "c50e2bcb58dea051c997559af2974be5";

export default node;
