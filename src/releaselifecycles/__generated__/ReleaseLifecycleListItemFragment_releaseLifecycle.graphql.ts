/**
 * @generated SignedSource<<3bb4edb8a4c34744dce025f2938238c1>>
 * @lightSyntaxTransform
 * @nogrep
 */

/* tslint:disable */
/* eslint-disable */
// @ts-nocheck

import { Fragment, ReaderFragment } from 'relay-runtime';
export type ScopeType = "GLOBAL" | "ORGANIZATION" | "PROJECT" | "%future added value";
import { FragmentRefs } from "relay-runtime";
export type ReleaseLifecycleListItemFragment_releaseLifecycle$data = {
  readonly id: string;
  readonly metadata: {
    readonly updatedAt: any;
  };
  readonly name: string;
  readonly organizationName: string;
  readonly project: {
    readonly name: string;
  } | null | undefined;
  readonly scope: ScopeType;
  readonly " $fragmentType": "ReleaseLifecycleListItemFragment_releaseLifecycle";
};
export type ReleaseLifecycleListItemFragment_releaseLifecycle$key = {
  readonly " $data"?: ReleaseLifecycleListItemFragment_releaseLifecycle$data;
  readonly " $fragmentSpreads": FragmentRefs<"ReleaseLifecycleListItemFragment_releaseLifecycle">;
};

const node: ReaderFragment = (function(){
var v0 = {
  "alias": null,
  "args": null,
  "kind": "ScalarField",
  "name": "name",
  "storageKey": null
};
return {
  "argumentDefinitions": [],
  "kind": "Fragment",
  "metadata": null,
  "name": "ReleaseLifecycleListItemFragment_releaseLifecycle",
  "selections": [
    {
      "alias": null,
      "args": null,
      "kind": "ScalarField",
      "name": "id",
      "storageKey": null
    },
    (v0/*: any*/),
    {
      "alias": null,
      "args": null,
      "kind": "ScalarField",
      "name": "scope",
      "storageKey": null
    },
    {
      "alias": null,
      "args": null,
      "concreteType": "ResourceMetadata",
      "kind": "LinkedField",
      "name": "metadata",
      "plural": false,
      "selections": [
        {
          "alias": null,
          "args": null,
          "kind": "ScalarField",
          "name": "updatedAt",
          "storageKey": null
        }
      ],
      "storageKey": null
    },
    {
      "alias": null,
      "args": null,
      "kind": "ScalarField",
      "name": "organizationName",
      "storageKey": null
    },
    {
      "alias": null,
      "args": null,
      "concreteType": "Project",
      "kind": "LinkedField",
      "name": "project",
      "plural": false,
      "selections": [
        (v0/*: any*/)
      ],
      "storageKey": null
    }
  ],
  "type": "ReleaseLifecycle",
  "abstractKey": null
};
})();

(node as any).hash = "86be2c1bd0f06a428987f13450e5e8c5";

export default node;
