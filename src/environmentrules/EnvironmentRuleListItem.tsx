import { useEffect, useState } from 'react';
import graphql from 'babel-plugin-relay/macro';
import { Box, Button, Chip, Stack, styled, TableCell, TableRow, Tooltip, Typography } from '@mui/material';
import Gravatar from '../common/Gravatar';
import { Link as RouterLink } from 'react-router-dom';
import DeleteIcon from '@mui/icons-material/CloseOutlined';
import EditIcon from '@mui/icons-material/EditOutlined';
import { useFragment } from 'react-relay/hooks';
import PRNButton from '../common/PRNButton';
import StackedContainer from '../common/StackedContainer';
import StyledAvatar from '../common/StyledAvatar';
import { User, Team, ServiceAccount, Role } from './EnvironmentRuleForm';
import { EnvironmentRuleListItemFragment_environmentRule$key } from './__generated__/EnvironmentRuleListItemFragment_environmentRule.graphql';

const StyledTableRow = styled(
    TableRow
)(() => ({
    '&:last-child td, &:last-child th': {
        border: 0
    }
}));

interface Props {
    onDelete: () => void
    fragmentRef: EnvironmentRuleListItemFragment_environmentRule$key
}

const buildPrincipalsList = (users: User[], serviceAccounts: ServiceAccount[], teams: Team[]) => {
    return [
        ...users.map((user: User) =>
            ({ id: user.id, label: user.email, tooltip: user.email, type: 'user' })),
        ...serviceAccounts.map((serviceAccount: ServiceAccount) =>
            ({ id: serviceAccount.id, label: serviceAccount.name[0].toUpperCase(), tooltip: serviceAccount.name, type: 'serviceAccount' })),
        ...teams.map((team: Team) =>
            ({ id: team.id, label: team.name[0].toUpperCase(), tooltip: team.name, type: 'team' }))
    ]
};

const buildRolesList = (roles: Role[]) => {
    return roles.map((role: Role) => ({ id: role.id, label: role.name }))
};

function EnvironmentRuleListItem({ fragmentRef, onDelete }: Props) {
    const [principals, setPrincipals] = useState<(User | Team | ServiceAccount)[]>([]);
    const [roles, setRoles] = useState<Role[]>([]);

    const data = useFragment<EnvironmentRuleListItemFragment_environmentRule$key>(
        graphql`
            fragment EnvironmentRuleListItemFragment_environmentRule on EnvironmentRule
            {
                id
                metadata {
                    prn
                }
                environmentName
                users {
                    id
                    email
                    username
                }
                serviceAccounts {
                    id
                    name
                }
                teams {
                    id
                    name
                }
                roles {
                    id
                    name
                }
            }
        `, fragmentRef
    );

    useEffect(() => {
        if (data.users && data.serviceAccounts && data.teams) {
            setPrincipals(buildPrincipalsList(
                data.users as User[],
                data.serviceAccounts as ServiceAccount[],
                data.teams as Team[]
            ).map(principal => ({
                ...principal,
                name: principal.label
            })));
        }
        if (data.roles) {
            setRoles(buildRolesList(data.roles as Role[]).map(role => ({
                ...role,
                name: role.label
            })))
        }
    }, [data]);

    const environmentRule = data;

    return (
        <StyledTableRow>
            <TableCell>
                <Typography variant="body2">{environmentRule.environmentName.length > 20 ? `${environmentRule.environmentName.substring(0, 30)}...` : environmentRule.environmentName}</Typography>
            </TableCell>
            <TableCell>
                {principals.length === 0 ? '--' : <StackedContainer>
                    {principals.map((approver: any) => (
                        <Tooltip key={approver.id} title={approver.tooltip}>
                            <Box>
                                {approver.type == 'user'
                                    ? <Gravatar width={24} height={24} email={approver.label} />
                                    : <StyledAvatar>{approver.label}</StyledAvatar>}
                            </Box>
                        </Tooltip>
                    ))}
                </StackedContainer>}
            </TableCell>
            <TableCell>
                {roles.length === 0 ? '--' : <StackedContainer>
                    {roles.map((role: any) => (
                        <Chip
                            key={role.id}
                            label={role.label}
                            size="small"
                            sx={{ fontWeight: 500 }}
                        />
                    ))}
                </StackedContainer>}
            </TableCell>
            <TableCell>
                <Stack direction="row" spacing={1} justifyContent="center">
                <PRNButton prn={data.metadata.prn} />
                    <Button
                        sx={{ minWidth: 40, padding: '2px' }}
                        size="small"
                        color="info"
                        variant="outlined"
                        component={RouterLink}
                        to={`edit_environment_rule/${environmentRule.id}`}
                    >
                        <EditIcon />
                    </Button>
                    <Button
                        sx={{ minWidth: 40, padding: '2px' }}
                        size="small"
                        color="info"
                        variant="outlined"
                        onClick={onDelete}
                    >
                        <DeleteIcon />
                    </Button>
                </Stack>
            </TableCell>
        </StyledTableRow>
    );
}

export default EnvironmentRuleListItem;
