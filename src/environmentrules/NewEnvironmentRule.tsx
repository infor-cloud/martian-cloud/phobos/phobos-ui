import { useState } from 'react';
import { MutationError } from '../common/error';
import { Box, Button, Typography } from '@mui/material';
import { Link as RouterLink, useNavigate } from 'react-router-dom';
import LoadingButton from "@mui/lab/LoadingButton";
import EnvironmentRuleForm, { EnvironmentRule } from './EnvironmentRuleForm';
import { ConnectionHandler, useMutation } from 'react-relay/hooks';
import { useSnackbar } from 'notistack';
import graphql from 'babel-plugin-relay/macro';
import { RecordSourceProxy } from 'relay-runtime';
import { NewEnvironmentRuleMutation, NewEnvironmentRuleMutation$data } from './__generated__/NewEnvironmentRuleMutation.graphql';

interface Props {
    orgId?: string
    projectId?: string
    environmentNames: readonly string[]
    getConnections: (id: string) => [string]
}

function NewEnvironmentRule({ orgId, projectId, environmentNames, getConnections }: Props) {
    const navigate = useNavigate();
    const { enqueueSnackbar } = useSnackbar();

    const [commit, isInFlight] = useMutation<NewEnvironmentRuleMutation>(graphql`
        mutation NewEnvironmentRuleMutation($input: CreateEnvironmentRuleInput!) {
            createEnvironmentRule(input: $input) {
                environmentRule {
                    id
                    ...EnvironmentRuleListItemFragment_environmentRule
                }
                problems {
                    message
                    field
                    type
                }
            }
        }
    `);

    const [error, setError] = useState<MutationError>();
    const [environmentRule, setEnvironmentRule] = useState<EnvironmentRule>({
        environmentName: null,
        users: [],
        teams: [],
        serviceAccounts: [],
        roles: [],
    });

    const onSave = () => {
        if (environmentRule.environmentName) {
            commit({
                variables: {
                    input: {
                        environmentName: environmentRule.environmentName.trim(),
                        users: environmentRule.users.map(u => u.id),
                        teams: environmentRule.teams.map(t => t.id),
                        serviceAccounts: environmentRule.serviceAccounts.map(sa => sa.id),
                        roles: environmentRule.roles.map(r => r.id),
                        scope: orgId ? 'ORGANIZATION' : 'PROJECT',
                        orgId: orgId,
                        projectId: projectId,
                    }
                },
                onCompleted: data => {
                    if (data.createEnvironmentRule.problems.length) {
                        setError({
                            severity: 'warning',
                            message: data.createEnvironmentRule.problems.map((problem: any) => problem.message).join('; ')
                        });
                    } else if (!data.createEnvironmentRule.environmentRule) {
                        setError({
                            severity: 'error',
                            message: "Unexpected error occurred"
                        });
                    } else {
                        navigate(-1 as any);
                        enqueueSnackbar('Environment rule created', { variant: 'success' });
                    }
                },
                updater: (store: RecordSourceProxy, payload: NewEnvironmentRuleMutation$data) => {
                    const connectionRecord = store.get(orgId || projectId || '');
                    if (!payload.createEnvironmentRule.environmentRule) {
                        return;
                    }
                    const record = store.get(payload.createEnvironmentRule.environmentRule.id);

                    if (connectionRecord == null || record == null) {
                        return;

                    } else {
                        const newEdge = ConnectionHandler.createEdge(
                            store,
                            connectionRecord,
                            record,
                            'EnvironmentRuleEdge'
                        );
                        const connectionId = getConnections(orgId || projectId || '')[0];
                        const connection = store.get(connectionId);

                        if (!connection) {
                            console.error('Connection not found');
                            return;
                        }
                        let edges = connection.getLinkedRecords('edges') || [];
                        edges = [...edges, newEdge];
                        edges.sort((a: any, b: any) => {
                            const aName = a.getLinkedRecord('node')?.getValue('environmentName');
                            const bName = b.getLinkedRecord('node')?.getValue('environmentName');
                            return aName.localeCompare(bName);
                        });
                        connection.setLinkedRecords(edges, 'edges');
                    }
                },
                onError: error => {
                    setError({
                        severity: 'error',
                        message: `Unexpected error occurred: ${error.message}`
                    });
                }
            });
        }
    }

    const disabled = isInFlight || !environmentRule.environmentName ||
        !environmentRule.users.length && !environmentRule.teams.length && !environmentRule.serviceAccounts.length && !environmentRule.roles.length;

    return (
        <Box>
            <Typography variant="h5">New Environment Rule</Typography>
            <EnvironmentRuleForm
                error={error}
                orgId={orgId}
                projectId={projectId}
                environmentNames={environmentNames}
                onChange={(environmentRule: EnvironmentRule) => setEnvironmentRule(environmentRule)}
                data={environmentRule}
            />
            <Box mt={2}>
                <LoadingButton
                    disabled={disabled}
                    loading={isInFlight}
                    variant="outlined"
                    color="primary"
                    sx={{ mr: 2 }}
                    onClick={onSave}
                >
                    Create Environment Rule
                </LoadingButton>
                <Button component={RouterLink} color="inherit" to={-1 as any}>Cancel</Button>
            </Box>
        </Box>
    );
}

export default NewEnvironmentRule;
