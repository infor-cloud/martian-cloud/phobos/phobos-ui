import { useMemo } from 'react';
import { Alert, Box, Chip, Divider, IconButton, List, ListItem, ListItemText, Typography, useTheme } from '@mui/material';
import DeleteIcon from '@mui/icons-material/Delete';
import Gravatar from '../common/Gravatar';
import StyledAvatar from '../common/StyledAvatar';
import PrincipalAutocomplete, { Option, BaseOption, UserOption } from '../common/PrincipalAutocomplete';
import RoleAutocomplete, { RoleOption } from '../common/RoleAutocomplete';
import { MutationError } from '../common/error';
import EnvironmentAutocomplete from './EnvironmentAutocomplete';

export interface User {
    id: string
    email: string
    username: string
}

export interface ServiceAccount {
    id: string
    name: string
}

export interface Team {
    id: string
    name: string
}

export interface Role {
    id: string
    name: string
}

export interface EnvironmentRule {
    environmentName: string | null
    users: User[]
    serviceAccounts: ServiceAccount[]
    teams: Team[]
    roles: Role[]
}

interface Props {
    data: EnvironmentRule
    orgId?: string
    projectId?: string
    editMode?: boolean
    error?: MutationError
    selectedEnvironment?: string
    environmentNames?: readonly string[]
    onChange: (environmentRule: EnvironmentRule) => void
}

function EnvironmentRuleForm({ data, orgId, projectId, editMode, error, selectedEnvironment, environmentNames, onChange }: Props) {
    const theme = useTheme();

    const principals = useMemo(() =>
        [
            ...data.users.map((user: User) => ({ id: user.id, type: 'user', label: user.email, tooltip: user.username, name: user.username })),
            ...data.serviceAccounts.map((sa: ServiceAccount) => ({ id: sa.id, type: 'serviceaccount', label: sa.name[0].toUpperCase(), tooltip: sa.name, name: sa.name })),
            ...data.teams.map((team: Team) => ({ id: team.id, type: 'team', label: team.name[0].toUpperCase(), tooltip: team.name, name: team.name }))
        ]
        , [data.users, data.serviceAccounts, data.teams]
    );

    const roles = [
        ...data.roles.map((role: Role) => ({ id: role.id, name: role.name }))
    ];

    const onPrincipalSelected = (value: Option | null) => {
        if (value) {
            switch (value.type) {
                case 'user': {
                    const user = value as UserOption;
                    onChange({ ...data, users: [...data.users, { id: user.id, email: user.email, username: user.username }] });
                    break;
                }
                case 'team': {
                    const team = value as BaseOption;
                    onChange({ ...data, teams: [...data.teams, { id: team.id, name: team.label }] });
                    break;
                }
                case 'serviceaccount': {
                    const sa = value as BaseOption;
                    onChange({ ...data, serviceAccounts: [...data.serviceAccounts, { id: sa.id, name: sa.label }] });
                    break;
                }
            }
        }
    };

    const onRoleSelected = (value: RoleOption | null) => {
        if (value) {
            onChange({ ...data, roles: [...data.roles, value] });
        }
    };

    const onPrincipalRemove = (principal: { id: string; type: 'user' | 'team' | 'serviceaccount' }) => {
        switch (principal.type) {
            case 'user': {
                onChange({ ...data, users: data.users.filter((user: User) => user.id !== principal.id) });
                break;
            }
            case 'team': {
                onChange({ ...data, teams: data.teams.filter((team: Team) => team.id !== principal.id) });
                break;
            }
            case 'serviceaccount': {
                onChange({ ...data, serviceAccounts: data.serviceAccounts.filter((sa: ServiceAccount) => sa.id !== principal.id) });
                break;
            }
        }
    };

    const selectedPrincipals = principals.reduce((accumulator: Set<string>, item: User | Team | ServiceAccount) => {
        accumulator.add(item.id);
        return accumulator;
    }, new Set());

    const selectedRoles = roles.reduce((accumulator: Set<string>, item: Role) => {
        accumulator.add(item.name);
        return accumulator;
    }, new Set());

    return (
        <Box mt={2}>
            {error && <Alert sx={{ mt: 2 }} severity={error.severity}>
                {error.message}
            </Alert>}
            <Box mb={4}>
                <Typography variant="subtitle1" gutterBottom>Environment</Typography>
                {!selectedEnvironment &&
                    <Typography
                        mb={2}
                        variant="subtitle2">Select an existing environment or enter the name of the environment if it hasn't been created yet.
                    </Typography>}
                <EnvironmentAutocomplete
                    disabled={editMode}
                    defaultSelectedEnvironment={selectedEnvironment}
                    options={environmentNames || []}
                    onSelected={(environmentName: string | null) => onChange({ ...data, environmentName })}
                />
            </Box>
            <Box>
                <Box mb={2}>
                    <Typography variant="subtitle1" gutterBottom>Principals and Roles</Typography>
                    <Typography variant="subtitle2">An environment rule must have at least one principal or one role.</Typography>
                    <Typography marginBottom={2} variant="subtitle2">Only the selected principals and roles will be allowed to create pipelines in a given environment.</Typography>
                </Box>
                <Box sx={{ mb: 4, p: 2, border: `1px solid ${theme.palette.divider}`, borderRadius: '4px' }}>
                    <Box>
                        <Box mb={2}>
                            <Typography mb={1} variant="subtitle2">Principals</Typography>
                            <PrincipalAutocomplete
                                orgId={orgId}
                                projectId={projectId}
                                onSelected={onPrincipalSelected}
                                filterOptions={(options: Option[]) => options.filter(option => !selectedPrincipals.has(option.id))}
                            />
                        </Box>
                        <Typography color="textSecondary">
                            {principals.length} principal{principals.length === 1 ? '' : 's'} selected
                        </Typography>
                        <List dense>
                            {principals.map((principal: any) => (
                                <ListItem
                                    disableGutters
                                    secondaryAction={
                                        <IconButton onClick={() => onPrincipalRemove(principal)}>
                                            <DeleteIcon />
                                        </IconButton>}
                                    key={principal.id}>
                                    {
                                        principal.type === 'user' ?
                                            <Gravatar sx={{ mr: 1 }} width={24} height={24} email={principal.label} />
                                            :
                                            <StyledAvatar sx={{ mr: 1 }}>{principal.label}</StyledAvatar>
                                    }
                                    <ListItemText primary={principal.name} primaryTypographyProps={{ noWrap: true }} />
                                </ListItem>
                            ))}
                        </List>
                    </Box>
                </Box>
            </Box>
            <Box>
                <Box sx={{ mb: 4, p: 2, border: `1px solid ${theme.palette.divider}`, borderRadius: '4px' }}>
                    <Box>
                        <Typography mb={1} variant="subtitle2">Roles</Typography>

                        <Box mb={2}>
                            <RoleAutocomplete
                                clearInputOnSelection
                                onSelected={onRoleSelected}
                                filterOptions={(options: RoleOption[]) => options.filter(option => !selectedRoles.has(option.name))}
                            />
                        </Box>
                        <Typography color="textSecondary">
                            {roles.length} role{roles.length === 1 ? '' : 's'} selected
                        </Typography>
                        <List dense>
                            {roles.map((role: Role) => (
                                <ListItem
                                    disableGutters
                                    secondaryAction={
                                        <IconButton onClick={() => onChange({ ...data, roles: data.roles.filter((r: Role) => r.id !== role.id) })}>
                                            <DeleteIcon />
                                        </IconButton>}
                                    key={role.id}>
                                    <ListItemText primary={
                                        <Chip
                                            key={role.id}
                                            label={role.name}
                                            size="small"
                                            sx={{ fontWeight: 500 }}
                                        />}
                                        primaryTypographyProps={{ noWrap: true }} />
                                </ListItem>
                            ))}
                        </List>
                    </Box>
                </Box>
            </Box>
            <Divider sx={{ opacity: 0.6 }} />
        </Box>
    );
}

export default EnvironmentRuleForm;
