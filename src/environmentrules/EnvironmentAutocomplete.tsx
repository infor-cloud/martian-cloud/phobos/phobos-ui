import { useState } from 'react';
import { Autocomplete, TextField } from '@mui/material';

interface Props {
    disabled?: boolean
    defaultSelectedEnvironment?: string
    options: readonly string[]
    onSelected: (environmentName: string | null) => void
}

const EnvironmentAutocomplete = ({ disabled, defaultSelectedEnvironment, options, onSelected }: Props) => {
    const [value, setValue] = useState<string | null>(defaultSelectedEnvironment || null);
    const [inputValue, setInputValue] = useState('');

    return (
        <Autocomplete
            freeSolo
            disabled={disabled}
            size="small"
            value={value}
            inputValue={inputValue}
            onChange={(event, newValue) => setValue(newValue)}
            onInputChange={(event, newInputValue) => {
                setInputValue(newInputValue);
                onSelected(newInputValue);
            }}
            options={options}
            renderInput={(params) => (
                <TextField
                    {...params}
                    placeholder="Select or type an environment"
                />
            )}
        />
    );
};

export default EnvironmentAutocomplete;
