/**
 * @generated SignedSource<<ba2ffa429145990a5d382f306863f97a>>
 * @lightSyntaxTransform
 * @nogrep
 */

/* tslint:disable */
/* eslint-disable */
// @ts-nocheck

import { Fragment, ReaderFragment } from 'relay-runtime';
import { FragmentRefs } from "relay-runtime";
export type EnvironmentRuleListItemFragment_environmentRule$data = {
  readonly environmentName: string;
  readonly id: string;
  readonly metadata: {
    readonly prn: string;
  };
  readonly roles: ReadonlyArray<{
    readonly id: string;
    readonly name: string;
  }>;
  readonly serviceAccounts: ReadonlyArray<{
    readonly id: string;
    readonly name: string;
  }>;
  readonly teams: ReadonlyArray<{
    readonly id: string;
    readonly name: string;
  }>;
  readonly users: ReadonlyArray<{
    readonly email: string;
    readonly id: string;
    readonly username: string;
  }>;
  readonly " $fragmentType": "EnvironmentRuleListItemFragment_environmentRule";
};
export type EnvironmentRuleListItemFragment_environmentRule$key = {
  readonly " $data"?: EnvironmentRuleListItemFragment_environmentRule$data;
  readonly " $fragmentSpreads": FragmentRefs<"EnvironmentRuleListItemFragment_environmentRule">;
};

const node: ReaderFragment = (function(){
var v0 = {
  "alias": null,
  "args": null,
  "kind": "ScalarField",
  "name": "id",
  "storageKey": null
},
v1 = [
  (v0/*: any*/),
  {
    "alias": null,
    "args": null,
    "kind": "ScalarField",
    "name": "name",
    "storageKey": null
  }
];
return {
  "argumentDefinitions": [],
  "kind": "Fragment",
  "metadata": null,
  "name": "EnvironmentRuleListItemFragment_environmentRule",
  "selections": [
    (v0/*: any*/),
    {
      "alias": null,
      "args": null,
      "concreteType": "ResourceMetadata",
      "kind": "LinkedField",
      "name": "metadata",
      "plural": false,
      "selections": [
        {
          "alias": null,
          "args": null,
          "kind": "ScalarField",
          "name": "prn",
          "storageKey": null
        }
      ],
      "storageKey": null
    },
    {
      "alias": null,
      "args": null,
      "kind": "ScalarField",
      "name": "environmentName",
      "storageKey": null
    },
    {
      "alias": null,
      "args": null,
      "concreteType": "User",
      "kind": "LinkedField",
      "name": "users",
      "plural": true,
      "selections": [
        (v0/*: any*/),
        {
          "alias": null,
          "args": null,
          "kind": "ScalarField",
          "name": "email",
          "storageKey": null
        },
        {
          "alias": null,
          "args": null,
          "kind": "ScalarField",
          "name": "username",
          "storageKey": null
        }
      ],
      "storageKey": null
    },
    {
      "alias": null,
      "args": null,
      "concreteType": "ServiceAccount",
      "kind": "LinkedField",
      "name": "serviceAccounts",
      "plural": true,
      "selections": (v1/*: any*/),
      "storageKey": null
    },
    {
      "alias": null,
      "args": null,
      "concreteType": "Team",
      "kind": "LinkedField",
      "name": "teams",
      "plural": true,
      "selections": (v1/*: any*/),
      "storageKey": null
    },
    {
      "alias": null,
      "args": null,
      "concreteType": "Role",
      "kind": "LinkedField",
      "name": "roles",
      "plural": true,
      "selections": (v1/*: any*/),
      "storageKey": null
    }
  ],
  "type": "EnvironmentRule",
  "abstractKey": null
};
})();

(node as any).hash = "1b8f27df6c768eaf6f0f2875d018c8ce";

export default node;
