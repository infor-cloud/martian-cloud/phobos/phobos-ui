/**
 * @generated SignedSource<<331e43be400b5cd1ebf400d5fcf83422>>
 * @lightSyntaxTransform
 * @nogrep
 */

/* tslint:disable */
/* eslint-disable */
// @ts-nocheck

import { Fragment, ReaderFragment } from 'relay-runtime';
import { FragmentRefs } from "relay-runtime";
export type EditEnvironmentRuleFragment_environmentRule$data = {
  readonly environmentName: string;
  readonly id: string;
  readonly roles: ReadonlyArray<{
    readonly id: string;
    readonly name: string;
  }>;
  readonly serviceAccounts: ReadonlyArray<{
    readonly id: string;
    readonly name: string;
  }>;
  readonly teams: ReadonlyArray<{
    readonly id: string;
    readonly name: string;
  }>;
  readonly users: ReadonlyArray<{
    readonly email: string;
    readonly id: string;
    readonly username: string;
  }>;
  readonly " $fragmentType": "EditEnvironmentRuleFragment_environmentRule";
};
export type EditEnvironmentRuleFragment_environmentRule$key = {
  readonly " $data"?: EditEnvironmentRuleFragment_environmentRule$data;
  readonly " $fragmentSpreads": FragmentRefs<"EditEnvironmentRuleFragment_environmentRule">;
};

const node: ReaderFragment = (function(){
var v0 = {
  "alias": null,
  "args": null,
  "kind": "ScalarField",
  "name": "id",
  "storageKey": null
},
v1 = [
  (v0/*: any*/),
  {
    "alias": null,
    "args": null,
    "kind": "ScalarField",
    "name": "name",
    "storageKey": null
  }
];
return {
  "argumentDefinitions": [],
  "kind": "Fragment",
  "metadata": null,
  "name": "EditEnvironmentRuleFragment_environmentRule",
  "selections": [
    (v0/*: any*/),
    {
      "alias": null,
      "args": null,
      "kind": "ScalarField",
      "name": "environmentName",
      "storageKey": null
    },
    {
      "alias": null,
      "args": null,
      "concreteType": "User",
      "kind": "LinkedField",
      "name": "users",
      "plural": true,
      "selections": [
        (v0/*: any*/),
        {
          "alias": null,
          "args": null,
          "kind": "ScalarField",
          "name": "email",
          "storageKey": null
        },
        {
          "alias": null,
          "args": null,
          "kind": "ScalarField",
          "name": "username",
          "storageKey": null
        }
      ],
      "storageKey": null
    },
    {
      "alias": null,
      "args": null,
      "concreteType": "ServiceAccount",
      "kind": "LinkedField",
      "name": "serviceAccounts",
      "plural": true,
      "selections": (v1/*: any*/),
      "storageKey": null
    },
    {
      "alias": null,
      "args": null,
      "concreteType": "Team",
      "kind": "LinkedField",
      "name": "teams",
      "plural": true,
      "selections": (v1/*: any*/),
      "storageKey": null
    },
    {
      "alias": null,
      "args": null,
      "concreteType": "Role",
      "kind": "LinkedField",
      "name": "roles",
      "plural": true,
      "selections": (v1/*: any*/),
      "storageKey": null
    }
  ],
  "type": "EnvironmentRule",
  "abstractKey": null
};
})();

(node as any).hash = "264d2e9503f5df205d46459c202b89a6";

export default node;
