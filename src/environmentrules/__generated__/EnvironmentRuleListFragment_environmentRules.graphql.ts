/**
 * @generated SignedSource<<c3a5183b6bab43a69820703274f5be35>>
 * @lightSyntaxTransform
 * @nogrep
 */

/* tslint:disable */
/* eslint-disable */
// @ts-nocheck

import { Fragment, ReaderFragment } from 'relay-runtime';
import { FragmentRefs } from "relay-runtime";
export type EnvironmentRuleListFragment_environmentRules$data = {
  readonly edges: ReadonlyArray<{
    readonly node: {
      readonly environmentName: string;
      readonly id: string;
      readonly " $fragmentSpreads": FragmentRefs<"EnvironmentRuleListItemFragment_environmentRule">;
    } | null | undefined;
  } | null | undefined> | null | undefined;
  readonly " $fragmentType": "EnvironmentRuleListFragment_environmentRules";
};
export type EnvironmentRuleListFragment_environmentRules$key = {
  readonly " $data"?: EnvironmentRuleListFragment_environmentRules$data;
  readonly " $fragmentSpreads": FragmentRefs<"EnvironmentRuleListFragment_environmentRules">;
};

const node: ReaderFragment = {
  "argumentDefinitions": [],
  "kind": "Fragment",
  "metadata": null,
  "name": "EnvironmentRuleListFragment_environmentRules",
  "selections": [
    {
      "alias": null,
      "args": null,
      "concreteType": "EnvironmentRuleEdge",
      "kind": "LinkedField",
      "name": "edges",
      "plural": true,
      "selections": [
        {
          "alias": null,
          "args": null,
          "concreteType": "EnvironmentRule",
          "kind": "LinkedField",
          "name": "node",
          "plural": false,
          "selections": [
            {
              "alias": null,
              "args": null,
              "kind": "ScalarField",
              "name": "id",
              "storageKey": null
            },
            {
              "alias": null,
              "args": null,
              "kind": "ScalarField",
              "name": "environmentName",
              "storageKey": null
            },
            {
              "args": null,
              "kind": "FragmentSpread",
              "name": "EnvironmentRuleListItemFragment_environmentRule"
            }
          ],
          "storageKey": null
        }
      ],
      "storageKey": null
    }
  ],
  "type": "EnvironmentRuleConnection",
  "abstractKey": null
};

(node as any).hash = "49d05fce20590fea85e762540f90e04f";

export default node;
