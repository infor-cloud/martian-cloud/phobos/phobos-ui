import { useState } from 'react';
import { Box, Link, Table, TableBody, TableCell, TableContainer, TableHead, TableRow, Paper, Typography } from '@mui/material';
import graphql from 'babel-plugin-relay/macro';
import EnvironmentRuleListItem from './EnvironmentRuleListItem';
import ConfirmationDialog from '../common/ConfirmationDialog';
import { useSnackbar } from 'notistack';
import { LoadMoreFn, useFragment, useMutation } from 'react-relay/hooks';
import { OrgEnvironmentRulesPaginationQuery } from '../../src/organizations/settings/environmentrules/__generated__/OrgEnvironmentRulesPaginationQuery.graphql';
import { EnvironmentRuleListFragment_environmentRules$key } from './__generated__/EnvironmentRuleListFragment_environmentRules.graphql';
import { EnvironmentRuleList_DeleteMutation } from './__generated__/EnvironmentRuleList_DeleteMutation.graphql';

interface Props {
    fragmentRef: EnvironmentRuleListFragment_environmentRules$key
    loadNext: LoadMoreFn<OrgEnvironmentRulesPaginationQuery>
    hasNext: boolean
    getConnections: () => string[]
}

function EnvironmentRuleList({ fragmentRef, loadNext, hasNext, getConnections }: Props) {
    const { enqueueSnackbar } = useSnackbar();
    const [ruleToDelete, setRuleToDelete] = useState<{ id: string, environmentName: string } | null>(null);

    const data = useFragment<EnvironmentRuleListFragment_environmentRules$key>(graphql`
        fragment EnvironmentRuleListFragment_environmentRules on EnvironmentRuleConnection {
            edges {
                node {
                    id
                    environmentName
                    ...EnvironmentRuleListItemFragment_environmentRule
                }
            }
        }
    `, fragmentRef);

    const [commit, isInFlight] = useMutation<EnvironmentRuleList_DeleteMutation>(graphql`
        mutation EnvironmentRuleList_DeleteMutation($input: DeleteEnvironmentRuleInput!, $connections: [ID!]!) {
            deleteEnvironmentRule(input: $input) {
                environmentRule {
                    id @deleteEdge(connections: $connections)
                }
                problems {
                    message
                    field
                    type
                }
            }
        }
    `);

    const onDelete = (environmentRuleId: string | null) => {
        if (environmentRuleId) {
            commit({
                variables: {
                    input: {
                        id: environmentRuleId
                    },
                    connections: getConnections()
                },
                onCompleted: data => {
                    if (data.deleteEnvironmentRule.problems.length) {
                        enqueueSnackbar(data.deleteEnvironmentRule.problems.map(problem => problem.message).join('; '), { variant: 'warning' });
                    }
                },
                onError: error => {
                    enqueueSnackbar(`Unexpected error occurred: ${error.message}`, { variant: 'error' });
                }
            })
        }
        setRuleToDelete(null);
    };

    return (
        <Box mb={2}>
            <Typography variant="subtitle1" gutterBottom>Environment Protection Rules</Typography>
            <Typography mb={2} variant="subtitle2" gutterBottom>Environment protection rules designate the users, service accounts, teams, and roles that can deploy to a specified environment</Typography>
            {data?.edges && data?.edges?.length > 0 && <TableContainer component={Paper}>
                <Table>
                    <TableHead>
                        <TableRow>
                            <TableCell width={150}>Environment</TableCell>
                            <TableCell>Principals</TableCell>
                            <TableCell>Roles</TableCell>
                            <TableCell></TableCell>
                        </TableRow>
                    </TableHead>
                    <TableBody>
                        {data?.edges?.map((edge: any) => (
                            <EnvironmentRuleListItem
                                key={edge.node.id}
                                onDelete={() => setRuleToDelete({ id: edge.node.id, environmentName: edge.node.environmentName })}
                                fragmentRef={edge.node}
                            />
                        ))}
                        {hasNext && (
                            <TableRow
                                sx={{
                                    '&:last-child td, &:last-child th': {
                                        border: 0
                                    }
                                }}>
                                <TableCell align="center" colSpan={4}>
                                    <Link
                                        variant="body2"
                                        color="textSecondary"
                                        sx={{ cursor: 'pointer' }}
                                        underline="hover"
                                        onClick={() => loadNext(10)}
                                    >
                                        Show more
                                    </Link>
                                </TableCell>
                            </TableRow>
                        )}
                    </TableBody>
                </Table>
            </TableContainer>}
            {ruleToDelete && <ConfirmationDialog
                title="Delete Environment Rule"
                message={<span>Are you sure you want to delete the rule for environment <strong>{ruleToDelete.environmentName}</strong>?</span>}
                confirmButtonLabel='Delete'
                opInProgress={isInFlight}
                onConfirm={() => onDelete(ruleToDelete.id)}
                onClose={() => onDelete(null)}
            />}
        </Box>
    );
}

export default EnvironmentRuleList;
