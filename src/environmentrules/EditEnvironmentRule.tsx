import { useState } from 'react';
import { Box, Button, Typography } from '@mui/material';
import LoadingButton from '@mui/lab/LoadingButton';
import { Link as RouterLink, useNavigate } from 'react-router-dom';
import { useFragment, useMutation } from 'react-relay/hooks';
import { useSnackbar } from 'notistack';
import graphql from 'babel-plugin-relay/macro';
import { MutationError } from '../common/error';
import EnvironmentRuleForm, { EnvironmentRule, User, ServiceAccount, Team, Role } from './EnvironmentRuleForm';
import { EditEnvironmentRuleFragment_environmentRule$key } from './__generated__/EditEnvironmentRuleFragment_environmentRule.graphql';
import { EditEnvironmentRuleMutation } from './__generated__/EditEnvironmentRuleMutation.graphql';

interface Props {
    orgId?: string
    projectId?: string
    fragmentRef: EditEnvironmentRuleFragment_environmentRule$key
}

function EditEnvironmentRule({ orgId, projectId, fragmentRef }: Props) {
    const navigate = useNavigate();
    const { enqueueSnackbar } = useSnackbar();
    const [error, setError] = useState<MutationError>();

    const environmentRule = useFragment<EditEnvironmentRuleFragment_environmentRule$key>(graphql`
        fragment EditEnvironmentRuleFragment_environmentRule on EnvironmentRule
        {
            id
            environmentName
            users {
                id
                email
                username
            }
            serviceAccounts {
                id
                name
            }
            teams {
                id
                name
            }
            roles {
                id
                name
            }
        }`,
        fragmentRef
    );

    const [commit, isInFlight] = useMutation<EditEnvironmentRuleMutation>(graphql`
        mutation EditEnvironmentRuleMutation($input: UpdateEnvironmentRuleInput!) {
            updateEnvironmentRule(input: $input) {
                environmentRule {
                    ...EnvironmentRuleListItemFragment_environmentRule
                }
                problems {
                    message
                    field
                    type
                }
            }
        }
    `);

    const [formData, setFormData] = useState<EnvironmentRule>({
        environmentName: environmentRule.environmentName,
        users: environmentRule.users.map((user: User) => ({ id: user.id, email: user.email, username: user.username })),
        serviceAccounts: environmentRule.serviceAccounts.map((serviceAccount: ServiceAccount) => ({ id: serviceAccount.id, name: serviceAccount.name})),
        teams: environmentRule.teams.map((team: Team) => ({ id: team.id, name: team.name})),
        roles: environmentRule.roles.map((role: Role) => ({ id: role.id, name: role.name }))
    });

    const onUpdate = () => {
        commit({
            variables: {
                input: {
                    id: environmentRule.id,
                    users: formData.users.map(user => user.id),
                    serviceAccounts: formData.serviceAccounts.map(serviceAccount => serviceAccount.id),
                    teams: formData.teams.map(team => team.id),
                    roles: formData.roles.map(role => role.id)
                }
            },
            onCompleted: data => {
                if (data.updateEnvironmentRule.problems.length) {
                    setError({
                        severity: 'warning',
                        message: data.updateEnvironmentRule.problems.map((problem: any) => problem.message).join('; ')
                    });
                } else if (!data.updateEnvironmentRule.environmentRule) {
                    setError({
                        severity: 'error',
                        message: "Unexpected error occurred"
                    });
                } else {
                    navigate(-1);
                    enqueueSnackbar('Environment rule updated', { variant: 'success' });
                }
            },
            onError: error => {
                setError({
                    severity: 'error',
                    message: `Unexpected error occurred: ${error.message}`
                });
            }
        });
    };

    return (
        <Box>
            <Typography variant="h5">Edit Environment Rule</Typography>
            <EnvironmentRuleForm
                editMode
                orgId={orgId}
                projectId={projectId}
                selectedEnvironment={environmentRule.environmentName}
                data={formData}
                onChange={(data: EnvironmentRule) => setFormData(data)}
                error={error}
            />
            <Box mt={2}>
                <LoadingButton
                    loading={isInFlight}
                    variant="outlined"
                    color="primary"
                    sx={{ mr: 2 }}
                    onClick={onUpdate}
                >
                    Update Environment Rule
                </LoadingButton>
                <Button component={RouterLink} color="inherit" to={-1 as any}>Cancel</Button>
            </Box>
        </Box>
    );
}

export default EditEnvironmentRule;
