import { Button } from '@mui/material';
import CssBaseline from '@mui/material/CssBaseline';
import purple from '@mui/material/colors/purple';
import { ThemeProvider, createTheme } from "@mui/material/styles";
import { SnackbarProvider } from 'notistack';
import React from 'react';
import { RelayEnvironmentProvider } from 'react-relay';
import { RouterProvider } from 'react-router';

// TODO: In a future story this will be configurable via settings
const mode = 'dark' as any

declare module '@mui/material/Chip' {
    interface ChipPropsSizeOverrides {
        xs: true;
    }
}

const theme = createTheme({
    palette: {
        mode,
        primary: {
            main: mode === 'dark' ? purple[300] : purple[500]
        },
        secondary: {
            main: '#29b6f6'
        },
        info: {
            main: 'rgba(255,255,255,0.7)'
        }
    },
    typography: {
        fontFamily: [
            '-apple-system',
            'BlinkMacSystemFont',
            '"Segoe UI"',
            'Roboto',
            '"Helvetica Neue"',
            'Arial',
            'sans-serif',
            '"Apple Color Emoji"',
            '"Segoe UI Emoji"',
            '"Segoe UI Symbol"',
        ].join(','),
    },
    components: {
        MuiChip: {
            variants: [
                {
                    props: { size: 'xs' },
                    style: {
                        fontSize: '0.75rem',
                        lineHeight: '1rem',
                        height: '20px',
                        borderRadius: '0.25rem'
                    }
                }
            ]
        }
    }
});

interface Props {
    relayEnvironment: any;
    router: any;
}

function App(props: Props) {
    // Add dismiss action to all snackbars
    const notistackRef = React.createRef<SnackbarProvider>();
    const onClickDismiss = (key: any) => () => {
        notistackRef.current?.closeSnackbar(key);
    }

    return (
        <RelayEnvironmentProvider environment={props.relayEnvironment}>
            <ThemeProvider theme={theme}>
                <SnackbarProvider
                    ref={notistackRef}
                    action={(key) => (
                        <Button onClick={onClickDismiss(key)} color="inherit">
                            Dismiss
                        </Button>
                    )}>
                    <CssBaseline />
                    <RouterProvider router={props.router} />
                </SnackbarProvider>
            </ThemeProvider>
        </RelayEnvironmentProvider>
    );
}

export default App;
