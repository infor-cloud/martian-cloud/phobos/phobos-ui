import { createGraphiQLFetcher, Fetcher } from '@graphiql/toolkit';
import { Box, Toolbar } from '@mui/material';
import GraphiQL from 'graphiql';
import 'graphiql/graphiql.css';
import cfg from '../common/config';

interface Props {
    getAccessToken: () => Promise<string>
}

const fetcher = (getAccessToken: () => Promise<string>): Fetcher => createGraphiQLFetcher({
    url: `${cfg.apiUrl}/graphql`,
    fetch: (input: URL | RequestInfo, config?: RequestInit | undefined) => {
        return getAccessToken().then(accessToken => {
            if (config) {
                config.headers = { ...config.headers, "Authorization": `bearer ${accessToken}` }
            }
            return fetch(input, config) // nosemgrep: nodejs_scan.javascript-ssrf-rule-node_ssrf
        });
    }
});

function GraphiQLEditor(props: Props) {
    const f = fetcher(props.getAccessToken);

    return (
        <Box sx={{
            position: 'absolute',
            top: 0,
            left: 0,
            width: '100%',
            height: '100%',
            display: 'flex',
            flexDirection: 'column'
        }}>
            <Toolbar />
            <GraphiQL
                fetcher={f}
            />
        </Box>
    );
}

export default GraphiQLEditor
