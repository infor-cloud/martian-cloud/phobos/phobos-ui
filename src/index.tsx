import moment from 'moment';
import { UserManager } from 'oidc-client-ts';
import React from 'react';
import { createRoot } from 'react-dom/client';
import { AuthContext, AuthContextProps, AuthProvider } from 'react-oidc-context';
import App from './App';
import environment from './RelayEnvironment';
import config from './common/config';
import reportWebVitals from './reportWebVitals';
import { buildRoutes } from './routes/AppRoutes';
import './insights/registerChartjs';

const authSettingsQuery = `query srcQuery {
    authSettings {
        oidcIssuerUrl
        oidcClientId
        oidcScope
    }
}`;

overwriteResizeObserver();

const container = document.getElementById('root');

// Use createRoot to enable React concurrent mode
if (!container) throw new Error('Failed to find the root element');
const root = createRoot(container);

const graphqlEndpoint = `${config.apiUrl}/graphql`;

// A deferred promise is used here since we need to wait for the auth provider context
// to be initialized before the getAccessToken function can be used. This is required
// since the Relay environment is created before the auth provider.
const deferredToken = (() => {
    const props = {} as any;
    props.promise = new Promise((resolve) => props.resolve = resolve);
    return props;
})();

export const getAccessToken = async () => {
    const getToken = await deferredToken.promise;
    return getToken();
}

fetch(graphqlEndpoint, {
    method: 'POST',
    headers: {
        'Content-Type': 'application/json',
    },
    body: JSON.stringify({
        query: authSettingsQuery,
        variables: {},
    }),
}).then(async response => {
    const { authSettings } = (await response.json()).data;

    const oidcConfig = {
        authority: authSettings.oidcIssuerUrl,
        client_id: authSettings.oidcClientId,
        redirect_uri: window.location.origin,
    };

    const userManager = new UserManager(oidcConfig);

    const relayEnv = environment(getAccessToken);
    const router = buildRoutes(getAccessToken);

    root.render(
        <React.StrictMode>
            <AuthProvider userManager={userManager}>
                <AuthContext.Consumer>
                    {(props: (AuthContextProps | undefined)) => {
                        if (props) {
                            deferredToken.resolve(() => userManager.getUser().then((user: any) => {
                                // Check user.profile.exp to calculate expiration instead of expired field
                                // since it's more accurate
                                const current_ts = moment().unix();
                                if (!user || user.profile.exp < current_ts) {
                                    return props.signinRedirect();
                                }
                                return user?.id_token
                            }));
                        }
                        return (
                            <App relayEnvironment={relayEnv} router={router} />
                        );
                    }}
                </AuthContext.Consumer>
            </AuthProvider>
        </React.StrictMode>
    );
}).catch(error => {
    const msg = `failed to query auth settings from ${graphqlEndpoint}: ${error}`;
    console.error(msg);
    root.render(
        <React.StrictMode>
            {msg}
        </React.StrictMode>
    );
});

// This is needed due to a bug in the monaco editor that causes the ResizeObserver to
// throw a "ResizeObserver loop limit exceeded" error.
function overwriteResizeObserver() {
    const OriginalResizeObserver = (window as any).ResizeObserver;

    (window as any).ResizeObserver = function (callback: ResizeObserverCallback) {
        const wrappedCallback: ResizeObserverCallback = (entries, observer) => {
            // Prevent resize limit exceeded error by wrapping callback in requestAnimationFrame
            window.requestAnimationFrame(() => {
                callback(entries, observer);
            });
        };

        return new OriginalResizeObserver(wrappedCallback);
    };

    // Copy over static methods, if any
    for (const staticMethod in OriginalResizeObserver) {
        // eslint-disable-next-line no-prototype-builtins
        if (OriginalResizeObserver.hasOwnProperty(staticMethod)) {
            (window as any).ResizeObserver[staticMethod] = OriginalResizeObserver[staticMethod];
        }
    }
}

// If you want to start measuring performance in your app, pass a function
// to log results (for example: reportWebVitals(console.log))
// or send to an analytics endpoint. Learn more: https://bit.ly/CRA-vitals
reportWebVitals();
