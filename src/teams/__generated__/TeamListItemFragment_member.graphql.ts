/**
 * @generated SignedSource<<bc6b0e00128a5c2bb25dcfff19ab2a6a>>
 * @lightSyntaxTransform
 * @nogrep
 */

/* tslint:disable */
/* eslint-disable */
// @ts-nocheck

import { Fragment, ReaderFragment } from 'relay-runtime';
import { FragmentRefs } from "relay-runtime";
export type TeamListItemFragment_member$data = {
  readonly isMaintainer: boolean;
  readonly metadata: {
    readonly updatedAt: any;
  };
  readonly user: {
    readonly email: string;
    readonly username: string;
  };
  readonly " $fragmentType": "TeamListItemFragment_member";
};
export type TeamListItemFragment_member$key = {
  readonly " $data"?: TeamListItemFragment_member$data;
  readonly " $fragmentSpreads": FragmentRefs<"TeamListItemFragment_member">;
};

const node: ReaderFragment = {
  "argumentDefinitions": [],
  "kind": "Fragment",
  "metadata": null,
  "name": "TeamListItemFragment_member",
  "selections": [
    {
      "alias": null,
      "args": null,
      "concreteType": "User",
      "kind": "LinkedField",
      "name": "user",
      "plural": false,
      "selections": [
        {
          "alias": null,
          "args": null,
          "kind": "ScalarField",
          "name": "username",
          "storageKey": null
        },
        {
          "alias": null,
          "args": null,
          "kind": "ScalarField",
          "name": "email",
          "storageKey": null
        }
      ],
      "storageKey": null
    },
    {
      "alias": null,
      "args": null,
      "concreteType": "ResourceMetadata",
      "kind": "LinkedField",
      "name": "metadata",
      "plural": false,
      "selections": [
        {
          "alias": null,
          "args": null,
          "kind": "ScalarField",
          "name": "updatedAt",
          "storageKey": null
        }
      ],
      "storageKey": null
    },
    {
      "alias": null,
      "args": null,
      "kind": "ScalarField",
      "name": "isMaintainer",
      "storageKey": null
    }
  ],
  "type": "TeamMember",
  "abstractKey": null
};

(node as any).hash = "e1e735d1ce0219d5d7d92685513664a6";

export default node;
