import { Avatar, Box, Stack, Typography } from '@mui/material';
import { purple } from '@mui/material/colors';
import { useParams } from 'react-router-dom';
import { useLazyLoadQuery } from 'react-relay/hooks';
import graphql from 'babel-plugin-relay/macro';
import TeamList from './TeamList';
import { TeamDetailsQuery } from './__generated__/TeamDetailsQuery.graphql';
import PRNButton from '../common/PRNButton';

const query = graphql`
    query TeamDetailsQuery($prn: String!, $first: Int!, $after: String) {
        node(id: $prn) {
            __typename
            ...on Team {
                name
                description
                ...TeamListFragment_members
            }
        }
    }
`;

function TeamDetails() {
    const teamName  = useParams().teamName as string;

    const queryData = useLazyLoadQuery<TeamDetailsQuery>(query, { prn: `prn:team:${teamName}`, first: 50 }, { fetchPolicy: 'store-and-network' });

    if (queryData.node && queryData.node.__typename === 'Team') {
        return (
            <Box maxWidth={1000} margin="auto" padding={2}>
                <Box sx={{ display: "flex", justifyContent: 'space-between'}}>
                    <Box display="flex" marginBottom={4} alignItems="center">
                        <Avatar sx={{ width: 56, height: 56, marginRight: 2, bgcolor: purple[300] }} variant="rounded">{queryData.node.name[0].toUpperCase()}</Avatar>
                        <Stack>
                            <Typography noWrap variant="h5" sx={{ maxWidth: 400, fontWeight: "bold" }}>{queryData.node.name}</Typography>
                            <Typography color="textSecondary" variant="subtitle2">{`${queryData.node.description.slice(0, 50)}${queryData.node.description.length > 50 ? '...' : ''}`}</Typography>
                        </Stack>
                    </Box>
                    <PRNButton prn={`prn:team:${teamName}`} />
                </Box>
                <TeamList fragmentRef={queryData.node} />
            </Box>
        );
    } else {
        return <Box display="flex" justifyContent="center" paddingTop={2}>
            <Typography color="textSecondary">Team not found</Typography>
        </Box>;
    }
}

export default TeamDetails
