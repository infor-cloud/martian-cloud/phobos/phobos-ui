import React from 'react';
import moment from 'moment';
import Gravatar from '../common/Gravatar';
import graphql from 'babel-plugin-relay/macro';
import { Box, Stack, TableCell, TableRow } from '@mui/material';
import { useFragment } from 'react-relay/hooks';
import { TeamListItemFragment_member$key } from './__generated__/TeamListItemFragment_member.graphql';

interface Props {
    fragmentRef: TeamListItemFragment_member$key
}

function TeamListItem({ fragmentRef }: Props) {

    const data = useFragment<TeamListItemFragment_member$key>(graphql`
        fragment TeamListItemFragment_member on TeamMember {
            user {
                username
                email
            }
            metadata {
                updatedAt
            }
            isMaintainer
        }
    `, fragmentRef);

    return (
        <TableRow>
            <TableCell sx={{ fontWeight: 'bold' }}>
                <Stack direction="row" alignItems="center" spacing={1}>
                    <React.Fragment>
                        <Gravatar width={24} height={24} sx={{ marginRight: 1 }} email={data.user.email ?? ''} />
                        <Box>{data.user.username}</Box>
                    </React.Fragment>
                </Stack>
            </TableCell>
            <TableCell>
                {data.isMaintainer ? 'Yes' : 'No'}
            </TableCell>
            <TableCell>
                {moment(data.metadata.updatedAt as moment.MomentInput).fromNow()}
            </TableCell>
        </TableRow>
    );
}

export default TeamListItem
