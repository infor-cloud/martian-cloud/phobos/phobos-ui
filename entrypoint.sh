#!/bin/sh -e
set -e

# Replace env var placeholder in env.js file
sed -i 's#__PHOBOS_API_ENDPOINT__#'"$PHOBOS_API_ENDPOINT"'#g' /bin/www/env.js
sed -i 's#__PHOBOS_DOCUMENTS_URL__#'"$PHOBOS_DOCUMENTS_URL"'#g' /bin/www/env.js
sed -i 's#__PHOBOS_UI_VERSION__#'"$PHOBOS_UI_VERSION"'#g' /bin/www/env.js

exec "$@"
